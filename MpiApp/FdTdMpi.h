/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FDTDMPI_H
#define FDTDLIFE_FDTDMPI_H

#include <string>
#include <Fdtd/Model.h>
#include <chrono>
#include <Fdtd/Notifiable.h>
#include <list>
#include <memory>
class RemoteNode;
namespace fdtd {class Job;}

// The main class for the MPI processes
class FdTdMpi : public fdtd::Model, public fdtd::Notifiable {
public:
    // Construction
    FdTdMpi(const std::string& pathName, const std::string& fileName);

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // Constants
    enum MpiTags {tagExit=0, tagJob};

protected:
    // Variables
    int worldSize_;
    int worldRank_;
    std::string processorName_;
    bool going_;
    std::chrono::time_point<std::chrono::steady_clock> lastTime_;
    std::list<RemoteNode*> remoteNodes_;
    std::shared_ptr<fdtd::Job> clientJob_;
    bool runningModel_;

protected:
    // Helpers
    void waitForDebugger();
};


#endif //FDTDLIFE_FDTDMPI_H
