/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AppConnection.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <cstring>

// Constructor.
AppConnection::AppConnection() {
}

// Open the network connection with the application and
// receive messages from it.
void AppConnection::run() {
    struct sockaddr_in server {};
    socket_ = socket(AF_INET, SOCK_STREAM, 0);
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(9101);
    if(connect(socket_, (struct sockaddr*)&server, sizeof(server)) < 0) {
        std::cout << ": Failed to connect to Qt application" << std::endl;
    } else {
        std::cout << ": Connected to Qt application" << std::endl;
        // Handle requests
        bool running = true;
        char message[maxMessageSize_+1];
        while(running) {
            ssize_t result = recv(socket_, message, maxMessageSize_, 0);
            if(result > 0 && result <= maxMessageSize_) {
                // Message received
                message[result] = '\0';
                // Use the message
            } else if(result == 0) {
                // Disconnected
                running = false;
            } else {
                // Error
                int err = errno;
                std::cout << ": Error in Qt application comms: "
                          << strerror(err) << std::endl;
                running = false;
            }
        }
    }
    // Close the socket
    close(socket_);
}

