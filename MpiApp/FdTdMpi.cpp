/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FdTdMpi.h"
#include "RemoteNode.h"
#include <Fdtd/NodeManager.h>
#include <Seq/Sequencer.h>
#include <mpi.h>
#include <unistd.h>
#include <Xml/DomObject.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/DomDocument.h>
#include <Xml/Exception.h>
#include <Fdtd/Job.h>

// Constructor
FdTdMpi::FdTdMpi(const std::string& pathName, const std::string& fileName) :
        Notifiable(this),
        going_(false),
        runningModel_(false) {
    // Initialise MPI and retrieve information
    int mpiThreadSupport;
    MPI_Init_thread(nullptr, nullptr, MPI_THREAD_FUNNELED, &mpiThreadSupport);
    MPI_Comm_size(MPI_COMM_WORLD, &worldSize_);
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank_);
    char name[MPI_MAX_PROCESSOR_NAME];
    int nameLen;
    MPI_Get_processor_name(name, &nameLen);
    processorName_.assign(name, (size_t) nameLen);
    std::cout << "FDTD MPI application, " << worldRank_ << " of " << worldSize_
              << " " << processorName_ << std::endl;
    std::cout << " - MPI thread support " << mpiThreadSupport << std::endl;
    //waitForDebugger();
    // Load the file
    readConfigFile(pathName.c_str(), fileName.c_str());
    p()->initialise();  // To calculate the number of cores being used
    std::cout << worldRank_ << " - Running " << pathName << fileName << ".fdtd using "
              << p()->numThreadsUsed() << " cores" << std::endl;
    // Who are we?
    if(worldRank_ == 0) {
        // We are the master, create the remote nodes
        for(int i=1; i<worldSize_; i++) {
            auto* node = new RemoteNode(this, i);
            processingMgr_->addNode(node);
            remoteNodes_.push_back(node);
        }
        lastTime_ = std::chrono::steady_clock::now();
        // Run the model
        going_ = start();
        std::cout << worldRank_ << " - Starting model, going=" << going_ << std::endl;
        while(going_) {
            // Step the local model
            step();
            // See if any of the remote models have completed
            for(auto& node : remoteNodes_) {
                node->tryComplete();
            }
        }
        // We are done, stop the other processes
        for(int i = 1; i < worldSize_; i++) {
            int intData;
            MPI_Send(&intData, 1, MPI_INT, i, tagExit, MPI_COMM_WORLD);
        }
    } else {
        // We are a client
        // Tell the sequencers that they are slaves
        processingMgr_->slave(true);
        for(auto& sequencer : sequencers_.sequencers()) {
            sequencer->slave(true);
        }
        // Run the model
        going_ = true;
        while(going_) {
            // Wait for a message
            int count;
            MPI_Status status {};
            MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_CHAR, &count);
            std::string text;
            text.resize((unsigned long)count);
            MPI_Recv(&text[0], count, MPI_CHAR, 0, status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            // What are we doing?
            switch(status.MPI_TAG) {
                case tagExit:
                    going_ = false;
                    break;
                case tagJob: {
                    std::cout << worldRank_ << " - Received job" << std::endl;
                    // Parse into a DOM
                    xml::Reader reader;
                    xml::DomDocument dom("job");
                    try {
                        std::cout << worldRank_ << " - Got job: " << std::endl << text << std::endl;    
                        reader.readString(&dom, text);
                        // Now process the DOM
                        std::cout << worldRank_ << " - DOM obj=" << (void*)dom.getObject() << std::endl;
                        clientJob_.reset(fdtd::Job::factory(*dom.getObject(), this));
                        if(clientJob_ != nullptr) {
                            // Start the job
                            processingMgr_->addJob(clientJob_);
                            // Now step the model
                            runningModel_ = start();
                            while(runningModel_) {
                                step();
                            }
                        } else {
                            std::cout << worldRank_ << " - Could not decode job" << std::endl;
                            std::cout << text << std::endl;
                            std::cout << "=========" << std::endl;
                        }
                    } catch(xml::Exception& e) {
                        std::cout << worldRank_ << " - Failed to decode Job XML: " << e.what() << std::endl;
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }
    // Finish
    MPI_Finalize();
}

// Wait for the debugger to be attached
void FdTdMpi::waitForDebugger() {
    if(worldRank_ == 0) {
        volatile int i=0;
        fprintf(stderr, "pid %ld waiting for debugger\n", (long)getpid());
        while(i==0) {
            /* Changed i in the debugger */
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
}

// Handle notifications
void FdTdMpi::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(worldRank_ == 0) {
        // Notification from the master
        if(source == processingMgr_) {
            switch(why) {
                case fdtd::NodeManager::notifyAllJobsComplete:
                    std::cout << worldRank_ << " - All jobs complete" << std::endl;
                    going_ = false;
                    break;
                case fdtd::NodeManager::notifyJobComplete:
                    std::cout << worldRank_ << " - Local job complete" << std::endl;
                    break;
                default:
                    break;
            }
        }
    } else {
        // Notification from a client
        if(source == processingMgr_) {
            switch(why) {
                case fdtd::NodeManager::notifyJobComplete: {
                    std::cout << worldRank_ << " - Client job complete" << std::endl;
                    // Send the result back to the master
                    runningModel_ = false;
                    xml::DomObject* root = new xml::DomObject("job");
                    xml::DomDocument dom(root);
                    *root << *clientJob_;
                    xml::Writer writer;
                    std::string text;
                    writer.writeString(&dom, text);
                    std::cout << worldRank_ << " - Sending result" << std::endl;
                    MPI_Send(&text[0], (int)text.length(), MPI_CHAR, 0,
                             FdTdMpi::tagJob, MPI_COMM_WORLD);
                    break;
                }
                default:
                    break;
            }
        }
    }
    if(source == this) {
        switch(why) {
            case notifySteppedH:
                if((p()->timeStep() % 50) == 0) {
                    std::cout << worldRank_ << " - Step " << p()->timeStep() << " complete" << std::endl;
                }
                break;
            default:
                break;
        }
    }
}
