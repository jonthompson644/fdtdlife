/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RemoteNode.h"
#include <Fdtd/Job.h>
#include <Fdtd/NodeManager.h>
#include "FdTdMpi.h"
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Seq/Sequencer.h>
#include <mpi.h>

// Constructor
RemoteNode::RemoteNode(FdTdMpi *model, int nodeNumber) :
        Node(model->processingMgr()),
        model_(model),
        nodeNumber_(nodeNumber) {
    std::stringstream buf;
    buf << "RemoteNode:" << nodeNumber_;
    name_ = buf.str();
}

// Process this job
void RemoteNode::process(const std::shared_ptr<fdtd::Job>& job) {
    Node::process(job);
    // Package the job information up into a message
    xml::DomObject* root = new xml::DomObject("job");
    xml::DomDocument dom(root);
    *root << *job;
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom, text);
    // Now send it to the remote node
    MPI_Send(&text[0], (int)text.length(), MPI_CHAR, nodeNumber_,
            FdTdMpi::tagJob, MPI_COMM_WORLD);
}

// Try to complete the job
void RemoteNode::tryComplete() {
    // First a non-blocking check to see if the result is waiting
    int flag = 0;
    MPI_Status status {};
    MPI_Iprobe(nodeNumber_, FdTdMpi::tagJob, MPI_COMM_WORLD, &flag, &status);
    if(flag) {
        // How much data to receive
        int count=0;
        MPI_Get_count(&status, MPI_CHAR, &count);
        // Receive the result
        std::string text;
        text.resize((size_t)count);
        MPI_Recv(&text[0], count, MPI_CHAR, status.MPI_SOURCE,
                 status.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        std::cout << "0 - Client job result received" << std::endl;
        // Decode the returned message
        xml::Reader reader;
        xml::DomDocument dom("job");
        try {
            reader.readString(&dom, text);
            // Now process the DOM
            *dom.getObject() >> *job_;
        } catch(xml::Exception& e) {
            std::cout << "Failed to Job XML: " << e.what() << std::endl;
        }
        // Indicate job completion
        job_->complete(model_);
        job_->sequencer()->jobComplete(job_);
        model_->processingMgr()->jobDone(this);
    }
}
