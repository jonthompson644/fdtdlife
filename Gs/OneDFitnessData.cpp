/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "OneDFitnessData.h"
#include <complex>

// Write the data to the XML DOM
template <class T>
void gs::OneDFitnessData<T>::write(xml::DomObject& root) const {
    // Base class first
    FitnessData::write(root);
    // Now my stuff
    root << xml::Reopen();
    for(auto& d : data_) {
        root << xml::Obj("data") << d;
    }
    root << xml::Obj("axisxmin") << axisXMin_;
    root << xml::Obj("axisxstep") << axisXStep_;
    root << xml::Obj("axisxunits") << axisXUnits_;
    root << xml::Obj("axisymin") << axisYMin_;
    root << xml::Obj("axisymax") << axisYMax_;
    root << xml::Obj("axisxunits") << axisXUnits_;
    root << xml::Close();
}

// Read the data from the XML DOM
template <class T>
void gs::OneDFitnessData<T>::read(xml::DomObject& root) {
    // Base class first
    FitnessData::read(root);
    // Now my stuff
    root >> xml::Reopen();
    data_.clear();
    for(auto& o : root.obj("data")) {
        data_.emplace_back();
        *o >> data_.back();
    }
    root >> xml::Obj("axisxmin") >> axisXMin_;
    root >> xml::Obj("axisxstep") >> axisXStep_;
    root >> xml::Obj("axisxunits") >> axisXUnits_;
    root >> xml::Obj("axisymin") >> axisYMin_;
    root >> xml::Obj("axisymax") >> axisYMax_;
    root >> xml::Obj("axisxunits") >> axisXUnits_;
    root >> xml::Close();
}

// Set the size of the data array
template <class T>
void gs::OneDFitnessData<T>::setSize(size_t s, size_t numDataSets) {
    data_.resize(numDataSets);
    for(size_t i=0; i<numDataSets; i++) {
        data_[i].resize(s);
    }
}

// Write a data value into the array
template <class T>
void gs::OneDFitnessData<T>::setData(size_t x, T v, size_t dataSet) {
    data_.at(dataSet).at(x) = v;
}

template <class T>
void gs::OneDFitnessData<T>::setXAxisInfo(double min, double step, const std::string& units) {
    axisXMin_ = min;
    axisXStep_ = step;
    axisXUnits_ = units;
}

template <class T>
void gs::OneDFitnessData<T>::setYAxisInfo(double min, double max, const std::string& units) {
    axisYMin_ = min;
    axisYMax_ = max;
    axisYUnits_ = units;
}

template class gs::OneDFitnessData<double>;
template class gs::OneDFitnessData<std::complex<double>>;
