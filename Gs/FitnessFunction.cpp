/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessFunction.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Xml/DomObject.h>
#include <Xml/Exception.h>
#include "Individual.h"
#include "FitnessTransmittance.h"
#include "FitnessPhaseShift.h"
#include "FitnessPhaseShiftChange.h"
#include "Fitness3dBPoint.h"
#include "FitnessAdmittance.h"
#include "FitnessBirefPhaseDiff.h"
#include "FitnessAreaIntensity.h"
#include "FitnessIntensityLine.h"
#include "FitnessInPhaseLine.h"
#include "FitnessFilter.h"
#include "FitnessAnalyser.h"

// Constructor
gs::FitnessFunction::FitnessFunction(GeneticSearch* s) :
        s_(s) {
    // Make the fitness function factory
    factory_.define(new FitnessFactory::Creator<FitnessTransmittance>(
            modeAttenuation, "Transmittance"));
    factory_.define(new FitnessFactory::Creator<FitnessPhaseShift>(
            modePhaseShift, "Phase Shift"));
    factory_.define(new FitnessFactory::Creator<FitnessPhaseShiftChange>(
            modePhaseShiftChange, "Phase Shift Change"));
    factory_.define(new FitnessFactory::Creator<Fitness3dBPoint>(
            mode3dBPoint, "3dB Point"));
    factory_.define(new FitnessFactory::Creator<FitnessAdmittance>(
            modeAdmittance, "Admittance"));
    factory_.define(new FitnessFactory::Creator<FitnessBirefPhaseDiff>(
            modeBirefPhaseDiff, "Birefringent Phase Difference"));
    factory_.define(new FitnessFactory::Creator<FitnessAreaIntensity>(
            modeAreaIntensity, "Area Intensity"));
    factory_.define(new FitnessFactory::Creator<FitnessIntensityLine>(
            modeIntensityLine, "Intensity on a Line"));
    factory_.define(new FitnessFactory::Creator<FitnessInPhaseLine>(
            modeInPhaseLine, "Phase on a Line"));
    factory_.define(new FitnessFactory::Creator<FitnessFilter>(
            modeFilter, "Filter Response"));
    factory_.define(new FitnessFactory::Creator<FitnessAnalyser>(
            modeAnalyser, "Analyser"));
}

// Destructor
gs::FitnessFunction::~FitnessFunction() {
    clear();
}

// Clear the fitness functions
void gs::FitnessFunction::clear() {
    functions_.clear();
}

// Initialise the fitness functions
void gs::FitnessFunction::initialise(size_t numFrequencies, double frequencyStart,
                                     double frequencySpacing) {
    for(auto& f : functions_) {
        f->initialise(numFrequencies, frequencyStart, frequencySpacing);
    }
}

// Calculate the unfitness parameter for the specified data.
// The bigger this number, the less fit is the solution.
double gs::FitnessFunction::unfitness(Individual* ind) {
    double result = 0.0;
    for(auto& f : functions_) {
        double unfitness = f->unfitness(ind);
        result += unfitness;
    }
    return result;
}

// Write the configuration to the DOM object
void gs::FitnessFunction::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& f : functions_) {
        root << xml::Obj("fitnessfunction") << *f;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessFunction::readConfig(xml::DomObject& root) {
    clear();
    // Read the functions
    root >> xml::Open();
    for(auto& p : root.obj("fitnessfunction")) {
        try {
            functions_.emplace_back(factory_.restore(*p, s_));
        } catch(xml::Exception&) {
            // Functions did not used to have identifiers
            functions_.emplace_back(factory_.duplicate(*p, s_));
        }
        *p >> *functions_.back();
    }
    root >> xml::Close();
    // Update the object
    initialise(s_->numFrequencies(), s_->frequencySpacing(),
               s_->frequencySpacing());
}

// Return the fitness function associated with the index
gs::FitnessBase* gs::FitnessFunction::getFunction(int index) {
    gs::FitnessBase* result = nullptr;
    auto pos = functions_.begin();
    std::advance(pos, index);
    // This is the one
    if(pos != functions_.end()) {
        result = pos->get();
    }
    return result;
}

// Remove the function
void gs::FitnessFunction::removeFunction(FitnessBase* fn) {
    functions_.remove_if([fn](const std::unique_ptr<FitnessBase>& p) {
        return p.get() == fn;
    });
}

// Make a fitness function item from an XML object
gs::FitnessBase* gs::FitnessFunction::make(xml::DomObject& o) {
    functions_.emplace_back(factory_.duplicate(o, s_));
    o >> *functions_.back();
    return functions_.back().get();
}

// Make a fitness function item of a particular type
gs::FitnessBase* gs::FitnessFunction::make(int mode) {
    functions_.emplace_back(factory_.make(mode, s_));
    return functions_.back().get();
}

// And FDTD step has completed
void gs::FitnessFunction::stepComplete() {
    for(auto& f : functions_) {
        f->stepComplete();
    }
}
