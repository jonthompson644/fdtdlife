/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsSpecBoolean.h"
#include <Xml/DomObject.h>
#include "Chromosome.h"

// Initialising constructor
gs::GeneBitsSpecBoolean::GeneBitsSpecBoolean(Function function) :
        function_(function) {
}

// Copy constructor
gs::GeneBitsSpecBoolean::GeneBitsSpecBoolean(const gs::GeneBitsSpecBoolean& other) {
    *this = other;
}

// Equality operator
bool gs::GeneBitsSpecBoolean::operator==(const gs::GeneBitsSpecBoolean& other) const {
    return function_ == other.function_;
}

// Inequality operator
bool gs::GeneBitsSpecBoolean::operator!=(const gs::GeneBitsSpecBoolean& other) const {
    return !(*this == other);
}

// Write the specification to the XML DOM
void gs::GeneBitsSpecBoolean::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("function") << function_;
    p << xml::Close();
}

// Read the specification from the XML DOM
void gs::GeneBitsSpecBoolean::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("function") >> xml::Default(Function::evolve) >> function_;
    p >> xml::Close();
}

// Encode a value into the chromosome
void gs::GeneBitsSpecBoolean::encode(Chromosome* c, bool value) {
    switch(function_) {
        case Function::evolve:
            c->encode(static_cast<uint64_t>(value), 1U);
            break;
        case Function::fixedFalse:
        case Function::fixedTrue:
        default:
            break;
    }
}

// Decode a value from the chromosome
bool gs::GeneBitsSpecBoolean::decode(Chromosome* c) {
    bool result;
    switch(function_) {
        case Function::evolve:
            result = static_cast<bool>(c->decode(1U));
            break;
        default:
        case Function::fixedFalse:
            result = false;
            break;
        case Function::fixedTrue:
            result = true;
            break;
    }
    return result;
}
