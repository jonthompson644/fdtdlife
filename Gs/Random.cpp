/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Random.h"
#include <chrono>

// Construtor
gs::Random::Random() :
        distribution_(0.0, 1.0) {
    // Seed the random number generator from the clock
    generator_.seed((unsigned int) std::chrono::high_resolution_clock::now().
            time_since_epoch().count());
}

// Return a random number less than the upper bound
double gs::Random::get(double upper) {
    return distribution_(generator_) * upper;
}

// Return a random number between two bounds.
// Includes the lower, doesn't include the upper.
size_t gs::Random::get(size_t lower, size_t upper) {
    return get(upper - lower) + lower;
}

// Return a random number less than the upper bound
size_t gs::Random::get(size_t upper) {
    return (size_t) floor(distribution_(generator_) * upper);
}

// Return a true/false with the given probability
bool gs::Random::flip(double probability) {
    double r = distribution_(generator_);
    return r < probability;
}
