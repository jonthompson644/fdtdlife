/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessBirefPhaseDiff.h"
#include <cmath>
#include <algorithm>
#include "GeneticSearch.h"
#include "Individual.h"
#include "OneDFitnessData.h"
#include <Sensor/ArraySensor.h>

// Second stage constructor
void gs::FitnessBirefPhaseDiff::construct(int mode, int id, GeneticSearch* m) {
    FitnessAdmittanceBase::construct(mode, id, m);
    rangeMin_ = -180.0;
    rangeMax_ = +180.0;
}

// Calculate the unfitness parameter for the specified data.
// The bigger this number, the less fit is the solution.
// We will use the sum of the square of the difference.
// This penalises a few large excursions from the desired over
// many small excursions.  This overridden version handles the
// 360 degree wrap round needed for phase data.
double gs::FitnessBirefPhaseDiff::unfitness(Individual* ind) {
    double result = 0.0;
    auto sensor = s_->getGeneticSensor();
    if(sensor != nullptr) {
        // Create the data objects
        auto dataX = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseShift, "PhaseShiftX"));
        auto dataY = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseShift, "PhaseShiftY"));
        dataX->setSize(min_.size());
        dataY->setSize(min_.size());
        // Assess the fitness and copy the data
        for(size_t i = 0; i < min_.size(); i++) {
            double minRequired = min_[i];
            double maxRequired = max_[i];
            double measured;
            double x = 0.0;
            double y = 0.0;
            sensor->points().at(sensor->index(0, 0)).phaseShiftAt(i, x, y, false);
            dataX->setData(i, x);
            dataY->setData(i, y);
            measured = x - y;
            if(measured < -180.0) { measured += 360.0; }
            if(measured > 180.0) { measured -= 360.0; }
            if(minRequired <= maxRequired) {
                if(measured < minRequired) {
                    double error1 = minRequired - measured;
                    double error2 = (measured + 360.0) - maxRequired;
                    double error = std::min(error1, error2);
                    result += error * error;
                } else if(measured > maxRequired) {
                    double error1 = minRequired - (measured - 360.0);
                    double error2 = measured - maxRequired;
                    double error = std::min(error1, error2);
                    result += error * error;
                }
            } else {
                if(measured > maxRequired && measured < minRequired) {
                    double error1 = measured - maxRequired;
                    double error2 = minRequired - measured;
                    double error = std::min(error1, error2);
                    result += error * error;
                }
            }
        }
        result *= weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Return the specification data the function is using, if possible.
void gs::FitnessBirefPhaseDiff::getFitnessSpec(std::vector<double>& /*minTrans*/,
                                               std::vector<double>& /*maxTrans*/,
                                               std::vector<double>& minPhase,
                                               std::vector<double>& maxPhase,
                                               const FitnessPartResult* /*result*/) {
    minPhase = min_;
    maxPhase = max_;
}
