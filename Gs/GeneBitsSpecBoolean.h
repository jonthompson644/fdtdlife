/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEBITSSPECBOOLEAN_H
#define FDTDLIFE_GENEBITSSPECBOOLEAN_H

#include <cstddef>
#include <cstdint>

namespace xml { class DomObject; }

namespace gs {

    class Chromosome;

    // A class the represents the gene bits specification for a boolean.
    class GeneBitsSpecBoolean {
    public:
        // Types
        enum class Function {
            evolve, fixedFalse, fixedTrue
        };

        // Construction
        GeneBitsSpecBoolean() = default;
        explicit GeneBitsSpecBoolean(Function f);
        GeneBitsSpecBoolean(const GeneBitsSpecBoolean& other);

        // Operators
        GeneBitsSpecBoolean& operator=(const GeneBitsSpecBoolean& other) = default;
        bool operator==(const GeneBitsSpecBoolean& other) const;
        bool operator!=(const GeneBitsSpecBoolean& other) const;

        // API
        void writeXml(xml::DomObject& p) const;
        void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneBitsSpecBoolean& v) {
            v.writeXml(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneBitsSpecBoolean& v) {
            v.readXml(o);
            return o;
        }
        void encode(Chromosome* c, bool value);
        bool decode(Chromosome* c);

        // Getters
        Function function() const { return function_; }
        size_t bits() const { return function_ == Function::evolve ? 1U : 0U; }

        // Setters
        void function(Function v) { function_ = v; }

    protected:
        // Members
        Function function_{};
    };
}


#endif //FDTDLIFE_GENEBITSSPECBOOLEAN_H
