/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEFACTORY_H
#define FDTDLIFE_GENEFACTORY_H

#include <list>
#include "Gene.h"
namespace xml {class DomObject;}

namespace gs {

    class GeneTemplate;
    class Chromosome;
    class Random;

    // A class that creates genes
    class GeneFactory {
    public:
        GeneFactory();
        ~GeneFactory();
        void decode(Chromosome* c, GeneticSearch* s);
        void encode(Chromosome* c, GeneticSearch* s);
        GeneTemplate* getTemplate(int identifier);
        void addTemplate(GeneTemplate* t);
        void removeTemplate(GeneTemplate* t);
        size_t sizeBits();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneFactory& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneFactory& v) {v.readConfig(o); return o;}
        std::list<GeneTemplate*>& templates() {return templates_;}
        int allocIdentifier() {return nextIdentifier_++;}
        bool configurationValid() const {return !templates_.empty();}
        void clear();
        void setTemplates(const std::list<GeneTemplate*>& t) {templates_ = t;}
    protected:
        std::list<GeneTemplate*> templates_;
        int nextIdentifier_;
    };

}

#endif //FDTDLIFE_GENEFACTORY_H
