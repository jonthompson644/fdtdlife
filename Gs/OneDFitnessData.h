/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ONEDFITNESSDATA_H
#define FDTDLIFE_ONEDFITNESSDATA_H

#include "FitnessData.h"
#include <vector>
#include <string>

namespace gs {

    // One-dimensional data recorded with an individual by a fitness function
    template <class T>
    class OneDFitnessData : public FitnessData {
    public:
        // Construction
        OneDFitnessData() = default;

        // Overrides of FitnessData
        void write(xml::DomObject& root) const override;
        void read(xml::DomObject& root) override;

        // API
        void setSize(size_t s, size_t numDataSets=1);
        void setData(size_t x, T v, size_t dataSet=0);
        void setXAxisInfo(double min, double step, const std::string& units);
        void setYAxisInfo(double min, double max, const std::string& units);
        void clear() {data_.clear();}

        // Data access
        size_t numDataSets() const {return data_.size();}
        const std::vector<T>& data(size_t dataSet=0) const {return data_.at(dataSet);}
        double axisXMin() const {return axisXMin_;}
        double axisXStep() const {return axisXStep_;}
        const std::string& axisXUnits() const {return axisXUnits_;}
        double axisYMin() const {return axisYMin_;}
        double axisYMax() const {return axisYMax_;}
        const std::string& axisYUnits() const {return axisYUnits_;}

    protected:
        std::vector<std::vector<T>> data_;
        double axisXMin_ {};
        double axisXStep_ {};
        std::string axisXUnits_;
        double axisYMin_ {};
        double axisYMax_ {};
        std::string axisYUnits_;
    };
}


#endif //FDTDLIFE_ONEDFITNESSDATA_H
