/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSFUNCTION_H
#define FDTDLIFE_FITNESSFUNCTION_H

#include <list>
#include <memory>
#include "FunctionPoint.h"
#include "FitnessPartResult.h"
#include <Box/Factory.h>

namespace xml { class DomObject; }
namespace sensor { class Sensor; }

namespace gs {

    class GeneticSearch;

    class FitnessBase;

    class FitnessTransmittance;

    class FitnessPhaseShift;

    class Individual;

    // Represents the target function against which fitness is measured
    class FitnessFunction {
    public:
        // Types
        enum Mode {
            modeAttenuation = 0, modePhaseShift, modePhaseShiftChange,
            mode3dBPoint, modeAdmittance, modeBirefPhaseDiff, modeAreaIntensity,
            modeIntensityLine, modeInPhaseLine, modeFilter,
            modeAnalyser
        };
        typedef box::Factory<FitnessBase> FitnessFactory;

        // Construction
        explicit FitnessFunction(GeneticSearch* s);
        ~FitnessFunction();

        // API
        void initialise(size_t numFrequencies, double frequencyStart, double frequencySpacing);
        double unfitness(Individual* ind);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const FitnessFunction& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, FitnessFunction& v) {
            v.readConfig(o);
            return o;
        }
        void clear();
        FitnessBase* getFunction(int index);
        void removeFunction(FitnessBase* fn);
        bool configurationValid() const { return !functions_.empty(); }
        FitnessBase* make(xml::DomObject& o);
        FitnessBase* make(int mode);
        void stepComplete();

        // Getters
        const std::list<std::unique_ptr<FitnessBase>>& functions() const { return functions_; }
        GeneticSearch* s() { return s_; }
        FitnessFactory& factory() { return factory_; }

    protected:
        // Configuration
        std::list<std::unique_ptr<FitnessBase>> functions_;

        // Members
        GeneticSearch* s_;
        FitnessFactory factory_;
    };

}


#endif //FDTDLIFE_FITNESSFUNCTION_H
