/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessBase.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Xml/DomObject.h>
#include "Individual.h"
#include <Sensor/ArraySensor.h>

// Second stage constructor used by factory
void gs::FitnessBase::construct(int mode, int id, GeneticSearch* s) {
    mode_ = mode;
    identifier_ = id;
    s_ = s;
}

// Create the fitness data from the function segments
void gs::FitnessBase::initialise(size_t numSamples, double firstSample, double sampleSpacing) {
    min_.resize(numSamples);
    max_.resize(numSamples);
    FitnessPoint last(0.0, 0.0, 0.0);
    double nextSample = firstSample;
    size_t sampleN = 0;
    for(auto& point : points_) {
        // The equation of the straight lines between the two points
        double minM = (point.min() - last.min()) / (point.x() - last.x());
        double minC = last.min() - minM * last.x();
        double maxM = (point.max() - last.max()) / (point.x() - last.x());
        double maxC = last.max() - maxM * last.x();
        // Now fill the frequency samples between the two points
        while(nextSample > last.x() &&
              nextSample <= point.x() && sampleN < numSamples) {
            min_[sampleN] = minM * nextSample + minC;
            max_[sampleN] = maxM * nextSample + maxC;
            nextSample += sampleSpacing;
            sampleN++;
        }
        last = point;
    }
}

// Calculate the unfitness parameter for the specified data.
// The bigger this number, the less fit is the solution.
// We will use the sum of the square of the difference.
// This penalises a few large excursions from the desired over
// many small excursions.
double gs::FitnessBase::unfitness(Individual* ind) {
    double result = 0.0;
    getPhaseShiftData(ind, false);
    OneDFitnessData<double>* data = getTransmittanceData(ind, true);
    getTimeSeriesData(ind);
    if(data != nullptr) {
        // Now assess the fitness
        for(size_t i = 0; i < min_.size(); i++) {
            double minRequired = min_[i];
            double maxRequired = max_[i];
            double measured = data->data(0)[i];
            if(measured < minRequired) {
                result += (measured - minRequired) * (measured - minRequired);
            } else if(measured > maxRequired) {
                result += (measured - maxRequired) * (measured - maxRequired);
            }
        }
        result *= weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Create the transmittance data object and fill it
gs::OneDFitnessData<double>* gs::FitnessBase::getTransmittanceData(Individual* ind, bool withRange) {
    OneDFitnessData<double>* result = nullptr;
    auto sensor = s_->getGeneticSensor();
    if(sensor != nullptr) {
        // Create the transmittance data object
        std::string dataName;
        if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
            dataName = "Transmittance";
        } else if(dataSource_ == dataSourceYComponent) {
            dataName = "Transmittance Y";
        } else {
            dataName = "Transmittance X";
        }
        result = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataTransmittance, dataName));
        result->setSize(min_.size(), withRange ? 3 : 1);  // Data sets: data, max, min
        // Record data
        for(size_t i = 0; i < min_.size(); i++) {
            double minRequired = min_[i];
            double maxRequired = max_[i];
            double measured = 0.0;
            double extra = 0.0;
            if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
                sensor->points().at(sensor->index(0, 0)).propMatTransmittanceAt(i, measured);
            } else if(dataSource_ == dataSourceYComponent) {
                sensor->points().at(sensor->index(0, 0)).transmittanceAt(i, extra, measured, false);
            } else {
                sensor->points().at(sensor->index(0, 0)).transmittanceAt(i, measured, extra, false);
            }
            result->setData(i, measured, 0);
            if(withRange) {
                result->setData(i, minRequired, 1);
                result->setData(i, maxRequired, 2);
            }
        }
        // Set the data object info
        double frequencySpacing = s_->frequencySpacing() / box::Constants::giga_;
        result->setYAxisInfo(0.0, 1.0, "");
        result->setXAxisInfo(frequencySpacing, frequencySpacing, "GHz");
    }
    return result;
}

// Create the time series data object and fill it
gs::OneDFitnessData<double>* gs::FitnessBase::getTimeSeriesData(Individual* ind) {
    OneDFitnessData<double>* result = nullptr;
    if(s_->algorithm() != GeneticSearch::algorithmPropMatrix) {
        auto sensor = s_->getGeneticSensor();
        if(sensor != nullptr) {
            // Create the time series data object
            std::string dataName;
            if(dataSource_ == dataSourceYComponent) {
                dataName = "Time Series Y";
            } else {
                dataName = "Time Series X";
            }
            result = dynamic_cast<OneDFitnessData<double>*>(
                    ind->getFitnessData(s_, GeneticSearch::fitnessDataTimeSeries, dataName));
            result->setSize(sensor->nSamples(), 1);  // Data sets: data
            // Record data
            double maxAmpl = 0.0;
            for(size_t i = 0; i < sensor->nSamples(); i++) {
                double measured;
                if(dataSource_ == dataSourceYComponent) {
                    measured = sensor->points().at(sensor->index(0, 0)).
                            transmittedData().timeSeriesX()[i];
                } else {
                    measured = sensor->points().at(sensor->index(0, 0)).
                            transmittedData().timeSeriesY()[i];
                }
                maxAmpl = std::max(maxAmpl, measured);
                result->setData(i, measured, 0);
            }
            // Set the data object info
            result->setYAxisInfo(0.0, maxAmpl, "V/m");
            result->setXAxisInfo(0, s_->m()->p()->dt() / box::Constants::pico_, "ps");
        }
    }
    return result;
}

// Create the phase shift data object and fill it
gs::OneDFitnessData<double>* gs::FitnessBase::getPhaseShiftData(Individual* ind, bool withRange) {
    OneDFitnessData<double>* result = nullptr;
    auto sensor = s_->getGeneticSensor();
    if(sensor != nullptr) {
        // Create the phase shift data object
        std::string dataName;
        if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
            dataName = "Phase Shift";
        } else if(dataSource_ == dataSourceYComponent) {
            dataName = "Phase Shift Y";
        } else {
            dataName = "Phase Shift X";
        }
        result = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseShift, dataName));
        result->setSize(min_.size(), withRange ? 3 : 1);  // Data sets: data, max, min
        // Record data
        for(size_t i = 0; i < min_.size(); i++) {
            double minRequired = min_[i];
            double maxRequired = max_[i];
            double measured = 0.0;
            double extra = 0.0;
            if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
                sensor->points().at(sensor->index(0, 0)).propMatPhaseShiftAt(i, measured);
            } else if(dataSource_ == dataSourceYComponent) {
                sensor->points().at(sensor->index(0, 0)).phaseShiftAt(i, extra, measured, false);
            } else {
                sensor->points().at(sensor->index(0, 0)).phaseShiftAt(i, measured, extra, false);
            }
            result->setData(i, measured, 0);
            if(withRange) {
                result->setData(i, minRequired, 1);
                result->setData(i, maxRequired, 2);
            }
        }
        // Set the data object info
        double frequencySpacing = s_->frequencySpacing() / box::Constants::giga_;
        result->setYAxisInfo(-180.0, 180.0, "deg");
        result->setXAxisInfo(frequencySpacing, frequencySpacing, "GHz");
    }
    return result;
}

// Write the configuration to the DOM object
void gs::FitnessBase::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("weight") << weight_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("datasource") << dataSource_;
    for(auto& point : points_) {
        root << xml::Obj("fitnesspoint") << point;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessBase::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("weight") >> xml::Default(weight_) >> weight_;
    root >> xml::Obj("name") >> xml::Default(name_) >> name_;
    root >> xml::Obj("datasource") >> xml::Default(dataSourceMagnitude) >> dataSource_;
    points_.clear();
    for(auto& p : root.obj("fitnesspoint")) {
        points_.emplace_back();
        *p >> points_.back();
    }
    root >> xml::Close();
}

// Add a point to the fitness function
void gs::FitnessBase::addPoint(double frequency, double min, double max) {
    points_.emplace_back(frequency, min, max);
    points_.sort();
}

// Delete the point at the given index
void gs::FitnessBase::deletePoint(int index) {
    auto pos = points_.begin();
    for(int i = 0;
        i < index && pos != points_.end();
        i++) {
        pos++;
    }
    // Delete the row
    if(pos != points_.end()) {
        points_.erase(pos);
        points_.sort();
    }
}

// Set the point at the given index
void gs::FitnessBase::setPoint(int index, double frequency, double min, double max) {
    auto pos = points_.begin();
    for(int i = 0;
        i < index && pos != points_.end();
        i++) {
        pos++;
    }
    // Set the point
    if(pos != points_.end()) {
        pos->set(frequency, min, max);
        points_.sort();
    }
}

// Set the function parameters
void gs::FitnessBase::set(const std::string& name, double weight, DataSource dataSource) {
    name_ = name;
    weight_ = weight;
    dataSource_ = dataSource;
}

// Return the specification data the function is using, if possible.
void gs::FitnessBase::getFitnessSpec(std::vector<double>& minTrans,
                                     std::vector<double>& maxTrans,
                                     std::vector<double>& minPhase,
                                     std::vector<double>& maxPhase,
                                     const gs::FitnessPartResult* /*result*/) {
    minTrans.clear();
    maxTrans.clear();
    minPhase.clear();
    maxPhase.clear();
}
