/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableGene.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include <iomanip>

// Constructor
gs::VariableGene::VariableGene(const gs::GeneBitsSpecDouble& valueSpec, int variableId) :
        Gene(GeneTemplate::geneTypeVariable),
        valueSpec_(valueSpec),
        variableId_(variableId) {
}

// Encode the gene into the chromosome
void gs::VariableGene::encode(gs::Chromosome* c) {
    // Base class first
    Gene::encode(c);
    // Now my things
    valueSpec_.encode(c, value_);
}

// Decode the gene from the chromosome
void gs::VariableGene::decode(gs::Chromosome* c) {
    // Base class first
    Gene::decode(c);
    // Now my things
    value_ = valueSpec_.decode(c);
}

// Configure the mode from the gene
double gs::VariableGene::configureModel(gs::GeneticSearch* s, unsigned int /*what*/) {
    // Get the variable
    auto* var = s->m()->variables().find(variableId_);
    if(var != nullptr) {
        std::stringstream t;
        t << std::setprecision(10) << value_;
        var->text(t.str());
    }
    return 0.0;
}

// Copy the state from the other gene into this one
void gs::VariableGene::copyFrom(gs::Gene* other, fdtd::Model* m) {
    // Base class first
    Gene::copyFrom(other, m);
    // Now my stuff
    auto* from = dynamic_cast<VariableGene*>(other);
    if(from != nullptr) {
        value_ = from->value_;
    }
}

// Return the size of the gene
size_t gs::VariableGene::sizeBits() const {
    return Gene::sizeBits() + valueSpec_.bits();
}

// Return textual information about the state of the gene
std::string gs::VariableGene::info(GeneticSearch* s) const {
    std::stringstream t;
    auto* var = s->m()->variables().find(variableId_);
    if(var != nullptr) {
        t << var->name() << "=" << value_ << " (" << valueSpec_.min()
          << ".." << valueSpec_.max() << ")";
    }
    return t.str();
}
