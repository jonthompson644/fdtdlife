/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSADMITTANCE_H
#define FDTDLIFE_FITNESSADMITTANCE_H

#include "FitnessAdmittanceBase.h"
#include <Box/Polynomial.h>
#include <complex>

namespace gs {

    // Fitness function that matches a complex admittance specified
    // by a pair of polynomials.
    class FitnessAdmittance : public FitnessAdmittanceBase {
    public:
        // Overrides of FitnessAdmittanceBase
        double unfitness(Individual* ind) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void getFitnessSpec(std::vector<double>& minTrans,
                            std::vector<double>& maxTrans,
                            std::vector<double>& minPhase,
                            std::vector<double>& maxPhase,
                            const FitnessPartResult* result) override;

        // Getters
        const box::Polynomial& real() const { return real_; }
        const box::Polynomial& imag() const { return imag_; }
        double minRepeatCellScale() const {return minRepeatCellScale_;}
        int numRepeatCellScaleSamples() const {return numRepeatCellScaleSamples_;}

        // Setters
        void set(const std::string& name, double weight,
                 const box::Polynomial& real, const box::Polynomial& imag,
                 double minRepeatCellScale, int numRepeatCellScaleSamples);
        void set(const box::Polynomial& real, const box::Polynomial& imag);

    protected:
        // Configuration
        box::Polynomial real_;
        box::Polynomial imag_;
        double minRepeatCellScale_ {1.0};
        int numRepeatCellScaleSamples_ {1};

        // Constants
        static constexpr double maxRepeatCellScale_ = 1.0;

        // Helpers
        void findRangeMinMax();
    };
}


#endif //FDTDLIFE_FITNESSADMITTANCE_H
