/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <cassert>
#include "UnitCellLayerGene.h"
#include "Chromosome.h"

// Constructor
gs::UnitCellLayerGene::UnitCellLayerGene(GeneTemplate::GeneType geneType,
                                           size_t repeatBits, size_t repeatMin) :
        LayerGene(geneType),
        repeatBits_(repeatBits),
        repeatMin_(repeatMin) {
}

// Complete the decoding of a gene
void gs::UnitCellLayerGene::decode(Chromosome* c) {
    LayerGene::decode(c);
    repeat_ = c->decode(repeatBits_) + repeatMin_;
}

// Complete the encoding of a gene
void gs::UnitCellLayerGene::encode(Chromosome* c) {
    LayerGene::encode(c);
    c->encode(repeat_ - repeatMin_, repeatBits_);
}

// Connect the gene to other genes it depends on
// and work out which layer this is
bool gs::UnitCellLayerGene::connect(Chromosome* c, GeneticSearch* s) {
    LayerGene::connect(c, s);
    unitCell_ = repeatCell_ / repeat_;
    return true;
}

// Return the number of bits used by the gene
size_t gs::UnitCellLayerGene::sizeBits() const {
    return LayerGene::sizeBits() + repeatBits_;
}

// Copy the state from the other gene into this one
void gs::UnitCellLayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    LayerGene::copyFrom(other, m);
    auto* from = dynamic_cast<UnitCellLayerGene*>(other);
    if(from != nullptr) {
        repeat_ = from->repeat_;
    }
}
