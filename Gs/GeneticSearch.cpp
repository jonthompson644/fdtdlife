/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearch.h"
#include "Population.h"
#include <Fdtd/Model.h>
#include <Source/Sources.h>
#include <Source/PlaneWaveSource.h>
#include "FitnessFunction.h"
#include "Chromosome.h"
#include <Sensor/ArraySensor.h>
#include <Sensor/TwoDSlice.h>
#include "Individual.h"
#include "GeneTemplate.h"
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Fdtd/NodeManager.h>
#include <Box/Constants.h>
#include "OneDFitnessData.h"
#include "TwoDFitnessData.h"

// Constructor
gs::GeneticSearch::GeneticSearch() :
        selectionMode_(selectionFitnessProportional),
        algorithm_(algorithmFdtd),
        crossoverMode_(crossoverSingle),
        microGa_(false),
        tournamentSize_(4),
        breedingPoolSize_(defaultPopulationSize_ / 2),
        populationSize_(defaultPopulationSize_),
        nCellsXY_(64),
        cellSizeZ_(0.000010),
        sensorDistance_(0.001),
        gridMaterialId_(domain::Domain::defaultMaterial),
        frequencyRange_(1 * box::Constants::tera_),
        crossOverProbability_(0.25),
        mutationProbability_(0.01),
        numFrequencies_(128),
        unfitnessThreshold_(0.01),
        generationNumber_(0),
        keepParents_(false),
        averageSensor_(true),
        repeatCell_(0.0005),
        layerSpacing_(0.00002),
        layerSymmetry_(LayerSymmetry::layerSymmetryNone),
        xyComponents_(false),
        useExistingDomain_(false),
        population_(nullptr),
        fitness_(nullptr),
        frequencySpacing_(0.0),
        populationLoaded_(false) {
    population_ = new Population(populationSize_, this);
    fitness_ = new FitnessFunction(this);
    // Initialise the fitness data factory
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataTransmittance, "Transmittance"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<std::complex<double>>>(
                    fitnessDataAdmittance, "Admittance"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataPhaseShift, "Phase Shift"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataAmplitudeDist, "Amplitude Distribution"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataPhaseDist, "Phase Distribution"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataTransmittanceDist, "Transmittance Distribution"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<TwoDFitnessData<double>>(
                    fitnessDataField, "Field Component"));
    fitnessDataFactory_.define(
            new box::Factory<FitnessData>::Creator<OneDFitnessData<double>>(
                    fitnessDataTimeSeries, "Time Series"));
}

// Destructor
gs::GeneticSearch::~GeneticSearch() {
    delete population_;
    delete fitness_;
}

// Configuration setting
void gs::GeneticSearch::set(size_t populationSize, int nCellsXY,
                            double cellSizeZ, double sensorDistance,
                            int gridMaterialId, double frequencyRange,
                            double crossOverProbability, double mutationProbability,
                            double unfitnessThreshold, Selection selectionMode,
                            Algorithm algorithm, Crossover crossoverMode,
                            int tournamentSize, int breedingPoolSize, bool keepParents,
                            bool microGa, bool averageSensor, double repeatCell,
                            double layerSpacing) {
    // Are we going to need a new population?
    bool needsNewPopulation = population_ == nullptr ||
                              populationSize_ != populationSize;
    // Set the configuration
    populationSize_ = populationSize;
    nCellsXY_ = nCellsXY;
    cellSizeZ_ = cellSizeZ;
    sensorDistance_ = sensorDistance;
    gridMaterialId_ = gridMaterialId;
    frequencyRange_ = frequencyRange;
    crossOverProbability_ = crossOverProbability;
    mutationProbability_ = mutationProbability;
    unfitnessThreshold_ = unfitnessThreshold;
    frequencySpacing_ = frequencyRange / numFrequencies_;
    selectionMode_ = selectionMode;
    algorithm_ = algorithm;
    crossoverMode_ = crossoverMode;
    tournamentSize_ = tournamentSize;
    breedingPoolSize_ = breedingPoolSize;
    keepParents_ = keepParents;
    microGa_ = microGa;
    averageSensor_ = averageSensor;
    repeatCell_ = repeatCell;
    layerSpacing_ = layerSpacing;
    // Create the new population
    if(needsNewPopulation) {
        delete population_;
        population_ = new Population(populationSize_, this);
        generationNumber_ = 0;
        unfitnessProgress_.clear();
    }
}

// Clear and re-initialise the population
void gs::GeneticSearch::resetPopulation() {
    delete population_;
    population_ = new Population(populationSize_, this);
    generationNumber_ = 0;
    unfitnessProgress_.clear();
}

// Configuration setting
void gs::GeneticSearch::set(int nCellsXY,
                            double cellSizeZ, double sensorDistance,
                            int gridMaterialId, double frequencyRange) {
    // Set the configuration
    nCellsXY_ = nCellsXY;
    cellSizeZ_ = cellSizeZ;
    sensorDistance_ = sensorDistance;
    gridMaterialId_ = gridMaterialId;
    frequencyRange_ = frequencyRange;
    frequencySpacing_ = frequencyRange / numFrequencies_;
}

// Breed a new generation of individuals
void gs::GeneticSearch::breedNewGeneration() {
    double min, max, mean;
    population_->calculateStats(&min, &max, &mean);
    unfitnessProgress_.push_back(min);
    generationNumber_++;
    population_->breedNewGeneration(populationSize_, crossOverProbability_,
                                    mutationProbability_, selectionMode_,
                                    tournamentSize_, breedingPoolSize_,
                                    keepParents_, crossoverMode_, microGa_,
                                    this);
}

// Fill the list with individuals that require modelling
void gs::GeneticSearch::getWaitingIndividuals(std::list<std::shared_ptr<Individual>>& inds) {
    inds.clear();
    for(auto& ind : population_->individuals()) {
        if(!ind->isEvaluated()) {
            inds.push_back(ind);
        }
    }
}

// Configure the model for the evaluation of an individual in the population
void gs::GeneticSearch::loadIndividualIntoModel(const std::shared_ptr<Individual>& ind,
                                                unsigned int what) {
    population_->currentIndividual(ind->id());
    // Clear out any previous shapes and materials
    m_->clearSeqGenerated();
    layerShapeIds_.clear();
    layerMaterialIds_.clear();
    // Set general parameters
    if((what & GeneticSearch::individualLoadGeneral) != 0) {
        if(!useExistingDomain_) {
            m_->p()->boundary({fdtd::Configuration::Boundary::periodic,
                               fdtd::Configuration::Boundary::periodic,
                               fdtd::Configuration::Boundary::absorbing});
            m_->p()->boundarySize(40);
        }
        m_->p()->doFdtd(algorithm_ == algorithmFdtd);  // FDTD is turned on when required
        m_->p()->doPropagationMatrices(algorithm_ == algorithmPropMatrix);
    }
    // The test frequencies
    frequencySpacing_ = frequencyRange_ / numFrequencies_;
    // The default size of the model
    if(!useExistingDomain_) {
        double repeatCellSize = ind->getRepeatCell(repeatCell_);
        double cellSizeXY = repeatCellSize / nCellsXY_;
        double gridToTop = gridToTopCells_ * cellSizeZ_;
        double sensorToBottom = sensorToBottomCells_ * cellSizeXY;
        box::VectorExpression<double> p1(-repeatCellSize / 2.0, -repeatCellSize / 2.0,
                                         -gridToTop);
        box::VectorExpression<double> p2(repeatCellSize / 2.0, repeatCellSize / 2.0,
                                         sensorDistance_ + sensorToBottom);
        m_->p()->p1(p1);
        m_->p()->p2(p2);
        m_->p()->dr({cellSizeXY, cellSizeXY, cellSizeZ_});
    }
    // Use the individual
    double thickness = ind->configureModel(this, static_cast<int>(what));
    if(!useExistingDomain_) {
        // Adjust the z size of the model
        if((what & GeneticSearch::individualLoadDomainSize) != 0) {
            box::VectorExpression<double> p2 = m_->p()->p2();
            p2.z(box::Expression(p2.z().value() + thickness));
            m_->p()->p2(p2);
        }
    }
    // Initialise the fitness function
    fitness_->initialise(numFrequencies_, frequencySpacing_, frequencySpacing_);
    // We want the model to run long enough to fill the sensorId time series
    // including an allowance for the time the signal takes to reach the sensorId
    // behind the grid under test and some more for the possible phase shift the
    // grid may add.
    auto nSamples = static_cast<int>(std::floor(1.0 / frequencySpacing_ / m_->p()->calcDt()));
    if(!useExistingDomain_) {
        m_->p()->tSize(box::Expression(
                m_->p()->calcDt() * nSamples * 1.1 +
                m_->p()->size().z() / box::Constants::c_));
    }
}

// Evaluate the fitness of the current individual.  Returns true if
// the FDTD model should be run now.
bool gs::GeneticSearch::evaluateFitness(const std::shared_ptr<Individual>& ind) {
    // Record the array sensorId data and evaluate fitness
    bool result = false;
    ind->evaluateFitness(fitness_);
    // Record some field data if we are asked to
    if(collectFieldData_) {
        for(auto& s : m_->sensors()) {
            auto* twoDSlice = dynamic_cast<sensor::TwoDSlice*>(s.get());
            if(twoDSlice != nullptr &&
               twoDSlice->dataSource() != sensor::TwoDSlice::material) {
                // Make the data
                std::string dataName;
                switch(twoDSlice->dataSource()) {
                    case sensor::TwoDSlice::eMagnitude:
                        dataName = "EMagnitude";
                        break;
                    case sensor::TwoDSlice::hMagnitude:
                        dataName = "HMagnitude";
                        break;
                    case sensor::TwoDSlice::ex:
                        dataName = "Ex";
                        break;
                    case sensor::TwoDSlice::ey:
                        dataName = "Ey";
                        break;
                    case sensor::TwoDSlice::ez:
                        dataName = "Ez";
                        break;
                    case sensor::TwoDSlice::hx:
                        dataName = "Hx";
                        break;
                    case sensor::TwoDSlice::hy:
                        dataName = "Hy";
                        break;
                    case sensor::TwoDSlice::hz:
                        dataName = "Hz";
                        break;
                    case sensor::TwoDSlice::material:
                        // Should never get here
                        break;
                }
                auto data = dynamic_cast<TwoDFitnessData<double>*>(
                        ind->getFitnessData(this, GeneticSearch::fitnessDataField,
                                            dataName));
                data->setSize(twoDSlice->nSliceX(), twoDSlice->nSliceY());
                twoDSlice->collect();
                for(size_t y = 0; y < twoDSlice->nSliceY(); ++y) {
                    for(size_t x = 0; x < twoDSlice->nSliceX(); ++x) {
                        data->setData(x, y, twoDSlice->data()[twoDSlice->index(x, y)]);
                    }
                }
            }
        }
    }
    return result;
}

// Return the main array sensorId used by the genetic search
sensor::ArraySensor* gs::GeneticSearch::getGeneticSensor() {
    return dynamic_cast<sensor::ArraySensor*>(m_->sensors().find("GeneticSensor"));
}


// Test to see if any member of the population is fit enough
// to be regarded as a solution
bool gs::GeneticSearch::evaluateResult() {
    return population_->evaluateResult(unfitnessThreshold_);
}

// Return a layer shape identifier, creating identifiers if necessary
int gs::GeneticSearch::layerShapeId(size_t layer) {
    if(layer >= layerShapeIds_.size()) {
        for(size_t i = layerShapeIds_.size(); i <= layer; i++) {
            int id = m_->d()->allocShapeIdentifier();
            layerShapeIds_.push_back(id);
        }
    }
    return layerShapeIds_[layer];
}

// Return a layer material identifier, creating identifiers if necessary
int gs::GeneticSearch::layerMaterialId(size_t layer) {
    if(layer >= layerMaterialIds_.size()) {
        for(size_t i = layerMaterialIds_.size(); i <= layer; i++) {
            int id = m_->d()->allocMaterialIndex();
            layerMaterialIds_.push_back(id);
        }
    }
    return layerMaterialIds_[layer];
}

// Set the population to the single specified individual
void gs::GeneticSearch::setPopulation(const std::shared_ptr<Individual>& ind) {
    population_->clear();
    population_->addIndividual(ind);
}

// Write the configuration to the DOM object
void gs::GeneticSearch::writeConfig(xml::DomObject& root) const {
    // Base class first
    Sequencer::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("populationsize") << populationSize_;
    root << xml::Obj("selectionmode") << selectionMode_;
    root << xml::Obj("algorithm") << algorithm_;
    root << xml::Obj("crossovermode") << crossoverMode_;
    root << xml::Obj("tournamentsize") << tournamentSize_;
    root << xml::Obj("breedingpoolsize") << breedingPoolSize_;
    root << xml::Obj("keepparents") << keepParents_;
    root << xml::Obj("microga") << microGa_;
    root << xml::Obj("averagesensor") << averageSensor_;
    root << xml::Obj("xycomponents") << xyComponents_;
    root << xml::Obj("useexistingdomain") << useExistingDomain_;
    root << xml::Obj("ncellsxy") << nCellsXY_;
    root << xml::Obj("cellsizez") << cellSizeZ_;
    root << xml::Obj("sensordistance") << sensorDistance_;
    root << xml::Obj("frequencyrange") << frequencyRange_;
    root << xml::Obj("repeatcell") << repeatCell_;
    root << xml::Obj("layerspacing") << layerSpacing_;
    root << xml::Obj("crossoverprobability") << crossOverProbability_;
    root << xml::Obj("mutationprobability") << mutationProbability_;
    root << xml::Obj("unfitnessthreshold") << unfitnessThreshold_;
    root << xml::Obj("gridmaterialid") << gridMaterialId_;
    root << xml::Obj("layersymmetry") << layerSymmetry_;
    root << xml::Obj("numfrequencies") << numFrequencies_;
    root << xml::Obj("collectfielddata") << collectFieldData_;
    for(int layerShapeId : layerShapeIds_) {
        root << xml::Obj("gridshapeid") << layerShapeId;
    }
    root << xml::Obj("fitness") << *fitness_;
    root << geneFactory_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::GeneticSearch::readConfig(xml::DomObject& root) {
    // Base class first
    Sequencer::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("populationsize") >> populationSize_;
    root >> xml::Obj("ncellsxy") >> nCellsXY_;
    root >> xml::Obj("cellsizez") >> cellSizeZ_;
    root >> xml::Obj("sensordistance") >> sensorDistance_;
    root >> xml::Obj("frequencyrange") >> frequencyRange_;
    root >> xml::Obj("crossoverprobability") >> crossOverProbability_;
    root >> xml::Obj("mutationprobability") >> mutationProbability_;
    root >> xml::Obj("unfitnessthreshold") >> unfitnessThreshold_;
    root >> xml::Obj("gridmaterialid") >> gridMaterialId_;
    root >> xml::Obj("selectionmode") >> xml::Default(selectionMode_) >> selectionMode_;
    root >> xml::Obj("algorithm") >> xml::Default(algorithm_) >> algorithm_;
    root >> xml::Obj("crossovermode") >> xml::Default(crossoverMode_) >> crossoverMode_;
    root >> xml::Obj("tournamentsize") >> xml::Default(tournamentSize_) >> tournamentSize_;
    root >> xml::Obj("breedingpoolsize") >> xml::Default(breedingPoolSize_)
         >> breedingPoolSize_;
    root >> xml::Obj("keepparents") >> xml::Default(keepParents_) >> keepParents_;
    root >> xml::Obj("microga") >> xml::Default(microGa_) >> microGa_;
    root >> xml::Obj("averagesensor") >> xml::Default(averageSensor_) >> averageSensor_;
    root >> xml::Obj("xycomponents") >> xml::Default(xyComponents_) >> xyComponents_;
    root >> xml::Obj("useexistingdomain") >> xml::Default(false) >> useExistingDomain_;
    root >> xml::Obj("repeatcell") >> xml::Default(repeatCell_) >> repeatCell_;
    root >> xml::Obj("layerspacing") >> xml::Default(layerSpacing_) >> layerSpacing_;
    root >> xml::Obj("generationnumber") >> xml::Default(0) >> generationNumber_;
    root >> xml::Obj("numfrequencies") >> xml::Default(numFrequencies_) >> numFrequencies_;
    root >> xml::Obj("layersymmetry") >> xml::Default(layerSymmetry_) >> layerSymmetry_;
    root >> xml::Obj("collectfielddata") >> xml::Default(false) >> collectFieldData_;
    layerShapeIds_.clear();
    for(auto& p : root.obj("gridshapeid")) {
        int v;
        *p >> v;
        layerShapeIds_.push_back(v);
    }
    root >> xml::Obj("fitness") >> *fitness_;
    root >> geneFactory_;
    root >> xml::Close();
    frequencySpacing_ = frequencyRange_ / numFrequencies_;
    // For backwards compatibility and removal of both algorithm
    if(algorithm_ != algorithmFdtd && algorithm_ != algorithmPropMatrix) {
        algorithm_ = algorithmFdtd;
    }
    // And the population information and history
    generationNumber_ = 0;
    readPopulation();
    readHistoryIndex();
    populationLoaded_ = true;
}

// Write out the current population to the DOM object
void gs::GeneticSearch::writePopulation(xml::DomObject& root) {
    // Generation number
    root << xml::Obj("generationnumber") << generationNumber_;
    // The population members
    root << *population_;
}

// Read in the current population
void gs::GeneticSearch::readPopulation(xml::DomObject& root) {
    root >> xml::Obj("generationnumber") >> generationNumber_;
    // The population members
    root >> *population_;
}

// Returns true if the search configuration is valid and a population can
// be generated and run
bool gs::GeneticSearch::configurationValid() {
    return geneFactory_.configurationValid() && fitness_->configurationValid();
}

// Make sure there is an initial population
void gs::GeneticSearch::initialPopulation() {
    if(population_->individuals().empty()) {
        population_->generateInitialPopulation(populationSize_, this);
    }
}

// Adjust the current population to account for the addition of a gene template
// This function must be called AFTER the gene template has been added to the factory
void gs::GeneticSearch::geneTemplateAdded(GeneTemplate* inserted) {
    // For each individual...
    for(auto& ind : population_->individuals()) {
        // Get a gene iterator
        auto genePos = ind->chromosome()->genes().begin();
        // Run through the gene template...
        for(auto& t : geneFactory_.templates()) {
            // Is this the inserted template?
            if(t == inserted) {
                // Insert a gene here
                Gene* gene = t->create();
                ind->chromosome()->genes().insert(genePos, gene);
            } else {
                // No, next gene
                if(genePos != ind->chromosome()->genes().end()) {
                    genePos++;
                }
            }
        }
        ind->encodeGenes(this);
    }
}

// Adjust the current population to account for the removal of a gene template
// This function must be called BEFORE the gene template is removed from the factory.
void gs::GeneticSearch::geneTemplateRemoved(GeneTemplate* removed) {
    // For each individual...
    for(auto& ind : population_->individuals()) {
        // Get a gene iterator
        auto genePos = ind->chromosome()->genes().begin();
        // Run through the gene template...
        for(auto& t : geneFactory_.templates()) {
            // Is this the template being removed?
            if(t == removed) {
                // Remove the gene here
                if(genePos != ind->chromosome()->genes().end()) {
                    Gene* gene = *genePos;
                    genePos = ind->chromosome()->genes().erase(genePos);
                    delete gene;
                }
            } else {
                // No, next gene
                if(genePos != ind->chromosome()->genes().end()) {
                    genePos++;
                }
            }
        }
        ind->encodeGenes(this);
    }
}

// Write the current generation's population
bool gs::GeneticSearch::writePopulation() {
    bool result = false;
    try {
        // Make the DOM
        auto* root = new xml::DomObject("fdtdpopulation");
        xml::DomDocument dom(root);
        writePopulation(*root);
        xml::Writer writer;
        writer.writeFile(&dom, productFilename() + ".pop");
        result = true;
    } catch(xml::Exception& e) {
        std::cout << "Failed to write population XML file: " << e.what() << std::endl;
    }
    return result;
}

// Read the current generation's population
void gs::GeneticSearch::readPopulation() {
    // Open the file
    std::ifstream file;
    file.open(productFilename() + ".pop", std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Read the whole file
        std::stringstream buffer;
        buffer << file.rdbuf();
        // Parse into a DOM
        xml::Reader reader;
        xml::DomDocument dom("fdtdpopulation");
        try {
            reader.readString(&dom, buffer.str());
            // Now process the DOM
            readPopulation(*dom.getObject());
        } catch(xml::Exception& e) {
            std::cout << "Failed to read population XML file: " << e.what() << std::endl;
        }
        // Clean up
        file.close();
    }
}

// Write the current generation to the history
bool gs::GeneticSearch::writeHistory() {
    bool result = false;
    // Open the file
    std::ofstream file;
    file.open(productFilename() + ".history",
              std::ios::out | std::ios::binary | std::ios::app);
    if(file.is_open()) {
        result = true;
        file.seekp(0, std::ofstream::end);
        // Record the location in the index
        historyIndex_[generationNumber_] = file.tellp();
        // Make the DOM
        auto* root = new xml::DomObject("generation");
        xml::DomDocument dom(root);
        writePopulation(*root);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        file.write(text.data(), static_cast<std::streamsize>(text.size()));
        // Clean up
        file.close();
    }
    return result;
}

// Rebuild the history index by reading through the history file
void gs::GeneticSearch::rebuildHistoryIndex() {
    // Clear the history
    historyIndex_.clear();
    // Open the history file
    std::ifstream file;
    file.open(productFilename() + ".history", std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Read the whole file
        std::stringstream buffer;
        buffer << file.rdbuf();
        // Find the start and end positions of the first generation
        std::string::size_type startPos = 0;
        std::string::size_type endPos = 0;
        // While we can find the XML document start tag...
        bool going = !buffer.str().empty();
        while(going) {
            // The start and stop positions of this record
            startPos = endPos;
            endPos = buffer.str().find("<?xml", startPos + 1);
            // Fix the end position if this is the last record
            if(endPos == std::string::npos) {
                endPos = buffer.str().length();
                going = false;
            }
            // Parse the XML record
            std::string text = buffer.str().substr(startPos, endPos - startPos);
            xml::Reader reader;
            xml::DomDocument dom("generation");
            try {
                reader.readString(&dom, text);
                // Record in the index
                int generation;
                *dom.getObject() >> xml::Obj("generationnumber") >> generation;
                historyIndex_[generation] = static_cast<int64_t>(startPos);
            } catch(xml::Exception& e) {
                std::cout << "Failed to read history index XML file: " << e.what()
                          << std::endl;
            }
        }
        // Write out the history
        writeHistoryIndex();
    }
}

// Read the population in from the history
void gs::GeneticSearch::readHistory(int generationNumber) {
    // Find the generation in the index
    auto pos = historyIndex_.find(generationNumber);
    if(pos != historyIndex_.end()) {
        // Open the history file
        std::ifstream file;
        file.open(productFilename() + ".history", std::ios::in | std::ios::binary);
        if(file.is_open()) {
            // Seek to the location
            int64_t start = pos->second;
            file.seekg(start);
            pos++;
            // Read the necessary amount of data
            std::string text;
            if(pos == historyIndex_.end()) {
                // Read the rest of the file
                std::stringstream buffer;
                buffer << file.rdbuf();
                text = buffer.str();
            } else {
                // Read just the portion
                int64_t count = pos->second - start;
                text.resize(static_cast<size_t>(count));
                file.read(&text[0], count);
            }
            // Parse into a DOM
            xml::Reader reader;
            xml::DomDocument dom("generation");
            try {
                reader.readString(&dom, text);
                // Now process the DOM
                readPopulation(*dom.getObject());
            } catch(xml::Exception& e) {
                std::cout << "Failed to read history XML file: " << e.what() << std::endl;
            }
            // Clean up
            file.close();
        }
    } else {
        // Generation not in the index, just read the current generation
        readPopulation();
    }
}

// Write the history index file
bool gs::GeneticSearch::writeHistoryIndex() {
    bool result = false;
    // Open the index file
    std::ofstream file;
    file.open(productFilename() + ".index",
              std::ios::out | std::ios::binary | std::ios::trunc);
    if(file.is_open()) {
        result = true;
        // Generate the index DOM
        auto* root = new xml::DomObject("index");
        xml::DomDocument dom(root);
        for(auto item : historyIndex_) {
            *root << xml::Open("generation");
            *root << xml::Obj("number") << item.first;
            *root << xml::Obj("offset") << item.second;
            *root << xml::Close("generation");
        }
        // Write the DOM out
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        file.write(text.data(), static_cast<std::streamsize>(text.size()));
        // Clean up
        file.close();
    }
    return result;
}

// Read in the history index
void gs::GeneticSearch::readHistoryIndex() {
    // Open the index file
    std::ifstream file;
    file.open(productFilename() + ".index", std::ios::in | std::ios::binary);
    if(file.is_open()) {
        historyIndex_.clear();
        // Read the whole file
        std::stringstream buffer;
        buffer << file.rdbuf();
        // Parse into a DOM
        xml::Reader reader;
        xml::DomDocument dom("index");
        try {
            reader.readString(&dom, buffer.str());
            // Now process the DOM
            for(auto& o : dom.getObject()->obj("generation")) {
                int number;
                int64_t offset;
                *o >> xml::Obj("number") >> number;
                *o >> xml::Obj("offset") >> offset;
                if(number >= 0) {
                    historyIndex_[number] = offset;
                }
            }
        } catch(xml::Exception& e) {
            std::cout << "Failed to read history index XML file: " << e.what() << std::endl;
        }
        // Clean up
        file.close();
    }
}

// Start running the sequence
void gs::GeneticSearch::run() {
    // At this point, the configuration will have been read into
    // the model and any existing population loaded.
    // Make sure there is an initial population
    if(!populationLoaded_) {
        readPopulation();  // Make sure the genetic search has the current population
        populationLoaded_ = true;
    }
    initialPopulation();
    // And then arrange to run the model on the individuals
    if(!slave_) {
        runGeneration();
    }
}

// Stop running the sequence
void gs::GeneticSearch::stop() {
    if(!slave_) {
        jobs_.clear();
    }
}

// A job has completed
void gs::GeneticSearch::jobComplete(const std::shared_ptr<fdtd::Job>& job) {
    // Is this a job we are waiting for?
    auto pos = jobs_.find(job);
    if(pos != jobs_.end()) {
        jobs_.erase(pos);
        if(!slave_) {
            writePopulation();
        }
        doNotification(notifyIndividualComplete);
        // Is the generation complete?
        if(jobs_.empty()) {
            // All individuals in the generation have been run
            if(!slave_) {
                writeHistory();
                writeHistoryIndex();
            }
            doNotification(notifyGenerationComplete);
            if(!slave_) {
                runGeneration();
            }
        }
    }
}

// Run the next generation of the genetic algorithm
void gs::GeneticSearch::runGeneration() {
    // Create jobs for each individual that needs modelling
    std::list<std::shared_ptr<Individual>> inds;
    getWaitingIndividuals(inds);
    if(inds.empty()) {
        // We must need a new generation
        if(evaluateResult()) {
            // We have a result that is near enough
            // There's nothing further to do
        } else {
            // Another generation
            breedNewGeneration();
            getWaitingIndividuals(inds);
        }
    }
    // Create jobs for the waiting individuals
    for(auto& ind : inds) {
        auto job = m_->processingMgr()->addIndividualJob(this, ind);
        jobs_.insert(job);
    }
}

// The sequencer name is being changed
void gs::GeneticSearch::name(const std::string& v) {
    // Get the original product file name
    std::string oldFileRoot = productFilename();
    // Call base class to actually change the name
    Sequencer::name(v);
    // Do we need to change the name of any product files?
    std::string newFileRoot = productFilename();
    if(oldFileRoot != newFileRoot) {
        renameFiles(oldFileRoot, newFileRoot, {".pop", ".history", ".index"});
    }
}

// Make a fitness data object of the specified type
gs::FitnessData* gs::GeneticSearch::makeFitnessData(int mode, const std::string& name) {
    return fitnessDataFactory_.make(mode, name);
}

// Make a fitness data object from the XML object
gs::FitnessData* gs::GeneticSearch::makeFitnessData(xml::DomObject& o) {
    return fitnessDataFactory_.restore(o, "");
}

// Clear the sequencer and discard all its data
void gs::GeneticSearch::clearAndDiscard() {
    delete population_;
    population_ = new Population(populationSize_, this);
    generationNumber_ = 0;
    unfitnessProgress_.clear();
    ::remove((productFilename() + ".pop").c_str());
    ::remove((productFilename() + ".history").c_str());
    ::remove((productFilename() + ".index").c_str());
    m_->clearSeqGenerated();
}

// And indication that an FDTD model step has completed
void gs::GeneticSearch::stepComplete() {
    if(fitness_ != nullptr) {
        fitness_->stepComplete();
    }
}

