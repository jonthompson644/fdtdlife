/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_INDIVIDUAL_H
#define FDTDLIFE_INDIVIDUAL_H

#include <list>
#include <random>
#include <memory>
#include "Chromosome.h"
#include "FitnessPartResult.h"
#include <Fdtd/NotificationData.h>
#include "FitnessData.h"
#include <Xml/DomObject.h>
namespace sensor {class ArraySensor;}
namespace sensor {class TwoDSlice;}
namespace fdtd {class Model;}

namespace gs {

    class Random;

    class FitnessFunction;

    class GeneticSearch;

    // Represents an instance within the current population of
    // a genetic algorithm run
    class Individual : public fdtd::NotificationData {
    public:
        enum State {
            stateWaiting = 0, stateRunning, stateEvaluated
        };
    public:
        Individual(int id, GeneticSearch* s);
        Individual(int id, const std::vector<double>& attenuation,
                   const std::vector<double>& phaseShift, bool xyComponents,
                   const std::vector<double>& attenuationY,
                   const std::vector<double>& phaseShiftY, GeneticSearch* s);
        Individual(int id, Random* random, size_t numBits, GeneticSearch* s);
        Individual(int id, double unfitness, double fitness,
                   int mother, int father, int crossPoint,
                   int numMutations, const char* chromosome, State state, GeneticSearch* s);
        Individual(int id, Chromosome* c, GeneticSearch* s);
        ~Individual();
    public:
        static std::shared_ptr<Individual> breedSingleCrossover(int id, std::shared_ptr<Individual> mother,
                                                std::shared_ptr<Individual> father,
                                                size_t crossPoint);
        static std::shared_ptr<Individual> breedUniformCrossover(int id, std::shared_ptr<Individual> mother,
                                                 std::shared_ptr<Individual> father, Random* random);
        Chromosome* chromosome() { return chromosome_; }
        double fitness() const { return fitness_; }
        void fitness(double v) { fitness_ = v; }
        double unfitness() const { return unfitness_; }
        void unfitness(double v) { unfitness_ = v; }
        size_t crossPoint() const { return crossPoint_; }
        int mother() const { return mother_; }
        int father() const { return father_; }
        size_t numMutations() const { return numMutations_; }
        State state() const { return state_; }
        void state(State v) { state_ = v; }
        bool isEvaluated() const { return state_ == stateEvaluated; }
        int id() const { return id_; }
        int mutate(double probability, Random* random);
        double configureModel(GeneticSearch* s, int what);
        void evaluateFitness(FitnessFunction* fitnessFunction);
        void setRunning();
        const std::vector<double>& attenuation() const { return attenuation_; }
        const std::vector<double>& attenuationY() const { return attenuationY_; }
        const std::vector<double>& phaseShift() const { return phaseShift_; }
        const std::vector<double>& phaseShiftY() const { return phaseShiftY_; }
        const std::vector<double>& timeSeries() const { return timeSeries_; }
        const std::vector<double>& reference() const { return reference_; }
        //void attenuation(const std::vector<double>& d) { attenuation_ = d; }
        //void phaseShift(const std::vector<double>& d) { phaseShift_ = d; }
        bool decodeGenes(GeneticSearch* m);
        void encodeGenes(GeneticSearch* m);
        void copyEvaluation(const std::shared_ptr<Individual>& other);
        size_t distanceBetween(const std::shared_ptr<Individual>& other);
        bool operator<(const Individual& other) const;
        void writeIndividual(xml::DomObject& root) const;
        void readIndividual(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Individual& i) {i.writeIndividual(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Individual& i) {i.readIndividual(o); return o;}
        double getRepeatCell(double defaultValue);
        int getLayerSymmetry(int defaultValue);
        static std::shared_ptr<Individual> factory(xml::DomObject& o, GeneticSearch* s);
        std::list<FitnessPartResult>& fitnessResults() { return fitnessResults_; }
        void getTransmittance(std::vector<double>& data) const;
        void getTransmittanceY(std::vector<double>& data) const;
        bool xyComponents() const {return xyComponents_;}
        FitnessData* getFitnessData(GeneticSearch* s, int mode, const std::string& name);
        const std::list<std::unique_ptr<FitnessData>>& fitnessData() const {return fitnessData_;}
    protected:
        int id_;
        Chromosome* chromosome_;
        std::vector<double> attenuation_;
        std::vector<double> phaseShift_;
        std::vector<double> attenuationY_;
        std::vector<double> phaseShiftY_;
        std::vector<double> timeSeries_;
        std::vector<double> reference_;
        std::list<std::unique_ptr<FitnessData>> fitnessData_;
        bool xyComponents_;
        double unfitness_;
        double fitness_;
        int mother_;
        int father_;
        size_t crossPoint_;
        size_t numMutations_;
        State state_;
        std::list<FitnessPartResult> fitnessResults_;
        GeneticSearch* s_;
    protected:
        void construct(int id);
    };

}

#endif //FDTDLIFE_INDIVIDUAL_H
