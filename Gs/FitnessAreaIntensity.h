/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSAREAINTENSITY_H
#define FDTDLIFE_FITNESSAREAINTENSITY_H

#include "FitnessPlaneWaveBase.h"
#include <Box/Vector.h>
#include "Box/Constants.h"
namespace sensor {class AreaSensor;}

namespace gs {

    // A fitness function for attenuation
    class FitnessAreaIntensity : public FitnessPlaneWaveBase {
    public:
        // Overrides of FitnessBase
        double unfitness(Individual* ind) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void initialise(size_t numFrequencies, double frequencyStart,
                        double frequencySpacing) override;

        // Getters
        double frequency() const {return frequency_;}
        const box::Vector<double>& center() const {return center_;}
        double width() const {return width_;}
        double height() const {return height_;}
        box::Constants::Orientation orientation() const {return orientation_;}

        // Setters
        void frequency(double frequency) {frequency_ = frequency;}
        void center(const box::Vector<double>& v) {center_ = v;}
        void width(double v) {width_ = v;}
        void height(double v) {height_ = v;}
        void orientation(box::Constants::Orientation v) {orientation_ = v;}

    protected:
        // Configuration
        double frequency_ {};  // The frequency at which to maximise the signal
        box::Vector<double> center_;  // The center of the sensorId area
        double width_ {};  // The width of the sensorId
        double height_ {};  // The height of the sensorId
        box::Constants::Orientation orientation_ {box::Constants::orientationZPlane};  // The orientation of the sensorId

        // Helpers
        sensor::AreaSensor* getSensor();
    };

}


#endif //FDTDLIFE_FITNESSAREAINTENSITY_H
