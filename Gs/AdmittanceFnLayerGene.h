/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCEFNLAYERGENE_H
#define FDTDLIFE_ADMITTANCEFNLAYERGENE_H

#include "UnitCellLayerGene.h"
#include <Box/Polynomial.h>
#include "GeneBitsSpecDouble.h"
#include "GeneBitsSpecLogDouble.h"

namespace gs {

    class FitnessAdmittance;

    // A layer gene that evolves an admittance function
    // Only useful with the propagation matrix method.
    class AdmittanceFnLayerGene : public UnitCellLayerGene {
    protected:
        // Evolving parts
        box::Polynomial real_;
        box::Polynomial imag_;
        // Template parts
        GeneBitsSpecDouble realA_;
        GeneBitsSpecLogDouble realB_;
        GeneBitsSpecLogDouble realC_;
        GeneBitsSpecLogDouble realD_;
        GeneBitsSpecLogDouble realE_;
        GeneBitsSpecLogDouble realF_;
        GeneBitsSpecDouble imagA_;
        GeneBitsSpecLogDouble imagB_;
        GeneBitsSpecLogDouble imagC_;
        GeneBitsSpecLogDouble imagD_;
        GeneBitsSpecLogDouble imagE_;
        GeneBitsSpecLogDouble imagF_;
    public:
        AdmittanceFnLayerGene(const GeneBitsSpecDouble& realA,
                              const GeneBitsSpecLogDouble& realB,
                              const GeneBitsSpecLogDouble& realC,
                              const GeneBitsSpecLogDouble& realD,
                              const GeneBitsSpecLogDouble& realE,
                              const GeneBitsSpecLogDouble& realF,
                              const GeneBitsSpecDouble& imagA,
                              const GeneBitsSpecLogDouble& imagB,
                              const GeneBitsSpecLogDouble& imagC,
                              const GeneBitsSpecLogDouble& imagD,
                              const GeneBitsSpecLogDouble& imagE,
                              const GeneBitsSpecLogDouble& imagF);
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        size_t sizeBits() const override;
        void makeLayer(GeneticSearch* s, unsigned int what, int layer,
                       double zLocation) override;
        FitnessAdmittance* makeAdmittanceFunction(GeneticSearch* s);
        // Getters
        const box::Polynomial& real() const { return real_; }
        const box::Polynomial& imag() const { return imag_; }
        // Setters
        void real(const box::Polynomial& v) { real_ = v; }
        void imag(const box::Polynomial& v) { imag_ = v; }
    };

}


#endif //FDTDLIFE_ADMITTANCEFNLAYERGENE_H
