/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LAYERGENE_H
#define FDTDLIFE_LAYERGENE_H

#include "Gene.h"

namespace domain { class Material; }

namespace gs {

    // Base class for all genes that represent layers
    class LayerGene : public Gene {
    protected:
        int layer_{};  // The layer number
        int symmetryLayer_{};  // The layer number of the symmetry layer
        double zLocation_{};  // Z start location of the layer
        double symmetryZLocation_{};  // Z start location of the symmetry layer
        bool hasSymmetryLayer_{};  // Indicates if the symmetry layer is to be used
        double repeatCell_{};  // The repeat cell dimension
        double layerSpacing_{};  // The spacing between layers
        bool present_{};  // The layer is present
        int layerSymmetry_{};  // The layer symmetry mode to use
    public:
        explicit LayerGene(GeneTemplate::GeneType geneType);
        bool connect(Chromosome* c, GeneticSearch* s) override;
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        domain::Material* material(GeneticSearch* s);
        bool present() const { return present_; }
        size_t sizeBits() const override;
        virtual double thickness() const { return layerSpacing_; }
        void symmetryConnect(Chromosome* c, GeneticSearch* s);
        virtual void makeLayer(GeneticSearch* /*s*/, unsigned int /*what*/, int /*layer*/,
                               double /*zLocation*/) {}
    };
}


#endif //FDTDLIFE_LAYERGENE_H
