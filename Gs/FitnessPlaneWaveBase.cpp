/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessPlaneWaveBase.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Source/PlaneWaveSource.h>

// Initialise the model ready for the assessment of an individual
void gs::FitnessPlaneWaveBase::initialise(size_t numFrequencies, double frequencyStart,
                                             double frequencySpacing) {
    // Base class first
    FitnessBase::initialise(numFrequencies, frequencyStart, frequencySpacing);
    // Get the plane wave source
    source::PlaneWaveSource* source = getSource();
    // Configure the source
    source->azimuth(box::Expression(0.0));
    source->initialTime(box::Expression(0.0));
    source->frequencies().value({s_->frequencySpacing(),
                                 s_->frequencySpacing(),
                         (size_t)s_->numFrequencies()});
    source->amplitude(box::Expression(1.0));
    if(s_->xyComponents()) {
        source->polarisation(box::Expression(45.0));
    } else {
        source->polarisation(box::Expression(0.0));
    }
    source->pmlSigma(1.0);
    source->continuous(true);
    source->time(box::Expression(0.0));
    source->pmlSigmaFactor(1.04);
    // We want the source to run from halfway through the first cycle of the lowest frequency
    auto nSamples = (int) std::floor(1.0 / s_->frequencySpacing() / s_->m()->p()->calcDt());
    source->initialTime(box::Expression(s_->m()->p()->calcDt() * nSamples * 0.5));
}

// Get or create the source for this fitness function
source::PlaneWaveSource* gs::FitnessPlaneWaveBase::getSource() {
    const std::string name = "GeneticSource";
    auto* result = dynamic_cast<source::PlaneWaveSource*>(s_->m()->sources().find(name));
    if(result == nullptr) {
        result = dynamic_cast<source::PlaneWaveSource*>(
                s_->m()->sources().factory()->make(
                        source::SourceFactory::modePlaneWave, s_->m()));
        result->name(name);
    }
    return result;
}


