/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYPLATENXNGENE_H
#define FDTDLIFE_BINARYPLATENXNGENE_H

#include <cstdint>
#include <vector>
#include "UnitCellLayerGene.h"

namespace gs {

    // A gene that codes binary plates
    class BinaryPlateNxNGene : public UnitCellLayerGene {
    protected:
        // Evolving parts
        std::vector<uint64_t> pattern_;   // The plate pattern
        // Template parts
        bool fourFoldSymmetry_;  // Controls the symmetry
        int bitsPerHalfSide_;  // Controls the size of the pattern
        // Others
        std::vector<uint64_t> bitLocation_;
        size_t patternSize_;  // Number of bits used in the gene
        size_t patternSize2Fold_;  // Number of bits in the two fold pattern
        size_t patternSize4Fold_;  // Number of bits in the four fold pattern
        std::vector<uint64_t> pattern2Fold_;  // The two fold pattern used by the model
        static const size_t bitsPerWord_ = 64;
    public:
        explicit BinaryPlateNxNGene(int bitsPerHalfSide, bool fourFoldSymmetry,
                                    size_t repeatBits, int repeatMin);
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        size_t sizeBits() const override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        void makeLayer(GeneticSearch* s, unsigned int what, int layer,
                       double zLocation) override;
        // Getters
        const std::vector<uint64_t>& pattern() const { return pattern_; }
        const std::vector<uint64_t>& pattern2Fold() const { return pattern2Fold_; }
        bool fourFoldSymmetry() const { return fourFoldSymmetry_; }
        int bitsPerHalfSide() const { return bitsPerHalfSide_; }
        // Setters
        void pattern(const std::vector<uint64_t>& v) { pattern_ = v; }
        void pattern2Fold(const std::vector<uint64_t>& v);
    protected:
        void makePattern();
        void initialise();
    };
}


#endif //FDTDLIFE_BINARYPLATENXNGENE_H
