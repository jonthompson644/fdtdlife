/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricBlockGene.h"
#include <Domain/Material.h>
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include <Domain/Cuboid.h>
#include <Box/Constants.h>
#include <sstream>

// Constructor
gs::DielectricBlockGene::DielectricBlockGene(const box::Vector<double>& center,
                                             const box::Vector<double>& size,
                                             const GeneBitsSpecDouble& permittivitySpec) :
        Gene(GeneTemplate::geneTypeDielectricBlock),
        center_(center),
        size_(size),
        permittivitySpec_(permittivitySpec),
        permittivity_(0.0) {
}

// Encode the gene into the chromosome
void gs::DielectricBlockGene::encode(Chromosome* c) {
    permittivitySpec_.encode(c, permittivity_);
}

// Decode the gene from the chromosome
void gs::DielectricBlockGene::decode(Chromosome* c) {
    permittivity_ = permittivitySpec_.decode(c);
}

// Configure the model
double gs::DielectricBlockGene::configureModel(GeneticSearch* s, unsigned int /*what*/) {
    // The dielectric material
    std::stringstream name;
    name << "Dielectric " << center_;
    auto* mat = s->m()->d()->makeMaterial(domain::Domain::materialModeNormal).get();
    mat->name(name.str());
    mat->priority(0);
    mat->sigma(box::Expression(box::Constants::sigma0_));
    mat->sigmastar(box::Expression(box::Constants::sigmastar0_));
    mat->color(box::Constants::colourYellow);
    mat->mu(box::Expression(box::Constants::mu0_));
    mat->epsilon(box::Expression(permittivity_ * box::Constants::epsilon0_));
    mat->seqGenerated(true);
    // The main block
    makeBlock(s, center_, mat->index());
    // The symmetric block (around the z axis in the y plane) but not in the middle
    if(center_.x() < -size_.x() / 4 || center_.x() > size_.x() / 4) {
        makeBlock(s, center_ * box::Vector<double>(-1, 1, 1), mat->index());
    }
    return 0.0;
}

// Make a dielectric block around the specified center
void gs::DielectricBlockGene::makeBlock(GeneticSearch* s, const box::Vector<double>& center,
                                        int mat) {
    // Dimensions
    box::Vector<double> topLeft = center - size_ / 2.0;
    box::Vector<double> bottomRight = center + size_ / 2.0;
    // Name
    std::stringstream name;
    name << "Block " << center;
    // Plant the block
    auto* shape = dynamic_cast<domain::Cuboid*>(s->m()->d()->makeShape(
            domain::Domain::shapeModeCuboid));
    shape->name(name.str());
    shape->material(mat);
    shape->p1(topLeft);
    shape->p2(bottomRight);
    shape->seqGenerated(true);
}

// Make connections between genes
bool gs::DielectricBlockGene::connect(Chromosome* c, GeneticSearch* s) {
    return Gene::connect(c, s);
}

// Copy the state from the other gene into this one
void gs::DielectricBlockGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<DielectricBlockGene*>(other);
    if(from != nullptr) {
        permittivity_ = from->permittivity_;
    }
}

// Return the number of bits taken by the gene
size_t gs::DielectricBlockGene::sizeBits() const {
    return Gene::sizeBits() + permittivitySpec_.bits();
}
