/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LENSDIMENSIONSGENE_H
#define FDTDLIFE_LENSDIMENSIONSGENE_H

#include "Gene.h"
#include "GeneBitsSpecInteger.h"

namespace gs {

    // A gene that defines the dimensions of the lens structure
    class LensDimensionsGene : public Gene {
    protected:
        // Evolving parts
        double unitCell_ {0.0001};
        size_t numRows_ {1};
        double layerSpacing_ {0.0001};
        // Template parts
        GeneBitsSpecDouble unitCellSpec_;
        GeneBitsSpecDouble layerSpacingSpec_;
        GeneBitsSpecInteger<size_t> numRowsSpec_;
        // Other members
        std::vector<std::vector<int>> shapes_;  // [rows][columns]
        int metal_ {-1};
        size_t numColumns_ {};
    public:
        LensDimensionsGene(const GeneBitsSpecDouble& unitCellSpec,
                              const GeneBitsSpecDouble& layerSpacingSpec,
                              const GeneBitsSpecInteger<size_t>& numRowsSpec);
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        size_t sizeBits() const override;
        // Getters
        size_t numRows() const { return numRows_; }
        double unitCell() const { return unitCell_; }
        double layerSpacing() const { return layerSpacing_; }
        const std::vector<std::vector<int>>& shapes() const {return shapes_;}
        size_t numColumns() const {return numColumns_;}
        // Setters
        void numRows(size_t v) { numRows_ = v; }
        void unitCell(double v) { unitCell_ = v; }
        void layerSpacing(double v) { layerSpacing_ = v; }
    };

}


#endif //FDTDLIFE_LENSDIMENSIONSGENE_H
