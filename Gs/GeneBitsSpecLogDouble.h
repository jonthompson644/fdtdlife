/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEBITSSPECLOGDOUBLE_H
#define FDTDLIFE_GENEBITSSPECLOGDOUBLE_H


#include <cstddef>
#include <cstdint>
#include "GeneBitsSpecDouble.h"
namespace xml {class DomObject;}

namespace gs {

    // A class the represents the gene bits specification for a double
    // with a min and a max range encoded logarithmically.
    class GeneBitsSpecLogDouble : public GeneBitsSpecDouble {
    public:
        // Construction
        GeneBitsSpecLogDouble() = default;
        GeneBitsSpecLogDouble(size_t bits, double min, double max);
        // API
        void encode(Chromosome* c, double value) override;
        double decode(Chromosome* c) override;
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneBitsSpecLogDouble& v) {v.writeXml(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneBitsSpecLogDouble& v) {v.readXml(o); return o;}
    protected:
        // Helpers
        [[nodiscard]] double scaling() const override;
    };

}



#endif //FDTDLIFE_GENEBITSSPECLOGDOUBLE_H
