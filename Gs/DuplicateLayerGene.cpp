/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DuplicateLayerGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <sstream>

// Constructor
gs::DuplicateLayerGene::DuplicateLayerGene() :
        LayerGene(GeneTemplate::geneTypeDuplicateLayer),
        duplicateThisLayer_(nullptr) {
}

// Encode the allele into the chromosome
void gs::DuplicateLayerGene::encode(Chromosome* c) {
    LayerGene::encode(c);
}

// Decode the allele from the chromosome
void gs::DuplicateLayerGene::decode(Chromosome* c) {
    LayerGene::decode(c);
}

// Connect the gene to other genes it depends on
bool gs::DuplicateLayerGene::connect(Chromosome* c, GeneticSearch* s) {
    bool result = LayerGene::connect(c, s);
    // Find the last layer gene before me for the spacing
    duplicateThisLayer_ = nullptr;
    for(auto g : c->genes()) {
        if(g == this) {
            break;
        } else {
            auto gene = dynamic_cast<LayerGene*>(g);
            if(gene != nullptr) {
                duplicateThisLayer_ = gene;
            }
        }
    }
    return result;
}

// Set the model configuration corresponding to this allele
double gs::DuplicateLayerGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(duplicateThisLayer_ != nullptr && present_ &&
       (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_;
        duplicateThisLayer_->makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_;
            duplicateThisLayer_->makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Return the number of bits used by the gene
size_t gs::DuplicateLayerGene::sizeBits() const {
    return LayerGene::sizeBits();
}

// Copy the state from the other gene into this one
void gs::DuplicateLayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    LayerGene::copyFrom(other, m);
}

// Make a layer using the duplicate
void gs::DuplicateLayerGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
                                       double zLocation) {
    if(duplicateThisLayer_ != nullptr) {
        duplicateThisLayer_->makeLayer(s, what, layer, zLocation);
    }
}
