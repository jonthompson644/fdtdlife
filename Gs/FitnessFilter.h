/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSFILTER_H
#define FDTDLIFE_FITNESSFILTER_H

#include "FitnessTransmittance.h"

namespace gs {

    // A fitness function that matches transmittance to a standard filter response
    class FitnessFilter : public FitnessTransmittance {
    public:
        // Types
        enum class FilterType {
            butterworth, chebyshevI, chebyshevII, elliptical, bessel
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, FilterType& v) {
            int t;
            o >> t;
            v = static_cast<FilterType>(t);
            return o;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, FilterType v) {
            o << static_cast<int>(v);
            return o;
        }
        enum class Property {
            transmittance, phaseShift
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, Property& v) {
            int t;
            o >> t;
            v = static_cast<Property>(t);
            return o;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, Property v) {
            o << static_cast<int>(v);
            return o;
        }
        enum class FilterMode {
            lowPass, highPass
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, FilterMode& v) {
            int t;
            o >> t;
            v = static_cast<FilterMode>(t);
            return o;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, FilterMode v) {
            o << static_cast<int>(v);
            return o;
        }

        // Overrides of FitnessAdmittanceBase
        void initialise(size_t numSamples, double firstSample, double sampleSpacing) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        std::string makeTsvData(size_t numFrequencies, double firstFrequency,
                                double frequencySpacing) override;

        // Getters
        FilterType filterType() const { return filterType_; }
        FilterMode filterMode() const { return filterMode_; }
        Property property() const { return property_; }
        size_t order() const { return order_; }
        double cutOffFrequency() const { return cutOffFrequency_; }
        double maxFrequency() const { return maxFrequency_; }
        double passbandRipple() const { return passbandRipple_; }

        // Setters
        void filterType(FilterType v) { filterType_ = v; }
        void filterMode(FilterMode v) { filterMode_ = v; }
        void property(Property v) { property_ = v; }
        void order(size_t v) { order_ = v; }
        void cutOffFrequency(double v) { cutOffFrequency_ = v; }
        void maxFrequency(double v) { maxFrequency_ = v; }
        void passbandRipple(double v) { passbandRipple_ = v; }

    protected:
        // Configuration
        FilterType filterType_{FilterType::butterworth};  // Which filter type
        Property property_{Property::transmittance};  // Which property to test against
        FilterMode filterMode_{FilterMode::lowPass}; // Which mode of filter operation
        size_t order_{1};  // The order of the filter
        double cutOffFrequency_{10e9};  // The (3dB) cut off frequency
        double maxFrequency_{20e9};  // The maximum frequency to test for
        double passbandRipple_{1.0};  // Passband ripple in dB for chebyshev filters
    };
}


#endif //FDTDLIFE_FITNESSFILTER_H
