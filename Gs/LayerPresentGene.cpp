/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LayerPresentGene.h"
#include "Chromosome.h"
#include "GeneticSearch.h"

// Constructor
gs::LayerPresentGene::LayerPresentGene() :
        Gene(GeneTemplate::geneTypeLayerPresent) {
}

// Connect the gene to other genes it depends on
// and work out which layer this is
bool gs::LayerPresentGene::connect(Chromosome* /*c*/, GeneticSearch* /*s*/) {
    return true;
}

// Encode the gene into the chromosome
void gs::LayerPresentGene::encode(Chromosome* c) {
    Gene::encode(c);
    c->encode((uint64_t)present_, presentBits_);
}

// Decode the gene from the chromosome
void gs::LayerPresentGene::decode(Chromosome* c) {
    Gene::decode(c);
    present_ = (bool)c->decode(presentBits_);
}

// Return the number of bits used by the gene
size_t gs::LayerPresentGene::sizeBits() const {
    return Gene::sizeBits() + presentBits_;
}

// Copy the state from the other gene into this one
void gs::LayerPresentGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<LayerPresentGene*>(other);
    if(from != nullptr) {
        present_ = from->present_;
    }
}
