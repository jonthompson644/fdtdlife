/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LongLensSquareGene.h"
#include "GeneticSearch.h"
#include "LensDimensionsGene.h"
#include <Fdtd/Model.h>
#include <Domain/SquarePlate.h>
#include <Domain/Domain.h>

// Constructor
gs::LongLensSquareGene::LongLensSquareGene(const GeneBitsSpecDouble& a,
                                               const GeneBitsSpecLogDouble& b,
                                               const GeneBitsSpecLogDouble& c,
                                               const GeneBitsSpecLogDouble& d,
                                               const GeneBitsSpecLogDouble& e,
                                               const GeneBitsSpecLogDouble& f) :
        LensSquareGene(GeneTemplate::geneTypeLongLensSquare, a, b, c, d, e, f) {
}

// Set the model configuration corresponding to this allele
double gs::LongLensSquareGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if((what & GeneticSearch::individualLoadShapes) != 0) {
        if(lensDimensions_) {
            // for each row...
            for(size_t row=0; row<lensDimensions_->numRows(); ++row) {
                // for each col...
                for(size_t col=0; col<lensDimensions_->numColumns(); ++col) {
                    // Scale the plate size by the polynomial value
                    // Note the use of abs to enforce symmetry around the center
                    double cz = (lensDimensions_->numRows() / 2.0 - 0.5);
                    double dz = std::abs(row - cz);
                    double scale = 1.0 - squareSize_.value(dz);
                    if(scale < 0) {
                        scale = 0.0;
                    }
                    if(scale > 1.0) {
                        scale = 1.0;
                    }
                    auto shape = dynamic_cast<domain::SquarePlate*>(s->m()->d()->getShape(
                            lensDimensions_->shapes()[row][col]));
                    if(shape != nullptr) {
                        shape->plateSize(box::Expression(scale * shape->plateSize().value()));
                    }
                }
            }
        }
    }
    return result;
}

// Copy the state from the other gene into this one
void gs::LongLensSquareGene::copyFrom(Gene* other, fdtd::Model* m) {
    LensSquareGene::copyFrom(other, m);
    auto* from = dynamic_cast<LongLensSquareGene*>(other);
    if(from != nullptr) {
        squareSize_ = from->squareSize_;
    }
}

