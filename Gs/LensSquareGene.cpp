/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LensSquareGene.h"
#include "LensDimensionsGene.h"
#include "GeneticSearch.h"
#include "Chromosome.h"

// Constructor
gs::LensSquareGene::LensSquareGene(GeneTemplate::GeneType geneType,
                                     const GeneBitsSpecDouble& a,
                                     const GeneBitsSpecLogDouble& b,
                                     const GeneBitsSpecLogDouble& c,
                                     const GeneBitsSpecLogDouble& d,
                                     const GeneBitsSpecLogDouble& e,
                                     const GeneBitsSpecLogDouble& f) :
        Gene(geneType),
        a_(a),
        b_(b),
        c_(c),
        d_(d),
        e_(e),
        f_(f) {
}

// Encode the gene into the chromosome
void gs::LensSquareGene::encode(Chromosome* c) {
    Gene::encode(c);
    a_.encode(c, squareSize_.a());
    b_.encode(c, squareSize_.b());
    c_.encode(c, squareSize_.c());
    d_.encode(c, squareSize_.d());
    e_.encode(c, squareSize_.e());
    f_.encode(c, squareSize_.f());
}

// Decode the gene from the chromosome
void gs::LensSquareGene::decode(Chromosome* c) {
    Gene::decode(c);
    squareSize_.a(a_.decode(c));
    squareSize_.b(b_.decode(c));
    squareSize_.c(c_.decode(c));
    squareSize_.d(d_.decode(c));
    squareSize_.e(e_.decode(c));
    squareSize_.f(f_.decode(c));
}

// Return the lens dimensions gene before this one
// Connect the gene to other genes it depends on
// and work out which layer this is
bool gs::LensSquareGene::connect(Chromosome* c, GeneticSearch* /*s*/) {
    // Find the last lens dimensions gene before this gene
    lensDimensions_ = nullptr;
    for(auto& g : c->genes()) {
        auto gene = dynamic_cast<LensDimensionsGene*>(g);
        if(gene != nullptr) {
            lensDimensions_ = gene;
        }
        if(g == this) {
            break;
        }
    }
    return true;
}

// Return the size of the gene
size_t gs::LensSquareGene::sizeBits() const {
    return Gene::sizeBits() + a_.bits() + b_.bits() + c_.bits() + d_.bits() + e_.bits() + f_.bits();
}
