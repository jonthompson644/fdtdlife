/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETICSEARCH_H
#define FDTDLIFE_GENETICSEARCH_H

#include <cstddef>
#include <vector>
#include <list>
#include <complex>
#include <map>
#include <set>
#include <memory>
#include <Box/Vector.h>
#include "GeneFactory.h"
#include "GeneTemplate.h"
#include <Fdtd/PlateAdmittance.h>
#include "FitnessData.h"
#include <Seq/Sequencer.h>
#include <Box/Factory.h>

namespace xml { class DomObject; }
namespace sensor { class ArraySensor; }
namespace fdtd { class Model; }

namespace gs {

    class Population;

    class FitnessFunction;

    class Chromosome;

    class Individual;

    // The top level class for the optimisation algorithm
    class GeneticSearch : public seq::Sequencer {
    public:
        // Constants
        enum {
            notifyIndividualComplete = seq::Sequencer::notifyNextAvailableCode,
            notifyIndividualLoaded, notifyGenerationComplete
        };
        enum Selection {
            selectionFitnessProportional, selectionTournament
        };
        enum Algorithm {
            algorithmFdtd, algorithmPropMatrix
        };
        enum Crossover {
            crossoverSingle, crossoverUniform
        };
        enum IndividualLoad {
            individualLoadAll = 0xffff, individualLoadShapes = 0x0001,
            individualLoadGeneral = 0x0002, individualLoadSource = 0x0004,
            individualLoadDomainSize = 0x0008, individualLoadSensor = 0x0010
        };
        enum LayerSymmetry {
            layerSymmetryNone, layerSymmetryEven, layerSymmetryOdd
        };
        enum {
            fitnessDataTransmittance, fitnessDataAdmittance, fitnessDataPhaseShift,
            fitnessDataAmplitudeDist, fitnessDataPhaseDist, fitnessDataTransmittanceDist,
            fitnessDataField, fitnessDataTimeSeries
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, Selection& v) {
            o >> (int&) v;
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Algorithm& v) {
            o >> (int&) v;
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Crossover& v) {
            o >> (int&) v;
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, LayerSymmetry& v) {
            o >> (int&) v;
            return o;
        }
        static constexpr int gridToTopCells_ = 10;
        static constexpr int sensorToBottomCells_ = 4;
        static constexpr size_t defaultPopulationSize_ = 5;
        // Construction
        GeneticSearch();
        ~GeneticSearch() override;
        // API
        void run() override;
        void stop() override;
        void jobComplete(const std::shared_ptr<fdtd::Job>& job) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void clearAndDiscard() override;
        void writePopulation(xml::DomObject& root);
        void readPopulation(xml::DomObject& root);
        bool configurationValid();
        void initialPopulation();
        void resetPopulation();
        void geneTemplateAdded(GeneTemplate* inserted);
        void geneTemplateRemoved(GeneTemplate* removed);
        void getWaitingIndividuals(std::list<std::shared_ptr<Individual>>& inds);
        void breedNewGeneration();
        bool evaluateFitness(const std::shared_ptr<Individual>& ind);
        bool evaluateResult();
        void setPopulation(const std::shared_ptr<Individual>& ind);
        void loadIndividualIntoModel(const std::shared_ptr<Individual>& ind,
                                     unsigned int what = individualLoadAll);
        bool writePopulation();
        void readPopulation();
        bool writeHistory();
        void rebuildHistoryIndex();
        void readHistory(int generationNumber);
        bool writeHistoryIndex();
        void readHistoryIndex();
        sensor::ArraySensor* getGeneticSensor();
        FitnessData* makeFitnessData(int mode, const std::string& name);
        FitnessData* makeFitnessData(xml::DomObject& o);
        void stepComplete() override;
    protected:
        // Configuration
        Selection selectionMode_;
        Algorithm algorithm_;
        Crossover crossoverMode_;
        bool microGa_;
        int tournamentSize_;
        int breedingPoolSize_;
        size_t populationSize_;
        int nCellsXY_;
        double cellSizeZ_;
        double sensorDistance_;
        int gridMaterialId_;
        double frequencyRange_;
        double crossOverProbability_;
        double mutationProbability_;
        size_t numFrequencies_;
        double unfitnessThreshold_;
        int generationNumber_;
        GeneFactory geneFactory_;
        bool keepParents_;
        bool averageSensor_;
        double repeatCell_;
        double layerSpacing_;
        LayerSymmetry layerSymmetry_;
        bool xyComponents_;
        bool useExistingDomain_;
        bool collectFieldData_{false};
        // Attributes
        Population* population_;
        std::vector<int> layerShapeIds_;
        std::vector<int> layerMaterialIds_;
        FitnessFunction* fitness_;
        double frequencySpacing_;
        std::vector<double> unfitnessProgress_;
        std::map<int, int64_t> historyIndex_; // <generationNumber, fileOffset>
        std::set<std::shared_ptr<fdtd::Job>> jobs_;
        bool populationLoaded_;
        box::Factory<FitnessData> fitnessDataFactory_;
    public:
        // Getters
        size_t populationSize() const { return populationSize_; }
        Population* population() const { return population_; }
        double sensorDistance() const { return sensorDistance_; }
        int nCellsXY() const { return nCellsXY_; }
        double cellSizeZ() const { return cellSizeZ_; }
        size_t numFrequencies() const { return numFrequencies_; }
        double frequencyRange() const { return frequencyRange_; }
        double unfitnessThreshold() const { return unfitnessThreshold_; }
        FitnessFunction* fitness() { return fitness_; }
        int layerMaterialId(size_t layer);
        int layerShapeId(size_t layer);
        double frequencySpacing() const { return frequencySpacing_; }
        int generationNumber() const { return generationNumber_; }
        GeneFactory* geneFactory() { return &geneFactory_; }
        Selection selectionMode() const { return selectionMode_; }
        Algorithm algorithm() const { return algorithm_; }
        Crossover crossoverMode() const { return crossoverMode_; }
        bool microGa() const { return microGa_; }
        int tournamentSize() const { return tournamentSize_; }
        int breedingPoolSize() const { return breedingPoolSize_; }
        bool keepParents() const { return keepParents_; }
        bool averageSensor() const { return averageSensor_; }
        double repeatCell() const { return repeatCell_; }
        double layerSpacing() const { return layerSpacing_; }
        void generationNumber(int v) { generationNumber_ = v; }
        double crossOverProbability() const { return crossOverProbability_; }
        double mutationProbability() const { return mutationProbability_; }
        int gridMaterialId() const { return gridMaterialId_; }
        LayerSymmetry layerSymmetry() const { return layerSymmetry_; }
        const std::vector<double>& unfitnessProgress() const { return unfitnessProgress_; }
        bool populationLoaded() const { return populationLoaded_; }
        std::map<int, int64_t>& historyIndex() { return historyIndex_; }
        bool xyComponents() const { return xyComponents_; }
        bool useExistingDomain() const { return useExistingDomain_; }
        bool collectFieldData() const { return collectFieldData_; }

        // Setters
        void set(size_t populationSize, int nCellsXY,
                 double cellSizeZ,
                 double sensorDistance, int gridMaterialId,
                 double frequencyRange, double crossOverProbability,
                 double mutationProbability, double unfitnessThreshold,
                 Selection selectionMode, Algorithm algorithm, Crossover crossoverMode,
                 int tournamentSize, int breedingPoolSize, bool keepParents,
                 bool microGa, bool averageSensor, double repeatCell, double layerSpacing);
        void set(int nCellsXY, double cellSizeZ,
                 double sensorDistance, int gridMaterialId,
                 double frequencyRange);
        void layerSymmetry(LayerSymmetry v) { layerSymmetry_ = v; }
        void repeatCell(double v) { repeatCell_ = v; }
        void layerSpacing(double v) { layerSpacing_ = v; }
        void numFrequencies(size_t v) { numFrequencies_ = v; }
        void populationLoaded(bool v) { populationLoaded_ = v; }
        void xyComponents(bool v) { xyComponents_ = v; }
        void useExistingDomain(bool v) { useExistingDomain_ = v; }
        void collectFieldData(bool v) { collectFieldData_ = v; }
        void name(const std::string& v) override;
        void microGa(bool v) {microGa_ = v;}
        void algorithm(Algorithm v) {algorithm_ = v;}
        void averageSensor(bool v) {averageSensor_ = v;}
        void populationSize(size_t v) {populationSize_ = v;}
        void unfitnessThreshold(double v) {unfitnessThreshold_ = v;}
        void selectionMode(Selection v) {selectionMode_ = v;}
        void tournamentSize(int v) {tournamentSize_ = v;}
        void breedingPoolSize(int v) {breedingPoolSize_ = v;}
        void crossoverMode(Crossover v) {crossoverMode_ = v;}
        void keepParents(bool v) {keepParents_ = v;}

    protected:
        // Helpers
        void runGeneration();
    };

}


#endif //FDTDLIFE_GENETICSEARCH_H
