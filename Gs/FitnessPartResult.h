/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSPARTRESULT_H
#define FDTDLIFE_FITNESSPARTRESULT_H

#include <string>
#include <Xml/DomObject.h>

namespace gs {

    // A class that records the unfitness result from one requirement
    class FitnessPartResult {
    public:
        FitnessPartResult(int position, const char* partType, double result);
        FitnessPartResult();
        FitnessPartResult(const FitnessPartResult& other);
        FitnessPartResult& operator=(const FitnessPartResult& other);
        int position() const {return position_;}
        const char* partType() const {return partType_.c_str();}
        double result() const {return result_;}
        const char* extraInfo() const {return extraInfo_.c_str();}
        double extraValue() const {return extraValue_;}
        void read(xml::DomObject& root);
        void write(xml::DomObject& root) const;
        friend xml::DomObject& operator<<(xml::DomObject& o, const FitnessPartResult& i) {i.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, FitnessPartResult& i) {i.read(o); return o;}
        void setExtra(const std::string& extraInfo, double extraValue);
    protected:
        int position_;
        std::string partType_;
        double result_;
        std::string extraInfo_;
        double extraValue_;
    };
}


#endif //FDTDLIFE_FITNESSPARTRESULT_H
