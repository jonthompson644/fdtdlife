/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCECATLAYERGENE_H
#define FDTDLIFE_ADMITTANCECATLAYERGENE_H

#include "UnitCellLayerGene.h"
#include <vector>
#include <cstdint>
#include <map>
#include <complex>

namespace gs {
    // A gene that codes layers taken from the admittance catalogue.
    class AdmittanceCatLayerGene : public UnitCellLayerGene {
    protected:
        // Evolving parts
        uint64_t coding_;   // The catalogue item coding
        double gFactor_;   // The factor to use for the unit cell
        // Others
        size_t catBits_;
        GeneBitsSpecDouble gFactorSpec_;
    public:
        // Construction
        AdmittanceCatLayerGene(size_t repeatBits, int repeatMin, size_t catBits,
                               const GeneBitsSpecDouble& gFactorSpec);
        // API
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        size_t sizeBits() const override;
        uint64_t pattern(fdtd::Model* m) const;
        void makeLayer(GeneticSearch* s, unsigned int what, int layer,
                       double zLocation) override;
        // Getters
        uint64_t coding() const { return coding_; }
        double gFactor() const { return gFactor_; }
        // Setters
        void coding(uint64_t v) { coding_ = v; }
        void gFactor(double v) { gFactor_ = v; }
    };
}


#endif //FDTDLIFE_ADMITTANCELAYERGENE_H
