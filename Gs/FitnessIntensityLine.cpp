/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessIntensityLine.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include "Individual.h"
#include "OneDFitnessData.h"
#include <Sensor/ArraySensor.h>
#include <Xml/DomObject.h>
#include <cmath>
#include <sstream>

// Initialise the model ready for the assessment of an individual
void gs::FitnessIntensityLine::initialise(size_t numFrequencies, double frequencyStart,
                                          double frequencySpacing) {
    // Base class first
    FitnessPlaneWaveBase::initialise(numFrequencies, frequencyStart, frequencySpacing);
    // Use the min_ array for the weight values for each cell
    min_.resize(numPointsX_);
    max_.resize(numPointsX_);
    double x = s_->m()->p()->p1().x().value();
    double xStep = (s_->m()->p()->p2().x().value() - s_->m()->p()->p1().x().value()) /
                   (static_cast<double>(numPointsX_) - 1.0);
    size_t sampleN = 0;
    bool firstPoint = true;
    double weight = 0.0;
    FitnessPoint prevPoint(0.0, 0.0, 0.0);
    for(auto& point : points_) {
        if(!firstPoint) {
            // The equation of the straight lines between the two points
            double minM = (point.min() - prevPoint.min()) / (point.x() - prevPoint.x());
            double minC = prevPoint.min() - minM * prevPoint.x();
            // Now fill the frequency samples between the two points
            weight = minM * x + minC;
            while(x >= prevPoint.x() && x < point.x() && sampleN < numPointsX_) {
                min_[sampleN] = weight;
                max_[sampleN] = 0.0;
                x += xStep;
                weight = minM * x + minC;
                sampleN++;
            }
        }
        prevPoint = point;
        firstPoint = false;
    }
    while(sampleN < numPointsX_) {
        min_[sampleN] = weight;
        max_[sampleN] = 0.0;
        sampleN++;
    }
    // Create the array sensorId
    auto sensor = getSensor();
    // Configure the sensorId
    sensor->numPointsX((int) numPointsX_);
    sensor->numPointsY(1);
    sensor->averageOverCell(s_->averageSensor());
    sensor->componentSel({true, s_->xyComponents(), s_->xyComponents()});
    sensor->manualZPos(true);
    sensor->transmittedZPos(z_);
    fdtd::Configuration* p = s_->m()->p();
    sensor->reflectedZPos(p->p1().z().value() -
                          p->dr().z().value() * p->scatteredFieldZoneSize() / 2);
    // The range
    calcRange();
}

// Get or create the sensorId for this fitness function
sensor::ArraySensor* gs::FitnessIntensityLine::getSensor() {
    std::stringstream name;
    name << "IntensityLine:" << s_->identifier();
    sensor::ArraySensor* result = dynamic_cast<sensor::ArraySensor*>(s_->m()->sensors().find(
            name.str()));
    if(result == nullptr) {
        result = dynamic_cast<sensor::ArraySensor*>(
                s_->m()->sensors().makeSensor(sensor::Sensors::modeArraySensor));
        result->name(name.str());
    }
    return result;
}

void gs::FitnessIntensityLine::addPoint(double frequency, double min, double max) {
    // Base class
    FitnessBase::addPoint(frequency, min, max);
    // The range
    calcRange();
}

// Determine the unfitness of the individual
double gs::FitnessIntensityLine::unfitness(Individual* ind) {
    double result = 0.0;
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        // Create the data objects
        OneDFitnessData<double>* transmittance = nullptr;
        switch(dataSource_) {
            case dataSourceMagnitude:
                transmittance = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataTransmittanceDist,
                                            "TransmittanceMag"));
                break;
            case dataSourceXComponent:
                transmittance = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataTransmittanceDist,
                                            "TransmittanceX"));
                break;
            case dataSourceYComponent:
                transmittance = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataAmplitudeDist,
                                            "TransmittanceY"));
                break;
        }
        transmittance->setSize(numPointsX_);
        // Get the sensorId data
        for(size_t i = 0; i < numPointsX_; i++) {
            // Get the signal magnitude at the specified frequency
            double x, y;
            sensor->transmittanceAt(i, 0, frequency_, x, y, false);
            switch(dataSource_) {
                case dataSourceMagnitude:
                    transmittance->setData(i, std::hypot(x, y));
                    break;
                case dataSourceXComponent:
                    transmittance->setData(i, x);
                    break;
                case dataSourceYComponent:
                    transmittance->setData(i, y);
                    break;
            }
        }
        // Find the mean
        double total = 0.0;
        for(double v : transmittance->data()) {
            total += v;
        }
        double mean = total / numPointsX_;
        // Calculate the unfitness
        result = 0.0;
        for(size_t i = 0; i < numPointsX_; i++) {
            result += transmittance->data()[i] / mean * min_[i];
        }
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Write the configuration to the XML
void gs::FitnessIntensityLine::writeConfig(xml::DomObject& root) const {
    // Base class
    FitnessBase::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Obj("numpoints") << numPointsX_;
    root << xml::Obj("z") << z_;
    root << xml::Close();
}

// Read configuration from the XML
void gs::FitnessIntensityLine::readConfig(xml::DomObject& root) {
    // Base class
    FitnessBase::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Obj("numpoints") >> numPointsX_;
    root >> xml::Obj("z") >> xml::Default(0.0) >> z_;
    root >> xml::Close();
}

// Calculate the range
void gs::FitnessIntensityLine::calcRange() {
    // Calculate upper range
    rangeMax_ = 0;
    for(auto& p : points_) {
        rangeMax_ = std::max(rangeMax_, p.min());
    }
}
