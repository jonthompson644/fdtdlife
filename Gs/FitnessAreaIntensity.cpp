/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessAreaIntensity.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include "Individual.h"
#include "OneDFitnessData.h"
#include <Sensor/AreaSensor.h>
#include <Xml/DomObject.h>
#include <cmath>

// Initialise the model ready for the assessment of an individual
void gs::FitnessAreaIntensity::initialise(size_t numFrequencies, double frequencyStart, double frequencySpacing) {
    // Base class
    FitnessPlaneWaveBase::initialise(numFrequencies, frequencyStart, frequencySpacing);
    // Get the area sensorId
    sensor::AreaSensor* sensor = getSensor();
    // Configure the sensorId
    sensor->center(center_);
    sensor->width(width_);
    sensor->height(height_);
    sensor->orientation(orientation_);
}

// Determine the unfitness of the individual
double gs::FitnessAreaIntensity::unfitness(Individual* ind) {
    double result = 0.0;
    // Get the signal magnitude at the specified frequency
    sensor::AreaSensor* sensor = getSensor();
    if(sensor != nullptr) {
        // Create the data object
        std::string dataName;
        switch(dataSource_) {
            case dataSourceMagnitude:
                dataName = "AmplitudeMag";
                break;
            case dataSourceXComponent:
                dataName = "AmplitudeX";
                break;
            case dataSourceYComponent:
                dataName = "AmplitudeY";
                break;
        }
        auto data = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataTransmittance, dataName));
        data->setSize(sensor->frequencies().n());
        // Copy in the data
        for(size_t i=0; i<sensor->frequencies().n(); i++) {
            switch(dataSource_) {
                case dataSourceMagnitude:
                    data->setData(i, std::hypot(sensor->point().spectrumX().amplitude()[i],
                                                sensor->point().spectrumY().amplitude()[i]));
                    break;
                case dataSourceXComponent:
                    data->setData(i, sensor->point().spectrumX().amplitude()[i]);
                    break;
                case dataSourceYComponent:
                    data->setData(i, sensor->point().spectrumY().amplitude()[i]);
                    break;
            }
        }
        // Assess the fitness
        double x, y;
        sensor->at(frequency_, x, y);
        switch(dataSource_) {
            case dataSourceMagnitude:
                result = std::hypot(x, y);
                break;
            case dataSourceXComponent:
                result = x;
                break;
            case dataSourceYComponent:
                result = y;
                break;
        }
    }
    result = 100.0-result * weight_;
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Write the configuration to the XML
void gs::FitnessAreaIntensity::writeConfig(xml::DomObject& root) const {
    // Base class
    FitnessBase::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Obj("centerx") << center_.x();
    root << xml::Obj("centery") << center_.y();
    root << xml::Obj("centerz") << center_.z();
    root << xml::Obj("width") << width_;
    root << xml::Obj("height") << height_;
    root << xml::Obj("orientation") << orientation_;
    root << xml::Close();
}

// Read configuration from the XML
void gs::FitnessAreaIntensity::readConfig(xml::DomObject& root) {
    // Base class
    FitnessBase::readConfig(root);
    // My stuff
    double v;
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Obj("width") >> width_;
    root >> xml::Obj("height") >> height_;
    root >> xml::Obj("orientation") >> orientation_;
    root >> xml::Obj("centerx") >> v; center_.x(v);
    root >> xml::Obj("centery") >> v; center_.y(v);
    root >> xml::Obj("centerz") >> v; center_.z(v);
    root >> xml::Close();
}

// Get or create the sensorId for this fitness function
sensor::AreaSensor* gs::FitnessAreaIntensity::getSensor() {
    std::stringstream name;
    name << "AreaIntensity:" << s_->identifier();
    auto* result = dynamic_cast<sensor::AreaSensor*>(s_->m()->sensors().find(name.str()));
    if(result == nullptr) {
        result = dynamic_cast<sensor::AreaSensor*>(
                s_->m()->sensors().makeSensor(sensor::Sensors::modeArea));
        result->name(name.str());
    }
    return result;
}

