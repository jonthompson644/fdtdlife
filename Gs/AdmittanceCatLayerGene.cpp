/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceCatLayerGene.h"
#include "Chromosome.h"
#include "GeneticSearch.h"
#include <Domain/Material.h>
#include <Fdtd/Model.h>
#include <Domain/BinaryPlateNxN.h>
#include <Fdtd/AdmittanceCatalogue.h>
#include <Fdtd/CatalogueItem.h>
#include <Box/Constants.h>
#include <algorithm>

// Constructor
gs::AdmittanceCatLayerGene::AdmittanceCatLayerGene(size_t repeatBits, int repeatMin,
                                                   size_t catBits,
                                                   const GeneBitsSpecDouble& gFactorSpec) :
        UnitCellLayerGene(GeneTemplate::geneTypeAdmittanceCatLayer, repeatBits,
                          repeatMin),
        coding_(0),
        gFactor_(1.0),
        catBits_(catBits),
        gFactorSpec_(gFactorSpec) {
}

// Encode the gene into the chromosome
void gs::AdmittanceCatLayerGene::encode(Chromosome* c) {
    UnitCellLayerGene::encode(c);
    c->encode(coding_, catBits_);
    gFactorSpec_.encode(c, gFactor_);
}

// Decode the gene from the chromosome
void gs::AdmittanceCatLayerGene::decode(Chromosome* c) {
    UnitCellLayerGene::decode(c);
    coding_ = c->decode(catBits_);
    gFactor_ = gFactorSpec_.decode(c);
}

// Connect the gene to other genes it depends on
bool gs::AdmittanceCatLayerGene::connect(Chromosome* c, GeneticSearch* s) {
    bool result = UnitCellLayerGene::connect(c, s);
    // Validate myself (nothing yet)
    return result;
}

// Return the layer's pattern
uint64_t gs::AdmittanceCatLayerGene::pattern(fdtd::Model* m) const {
    uint64_t pattern = 0;
    std::shared_ptr<fdtd::CatalogueItem> catItem = m->admittanceCat().get(coding_);
    if(catItem) {
        pattern = catItem->pattern();
    }
    return pattern;
}

// Set the model configuration corresponding to this allele
double gs::AdmittanceCatLayerGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(present_ && (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_;
        makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_;
            makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Return the number of bits used by the gene
size_t gs::AdmittanceCatLayerGene::sizeBits() const {
    return catBits_ + UnitCellLayerGene::sizeBits();
}

// Make a layer in the model for this gene
void gs::AdmittanceCatLayerGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
                                           double zLocation) {
    // Set the parameters of the material
    domain::Material* mat = material(s);
    mat->epsilon(box::Expression(box::Constants::epsilonMetal_));   // A metallic epsilon
    mat->mu(box::Expression(box::Constants::mu0_));  // A metallic mu
    mat->sigma(box::Expression(box::Constants::sigmaMetal_));   // A metallic sigma
    mat->sigmastar(box::Expression(box::Constants::sigmastar0_));  // A metallic sigma star?
    // What's the unit cell of this item going to be
    double myUnitCell;
    if((what & GeneticSearch::individualLoadDomainSize) != 0) {
        myUnitCell = unitCell_;
    } else {
        myUnitCell = (s->m()->p()->p2().x().value() - s->m()->p()->p1().x().value())
                     / (double) repeat_;
    }
    myUnitCell *= gFactor_;
    // Get the admittance catalogue item
    fdtd::AdmittanceCatalogue* cat = &s->m()->admittanceCat();
    std::shared_ptr<fdtd::CatalogueItem> catItem;
    if(cat->catalogueSize() > 0) {
        catItem = cat->get(coding_ % cat->catalogueSize());
    }
    fdtd::AdmittanceData admittance;
    uint64_t pattern = 0;
    int bitsPerHalfSide = cat->bitsPerHalfSide();
    if(catItem != nullptr) {
        pattern = catItem->pattern();
        admittance = catItem->admittance(cat->unitCell(), cat->startFrequency(),
                                         cat->frequencyStep());
    }
    // Find or create the shape
    int shapeId = s->layerShapeId(layer);
    domain::Shape* shape = s->m()->d()->getShape(shapeId);
    auto plate = dynamic_cast<domain::BinaryPlateNxN*>(shape);
    if(plate == nullptr) {
        s->m()->d()->remove(shape);
        plate = dynamic_cast<domain::BinaryPlateNxN*>(s->m()->d()->makeShape(
                shapeId, domain::Domain::shapeModeBinaryPlateNxN));
    }
    // Set the shape parameters
    std::stringstream name;
    name << "GeneticLayer" << layer;
    plate->name(name.str());
    plate->fillMaterial(domain::Domain::defaultMaterial);
    plate->layerThickness(box::Expression(0.000001));
    plate->bitsPerHalfSide(bitsPerHalfSide);
    plate->material(mat->index());
    plate->cellSizeX(box::Expression(myUnitCell));
    plate->center().value(box::Vector<double>(0.0, 0.0, zLocation));
    plate->coding({pattern});
    plate->admittanceTable() = admittance;
    plate->seqGenerated(true);
}

// Copy the state from the other gene into this one
void gs::AdmittanceCatLayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    UnitCellLayerGene::copyFrom(other, m);
    auto* from = dynamic_cast<AdmittanceCatLayerGene*>(other);
    if(from != nullptr) {
        coding_ = from->coding_;
        gFactor_ = from->gFactor_;
    }
}
