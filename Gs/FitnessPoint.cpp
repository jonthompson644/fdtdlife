/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessPoint.h"
#include <Xml/DomObject.h>

// Initialising constructor
gs::FitnessPoint::FitnessPoint(double x, double min, double max) :
        x_(x),
        min_(min),
        max_(max) {
}

// Default constructor
gs::FitnessPoint::FitnessPoint() :
        x_(0.0),
        min_(0.0),
        max_(0.0) {
}

// Copy constructor
gs::FitnessPoint::FitnessPoint(const FitnessPoint& other) :
        x_(other.x_),
        min_(other.min_),
        max_(other.max_) {
}

// Set the point
void gs::FitnessPoint::set(double x, double min, double max) {
    x_ = x;
    min_ = min;
    max_ = max;
}

// Assignment operator
gs::FitnessPoint& gs::FitnessPoint::operator=(const FitnessPoint &other) {
    x_ = other.x_;
    min_ = other.min_;
    max_ = other.max_;
    return *this;
}

// Comparison operator, used by sort function etc.
// The frequency coordinate is used for comparison purposes
bool gs::FitnessPoint::operator<(const FitnessPoint& other) const {
    return x_ < other.x_;
}

// Write the configuration to the DOM object
void gs::FitnessPoint::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("x") << x_;
    root << xml::Obj("min") << min_;
    root << xml::Obj("max") << max_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessPoint::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("frequency") >> xml::Default(x_) >> x_;  // Backwards compatibility
    root >> xml::Obj("x") >> xml::Default(x_) >> x_;
    root >> xml::Obj("min") >> xml::Default(min_) >> min_;
    root >> xml::Obj("max") >> xml::Default(max_) >> max_;
    root >> xml::Close();
}
