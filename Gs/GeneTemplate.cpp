/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneTemplate.h"
#include "GeneFactory.h"
#include "LayerSpacingGene.h"
#include "SquarePlateGene.h"
#include "DielectricLayerGene.h"
#include "RepeatCellGene.h"
#include "LayerPresentGene.h"
#include "AdmittanceCatLayerGene.h"
#include <Xml/DomObject.h>
#include <Xml/Exception.h>
#include "LayerSymmetryGene.h"
#include "BinaryPlateNxNGene.h"
#include "DuplicateLayerGene.h"
#include "AdmittanceFnLayerGene.h"
#include "DielectricBlockGene.h"
#include "LensDimensionsGene.h"
#include "TransLensSquareGene.h"
#include "LongLensSquareGene.h"
#include "VariableGene.h"

// Constructor
gs::GeneTemplate::GeneTemplate(int identifier, GeneFactory* factory, GeneType geneType) :
        identifier_(identifier),
        factory_(factory),
        range_(14, 0.0000005, 0.0081925),
        gFactor_(0, 1.0, 1.0) {
    set(geneType, false, 8, 3, 1, {});
    factory_->addTemplate(this);
}

// Destructor
gs::GeneTemplate::~GeneTemplate() {
    factory_->removeTemplate(this);
}

// Set the template configuration
void gs::GeneTemplate::set(GeneType t, bool fourFoldSymmetry, int bitsPerHalfSide,
                           size_t repeatBits, int repeatMin,
                           size_t catBits) {
    geneType_ = t;
    fourFoldSymmetry_ = fourFoldSymmetry;
    bitsPerHalfSide_ = bitsPerHalfSide;
    repeatBits_ = repeatBits;
    repeatMin_ = repeatMin;
    catBits_ = catBits;
    calcSizeBits();
}

// Calculate the number of bits required for this gene template
void gs::GeneTemplate::calcSizeBits() {
    Gene* g = create();
    sizeBits_ = g->sizeBits();
    delete g;
}

// Create a gene from the template
gs::Gene* gs::GeneTemplate::create() {
    Gene* result = nullptr;
    switch(geneType_) {
        case geneTypeSquarePlate:
            result = new SquarePlateGene(range_, hole_,
                                         repeatBits_, repeatMin_);
            break;
        case geneTypeDielectricLayer:
            result = new DielectricLayerGene;
            break;
        case geneTypeLayerSpacing:
            result = new LayerSpacingGene(range_);
            break;
        case geneTypeLayerPresent:
            result = new LayerPresentGene;
            break;
        case geneTypeLayerSymmetry:
            result = new LayerSymmetryGene;
            break;
        case geneTypeAdmittanceCatLayer:
            result = new AdmittanceCatLayerGene(repeatBits_, repeatMin_,
                                                catBits_, gFactor_);
            break;
        case geneTypeBinaryPlateNxN:
            result = new BinaryPlateNxNGene(bitsPerHalfSide_,
                                            fourFoldSymmetry_,
                                            repeatBits_, repeatMin_);
            break;
        case geneTypeDuplicateLayer:
            result = new DuplicateLayerGene;
            break;
        case geneTypeAdmittanceFn:
            result = new AdmittanceFnLayerGene(realA_, realB_, realC_,
                                               realD_, realE_, realF_,
                                               imagA_, imagB_, imagC_,
                                               imagD_, imagE_, imagF_);
            break;
        default:
        case geneTypeRepeatCell:
            result = new RepeatCellGene(range_);
            break;
        case geneTypeDielectricBlock:
            result = new DielectricBlockGene(center_, size_,
                                             permittivity_);
            break;
        case geneTypeLensDimensions:
            result = new LensDimensionsGene(unitCell_,
                                            layerSpacing_,
                                            numRows_);
            break;
        case geneTypeTransLensSquare:
            result = new TransLensSquareGene(realA_, realB_, realC_, realD_,
                                             realE_, realF_);
            break;
        case geneTypeLongLensSquare:
            result = new LongLensSquareGene(realA_, realB_, realC_, realD_,
                                            realE_, realF_);
            break;
        case geneTypeVariable:
            result = new VariableGene(range_, variableId_);
            break;
    }
    return result;
}

// The gene template type strings
const std::vector<const char*> gs::GeneTemplate::geneTypeStrings_ = {
        "Repeat Cell", "Binary Coded Plate Deleted", "Square Plate", "Dielectric Layer",
        "Layer Spacing", "Layer Present", "Admittance Cat Layer", "Layer Symmetry",
        "Binary Plate NxN", "Duplicate Layer", "Admittance Fn Layer",
        "Dielectric Block", "Transverse Lens Square", "Longitudinal Lens Square",
        "Lens Dimensions", "Variable"
};

// Write the configuration to the DOM object
void gs::GeneTemplate::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("genetype") << geneType_;
    root << xml::Obj("fourfoldsymmetry") << fourFoldSymmetry_;
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    root << xml::Obj("repeatbits") << repeatBits_;
    root << xml::Obj("repeatmin") << repeatMin_;
    root << xml::Obj("catbits") << catBits_;
    root << xml::Obj("centerx") << center_.x();
    root << xml::Obj("centery") << center_.y();
    root << xml::Obj("centerz") << center_.z();
    root << xml::Obj("sizex") << size_.x();
    root << xml::Obj("sizey") << size_.y();
    root << xml::Obj("sizez") << size_.z();
    root << xml::Obj("range") << range_;
    root << xml::Obj("reala") << realA_;
    root << xml::Obj("realb") << realB_;
    root << xml::Obj("realc") << realC_;
    root << xml::Obj("reald") << realD_;
    root << xml::Obj("reale") << realE_;
    root << xml::Obj("realf") << realF_;
    root << xml::Obj("imaga") << imagA_;
    root << xml::Obj("imagb") << imagB_;
    root << xml::Obj("imagc") << imagC_;
    root << xml::Obj("imagd") << imagD_;
    root << xml::Obj("image") << imagE_;
    root << xml::Obj("imagf") << imagF_;
    root << xml::Obj("gfactor") << gFactor_;
    root << xml::Obj("permittivity") << permittivity_;
    root << xml::Obj("layerspacing") << layerSpacing_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("numrows") << numRows_;
    root << xml::Obj("variableid") << variableId_;
    root << xml::Obj("hole") << hole_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::GeneTemplate::readConfig(xml::DomObject& root) {
    double v;
    size_t w;
    root >> xml::Open();
    root >> xml::Obj("fourfoldsymmetry") >> xml::Default(fourFoldSymmetry_)
         >> fourFoldSymmetry_;
    root >> xml::Obj("repeatbits") >> xml::Default(repeatBits_) >> repeatBits_;
    root >> xml::Obj("catbits") >> xml::Default(catBits_) >> catBits_;
    root >> xml::Obj("repeatmin") >> xml::Default(1) >> repeatMin_;
    root >> xml::Obj("bitsperhalfside") >> xml::Default(8) >> bitsPerHalfSide_;
    root >> xml::Obj("centerx") >> xml::Default(0.0) >> v;
    center_.x(v);
    root >> xml::Obj("centery") >> xml::Default(0.0) >> v;
    center_.y(v);
    root >> xml::Obj("centerz") >> xml::Default(0.0) >> v;
    center_.z(v);
    root >> xml::Obj("sizex") >> xml::Default(0.0) >> v;
    size_.x(v);
    root >> xml::Obj("sizey") >> xml::Default(0.0) >> v;
    size_.y(v);
    root >> xml::Obj("sizez") >> xml::Default(0.0) >> v;
    size_.z(v);
    root >> xml::Obj("variableid") >> xml::Default(-1) >> variableId_;
    if(!root.obj("range").empty()) {
        root >> xml::Obj("range") >> range_;
    }
    if(!root.obj("reala").empty()) {
        root >> xml::Obj("reala") >> realA_;
    }
    if(!root.obj("realb").empty()) {
        root >> xml::Obj("realb") >> realB_;
    }
    if(!root.obj("realc").empty()) {
        root >> xml::Obj("realc") >> realC_;
    }
    if(!root.obj("reald").empty()) {
        root >> xml::Obj("reald") >> realD_;
    }
    if(!root.obj("reale").empty()) {
        root >> xml::Obj("reale") >> realE_;
    }
    if(!root.obj("realf").empty()) {
        root >> xml::Obj("realf") >> realF_;
    }
    if(!root.obj("imaga").empty()) {
        root >> xml::Obj("imaga") >> imagA_;
    }
    if(!root.obj("imagb").empty()) {
        root >> xml::Obj("imagb") >> imagB_;
    }
    if(!root.obj("imagc").empty()) {
        root >> xml::Obj("imagc") >> imagC_;
    }
    if(!root.obj("imagd").empty()) {
        root >> xml::Obj("imagd") >> imagD_;
    }
    if(!root.obj("image").empty()) {
        root >> xml::Obj("image") >> imagE_;
    }
    if(!root.obj("imagf").empty()) {
        root >> xml::Obj("imagf") >> imagF_;
    }
    if(!root.obj("gfactor").empty()) {
        root >> xml::Obj("gfactor") >> gFactor_;
    }
    if(!root.obj("permittivity").empty()) {
        root >> xml::Obj("permittivity") >> permittivity_;
    }
    if(!root.obj("layerspacing").empty()) {
        root >> xml::Obj("layerspacing") >> layerSpacing_;
    }
    if(!root.obj("unitcell").empty()) {
        root >> xml::Obj("unitcell") >> unitCell_;
    }
    if(!root.obj("numrows").empty()) {
        root >> xml::Obj("numrows") >> numRows_;
    }
    if(!root.obj("hole").empty()) {
        root >> xml::Obj("hole") >> hole_;
    }
    // Backwards compatible from before the gene bits spec objects
    root >> xml::Obj("rangemin") >> xml::Default(range_.min()) >> v;
    range_.min(v);
    root >> xml::Obj("rangemax") >> xml::Default(range_.max()) >> v;
    range_.max(v);
    root >> xml::Obj("rangebits") >> xml::Default(range_.bits()) >> w;
    range_.bits(w);
    root >> xml::Close();
    // Calculate the size
    Gene* g = create();
    sizeBits_ = g->sizeBits();
    delete g;
}

// Return the gene type as text
const char* gs::GeneTemplate::geneTypeText() const {
    return geneTypeStrings_[geneType_];
}

// Copy gene template contents from the other
void gs::GeneTemplate::copyFrom(const gs::GeneTemplate* other) {
    range_ = other->range_;
    sizeBits_ = other->sizeBits_;
    catBits_ = other->catBits_;
    fourFoldSymmetry_ = other->fourFoldSymmetry_;
    repeatBits_ = other->repeatBits_;
    repeatMin_ = other->repeatMin_;
    bitsPerHalfSide_ = other->bitsPerHalfSide_;
    realA_ = other->realA_;
    realB_ = other->realB_;
    realC_ = other->realC_;
    realD_ = other->realD_;
    realE_ = other->realE_;
    realF_ = other->realF_;
    imagA_ = other->imagA_;
    imagB_ = other->imagB_;
    imagC_ = other->imagC_;
    imagD_ = other->imagD_;
    imagE_ = other->imagE_;
    imagF_ = other->imagF_;
    gFactor_ = other->gFactor_;
    permittivity_ = other->permittivity_;
    center_ = other->center_;
    size_ = other->size_;
    layerSpacing_ = other->layerSpacing_;
    unitCell_ = other->unitCell_;
    numRows_ = other->numRows_;
    variableId_ = other->variableId_;
    hole_ = other->hole_;
}
