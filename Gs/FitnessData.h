#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSDATA_H
#define FDTDLIFE_FITNESSDATA_H

#include <string>
#include <Xml/DomObject.h>

namespace gs {

    // Base class for data stored with an individual by the fitness evaluation
    class FitnessData {
    public:
        // Construction
        FitnessData() = default;
        virtual void construct(int mode, int identifier, const std::string& name);
        virtual ~FitnessData() = default;

        // API
        virtual void write(xml::DomObject& root) const;
        virtual void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const FitnessData& m) {
            m.write(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, FitnessData& m) {
            m.read(o);
            return o;
        }

        // Getters
        const std::string& name() const {return name_;}
        int mode() const {return mode_;}

    protected:
        std::string name_;
        int identifier_ {-1};
        int mode_ {-1};
    };
}

#endif //FDTDLIFE_FITNESSDATA_H
