/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPlateNxNGene.h"
#include <sstream>
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include <Domain/BinaryPlateNxN.h>
#include "GeneticSearch.h"
#include <Domain/Material.h>
#include "AdmittanceCatLayerGene.h"
#include <Box/Constants.h>

// Constructor
gs::BinaryPlateNxNGene::BinaryPlateNxNGene(int bitsPerHalfSide, bool fourFoldSymmetry,
                                           size_t repeatBits, int repeatMin) :
        UnitCellLayerGene(GeneTemplate::geneTypeBinaryPlateNxN, repeatBits, repeatMin),
        pattern_(0),
        fourFoldSymmetry_(fourFoldSymmetry),
        bitsPerHalfSide_(bitsPerHalfSide),
        patternSize_(0),
        patternSize2Fold_(0),
        patternSize4Fold_(0) {
    initialise();
}

// Initialise the various data structures
void gs::BinaryPlateNxNGene::initialise() {
    // The sizes
    patternSize2Fold_ = (size_t) bitsPerHalfSide_ * (size_t) bitsPerHalfSide_;
    patternSize4Fold_ = 0;
    for(size_t i = 0; i < (size_t) bitsPerHalfSide_; i++) {
        patternSize4Fold_ += (i + 1);
    }
    // The main pattern
    if(fourFoldSymmetry_) {
        patternSize_ = patternSize4Fold_;
    } else {
        patternSize_ = patternSize2Fold_;
    }
    size_t numWords = (patternSize_ + bitsPerWord_ - 1) / bitsPerWord_;
    pattern_.resize(numWords, 0);
    // The two fold pattern
    numWords = (patternSize2Fold_ + bitsPerWord_ - 1) / bitsPerWord_;
    pattern2Fold_.resize(numWords, 0);
    // The bit location table
    bitLocation_.resize(patternSize2Fold_);
    uint64_t v = 0;
    for(int i = 0; i < bitsPerHalfSide_; i++) {
        for(int j = i; j < bitsPerHalfSide_; j++) {
            bitLocation_[i * bitsPerHalfSide_ + j] = v;
            bitLocation_[j * bitsPerHalfSide_ + i] = v;
            v++;
        }
    }
    // Print out the bit location table
    //for(int i=0; i<bitsPerHalfSide_; i++) {
    //    for(int j=0; j<bitsPerHalfSide_; j++) {
    //        std::cout << bitLocation_[j * bitsPerHalfSide_ + i] << " ";
    //    }
    //    std::cout << std::endl;
    //}
}

// Encode the allele into the chromosome
void gs::BinaryPlateNxNGene::encode(Chromosome* c) {
    UnitCellLayerGene::encode(c);
    size_t bitsRemaining = patternSize_;
    for(auto& word : pattern_) {
        size_t bits = (bitsRemaining > bitsPerWord_) ? bitsPerWord_ : bitsRemaining;
        c->encode(word, bits);
        bitsRemaining -= bits;
    }
}

// Decode the allele from the chromosome
void gs::BinaryPlateNxNGene::decode(Chromosome* c) {
    UnitCellLayerGene::decode(c);
    size_t bitsRemaining = patternSize_;
    for(auto& word : pattern_) {
        size_t bits = (bitsRemaining > bitsPerWord_) ? bitsPerWord_ : bitsRemaining;
        word = c->decode(bits);
        bitsRemaining -= bits;
    }
    makePattern();
}

// Connect the gene to other genes it depends on
bool gs::BinaryPlateNxNGene::connect(Chromosome* c, GeneticSearch* s) {
    bool result = UnitCellLayerGene::connect(c, s);
    // Validate myself (nothing yet)
    return result;
}

// Set the model configuration corresponding to this allele
double gs::BinaryPlateNxNGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(present_ && (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_;
        makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_;
            makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Set the model configuration corresponding to this allele
void
gs::BinaryPlateNxNGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
        double zLocation) {
    // Set the parameters of the material
    domain::Material* mat = material(s);
    mat->epsilon(box::Expression(box::Constants::epsilonMetal_));   // A metallic epsilon
    mat->mu(box::Expression(box::Constants::mu0_));
    // Find or create the shape
    int shapeId = s->layerShapeId(layer);
    domain::Shape* shape = s->m()->d()->getShape(shapeId);
    auto plate = dynamic_cast<domain::BinaryPlateNxN*>(shape);
    if(plate == nullptr) {
        plate = dynamic_cast<domain::BinaryPlateNxN*>(s->m()->d()->makeShape(
                shapeId, domain::Domain::shapeModeBinaryPlateNxN));
    }
    // Set the shape parameters
    if((what & GeneticSearch::individualLoadDomainSize) != 0) {
        plate->cellSizeX(box::Expression(unitCell_));
    } else {
        plate->cellSizeX(box::Expression(s->m()->p()->dr().x().value() / (double) repeat_));
    }
    std::stringstream name;
    name << "GeneticLayer" << layer;
    plate->name(name.str());
    plate->material(mat->index());
    plate->bitsPerHalfSide(bitsPerHalfSide_);
    plate->layerThickness(box::Expression(0.000001));
    plate->fillMaterial(domain::Domain::defaultMaterial);
    plate->center().value(box::Vector<double>(0.0, 0.0, zLocation));
    plate->coding(pattern2Fold_);
    plate->seqGenerated(true);
}

// Return the number of bits used by the gene
size_t gs::BinaryPlateNxNGene::sizeBits() const {
    return UnitCellLayerGene::sizeBits() + patternSize_;
}

// Set the binary coded plate gene from the 2 fold pattern
void gs::BinaryPlateNxNGene::pattern2Fold(const std::vector<uint64_t>& v) {
    pattern2Fold_ = v;
    // Convert the 2 fold pattern into the 4 fold if required by the gene
    if(fourFoldSymmetry_) {
        pattern_.clear();
        size_t numWords = (patternSize4Fold_ + bitsPerWord_ - 1) / bitsPerWord_;
        pattern_.resize(numWords, 0);
        for(uint64_t i = 0; i < patternSize2Fold_; i++) {
            uint64_t wordNum2 = i / bitsPerWord_;
            uint64_t bitNum2 = i % bitsPerWord_;
            uint64_t wordNum4 = bitLocation_[(size_t) i] / (uint64_t) bitsPerWord_;
            uint64_t bitNum4 = bitLocation_[(size_t) i] % (uint64_t) bitsPerWord_;
            uint64_t bit = (pattern2Fold_[(size_t) wordNum2] >> bitNum2) & (uint64_t) 1;
            pattern_[(size_t) wordNum4] |= (bit << bitNum4);
        }
    } else {
        pattern_ = pattern2Fold_;
    }
}

// Convert pattern into the 2 fold pattern required by the model shape
void gs::BinaryPlateNxNGene::makePattern() {
    if(fourFoldSymmetry_) {
        pattern2Fold_.clear();
        size_t numWords = (patternSize2Fold_ + bitsPerWord_ - 1) / bitsPerWord_;
        pattern2Fold_.resize(numWords, 0);
        for(uint64_t i = 0; i < patternSize2Fold_; i++) {
            uint64_t wordNum2 = i / (uint64_t) bitsPerWord_;
            uint64_t bitNum2 = i % (uint64_t) bitsPerWord_;
            uint64_t wordNum4 = bitLocation_[(size_t) i] / (uint64_t) bitsPerWord_;
            uint64_t bitNum4 = bitLocation_[(size_t) i] % (uint64_t) bitsPerWord_;
            uint64_t bit = (pattern_[(size_t) wordNum4] >> bitNum4) & (uint64_t) 1;
            pattern2Fold_[(size_t) wordNum2] |= (bit << bitNum2);
        }
    } else {
        pattern2Fold_ = pattern_;
    }
}

// Copy the state from the other gene into this one
void gs::BinaryPlateNxNGene::copyFrom(Gene* other, fdtd::Model* m) {
    UnitCellLayerGene::copyFrom(other, m);
    auto* from1 = dynamic_cast<BinaryPlateNxNGene*>(other);
    if(from1 != nullptr) {
        fourFoldSymmetry_ = from1->fourFoldSymmetry_;
        bitsPerHalfSide_ = from1->bitsPerHalfSide_;
        initialise();
        pattern2Fold_ = from1->pattern2Fold_;
        makePattern();
    } else {
        auto* from2 = dynamic_cast<AdmittanceCatLayerGene*>(other);
        if(from2 != nullptr) {
            fourFoldSymmetry_ = true;
            initialise();
        }
    }
}
