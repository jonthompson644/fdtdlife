#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Gs
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AdmittanceCatLayerGene.cpp \
    AdmittanceFnLayerGene.cpp \
    BinaryCodedPlateGene.cpp \
    BinaryPlateNxNGene.cpp \
    Chromosome.cpp \
    DielectricBlockGene.cpp \
    DielectricLayerGene.cpp \
    DuplicateLayerGene.cpp \
    Fitness3dBPoint.cpp \
    FitnessAdmittance.cpp \
    FitnessAdmittanceBase.cpp \
    FitnessAreaIntensity.cpp \
    FitnessTransmittance.cpp \
    FitnessBase.cpp \
    FitnessBirefPhaseDiff.cpp \
    FitnessData.cpp \
    FitnessFilter.cpp \
    FitnessFunction.cpp \
    FitnessInPhaseLine.cpp \
    FitnessIntensityLine.cpp \
    FitnessPartResult.cpp \
    FitnessPhaseShift.cpp \
    FitnessPhaseShiftChange.cpp \
    FitnessPlaneWaveBase.cpp \
    FitnessPoint.cpp \
    FunctionPoint.cpp \
    Gene.cpp \
    GeneBitsSpecDouble.cpp \
    GeneBitsSpecInteger.cpp \
    GeneBitsSpecLogDouble.cpp \
    GeneFactory.cpp \
    GeneTemplate.cpp \
    GeneticSearch.cpp \
    Individual.cpp \
    LayerGene.cpp \
    LayerPresentGene.cpp \
    LayerSpacingGene.cpp \
    LayerSymmetryGene.cpp \
    LensDimensionsGene.cpp \
    LensSquareGene.cpp \
    LongLensSquareGene.cpp \
    OneDFitnessData.cpp \
    Population.cpp \
    Random.cpp \
    RepeatCellGene.cpp \
    SquarePlateGene.cpp \
    TransLensSquareGene.cpp \
    TwoDFitnessData.cpp \
    UnitCellLayerGene.cpp

HEADERS += \
    AdmittanceCatLayerGene.h \
    AdmittanceFnLayerGene.h \
    BinaryCodedPlateGene.h \
    BinaryPlateNxNGene.h \
    Chromosome.h \
    DielectricBlockGene.h \
    DielectricLayerGene.h \
    DuplicateLayerGene.h \
    Fitness3dBPoint.h \
    FitnessAdmittance.h \
    FitnessAdmittanceBase.h \
    FitnessAreaIntensity.h \
    FitnessTransmittance.h \
    FitnessBase.h \
    FitnessBirefPhaseDiff.h \
    FitnessData.h \
    FitnessFilter.h \
    FitnessFunction.h \
    FitnessInPhaseLine.h \
    FitnessIntensityLine.h \
    FitnessPartResult.h \
    FitnessPhaseShift.h \
    FitnessPhaseShiftChange.h \
    FitnessPlaneWaveBase.h \
    FitnessPoint.h \
    FunctionPoint.h \
    Gene.h \
    GeneBitsSpecDouble.h \
    GeneBitsSpecInteger.h \
    GeneBitsSpecLogDouble.h \
    GeneFactory.h \
    GeneTemplate.h \
    GeneticSearch.h \
    Individual.h \
    LayerGene.h \
    LayerPresentGene.h \
    LayerSpacingGene.h \
    LayerSymmetryGene.h \
    LensDimensionsGene.h \
    LensSquareGene.h \
    LongLensSquareGene.h \
    OneDFitnessData.h \
    Population.h \
    Random.h \
    RepeatCellGene.h \
    SquarePlateGene.h \
    TransLensSquareGene.h \
    TwoDFitnessData.h \
    UnitCellLayerGene.h

unix {cpp
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Gs.pro

DISTFILES += \
    Gs.pro.user \
    CMakeLists.txt \
    Makefile
