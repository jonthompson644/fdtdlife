/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEBITSSPECDOUBLE_H
#define FDTDLIFE_GENEBITSSPECDOUBLE_H

#include <cstddef>
#include <cstdint>
namespace xml {class DomObject;}

namespace gs {

    class Chromosome;

    // A class the represents the gene bits specification for a double
    // with a min and a max range.
    class GeneBitsSpecDouble {
    public:
        // Construction
        GeneBitsSpecDouble() = default;
        GeneBitsSpecDouble(size_t bits, double min, double max);
        GeneBitsSpecDouble(const GeneBitsSpecDouble& other);
        // Operators
        GeneBitsSpecDouble& operator=(const GeneBitsSpecDouble& other) = default;
        bool operator==(const GeneBitsSpecDouble& other) const;
        bool operator!=(const GeneBitsSpecDouble& other) const;
        // API
        void writeXml(xml::DomObject& p) const;
        void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneBitsSpecDouble& v) {v.writeXml(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneBitsSpecDouble& v) {v.readXml(o); return o;}
        virtual void encode(Chromosome* c, double value);
        virtual double decode(Chromosome* c);
        // Getters
        size_t bits() const {return bits_;}
        double min() const {return min_;}
        double max() const {return max_;}
        // Setters
        void bits(size_t v) {bits_ = v;}
        void min(double v) {min_ = v;}
        void max(double v) {max_ = v;}
    protected:
        // Members
        size_t bits_ {};
        double min_ {};
        double max_ {};
        // Helpers
        virtual double scaling() const;
    };

}


#endif //FDTDLIFE_GENEBITSDOUBLE_H
