/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETEMPLATE_H
#define FDTDLIFE_GENETEMPLATE_H

#include <Fdtd/PlateAdmittance.h>
#include "GeneBitsSpecDouble.h"
#include "GeneBitsSpecLogDouble.h"
#include "GeneBitsSpecInteger.h"
#include "GeneBitsSpecBoolean.h"
#include <Box/Vector.h>
#include <vector>
namespace xml {class DomObject;}

namespace gs {

    class GeneFactory;

    class Chromosome;

    class Gene;

    // Specifies a gene to create in a chromosome
    class GeneTemplate {
    public:
        enum GeneType {
            geneTypeRepeatCell = 0, geneTypeBinaryCodedPlateDeleted,
            geneTypeSquarePlate, geneTypeDielectricLayer, geneTypeLayerSpacing,
            geneTypeLayerPresent, geneTypeAdmittanceCatLayer, geneTypeLayerSymmetry,
            geneTypeBinaryPlateNxN, geneTypeDuplicateLayer, geneTypeAdmittanceFn,
            geneTypeDielectricBlock, geneTypeTransLensSquare, geneTypeLongLensSquare,
            geneTypeLensDimensions, geneTypeVariable
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneType& v) {o >> (int&)v; return o;}
        static const std::vector<const char*> geneTypeStrings_;
    protected:
        int identifier_ {-1};
        GeneFactory* factory_ {};
        GeneType geneType_ {geneTypeRepeatCell};
        GeneBitsSpecDouble range_;
        size_t sizeBits_ {};
        bool fourFoldSymmetry_ {};
        int bitsPerHalfSide_ {};
        size_t repeatBits_ {};
        int repeatMin_ {};
        size_t catBits_ {};
        GeneBitsSpecDouble realA_;
        GeneBitsSpecLogDouble realB_;
        GeneBitsSpecLogDouble realC_;
        GeneBitsSpecLogDouble realD_;
        GeneBitsSpecLogDouble realE_;
        GeneBitsSpecLogDouble realF_;
        GeneBitsSpecDouble imagA_;
        GeneBitsSpecLogDouble imagB_;
        GeneBitsSpecLogDouble imagC_;
        GeneBitsSpecLogDouble imagD_;
        GeneBitsSpecLogDouble imagE_;
        GeneBitsSpecLogDouble imagF_;
        GeneBitsSpecDouble gFactor_;
        box::Vector<double> center_;
        box::Vector<double> size_;
        GeneBitsSpecDouble permittivity_;
        GeneBitsSpecDouble layerSpacing_;
        GeneBitsSpecDouble unitCell_;
        GeneBitsSpecInteger<size_t> numRows_;
        GeneBitsSpecBoolean hole_;
        int variableId_{-1};
    public:
        GeneTemplate(int identifier, GeneFactory* m, GeneType geneType);
        ~GeneTemplate();
        Gene* create();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneTemplate& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneTemplate& v) {v.readConfig(o); return o;}
        size_t sizeBits() const { return sizeBits_; }
        void set(GeneType t, bool fourFoldSymmetry, int bitsPerHalfSide, size_t repeatBits, int repeatMin,
                 size_t catBits);
        void copyFrom(const GeneTemplate* other);
        // Getters
        GeneType geneType() const { return geneType_; }
        const char* geneTypeText() const;
        const GeneBitsSpecDouble& range() const { return range_; }
        int identifier() const { return identifier_; }
        size_t catBits() const { return catBits_; }
        bool fourFoldSymmetry() const { return fourFoldSymmetry_; }
        size_t repeatBits() const { return repeatBits_; }
        int repeatMin() const { return repeatMin_; }
        int bitsPerHalfSide() const { return bitsPerHalfSide_; }
        int variableId() const {return variableId_;}
        const GeneBitsSpecDouble& realA() const { return realA_; }
        const GeneBitsSpecLogDouble& realB() const { return realB_; }
        const GeneBitsSpecLogDouble& realC() const { return realC_; }
        const GeneBitsSpecLogDouble& realD() const { return realD_; }
        const GeneBitsSpecLogDouble& realE() const { return realE_; }
        const GeneBitsSpecLogDouble& realF() const { return realF_; }
        const GeneBitsSpecDouble& imagA() const { return imagA_; }
        const GeneBitsSpecLogDouble& imagB() const { return imagB_; }
        const GeneBitsSpecLogDouble& imagC() const { return imagC_; }
        const GeneBitsSpecLogDouble& imagD() const { return imagD_; }
        const GeneBitsSpecLogDouble& imagE() const { return imagE_; }
        const GeneBitsSpecLogDouble& imagF() const { return imagF_; }
        const GeneBitsSpecDouble& gFactor() const { return gFactor_; }
        const GeneBitsSpecDouble& permittivity() const { return permittivity_; }
        const box::Vector<double>& center() const {return center_;}
        const box::Vector<double>& size() const {return size_;}
        const GeneBitsSpecDouble& unitCell() const {return unitCell_;}
        const GeneBitsSpecDouble& layerSpacing() const {return layerSpacing_;}
        const GeneBitsSpecInteger<size_t>& numRows() const {return numRows_;}
        const GeneBitsSpecBoolean& hole() const {return hole_;}
        // Setters
        void range(const GeneBitsSpecDouble& v) {
            range_ = v;
            calcSizeBits();
        }
        void catBits(size_t v) {
            catBits_ = v;
            calcSizeBits();
        }
        void fourFoldSymmetry(bool v) { fourFoldSymmetry_ = v; }
        void repeatBits(size_t v) {
            repeatBits_ = v;
            calcSizeBits();
        }
        void repeatMin(int v) { repeatMin_ = v; }
        void bitsPerHalfSide(int v) { bitsPerHalfSide_ = v; }
        void realA(const GeneBitsSpecDouble& v) {
            realA_ = v;
            calcSizeBits();
        }
        void realB(const GeneBitsSpecLogDouble& v) {
            realB_ = v;
            calcSizeBits();
        }
        void realC(const GeneBitsSpecLogDouble& v) {
            realC_ = v;
            calcSizeBits();
        }
        void realD(const GeneBitsSpecLogDouble& v) {
            realD_ = v;
            calcSizeBits();
        }
        void realE(const GeneBitsSpecLogDouble& v) {
            realE_ = v;
            calcSizeBits();
        }
        void realF(const GeneBitsSpecLogDouble& v) {
            realF_ = v;
            calcSizeBits();
        }
        void imagA(const GeneBitsSpecDouble& v) {
            imagA_ = v;
            calcSizeBits();
        }
        void imagB(const GeneBitsSpecLogDouble& v) {
            imagB_ = v;
            calcSizeBits();
        }
        void imagC(const GeneBitsSpecLogDouble& v) {
            imagC_ = v;
            calcSizeBits();
        }
        void imagD(const GeneBitsSpecLogDouble& v) {
            imagD_ = v;
            calcSizeBits();
        }
        void imagE(const GeneBitsSpecLogDouble& v) {
            imagE_ = v;
            calcSizeBits();
        }
        void imagF(const GeneBitsSpecLogDouble& v) {
            imagF_ = v;
            calcSizeBits();
        }
        void gFactor(const GeneBitsSpecDouble& v) {
            gFactor_ = v;
            calcSizeBits();
        }
        void permittivity(const GeneBitsSpecDouble& v) {
            permittivity_ = v;
            calcSizeBits();
        }
        void unitCell(const GeneBitsSpecDouble& v) {
            unitCell_ = v;
            calcSizeBits();
        }
        void layerSpacing(const GeneBitsSpecDouble& v) {
            layerSpacing_ = v;
            calcSizeBits();
        }
        void numRows(const GeneBitsSpecInteger<size_t>& v) {
            numRows_ = v;
            calcSizeBits();
        }
        void hole(const GeneBitsSpecBoolean& v) {
            hole_ = v;
            calcSizeBits();
        }
        void center(const box::Vector<double>& v) {center_ = v;}
        void size(const box::Vector<double>& v) {size_ = v;}
        void variableId(int v) {variableId_ = v;}
    protected:
        void calcSizeBits();
    };

}


#endif //FDTDLIFE_GENETEMPLATE_H
