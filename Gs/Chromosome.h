/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CHROMOSOME_H
#define FDTDLIFE_CHROMOSOME_H

#include <vector>
#include <list>
#include <cmath>
#include <random>
namespace fdtd { class Model; }

namespace gs {

    class Gene;
    class Random;
    class GeneticSearch;

    // Represents a chromosome for the genetic algorithm
    class Chromosome {
    public:
        Chromosome();
        Chromosome(Random* random, size_t numGenes);
        static Chromosome* breedSingle(Chromosome* mother, Chromosome* father, size_t crossOver);
        static Chromosome* breedUniform(Chromosome* mother, Chromosome* father, Random* random);
        explicit Chromosome(const char* data);
        explicit Chromosome(const std::vector<uint8_t>& data);
        ~Chromosome();
        void encode(uint64_t d, size_t bits);
        uint64_t decode(size_t bits);
        bool decodeBitsLeft(size_t bits);
        void initialise();
        size_t size();
        std::string dataDisplay() const;
        size_t mutate(double probability, Random* random);
        double configureModel(GeneticSearch* m, int what);
        std::list<Gene*>& genes() {return genes_;}
        void clearGenes();
        void clearData();
        void addGene(Gene* g);
        void connectGenes(GeneticSearch* s);
        bool isViable() const {return viable_;}
        size_t distanceBetween(Chromosome* other);
        size_t currentPos() const {return bytePos_*bitsPerByte_ + bitPos_;}
        std::vector<uint8_t>& data() {return data_;}
        Gene* getGene(int index);

    protected:
        uint8_t& dataAccess(size_t i);
    protected:
        std::list<Gene*> genes_;
        std::vector<uint8_t> data_;
        size_t bytePos_;
        size_t bitPos_;
        bool viable_;
    public:
        static constexpr size_t bitsPerByte_ = 8;
    };

}

#endif //FDTDLIFE_CHROMOSOME_H
