/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include <tuple>
#include "Population.h"
#include "Individual.h"
#include "GeneticSearch.h"
#include "UnitCellLayerGene.h"
#include <Fdtd/Model.h>
#include <Xml/DomObject.h>

// Constructor that creates an initial population for the genetic algorithm run
gs::Population::Population(size_t populationSize, GeneticSearch* s) :
        s_(s) {
    // Generate the initial population
    generateInitialPopulation(populationSize, s);
}

// Destructor
gs::Population::~Population() {
    // Delete the population
    clear();
}

// Generate an initial random population
void gs::Population::generateInitialPopulation(size_t populationSize,
                                               GeneticSearch* m) {
    clear();
    if(m->configurationValid()) {
        while(individuals_.size() < populationSize) {
            // Make the individual
            auto individual = std::make_shared<Individual>(nextId_++, &random_,
                                                           m->geneFactory()->sizeBits(), m);
            if(individual->decodeGenes(m)) {
                addIndividual(individual);
            }
        }
    }
    eliteIndividual_ = -1;
}

// Select the breeding population using the fitness proportional scheme
void gs::Population::selectFitnessProportional(
        std::set<std::shared_ptr<Individual>>& breedingPool,
        int breedingPoolSize) {
    breedingPoolSize = std::min(breedingPoolSize, (int) individuals_.size());
    while((int) breedingPool.size() < breedingPoolSize) {
        // Calculate the current total fitness of the remaining population
        double totalFitness = 0.0;
        for(auto& i : individuals_) {
            totalFitness += i->fitness();
        }
        // Choose an individual from them
        double random = random_.get(totalFitness);
        double fitness = 0;
        std::shared_ptr<Individual> parent;
        for(auto& i : individuals_) {
            if(random >= fitness && random < (fitness + i->fitness())) {
                // This is the one to select
                parent = i;
                break;
            } else {
                fitness += i->fitness();
            }
        }
        // Place into the breeding pool
        breedingPool.insert(parent);
    }
}

// Select the breeding population using the tournament scheme
void gs::Population::selectTournament(std::set<std::shared_ptr<Individual>>& breedingPool,
                                      int breedingPoolSize, int tournamentSize) {
    breedingPoolSize = std::min(breedingPoolSize, (int) individuals_.size());
    tournamentSize = std::max(tournamentSize, 1);
    tournamentSize = std::min(tournamentSize, (int) individuals_.size());
    while((int) breedingPool.size() < breedingPoolSize) {
        // Select an individual using the tournament method
        std::shared_ptr<Individual> parent;
        for(int i = 0; i < tournamentSize; i++) {
            auto random = (int) random_.get(individuals_.size());
            auto pos = individuals_.begin();
            for(int j = 0; j < random; j++) {
                pos++;
            }
            if(parent == nullptr || (*pos)->fitness() > parent->fitness()) {
                parent = *pos;
            }
        }
        // Place into the breeding pool (make sure it is not already present)
        breedingPool.insert(parent);
    }
}

// Breed a new generation
void gs::Population::breedNewGeneration(size_t populationSize,
                                        double crossOverProbability,
                                        double mutationProbability,
                                        GeneticSearch::Selection selectionMode,
                                        int tournamentSize, int breedingPoolSize,
                                        bool keepParents,
                                        GeneticSearch::Crossover crossoverMode,
                                        bool microGa, GeneticSearch* m) {
    if(microGa) {
        breedMicroGa(populationSize, crossOverProbability, selectionMode,
                     tournamentSize, crossoverMode, m);
    } else {
        breedRegularGa(populationSize, crossOverProbability, mutationProbability,
                       selectionMode,
                       tournamentSize, breedingPoolSize, keepParents, crossoverMode, m);
    }
}

// Breed a new generation
void gs::Population::breedRegularGa(size_t populationSize,
                                    double crossOverProbability,
                                    double mutationProbability,
                                    GeneticSearch::Selection selectionMode,
                                    int tournamentSize, int breedingPoolSize,
                                    bool keepParents,
                                    GeneticSearch::Crossover /*crossoverMode*/,
                                    GeneticSearch* m) {
    std::list<std::shared_ptr<Individual>> newPopulation;
    // Select the breeding pool
    std::set<std::shared_ptr<Individual>> breedingPool;
    switch(selectionMode) {
        case GeneticSearch::selectionFitnessProportional:
            selectFitnessProportional(breedingPool, breedingPoolSize - 1);
            break;
        case GeneticSearch::selectionTournament:
            selectTournament(breedingPool, breedingPoolSize - 1,
                             tournamentSize);
            break;
    }
    // Introduce fresh blood
    while((int) breedingPool.size() < breedingPoolSize) {
        auto c = std::make_shared<Individual>(nextId_++, &random_,
                                              m->geneFactory()->sizeBits(), m);
        if(c->decodeGenes(m)) {
            breedingPool.insert(c);
        }
    }
    // If we are keeping the parents, put them straight into the new generation
    if(keepParents) {
        for(auto& ind : breedingPool) {
            newPopulation.push_back(ind);
        }
    }
    // Create the children
    auto pos = breedingPool.begin();
    while(newPopulation.size() < populationSize) {
        // Select a pair of individuals to breed
        std::shared_ptr<Individual> a = *pos;
        pos++;
        if(pos == breedingPool.end()) {
            pos = breedingPool.begin();
        }
        std::shared_ptr<Individual> b = *pos;
        pos++;
        if(pos == breedingPool.end()) {
            pos = breedingPool.begin();
        }
        // Decide which way round the parents are
        if(random_.flip(0.5)) {
            std::shared_ptr<Individual> t = a;
            a = b;
            b = t;
        }
        // Breed them until we have a valid child
        std::shared_ptr<Individual> c = nullptr;
        while(c == nullptr) {
            bool sameAsParent = true;
            // Work out the cross over point
            size_t chromoSize = std::min(a->chromosome()->size(), b->chromosome()->size());
            size_t crossPoint = chromoSize;
            if(random_.flip(crossOverProbability)) {
                crossPoint = random_.get(chromoSize);
                sameAsParent = false;
            }
            // Make a child
            c = Individual::breedSingleCrossover(nextId_++, a, b, crossPoint);
            if(c->mutate(mutationProbability, &random_) > 0) {
                sameAsParent = false;
            }
            if(!c->decodeGenes(m)) {
                c = nullptr;
            } else if(sameAsParent) {
                c->copyEvaluation(a);
            }
        }
        // Add the child to the population
        newPopulation.push_back(c);
    }
    // Kill off the old population
    individuals_.clear();
    // Record the new population
    individuals_ = newPopulation;
}

// Breed a new generation
void gs::Population::breedMicroGa(size_t populationSize,
                                  double crossOverProbability,
                                  GeneticSearch::Selection selectionMode,
                                  int tournamentSize,
                                  GeneticSearch::Crossover crossoverMode,
                                  GeneticSearch* model) {
    std::list<std::shared_ptr<Individual>> newPopulation;
    // Find the elite member of the current population
    std::shared_ptr<Individual> eliteMember = nullptr;
    for(auto& i : individuals_) {
        if(eliteMember == nullptr || i->fitness() > eliteMember->fitness()) {
            eliteMember = i;
        }
        i->decodeGenes(model);
    }
    newPopulation.push_back(eliteMember);
    eliteIndividual_ = eliteMember->id();
    // Is the population too in-bred?
    size_t maxDistance = 0;
    for(auto& i : individuals_) {
        maxDistance = std::max(maxDistance, i->distanceBetween(eliteMember));
    }
    size_t chromosomeSize = eliteMember->chromosome()->size();
    bool converged = (double) maxDistance <= (double) chromosomeSize * 5.0 / 100.0;
    // Now breed new children
    std::set<std::shared_ptr<Individual>> breedingPair;
    std::list<std::pair<std::shared_ptr<Individual>, std::shared_ptr<Individual>>> parentsUsed;
    while(newPopulation.size() < populationSize) {
        std::shared_ptr<Individual> c = nullptr;
        if(converged) {
            // Generate a random child until we get a viable one
            while(c == nullptr) {
                c = std::make_shared<Individual>(nextId_++, &random_,
                                                 model->geneFactory()->sizeBits(), model);
                if(!c->decodeGenes(model)) {
                    c = nullptr;
                }
            }
        } else {
            // Select the parents
            bool usedAlready;
            do {
                breedingPair.clear();
                switch(selectionMode) {
                    case GeneticSearch::selectionFitnessProportional:
                        selectFitnessProportional(breedingPair, 2);
                        break;
                    case GeneticSearch::selectionTournament:
                        selectTournament(breedingPair, 2, tournamentSize);
                        break;
                }
                // Has this pair already been used?
                usedAlready = false;
                for(auto& prev : parentsUsed) {
                    if(prev.first == *breedingPair.begin() &&
                       prev.second == *breedingPair.rbegin()) {
                        usedAlready = true;
                        break;
                    } else if(prev.second == *breedingPair.begin() &&
                              prev.first == *breedingPair.rbegin()) {
                        usedAlready = true;
                        break;
                    }
                }
            } while(usedAlready);
            // Use the parents
            std::shared_ptr<Individual> f = *breedingPair.begin();
            std::shared_ptr<Individual> m = *breedingPair.rbegin();
            parentsUsed.emplace_back(std::pair<std::shared_ptr<Individual>,
                    std::shared_ptr<Individual>>(f, m));
            // Breed the child until we get a viable one
            size_t chromoSize;
            size_t crossPoint;
            while(c == nullptr) {
                switch(crossoverMode) {
                    case GeneticSearch::crossoverSingle:
                        // Work out the cross over point
                        chromoSize = std::min(f->chromosome()->size(),
                                              m->chromosome()->size());
                        crossPoint = chromoSize;
                        if(random_.flip(crossOverProbability)) {
                            crossPoint = random_.get(chromoSize);
                        }
                        // Breed
                        c = Individual::breedSingleCrossover(nextId_++, f, m, crossPoint);
                        break;
                    case GeneticSearch::crossoverUniform:
                        // Breed
                        c = Individual::breedUniformCrossover(nextId_++, f, m, &random_);
                        break;
                }
                if(!c->decodeGenes(model)) {
                    c = nullptr;
                }
            }
        }
        newPopulation.push_back(c);
    }
    // Delete the old population
    individuals_.clear();
    // Record the new population
    individuals_ = newPopulation;
}

// Decide whether any of the individuals of the current population
// are fit enough to be regarded as the result.
bool gs::Population::evaluateResult(double unfitnessThreshold) {
    bool result = false;
    // Find the maximum unfitness
    double maxUnfitness = 0.0;
    for(auto& ind : individuals_) {
        maxUnfitness = std::max(maxUnfitness, ind->unfitness());
    }
    // Now convert the unfitness parameters to fitness
    for(auto& ind : individuals_) {
        ind->fitness(maxUnfitness - ind->unfitness());
    }
    // Find the fittest individual
    std::shared_ptr<Individual> fittest = nullptr;
    for(auto& ind : individuals_) {
        if(fittest == nullptr || ind->fitness() > fittest->fitness()) {
            fittest = ind;
            // Is this good enough?
            result = fittest->unfitness() <= unfitnessThreshold;
        }
    }
    return result;
}

// Clear the population
void gs::Population::clear() {
    individuals_.clear();
    nextId_ = 0;
}

// Add an individual to the population
void gs::Population::addIndividual(const std::shared_ptr<Individual>& ind) {
    if(ind->id() >= nextId_) {
        nextId_ = ind->id() + 1;
    }
    individuals_.push_back(ind);
}

// Return the individual in the population with the given identifier
std::shared_ptr<gs::Individual> gs::Population::getIndividual(int id) {
    std::shared_ptr<Individual> result = nullptr;
    for(auto& ind : individuals_) {
        if(ind->id() == id) {
            result = ind;
            break;
        }
    }
    return result;
}

// Calculate some statistics regarding the overall fitness of the population.
void gs::Population::calculateStats(double* min, double* max, double* mean) {
    double sum = 0.0;
    *min = 0.0;
    *max = 0.0;
    *mean = 0.0;
    int numEvaluated = 0;
    for(auto& ind : individuals_) {
        if(ind->isEvaluated()) {
            if(numEvaluated == 0) {
                *min = ind->unfitness();
                *max = ind->unfitness();
            } else {
                *min = std::min(*min, ind->unfitness());
                *max = std::max(*max, ind->unfitness());
            }
            sum += ind->unfitness();
            numEvaluated++;
        }
    }
    if(numEvaluated > 0) {
        *mean = sum / numEvaluated;
    }
}

// Write the population to the XML DOM object
void gs::Population::writePopulation(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& i : individuals_) {
        root << xml::Obj("individual") << *i;
    }
    root << xml::Close();
}

// Read in the current population
void gs::Population::readPopulation(xml::DomObject& root) {
    // Remove any existing population
    clear();
    // Read the individuals
    for(auto& o : root.obj("individual")) {
        std::shared_ptr<Individual> ind = Individual::factory(*o, s_);
        *o >> *ind;
        addIndividual(ind);
    }
}

