/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_POPULATION_H
#define FDTDLIFE_POPULATION_H

#include <list>
#include <set>
#include <memory>
#include "Random.h"
#include "GeneticSearch.h"
#include <Xml/DomObject.h>

namespace sensor { class ArraySensor; }
namespace fdtd { class Model; }

namespace gs {

    class Individual;

    class FitnessFunction;

    // Represents the current population of individuals
    // in a genetic algorithm run
    class Population {
    public:
        explicit Population(size_t populationSize, GeneticSearch* s);
        ~Population();
        void breedNewGeneration(size_t populationSize, double crossOverProbability,
                                double mutationProbability,
                                GeneticSearch::Selection selectionMode,
                                int tournamentSize, int breedingPoolSize, bool keepParents,
                                GeneticSearch::Crossover crossoverMode, bool microGa,
                                GeneticSearch* m);
        void breedRegularGa(size_t populationSize, double crossOverProbability,
                            double mutationProbability,
                            GeneticSearch::Selection selectionMode,
                            int tournamentSize, int breedingPoolSize, bool keepParents,
                            GeneticSearch::Crossover crossoverMode,
                            GeneticSearch* m);
        void breedMicroGa(size_t populationSize, double crossOverProbability,
                          GeneticSearch::Selection selectionMode,
                          int tournamentSize,
                          GeneticSearch::Crossover crossoverMode,
                          GeneticSearch* m);
        bool evaluateResult(double unfitnessThreshold);
        void clear();
        std::shared_ptr<Individual> getIndividual(int id);
        void addIndividual(const std::shared_ptr<Individual>& ind);
        void generateInitialPopulation(size_t populationSize, GeneticSearch* m);
        void writePopulation(xml::DomObject& root) const;
        void readPopulation(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Population& p) {
            p.writePopulation(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Population& p) {
            p.readPopulation(o);
            return o;
        }
    protected:
        std::list<std::shared_ptr<Individual>> individuals_;
        int currentIndividual_{-1};
        int eliteIndividual_{-1};
        Random random_;
        int nextId_{0};
        GeneticSearch* s_{nullptr};
    protected:
        void selectFitnessProportional(std::set<std::shared_ptr<Individual>>& breedingPool,
                                       int breedingPoolSize);
        void selectTournament(std::set<std::shared_ptr<Individual>>& breedingPool,
                              int breedingPoolSize, int tournamentSize);
    public:
        std::list<std::shared_ptr<Individual>>& individuals() { return individuals_; }
        void calculateStats(double* min, double* max, double* mean);
        int currentIndividual() const { return currentIndividual_; }
        int eliteIndividual() const { return eliteIndividual_; }
        void currentIndividual(int ind) { currentIndividual_ = ind; }
    };

}


#endif //FDTDLIFE_POPULATION_H
