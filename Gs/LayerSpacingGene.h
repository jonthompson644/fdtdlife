/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LAYERSPACINGGENE_H
#define FDTDLIFE_LAYERSPACINGGENE_H

#include "Gene.h"
#include "GeneBitsSpecDouble.h"

namespace gs {

    // Represents the spacing between layers
    class LayerSpacingGene : public Gene {
    protected:
        // Evolving parts
        double layerSpacing_;   // The spacing between layers
        // Template parts
        GeneBitsSpecDouble layerSpacingSpec_;  // The specification for the layer spacing gene
        // Other
        static constexpr double defaultLayerSpacing_ = 0.0005;
    public:
        explicit LayerSpacingGene(const GeneBitsSpecDouble& layerSpacingSpec);
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        double layerSpacing() const {return layerSpacing_;}
        void layerSpacing(double v) {layerSpacing_ = v;}
        size_t sizeBits() const override;
    };
}


#endif //FDTDLIFE_LAYERSPACINGGENE_H
