/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSINPHASELINE_H
#define FDTDLIFE_FITNESSINPHASELINE_H

#include "FitnessBase.h"
#include <Box/Constants.h>
namespace sensor {class ArraySensor;}
namespace source {class ZoneSource;}

namespace gs {

    // A fitness function that wants all the points on a line sensorId
    // to be in phase at a particular frequency
    class FitnessInPhaseLine : public FitnessBase {
    public:
        // Overrides of FitnessBase
        double unfitness(Individual* ind) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void initialise(size_t numFrequencies, double frequencyStart,
                        double frequencySpacing) override;

        // Getters
        double frequency() const {return frequency_;}
        size_t numPointsX() const {return numPointsX_;}
        double desiredAmplitude() const {return desiredAmplitude_;}
        double amplitudeWeight() const {return amplitudeWeight_;}

        // Setters
        void frequency(double frequency) {frequency_ = frequency;}
        void numPointsX(size_t v) {numPointsX_ = v;}
        void desiredAmplitude(double v) {desiredAmplitude_ = v;}
        void amplitudeWeight(double v) {amplitudeWeight_ = v;}

    protected:
        // Configuration
        double frequency_ {1 * box::Constants::giga_};  // The frequency at which to require constant phase
        size_t numPointsX_ {1};  // The number of points on the array sensorId in the X direction
        double desiredAmplitude_ {1.0};  // The amplitude against which to compare the points
        double amplitudeWeight_ {1.0};  // The weighting to give the amplitude in the unfitness calc

        // Helpers
        sensor::ArraySensor* getSensor();
        double unfitnessPhase(const std::vector<double>& phase);
        double unfitnessAmplitude(const std::vector<double>& amplitude);
    };
}


#endif //FDTDLIFE_FITNESSINPHASELINE_H
