/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DIELECTRICBLOCKGENE_H
#define FDTDLIFE_DIELECTRICBLOCKGENE_H

#include "Gene.h"
#include <Box/Vector.h>
#include "GeneBitsSpecDouble.h"

namespace gs {
    // A gene that defines a dielectric block
    class DielectricBlockGene : public Gene {
    public:
        // Construction
        explicit DielectricBlockGene(const box::Vector<double>& center,
                                     const box::Vector<double>& size,
                                     const GeneBitsSpecDouble& permittivitySpec);

        // Overrides of Gene
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        double configureModel(GeneticSearch* s, unsigned int what) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
        size_t sizeBits() const override;

        // Getters
        double permittivity() const {return permittivity_;}
        void permittivity(double v) {permittivity_ = v;}

    protected:
        // Configuration
        box::Vector<double> center_;  // The center of the block
        box::Vector<double> size_;  // The size of the block
        GeneBitsSpecDouble permittivitySpec_;  // Refractive index config

        // Evolving parts
        double permittivity_;  // The current relative permittivity

        // Helpers
        void makeBlock(GeneticSearch* s, const box::Vector<double>& center, int mat);
    };
}


#endif //FDTDLIFE_DIELECTRICBLOCKGENE_H
