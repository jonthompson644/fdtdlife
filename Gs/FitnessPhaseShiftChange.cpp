/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessPhaseShiftChange.h"
#include <Fdtd/Model.h>
#include <Fdtd/Configuration.h>
#include "GeneticSearch.h"
#include "OneDFitnessData.h"
#include "Individual.h"
#include <Sensor/ArraySensor.h>

// Second stage constructor
void gs::FitnessPhaseShiftChange::construct(int mode, int id, GeneticSearch* m) {
    FitnessAdmittanceBase::construct(mode, id, m);
    name_ = "Phase Shift Change";
    rangeMin_ = -10;
    rangeMax_ = +10;
}

// Calculate the unfitness parameter for the specified data.
// The bigger this number, the less fit is the solution.
// We will use the sum of the square of the difference.
// This penalises a few large excursions from the desired over
// many small excursions.  This overridden version handles the
// 360 degree wrap round needed for phase data.
double gs::FitnessPhaseShiftChange::unfitness(Individual* ind) {
    double result = 0.0;
    auto sensor = s_->getGeneticSensor();
    if(sensor != nullptr) {
        // Create the data object
        std::string dataName;
        if(dataSource_ == dataSourceYComponent) {
            dataName = "PhaseShiftY";
        } else {
            dataName = "PhaseShiftX";
        }
        auto data = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseShift, dataName));
        data->setSize(min_.size());
        // Copy the data
        for(size_t i = 0; i < min_.size(); i++) {
            double x, y;
            sensor->points().at(sensor->index(0, 0)).phaseShiftAt(i, x, y, false);
            if(dataSource_ == dataSourceYComponent) {
                data->setData(i, y);
            } else {
                data->setData(i, x);
            }
        }
        // Calculate the derivative and assess the fitness
        std::vector<double> derivative;
        derivative.resize(min_.size());
        // Need at least two points to calculate a derivative
        if(min_.size() >= 2) {
            // Fill the derivatives array
            for(size_t i = 0; i < min_.size(); i++) {
                double d1;
                double d2;
                if(i == 0) {
                    // First point, use asymmetric difference
                    d1 = data->data().at(i);
                    d2 = data->data().at(i + 1);
                    derivative[i] = diff(d1, d2) / s_->m()->p()->dt();
                } else if((i + 1) >= min_.size()) {
                    // Last point, use asymmetric difference
                    d1 = data->data().at(i - 1);
                    d2 = data->data().at(i);
                    derivative[i] = diff(d1, d2) / s_->m()->p()->dt();
                } else {
                    // Middle point, use symmetric difference
                    d1 = data->data().at(i - 1);
                    d2 = data->data().at(i + 1);
                    derivative[i] = diff(d1, d2) / s_->m()->p()->dt() / 2.0;
                }
            }
            // Now calculate the unfitness using the derivatives
            for(size_t i = 0; i < min_.size(); i++) {
                double minRequired = min_[i];
                double maxRequired = max_[i];
                double measured = derivative[i];
                if(measured < minRequired) {
                    result += (measured - minRequired) * (measured - minRequired);
                } else if(measured > maxRequired) {
                    result += (measured - maxRequired) * (measured - maxRequired);
                }
            }
        }
        result *= weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Calculate the difference between two phase shift angles that have the range +/-180
double gs::FitnessPhaseShiftChange::diff(double a, double b) {
    double result;
    if(a <= b) {
        double diff = b - a;
        if(diff < (360.0 - diff)) {
            result = diff;
        } else {
            result = -(360.0 - diff);
        }
    } else {
        double diff = a - b;
        if(diff < (360.0 - diff)) {
            result = -diff;
        } else {
            result = 360.0 - diff;
        }
    }
    return result;
}

