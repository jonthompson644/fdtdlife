/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEBITSSPECINTEGER_H
#define FDTDLIFE_GENEBITSSPECINTEGER_H

#include <cstddef>
#include <cstdint>
namespace xml {class DomObject;}

namespace gs {

    class Chromosome;

    // A class the represents the gene bits specification for an integer
    // from a minimum.
    template <class T>
    class GeneBitsSpecInteger {
    public:
        // Construction
        GeneBitsSpecInteger() = default;
        GeneBitsSpecInteger(size_t bits, T min);
        GeneBitsSpecInteger(const GeneBitsSpecInteger& other);
        // Operators
        GeneBitsSpecInteger& operator=(const GeneBitsSpecInteger& other) = default;
        bool operator==(const GeneBitsSpecInteger& other) const;
        bool operator!=(const GeneBitsSpecInteger& other) const;
        // API
        void writeXml(xml::DomObject& p) const;
        void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneBitsSpecInteger& v) {v.writeXml(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneBitsSpecInteger& v) {v.readXml(o); return o;}
        void encode(Chromosome* c, T value);
        T decode(Chromosome* c);
        // Getters
        size_t bits() const {return bits_;}
        T min() const {return min_;}
        // Setters
        void bits(size_t v) {bits_ = v;}
        void min(T v) {min_ = v;}
    protected:
        // Members
        size_t bits_ {};
        T min_ {};
    };

}


#endif //FDTDLIFE_GENEBITSSPECINTEGER_H
