/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessFilter.h"
#include "Individual.h"
#include <Box/Constants.h>
#include <iostream>

// Write the configuration to the DOM object
void gs::FitnessFilter::writeConfig(xml::DomObject& root) const {
    // Base class first
    FitnessBase::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("filtertype") << filterType_;
    root << xml::Obj("filtermode") << filterMode_;
    root << xml::Obj("order") << order_;
    root << xml::Obj("cutofffrequency") << cutOffFrequency_;
    root << xml::Obj("maxfrequency") << maxFrequency_;
    root << xml::Obj("passbandripple") << passbandRipple_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessFilter::readConfig(xml::DomObject& root) {
    // Base class first
    FitnessBase::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("filtertype") >> filterType_;
    root >> xml::Obj("filtermode") >> xml::Default(static_cast<int>(FilterMode::lowPass))
         >> filterMode_;
    root >> xml::Obj("order") >> order_;
    root >> xml::Obj("cutofffrequency") >> cutOffFrequency_;
    root >> xml::Obj("maxfrequency") >> maxFrequency_;
    root >> xml::Obj("passbandripple") >> xml::Default(1.0) >> passbandRipple_;
    root >> xml::Close();
}

// Initialise the fitness
void gs::FitnessFilter::initialise(size_t numSamples, double firstSample,
                                   double sampleSpacing) {
    // Base class first
    FitnessAdmittanceBase::initialise(numSamples, firstSample, sampleSpacing);
    // Now over-write the min/max using the appropriate filter equation
    min_.resize(numSamples);
    max_.resize(numSamples);
    for(size_t i = 0; i < numSamples; i++) {
        double frequency = firstSample + i * sampleSpacing;
        if(frequency < maxFrequency_) {
            std::complex<double> value;
            std::complex<double> a = {0.0, frequency / cutOffFrequency_};
            switch(filterMode_) {
                case FilterMode::lowPass:
                    a = {0.0, frequency / cutOffFrequency_};
                    break;
                case FilterMode::highPass:
                    a = {0.0, cutOffFrequency_ / frequency};
                    break;
            }
            switch(filterType_) {
                case FilterType::butterworth: {
                    size_t limit;
                    if(order_ % 2) {
                        // Odd numbered order
                        value = a + 1.0;
                        limit = (order_ - 1U) / 2U;
                    } else {
                        // Even numbered order
                        value = 1.0;
                        limit = order_ / 2U;
                    }
                    for(size_t k = 1; k <= limit; k++) {
                        value *= (a * a - a * 2.0 *
                                          cos((2.0 * k + order_ - 1.0) /
                                              2.0 / order_ * box::Constants::pi_) + 1.0);
                    }
                    break;
                }
                case FilterType::chebyshevI: {
                    std::complex<double> epsilon = std::sqrt(
                            std::pow(10.0, 0.1 * passbandRipple_) -
                            std::complex<double>(1.0, 0.0));
                    value = std::pow(2.0, order_ - 1.0) * epsilon;
                    std::complex<double> h = 1.0 / order_ * std::asinh(1.0 / epsilon);
                    for(size_t m = 1; m <= order_; m++) {
                        double theta = (2.0 * m - 1.0) * box::Constants::pi_ / order_ / 2.0;
                        std::complex<double> term = a;
                        term += std::sinh(h) * sin(theta);
                        term -= std::cosh(h) * cos(theta) * std::complex<double>(0.0, 1.0);
                        value *= term;
                    }
                    break;
                }
                default:
                    break;
            }
            value = 1.0 / value;
            switch(property_) {
                case Property::transmittance:
                    max_[i] = min_[i] = std::abs(value) * std::abs(value);
                    break;
                case Property::phaseShift:
                    max_[i] = min_[i] = std::arg(value) / box::Constants::deg2rad_;
                    break;
            }
        } else {
            switch(property_) {
                case Property::transmittance:
                    max_[i] = 10.0;
                    min_[i] = 0.0;
                    break;
                case Property::phaseShift:
                    max_[i] = 180.0;
                    min_[i] = -180.0;
                    break;
            }
        }
    }
}

// Fill the string with TSV data of the fitness function
std::string gs::FitnessFilter::makeTsvData(size_t numFrequencies, double firstFrequency,
                                           double frequencySpacing) {
    initialise(numFrequencies, firstFrequency, frequencySpacing);
    std::stringstream t;
    t << "Frequency\tMin\tMax" << std::endl;
    double frequency = firstFrequency;
    for(size_t i = 0; i < min_.size(); i++) {
        t << frequency << "\t" << min_[i] << "\t" << max_[i] << std::endl;
        frequency += frequencySpacing;
    }
    return t.str();
}


