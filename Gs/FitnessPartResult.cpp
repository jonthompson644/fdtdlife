/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessPartResult.h"
#include <Xml/DomObject.h>

// Initialising constructor
gs::FitnessPartResult::FitnessPartResult(int position, const char* partType, double result) :
        position_(position),
        partType_(partType),
        result_(result),
        extraValue_(0.0) {

}

// Default constructor
gs::FitnessPartResult::FitnessPartResult() :
        position_(0),
        result_(-1.0),
        extraValue_(0.0) {
}

// Copy constructor
gs::FitnessPartResult::FitnessPartResult(const FitnessPartResult& other) :
        FitnessPartResult() {
    *this = other;
}

// Assignment operator
gs::FitnessPartResult& gs::FitnessPartResult::operator=(const FitnessPartResult& other) {
    position_ = other.position_;
    partType_ = other.partType_;
    result_ = other.result_;
    extraInfo_ = other.extraInfo_;
    extraValue_ = other.extraValue_;
    return *this;
}

// Write the object to the DOM
void gs::FitnessPartResult::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("position") << position_;
    root << xml::Obj("parttype") << partType_;
    root << xml::Obj("result") << result_;
    root << xml::Obj("extrainfo") << extraInfo_;
    root << xml::Obj("extravalue") << extraValue_;
    root << xml::Close();
}

// Read the object from the DOM
void gs::FitnessPartResult::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("position") >> position_;
    root >> xml::Obj("parttype") >> partType_;
    root >> xml::Obj("result") >> result_;
    root >> xml::Obj("extrainfo") >> xml::Default(extraInfo_) >> extraInfo_;
    root >> xml::Obj("extravalue") >> xml::Default(extraValue_) >> extraValue_;
    root >> xml::Close();
}

// Set extra information in the result
void gs::FitnessPartResult::setExtra(const std::string& extraInfo, double extraValue) {
    extraInfo_ = extraInfo;
    extraValue_ = extraValue;
}


