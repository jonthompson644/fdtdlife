/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LENSSQUAREGENE_H
#define FDTDLIFE_LENSSQUAREGENE_H

#include "Gene.h"
#include <Box/Polynomial.h>
#include "GeneBitsSpecDouble.h"
#include "GeneBitsSpecLogDouble.h"

namespace gs {

    class LensDimensionsGene;

    // Base class for genes that evolve the size of the squares (usually in a lens structure)
    // in an entire row or column.
    class LensSquareGene : public Gene {
    protected:
        // Evolving parts
        box::Polynomial squareSize_;
        // Template parts
        GeneBitsSpecDouble a_;
        GeneBitsSpecLogDouble b_;
        GeneBitsSpecLogDouble c_;
        GeneBitsSpecLogDouble d_;
        GeneBitsSpecLogDouble e_;
        GeneBitsSpecLogDouble f_;
        // Connections
        LensDimensionsGene* lensDimensions_ {};
    public:
        LensSquareGene(GeneTemplate::GeneType geneType, const GeneBitsSpecDouble& a,
                              const GeneBitsSpecLogDouble& b,
                              const GeneBitsSpecLogDouble& c,
                              const GeneBitsSpecLogDouble& d,
                              const GeneBitsSpecLogDouble& e,
                              const GeneBitsSpecLogDouble& f);
        void encode(Chromosome* c) override;
        void decode(Chromosome* c) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
        size_t sizeBits() const override;
        // Getters
        const box::Polynomial& squareSize() const { return squareSize_; }
        // Setters
        void squareSize(const box::Polynomial& v) { squareSize_ = v; }
    };

}


#endif //FDTDLIFE_LENSSQUAREGENE_H
