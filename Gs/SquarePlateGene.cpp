/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlateGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Domain/Material.h>
#include <Domain/SquarePlate.h>
#include <Domain/SquareHole.h>
#include <Box/Constants.h>
#include <sstream>

// Constructor
gs::SquarePlateGene::SquarePlateGene(const GeneBitsSpecDouble& plateSizeSpec,
                                     const GeneBitsSpecBoolean& holeSpec,
                                     size_t repeatBits, int repeatMin) :
        UnitCellLayerGene(GeneTemplate::geneTypeSquarePlate, repeatBits, repeatMin),
        plateSize_(0.0),
        hole_(false),
        plateSizeSpec_(plateSizeSpec),
        holeSpec_(holeSpec) {
}

// Encode the allele into the chromosome
void gs::SquarePlateGene::encode(Chromosome* c) {
    UnitCellLayerGene::encode(c);
    plateSizeSpec_.encode(c, plateSize_);
    holeSpec_.encode(c, hole_);
}

// Decode the allele from the chromosome
void gs::SquarePlateGene::decode(Chromosome* c) {
    UnitCellLayerGene::decode(c);
    plateSize_ = plateSizeSpec_.decode(c);
    hole_ = holeSpec_.decode(c);
}

// Connect the gene to other genes it depends on
bool gs::SquarePlateGene::connect(Chromosome* c, GeneticSearch* s) {
    bool result = UnitCellLayerGene::connect(c, s);
    // Validate myself (nothing yet)
    return result;
}

// Set the model configuration corresponding to this allele
double gs::SquarePlateGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(present_ && (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_;
        makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_;
            makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Set the model configuration corresponding to this allele
void gs::SquarePlateGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
                                    double zLocation) {
    // Set the parameters of the material
    domain::Material* mat = material(s);
    mat->epsilon(box::Expression(box::Constants::epsilonMetal_));   // A metallic epsilon
    mat->mu(box::Expression(box::Constants::mu0_));
    // Find or create the shape
    int shapeId = s->layerShapeId(layer);
    std::stringstream name;
    name << "GeneticLayer" << layer;
    domain::Shape* shape = s->m()->d()->getShape(shapeId);
    if(hole_) {
        auto plate = dynamic_cast<domain::SquareHole*>(shape);
        if(plate == nullptr) {
            s->m()->d()->remove(shape);
            plate = dynamic_cast<domain::SquareHole*>(s->m()->d()->makeShape(
                    shapeId, domain::Domain::shapeModeSquareHole));
        }
        plate->material(mat->index());
        if((what & GeneticSearch::individualLoadDomainSize) != 0) {
            plate->cellSizeX(box::Expression(unitCell_));
        } else {
            plate->cellSizeX(box::Expression(s->m()->p()->dr().x().value() / (double) repeat_));
        }
        plate->holeSize(box::Expression(plateSize_));
        plate->name(name.str());
        plate->center().value(box::Vector<double>(0.0, 0.0, zLocation));
        plate->repeatX(box::Expression(repeat_));
        plate->repeatY(box::Expression(repeat_));
        plate->seqGenerated(true);
    } else {
        auto plate = dynamic_cast<domain::SquarePlate*>(shape);
        if(plate == nullptr) {
            s->m()->d()->remove(shape);
            plate = dynamic_cast<domain::SquarePlate*>(s->m()->d()->makeShape(
                    shapeId, domain::Domain::shapeModeSquarePlate));
        }
        plate->material(mat->index());
        if((what & GeneticSearch::individualLoadDomainSize) != 0) {
            plate->cellSizeX(box::Expression(unitCell_));
        } else {
            plate->cellSizeX(box::Expression(s->m()->p()->dr().x().value() / (double) repeat_));
        }
        plate->plateSize(box::Expression(plateSize_));
        plate->name(name.str());
        plate->center().value(box::Vector<double>(0.0, 0.0, zLocation));
        plate->repeatX(box::Expression(repeat_));
        plate->repeatY(box::Expression(repeat_));
        plate->seqGenerated(true);
    }
}

// Return the number of bits used by the gene
size_t gs::SquarePlateGene::sizeBits() const {
    return UnitCellLayerGene::sizeBits() + plateSizeSpec_.bits() + holeSpec_.bits();
}

// Copy the state from the other gene into this one
void gs::SquarePlateGene::copyFrom(Gene* other, fdtd::Model* m) {
    UnitCellLayerGene::copyFrom(other, m);
    auto* from = dynamic_cast<SquarePlateGene*>(other);
    if(from != nullptr) {
        plateSize_ = from->plateSize_;
        hole_ = from->hole_;
    }
}
