/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <cmath>
#include "RepeatCellGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"

// Constructor
gs::RepeatCellGene::RepeatCellGene(const GeneBitsSpecDouble& repeatCellSpec) :
        Gene(GeneTemplate::geneTypeRepeatCell),
        repeatCell_(defaultRepeatCell_),
        repeatCellSpec_(repeatCellSpec),
        active_(false) {
}

// Encode the gene into the chromosome
void gs::RepeatCellGene::encode(Chromosome* c) {
    repeatCellSpec_.encode(c, repeatCell_);
}

// Decode the gene from the chromosome
void gs::RepeatCellGene::decode(Chromosome* c) {
    repeatCell_ = repeatCellSpec_.decode(c);
}

// Set the model configuration corresponding to this gene
double gs::RepeatCellGene::configureModel(GeneticSearch* s, unsigned int what) {
    if(active_ && (what & GeneticSearch::individualLoadDomainSize) != 0) {
        double cellSizeXY = repeatCell_ / s->nCellsXY();
        double gridToTop = gridToTopCells_ * s->cellSizeZ();
        double sensorToBottom = sensorToBottomCells_ * cellSizeXY;
        s->m()->p()->p1({-repeatCell_ / 2.0, -repeatCell_ / 2.0, -gridToTop});
        s->m()->p()->p2({repeatCell_ / 2.0, repeatCell_ / 2.0,
                         s->sensorDistance() + sensorToBottom});
        s->m()->p()->dr({cellSizeXY, cellSizeXY, s->cellSizeZ()});
    }
    return 0.0;
}

// Connect to other genes.  Returns true if everything required exists.
bool gs::RepeatCellGene::connect(Chromosome* c, GeneticSearch* /*s*/) {
    // Are we the first repeat cell gene?
    active_ = false;
    for(auto gene : c->genes()) {
        auto otherGlobal = dynamic_cast<RepeatCellGene*>(gene);
        if(otherGlobal != nullptr) {
            active_ = otherGlobal == this;
            break;
        }
    }
    return true;
}

// Return the number of bits used by the gene
size_t gs::RepeatCellGene::sizeBits() const {
    return Gene::sizeBits() + repeatCellSpec_.bits();
}

// Copy the state from the other gene into this one
void gs::RepeatCellGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<RepeatCellGene*>(other);
    if(from != nullptr) {
        repeatCell_ = from->repeatCell_;
    }
}
