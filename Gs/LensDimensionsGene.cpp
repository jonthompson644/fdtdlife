/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Domain/SquarePlate.h>
#include "LensDimensionsGene.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include <Domain/Material.h>

// Constructor
gs::LensDimensionsGene::LensDimensionsGene(const GeneBitsSpecDouble& unitCellSpec,
                                           const GeneBitsSpecDouble& layerSpacingSpec,
                                           const GeneBitsSpecInteger<size_t>& numRowsSpec) :
        Gene(GeneTemplate::geneTypeLensDimensions),
        unitCellSpec_(unitCellSpec),
        layerSpacingSpec_(layerSpacingSpec),
        numRowsSpec_(numRowsSpec) {
}

// Encode the gene into the chromosome
void gs::LensDimensionsGene::encode(Chromosome* c) {
    Gene::encode(c);
    unitCellSpec_.encode(c, unitCell_);
    layerSpacingSpec_.encode(c, layerSpacing_);
    numRowsSpec_.encode(c, numRows_);
}

// Decode the gene from the chromosome
void gs::LensDimensionsGene::decode(Chromosome* c) {
    Gene::decode(c);
    unitCell_ = unitCellSpec_.decode(c);
    layerSpacing_ = layerSpacingSpec_.decode(c);
    numRows_ = numRowsSpec_.decode(c);
}

// Set the model configuration corresponding to this allele
double gs::LensDimensionsGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if((what & GeneticSearch::individualLoadShapes) != 0) {
        if(numRows_ > 0 && unitCell_ > 0.0) {
            // Get or make the metal material
            domain::Material* mat = s->m()->d()->getMaterial(metal_);
            if(mat == nullptr) {
                mat = s->m()->d()->makeMaterial(domain::Domain::materialModeNormal).get();
                metal_ = mat->index();
            }
            mat->epsilon(box::Expression(box::Constants::epsilonMetal_));
            mat->mu(box::Expression(box::Constants::mu0_));
            mat->sigma(box::Expression(1.0));
            mat->color(box::Constants::colourMagenta);
            mat->name("LensMetal");
            mat->priority(1);
            mat->seqGenerated(true);
            // Get the dimensions of the lens focussing zone
            numColumns_ = static_cast<size_t>(std::floor(s->m()->p()->size().x() / unitCell_));
            double firstColCenter = (numColumns_ / 2.0 - 0.5) * unitCell_;
            // Now for each layer in each column...
            shapes_.resize(numRows_);
            for(size_t row = 0; row < numRows_; ++row) {
                shapes_[row].resize(numColumns_, -1);
                for(size_t col = 0; col < numColumns_; ++col) {
                    // Get a shape identifier
                    if(shapes_[row][col] < 0) {
                        shapes_[row][col] = s->m()->d()->allocShapeIdentifier();
                    }
                    // Make or get the shape
                    domain::Shape* shape = s->m()->d()->getShape(shapes_[row][col]);
                    auto plate = dynamic_cast<domain::SquarePlate*>(shape);
                    if(plate == nullptr) {
                        s->m()->d()->remove(shape);
                        plate = dynamic_cast<domain::SquarePlate*>(s->m()->d()->makeShape(
                                shapes_[row][col],
                                domain::Domain::shapeModeSquarePlate));
                    }
                    // Make the plate name
                    std::stringstream name;
                    name << "LensCell_" << row << "_" << col;
                    // Calculate the plate center
                    box::Vector<double> pos;
                    pos.z(row * layerSpacing_);
                    pos.y(0.0);
                    pos.x(col * unitCell_ - firstColCenter);
                    // Configure the plate
                    plate->name(name.str());
                    plate->center().value(pos);
                    plate->cellSizeX(box::Expression(unitCell_));
                    plate->plateSize(box::Expression(100.0));
                    plate->repeatY(box::Expression(1));
                    plate->repeatX(box::Expression(1));
                    plate->material(metal_);
                    plate->fillMaterial(domain::Domain::defaultMaterial);
                    plate->seqGenerated(true);
                    plate->layerThickness(box::Expression(0.000001));
                    plate->plateOffsetY(0.0);
                    plate->plateOffsetX(0.0);
                }
            }
        }
    }
    return result;
}

// Return the number of bits used by the gene
size_t gs::LensDimensionsGene::sizeBits() const {
    return Gene::sizeBits() + unitCellSpec_.bits() +
           layerSpacingSpec_.bits() + numRowsSpec_.bits();
}

// Copy the state from the other gene into this one
void gs::LensDimensionsGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<LensDimensionsGene*>(other);
    if(from != nullptr) {
        unitCellSpec_ = from->unitCellSpec_;
        numRowsSpec_ = from->numRowsSpec_;
    }
}

