/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneFactory.h"
#include "GeneTemplate.h"
#include "Chromosome.h"
#include <Xml/DomObject.h>

// Constructor
gs::GeneFactory::GeneFactory() :
        nextIdentifier_(0) {
}

// Destructor
gs::GeneFactory::~GeneFactory() {
    clear();
}

// Clear the factory
void gs::GeneFactory::clear() {
    // Delete the templates
    while(!templates_.empty()) {
        delete templates_.front();
    }
}

// Decode the genetic code into genes
void gs::GeneFactory::decode(Chromosome* c, GeneticSearch* s) {
    c->clearGenes();
    c->initialise();
    // Now decode the genes
    for(auto geneTemplate : templates_) {
        size_t geneStart = c->currentPos();
        Gene* gene = geneTemplate->create();
        gene->startBits(geneStart);
        c->addGene(gene);
        gene->decode(c);
    }
    // Now connect the genes up and validate
    c->connectGenes(s);
}

// Encode the genetic code from the genes
void gs::GeneFactory::encode(Chromosome* c, GeneticSearch* s) {
    c->clearData();
    c->initialise();
    // Now encode the genes
    for(auto gene : c->genes()) {
        gene->encode(c);
    }
    // Now connect the genes up and validate
    c->connectGenes(s);
}

// Get a gene template
gs::GeneTemplate* gs::GeneFactory::getTemplate(int identifier) {
    GeneTemplate* result = nullptr;
    for(auto& t : templates_) {
        if(t->identifier() == identifier) {
            result = t;
            break;
        }
    }
    return result;
}

// Return the number of bits in the genome
size_t gs::GeneFactory::sizeBits() {
    size_t result = 0;
    for(auto t : templates_) {
        result += t->sizeBits();
    }
    return result;
}

// Write the configuration to the DOM object
void gs::GeneFactory::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& t : templates_) {
        root << xml::Obj("genetemplate") << *t;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::GeneFactory::readConfig(xml::DomObject& root) {
    clear();
    root >> xml::Open();
    for(auto& p : root.obj("genetemplate")) {
        int identifier;
        GeneTemplate::GeneType geneType;
        *p >> xml::Obj("identifier") >> xml::Default(nextIdentifier_) >> identifier;
        *p >> xml::Obj("genetype") >> geneType;
        auto t = new GeneTemplate(identifier, this, geneType);
        *p >> *t;
    }
    root >> xml::Close();
}

// Add a gene template to the factory
void gs::GeneFactory::addTemplate(GeneTemplate* t) {
    templates_.push_back(t);
    if(nextIdentifier_ <= t->identifier()) {
        nextIdentifier_ = t->identifier() + 1;
    }
}

// Remove a gene template from the factory
void gs::GeneFactory::removeTemplate(GeneTemplate* t) {
    templates_.remove(t);
}

