/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FitnessAnalyser.h"
#include "GeneticSearch.h"
#include "Individual.h"
#include <Fdtd/Model.h>
#include <Sensor/Analyser.h>

// Return the analyser to use
sensor::Analyser* gs::FitnessAnalyser::analyser() {
    return s_->m()->analysers().find(analyserId_);
}

// Write the configuration to the DOM object
void gs::FitnessAnalyser::writeConfig(xml::DomObject& root) const {
    // Base class first
    FitnessBase::writeConfig(root);
    // Now me
    root << xml::Reopen();
    root << xml::Obj("analyserid") << analyserId_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessAnalyser::readConfig(xml::DomObject& root) {
    // Base class first
    FitnessBase::readConfig(root);
    // Now me
    root >> xml::Reopen();
    root >> xml::Obj("analyserid") >> analyserId_;
    root >> xml::Close();
}

// Calculate the unfitness factor for the individual
// An ideal gaussian is fitted to the measured amplitude
// on the transmitted side of the lens by calculating the
// a,b,c parameters from the measured data.  The unfitness
// is then the area between the measured and the fitted.
double gs::FitnessAnalyser::unfitness(gs::Individual* ind) {
    double result = 0.0;
    auto a = analyser();
    if(a != nullptr) {
        result = a->error();
        // Get the data object
        // TODO: Remove this enum translation
        int dataMode;
        std::string unitsY;
        std::string unitsX;
        switch(a->dataType()) {
            case sensor::Analyser::DataType::transmittance:
                dataMode = GeneticSearch::fitnessDataTransmittance;
                unitsX = "Hz";
                break;
            case sensor::Analyser::DataType::phaseShift:
                dataMode = GeneticSearch::fitnessDataPhaseShift;
                unitsY = "deg";
                unitsX = "Hz";
                break;
            case sensor::Analyser::DataType::amplitudeDistribution:
                dataMode = GeneticSearch::fitnessDataAmplitudeDist;
                unitsY = "V/m";
                unitsX = "mm";
                break;
            case sensor::Analyser::DataType::phaseDistribution:
                dataMode = GeneticSearch::fitnessDataPhaseDist;
                unitsY = "rad";
                unitsX = "mm";
                break;
        }
        auto* data = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, dataMode, a->name()));
        // Record the data
        data->setSize(a->data().size(), numDataSets);  // Data sets: data, reference
        for(size_t i = 0; i < a->data().size(); i++) {
            data->setData(i, a->data()[i], measuredData);  // Measured
            data->setData(i, a->reference()[i], referenceData);  // Reference
        }
        data->setYAxisInfo(a->minY(), a->maxY(), unitsY);
        data->setXAxisInfo(a->minX(), a->stepX(), unitsX);
    }
    return result;
}
