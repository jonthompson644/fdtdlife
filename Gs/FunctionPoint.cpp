/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FunctionPoint.h"
#include <Xml/DomObject.h>

// Initialising constructor
gs::FunctionPoint::FunctionPoint(double frequency, double minAttenuation,
                                   double minPhaseShift, double maxAttenuation,
                                   double maxPhaseShift) :
        frequency_(frequency),
        minAttenuation_(minAttenuation),
        maxAttenuation_(maxAttenuation),
        minPhaseShift_(minPhaseShift),
        maxPhaseShift_(maxPhaseShift) {
}

// Default constructor
gs::FunctionPoint::FunctionPoint() :
        frequency_(0.0),
        minAttenuation_(0.0),
        maxAttenuation_(0.0),
        minPhaseShift_(0.0),
        maxPhaseShift_(0.0) {
}

// Copy constructor
gs::FunctionPoint::FunctionPoint(const FunctionPoint& other) :
        frequency_(other.frequency_),
        minAttenuation_(other.minAttenuation_),
        maxAttenuation_(other.maxAttenuation_),
        minPhaseShift_(other.minPhaseShift_),
        maxPhaseShift_(other.maxPhaseShift_) {
}

// Set the point
void gs::FunctionPoint::set(double frequency, double minAttenuation,
                              double minPhaseShift, double maxAttenuation,
                              double maxPhaseShift) {
    frequency_ = frequency;
    minAttenuation_ = minAttenuation;
    maxAttenuation_ = maxAttenuation;
    minPhaseShift_ = minPhaseShift;
    maxPhaseShift_ = maxPhaseShift;
}

// Assignment operator
gs::FunctionPoint& gs::FunctionPoint::operator=(const FunctionPoint &other) {
    frequency_ = other.frequency_;
    minAttenuation_ = other.minAttenuation_;
    maxAttenuation_ = other.maxAttenuation_;
    minPhaseShift_ = other.minPhaseShift_;
    maxPhaseShift_ = other.maxPhaseShift_;
    return *this;
}

// Comparison operator, used by sort function etc.
// The frequency coordinate is used for comparison purposes
bool gs::FunctionPoint::operator<(const FunctionPoint& other) const {
    return frequency_ < other.frequency_;
}

// Write the configuration to the DOM object
void gs::FunctionPoint::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Obj("minattenuation") << minAttenuation_;
    root << xml::Obj("minphaseshift") << minPhaseShift_;
    root << xml::Obj("maxattenuation") << maxAttenuation_;
    root << xml::Obj("maxphaseshift") << maxPhaseShift_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FunctionPoint::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Obj("minattenuation") >> minAttenuation_;
    root >> xml::Obj("minphaseshift") >> minPhaseShift_;
    root >> xml::Obj("maxattenuation") >> maxAttenuation_;
    root >> xml::Obj("maxphaseshift") >> maxPhaseShift_;
    root >> xml::Close();
}
