/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessAdmittanceBase.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Sensor/ArraySensor.h>

// Initialise the model ready for the assessment of an individual
void gs::FitnessAdmittanceBase::initialise(size_t numFrequencies, double frequencyStart,
                                          double frequencySpacing) {
    // Base class first
    FitnessPlaneWaveBase::initialise(numFrequencies, frequencyStart, frequencySpacing);
    // Get or create the array sensorId
    sensor::ArraySensor* sensor = getSensor();
    // Configure the sensorId
    sensor->colour(box::Constants::colourWhite);
    sensor->saveCsvData(false);
    sensor->numPointsX(1);
    sensor->numPointsY(1);
    sensor->currentPointX(0);
    sensor->currentPointY(0);
    sensor->frequency(s_->frequencySpacing());
    sensor->animateFrequency(false);
    sensor->frequencyStep(s_->frequencySpacing());
    sensor->averageOverCell(s_->averageSensor());
    sensor->componentSel({true, s_->xyComponents(), s_->xyComponents()});
}

// Get or create the sensorId for this fitness function
sensor::ArraySensor* gs::FitnessAdmittanceBase::getSensor() {
    const std::string name = "GeneticSensor";
    auto* result = dynamic_cast<sensor::ArraySensor*>(s_->m()->sensors().find(name));
    if(result == nullptr) {
        result = dynamic_cast<sensor::ArraySensor*>(
                s_->m()->sensors().makeSensor(sensor::Sensors::modeArraySensor));
        result->name(name);
    }
    return result;
}

