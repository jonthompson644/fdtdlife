/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Individual.h"
#include <Fdtd/Model.h>
#include <Sensor/ArraySensor.h>
#include <Sensor/TwoDSlice.h>
#include "FitnessFunction.h"
#include "GeneticSearch.h"
#include <Xml/DomObject.h>
#include <Box/Constants.h>
#include "RepeatCellGene.h"
#include "LayerSpacingGene.h"
#include "LayerSymmetryGene.h"
#include <Domain/AdmittanceLayerShape.h>

// Default constructor
gs::Individual::Individual(int id, GeneticSearch* s) :
        s_(s) {
    construct(id);
}

// Constructor that generates a random individual with no parents
gs::Individual::Individual(int id, Random* random, size_t numBits, GeneticSearch* s) :
        s_(s) {
    construct(id);
    chromosome_ = new Chromosome(random, numBits);
}

// Constructor that uses stored data
gs::Individual::Individual(int id, double unfitness, double fitness,
                             int mother, int father, int crossPoint,
                             int numMutations, const char* chromosome,
                             State state, GeneticSearch* s) :
        s_(s) {
    construct(id);
    chromosome_ = new Chromosome(chromosome);
    fitness_ = fitness;
    unfitness_ = unfitness;
    crossPoint_ = (size_t) crossPoint;
    numMutations_ = (size_t) numMutations;
    mother_ = mother;
    father_ = father;
    state_ = state;
}

// Constructor that takes a chromosome
gs::Individual::Individual(int id, Chromosome* c, GeneticSearch* s) :
        s_(s) {
    construct(id);
    chromosome_ = c;
}

// Constructor that initialises the individual's results data.
// Used by the array sensorId and other places that just want to assess fitness.
gs::Individual::Individual(int id, const std::vector<double>& attenuation,
                             const std::vector<double>& phaseShift, bool xyComponents,
                             const std::vector<double>& attenuationY,
                             const std::vector<double>& phaseShiftY, GeneticSearch* s) :
        s_(s) {
    construct(id);
    phaseShift_ = phaseShift;
    attenuation_ = attenuation;
    xyComponents_ = xyComponents;
    attenuationY_ = attenuationY;
    phaseShiftY_ = phaseShiftY;
}

// Breed an individual using single crossover
std::shared_ptr<gs::Individual> gs::Individual::breedSingleCrossover(
        int id, std::shared_ptr<Individual> mother,
        std::shared_ptr<Individual> father, size_t crossPoint) {
    auto result = std::make_shared<Individual>(id, father->s_);
    result->mother_ = mother->id();
    result->father_ = father->id();
    result->crossPoint_ = crossPoint;
    // Create a genome from the parents
    result->chromosome_ = Chromosome::breedSingle(mother->chromosome(),
                                                  father->chromosome(), crossPoint);
    return result;
}

// Breed an individual using uniform crossover
std::shared_ptr<gs::Individual> gs::Individual::breedUniformCrossover(
        int id, std::shared_ptr<Individual> mother,
        std::shared_ptr<Individual> father, Random* random) {
    auto result = std::make_shared<Individual>(id, father->s_);
    result->mother_ = mother->id();
    result->father_ = father->id();
    result->crossPoint_ = 0;
    // Create a genome from the parents
    result->chromosome_ = Chromosome::breedUniform(mother->chromosome(),
                                                   father->chromosome(), random);
    return result;
}

// Common parts of all constructors
void gs::Individual::construct(int id) {
    id_ = id;
    chromosome_ = nullptr;
    fitness_ = 1.0;
    unfitness_ = 1.0;
    mother_ = -1;
    father_ = -1;
    crossPoint_ = 0;
    numMutations_ = 0;
    state_ = stateWaiting;
    xyComponents_ = false;
}

// Destructor
gs::Individual::~Individual() {
    // Delete the components
    delete chromosome_;
}

// Mutate the chromosome of this individual
int gs::Individual::mutate(double probability, Random* random) {
    numMutations_ = chromosome_->mutate(probability, random);
    return (int) numMutations_;
}

// Configure the model for this individual
// Returns the total thickness of the structure
double gs::Individual::configureModel(GeneticSearch* s, int what) {
    double result = chromosome_->configureModel(s, what);
    // If the model has a single admittance layer, initialise the admittance
    // table using any data stored with the individual
    if(s->m()->d()->shapes().size() == 1) {
        auto* layer = dynamic_cast<domain::AdmittanceLayerShape*>(
                s->m()->d()->shapes().front().get());
        if(layer != nullptr) {
            if(!attenuation_.empty() && attenuation_.size() == phaseShift_.size()) {
                fdtd::AdmittanceData table;
                double frequency = s->frequencySpacing();
                for(size_t i = 0; i < attenuation_.size(); i++) {
                    double tanTheta = tan(phaseShift_[i] * box::Constants::deg2rad_);
                    double denominator = attenuation_[i] * sqrt(1 + tanTheta * tanTheta);
                    std::complex<double> admittance = {(1.0 / denominator) - 1.0,
                                                       -tanTheta / denominator};
                    table.addPoint(frequency, admittance);
                    frequency += s->frequencySpacing();
                }
                table.cellSize(layer->cellSizeX().value());
                layer->admittanceTable() = table;
            }
        }
    }
    return result;
}

// Set this individual to the running state
void gs::Individual::setRunning() {
    state_ = stateRunning;
}

// Decode the individual's chromosome, returns true if viable
bool gs::Individual::decodeGenes(GeneticSearch* m) {
    // Decode the chromosome
    m->geneFactory()->decode(chromosome_, m);
    return chromosome_->isViable();
}

// Encode the individual's chromosome
void gs::Individual::encodeGenes(GeneticSearch* m) {
    m->geneFactory()->encode(chromosome_, m);
}

// Evaluate the fitness of this individual from the data in the sensorId
void gs::Individual::evaluateFitness(FitnessFunction* fitnessFunction) {
    // Clear the fitness information
    fitnessResults_.clear();
    fitnessData_.clear();
    // Calculate the unfitness factor
    unfitness_ = fitnessFunction->unfitness(this);
    // Remember we have done this
    state_ = stateEvaluated;
}

// Copy the evaluation state from the other individual to this one
void gs::Individual::copyEvaluation(const std::shared_ptr<Individual>& other) {
    attenuation_ = other->attenuation_;
    phaseShift_ = other->phaseShift_;
    unfitness_ = other->unfitness_;
    fitness_ = other->fitness_;
    state_ = other->state_;
    xyComponents_ = other->xyComponents_;
    attenuationY_ = other->attenuationY_;
    phaseShiftY_ = other->phaseShiftY_;
}

// Returns the distance, in terms of number of bits that are different,
// between this individual's genes and the other.  Only the bits that form the active
// parts of genes are considered.
size_t gs::Individual::distanceBetween(const std::shared_ptr<Individual>& other) {
    return chromosome_->distanceBetween(other->chromosome_);
}

// Less than operator used by map, set, etc.
bool gs::Individual::operator<(const gs::Individual& /*other*/) const {
    return false; //chromosome_->signature() < other.chromosome_->signature();
}

// Create an individual
std::shared_ptr<gs::Individual> gs::Individual::factory(xml::DomObject& o, GeneticSearch* s) {
    int identifier;
    o >> identifier;
    return std::make_shared<Individual>(identifier, s);
}

// Write the object to the DOM
void gs::Individual::writeIndividual(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("id") << id_;
    root << xml::Obj("unfitness") << unfitness_;
    root << xml::Obj("fitness") << fitness_;
    root << xml::Obj("mother") << mother_;
    root << xml::Obj("father") << father_;
    root << xml::Obj("crosspoint") << crossPoint_;
    root << xml::Obj("nummutations") << numMutations_;
    root << xml::Obj("chromosome") << chromosome_->dataDisplay();
    root << xml::Obj("evaluated") << isEvaluated();
    root << xml::Obj("attenuation") << attenuation_;
    root << xml::Obj("phaseshift") << phaseShift_;
    root << xml::Obj("timeseries") << timeSeries_;
    root << xml::Obj("reference") << reference_;
    root << xml::Obj("xycomponents") << xyComponents_;
    if(xyComponents_) {
        root << xml::Obj("attenuationy") << attenuationY_;
        root << xml::Obj("phaseshifty") << phaseShiftY_;
    }
    for(auto& r : fitnessResults_) {
        root << xml::Obj("fitnessresult") << r;
    }
    for(auto& d : fitnessData_) {
        root << xml::Obj("fitnessdata") << *d;
    }
    root << xml::Close();
}

// Read the object from the DOM
void gs::Individual::readIndividual(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("id") >> id_;
    root >> xml::Obj("unfitness") >> unfitness_;
    root >> xml::Obj("fitness") >> fitness_;
    root >> xml::Obj("mother") >> mother_;
    root >> xml::Obj("father") >> father_;
    root >> xml::Obj("crosspoint") >> crossPoint_;
    root >> xml::Obj("nummutations") >> numMutations_;
    std::string c;
    root >> xml::Obj("chromosome") >> c;
    chromosome_ = new Chromosome(c.c_str());
    bool e;
    root >> xml::Obj("evaluated") >> e;
    state_ = e ? Individual::stateEvaluated : Individual::stateWaiting;
    root >> xml::Obj("attenuation") >> attenuation_;
    root >> xml::Obj("phaseshift") >> phaseShift_;
    root >> xml::Obj("timeseries") >> xml::Default("") >> timeSeries_;
    root >> xml::Obj("reference") >> xml::Default("") >> reference_;
    root >> xml::Obj("xycomponents") >> xml::Default(false) >> xyComponents_;
    if(xyComponents_) {
        root >> xml::Obj("attenuationy") >> attenuationY_;
        root >> xml::Obj("phaseshifty") >> phaseShiftY_;
    }
    fitnessResults_.clear();
    for(auto& o : root.obj("fitnessresult")) {
        fitnessResults_.emplace_back(FitnessPartResult());
        *o >> fitnessResults_.back();
    }
    for(auto& o : root.obj("fitnessdata")) {
        fitnessData_.emplace_back(s_->makeFitnessData(*o));
        *o >> *fitnessData_.back();
    }
    root >> xml::Close();
    decodeGenes(s_);
}

// Return the repeat cell size for this individual from its genome.
// If there is no repeat cell gene, returns the default value.
double gs::Individual::getRepeatCell(double defaultValue) {
    double result = defaultValue;
    for(auto gene : chromosome_->genes()) {
        auto repeatCellGene = dynamic_cast<RepeatCellGene*>(gene);
        if(repeatCellGene != nullptr) {
            result = repeatCellGene->repeatCell();
            break;
        }
    }
    return result;
}

// Return the layer symmetry for this individual from its genome.
// If there is no layer symmetry gene, returns the default value.
int gs::Individual::getLayerSymmetry(int defaultValue) {
    int result = defaultValue;
    for(auto gene : chromosome_->genes()) {
        auto layerSymmetryGene = dynamic_cast<LayerSymmetryGene*>(gene);
        if(layerSymmetryGene != nullptr) {
            result = layerSymmetryGene->symmetry();
            break;
        }
    }
    return result;
}

// Return the transmittance for the individual
void gs::Individual::getTransmittance(std::vector<double>& data) const {
    data.clear();
    for(auto& t : attenuation_) {
        data.push_back(t * t);
    }
}

// Return the transmittance for the individual
void gs::Individual::getTransmittanceY(std::vector<double>& data) const {
    data.clear();
    for(auto& t : attenuationY_) {
        data.push_back(t * t);
    }
}

// Get or create the specified fitness data
gs::FitnessData* gs::Individual::getFitnessData(GeneticSearch* s, int mode, const std::string& name) {
    FitnessData* result = nullptr;
    // Do we already have one with this name?
    auto found = std::find_if(fitnessData_.begin(), fitnessData_.end(),
                              [name, mode](const std::unique_ptr<FitnessData>& i) {
                                  return i->name() == name && i->mode() == mode;
                              });
    if(found != fitnessData_.end()) {
        result = found->get();
    } else {
        result = s->makeFitnessData(mode, name);
        fitnessData_.emplace_back(result);
    }
    return result;
}
