/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSPOINT_H
#define FDTDLIFE_FITNESSPOINT_H

namespace xml {class DomObject;}

namespace gs {

    // Represents a point in a function specification
    class FitnessPoint {
    public:
        FitnessPoint(double x, double min, double max);
        FitnessPoint();
        FitnessPoint(const FitnessPoint& other);
        double x() const {return x_;}
        double min() const {return min_;}
        double max() const {return max_;}
        void set(double x, double min, double max);
        FitnessPoint& operator=(const FitnessPoint& other);
        bool operator<(const FitnessPoint& other) const;
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const FitnessPoint& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, FitnessPoint& v) {v.readConfig(o); return o;}
    protected:
        double x_;
        double min_;
        double max_;
    };
}


#endif //FDTDLIFE_FITNESSPOINT_H
