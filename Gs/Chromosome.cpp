/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <sstream>
#include <iomanip>
#include <cassert>
#include "Chromosome.h"
#include "LayerSpacingGene.h"
#include "Random.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"

// Constructor
gs::Chromosome::Chromosome() {
}

// Create a chromosome using single crossover
gs::Chromosome* gs::Chromosome::breedSingle(Chromosome* mother, Chromosome* father,
                                                       size_t crossOver) {
    auto result = new Chromosome();
    // The size of the genome will be that of the father
    result->data_.resize(father->data_.size());
    // Cross over info
    size_t crossOverByte = crossOver / bitsPerByte_;
    size_t crossOverBit = crossOver % bitsPerByte_;
    // The bytes wholly from the mother
    for(size_t i=0; i<crossOverByte; i++) {
        result->dataAccess(i) = mother->dataAccess(i);
    }
    // The byte with the crossover position in it
    if(crossOverByte < result->data_.size()) {
        result->dataAccess(crossOverByte) = 0;
        for (size_t b = 0; b < bitsPerByte_; b++) {
            if (b < crossOverBit) {
                result->dataAccess(crossOverByte) |= ((1 << b) & mother->dataAccess(crossOverByte));
            } else {
                result->dataAccess(crossOverByte) |= ((1 << b) & father->dataAccess(crossOverByte));
            }
        }
    }
    // The bytes wholly from the father
    for(size_t i=crossOverByte+1; i<father->data_.size(); i++) {
        result->dataAccess(i) = father->dataAccess(i);
    }
    return result;
}

// Create a chromosome using uniform crossover
gs::Chromosome* gs::Chromosome::breedUniform(Chromosome* mother, Chromosome* father,
                                                        Random* random) {
    auto result = new Chromosome();
    // The size of the genome will be that of the father
    result->data_.resize(father->data_.size());
    // For each byte in the two genomes...
    for(size_t i=0; i<result->data_.size(); i++) {
        auto fatherMask = (uint8_t)random->get((size_t)256);
        uint8_t motherMask = ~fatherMask;
        uint8_t newData = father->dataAccess(i);
        if(i < mother->data_.size()) {
            newData = (newData & fatherMask) | (mother->dataAccess(i) & motherMask);
        }
        result->dataAccess(i) = newData;
    }
    return result;
}

// Access the data vector with bounds checking
uint8_t& gs::Chromosome::dataAccess(size_t i) {
    if(i >= data_.size()) {
        i=0;
    }
    return data_.at(i);
}

// Random constructor
gs::Chromosome::Chromosome(Random* random, size_t sizeBits) {
    data_.resize((sizeBits + bitsPerByte_ - 1) / bitsPerByte_);
    for (uint8_t &v : data_) {
        v = (uint8_t)random->get((size_t)256);
    }
}

// Constructor that uses stored data in hex text form
gs::Chromosome::Chromosome(const char* data) {
    std::stringstream decode(data);
    while(!decode.eof()) {
        int d;
        decode >> std::hex >> d;
        if(!decode.eof()) {
            data_.push_back((uint8_t) (d & 0xff));
        }
    }
}

// Constructor that uses specified data
gs::Chromosome::Chromosome(const std::vector<uint8_t>& data) {
    data_ = data;
}


// Destructor
gs::Chromosome::~Chromosome() {
    clearGenes();
}

// Connect the genes up and record the viability
void gs::Chromosome::connectGenes(GeneticSearch* s) {
    viable_ = !genes_.empty();
    for(auto g : genes_) {
        viable_ = g->connect(this, s) && viable_;
    }
}

// Return the total number of bits in the chromosome
size_t gs::Chromosome::size() {
    // Total number of bits required
    return data_.size() * bitsPerByte_;
}

// Initialise the chromosome for an encode or decode operation
void gs::Chromosome::initialise() {
    // The location information
    bytePos_ = 0;
    bitPos_ = 0;
}

// Set a gene encoded word in the encoding
// This is an inefficient but obvious implementation
void gs::Chromosome::encode(uint64_t d, size_t bits) {
    for(size_t n=0; n<bits; n++) {
        dataAccess(bytePos_) |= (uint8_t)(((d>>n) & (uint64_t)1) << bitPos_);
        bitPos_++;
        if(bitPos_ >= bitsPerByte_) {
            bitPos_ = 0;
            bytePos_++;
        }
    }
}

// Get a gene encoded word from the encoding
// This is an inefficient but obvious implementation
uint64_t gs::Chromosome::decode(size_t bits) {
    uint64_t result = 0;
    for(size_t n=0; n<bits; n++) {
        result |= (((uint64_t)dataAccess(bytePos_) >> bitPos_) & (uint64_t)1) << n;
        bitPos_++;
        if(bitPos_ >= bitsPerByte_) {
            bitPos_ = 0;
            bytePos_++;
        }
    }
    return result;
}

// Return true if there are enough bits remaining to be decoded in the chromosome
bool gs::Chromosome::decodeBitsLeft(size_t bits) {
    size_t bitsSoFar = bytePos_ * bitsPerByte_ + bitPos_;
    size_t totalBits = data_.size() * bitsPerByte_;
    return (bitsSoFar + bits) <= totalBits;
}

// Return the chromosome data as a display string
std::string gs::Chromosome::dataDisplay() const {
    std::stringstream result;
    for(auto d : data_) {
        result << std::hex << std::setw(2) << std::setfill('0') << (int)d << " ";
    }
    return result.str();
}

// Mutate with the given probability.  Returns the number of mutations
size_t gs::Chromosome::mutate(double probability, Random *random) {
    size_t result = 0;
    for(size_t b=0; b<size(); b++) {
        if(random->flip(probability)) {
            size_t byte = b / bitsPerByte_;
            size_t bit = b % bitsPerByte_;
            dataAccess(byte) = (uint8_t)(dataAccess(byte) ^ (uint8_t)((size_t)1<<bit));
            result ++;
        }
    }
    return result;
}

// Configure a model to match this chromosome
// Returns the total thickness of the structure.
double gs::Chromosome::configureModel(GeneticSearch* s, int what) {
    // Configure the model
    double thickness = 0.0;
    for(auto g : genes_) {
        double topZ = g->configureModel(s, what);
        thickness = std::max(thickness, topZ);
    }
    return thickness;
}

// Clear all genes from the chromosome
void gs::Chromosome::clearGenes() {
    while(!genes_.empty()) {
        delete genes_.front();
        genes_.pop_front();
    }
}

// Clear the genome data bits
void gs::Chromosome::clearData() {
    for(auto& d : data_) {
        d = (uint8_t)0;
    }
}

// Add a gene to the chromosome
void gs::Chromosome::addGene(Gene* g) {
    genes_.push_back(g);
}

// Return the gene at the specified index
gs::Gene* gs::Chromosome::getGene(int index) {
    Gene* result = nullptr;
    auto pos = genes_.begin();
    for(int i=0; i<index && pos != genes_.end(); i++) {
        pos++;
    }
    if(pos != genes_.end()) {
        result = *pos;
    }
    return result;
}

// Returns the distance, in terms of number of bits that are different,
// between this gene and the other.  Only the bits that form the active
// parts of genes are considered.
size_t gs::Chromosome::distanceBetween(Chromosome* other) {
    size_t smallest = std::min(size(), other->size());
    size_t largest = std::max(size(), other->size());
    size_t result = largest - smallest;
    for(size_t i=0; i<smallest; i++) {
        size_t bytePos = i / bitsPerByte_;
        size_t bitPos = i % bitsPerByte_;
        if((dataAccess(bytePos) & (1 << bitPos)) != ((other->dataAccess(bytePos) & (1 << bitPos)))){
            result++;
        }
    }
    return result;
}

