/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsSpecLogDouble.h"
#include <cmath>
#include <memory>
#include <Xml/DomObject.h>
#include "Chromosome.h"

// Initialising constructor
gs::GeneBitsSpecLogDouble::GeneBitsSpecLogDouble(size_t bits, double min, double max) :
        GeneBitsSpecDouble(bits, min, max) {
}

// Encode a value into the chromosome
void gs::GeneBitsSpecLogDouble::encode(Chromosome* c, double value) {
    if(bits_ > 0) {
        auto v = static_cast<uint64_t>(std::round((log(value) - log(min_)) / scaling()));
        c->encode(v, bits_);
    }
}

// Decode a value from the chromosome
double gs::GeneBitsSpecLogDouble::decode(Chromosome* c) {
    double result = min_;
    if(bits_ > 0) {
        result = exp(static_cast<double>(c->decode(bits_)) * scaling() + log(min_));
    }
    return result;
}

// Calculate the scaling factor
double gs::GeneBitsSpecLogDouble::scaling() const {
    double result;
    result = (log(max_) - log(min_)) / static_cast<double>(1U << bits_);
    if(result <= 0.0) {
        result = 1.0;
    }
    return result;
}


