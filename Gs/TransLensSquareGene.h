/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TRANSLENSSQUAREGENE_H
#define FDTDLIFE_TRANSLENSSQUAREGENE_H

#include "LensSquareGene.h"

namespace gs {

    // A class that evolves the sizes of a layer of squares in the x direction
    class TransLensSquareGene : public LensSquareGene {
    protected:
    public:
        TransLensSquareGene(const GeneBitsSpecDouble& a,
                       const GeneBitsSpecLogDouble& b,
                       const GeneBitsSpecLogDouble& c,
                       const GeneBitsSpecLogDouble& d,
                       const GeneBitsSpecLogDouble& e,
                       const GeneBitsSpecLogDouble& f);
        double configureModel(GeneticSearch* s, unsigned int what) override;
        void copyFrom(Gene* other, fdtd::Model* m) override;
    };

}


#endif //FDTDLIFE_TRANSLENSSQUAREGENE_H
