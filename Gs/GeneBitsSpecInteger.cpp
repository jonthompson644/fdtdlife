/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsSpecInteger.h"
#include <Xml/DomObject.h>
#include "Chromosome.h"

// Initialising constructor
template <class T>
gs::GeneBitsSpecInteger<T>::GeneBitsSpecInteger(size_t bits, T min) :
        bits_(bits),
        min_(min) {
}

// Copy constructor
template <class T>
gs::GeneBitsSpecInteger<T>::GeneBitsSpecInteger(const gs::GeneBitsSpecInteger<T>& other) {
    *this = other;
}

// Equality operator
template <class T>
bool gs::GeneBitsSpecInteger<T>::operator==(const gs::GeneBitsSpecInteger<T>& other) const {
    return bits_ == other.bits_ && min_ == other.min_;
}

// Inequality operator
template <class T>
bool gs::GeneBitsSpecInteger<T>::operator!=(const gs::GeneBitsSpecInteger<T>& other) const {
    return !(*this == other);
}

// Write the specification to the XML DOM
template <class T>
void gs::GeneBitsSpecInteger<T>::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("bits") << bits_;
    p << xml::Obj("min") << min_;
    p << xml::Close();
}

// Read the specification from the XML DOM
template <class T>
void gs::GeneBitsSpecInteger<T>::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("bits") >> xml::Default(bits_) >> bits_;
    p >> xml::Obj("min") >> xml::Default(min_) >> min_;
    p >> xml::Close();
}

// Encode a value into the chromosome
template <class T>
void gs::GeneBitsSpecInteger<T>::encode(Chromosome* c, T value) {
    if(bits_ > 0) {
        auto v = static_cast<uint64_t>(value - min_);
        c->encode(v, bits_);
    }
}

// Decode a value from the chromosome
template <class T>
T gs::GeneBitsSpecInteger<T>::decode(Chromosome* c) {
    T result = min_;
    if(bits_ > 0) {
        result = static_cast<T>(c->decode(bits_) + min_);
    }
    return result;
}

// Explicit instantiations
template class gs::GeneBitsSpecInteger<int>;
template class gs::GeneBitsSpecInteger<size_t>;
