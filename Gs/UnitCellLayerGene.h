/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DYNAMICGENE_H
#define FDTDLIFE_DYNAMICGENE_H

#include "LayerGene.h"

namespace gs {

    // Base class for layer genes that have a unit cell size.
    class UnitCellLayerGene : public LayerGene {
    public:
        UnitCellLayerGene(GeneTemplate::GeneType geneType, size_t repeatBits, size_t repeatMin);
        void decode(Chromosome* c) override;
        void encode(Chromosome* c) override;
        bool connect(Chromosome* c, GeneticSearch* s) override;
    protected:
        // Evolving
        size_t repeat_ {};  // Number of instances per repeat cell
        // Template
        size_t repeatBits_;  // Number of bits taken
        size_t repeatMin_;  // Minimum repeat value
        // Other
        double unitCell_{};  // The size of the unit cell
    public:
        size_t repeat() const {return repeat_;}
        void repeat(size_t v) {repeat_ = v;}
        size_t sizeBits() const override;
        double thickness() const override {return layerSpacing_;}
        void copyFrom(Gene* other, fdtd::Model* m) override;
    };

}


#endif //FDTDLIFE_DYNAMICGENE_H
