/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <cmath>
#include "LayerSpacingGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include "UnitCellLayerGene.h"

// Constructor
gs::LayerSpacingGene::LayerSpacingGene(const GeneBitsSpecDouble& layerSpacingSpec) :
        Gene(GeneTemplate::geneTypeLayerSpacing),
        layerSpacing_(defaultLayerSpacing_),
        layerSpacingSpec_(layerSpacingSpec) {
}

// Encode the allele into the chromosome
void gs::LayerSpacingGene::encode(Chromosome* c) {
    layerSpacingSpec_.encode(c, layerSpacing_);
}

// Decode the allele from the chromosome
void gs::LayerSpacingGene::decode(Chromosome* c) {
    layerSpacing_ = layerSpacingSpec_.decode(c);
}

// Set the model configuration corresponding to this gene
double gs::LayerSpacingGene::configureModel(GeneticSearch* /*s*/, unsigned int /*what*/) {
    return 0.0;
}

// Connect to other genes.  Returns true if everything required exists.
bool gs::LayerSpacingGene::connect(Chromosome* /*c*/, GeneticSearch* /*s*/) {
    return true;
}

// Return the number of bits used by the gene
size_t gs::LayerSpacingGene::sizeBits() const {
    return Gene::sizeBits() + layerSpacingSpec_.bits();
}

// Copy the state from the other gene into this one
void gs::LayerSpacingGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<LayerSpacingGene*>(other);
    if(from != nullptr) {
        layerSpacing_ = from->layerSpacing_;
    }
}
