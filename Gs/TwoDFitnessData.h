/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TWODFITNESSDATA_H
#define FDTDLIFE_TWODFITNESSDATA_H

#include "FitnessData.h"
#include <vector>

namespace gs {

    // Two-dimensional data record with an individual by a fitness function
    template <class T>
    class TwoDFitnessData : public FitnessData {
    public:
        // Construction
        TwoDFitnessData() = default;

        // Overrides of FitnessData
        void write(xml::DomObject& root) const override;
        void read(xml::DomObject& root) override;

        // API
        void setSize(size_t x, size_t y);
        void setData(size_t x, size_t y, T v);

        // Data access
        const std::vector<T>& data() const {return data_;}
        T at(size_t x, size_t y) const;
        size_t xSize() const {return xSize_;}
        size_t ySize() const {return ySize_;}

    protected:
        size_t xSize_ {};
        size_t ySize_ {};
        std::vector<T> data_;

    };
}

extern template class gs::TwoDFitnessData<double>;
extern template class gs::TwoDFitnessData<std::complex<double>>;

#endif //FDTDLIFE_TWODFITNESSDATA_H
