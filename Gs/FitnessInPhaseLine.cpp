/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessInPhaseLine.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include "Individual.h"
#include "OneDFitnessData.h"
#include <Sensor/ArraySensor.h>
#include <Source/ZoneSource.h>
#include <Xml/DomObject.h>
#include <cmath>
#include <sstream>

// Initialise the model ready for the assessment of an individual
void gs::FitnessInPhaseLine::initialise(size_t numFrequencies, double frequencyStart,
                                        double frequencySpacing) {
    // Base class first
    FitnessBase::initialise(numFrequencies, frequencyStart, frequencySpacing);
    // Create the array sensorId
    sensor::ArraySensor* sensor = getSensor();
    // Configure the sensorId
    sensor->numPointsX((int) numPointsX_);
    sensor->numPointsY(1);
    sensor->averageOverCell(s_->averageSensor());
    sensor->componentSel({true, s_->xyComponents(), s_->xyComponents()});
    sensor->manualZPos(false);
    sensor->frequency(frequency_);
    fdtd::Configuration* p = s_->m()->p();
    sensor->reflectedZPos(p->p1().z().value() -
                          p->dr().z().value() * p->scatteredFieldZoneSize() / 2);
}

// Get or create the sensorId for this fitness function
sensor::ArraySensor* gs::FitnessInPhaseLine::getSensor() {
    std::stringstream name;
    name << "InPhaseLine:" << s_->identifier();
    sensor::ArraySensor* result = dynamic_cast<sensor::ArraySensor*>(
            s_->m()->sensors().find(name.str()));
    if(result == nullptr) {
        result = dynamic_cast<sensor::ArraySensor*>(
                s_->m()->sensors().makeSensor(sensor::Sensors::modeArraySensor));
        result->name(name.str());
    }
    return result;
}

// Determine the unfitness of the individual
double gs::FitnessInPhaseLine::unfitness(Individual* ind) {
    double result = 0.0;
    double phaseUnfitness = 0.0;
    double amplitudeUnfitness = 0.0;
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        // Create the data objects
        OneDFitnessData<double>* amplitudeX = nullptr;
        OneDFitnessData<double>* amplitudeY = nullptr;
        OneDFitnessData<double>* phaseX = nullptr;
        OneDFitnessData<double>* phaseY = nullptr;
        bool hasExistingData = false;
        switch(dataSource_) {
            case dataSourceMagnitude:
                amplitudeX = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataAmplitudeDist,
                                            "AmplitudeX"));
                amplitudeY = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataAmplitudeDist,
                                            "AmplitudeY"));
                phaseX = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseDist,
                                            "PhaseX"));
                phaseY = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseDist,
                                            "PhaseY"));
                hasExistingData = amplitudeX->data().size() == numPointsX_ &&
                                  amplitudeY->data().size() == numPointsX_ &&
                                  phaseX->data().size() == numPointsX_ &&
                                  phaseY->data().size() == numPointsX_;
                break;
            case dataSourceXComponent:
                amplitudeX = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataAmplitudeDist,
                                            "AmplitudeX"));
                phaseX = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseDist,
                                            "PhaseX"));
                hasExistingData = amplitudeX->data().size() == numPointsX_ &&
                                  phaseX->data().size() == numPointsX_;
                break;
            case dataSourceYComponent:
                amplitudeY = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataAmplitudeDist,
                                            "AmplitudeY"));
                phaseY = dynamic_cast<OneDFitnessData<double>*>(
                        ind->getFitnessData(s_, GeneticSearch::fitnessDataPhaseDist,
                                            "PhaseY"));
                hasExistingData = amplitudeY->data().size() == numPointsX_ &&
                                  phaseY->data().size() == numPointsX_;
                break;
        }
        // Get the sensorId data
        if(!hasExistingData) {
            double px, py, ax, ay;
            double minAmp = 0.0;
            double maxAmp = 0.0;
            sensor->amplitudeAt(0, 0, frequency_, ax, ay);
            switch(dataSource_) {
                case dataSourceMagnitude:
                    minAmp = std::min(ax, ay);
                    maxAmp = std::max(ax, ay);
                    phaseX->setSize(numPointsX_);
                    phaseY->setSize(numPointsX_);
                    amplitudeX->setSize(numPointsX_);
                    amplitudeY->setSize(numPointsX_);
                    break;
                case dataSourceXComponent:
                    minAmp = ax;
                    maxAmp = ax;
                    phaseX->setSize(numPointsX_);
                    amplitudeX->setSize(numPointsX_);
                    break;
                case dataSourceYComponent:
                    minAmp = ay;
                    maxAmp = ay;
                    phaseY->setSize(numPointsX_);
                    amplitudeY->setSize(numPointsX_);
                    break;
            }
            for(size_t i = 0; i < numPointsX_; i++) {
                sensor->phaseAt(i, 0, frequency_, px, py);
                px /= box::Constants::deg2rad_;
                py /= box::Constants::deg2rad_;
                sensor->amplitudeAt(i, 0, frequency_, ax, ay);
                switch(dataSource_) {
                    case dataSourceMagnitude:
                        phaseX->setData(i, px);
                        phaseY->setData(i, py);
                        amplitudeX->setData(i, ax);
                        amplitudeY->setData(i, ay);
                        minAmp = std::min(minAmp, ax);
                        minAmp = std::min(minAmp, ay);
                        maxAmp = std::max(maxAmp, ax);
                        maxAmp = std::max(maxAmp, ay);
                        break;
                    case dataSourceXComponent:
                        phaseX->setData(i, px);
                        amplitudeX->setData(i, ax);
                        minAmp = std::min(minAmp, ax);
                        maxAmp = std::max(maxAmp, ax);
                        break;
                    case dataSourceYComponent:
                        phaseY->setData(i, py);
                        amplitudeY->setData(i, ay);
                        minAmp = std::min(minAmp, ay);
                        maxAmp = std::max(maxAmp, ay);
                        break;
                }
            }
            switch(dataSource_) {
                case dataSourceMagnitude:
                    amplitudeX->setYAxisInfo(minAmp, maxAmp, "V/m");
                    amplitudeY->setYAxisInfo(minAmp, maxAmp, "V/m");
                    amplitudeX->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    amplitudeY->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    phaseX->setYAxisInfo(-180.0, 180.0, "deg");
                    phaseY->setYAxisInfo(-180.0, 180.0, "deg");
                    phaseX->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    phaseY->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    break;
                case dataSourceXComponent:
                    amplitudeX->setYAxisInfo(minAmp, maxAmp, "V/m");
                    amplitudeX->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    phaseX->setYAxisInfo(-180.0, 180.0, "deg");
                    phaseX->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    break;
                case dataSourceYComponent:
                    amplitudeY->setYAxisInfo(minAmp, maxAmp, "V/m");
                    amplitudeY->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    phaseY->setYAxisInfo(-180.0, 180.0, "deg");
                    phaseY->setXAxisInfo(0.0, sensor->spacingX() * 1000.0, "mm");
                    break;
            }
        }
        // Calculate the unfitness
        switch(dataSource_) {
            case dataSourceMagnitude:
                phaseUnfitness = unfitnessPhase(phaseX->data()) +
                                 unfitnessPhase(phaseY->data());
                amplitudeUnfitness = unfitnessAmplitude(amplitudeX->data()) +
                                     unfitnessAmplitude(amplitudeY->data());
                break;
            case dataSourceXComponent:
                phaseUnfitness = unfitnessPhase(phaseX->data());
                amplitudeUnfitness = unfitnessAmplitude(amplitudeX->data());
                break;
            case dataSourceYComponent:
                phaseUnfitness = unfitnessPhase(phaseY->data());
                amplitudeUnfitness = unfitnessAmplitude(amplitudeY->data());
                break;
        }
        // Return the total unfitness
        amplitudeUnfitness *= amplitudeWeight_;
        result = (phaseUnfitness + amplitudeUnfitness) * weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    ind->fitnessResults().back().setExtra("Amplitude contribution", amplitudeUnfitness);
    return result;
}

// Determine the unfitness of one component
double gs::FitnessInPhaseLine::unfitnessPhase(const std::vector<double>& phase) {
    // Find the mean phase
    double total = 0.0;
    for(double v : phase) {
        total += v;
    }
    double mean = total / numPointsX_;
    // Calculate the phase unfitness as the square of the distance from the mean in degrees
    double phaseUnfitness = 0.0;
    for(double v : phase) {
        double p = v - mean;
        phaseUnfitness += p * p;
    }
    return phaseUnfitness;
}

// Determine the unfitness of one component
double gs::FitnessInPhaseLine::unfitnessAmplitude(const std::vector<double>& amplitude) {
    // Calculate the amplitude unfitness as the square of the distance less
    // than the desired value
    double amplitudeUnfitness = 0.0;
    for(double v : amplitude) {
        if(v < desiredAmplitude_) {
            double a = desiredAmplitude_ - v;
            amplitudeUnfitness += a * a;
        }
    }
    return amplitudeUnfitness;
}

// Write the configuration to the XML
void gs::FitnessInPhaseLine::writeConfig(xml::DomObject& root) const {
    // Base class
    FitnessBase::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Obj("numpoints") << numPointsX_;
    root << xml::Obj("desiredamplitude") << desiredAmplitude_;
    root << xml::Obj("amplitudeweight") << amplitudeWeight_;
    root << xml::Close();
}

// Read configuration from the XML
void gs::FitnessInPhaseLine::readConfig(xml::DomObject& root) {
    // Base class
    FitnessBase::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Obj("numpoints") >> numPointsX_;
    root >> xml::Obj("desiredamplitude") >> desiredAmplitude_;
    root >> xml::Obj("amplitudeweight") >> amplitudeWeight_;
    root >> xml::Close();
}
