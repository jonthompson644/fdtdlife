/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessAdmittance.h"
#include <Xml/DomObject.h>
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include "Individual.h"
#include "OneDFitnessData.h"
#include <Box/Constants.h>
#include <Sensor/ArraySensor.h>
#include <algorithm>

// Calculate the unfitness
double gs::FitnessAdmittance::unfitness(Individual* ind) {
    auto sensor = s_->getGeneticSensor();
    double result = 0.0;
    double bestScale = 0.0;
    if(sensor != nullptr) {
        // Create the data object
        auto data = dynamic_cast<OneDFitnessData<std::complex<double>>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataAdmittance, "AdmittanceX"));
        data->setSize(min_.size());
        // Record the admittance data
        for(size_t i = 0; i < min_.size(); i++) {
            double transmittance= 0.0;
            double phaseShift = 0.0;
            double other = 0.0;
            sensor->points().at(sensor->index(0,0)).transmittanceAt(i, transmittance, other, false);
            sensor->points().at(sensor->index(0,0)).phaseShiftAt(i, phaseShift, other, false);
            double tanPhaseShift = tan(phaseShift * box::Constants::deg2rad_);
            double denominator = transmittance * sqrt(1 + tanPhaseShift * tanPhaseShift);
            std::complex<double> measured = {1 / denominator - 1, -tanPhaseShift / denominator};
            data->setData(i, measured);
        }
        // Now assess the fitness
        int steps = std::max(numRepeatCellScaleSamples_, 1);
        double scaleStep = 0;
        if(steps > 1) {
            scaleStep = (maxRepeatCellScale_ - minRepeatCellScale_) / (double) (steps - 1);
        }
        bestScale = maxRepeatCellScale_;
        double maxFrequency = (min_.size() + 1) * s_->frequencySpacing();
        bool first = true;
        for(int step = 0; step < steps; step++) {
            double scale = maxRepeatCellScale_ - (double) step * scaleStep;
            double thisOne = 0.0;
            // Integrate under the curve up to the maximum frequency
            for(size_t i = 0; i < min_.size(); i++) {
                double frequency = (i + 1) * s_->frequencySpacing() / scale;
                if(frequency < maxFrequency) {
                    std::complex<double> required = {real_.value(frequency), imag_.value(frequency)};
                    std::complex<double> measured = data->data()[i];
                    std::complex<double> diff = measured - required;
                    thisOne += diff.real() * diff.real() + diff.imag() * diff.imag() / scale;
                }
            }
            // Is this better?
            if(first || thisOne < result) {
                result = thisOne;
                bestScale = scale;
                first = false;
            }
        }
        result *= weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    ind->fitnessResults().back().setExtra("Repeat Cell Scaling Factor", bestScale);
    return result * weight_;
}

// Write the configuration to the DOM object
void gs::FitnessAdmittance::writeConfig(xml::DomObject& root) const {
    // Base class first
    FitnessBase::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("real") << real_;
    root << xml::Obj("imag") << imag_;
    root << xml::Obj("minrepeatcellscale") << minRepeatCellScale_;
    root << xml::Obj("numrepeatcellscalesamples") << numRepeatCellScaleSamples_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::FitnessAdmittance::readConfig(xml::DomObject& root) {
    // Base class first
    FitnessBase::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("real") >> real_;
    root >> xml::Obj("imag") >> imag_;
    root >> xml::Obj("minrepeatcellscale") >> xml::Default(minRepeatCellScale_) >> minRepeatCellScale_;
    root >> xml::Obj("numrepeatcellscalesamples") >> xml::Default(numRepeatCellScaleSamples_) >> numRepeatCellScaleSamples_;
    root >> xml::Close();
    findRangeMinMax();
}

// Set the admittance
void gs::FitnessAdmittance::set(const std::string& name, double weight,
                                  const box::Polynomial& real, const box::Polynomial& imag,
                                  double minRepeatCellScale, int numRepeatCellScaleSamples) {
    FitnessBase::set(name, weight, dataSourceMagnitude);
    set(real, imag);
    minRepeatCellScale_ = minRepeatCellScale;
    numRepeatCellScaleSamples_ = numRepeatCellScaleSamples;
}

// Set the admittance
void gs::FitnessAdmittance::set(const box::Polynomial& real, const box::Polynomial& imag) {
    real_ = real;
    imag_ = imag;
    findRangeMinMax();
}

// Work out the range min and max
void gs::FitnessAdmittance::findRangeMinMax() {
    bool first = true;
    rangeMin_ = 0.0;
    rangeMax_ = 1.0;
    for(size_t i = 0; i < s_->numFrequencies(); i++) {
        double frequency = (i + 1) * s_->frequencySpacing();
        if(first) {
            rangeMin_ = std::min(real_.value(frequency), imag_.value(frequency));
            rangeMax_ = std::max(real_.value(frequency), imag_.value(frequency));
            first = false;
        } else {
            rangeMin_ = std::min(real_.value(frequency), rangeMin_);
            rangeMin_ = std::min(imag_.value(frequency), rangeMin_);
            rangeMax_ = std::max(real_.value(frequency), rangeMax_);
            rangeMax_ = std::max(imag_.value(frequency), rangeMax_);
        }
    }
}

// Return the specification data the function is using, if possible.
void gs::FitnessAdmittance::getFitnessSpec(std::vector<double>& minTrans,
                                             std::vector<double>& maxTrans,
                                             std::vector<double>& minPhase,
                                             std::vector<double>& maxPhase,
                                             const FitnessPartResult* result) {
    minTrans.clear();
    maxTrans.clear();
    minPhase.clear();
    maxPhase.clear();
    double maxFrequency = (s_->numFrequencies() + 1) * s_->frequencySpacing();
    for(size_t i = 0; i < s_->numFrequencies(); i++) {
        double frequency = (i + 1) * s_->frequencySpacing();
        if(result != nullptr) {
            frequency /= result->extraValue();
        }
        if(frequency < maxFrequency) {
            double phaseShift = atan2(-imag_.value(frequency), real_.value(frequency) + 1.0) /
                                box::Constants::deg2rad_;
            minPhase.push_back(phaseShift);
            maxPhase.push_back(phaseShift);
            double tanPhi = -imag_.value(frequency) / (real_.value(frequency) + 1.0);
            double transmission = 1.0 / (real_.value(frequency) + 1.0) / sqrt(1.0 + tanPhi * tanPhi);
            minTrans.push_back(transmission * transmission);
            maxTrans.push_back(transmission * transmission);
        } else {
            minPhase.push_back(-180.0);
            maxPhase.push_back(+180.0);
            minTrans.push_back(0.0);
            maxTrans.push_back(1.0);
        }
    }
}
