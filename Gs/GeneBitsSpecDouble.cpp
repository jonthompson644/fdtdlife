/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsSpecDouble.h"
#include <cmath>
#include <memory>
#include <Xml/DomObject.h>
#include "Chromosome.h"

// Initialising constructor
gs::GeneBitsSpecDouble::GeneBitsSpecDouble(size_t bits, double min, double max) :
        bits_(bits),
        min_(min),
        max_(max) {
}

// Copy constructor
gs::GeneBitsSpecDouble::GeneBitsSpecDouble(const gs::GeneBitsSpecDouble& other) :
        GeneBitsSpecDouble() {
    *this = other;
}

// Equality operator
bool gs::GeneBitsSpecDouble::operator==(const gs::GeneBitsSpecDouble& other) const {
    return bits_ == other.bits_ && min_ == other.min_ &&
           max_ == other.max_;
}

// Inequality operator
bool gs::GeneBitsSpecDouble::operator!=(const gs::GeneBitsSpecDouble& other) const {
    return !(*this == other);
}

// Write the specification to the XML DOM
void gs::GeneBitsSpecDouble::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("bits") << bits_;
    p << xml::Obj("min") << min_;
    p << xml::Obj("max") << max_;
    p << xml::Close();
}

// Read the specification from the XML DOM
void gs::GeneBitsSpecDouble::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("bits") >> xml::Default(bits_) >> bits_;
    p >> xml::Obj("min") >> xml::Default(min_) >> min_;
    p >> xml::Obj("max") >> xml::Default(max_) >> max_;
    p >> xml::Close();
}

// Encode a value into the chromosome
void gs::GeneBitsSpecDouble::encode(Chromosome* c, double value) {
    if(bits_ > 0) {
        auto v = static_cast<uint64_t>(std::round((value - min_) / scaling()));
        c->encode(v, bits_);
    }
}

// Decode a value from the chromosome
double gs::GeneBitsSpecDouble::decode(Chromosome* c) {
    double result = min_;
    if(bits_ > 0) {
        result = static_cast<double>(c->decode(bits_)) * scaling() + min_;
    }
    return result;
}

// Calculate the scaling factor
double gs::GeneBitsSpecDouble::scaling() const {
    double result = 1.0;
    if(bits_ > 0) {
        result = (max_ - min_) / ((double) (1U << bits_) - 1.0);
        if(result <= 0.0) {
            result = 1.0;
        }
    }
    return result;
}


