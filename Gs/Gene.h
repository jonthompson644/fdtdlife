/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENE_H
#define FDTDLIFE_GENE_H

#include <cstddef>
#include "GeneTemplate.h"
namespace fdtd {class Model;}

namespace gs {

    class Chromosome;
    class GeneticSearch;

    // Base class for all chromosome alleles
    class Gene {
    public:
        static constexpr double lengthScaling_ = 0.0000005;  // Lengths to 0.5um resolution
    protected:
        size_t startBits_ {};   // The bit number in the chromosome this gene starts at
        GeneTemplate::GeneType geneType_;
    public:
        explicit Gene(GeneTemplate::GeneType geneType);
        virtual ~Gene() = default;
        virtual void encode(Chromosome* /*c*/) {}
        virtual void decode(Chromosome* /*c*/) {}
        virtual double configureModel(GeneticSearch* /*s*/, unsigned int /*what*/) {return 0.0;}
        virtual bool connect(Chromosome* /*c*/, GeneticSearch* /*s*/) {return true;}
        size_t startBits() const {return startBits_;}
        virtual size_t sizeBits() const {return 0;}
        void startBits(size_t v) {startBits_ = v;}
        virtual void copyFrom(Gene* /*other*/, fdtd::Model* /*m*/) {}
        virtual std::string info(GeneticSearch* /*s*/) const {return "";}
        GeneTemplate::GeneType geneType() const {return geneType_;}
    };

}


#endif //FDTDLIFE_GENE_H
