/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "TwoDFitnessData.h"
#include <complex>

// Write the data to the XML DOM
template <class T>
void gs::TwoDFitnessData<T>::write(xml::DomObject& root) const {
    // Base class first
    FitnessData::write(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("xsize") << xSize_;
    root << xml::Obj("ysize") << ySize_;
    root << xml::Obj("data") << data_;
    root << xml::Close();
}

// Read the data from the XML DOM
template <class T>
void gs::TwoDFitnessData<T>::read(xml::DomObject& root) {
    // Base class first
    FitnessData::read(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("xsize") >> xSize_;
    root >> xml::Obj("ysize") >> ySize_;
    root >> xml::Obj("data") >> data_;
    root >> xml::Close();
}

// Set the size of the data array
template <class T>
void gs::TwoDFitnessData<T>::setSize(size_t x, size_t y) {
    data_.resize(x * y);
    xSize_ = x;
    ySize_ = y;
}

// Write a data value into the array
template <class T>
void gs::TwoDFitnessData<T>::setData(size_t x, size_t y, T v) {
    data_.at(x + y * xSize_) = v;
}

// Read a value from the array
template<class T>
T gs::TwoDFitnessData<T>::at(size_t x, size_t y) const {
    return data_.at(x + y * xSize_);
}

template class gs::TwoDFitnessData<double>;
template class gs::TwoDFitnessData<std::complex<double>>;
