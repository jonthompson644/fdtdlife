/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Fitness3dBPoint.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include "OneDFitnessData.h"
#include "Individual.h"
#include <Xml/DomObject.h>
#include <Sensor/ArraySensor.h>

// Calculate the unfitness parameter for the specified data.
// The bigger this number, the less fit is the solution.
// We will use the sum of the square of the difference.
// This penalises a few large excursions from the desired over
// many small excursions.  This overridden version handles the
// the requirement for a 3dB point at a particular frequency.
double gs::Fitness3dBPoint::unfitness(Individual* ind) {
    auto sensor = s_->getGeneticSensor();
    double result = 0.0;
    if(sensor != nullptr) {
        // Create the data object
        std::string dataName;
        if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
            dataName = "PropMatTransmittance";
        } else if(dataSource_ == dataSourceYComponent) {
            dataName = "TransmittanceY";
        } else {
            dataName = "TransmittanceX";
        }
        auto data = dynamic_cast<OneDFitnessData<double>*>(
                ind->getFitnessData(s_, GeneticSearch::fitnessDataTransmittance, dataName));
        data->setSize(min_.size());
        // Record the data
        for(size_t i = 0; i < min_.size(); i++) {
            double measured = 0.0;
            double extra = 0.0;
            if(s_->algorithm() == GeneticSearch::algorithmPropMatrix) {
                sensor->points().at(sensor->index(0,0)).propMatTransmittanceAt(i, measured);
            } else if(dataSource_ == dataSourceYComponent) {
                sensor->points().at(sensor->index(0,0)).transmittanceAt(i, extra, measured, false);
            } else {
                sensor->points().at(sensor->index(0,0)).transmittanceAt(i, measured, extra, false);
            }
            data->setData(i, measured);
        }
        // Find the data bins each side of the desired frequency
        double frequencyStep = s_->frequencyRange() / (double) s_->numFrequencies();
        int below = (int) floor(frequency_  * box::Constants::giga_ / frequencyStep);
        int above = (int) ceil(frequency_  * box::Constants::giga_ / frequencyStep);
        // Now get the estimate for the transmission at this frequency
        double value = 0.0;
        double x1 = 0.0;
        double y1 = 0.0;
        double x2 = 0.0;
        double y2 = 0.0;
        if(dataSource_ == dataSourceYComponent) {
            if(below < (int) min_.size()) {
                if(above < (int) min_.size()) {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(below, x1, y1, false);
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(above, x2, y2, false);
                    value = (y1 + y2) / 2.0;
                } else {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(below, x1, y1, false);
                    value = y1;
                }
            } else {
                if(above < (int) min_.size()) {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(above, x2, y2, false);
                    value = y2;
                }
            }
        } else {
            if(below < (int) min_.size()) {
                if(above < (int) min_.size()) {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(below, x1, y1, false);
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(above, x2, y2, false);
                    value = (x1 + x2) / 2.0;
                } else {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(below, x1, y1, false);
                    value = x1;
                }
            } else {
                if(above < (int) min_.size()) {
                    sensor->points().at(sensor->index(0,0)).phaseShiftAt(above, x2, y2, false);
                    value = x2;
                }
            }
        }
        // Calculate the distance from the 3dB point
        result = fabs(value - 0.5);
        // Use the square of the distance to penalise harder when further away
        result = result * result * weight_;
    }
    ind->fitnessResults().emplace_back(FitnessPartResult(
            (int) ind->fitnessResults().size(),
            name_.c_str(), result));
    return result;
}

// Write the configuration to the DOM object
void gs::Fitness3dBPoint::writeConfig(xml::DomObject& root) const {
    // Base class
    FitnessBase::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void gs::Fitness3dBPoint::readConfig(xml::DomObject& root) {
    // Base class
    FitnessBase::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> xml::Default(frequency_) >> frequency_;
    root >> xml::Close();
}

