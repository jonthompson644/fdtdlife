/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LayerSymmetryGene.h"
#include "Chromosome.h"

// Constructor
gs::LayerSymmetryGene::LayerSymmetryGene() :
        Gene(GeneTemplate::geneTypeLayerSymmetry) {
}

// Connect the gene to other genes it depends on
// and work out which layer this is
bool gs::LayerSymmetryGene::connect(Chromosome* /*c*/, GeneticSearch* /*s*/) {
    return true;
}

// Encode the gene into the chromosome
void gs::LayerSymmetryGene::encode(Chromosome* c) {
    Gene::encode(c);
    c->encode((uint64_t)symmetry_, symmetryBits_);
}

// Decode the gene from the chromosome
void gs::LayerSymmetryGene::decode(Chromosome* c) {
    Gene::decode(c);
    // Convert the 2 bit code into a one of three enumeration
    // This explicit conversion avoids any assumption regarding the
    // values of the symbols.
    switch(c->decode(symmetryBits_)) {
        default:
        case GeneticSearch::layerSymmetryNone:
            symmetry_ = GeneticSearch::layerSymmetryNone;
            break;
        case GeneticSearch::layerSymmetryEven:
            symmetry_ = GeneticSearch::layerSymmetryEven;
            break;
        case GeneticSearch::layerSymmetryOdd:
            symmetry_ = GeneticSearch::layerSymmetryOdd;
            break;
    }
}

// Return the number of bits used by the gene
size_t gs::LayerSymmetryGene::sizeBits() const {
    return Gene::sizeBits() + symmetryBits_;
}

// Copy the state from the other gene into this one
void gs::LayerSymmetryGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
    auto* from = dynamic_cast<LayerSymmetryGene*>(other);
    if(from != nullptr) {
        symmetry_ = from->symmetry_;
    }
}
