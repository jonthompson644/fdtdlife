/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnLayerGene.h"
#include "GeneticSearch.h"
#include <Fdtd/Model.h>
#include <Domain/AdmittanceFnLayer.h>
#include "FitnessAdmittance.h"
#include "FitnessFunction.h"

// Constructor
gs::AdmittanceFnLayerGene::AdmittanceFnLayerGene(const GeneBitsSpecDouble& realA,
                                                 const GeneBitsSpecLogDouble& realB,
                                                 const GeneBitsSpecLogDouble& realC,
                                                 const GeneBitsSpecLogDouble& realD,
                                                 const GeneBitsSpecLogDouble& realE,
                                                 const GeneBitsSpecLogDouble& realF,
                                                 const GeneBitsSpecDouble& imagA,
                                                 const GeneBitsSpecLogDouble& imagB,
                                                 const GeneBitsSpecLogDouble& imagC,
                                                 const GeneBitsSpecLogDouble& imagD,
                                                 const GeneBitsSpecLogDouble& imagE,
                                                 const GeneBitsSpecLogDouble& imagF) :
        UnitCellLayerGene(GeneTemplate::geneTypeAdmittanceFn, 0, 1),
        realA_(realA),
        realB_(realB),
        realC_(realC),
        realD_(realD),
        realE_(realE),
        realF_(realF),
        imagA_(imagA),
        imagB_(imagB),
        imagC_(imagC),
        imagD_(imagD),
        imagE_(imagE),
        imagF_(imagF) {
}

// Encode the gene into the chromosome
void gs::AdmittanceFnLayerGene::encode(Chromosome* c) {
    UnitCellLayerGene::encode(c);
    realA_.encode(c, real_.a());
    realB_.encode(c, real_.b());
    realC_.encode(c, real_.c());
    realD_.encode(c, real_.d());
    realE_.encode(c, real_.e());
    realF_.encode(c, real_.f());
    imagA_.encode(c, imag_.a());
    imagB_.encode(c, imag_.b());
    imagC_.encode(c, imag_.c());
    imagD_.encode(c, imag_.d());
    imagE_.encode(c, imag_.e());
    imagF_.encode(c, imag_.f());
}

// Decode the gene from the chromosome
void gs::AdmittanceFnLayerGene::decode(Chromosome* c) {
    UnitCellLayerGene::decode(c);
    real_.a(realA_.decode(c));
    real_.b(realB_.decode(c));
    real_.c(realC_.decode(c));
    real_.d(realD_.decode(c));
    real_.e(realE_.decode(c));
    real_.f(realF_.decode(c));
    imag_.a(imagA_.decode(c));
    imag_.b(imagB_.decode(c));
    imag_.c(imagC_.decode(c));
    imag_.d(imagD_.decode(c));
    imag_.e(imagE_.decode(c));
    imag_.f(imagF_.decode(c));
}

// Connect the gene to other genes it depends on
bool gs::AdmittanceFnLayerGene::connect(Chromosome* c, GeneticSearch* s) {
    return UnitCellLayerGene::connect(c, s);
}

// Set the model configuration corresponding to this allele
double gs::AdmittanceFnLayerGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(present_ && (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_;
        makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_;
            makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Set the model configuration corresponding to this allele
void
gs::AdmittanceFnLayerGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
                                     double zLocation) {
    // Find or create the shape
    int shapeId = s->layerShapeId(layer);
    domain::Shape* shape = s->m()->d()->getShape(shapeId);
    auto plate = dynamic_cast<domain::AdmittanceFnLayer*>(shape);
    if(plate == nullptr) {
        s->m()->d()->remove(shape);
        plate = dynamic_cast<domain::AdmittanceFnLayer*>(s->m()->d()->makeShape(
                shapeId, domain::Domain::shapeModeAdmittanceFn));
    }
    if((what & GeneticSearch::individualLoadDomainSize) != 0) {
        plate->cellSizeX(box::Expression(unitCell_));
    } else {
        plate->cellSizeX(box::Expression(s->m()->p()->dr().x().value()));
    }
    std::stringstream name;
    name << "GeneticLayer" << layer;
    plate->name(name.str());
    plate->center().value(box::Vector<double>(0.0));
    plate->real(real_);
    plate->imag(imag_);
    plate->center().value(box::Vector<double>(0.0, 0.0, zLocation));
    plate->seqGenerated(true);
}

// Return the number of bits used by the gene
size_t gs::AdmittanceFnLayerGene::sizeBits() const {
    return UnitCellLayerGene::sizeBits() +
           realA_.bits() + realB_.bits() + realC_.bits() +
           realD_.bits() + realE_.bits() + realF_.bits() +
           imagA_.bits() + imagB_.bits() + imagC_.bits() +
           imagD_.bits() + imagE_.bits() + imagF_.bits();
}

// Copy the state from the other gene into this one
void gs::AdmittanceFnLayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    UnitCellLayerGene::copyFrom(other, m);
    auto* from = dynamic_cast<AdmittanceFnLayerGene*>(other);
    if(from != nullptr) {
        real_ = from->real();
        imag_ = from->imag();
    }
}

// Make an admittance fitness function using this gene's admittance
gs::FitnessAdmittance* gs::AdmittanceFnLayerGene::makeAdmittanceFunction(GeneticSearch* s) {
    auto* fitnessFunction = dynamic_cast<gs::FitnessAdmittance*>(
            s->fitness()->make(gs::FitnessFunction::modeAdmittance));
    fitnessFunction->set(real_, imag_);
    return fitnessFunction;
}
