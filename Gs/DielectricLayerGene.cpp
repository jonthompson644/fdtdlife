/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <cmath>
#include <sstream>
#include "DielectricLayerGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Domain/DielectricLayer.h>
#include <Domain/Material.h>
#include <Box/Constants.h>

// Constructor
gs::DielectricLayerGene::DielectricLayerGene() :
        LayerGene(GeneTemplate::geneTypeDielectricLayer),
        refractiveIndex_(0.0),
        thickness_(0.0) {
}

// Encode the allele into the chromosome
void gs::DielectricLayerGene::encode(Chromosome* c) {
    LayerGene::encode(c);
    c->encode((uint64_t) std::round((thickness_ - minThickness_) / lengthScaling_),
              thicknessBits_);
    c->encode((uint64_t) std::round(refractiveIndex_ / refractiveIndexScaling_),
              refractiveIndexBits_);
}

// Decode the allele from the chromosome
void gs::DielectricLayerGene::decode(Chromosome* c) {
    LayerGene::decode(c);
    thickness_ = (double) c->decode(thicknessBits_) * lengthScaling_ + minThickness_;
    refractiveIndex_ = (double) c->decode(refractiveIndexBits_) *
                       refractiveIndexScaling_;
}

// Connect the gene to other genes it depends on
bool gs::DielectricLayerGene::connect(Chromosome* c, GeneticSearch* s) {
    bool result = LayerGene::connect(c, s);
    // Validate myself
    return result && refractiveIndex_ >= minRefractiveIndex;
}

// Set the model configuration corresponding to this allele
double gs::DielectricLayerGene::configureModel(GeneticSearch* s, unsigned int what) {
    double result = 0.0;
    if(present_ && (what & GeneticSearch::individualLoadShapes) != 0) {
        // The main layer
        result = zLocation_ + thickness_;
        makeLayer(s, what, layer_, zLocation_);
        // The symmetry layer
        if(hasSymmetryLayer_) {
            result = symmetryZLocation_ + thickness_;
            makeLayer(s, what, symmetryLayer_, symmetryZLocation_);
        }
    }
    return result;
}

// Set the model configuration corresponding to this allele
void gs::DielectricLayerGene::makeLayer(GeneticSearch* s, unsigned int what, int layer,
                                        double zLocation) {
    // Set the refractive index of the material
    domain::Material* mat = material(s);
    mat->epsilon(box::Expression(box::Constants::epsilon0_ * refractiveIndex_));
    // Find or create the shape
    int shapeId = s->layerShapeId(layer);
    domain::Shape* shape = s->m()->d()->getShape(shapeId);
    auto dielectricLayer = dynamic_cast<domain::DielectricLayer*>(shape);
    if(dielectricLayer == nullptr) {
        s->m()->d()->remove(shape);
        dielectricLayer = dynamic_cast<domain::DielectricLayer*>(s->m()->d()->makeShape(
                shapeId, domain::Domain::shapeModeDielectricLayer));
    }
    // Set the shape parameters
    if((what & GeneticSearch::individualLoadDomainSize) != 0) {
        dielectricLayer->cellSizeX(box::Expression(repeatCell_));
    } else {
        dielectricLayer->cellSizeX(box::Expression(s->m()->p()->p2().x().value() -
                                                   s->m()->p()->p1().x().value()));
    }
    std::stringstream name;
    name << "GeneticLayer" << layer;
    dielectricLayer->name(name.str());
    dielectricLayer->material(mat->index());
    dielectricLayer->thickness(box::Expression(thickness_));
    dielectricLayer->center().value(box::Vector<double>(0.0, 0.0, zLocation));
    dielectricLayer->seqGenerated(true);
}

// Return the number of bits used by the gene
size_t gs::DielectricLayerGene::sizeBits() const {
    return LayerGene::sizeBits() + refractiveIndexBits_ + thicknessBits_;
}

// Copy the state from the other gene into this one
void gs::DielectricLayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    LayerGene::copyFrom(other, m);
    auto* from = dynamic_cast<DielectricLayerGene*>(other);
    if(from != nullptr) {
        refractiveIndex_ = from->refractiveIndex_;
        thickness_ = from->thickness_;
    }
}
