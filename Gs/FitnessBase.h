/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSBASE_H
#define FDTDLIFE_FITNESSBASE_H

#include "FitnessPoint.h"
#include "OneDFitnessData.h"
#include <list>
#include <vector>
#include <string>
#include <Xml/DomObject.h>

namespace gs {

    class GeneticSearch;

    class Individual;

    class FitnessPartResult;

    class Individual;

    // A fitness function that can be used for attenuation and
    // as the base class for other fitness functions
    class FitnessBase {
    public:
        // Constants
        enum DataSource {
            dataSourceMagnitude = 0, dataSourceXComponent, dataSourceYComponent
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, DataSource& v) {
            o >> (int&) v;
            return o;
        }

        // Construction
        FitnessBase() = default;
        virtual void construct(int mode, int id, GeneticSearch* s);
        virtual ~FitnessBase() = default;

        // API
        virtual void initialise(size_t numSamples, double firstSample,
                                double sampleSpacing);
        virtual double unfitness(Individual* ind);
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const FitnessBase& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, FitnessBase& v) {
            v.readConfig(o);
            return o;
        }
        virtual void addPoint(double frequency, double min, double max);
        void deletePoint(int index);
        void setPoint(int index, double frequency, double min, double max);
        virtual void getFitnessSpec(std::vector<double>& minTrans,
                                    std::vector<double>& maxTrans,
                                    std::vector<double>& minPhase,
                                    std::vector<double>& maxPhase,
                                    const FitnessPartResult* result);
        virtual std::string makeTsvData(size_t /*numFrequencies*/, double /*firstFrequency*/,
                                        double /*frequencySpacing*/) { return ""; }
        virtual void stepComplete() {}

        // Getters
        const std::list<FitnessPoint>& points() const { return points_; }
        const std::string& name() const { return name_; }
        double weight() const { return weight_; }
        double rangeMin() const { return rangeMin_; }
        double rangeMax() const { return rangeMax_; }
        DataSource dataSource() const { return dataSource_; }
        const std::vector<double>& min() const { return min_; }
        const std::vector<double>& max() const { return max_; }
        int mode() const { return mode_; }
        GeneticSearch* s() const { return s_; }

        // Setters
        void set(const std::string& name, double weight, DataSource dataSource);

    protected:
        // Configuration
        int identifier_{-1};
        int mode_{-1};
        std::list<FitnessPoint> points_;
        std::vector<double> min_;
        std::vector<double> max_;
        double weight_{1.0};
        std::string name_;
        double rangeMin_{0.0};
        double rangeMax_{1.0};
        DataSource dataSource_{dataSourceMagnitude};

        // Members
        GeneticSearch* s_{};

        // Helpers
        OneDFitnessData<double>* getTransmittanceData(Individual* ind, bool withRange);
        OneDFitnessData<double>* getPhaseShiftData(Individual* ind, bool withRange);
        OneDFitnessData<double>* getTimeSeriesData(Individual* ind);
    };
}


#endif //FDTDLIFE_FITNESSBASE_H
