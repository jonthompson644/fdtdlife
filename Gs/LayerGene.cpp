/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <sstream>
#include "LayerGene.h"
#include "Chromosome.h"
#include <Fdtd/Model.h>
#include "GeneticSearch.h"
#include <Domain/Material.h>
#include "LayerSpacingGene.h"
#include "RepeatCellGene.h"
#include "LayerPresentGene.h"
#include "LayerSymmetryGene.h"
#include <Box/Constants.h>

// Constructor
gs::LayerGene::LayerGene(GeneTemplate::GeneType geneType) :
        Gene(geneType),
        layer_(0),
        zLocation_(0.0),
        symmetryZLocation_(0.0),
        hasSymmetryLayer_(false),
        repeatCell_(0.0),
        present_(true) {
}

// Connect the gene to other genes it depends on
// and work out which layer this is
bool gs::LayerGene::connect(Chromosome* c, GeneticSearch* s) {
    // Find the first symmetry gene for the layer symmetry
    hasSymmetryLayer_ = false;
    layerSymmetry_ = s->layerSymmetry();
    for(auto g : c->genes()) {
        auto gene = dynamic_cast<LayerSymmetryGene*>(g);
        if(gene != nullptr) {
            layerSymmetry_ = gene->symmetry();
            break;
        }
    }
    // Find the first repeat cell gene for the unit cell
    repeatCell_ = s->repeatCell();
    for(auto g : c->genes()) {
        auto gene = dynamic_cast<RepeatCellGene*>(g);
        if(gene != nullptr) {
            repeatCell_ = gene->repeatCell();
            break;
        }
    }
    // Find the last layer spacing gene before me for the spacing
    layerSpacing_ = s->layerSpacing();
    for(auto g : c->genes()) {
        if(g == this) {
            break;
        } else {
            auto gene = dynamic_cast<LayerSpacingGene*>(g);
            if(gene != nullptr) {
                layerSpacing_ = gene->layerSpacing();
            }
        }
    }
    // Find the last layer present gene before me but after the previous layer for presence
    present_ = true;
    for(auto g : c->genes()) {
        if(g == this) {
            break;
        } else if(dynamic_cast<LayerGene*>(g) != nullptr) {
            present_ = true;
        } else {
            auto gene = dynamic_cast<LayerPresentGene*>(g);
            if(gene != nullptr) {
                present_ = gene->present();
            }
        }
    }
    // Find the layer genes before me to work out my position
    layer_ = 0;
    zLocation_ = 0.0;
    for(auto g : c->genes()) {
        auto gene = dynamic_cast<LayerGene*>(g);
        if(gene != nullptr) {
            if(gene == this) {
                // It's me, we are done
                break;
            } else if(gene->present()) {
                layer_++;
                zLocation_ = gene->zLocation_ + gene->thickness();
            }
        }
    }
    // Is this the last layer gene?
    bool theLast = false;
    for(auto g : c->genes()) {
        auto gene = dynamic_cast<LayerGene*>(g);
        if(gene != nullptr) {
            if(gene == this) {
                theLast = true;
            } else if(theLast) {
                theLast = false;
            }
        }
    }
    if(theLast) {
        // Now we have positioned the last layer gene we need to handle the symmetry modes
        if(layerSymmetry_ != GeneticSearch::layerSymmetryNone) {
            for(auto pos = c->genes().rbegin(); pos != c->genes().rend(); ++pos) {
                auto gene = dynamic_cast<LayerGene*>(*pos);
                if(gene != nullptr) {
                    gene->symmetryConnect(c, s);
                }
            }
        }
    }
    return true;
}

// Encode the gene into the chromosome
void gs::LayerGene::encode(Chromosome* c) {
    Gene::encode(c);
}

// Decode the gene from the chromosome
void gs::LayerGene::decode(Chromosome* c) {
    Gene::decode(c);
}

// Find or create the active material the layer is made of
domain::Material* gs::LayerGene::material(GeneticSearch* s) {
    int materialId = s->layerMaterialId(layer_);
    domain::Material* material = s->m()->d()->getMaterial(materialId);
    if(material == nullptr) {
        std::stringstream name;
        name << "GeneticMaterial" << layer_;
        box::Constants::Colour color = box::Constants::colourYellow;
        if((static_cast<unsigned int>(layer_) & 1U) != 0) {
            color = box::Constants::colourCyan;
        }
        material = s->m()->d()->makeMaterial(
                materialId, domain::Domain::materialModeNormal).get();
        material->name(name.str());
        material->epsilon(box::Expression(box::Constants::epsilon0_));
        material->mu(box::Expression(box::Constants::mu0_));
        material->sigma(box::Expression(box::Constants::sigma0_));
        material->sigmastar(box::Expression(box::Constants::sigmastar0_));
        material->color(color);
        material->priority(0);
        material->seqGenerated(true);
    }
    return material;
}

// Return the number of bits used by the gene
size_t gs::LayerGene::sizeBits() const {
    return Gene::sizeBits();
}

// Connect the symmetry layers
void gs::LayerGene::symmetryConnect(Chromosome* c, GeneticSearch* /*s*/) {
    // If this layer was present in the first half...
    if(present_) {
        // Find the layer gene after me to work out my position
        symmetryZLocation_ = 0.0;
        bool looking = false;
        bool found = false;
        for(auto g : c->genes()) {
            auto gene = dynamic_cast<LayerGene*>(g);
            if(gene != nullptr) {
                if(gene == this) {
                    // It's me, we can start looking now
                    looking = true;
                } else if(looking && gene->present_) {
                    // This is the gene after me
                    hasSymmetryLayer_ = true;
                    symmetryLayer_ = gene->symmetryLayer_ + 1;
                    symmetryZLocation_ = gene->symmetryZLocation_ + layerSpacing_;
                    found = true;
                    break;
                }
            }
        }
        if(!found) {
            // There was no layer after me, so we must be the first symmetry gene
            if(layerSymmetry_ == GeneticSearch::layerSymmetryEven) {
                // in even layer symmetry, we duplicate the middle layer
                hasSymmetryLayer_ = true;
                symmetryLayer_ = layer_ + 1;
                symmetryZLocation_ = zLocation_ + layerSpacing_;
            } else {
                // In odd layer symmetry, we do not duplicate the middle layer
                hasSymmetryLayer_ = false;
                symmetryLayer_ = layer_;
                symmetryZLocation_ = zLocation_;
            }
        }
    }
}

// Copy the state from the other gene into this one
void gs::LayerGene::copyFrom(Gene* other, fdtd::Model* m) {
    Gene::copyFrom(other, m);
}
