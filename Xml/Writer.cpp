/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Writer.h"
#include "DomDocument.h"
#include "Exception.h"
#include <sstream>
#include <fstream>

// Converts a DOM into text
void xml::Writer::writeString(DomDocument* dom, std::string& text) {
    std::stringstream stream;
    text_ = &stream;
    dom->write(*this, "");
    text = stream.str();
}

// Write a DOM to a file
void xml::Writer::writeFile(DomDocument* dom, const std::string& fileName) {
    std::fstream file;
    file.open(fileName, std::ios::out | std::ios::binary | std::ios::trunc);
    if(file.is_open()) {
        // Parse the DOM
        text_ = &file;
        dom->write(*this, "");
        // Clean up
        file.close();
    } else {
        throw Exception("Could not open file " + fileName);
    }
}

// Write the text to the output
void xml::Writer::write(const std::string& text) {
    *text_ << text;
}
