#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DomObject.h"
#include "DomItem.h"
#include "Exception.h"
#include "DomMarkup.h"
#include "Reader.h"
#include "Writer.h"

// Destructor
xml::DomObject::~DomObject() {
    // Delete all the children
    for(auto& child : children_) {
        delete child;
    }
    children_.clear();
}

// Initialising constructor.
xml::DomObject::DomObject(std::string name) :
        name_(std::move(name)) {
}

// Adds an attribute to the object.  Throws XmlException if the attribute already
// exists.
void xml::DomObject::addAttribute(std::string const& name, std::string const& value) {
    auto pos = attributes_.find(name);
    if(pos == attributes_.end()) {
        attributes_[name] = value;
    } else {
        throw Exception("Duplicate attribute: " + name);
    }
}

// Adds a child item to the object.
void xml::DomObject::addNestedItem(DomItem* item) {
    children_.push_back(item);
}

// Writes the XML text to the given string.
void xml::DomObject::write(Writer& lexer, std::string const& indent) {
    // Write the tag open
    lexer.write(indent + "<" + name_);
    // Write the attributes
    for(auto& attribute : attributes_) {
        std::string attr = attribute.second;
        lexer.entities().insert(attr);
        lexer.write(" ");
        lexer.write(attribute.first);
        lexer.write("=\"");
        lexer.write(attr);
        lexer.write("\"");
    }
    // Are there any nested objects?
    if(children_.empty()) {
        // End an empty tag
        lexer.write("/>\n");
    } else {
        // End the open tag
        lexer.write(">");
        bool needsNewLine = children_.size() > 1;
        needsNewLine = needsNewLine ||
                       (children_.size() == 1 &&
                        dynamic_cast<DomObject*>(children_.front()) != nullptr);
        if(needsNewLine) {
            lexer.write("\n");
        }
        // Write the children
        std::string nextIndent = indent + "    ";
        for(auto& childPos : children_) {
            childPos->write(lexer, nextIndent);
        }
        // Write the closing tag
        if(needsNewLine) {
            lexer.write(indent);
        }
        lexer.write("</");
        lexer.write(name_);
        lexer.write(">\n");
    }
}

// Translates the text into DOM objects.
void xml::DomObject::read(Reader& lexer) {
    std::string attribute;
    std::string value;
    // The start of the open tag has already been read
    // Read the attributes
    bool going = true;
    while(going) {
        lexer.getOptionalSpaceToken();
        if(!lexer.isExactToken(">") && !lexer.isExactToken("/>")) {
            lexer.getIdentifierToken(attribute);
            lexer.getOptionalSpaceToken();
            lexer.getExactToken("=");
            lexer.getOptionalSpaceToken();
            lexer.getStringToken(value);
            addAttribute(attribute, value);
        } else {
            going = false;
        }
    }
    // Are there any nested objects?
    if(lexer.isExactToken(">")) {
        // Yes
        lexer.getExactToken(">");
        // Read them
        going = true;
        while(going) {
            lexer.getOptionalSpaceToken();
            if(lexer.isExactToken("</")) {
                // End of object
                lexer.getExactToken("</");
                lexer.getExactToken(name_.c_str());
                lexer.getOptionalSpaceToken();
                lexer.getExactToken(">");
                going = false;
            } else if(lexer.isExactToken("<")) {
                // Nested object
                lexer.getExactToken("<");
                std::string tag;
                lexer.getIdentifierToken(tag);
                auto* object = new DomObject(tag);
                addNestedItem(object);
                object->read(lexer);
            } else {
                // Markup text
                auto* markup = new DomMarkup;
                addNestedItem(markup);
                markup->read(lexer);
            }
        }
    } else {
        // No
        lexer.getExactToken("/>");
    }
}

// Returns the text content of this object
std::string xml::DomObject::getTextContent() {
    std::string result;
    for(auto& child : children_) {
        auto* m = dynamic_cast<DomMarkup*>(child);
        if(m != nullptr) {
            result += m->markup();
        }
    }
    return result;
}

// The next insertion will be into a new object of the given name
xml::DomObject& xml::DomObject::operator<<(const Obj& manip) {
    checkMode(false);
    doObj(manip);
    return *this;
}

// The next insertion will be into an attribute of the given name
xml::DomObject& xml::DomObject::operator<<(const Attr& manip) {
    checkMode(false);
    doAttr(manip);
    return *this;
}

// The next sequence of insertions will be into this new object
xml::DomObject& xml::DomObject::operator<<(const Open& manip) {
    checkMode(false);
    DomObject* p;
    makeStreamContext();
    if(c_->objQ_.empty()) {
        p = this;
    } else {
        p = c_->objQ_.front();
    }
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            if(manip.name_.empty()) {
                c_->objQ_.push_front(p);
            } else {
                auto* o = new DomObject(manip.name_);
                p->addNestedItem(o);
                c_->objQ_.push_front(o);
                o->mode_ = mode_;
            }
            c_->state_ = StreamContext::State::idle;
            break;
        case StreamContext::State::objectName:
            if(manip.name_.empty()) {
                auto* o = new DomObject(c_->nextName_);
                p->addNestedItem(o);
                c_->objQ_.push_front(o);
                o->mode_ = mode_;
            } else {
                throw Exception("Unused object name: " + c_->nextName_);
            }
            c_->state_ = StreamContext::State::idle;
            break;
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
        case StreamContext::State::objectDefault:
            throw Exception("Unexpected default: " + c_->nextDefault_);
    }
    return *this;
}

// The next sequence of insertions will be into this reopened object
xml::DomObject& xml::DomObject::operator<<(const Reopen& /*manip*/) {
    checkMode(false);
    doReopen();
    return *this;
}

// A sequence of insertions to an object is complete
xml::DomObject& xml::DomObject::operator<<(const Close& manip) {
    doClose(manip);
    return *this;
}

// The next extraction will be from an object with the given name
xml::DomObject& xml::DomObject::operator>>(const Obj& manip) {
    checkMode(true);
    doObj(manip);
    return *this;
}

// The next extraction will be from an attribute with the given name
xml::DomObject& xml::DomObject::operator>>(const Attr& manip) {
    checkMode(true);
    doAttr(manip);
    return *this;
}

// The next sequence of extractions will be into this new object
// If the name is blank, we use the name left by a previous Obj
xml::DomObject& xml::DomObject::operator>>(const Open& manip) {
    checkMode(true);
    makeStreamContext();
    DomObject* p;
    if(c_->objQ_.empty()) {
        p = this;
    } else {
        p = c_->objQ_.front();
    }
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            if(manip.name_.empty()) {
                c_->objQ_.push_front(p);
            } else {
                DomObject* o = p->findChildObject(manip.name_);
                if(o != nullptr) {
                    c_->objQ_.push_front(o);
                    o->mode_ = mode_;
                } else {
                    throw Exception("No nested object named: " + manip.name_);
                }
            }
            c_->state_ = StreamContext::State::idle;
            break;
        case StreamContext::State::objectName:
            if(manip.name_.empty()) {
                DomObject* o = p->findChildObject(c_->nextName_);
                if(o != nullptr) {
                    c_->objQ_.push_front(o);
                    o->mode_ = mode_;
                } else {
                    throw Exception("No nested object named: " + c_->nextName_);
                }
            } else {
                throw Exception("Unused object name: " + c_->nextName_);
            }
            c_->state_ = StreamContext::State::idle;
            break;
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
        case StreamContext::State::objectDefault:
            throw Exception("Unexpected default: " + c_->nextDefault_);
    }
    return *this;
}

// Reopen an object just closed
xml::DomObject& xml::DomObject::operator>>(const Reopen& /*manip*/) {
    checkMode(true);
    doReopen();
    return *this;
}

// A sequence of extractions from an object is complete
xml::DomObject& xml::DomObject::operator>>(const Close& manip) {
    checkMode(true);
    doClose(manip);
    return *this;
}

// A default value for the next extraction
xml::DomObject& xml::DomObject::operator>>(const Default& manip) {
    makeStreamContext();
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
        case StreamContext::State::objectDefault:
        case StreamContext::State::attrDefault:
            throw Exception("Unexpected default value: " + c_->nextDefault_);
        case StreamContext::State::objectName:
            c_->nextDefault_ = manip.value_;
            c_->state_ = StreamContext::State::objectDefault;
            break;
        case StreamContext::State::attrName:
            c_->nextDefault_ = manip.value_;
            c_->state_ = StreamContext::State::attrDefault;
            break;
    }
    return *this;
}

// The next extraction or insertion will be from/to an object with the given name
void xml::DomObject::doObj(const Obj& manip) {
    makeStreamContext();
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            c_->nextName_ = manip.name_;
            c_->state_ = StreamContext::State::objectName;
            break;
        case StreamContext::State::objectName:
        case StreamContext::State::objectDefault:
            throw Exception("Unused object name: " + c_->nextName_);
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
    }
}

// The next extraction or insertion will be from/to an attribute with the given name
void xml::DomObject::doAttr(const Attr& manip) {
    makeStreamContext();
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            c_->nextName_ = manip.name_;
            c_->state_ = StreamContext::State::attrName;
            break;
        case StreamContext::State::objectName:
        case StreamContext::State::objectDefault:
            throw Exception("Unused object name: " + c_->nextName_);
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
    }
}

// A sequence of insertions or extractions to/from an object is complete
void xml::DomObject::doClose(const Close& manip) {
    makeStreamContext();
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            if(c_->objQ_.empty()) {
                throw Exception("No matching open to this close");
            } else if(!manip.name_.empty() && c_->objQ_.front()->name_ != manip.name_) {
                throw Exception("Object closure name mismatch: " + manip.name_ +
                                " != " + c_->objQ_.front()->name_);
            } else {
                c_->prevOpen_ = c_->objQ_.front();
                c_->objQ_.pop_front();
                c_->state_ = StreamContext::State::justClosed;
            }
            break;
        case StreamContext::State::objectName:
        case StreamContext::State::objectDefault:
            throw Exception("Unused object name: " + c_->nextName_);
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
    }
}

// Insert a value as a child or an attribute of the current object
void xml::DomObject::insertValue(const std::string& value) {
    checkMode(false);
    DomObject* p;
    makeStreamContext();
    if(c_->objQ_.empty()) {
        p = this;
    } else {
        p = c_->objQ_.front();
    }
    switch(c_->state_) {
        case StreamContext::State::idle:
        case StreamContext::State::justClosed:
            throw Exception("Missing object/attribute name");
        case StreamContext::State::objectName: {
            auto* o = new DomObject(c_->nextName_);
            o->addNestedItem(new DomMarkup(value));
            p->addNestedItem(o);
            c_->state_ = StreamContext::State::idle;
            break;
        }
        case StreamContext::State::attrName: {
            auto pos = p->attributes_.find(c_->nextName_);
            if(pos == p->attributes_.end()) {
                p->attributes_[c_->nextName_] = value;
            } else {
                throw Exception("Duplicate attribute: " + c_->nextName_);
            }
            c_->state_ = StreamContext::State::idle;
            break;
        }
        case StreamContext::State::objectDefault:
        case StreamContext::State::attrDefault:
            throw Exception("Unexpected default: " + c_->nextDefault_);
    }
}

// Extract a value from a child or an attribute of the current object
std::string xml::DomObject::extractValue() {
    checkMode(true);
    std::string result;
    DomObject* p;
    makeStreamContext();
    if(c_->objQ_.empty()) {
        p = this;
    } else {
        p = c_->objQ_.front();
    }
    switch(c_->state_) {
        case StreamContext::State::idle:
            result = p->getTextContent();
            break;
        case StreamContext::State::justClosed:
            throw Exception("Missing object/attribute name");
        case StreamContext::State::objectName: {
            c_->state_ = StreamContext::State::idle;
            DomObject* o = p->findChildObject(c_->nextName_);
            if(o != nullptr) {
                result = o->getTextContent();
            } else {
                throw Exception("No object with name " + c_->nextName_);
            }
            break;
        }
        case StreamContext::State::objectDefault: {
            DomObject* o = p->findChildObject(c_->nextName_);
            if(o != nullptr) {
                result = o->getTextContent();
            } else {
                result = c_->nextDefault_;
            }
            c_->state_ = StreamContext::State::idle;
            break;
        }
        case StreamContext::State::attrName: {
            c_->state_ = StreamContext::State::idle;
            auto pos = p->attributes_.find(c_->nextName_);
            if(pos != p->attributes_.end()) {
                result = p->attributes_[c_->nextName_];
            } else {
                throw Exception("Missing attribute: " + c_->nextName_);
            }
            break;
        }
        case StreamContext::State::attrDefault: {
            auto pos = p->attributes_.find(c_->nextName_);
            if(pos != p->attributes_.end()) {
                result = p->attributes_[c_->nextName_];
            } else {
                result = c_->nextDefault_;
            }
            c_->state_ = StreamContext::State::idle;
            break;
        }
    }
    return result;
}

// Find the child object with the given name
xml::DomObject* xml::DomObject::findChildObject(const std::string& name) {
    DomObject* result = nullptr;
    for(auto& child : children_) {
        auto* t = dynamic_cast<DomObject*>(child);
        if(t != nullptr && t->name_ == name) {
            result = t;
        }
    }
    return result;
}

// Reset the state of the DOM object stream based
void xml::DomObject::reset() {
    c_.reset();
}

// Return a list of nested objects with the given name
std::list<xml::DomObject*> xml::DomObject::obj(const std::string& name) {
    DomObject* p;
    if(!c_ || c_->objQ_.empty()) {
        p = this;
    } else {
        p = c_->objQ_.front();
    }
    std::list<xml::DomObject*> result;
    for(auto& child : p->children_) {
        auto* t = dynamic_cast<DomObject*>(child);
        if(t != nullptr && t->name_ == name) {
            result.push_back(t);
        }
    }
    return result;
}

// Check, and if necessary, set, the serialisation mode
void xml::DomObject::checkMode(bool input) {
    switch(mode_) {
        case Mode::undefined:
            mode_ = input ? Mode::input : Mode::output;
            c_.reset();
            break;
        case Mode::input:
            if(!input) {
                throw Exception("Trying to output on an input DOM");
            }
            break;
        case Mode::output:
            if(input) {
                throw Exception("Trying to input on an output DOM");
            }
            break;
    }
}

// Reopen a just closed object
void xml::DomObject::doReopen() {
    switch(c_->state_) {
        case StreamContext::State::idle:
            throw Exception("An object has not just been closed");
        case StreamContext::State::justClosed:
            c_->objQ_.push_front(c_->prevOpen_);
            c_->state_ = StreamContext::State::idle;
            break;
        case StreamContext::State::objectName:
        case StreamContext::State::objectDefault:
            throw Exception("Unused object name: " + c_->nextName_);
        case StreamContext::State::attrName:
        case StreamContext::State::attrDefault:
            throw Exception("Unused attribute name: " + c_->nextName_);
    }
}

// Create the stream context object if necessary
void xml::DomObject::makeStreamContext() {
    if(!c_) {
        c_ = std::make_unique<StreamContext>();
    }
}

