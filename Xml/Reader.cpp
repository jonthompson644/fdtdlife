/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Reader.h"
#include "DomDocument.h"
#include "Exception.h"
#include <fstream>
#include <sstream>
#include <cstring>

// Converts text into a DOM.
void xml::Reader::readString(DomDocument* dom, std::string const& text) {
    std::stringstream stream(text);
    text_ = &stream;
	dom->read(*this);
}

// Read the file into the DOM
void xml::Reader::readFile(DomDocument* dom, std::string const& fileName) {
    std::fstream file;
    file.open(fileName, std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Parse the DOM
        text_ = &file;
        dom->read(*this);
        // Clean up
        file.close();
    } else {
        throw Exception("Could not open file " + fileName);
    }
}

// Gets the next char from the stream or buffer.  Can return EOF.
int xml::Reader::getChar() {
    int result;
    if(buffer_.empty()) {
        result = text_->get();
    } else {
        result = buffer_.front();
        buffer_.pop_front();
    }
    return result;
}

// Put a character into the buffer.  Ignores the EOF.
void xml::Reader::putChar(int ch) {
    if(ch != EOF) {
        buffer_.push_front(ch);
    }
}

// Gets the next token from the stream and checks it matches the one given.  Throws
// if not.
void xml::Reader::getExactToken(char const* token) {
    size_t tokenLen = strlen(token);
    int ch;
    for(size_t i = 0; i < tokenLen; i++) {
        ch = getChar();
        if(ch == EOF || ch != token[i]) {
            throw Exception(std::string("Syntax error, expected: ") + token);
        }
    }
}

// Gets the next token and checks that it is whitespace.  Throws if not.
void xml::Reader::getExactSpaceToken() {
    int ch = getChar();
    if(ch != EOF && ch <= ' ') {
        // Space found, skip any further consecutive spaces
        while(ch != EOF && ch <= ' ') {
            ch = getChar();
        }
        putChar(ch);
    } else {
        throw Exception("Syntax error, expected space");
    }
}

// Gets a space token from the input text if one exists.
void xml::Reader::getOptionalSpaceToken() {
    int ch = getChar();
    while(ch != EOF && ch <= ' ') {
        ch = getChar();
    }
    putChar(ch);
}

// Gets a string token from the text.  The quotes are not included in the result.
// If it is not present, an exception is thrown.
void xml::Reader::getStringToken(std::string& result) {
    result.clear();
    int ch = getChar();
    if(ch == '\"' || ch == '\'') {
        int quoteCh = ch;
        bool going = true;
        ch = getChar();
        while(going) {
            if(ch == EOF) {
                throw Exception("Syntax error, string missing closing quote");
            } else if(ch == quoteCh) {
                going = false;
            } else {
                result.push_back(static_cast<char>(ch));
                ch = getChar();
            }
        }
    } else {
        throw Exception("Syntax error, expected string");
    }
    entities_.extract(result);
}

// Gets an identifier token from the text.  If a valid identifier is not present,
// an exception is thrown.
void xml::Reader::getIdentifierToken(std::string& result)
{
    result.clear();
    int ch = getChar();
    if((ch >= 'a' && ch <= 'z') ||
              (ch >= 'A' && ch <= 'Z') ||
              (ch == ':') || (ch == '_')) {
        result.push_back(static_cast<char>(ch));
        bool going = true;
        while(going)
        {
            ch = getChar();
            if(ch == EOF) {
                throw Exception("Syntax error, expected identifier");
            } else if((ch >= 'a' && ch <= 'z') ||
                      (ch >= 'A' && ch <= 'Z') ||
                      (ch >= '0' && ch <= '9') ||
                      (ch == ':') || (ch == '_') ||
                      (ch == '-')) {
                result.push_back(static_cast<char>(ch));
            } else {
                putChar(ch);
                going = false;
            }
        }
    } else {
        throw Exception("Syntax error, expected identifier");
    }
}

// Returns true if the next token is the one given.  The token is not removed from
// the text.
bool xml::Reader::isExactToken(char const* token) {
    size_t tokenLen = strlen(token);
    std::list<int> saved;
    bool result = true;
    int ch;
    for(size_t i = 0; i < tokenLen; i++) {
        ch = getChar();
        saved.push_back(ch);
        if(ch == EOF || ch != token[i]) {
            result = false;
        }
    }
    while(!saved.empty()) {
        putChar(saved.back());
        saved.pop_back();
    }
    return result;
}

// Get markup
void xml::Reader::getMarkup(std::string& result) {
    // Everything is markup until the next '<'
    result.clear();
    int ch = getChar();
    while(ch != '<') {
        if(ch == EOF) {
            throw Exception("Unexpected end of XML ");
        }
        result.push_back(static_cast<char>(ch));
        ch = getChar();
    }
    putChar(ch);
    entities_.extract(result);
}
