/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Entities.h"
#include "Exception.h"

// Constructor
xml::Entities::Entities() {
	// The built in entities
	add("&amp;", "&");
	add("&lt;", "<");
	add("&gt;", ">");
	add("&apos;", "\'");
	add("&quot;", "\"");
	add("&nl;", "\n");
	add("&cr;", "\r");
}

// Destructor
xml::Entities::~Entities() {
}

// Converts entities in the string to their values
void xml::Entities::extract(std::string& text) const {
	// Are there any entities in the string?
	// We check first for speed purposes so we can avoid all
	// the copying required during entity expansion
	if(text.find('&') != std::string::npos) {
        // We have entities, expand them
        std::string result;
        const char* textPtr = text.c_str();
        // Scan the string for entities
        int pos = 0;
        while(pos < (int)text.length()) {
            // Is this the beginning of one
            if(*(textPtr + pos) == '&') {
                std::string entity;
                entity += *(textPtr + pos);
                // Scan for the end of the entity
                do {
                    pos++;
                    if(pos < (int)text.length()) {
                        entity += *(textPtr + pos);
                    } else {
                        // An unterminated entity
                        throw Exception("Unterminated entity: ", entity.c_str());
                    }
                } while(*(textPtr + pos) != ';');
                // Now replace it with the translation
                auto mapPos = entities_.find(entity);
                if(mapPos == entities_.end()) {
                    // An unknown entity
                    throw Exception("Unknown entity: ", entity.c_str());
                } else {
                    result += mapPos->second;
                }
            } else {
                result += *(textPtr + pos);
            }
            pos++;
        }
        text = result;
    }
}

// Converts entity values in the string with their encoded forms
// This is simple code that just inserts the standard, single
// character entities.
// TODO: Convert to using the entity table
std::string xml::Entities::insert(const std::string& text) const
{
	std::string result;
	for(int pos=0; pos<(int)text.length(); pos++) {
		// Is this a one character entity?
		char ch = text[pos];
		switch(ch) {
		    default:
		        result += ch;
		        break;
            case '&':
                // Insert the entity
                result += "&amp;";
                break;
            case '<':
                // Insert the entity
                result += "&lt;";
                break;
            case '>':
                // Insert the entity
                result += "&gt;";
                break;
            case '\'':
                // Insert the entity
                result += "&apos;";
                break;
            case '\"':
                // Insert the entity
                result += "&quot;";
                break;
            case '\n':
                // Insert the entity
                result += "&nl;";
                break;
            case '\r':
                // Insert the entity
                result += "&cr;";
                break;
		}
	}
	// Return the modified string
    return result;
}

// Adds an entity to the map
void xml::Entities::add(std::string const& entity, std::string const& value) {
	auto pos = entities_.find(entity);
	if(pos == entities_.end()) {
		entities_[entity] = value;
	} else {
        throw Exception("Duplicate entity definition: ", entity.c_str());
	}
}
