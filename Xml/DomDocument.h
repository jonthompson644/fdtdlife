/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_XML_DOMDOCUMENT_H
#define FDTDLIFE_XML_DOMDOCUMENT_H

#include "DomItem.h"
#include "Entities.h"
#include <string>

namespace xml {

    class DomObject;

    // A class that represents an XML DOM top level document
    class DomDocument : public DomItem {
    protected:
        std::string expectedObjectName_;
        std::map<std::string, std::string> attributes_;
        DomObject* object_{};
    public:
        DomDocument() = delete;
        explicit DomDocument(DomObject* object);
        explicit DomDocument(std::string expectedObjectName);
        ~DomDocument() override;

        void write(Writer& lexer, std::string const& indent) override;
        void read(Reader& lexer) override;
        DomObject* getObject() {return object_;}
    protected:
        void addAttribute(std::string const& name, std::string const& value);
    };
}

#endif //FDTDLIFE_XML_DOMDOCUMENT_H
