/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FUNCTORS_H
#define FDTDLIFE_FUNCTORS_H

#include <string>
#include <sstream>
#include <vector>

namespace xml {

    class DomObject;

    // A functor that defines the object to use for the next insertion or extraction
    class Obj {
    public:
        explicit Obj(const std::string& name) { name_ = name; }
    protected:
        std::string name_;

        friend class DomObject;
    };

    // A functor that defines the attribute to use for the next insertion or extraction
    class Attr {
    public:
        explicit Attr(const std::string& name) { name_ = name; }
    protected:
        std::string name_;

        friend class DomObject;
    };

    // A functor that opens an object for subsequent insertions or extractions until the next close
    class Open {
    public:
        explicit Open(const std::string& name = "") { name_ = name; }
    protected:
        std::string name_;

        friend class DomObject;
    };

    // A functor that re-opens an object that has just been closed
    class Reopen {
    public:
        Reopen() = default;
    protected:
        friend class DomObject;
    };

    // A functor that closes an object previously opened
    class Close {
    public:
        explicit Close(const std::string& name = "") { name_ = name; }
    protected:
        std::string name_;

        friend class DomObject;
    };

    // A functor that defines a default value for an Attr or Obj extraction
    class Default {
    public:
        template<class T>
        explicit Default(T v) {
            helper(v, std::is_enum<T>());
        }
        template<class T>
        void helper(T v, std::false_type) {
            std::stringstream s;
            s << v;
            value_ = s.str();
        }
        template<class T>
        void helper(T v, std::true_type) {
            std::stringstream s;
            s << static_cast<int>(v);
            value_ = s.str();
        }
        explicit Default(const std::string& v) {
            value_ = v;
        }
        template<class T>
        explicit Default(const std::vector<T>& v) {
            std::stringstream s;
            for(auto& i : v) { s << i << " "; }
            value_ = s.str();
        }
        template<class T>
        explicit Default(const std::list<T>& v) {
            std::stringstream s;
            for(auto& i : v) { s << i << " "; }
            value_ = s.str();
        }
    protected:
        std::string value_;

        friend class DomObject;
    };
}

#endif //FDTDLIFE_FUNCTORS_H
