#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DomDocument.h"
#include "DomObject.h"
#include "Exception.h"
#include "Reader.h"
#include "Writer.h"

// Initialising constructor.
xml::DomDocument::DomDocument(DomObject* object) :
        object_(object) {
    expectedObjectName_ = object->getName();
    addAttribute("version", "1.0");
    addAttribute("encoding", "UTF-8");
}

// Destructor.
xml::DomDocument::~DomDocument() {
    delete object_;
}

// Writes the XML text to the given string.
void xml::DomDocument::write(Writer& lexer, std::string const& indent) {
    // Write the tag open
    lexer.write(indent + "<?xml");
    // Write the attributes
    for(auto & attribute : attributes_) {
        std::string attr = attribute.second;
        lexer.entities().insert(attr);
        lexer.write(" ");
        lexer.write(attribute.first);
        lexer.write("=\"");
        lexer.write(attr);
        lexer.write("\"");
    }
    // End the open tag
    lexer.write("?>\n");
    // Write the object
    object_->write(lexer, indent);
}

// Initialising constructor.
xml::DomDocument::DomDocument(std::string expectedObjectName) :
        expectedObjectName_(std::move(expectedObjectName)),
        object_(nullptr) {
}

// Translates the text into DOM objects.
void xml::DomDocument::read(Reader& lexer) {
    // Process the xml header line
    std::string token;
    lexer.getExactToken("<?xml");
    lexer.getExactSpaceToken();

    // Read the attributes
    std::string attribute;
    std::string value;
    bool going = true;
    while(going) {
        lexer.getOptionalSpaceToken();
        if(!lexer.isExactToken("?>")) {
            lexer.getIdentifierToken(attribute);
            lexer.getOptionalSpaceToken();
            lexer.getExactToken("=");
            lexer.getOptionalSpaceToken();
            lexer.getStringToken(value);
            addAttribute(attribute, value);
        } else {
            going = false;
        }
    }
    lexer.getOptionalSpaceToken();
    lexer.getExactToken("?>");
    lexer.getOptionalSpaceToken();
    // Process the document object
    lexer.getExactToken("<");
    lexer.getIdentifierToken(token);
    if(token != expectedObjectName_) {
        throw Exception(std::string("Incorrect document type, found: ") + token);
    }
    object_ = new DomObject(token);
    object_->read(lexer);
}

// Adds an attribute to the object.  For internal use only.
void xml::DomDocument::addAttribute(std::string const& name, std::string const& value) {
    if(attributes_.count(name) == 0) {
        attributes_[name] = value;
    }
}
