/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_XML_READER_H
#define FDTDLIFE_XML_READER_H

#include <string>
#include "Entities.h"
#include <list>

namespace xml {

    class DomDocument;

    // A class that converts text into an XML DOM document
    class Reader {
    public:
        // Construction
        Reader() = default;

        // User API
        void readString(DomDocument* dom, const std::string& text);
        void readFile(DomDocument* dom, const std::string& fileName);

        // Internal API
        void getExactToken(char const* token);
        void getExactSpaceToken();
        void getOptionalSpaceToken();
        void getStringToken(std::string& result);
        void getIdentifierToken(std::string& result);
        void getMarkup(std::string& result);
        bool isExactToken(char const* token);

    protected:
        // Data
        Entities entities_;
        std::iostream* text_{nullptr};
        std::list<int> buffer_;

        // Helper functions
        int getChar();
        void putChar(int ch);
    };

}

#endif //FDTDLIFE_XML_READER_H

