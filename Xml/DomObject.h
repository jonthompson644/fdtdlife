/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_XML_DOMOBJECT_H
#define FDTDLIFE_XML_DOMOBJECT_H

#include "DomItem.h"
#include <map>
#include <list>
#include <vector>
#include <complex>
#include "Entities.h"
#include "Manipulators.h"
#include "StreamContext.h"

namespace xml {

    // An object in the DOM
    class DomObject : public DomItem {
    public:
        // Construction
        DomObject() = delete;
        explicit DomObject(std::string name);
        ~DomObject() override;

        // API
        void write(Writer& lexer, std::string const& indent) override;
        void read(Reader& lexer) override;

        // Getters
        std::string const& getName() { return name_; }
        std::string getTextContent();

        // Setters
        void fixName(const std::string& v) { name_ = v; }

        // The stream-like API
        template<typename T>
        DomObject& operator<<(T v) {
            return insertHelper(v, std::is_enum<T>());
        }
        template<typename T>
        DomObject& insertHelper(T v, std::false_type) {
            std::stringstream s;
            s << v;
            insertValue(s.str());
            return *this;
        }
        template<typename T>
        DomObject& insertHelper(T v, std::true_type) {
            std::stringstream s;
            s << static_cast<int>(v);
            insertValue(s.str());
            return *this;
        }
        DomObject& operator<<(const std::string& v) {
            insertValue(v);
            return *this;
        }
        template<class T>
        DomObject& operator<<(const std::vector<T>& value) {
            std::stringstream str;
            for(auto& v : value) {
                str << v << " ";
            }
            insertValue(str.str());
            return *this;
        }
        DomObject& operator<<(const std::vector<uint8_t>& value) {
            std::stringstream str;
            for(auto& v : value) {
                str << static_cast<uint64_t>(v) << " ";
            }
            insertValue(str.str());
            return *this;
        }
        template<class T>
        DomObject& operator<<(const std::list<T>& value) {
            std::stringstream str;
            for(auto& v : value) {
                str << v << " ";
            }
            insertValue(str.str());
            return *this;
        }
        DomObject& operator<<(const Obj& manip);
        DomObject& operator<<(const Attr& manip);
        DomObject& operator<<(const Open& manip);
        DomObject& operator<<(const Reopen& manip);
        DomObject& operator<<(const Close& manip);
        template<typename T>
        DomObject& operator>>(T& v) {
            return extractHelper(v, std::is_enum<T>());
        }
        template<typename T>
        DomObject& extractHelper(T& v, std::false_type) {
            std::stringstream s(extractValue());
            s >> v;
            return *this;
        }
        template<typename T>
        DomObject& extractHelper(T& v, std::true_type) {
            std::stringstream s(extractValue());
            int t;
            s >> t;
            v = static_cast<T>(t);
            return *this;
        }
        DomObject& operator>>(std::string& v) {
            v = extractValue();
            return *this;
        }
        template<class T>
        DomObject& operator>>(std::vector<T>& value) {
            value.clear();
            std::stringstream s(extractValue());
            while(!s.eof()) {
                T v;
                s >> v;
                if(!s.eof()) {
                    value.push_back(v);
                }
            }
            return *this;
        }
        DomObject& operator>>(std::vector<uint8_t>& value) {
            value.clear();
            std::stringstream s(extractValue());
            while(!s.eof()) {
                uint64_t v;
                s >> v;
                if(!s.eof()) {
                    value.push_back(static_cast<uint8_t>(v));
                }
            }
            return *this;
        }
        template<class T>
        DomObject& operator>>(std::list<T>& value) {
            value.clear();
            std::stringstream s(extractValue());
            while(!s.eof()) {
                T v;
                s >> v;
                if(!s.eof()) {
                    value.push_back(v);
                }
            }
            return *this;
        }
        DomObject& operator>>(const Obj& manip);
        DomObject& operator>>(const Attr& manip);
        DomObject& operator>>(const Open& manip);
        DomObject& operator>>(const Reopen& manip);
        DomObject& operator>>(const Close& manip);
        DomObject& operator>>(const Default& manip);
        void reset();
        std::list<DomObject*> obj(const std::string& name);

        // Test API
        void setState(StreamContext::State s) { c_->state_ = s; }

    protected:
        // DOM information
        std::string name_;
        std::map<std::string, std::string> attributes_;
        std::list<DomItem*> children_;

        // Stream base API state information
        std::unique_ptr<StreamContext> c_;
        enum class Mode {
            undefined, input, output
        };
        Mode mode_{Mode::undefined};

        // Helper functions
        void insertValue(const std::string& value);
        std::string extractValue();
        void doObj(const Obj& manip);
        void doAttr(const Attr& manip);
        void doClose(const Close& manip);
        void doReopen();
        DomObject* findChildObject(const std::string& name);
        void checkMode(bool input);
        void addAttribute(std::string const& name, std::string const& value);
        void addNestedItem(DomItem* item);
        void makeStreamContext();
    };
}

#endif  //FDTDLIFE_XML_DOMOBJECT_H
