/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PHASESHIFTSEARCHDLG_H
#define FDTDLIFE_PHASESHIFTSEARCHDLG_H

#include <QtIncludes.h>
#include <list>

class CatViewer;
namespace fdtd { class CatalogueItem; }
class DataSeriesWidget;

class PhaseShiftSearchDlg : public QDialog {
    Q_OBJECT
public:
    // Construction
    explicit PhaseShiftSearchDlg(CatViewer* app);

    // Types
    class Result {
    public:
        Result(std::shared_ptr<fdtd::CatalogueItem> item, double transmittance, double phaseShift) :
                item_(std::move(item)),
                transmittance_(transmittance),
                phaseShift_(phaseShift) {}
        std::shared_ptr<fdtd::CatalogueItem> item_;
        double transmittance_;
        double phaseShift_;
    };

protected slots:
    // Handlers
    void onSearch();
    void onResultSelected();

protected:
    // Helpers
    void fillWidgets();
    void readWidgets();

    // Variables
    CatViewer* app_;
    double desiredUnitCell_;
    double desiredFrequency_;
    double transmittanceThreshold_ {};
    std::list<Result> results_;

    // Constants
    enum {
        resultCodingCol = 0, resultTransmittanceCol, resultPhaseShiftCol, resultNumCols
    };

    // Widgets
    QLineEdit* desiredUnitCellEdit_;
    QLineEdit* desiredFrequencyEdit_;
    QLineEdit* transmittanceThresholdEdit_;
    QTreeWidget* resultsTree_;
    QPushButton* searchButton_;
};


#endif //FDTDLIFE_PHASESHIFTSEARCHDLG_H
