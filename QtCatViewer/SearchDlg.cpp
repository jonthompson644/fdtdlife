/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SearchDlg.h"
#include "CatViewer.h"
#include "PatternSearchDlg.h"
#include "AdmittanceFnSearchDlg.h"
#include "PhaseShiftSearchDlg.h"
#include "ClassificationDlg.h"

// Constructor
SearchDlg::SearchDlg(CatViewer* app) :
        app_(app) {
    setWindowTitle("FDTD Catalogue Viewer Search");
    // The main layout
    auto* mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    // The tab area for the various search methods
    modeTab_ = new QTabWidget;
    mainLayout->addWidget(modeTab_);
    // The search methods
    modeTab_->addTab(new PatternSearchDlg(app_), "Pattern");
    modeTab_->addTab(new AdmittanceFnSearchDlg(app_), "Fitness");
    modeTab_->addTab(new PhaseShiftSearchDlg(app_), "PhaseShift");
    modeTab_->addTab(new ClassificationDlg(app_), "Classification");
}

// The dialog is about to be closed
void SearchDlg::closeEvent(QCloseEvent* event) {
    app_->searchClosing();
    QDialog::closeEvent(event);
}
