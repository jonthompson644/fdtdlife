/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatDlg.h"
#include "ItemDlg.h"
#include "CatViewer.h"
#include "SearchDlg.h"
#include <DialogHelper.h>
#include <algorithm>
#include <Fdtd/CatalogueItem.h>

// Constructor
CatDlg::CatDlg(CatViewer* app) :
        app_(app),
        firstItemOnPage_(0),
        itemsPerPage_(4),
        searchDlg_(nullptr) {
    // The main layout
    auto* mainLayout = new QVBoxLayout;
    mainLayout->setSpacing(1);
    setLayout(mainLayout);
    // The information line
    auto* informationLayout = new QHBoxLayout;
    informationLayout->setSpacing(8);
    mainLayout->addLayout(informationLayout);
    informationLayout->addStretch();
    openButton_ = DialogHelper::buttonItem(informationLayout, "Open...");
    pixelsPerSideEdit_ = DialogHelper::textItem(informationLayout,
                                                "Plate Size", true);
    symmetryEdit_ = DialogHelper::textItem(informationLayout,
                                           "Symmetry", true);
    unitCellEdit_ = DialogHelper::doubleItem(informationLayout,
                                             "Unit Cell Recorded At (um)",
                                             true);
    showSearchCheck_ = DialogHelper::boolItem(informationLayout, "Show Search");
    informationLayout->addStretch();
    // The statistics line
    auto* statisticsLayout = new QHBoxLayout;
    statisticsLayout->setSpacing(8);
    mainLayout->addLayout(statisticsLayout);
    statisticsLayout->addStretch();
    countButton_ = DialogHelper::buttonItem(statisticsLayout, "Count Items");
    totalItemsEdit_ = DialogHelper::intItem(statisticsLayout,
                                            "Num Items", true);
    presentNoCornersEdit_ = DialogHelper::intItem(statisticsLayout,
                                                  "No Corners: Present", true);
    missingNoCornersEdit_ = DialogHelper::intItem(statisticsLayout,
                                                  "Missing", true);
    presentCornersEdit_ = DialogHelper::intItem(statisticsLayout,
                                                "Corners: Present", true);
    missingCornersEdit_ = DialogHelper::intItem(statisticsLayout,
                                                "Missing", true);
    statisticsLayout->addStretch();
    // The navigation line
    auto* navigationLayout = new QHBoxLayout;
    navigationLayout->setSpacing(8);
    mainLayout->addLayout(navigationLayout);
    navigationLayout->addStretch();
    firstButton_ = DialogHelper::buttonItem(navigationLayout, "|<");
    pageBackButton_ = DialogHelper::buttonItem(navigationLayout, "<<");
    backButton_ = DialogHelper::buttonItem(navigationLayout, "<");
    fromItemEdit_ = DialogHelper::intItem(navigationLayout, "", 0);
    forwardButton_ = DialogHelper::buttonItem(navigationLayout, ">");
    pageForwardButton_ = DialogHelper::buttonItem(navigationLayout, ">>");
    lastButton_ = DialogHelper::buttonItem(navigationLayout, ">|");
    itemsPerPageSpin_ = DialogHelper::intSpinItem(navigationLayout,
                                                  "Items per Page", 1, 20,
                                                  (int) itemsPerPage_);
    showClassification_ = DialogHelper::boolItem(navigationLayout, "Show Classification");
    navigationLayout->addStretch();
    // The data display
    dataLayout_ = new QVBoxLayout;
    mainLayout->addLayout(dataLayout_);
    mainLayout->addStretch();
    updateInformation();
    updateNavigation();
    updateDataDisplay();
    // Handlers
    connect(openButton_, SIGNAL(clicked()), SLOT(onOpen()));
    connect(forwardButton_, SIGNAL(clicked()), SLOT(onForward()));
    connect(backButton_, SIGNAL(clicked()), SLOT(onBack()));
    connect(pageForwardButton_, SIGNAL(clicked()), SLOT(onPageForward()));
    connect(pageBackButton_, SIGNAL(clicked()), SLOT(onPageBack()));
    connect(firstButton_, SIGNAL(clicked()), SLOT(onFirst()));
    connect(lastButton_, SIGNAL(clicked()), SLOT(onLast()));
    connect(fromItemEdit_, SIGNAL(editingFinished()), SLOT(onFromItem()));
    connect(itemsPerPageSpin_, SIGNAL(valueChanged(int)), SLOT(onItemsPerPage(int)));
    connect(showSearchCheck_, SIGNAL(stateChanged(int)), SLOT(onShowSearch(int)));
    connect(showClassification_, SIGNAL(stateChanged(int)), SLOT(onShowClassification(int)));
    connect(countButton_, SIGNAL(clicked()), SLOT(onCount()));
}

// The from item has been changed
void CatDlg::onFromItem() {
    firstItemOnPage_ = std::min(fromItemEdit_->text().toULongLong(),
                                app_->numItems() - itemsPerPage_);
    updateDataDisplay();
}

// Step one item forward
void CatDlg::onForward() {
    firstItemOnPage_ = std::min(firstItemOnPage_ + 1, app_->numItems() - itemsPerPage_);
    updateNavigation();
    updateDataDisplay();
}

// Step one item back
void CatDlg::onBack() {
    if(firstItemOnPage_ > 0) {
        firstItemOnPage_--;
    }
    updateNavigation();
    updateDataDisplay();
}

// Step one page worth of items forward
void CatDlg::onPageForward() {
    firstItemOnPage_ = std::min(firstItemOnPage_ + itemsPerPage_,
                                app_->numItems() - itemsPerPage_);
    updateNavigation();
    updateDataDisplay();
}

// Step one page worth of items back
void CatDlg::onPageBack() {
    if(firstItemOnPage_ > itemsPerPage_) {
        firstItemOnPage_ -= itemsPerPage_;
    } else {
        firstItemOnPage_ = 0;
    }
    updateNavigation();
    updateDataDisplay();
}

// Go to the first item
void CatDlg::onFirst() {
    firstItemOnPage_ = 0;
    updateNavigation();
    updateDataDisplay();
}

// Go to the last item
void CatDlg::onLast() {
    firstItemOnPage_ = app_->numItems() - itemsPerPage_;
    updateNavigation();
    updateDataDisplay();
}

// The number of items per page has changed
void CatDlg::onItemsPerPage(int value) {
    itemsPerPage_ = (uint64_t) value;
    firstItemOnPage_ = std::min(firstItemOnPage_, app_->numItems() - itemsPerPage_);
    updateNavigation();
    updateDataDisplay();
}

// Open a catalogue file
void CatDlg::onOpen() {
    app_->openCatalogue();
    firstItemOnPage_ = 0;
    updateInformation();
    updateNavigation();
    updateDataDisplay();
}

// Update the information line
void CatDlg::updateInformation() {
    pixelsPerSideEdit_->setText(QString("%1x%1").arg(app_->bitsPerHalfSide() * 2));
    unitCellEdit_->setText(QString::number(app_->unitCell() * 1000000.0));
    symmetryEdit_->setText(app_->fourFoldSymmetry() ? "Four Fold" : "Two Fold");
}

// Update the navigation line
void CatDlg::updateNavigation() {
    fromItemEdit_->setText(QString::number(firstItemOnPage_));
    totalItemsEdit_->setText(QString::number(app_->numItems()));
}

// Update data display
void CatDlg::updateDataDisplay() {
    // First adjust the number of display lines
    if(itemLines_.size() != itemsPerPage_) {
        itemLines_.clear();
        itemLines_.resize(itemsPerPage_);
        for(size_t i = 0; i < itemsPerPage_; i++) {
            itemLines_[i] = std::make_unique<ItemDlg>(app_, i == 0);
            dataLayout_->addWidget(itemLines_[i].get());
        }
    }
    // Now set their item codes
    auto coding = firstItemOnPage_;
    for(auto& itemLine : itemLines_) {
        itemLine->setItem(coding);
        coding++;
    }
}

// Show or hide the search dialog
void CatDlg::onShowSearch(int /*state*/) {
    if(showSearchCheck_->isChecked()) {
        // Show the search dialog
        searchDlg_ = new SearchDlg(app_);
        searchDlg_->show();
    } else {
        // Hide the search dialog
        delete searchDlg_;
        searchDlg_ = nullptr;
    }
}

// The search dialog is being closed
void CatDlg::searchClosing() {
    showSearchCheck_->setChecked(false);
}

// Show the specified item
void CatDlg::showItem(uint64_t coding) {
    firstItemOnPage_ = std::min(coding, app_->numItems() - itemsPerPage_);
    updateNavigation();
    updateDataDisplay();
}

// Update the count edit boxes
void CatDlg::onCount() {
    size_t presentCorners, missingCorners, presentNoCorners, missingNoCorners;
    app_->count(presentNoCorners, missingNoCorners, presentCorners,
                missingCorners);
    totalItemsEdit_->setText(QString::number(app_->numItems()));
    presentNoCornersEdit_->setText(QString::number(presentNoCorners));
    missingNoCornersEdit_->setText(QString::number(missingNoCorners));
    presentCornersEdit_->setText(QString::number(presentCorners));
    missingCornersEdit_->setText(QString::number(missingCorners));
}

// The show classification checkbox has changed state
// Show or hide the search dialog
void CatDlg::onShowClassification(int /*state*/) {
    app_->showClassification(showClassification_->isChecked());
    updateDataDisplay();
}


