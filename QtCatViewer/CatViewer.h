/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATVIEWER_H
#define FDTDLIFE_CATVIEWER_H

#include <QtIncludes.h>
#include <string>
#include <memory>
#include <deque>
#include "PolynomialClassification.h"

class CatDlg;
namespace fdtd { class CatalogueItem; }
namespace fdtd { class Configuration; }
class GuiConfiguration;

class CatViewer : public QMainWindow {
Q_OBJECT
public:
    // Construction
    CatViewer();
    ~CatViewer() override;

    // API
    void openCatalogue();
    void openCatalogue(const std::string& fileName);
    std::shared_ptr<fdtd::CatalogueItem> getItem(uint64_t coding);
    void searchClosing();
    void showItem(uint64_t coding);
    void count(size_t& presentNoCorners, size_t& missingNoCorners,
               size_t& presentCorners, size_t& missingCorners);
    void classifyTransmittance();
    void printClassifyCode();

    // Getters
    [[nodiscard]] uint64_t numItems() const { return numItems_; }
    [[nodiscard]] int bitsPerHalfSide() const { return bitsPerHalfSide_; }
    [[nodiscard]] double unitCell() const { return unitCell_; }
    [[nodiscard]] bool fourFoldSymmetry() const { return fourFoldSymmetry_; }
    [[nodiscard]] GuiConfiguration* pCfg() const { return pCfg_; }
    [[nodiscard]] double startFrequency() const { return startFrequency_; }
    [[nodiscard]] double frequencyStep() const { return frequencyStep_; }
    [[nodiscard]] const std::deque<std::shared_ptr<fdtd::CatalogueItem>>& items() const {
        return items_;
    }
    [[nodiscard]] const PolynomialClassification& classify() const { return classify_; }
    [[nodiscard]] bool showClassification() const { return showClassification_; }

    // Setters
    void showClassification(bool v) { showClassification_ = v; }
protected:
    // Helper functions
    void clear();
    void setTitle();
    void setNumItems();

    // Variables
    std::string currentFile_;  // The current file being displayed
    CatDlg* dlg_;  // The main dialog
    uint64_t numItems_;  // The number of items in the catalogue
    std::deque<std::shared_ptr<fdtd::CatalogueItem>> items_; // The catalogue
    //PolynomialClassification classify_{{-5,-1,3U},
    //                                   {-16,-11,3U},
    //                                   {-27,-22,3U},
    //                                   {-38,-33,3U},
    //                                   {-50, -44, 3U},
    //                                   {-61, -56, 3U}};
    PolynomialClassification classify_;
    bool showClassification_{false};  // Show the classification information
    int bitsPerHalfSide_;  // Size of the binary plate
    bool fourFoldSymmetry_;  // Are the diagonal symmetries used
    double unitCell_;  // The G the data was recorded at
    double startFrequency_;  // The start frequency of the data
    double frequencyStep_;  // The frequency step
    fdtd::Configuration* p_;  // Modelling parameters
    GuiConfiguration* pCfg_;  // Configuration parameters
};


#endif //FDTDLIFE_CATVIEWER_H
