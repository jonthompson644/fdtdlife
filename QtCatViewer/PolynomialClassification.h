/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_POLYNOMIALCLASSIFICATION_H
#define FDTDLIFE_POLYNOMIALCLASSIFICATION_H

#include <vector>
#include <array>
#include "PolynomialBin.h"
#include <Box/Constants.h>

// A class that sorts data blocks into groups according to
// a nearest fit polynomial
class PolynomialClassification {
public:
    // Types
    struct RangeSpec {
        double minExp_;
        double maxExp_;
        size_t steps_;
        [[nodiscard]] double lower(size_t s) const {
            double expInc = (maxExp_ - minExp_) / static_cast<double>(steps_ - 1);
            return std::pow(10.0, expInc * (s - 0.5) + minExp_);
        }
        [[nodiscard]] double upper(size_t s) const {
            double expInc = (maxExp_ - minExp_) / static_cast<double>(steps_ - 1);
            return std::pow(10.0, expInc * (s + 0.5) + minExp_);
        }
    };

    // Construction
    PolynomialClassification(const RangeSpec& a, const RangeSpec& b,
                             const RangeSpec& c, const RangeSpec& d,
                             const RangeSpec& e, const RangeSpec& f);
    PolynomialClassification() = default;
    // API
    void add(const PolynomialBin& bin);
    void add(const std::vector<PolynomialBin>& bins);
    size_t classify(const box::Polynomial& poly);
    [[nodiscard]] const std::vector<PolynomialBin>& bins() const { return bins_; }

protected:
    std::vector<PolynomialBin> bins_;
};


#endif //FDTDLIFE_POLYNOMIALCLASSIFICATION_H
