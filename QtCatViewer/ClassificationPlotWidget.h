/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CLASSIFICATIONPLOTWIDGET_H
#define FDTDLIFE_CLASSIFICATIONPLOTWIDGET_H

#include "QtIncludes.h"

class CatViewer;

class ClassificationPlotWidget : public QWidget {
Q_OBJECT
public:
    // Construction
    explicit ClassificationPlotWidget(CatViewer* app);

    // Overrides of QWidget
    QSize sizeHint() const override;
    void paintEvent(QPaintEvent* event) override;

    // API
    void addItem(uint64_t coding);
    void clear();

protected:
    // Members
    CatViewer* app_;
    std::vector<uint64_t> items_;
    double minY_{};
    double maxY_{};
    QString unitsY_{""};
    double scaleY_{};
    double tickStepY_{};
    double minX_{};
    double stepX_{};
    double scaleX_{};
    QString unitsX_{"GHz"};
    double tickStepX_{};

    // Helpers
    void drawItem(QPainter* painter, uint64_t coding);
    void calcScaling();
    void drawVerticalAxis(QPainter* painter);
    void drawHorizontalAxis(QPainter* painter);
    void drawData(QPainter* painter);
};


#endif //FDTDLIFE_CLASSIFICATIONPLOTWIDGET_H
