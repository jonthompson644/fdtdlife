/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnSearchDlg.h"
#include "CatViewer.h"
#include <DialogHelper.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Xml/DomObject.h>
#include <Fdtd/CatalogueItem.h>
#include <Fdtd/AdmittanceData.h>
#include <DataSeriesWidget.h>
#include <Box/Constants.h>

// Constructor
AdmittanceFnSearchDlg::AdmittanceFnSearchDlg(CatViewer* app) :
        app_(app) {
    // Main layout
    auto mainLayout = new QHBoxLayout;
    setLayout(mainLayout);
    // The search spec widgets
    auto searchLayout = new QVBoxLayout;
    mainLayout->addLayout(searchLayout);
    std::tie(raEdit_, rbEdit_, rcEdit_, rdEdit_, reEdit_, rfEdit_) =
            DialogHelper::polynomialItem(searchLayout, "Real:");
    std::tie(iaEdit_, ibEdit_, icEdit_, idEdit_, ieEdit_, ifEdit_) =
            DialogHelper::polynomialItem(searchLayout, "Imag:");
    auto* repeatCellLayout = new QHBoxLayout;
    searchLayout->addLayout(repeatCellLayout);
    repeatCellLayout->addWidget(new QLabel("Repeat Cell Scale:"));
    auto* repeatCellLayoutCol = new QVBoxLayout;
    repeatCellLayout->addLayout(repeatCellLayoutCol);
    minRepeatCellScaleEdit_ = DialogHelper::doubleItem(repeatCellLayoutCol, "Min");
    numRepeatCellScaleSamplesEdit_ = DialogHelper::doubleItem(repeatCellLayoutCol, "N samples");
    auto* buttonLayout = new QHBoxLayout;
    searchLayout->addLayout(buttonLayout);
    searchButton_ = DialogHelper::buttonItem(buttonLayout, "Search");
    pasteButton_ = DialogHelper::buttonItem(buttonLayout, "Paste");
    // The results
    resultsTree_ = new QTreeWidget;
    mainLayout->addWidget(resultsTree_);
    resultsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    resultsTree_->setColumnCount(resultsNumCols);
    resultsTree_->setHeaderLabels({"Coding", "G Scaling", "Mismatch"});
    resultsTree_->setFixedWidth(310);
    // The individual result display
    auto* resultLayout = new QVBoxLayout;
    mainLayout->addLayout(resultLayout);
    resultLayout->addWidget(new QLabel("Real"));
    realView_ = new DataSeriesWidget(app_->pCfg());
    realView_->setAxisInfo(0.0, 100.0, "GHz", 0.0, 1.0, "");
    resultLayout->addWidget(realView_);
    resultLayout->addWidget(new QLabel("Imaginary"));
    imaginaryView_ = new DataSeriesWidget(app_->pCfg());
    imaginaryView_->setAxisInfo(0.0, 100.0, "GHz", 0.0, 1.0, "");
    resultLayout->addWidget(imaginaryView_);
    // Connect handlers
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    connect(searchButton_, SIGNAL(clicked()), SLOT(onSearch()));
    connect(resultsTree_, SIGNAL(itemSelectionChanged()), SLOT(onResultSelected()));
    // Initial state
    fillWidgets();
}

// The paste button has been pressed
void AdmittanceFnSearchDlg::onPaste() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/fitnessadmittance")) {
        // Read the XML into the function
        std::string text = mimeData->data("fdtdlife/fitnessadmittance").toStdString();
        xml::DomDocument dom("fitnessadmittance");
        xml::Reader reader;
        try {
            reader.readString(&dom, text);
            auto* root = dom.getObject();
            *root >> xml::Obj("real") >> real_;
            *root >> xml::Obj("imag") >> imag_;
            *root >> xml::Obj("minrepeatcellscale") >> xml::Default(minRepeatCellScale_) >> minRepeatCellScale_;
            *root >> xml::Obj("numrepeatcellscalesamples") >> xml::Default(numRepeatCellScaleSamples_) >> numRepeatCellScaleSamples_;
        } catch(xml::Exception& e) {
            std::cout << "Admittance function paste failed to read XML: " << e.what() << std::endl;
        }
    }
    fillWidgets();
}

// Fill the widgets with the current search parameters
void AdmittanceFnSearchDlg::fillWidgets() {
    raEdit_->setText(QString::number(real_.a()));
    rbEdit_->setText(QString::number(real_.b()));
    rcEdit_->setText(QString::number(real_.c()));
    rdEdit_->setText(QString::number(real_.d()));
    reEdit_->setText(QString::number(real_.e()));
    rfEdit_->setText(QString::number(real_.f()));
    iaEdit_->setText(QString::number(imag_.a()));
    ibEdit_->setText(QString::number(imag_.b()));
    icEdit_->setText(QString::number(imag_.c()));
    idEdit_->setText(QString::number(imag_.d()));
    ieEdit_->setText(QString::number(imag_.e()));
    ifEdit_->setText(QString::number(imag_.f()));
    minRepeatCellScaleEdit_->setText(QString::number(minRepeatCellScale_));
    numRepeatCellScaleSamplesEdit_->setText(QString::number(numRepeatCellScaleSamples_));
}

// Read the current search parameters from the widgets
void AdmittanceFnSearchDlg::readWidgets() {
    real_.a(raEdit_->text().toDouble());
    real_.b(rbEdit_->text().toDouble());
    real_.c(rcEdit_->text().toDouble());
    real_.d(rdEdit_->text().toDouble());
    real_.e(reEdit_->text().toDouble());
    real_.f(rfEdit_->text().toDouble());
    imag_.a(iaEdit_->text().toDouble());
    imag_.b(ibEdit_->text().toDouble());
    imag_.c(icEdit_->text().toDouble());
    imag_.d(idEdit_->text().toDouble());
    imag_.e(ieEdit_->text().toDouble());
    imag_.f(ifEdit_->text().toDouble());
    minRepeatCellScale_ = minRepeatCellScaleEdit_->text().toDouble();
    numRepeatCellScaleSamples_ = numRepeatCellScaleSamplesEdit_->text().toInt();
}

// The search button has been pressed
void AdmittanceFnSearchDlg::onSearch() {
    results_.clear();
    readWidgets();
    // Calculate the results
    double gScaleIncrement = 0.0;
    if(numRepeatCellScaleSamples_ > 1) {
        gScaleIncrement = (maxRepeatCellScale_ - minRepeatCellScale_) /
                          (double) (numRepeatCellScaleSamples_ - 1);
    }
    for(auto& item : app_->items()) {
        if(item) {
            for(int i = 0; i < numRepeatCellScaleSamples_; i++) {
                double scaling = (double) i * gScaleIncrement + minRepeatCellScale_;
                double mismatch = calculateMismatch(item, scaling);
                results_.emplace_back(Result(item, scaling, mismatch));
                results_.sort([](const Result& a, const Result& b) { return a.mismatch_ < b.mismatch_; });
                if(results_.size() > maxNumResults_) {
                    results_.pop_back();
                }
            }
        }
    }
    // List the result tree
    resultsTree_->clear();
    for(auto& result : results_) {
        auto item = new QTreeWidgetItem(resultsTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setText(resultsColCoding, QString::number(result.item_->code()));
        item->setText(resultsColScaling, QString::number(result.scaling_));
        item->setText(resultsColMismatch, QString::number(result.mismatch_));
    }
    resultsTree_->setCurrentItem(resultsTree_->topLevelItem(0));
}

// A result has been clicked
void AdmittanceFnSearchDlg::onResultSelected() {
    // Clear the result data views
    realView_->clear();
    imaginaryView_->clear();
    // Do we have a selection?
    QTreeWidgetItem* sel = resultsTree_->currentItem();
    if(sel != nullptr) {
        // Find the result corresponding to the selection
        int index = resultsTree_->indexOfTopLevelItem(sel);
        auto pos = results_.begin();
        for(int i = 0; i < index && pos != results_.end(); i++) {
            pos++;
        }
        if(pos != results_.end()) {
            // Get the item and target admittances
            fdtd::AdmittanceData admittance = pos->item_->admittance(
                    app_->unitCell(), app_->startFrequency(), app_->frequencyStep());
            double frequency = app_->startFrequency();
            std::vector<double> dataReal;
            std::vector<double> dataImag;
            std::vector<double> targetReal;
            std::vector<double> targetImag;
            double min = 0.0;
            double max = 0.0;
            for(size_t i = 0; i < pos->item_->numPoints(); i++) {
                std::complex<double> a = admittance.admittanceAt(frequency, app_->unitCell() * pos->scaling_);
                std::complex<double> b = {real_.value(frequency), imag_.value(frequency)};
                dataReal.push_back(a.real());
                dataImag.push_back(a.imag());
                targetReal.push_back(b.real());
                targetImag.push_back(b.imag());
                min = std::min(min, a.real());
                min = std::min(min, a.imag());
                max = std::max(max, a.real());
                max = std::max(max, a.imag());
                min = std::min(min, b.real());
                min = std::min(min, b.imag());
                max = std::max(max, b.real());
                max = std::max(max, b.imag());
                frequency += app_->frequencyStep();
            }
            // Fill the real data view
            realView_->setData(dataReal, GuiConfiguration::elementDataA, true);
            realView_->setData(targetReal, GuiConfiguration::elementDataB);
            realView_->setAxisInfo(app_->startFrequency() / box::Constants::giga_,
                                   app_->frequencyStep() / box::Constants::giga_,
                                   "GHz", min, max, "");
            // Fill the imaginary view
            imaginaryView_->setData(dataImag, GuiConfiguration::elementDataA, true);
            imaginaryView_->setData(targetImag, GuiConfiguration::elementDataB);
            imaginaryView_->setAxisInfo(app_->startFrequency() / box::Constants::giga_,
                                        app_->frequencyStep() / box::Constants::giga_,
                                        "GHz", min, max, "");
            // Position the main window to this item
            app_->showItem(pos->item_->code());
        }
    }
}

// Calculate the mismatch for an item
double AdmittanceFnSearchDlg::calculateMismatch(
        const std::shared_ptr<fdtd::CatalogueItem>& item, double gScale) {
    fdtd::AdmittanceData admittance = item->admittance(
            app_->unitCell(), app_->startFrequency(), app_->frequencyStep());
    double result = 0;
    double frequency = app_->startFrequency();
    for(size_t i = 0; i < item->numPoints(); i++) {
        std::complex<double> a = admittance.admittanceAt(frequency, app_->unitCell() * gScale);
        std::complex<double> b = {real_.value(frequency), imag_.value(frequency)};
        result += fabs(a.real() - b.real()) + fabs(a.imag() - b.imag());
        frequency += app_->frequencyStep();
    }
    return result;
}