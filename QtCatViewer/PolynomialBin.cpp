/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PolynomialBin.h"

// Constructor
PolynomialBin::PolynomialBin(double aMin, double aMax, double bMin, double bMax,
                             double cMin, double cMax, double dMin, double dMax,
                             double eMin, double eMax, double fMin, double fMax) :
        aMin_(aMin),
        aMax_(aMax),
        bMin_(bMin),
        bMax_(bMax),
        cMin_(cMin),
        cMax_(cMax),
        dMin_(dMin),
        dMax_(dMax),
        eMin_(eMin),
        eMax_(eMax),
        fMin_(fMin),
        fMax_(fMax) {
}

// Return the difference between the polynomial and this range
double PolynomialBin::error(const box::Polynomial& poly) const {
    double result = 0.0;
    if(poly.a() < aMin_) {
        result += aMin_ - poly.a();
    }
    if(poly.a() > aMax_) {
        result += poly.a() - aMax_;
    }
    if(poly.b() < bMin_) {
        result += bMin_ - poly.b();
    }
    if(poly.b() > bMax_) {
        result += poly.b() - bMax_;
    }
    if(poly.c() < cMin_) {
        result += cMin_ - poly.c();
    }
    if(poly.c() > cMax_) {
        result += poly.c() - cMax_;
    }
    if(poly.d() < dMin_) {
        result += dMin_ - poly.d();
    }
    if(poly.d() > dMax_) {
        result += poly.d() - dMax_;
    }
    if(poly.e() < eMin_) {
        result += eMin_ - poly.e();
    }
    if(poly.e() > eMax_) {
        result += poly.e() - eMax_;
    }
    if(poly.f() < fMin_) {
        result += fMin_ - poly.f();
    }
    if(poly.f() > fMax_) {
        result += poly.f() - fMax_;
    }
    return result;
}

// Assign an item to this bin
void PolynomialBin::assignToBin(double error) {
    count_++;
    error_ += error;
}

// Clear this bin
void PolynomialBin::clear() {
    count_ = 0;
    error_ = 0.0;
}

// Split this bin into the specified quantity logarithmically
std::vector<PolynomialBin> PolynomialBin::splitLog(size_t na, size_t nb, size_t nc,
                                                   size_t nd, size_t ne, size_t nf) const {
    std::vector<PolynomialBin> result;
    double aStart = std::log10(aMin_);
    double aStep = (std::log10(aMax_) - aStart) / na;
    double bStart = std::log10(bMin_);
    double bStep = (std::log10(bMax_) - bStart) / nb;
    double cStart = std::log10(cMin_);
    double cStep = (std::log10(cMax_) - cStart) / nc;
    double dStart = std::log10(dMin_);
    double dStep = (std::log10(dMax_) - dStart) / nd;
    double eStart = std::log10(eMin_);
    double eStep = (std::log10(eMax_) - eStart) / ne;
    double fStart = std::log10(fMin_);
    double fStep = (std::log10(fMax_) - fStart) / nf;
    double aMin = aStart;
    for(size_t a = 0; a < na; a++) {
        double bMin = bStart;
        for(size_t b = 0; b < nb; b++) {
            double cMin = cStart;
            for(size_t c = 0; c < nc; c++) {
                double dMin = dStart;
                for(size_t d = 0; d < nd; d++) {
                    double eMin = eStart;
                    for(size_t e = 0; e < ne; e++) {
                        double fMin = fStart;
                        for(size_t f = 0; f < nf; f++) {
                            result.emplace_back(std::pow(10.0,aMin), std::pow(10.0,aMin + aStep),
                                                std::pow(10.0,bMin), std::pow(10.0,bMin + bStep),
                                                std::pow(10.0,cMin), std::pow(10.0,cMin + cStep),
                                                std::pow(10.0,dMin), std::pow(10.0,dMin + dStep),
                                                std::pow(10.0,eMin), std::pow(10.0,eMin + eStep),
                                                std::pow(10.0,fMin), std::pow(10.0,fMin + fStep));
                            fMin += fStep;
                        }
                        eMin += eStep;
                    }
                    dMin += dStep;
                }
                cMin += cStep;
            }
            bMin += bStep;
        }
        aMin += aStep;
    }
    return result;
}

// Split this bin into the specified quantity linearly
std::vector<PolynomialBin> PolynomialBin::splitLin(size_t na, size_t nb, size_t nc,
                                                   size_t nd, size_t ne, size_t nf) const {
    std::vector<PolynomialBin> result;
    double aStart = aMin_;
    double aStep = (aMax_ - aStart) / na;
    double bStart = bMin_;
    double bStep = (bMax_ - bStart) / nb;
    double cStart = cMin_;
    double cStep = (cMax_ - cStart) / nc;
    double dStart = dMin_;
    double dStep = (dMax_ - dStart) / nd;
    double eStart = eMin_;
    double eStep = (eMax_ - eStart) / ne;
    double fStart = fMin_;
    double fStep = (fMax_ - fStart) / nf;
    double aMin = aStart;
    for(size_t a = 0; a < na; a++) {
        double bMin = bStart;
        for(size_t b = 0; b < nb; b++) {
            double cMin = cStart;
            for(size_t c = 0; c < nc; c++) {
                double dMin = dStart;
                for(size_t d = 0; d < nd; d++) {
                    double eMin = eStart;
                    for(size_t e = 0; e < ne; e++) {
                        double fMin = fStart;
                        for(size_t f = 0; f < nf; f++) {
                            result.emplace_back(aMin, aMin + aStep,
                                                bMin, bMin + bStep,
                                                cMin, cMin + cStep,
                                                dMin, dMin + dStep,
                                                eMin, eMin + eStep,
                                                fMin, fMin + fStep);
                            fMin += fStep;
                        }
                        eMin += eStep;
                    }
                    dMin += dStep;
                }
                cMin += cStep;
            }
            bMin += bStep;
        }
        aMin += aStep;
    }
    return result;
}
