/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ItemDlg.h"
#include <BinaryCodedPlateWidget.h>
#include <DataSeriesWidget.h>
#include <Fdtd/CatalogueItem.h>
#include <Box/Constants.h>
#include <Box/BinaryPattern4Fold.h>
#include <Box/BinaryPattern.h>
#include "CatViewer.h"

// Constructor
ItemDlg::ItemDlg(CatViewer* app, bool title) :
        app_(app),
        code_(0) {
    auto* row = new QHBoxLayout;
    // The information column
    auto* info = new QVBoxLayout;
    if(title) {
        info->addWidget(new QLabel("Coding/Pattern/Classify"));
    }
    codeText_ = new QLineEdit("");
    codeText_->setReadOnly(true);
    info->addWidget(codeText_);
    patternText_ = new QLineEdit("");
    patternText_->setReadOnly(true);
    info->addWidget(patternText_);
    classificationText_ = new QLineEdit("");
    classificationText_->setReadOnly(true);
    info->addWidget(classificationText_);
    info->addStretch();
    row->addLayout(info);
    // Bit pattern column
    auto* bitPattern = new QVBoxLayout;
    if(title) {
        bitPattern->addWidget(new QLabel("Plate"));
    }
    bitPatternEdit_ = new BinaryCodedPlateWidget({0},
                                                 app_->bitsPerHalfSide() * 2,
                                                 app_->fourFoldSymmetry(),
                                                 true, 64);
    bitPattern->addWidget(bitPatternEdit_);
    bitPattern->addStretch();
    row->addLayout(bitPattern);
    // Transmittance column
    auto* transmittance = new QVBoxLayout;
    if(title) {
        transmittance->addWidget(new QLabel("Transmittance"));
    }
    transmittanceView_ = new DataSeriesWidget(app_->pCfg());
    transmittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0, 1.0, "");
    transmittance->addWidget(transmittanceView_);
    row->addLayout(transmittance);
    // Phase shift column
    auto* phaseShift = new QVBoxLayout;
    if(title) {
        phaseShift->addWidget(new QLabel("Phase Shift"));
    }
    phaseShiftView_ = new DataSeriesWidget(app_->pCfg());
    phaseShiftView_->setAxisInfo(0.0, 100.0, "GHz", -180.0, 180.0, "deg");
    phaseShift->addWidget(phaseShiftView_);
    row->addLayout(phaseShift);
    // Admittance column
    auto* admittance = new QVBoxLayout;
    if(title) {
        admittance->addWidget(new QLabel("Admittance (green/blue=real)"));
    }
    admittanceView_ = new DataSeriesWidget(app_->pCfg());
    admittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0, 1.0, "");
    admittance->addWidget(admittanceView_);
    row->addLayout(admittance);
    row->addStretch();
    setLayout(row);
}

// Set the item to be displayed by this dialog
void ItemDlg::setItem(uint64_t code) {
    // Clear the GUI
    codeText_->clear();
    patternText_->clear();
    classificationText_->clear();
    bitPatternEdit_->clear();
    transmittanceView_->clear();
    phaseShiftView_->clear();
    admittanceView_->clear();
    if(app_->showClassification()) {
        classificationText_->show();
    } else {
        classificationText_->hide();
    }
    // Now get the item to display
    code_ = code;
    if(code_ < app_->numItems()) {
        std::shared_ptr<fdtd::CatalogueItem> item = app_->getItem(code_);
        codeText_->setText(QString::number(code_));
        if(item) {
            // Polynomial fitting
            box::Polynomial fitted;
            size_t numFrequencies = item->transmittance().size();
            std::vector<double> frequencies(numFrequencies);
            for(size_t i = 0; i < numFrequencies; i++) {
                frequencies[i] = app_->startFrequency() + i * app_->frequencyStep();
            }
            fitted.fit(frequencies, item->transmittance(), 5);
            std::vector<double> fittedData(numFrequencies);
            for(size_t i = 0; i < numFrequencies; i++) {
                fittedData[i] = fitted.value(frequencies[i]);
            }
            std::cout << "Code=" << code_
                      << " f,e,d,c,b,a=" << fitted.f() << "\t" << fitted.e() << "\t"
                      << fitted.d() << "\t" << fitted.c() << "\t" << fitted.b()
                      << "\t" << fitted.a() << std::endl;
            // Fill the GUI
            classificationText_->setText(QString::number(item->classification()));
            patternText_->setText("0x" + QString::number(item->pattern(), 16));
            bitPatternEdit_->setData({item->pattern()},
                                     app_->bitsPerHalfSide() * 2);
            transmittanceView_->setData(item->transmittance(),
                                        GuiConfiguration::elementDataA, true);
            if(item->xyComponents()) {
                transmittanceView_->setData(item->transmittanceY(),
                                            GuiConfiguration::elementDataC);
            }
            if(app_->showClassification()) {
                transmittanceView_->setData(fittedData, GuiConfiguration::elementDataB);
            }
            transmittanceView_->setAxisInfo(app_->startFrequency() / box::Constants::giga_,
                                            app_->frequencyStep() / box::Constants::giga_,
                                            "GHz", 0.0, 1.0, "");
            phaseShiftView_->setData(item->phaseShift(), GuiConfiguration::elementDataA, true);
            if(item->xyComponents()) {
                phaseShiftView_->setData(item->phaseShiftY(), GuiConfiguration::elementDataC);
            }
            phaseShiftView_->setAxisInfo(app_->startFrequency() / box::Constants::giga_,
                                         app_->frequencyStep() / box::Constants::giga_,
                                         "GHz", -180.0, 180.0, "deg");
            std::vector<std::complex<double>> admittance = item->admittance();
            std::vector<std::complex<double>> admittanceY = item->admittanceY();
            double min = 0.0;
            double max = 0.0;
            for(auto& c : admittance) {
                min = std::min(min, c.real());
                min = std::min(min, c.imag());
                max = std::max(max, c.real());
                max = std::max(max, c.imag());
            }
            for(auto& c : admittanceY) {
                min = std::min(min, c.real());
                min = std::min(min, c.imag());
                max = std::max(max, c.real());
                max = std::max(max, c.imag());
            }
            admittanceView_->setData(admittance, GuiConfiguration::elementDataA,
                                     GuiConfiguration::elementDataB, true);
            if(item->xyComponents()) {
                admittanceView_->setData(admittanceY, GuiConfiguration::elementDataC,
                                         GuiConfiguration::elementDataD);
            }
            admittanceView_->setAxisInfo(app_->startFrequency() / box::Constants::giga_,
                                         app_->frequencyStep() / box::Constants::giga_,
                                         "GHz", min, max, "");
        } else {
            box::BinaryPattern pattern(app_->bitsPerHalfSide(), code_);
            if(app_->fourFoldSymmetry()) {
                box::BinaryPattern4Fold coding(app_->bitsPerHalfSide(), code_);
                pattern = coding;
            }
            bitPatternEdit_->setData({pattern.pattern()[0]},
                                     app_->bitsPerHalfSide() * 2);
            if(pattern.hasCornerToCorner()) {
                patternText_->setText("Corner-to-corner");
            } else {
                patternText_->setText("Missing");
            }
        }
    }
}

