#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCEFNSEARCHDLG_H
#define FDTDLIFE_ADMITTANCEFNSEARCHDLG_H

#include <QtIncludes.h>
#include <Box/Polynomial.h>
#include <list>

class CatViewer;
namespace fdtd { class CatalogueItem; }
class DataSeriesWidget;

class AdmittanceFnSearchDlg : public QDialog {
Q_OBJECT
public:
    // Construction
    explicit AdmittanceFnSearchDlg(CatViewer* app);

    // Types
    class Result {
    public:
        Result(std::shared_ptr<fdtd::CatalogueItem> item, double scaling, double mismatch) :
                item_(std::move(item)),
                scaling_(scaling),
                mismatch_(mismatch) {}
        std::shared_ptr<fdtd::CatalogueItem> item_;
        double scaling_;
        double mismatch_;
    };

protected slots:
    // Handlers
    void onSearch();
    void onPaste();
    void onResultSelected();

protected:
    // Helpers
    void fillWidgets();
    void readWidgets();
    double calculateMismatch(const std::shared_ptr<fdtd::CatalogueItem>& item, double gScale);

    // Variables
    CatViewer* app_;
    box::Polynomial real_;
    box::Polynomial imag_;
    double minRepeatCellScale_;
    int numRepeatCellScaleSamples_;
    std::list<Result> results_;

    // Constants
    enum {
        resultsColCoding = 0, resultsColScaling, resultsColMismatch, resultsNumCols
    };
    static constexpr double maxRepeatCellScale_ = 1.0;
    static const int maxNumResults_ = 20;

    // Widgets
    QLineEdit* raEdit_;
    QLineEdit* rbEdit_;
    QLineEdit* rcEdit_;
    QLineEdit* rdEdit_;
    QLineEdit* reEdit_;
    QLineEdit* rfEdit_;
    QLineEdit* iaEdit_;
    QLineEdit* ibEdit_;
    QLineEdit* icEdit_;
    QLineEdit* idEdit_;
    QLineEdit* ieEdit_;
    QLineEdit* ifEdit_;
    QLineEdit* minRepeatCellScaleEdit_;
    QLineEdit* numRepeatCellScaleSamplesEdit_;
    QTreeWidget* resultsTree_;
    QPushButton* searchButton_;
    QPushButton* pasteButton_;
    DataSeriesWidget* realView_;
    DataSeriesWidget* imaginaryView_;
};


#endif //FDTDLIFE_ADMITTANCEFNSEARCHDLG_H
