/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PhaseShiftSearchDlg.h"
#include "CatViewer.h"
#include <DialogHelper.h>
#include <Fdtd/CatalogueItem.h>

// Constructor
PhaseShiftSearchDlg::PhaseShiftSearchDlg(CatViewer* app) :
        app_(app),
        desiredUnitCell_(0.0001),
        desiredFrequency_(100e9) {
    // Main layout
    auto mainLayout = new QHBoxLayout;
    setLayout(mainLayout);
    // The search spec widgets
    auto searchLayout = new QVBoxLayout;
    mainLayout->addLayout(searchLayout);
    desiredUnitCellEdit_ = DialogHelper::doubleItem(searchLayout, "Desired Unit Cell (m)");
    desiredFrequencyEdit_ = DialogHelper::doubleItem(searchLayout, "Desired Frequency (Hz)");
    transmittanceThresholdEdit_ = DialogHelper::doubleItem(searchLayout, "Transmittance Threshold");
    auto* buttonLayout = new QHBoxLayout;
    searchLayout->addLayout(buttonLayout);
    searchButton_ = DialogHelper::buttonItem(buttonLayout, "Search");
    searchLayout->addStretch();
    // The results
    resultsTree_ = new QTreeWidget;
    mainLayout->addWidget(resultsTree_);
    resultsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    resultsTree_->setColumnCount(resultNumCols);
    resultsTree_->setHeaderLabels({"Coding", "Transmittance", "Phase Shift (deg)"});
    resultsTree_->setFixedWidth(310);
    // Connect handlers
    connect(searchButton_, SIGNAL(clicked()), SLOT(onSearch()));
    connect(resultsTree_, SIGNAL(itemSelectionChanged()), SLOT(onResultSelected()));
    // Initial state
    fillWidgets();
}

// Fill the widgets with the current search parameters
void PhaseShiftSearchDlg::fillWidgets() {
    desiredUnitCellEdit_->setText(QString::number(desiredUnitCell_));
    desiredFrequencyEdit_->setText(QString::number(desiredFrequency_));
    transmittanceThresholdEdit_->setText(QString::number(transmittanceThreshold_));
}

// Read the current search parameters from the widgets
void PhaseShiftSearchDlg::readWidgets() {
    desiredUnitCell_ = desiredUnitCellEdit_->text().toDouble();
    desiredFrequency_ = desiredFrequencyEdit_->text().toDouble();
    transmittanceThreshold_ = transmittanceThresholdEdit_->text().toDouble();
}

// The search button has been pressed
void PhaseShiftSearchDlg::onSearch() {
    results_.clear();
    readWidgets();
    // Which bin covers the frequency we are interested in?
    double cellScaling = app_->unitCell() / desiredUnitCell_;
    double startFreq = app_->startFrequency() * cellScaling;
    double freqStep = app_->frequencyStep() * cellScaling;
    if(desiredFrequency_ >= startFreq) {
        auto bin = static_cast<size_t>(std::round((desiredFrequency_ - startFreq) / freqStep));
        // Calculate the results
        for(auto& item : app_->items()) {
            if(item && bin < item->data().numPoints()) {
                double transmittance = item->data().transmittance()[bin];
                double phaseShift = item->data().phaseShift()[bin];
                if(transmittance >= transmittanceThreshold_) {
                    results_.emplace_back(Result(item, transmittance, phaseShift));
                }
            }
        }
    }
    // Sort the results into phase shift order
    results_.sort([](const Result& a, const Result& b) { return a.phaseShift_ < b.phaseShift_; });
    // List the result tree
    resultsTree_->clear();
    for(auto& result : results_) {
        auto item = new QTreeWidgetItem(resultsTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setText(resultCodingCol, QString::number(result.item_->code()));
        item->setText(resultTransmittanceCol, QString::number(result.transmittance_));
        item->setText(resultPhaseShiftCol, QString::number(result.phaseShift_));
    }
    resultsTree_->setCurrentItem(resultsTree_->topLevelItem(0));
}

// A result has been clicked
void PhaseShiftSearchDlg::onResultSelected() {
    // Do we have a selection?
    QTreeWidgetItem* sel = resultsTree_->currentItem();
    if(sel != nullptr) {
        // Find the result corresponding to the selection
        int index = resultsTree_->indexOfTopLevelItem(sel);
        auto pos = results_.begin();
        for(int i = 0; i < index && pos != results_.end(); i++) {
            pos++;
        }
        if(pos != results_.end()) {
            // Position the main window to this item
            app_->showItem(pos->item_->code());
        }
    }
}
