/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatViewer.h"
#include "CatDlg.h"
#include <Fdtd/AdmittanceCatalogue.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Fdtd/CatalogueItem.h>
#include <Fdtd/Configuration.h>
#include <Gui/GuiConfiguration.h>
#include <fstream>
#include <iostream>
#include <cmath>
#include <Box/BinaryPattern.h>

// Constructor
CatViewer::CatViewer() :
        dlg_(nullptr),
        numItems_(0),
        bitsPerHalfSide_(5),
        fourFoldSymmetry_(false),
        unitCell_(0.0008),
        startFrequency_(4e9),
        frequencyStep_(4e9),
        p_(nullptr),
        pCfg_(nullptr) {
    p_ = new fdtd::Configuration(nullptr);
    pCfg_ = new GuiConfiguration(p_);
    dlg_ = new CatDlg(this);
    setTitle();
    setCentralWidget(dlg_);
    //classify_.add(PolynomialBin( /* 0 */
    //        -2e-00, 2e-00,
    //        -1e-10, 1e-10,
    //        -1e-20, 1e-20,
    //        -2e-30, 2e-30,
    //        -1e-40, 1e-40,
    //        -1e-50, 1e-50).splitLin(4,4,4,4,4,4));
    classify_.add(PolynomialBin( /* 0 */
            -1, 0,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 1 */
            -1, 0,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 2 */
            -1, 0,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 3 */
            -1, 0,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 4 */
            -1, 0,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 5 */
            -0.5, -0.25,
            1.25e-11, 2.5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 6 */
            -0.5, -0.25,
            2.5e-11, 3.75e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 7 */
            -0.5, -0.25,
            3.75e-11, 5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 8 */
            -0.1875, -0.125,
            9.375e-12, 1.25e-11,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 9 */
            -0.125, -0.0625,
            3.125e-12, 6.25e-12,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 10 */
            -0.125, -0.0625,
            6.25e-12, 9.375e-12,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 11 */
            -0.125, -0.0625,
            9.375e-12, 1.25e-11,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 12 */
            -0.0625, 0,
            0, 3.125e-12,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 13 */
            -0.0625, 0,
            3.125e-12, 6.25e-12,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 14 */
            -0.0625, 0,
            6.25e-12, 9.375e-12,
            -3.125e-22, 2.82119e-37,
            0, 6.25e-32,
            -3.125e-42, 0,
            0, 3.125e-52));
    classify_.add(PolynomialBin( /* 15 */
            -0.25, 0,
            1.25e-11, 2.5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 16 */
            -0.25, 0,
            2.5e-11, 3.75e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 17 */
            -0.25, 0,
            3.75e-11, 5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 18 */
            -1, 0,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 19 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 20 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 21 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 22 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 23 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 24 */
            -1, 0,
            0, 5e-11,
            0, 5e-21,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 25 */
            0, 1,
            -5e-11, 0,
            -5e-21, 0,
            -1e-30, 0,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 26 */
            0, 1,
            -5e-11, 0,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 27 */
            0, 1,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 28 */
            0, 1,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 29 */
            0, 1,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 30 */
            0, 1,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 31 */
            0, 0.25,
            -3.75e-11, -2.5e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 32 */
            0, 0.25,
            -2.5e-11, -1.25e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 33 */
            0, 0.0625,
            -1.25e-11, -9.375e-12,
            3.125e-22, 6.25e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 34 */
            0, 0.0625,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 35 */
            0, 0.0625,
            -9.375e-12, -6.25e-12,
            3.125e-22, 6.25e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 36 */
            0, 0.0625,
            -6.25e-12, -3.125e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 37 */
            0, 0.0625,
            -6.25e-12, -3.125e-12,
            3.125e-22, 6.25e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 38 */
            0, 0.0625,
            -3.125e-12, 2.42338e-27,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 39 */
            0.0625, 0.125,
            -1.25e-11, -9.375e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 40 */
            0.0625, 0.125,
            -1.25e-11, -9.375e-12,
            3.125e-22, 6.25e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 41 */
            0.0625, 0.125,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 42 */
            0.0625, 0.125,
            -9.375e-12, -6.25e-12,
            3.125e-22, 6.25e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 43 */
            0.0625, 0.125,
            -6.25e-12, -3.125e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 44 */
            0.125, 0.1875,
            -1.25e-11, -9.375e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 45 */
            0.125, 0.1875,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 46 */
            0.125, 0.1875,
            -3.125e-12, 2.42338e-27,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 47 */
            0.1875, 0.25,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 48 */
            0.1875, 0.25,
            -6.25e-12, -3.125e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 49 */
            0.1875, 0.25,
            -3.125e-12, 2.42338e-27,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 50 */
            0.25, 0.5,
            -5e-11, -3.75e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 51 */
            0.25, 0.5,
            -3.75e-11, -2.5e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 52 */
            0.25, 0.5,
            -2.5e-11, -1.25e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 53 */
            0.25, 0.5,
            -1.25e-11, 3.23117e-27,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 54 */
            0.5, 0.75,
            -5e-11, -3.75e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 55 */
            0.5, 0.75,
            -3.75e-11, -2.5e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 56 */
            0.5, 0.75,
            -2.5e-11, -1.25e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 57 */
            0.75, 1,
            -1.25e-11, 3.23117e-27,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 58 */
            0, 1,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 59 */
            0, 1,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 60 */
            0, 1,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 61 */
            0, 1,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 62 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 63 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 64 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 65 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 66 */
            0, 0.25,
            0, 1.25e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 67 */
            0, 0.25,
            1.25e-11, 2.5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 68 */
            0.25, 0.5,
            0, 1.25e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 69 */
            0.5, 0.75,
            1.25e-11, 2.5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 70 */
            0.5, 0.75,
            2.5e-11, 3.75e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 71 */
            0.5, 0.75,
            3.75e-11, 5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 72 */
            0.75, 1,
            0, 1.25e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 73 */
            0.75, 1,
            1.25e-11, 2.5e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 74 */
            0.75, 1,
            2.5e-11, 3.75e-11,
            -1.25e-21, 3.76158e-37,
            0, 2.5e-31,
            -1.25e-41, 0,
            0, 1.25e-51));
    classify_.add(PolynomialBin( /* 75 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 76 */
            0, 1,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 77 */
            0, 1,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 78 */
            0, 1,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 79 */
            0, 1,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 80 */
            0, 1,
            0, 5e-11,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 81 */
            0, 1,
            0, 5e-11,
            0, 5e-21,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 82 */
            1, 2,
            -1e-10, -5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 83 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 84 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 85 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 86 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 87 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 88 */
            1, 2,
            -5e-11, 0,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 89 */
            1, 2,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 90 */
            1, 1.25,
            -2.5e-11, -1.25e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 91 */
            1, 1.0625,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 92 */
            1, 1.0625,
            -6.25e-12, -3.125e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 93 */
            1, 1.0625,
            -3.125e-12, 2.42338e-27,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 94 */
            1.0625, 1.125,
            -1.25e-11, -9.375e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 95 */
            1.0625, 1.125,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 96 */
            1.0625, 1.125,
            -6.25e-12, -3.125e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 97 */
            1.0625, 1.125,
            -3.125e-12, 2.42338e-27,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 98 */
            1.125, 1.1875,
            -1.25e-11, -9.375e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 99 */
            1.125, 1.1875,
            -9.375e-12, -6.25e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 100 */
            1.1875, 1.25,
            -1.25e-11, -9.375e-12,
            0, 3.125e-22,
            -6.25e-32, 0,
            0, 3.125e-42,
            -3.125e-52, 2.22552e-67));
    classify_.add(PolynomialBin( /* 101 */
            1.25, 1.5,
            -3.75e-11, -2.5e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 102 */
            1.25, 1.5,
            -2.5e-11, -1.25e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 103 */
            1.5, 1.75,
            -5e-11, -3.75e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 104 */
            1.5, 1.75,
            -3.75e-11, -2.5e-11,
            0, 1.25e-21,
            -2.5e-31, 0,
            0, 1.25e-41,
            -1.25e-51, 2.96736e-67));
    classify_.add(PolynomialBin( /* 105 */
            1, 2,
            -5e-11, 0,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 106 */
            1, 2,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 107 */
            1, 2,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 108 */
            1, 2,
            -5e-11, 0,
            0, 5e-21,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 109 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 110 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 111 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            -1e-30, 0,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 112 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 113 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            -5e-41, 0,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 114 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            -5e-51, 0));
    classify_.add(PolynomialBin( /* 115 */
            1, 2,
            0, 5e-11,
            -5e-21, 0,
            0, 1e-30,
            0, 5e-41,
            0, 5e-51));
    classify_.add(PolynomialBin( /* 116 */
            1, 2,
            0, 5e-11,
            0, 5e-21,
            -1e-30, 0,
            0, 5e-41,
            -5e-51, 0));
}

// Destructor
CatViewer::~CatViewer() {
    clear();
    delete pCfg_;
    delete p_;
}

// Clears the application down to a pristine state
void CatViewer::clear() {
    items_.clear();
    bitsPerHalfSide_ = 5;
    fourFoldSymmetry_ = true;
    unitCell_ = 0.0008;
    startFrequency_ = 4e9;
    frequencyStep_ = 4e9;
    setNumItems();
}

// Set the application title
void CatViewer::setTitle() {
    QString title("FDTD Catalogue Viewer");
    if(!currentFile_.empty()) {
        title += QString(" - ") + currentFile_.c_str();
    }
    setWindowTitle(title);
}

// Open a catalogue file
void CatViewer::openCatalogue() {
    // Get the file to read
    QString fileName = QFileDialog::getOpenFileName(
            this, "Open FDTD Catalogue", currentFile_.c_str(),
            "FDTD Catalogue Files (*.catalogue)");
    if(!fileName.isEmpty()) {
        QFileInfo fileInfo(fileName);
        std::string basePathName = fileInfo.canonicalPath().toStdString()
                                   + "/" + fileInfo.completeBaseName().toStdString();
        openCatalogue(basePathName);
    }
}

// Open a catalogue file
void CatViewer::openCatalogue(const std::string& fileName) {
    clear();
    currentFile_ = fileName;
    setTitle();
    // Parse file into a DOM
    xml::Reader reader;
    xml::DomDocument dom("binarycatalogue");
    try {
        std::cout << "creating DOM from file" << std::endl;
        reader.readFile(&dom, fileName + ".catalogue");
        // Now process the DOM
        std::cout << "interpreting DOM" << std::endl;
        xml::DomObject* root = dom.getObject();
        *root >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
        *root >> xml::Obj("fourfoldsymmetry") >> fourFoldSymmetry_;
        *root >> xml::Obj("unitcell") >> unitCell_;
        *root >> xml::Obj("startfrequency") >> startFrequency_;
        *root >> xml::Obj("frequencystep") >> frequencyStep_;
        setNumItems();
        for(auto& p : root->obj("catalogueitem")) {
            auto item = std::make_shared<fdtd::CatalogueItem>();
            *p >> *item;
            if(item->code() < items_.size()) {
                items_[item->code()] = item;
            } else {
                std::cout << "Item code " << item->code() << " out of range" << std::endl;
            }
        }
        std::cout << "Complete" << std::endl;
    } catch(xml::Exception& e) {
        std::cout << "Failed to read catalog XML file: " << e.what() << std::endl;
    }
}

// Set the number of items using the bits per half side parameter
void CatViewer::setNumItems() {
    int numBits = 0;
    if(fourFoldSymmetry_) {
        for(int i = 1; i <= bitsPerHalfSide_; i++) {
            numBits += i;
        }
    } else {
        numBits = bitsPerHalfSide_ * bitsPerHalfSide_;
    }
    numItems_ = (uint64_t) std::pow(2.0, numBits);
    items_.resize((size_t) numItems_);
}

// Return the item corresponding to the specified coding
std::shared_ptr<fdtd::CatalogueItem> CatViewer::getItem(uint64_t coding) {
    std::shared_ptr<fdtd::CatalogueItem> result;
    if(coding < items_.size()) {
        result = items_[coding];
    }
    return result;
}

// The search dialog is closing
void CatViewer::searchClosing() {
    dlg_->searchClosing();
}

// Show an item
void CatViewer::showItem(uint64_t coding) {
    dlg_->showItem(coding);
}

// Do various counts
void CatViewer::count(size_t& presentNoCorners, size_t& missingNoCorners,
                      size_t& presentCorners, size_t& missingCorners) {
    setNumItems();
    presentNoCorners = 0;
    presentCorners = 0;
    missingNoCorners = 0;
    missingCorners = 0;
    box::BinaryPattern pattern(bitsPerHalfSide_);
    for(size_t i = 0; i < numItems_; i++) {
        pattern.pattern({i});
        if(i >= items_.size() || !items_[i]) {
            if(pattern.hasCornerToCorner()) {
                missingCorners++;
            } else {
                missingNoCorners++;
            }
        } else {
            if(pattern.hasCornerToCorner()) {
                presentCorners++;
            } else {
                presentNoCorners++;
            }
        }
    }
}

// Fit polynomials to the catalogue items and classify them
void CatViewer::classifyTransmittance() {
    if(!items_.empty()) {
        box::Polynomial fitted;
        size_t numFrequencies = items_.front()->transmittance().size();
        std::vector<double> frequencies(numFrequencies);
        for(size_t i = 0; i < numFrequencies; i++) {
            frequencies[i] = startFrequency_ + i * frequencyStep_;
        }
        for(auto& item : items_) {
            if(item->code() % 10000 == 0) {
                std::cout << "Fitting " << item->code() << std::endl;
            }
            fitted.fit(frequencies, item->transmittance(), 5);
            size_t index = classify_.classify(fitted);
            item->classification(index);
        }
        size_t i = 0;
        size_t total = 0;
        for(auto& bin : classify_.bins()) {
            if(bin.count() > 0U) {
                std::cout << i << ": " << bin.count() << std::endl;
                total += bin.count();
            }
            i++;
        }
        std::cout << "Total: " << total << std::endl;
    }
    dlg_->updateDataDisplay();
}

// Print the classification code
void CatViewer::printClassifyCode() {
    std::stringstream text;
    size_t i = 0;
    for(auto& bin : classify_.bins()) {
        if(bin.count() > 0U) {
            text << "    classify_.add(PolynomialBin( /* " << i << " */\n"
                      << "                  " << bin.aMin() << ", " << bin.aMax() << ",\n"
                      << "                  " << bin.bMin() << ", " << bin.bMax() << ",\n"
                      << "                  " << bin.cMin() << ", " << bin.cMax() << ",\n"
                      << "                  " << bin.dMin() << ", " << bin.dMax() << ",\n"
                      << "                  " << bin.eMin() << ", " << bin.eMax() << ",\n"
                      << "                  " << bin.fMin() << ", " << bin.fMax() << "));"
                      << std::endl;
            i++;
        }
    }
    std::cout << text.str();
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    mimeData->setText(text.str().c_str());
    clipboard->setMimeData(mimeData);
}
