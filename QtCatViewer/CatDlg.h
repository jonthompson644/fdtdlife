/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATDLG_H
#define FDTDLIFE_CATDLG_H

#include <QtIncludes.h>

class CatViewer;
class ItemDlg;
class SearchDlg;

// The main dialog display for the catalogue
class CatDlg : public QDialog {
Q_OBJECT
public:
    // Construction
    explicit CatDlg(CatViewer* app);

    // API
    void searchClosing();
    void showItem(uint64_t coding);
    void updateDataDisplay();

protected slots:
    // Handlers
    void onFromItem();
    void onForward();
    void onBack();
    void onPageForward();
    void onPageBack();
    void onFirst();
    void onLast();
    void onItemsPerPage(int value);
    void onOpen();
    void onShowSearch(int state);
    void onCount();
    void onShowClassification(int state);

protected:
    // Helpers
    void updateInformation();
    void updateNavigation();

protected:
    // Variables
    CatViewer* app_;
    uint64_t firstItemOnPage_;
    uint64_t itemsPerPage_;
    SearchDlg* searchDlg_;

    // Information line GUI items
    QPushButton* openButton_;
    QLineEdit* pixelsPerSideEdit_;
    QLineEdit* unitCellEdit_;
    QLineEdit* symmetryEdit_;
    QCheckBox* showSearchCheck_;

    // Navigation line GUI items
    QLineEdit* fromItemEdit_;
    QPushButton* forwardButton_;
    QPushButton* backButton_;
    QPushButton* pageForwardButton_;
    QPushButton* pageBackButton_;
    QPushButton* firstButton_;
    QPushButton* lastButton_;
    QSpinBox* itemsPerPageSpin_;
    QCheckBox* showClassification_;

    // Statistics line GUI items
    QPushButton* countButton_;
    QLineEdit* totalItemsEdit_;
    QLineEdit* presentNoCornersEdit_;
    QLineEdit* missingNoCornersEdit_;
    QLineEdit* presentCornersEdit_;
    QLineEdit* missingCornersEdit_;

    // Data display
    QVBoxLayout* dataLayout_;

    // Catalogue lines
    std::vector<std::unique_ptr<ItemDlg>> itemLines_;
};


#endif //FDTDLIFE_CATDLG_H
