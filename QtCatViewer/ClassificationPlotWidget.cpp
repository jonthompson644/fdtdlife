/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ClassificationPlotWidget.h"
#include "CatViewer.h"
#include <Fdtd/CatalogueItem.h>

// Constructor
ClassificationPlotWidget::ClassificationPlotWidget(CatViewer* app) :
    app_(app) {
}

// Return a hint regarding the size of the widget
QSize ClassificationPlotWidget::sizeHint() const {
    return {200, 200};
}

// Paint the data
void ClassificationPlotWidget::paintEvent(QPaintEvent* /*event*/) {
    // Draw onto the screen
    QPainter painter(this);
    painter.save();
    painter.setFont(QFont("Arial", 8, QFont::Normal));
    painter.setBrush(Qt::black);
    painter.setPen(Qt::NoPen);
    painter.drawRect(0, 0, width(), height());
    // Calculate the scaling
    calcScaling();
    // Do all the drawing
    drawVerticalAxis(&painter);
    drawHorizontalAxis(&painter);
    drawData(&painter);
    painter.restore();
}

// Add an item to the list to display
void ClassificationPlotWidget::addItem(uint64_t coding) {
    items_.push_back(coding);
    update();
}

// Clear the display
void ClassificationPlotWidget::clear() {
    items_.clear();
    update();
}

// Draw an item
void ClassificationPlotWidget::drawItem(QPainter* painter, uint64_t coding) {
    auto item = app_->getItem(coding);
    if(item) {
        painter->save();
        auto dataY = item->transmittance();
        QPen aPen(Qt::green, 1);
        int x1, ya1 = 0, x2, ya2 = 0;
        x2 = 0;
        ya2 = height() - static_cast<int> (round((dataY[0] - minY_) * scaleY_));
        for(size_t t = 1; t < dataY.size(); t++) {
            x1 = x2;
            ya1 = ya2;
            x2 = static_cast<int>(round(static_cast<double>(t) * stepX_ * scaleX_));
            ya2 = height() - static_cast<int>(round((dataY[t] - minY_) * scaleY_));
            painter->setPen(aPen);
            painter->drawLine(x1, ya1, x2, ya2);
        }
        painter->restore();
    }
}

// Calculate the scaling factors and tick mark intervals.
void ClassificationPlotWidget::calcScaling() {
    // Data (y) axis scaling
    minY_ = 0.0;
    maxY_ = 1.0;
    stepX_ = 2.0;
    scaleY_ = static_cast<double>(height());
    // Y axis tick marks
    tickStepY_ = 0.5;
    // Time (x) axis scaling
    scaleX_ = static_cast<double>(width()) / 400.0;
    // X axis tick marks
    tickStepX_ = 100;
}

// Draw the vertical axis with tick marks and annotation
void ClassificationPlotWidget::drawVerticalAxis(QPainter* painter) {
    // Draw the vertical axis
    QPen axisPen(Qt::blue, 1);
    QPen textPen(Qt::gray, 1);
    painter->save();
    double tick = (floor(minY_ / tickStepY_) + 1.0) * tickStepY_;
    while(tick < maxY_) {
        QString text = QString("%1%2").arg(tick).arg(unitsY_);
        int tickY = height() - static_cast<int>(round((tick - minY_) * scaleY_));
        painter->setPen(axisPen);
        painter->drawLine(0, tickY, width(), tickY);
        painter->setPen(textPen);
        painter->drawText(5, tickY, text);
        tick += tickStepY_;
    }
    painter->restore();
}

// Draw the horizontal axis with tick marks and annotation
void ClassificationPlotWidget::drawHorizontalAxis(QPainter* painter) {
    QPen axisPen(Qt::blue, 1);
    QPen textPen(Qt::gray, 1);
    painter->save();
    double tick = (floor(minX_ / tickStepX_) + 1.0) * tickStepX_;
    while(tick < 400) {
        QString text = QString("%1%2").arg(tick).arg(unitsX_);
        int tickX = static_cast<int>(round((tick - minX_) * scaleX_));
        painter->setPen(axisPen);
        painter->drawLine(tickX, height(), tickX, 0);
        painter->setPen(textPen);
        painter->drawText(tickX - 10, height() - 5, text);
        tick += tickStepX_;
    }
    painter->restore();
}

// Draw the data
void ClassificationPlotWidget::drawData(QPainter* painter) {
    // Draw the data
    painter->save();
    for(auto coding : items_) {
        drawItem(painter, coding);
    }
    painter->restore();
}
