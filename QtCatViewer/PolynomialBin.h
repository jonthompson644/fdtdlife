/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_POLYNOMIALBIN_H
#define FDTDLIFE_POLYNOMIALBIN_H

#include <vector>
#include <Box/Polynomial.h>

// A record of the items that fall into the scope of a range
class PolynomialBin {
public:
    // Construction
    PolynomialBin(double aMin, double aMax, double bMin, double bMax,
                  double cMin, double cMax, double dMin, double dMax,
                  double eMin, double eMax, double fMin, double fMax);

    // API
    [[nodiscard]] double error(const box::Polynomial& poly) const;
    void assignToBin(double error);
    void clear();
    std::vector<PolynomialBin> splitLog(size_t na, size_t nb, size_t nc,
                                        size_t nd, size_t ne, size_t nf) const;
    std::vector<PolynomialBin> splitLin(size_t na, size_t nb, size_t nc,
                                        size_t nd, size_t ne, size_t nf) const;

    // Getters
    [[nodiscard]] size_t count() const { return count_; }
    [[nodiscard]] double aMin() const { return aMin_; }
    [[nodiscard]] double aMax() const { return aMax_; }
    [[nodiscard]] double bMin() const { return bMin_; }
    [[nodiscard]] double bMax() const { return bMax_; }
    [[nodiscard]] double cMin() const { return cMin_; }
    [[nodiscard]] double cMax() const { return cMax_; }
    [[nodiscard]] double dMin() const { return dMin_; }
    [[nodiscard]] double dMax() const { return dMax_; }
    [[nodiscard]] double eMin() const { return eMin_; }
    [[nodiscard]] double eMax() const { return eMax_; }
    [[nodiscard]] double fMin() const { return fMin_; }
    [[nodiscard]] double fMax() const { return fMax_; }
    [[nodiscard]] double error() const { return error_; }

protected:
    double aMin_{};
    double aMax_{};
    double bMin_{};
    double bMax_{};
    double cMin_{};
    double cMax_{};
    double dMin_{};
    double dMax_{};
    double eMin_{};
    double eMax_{};
    double fMin_{};
    double fMax_{};
    size_t count_{};
    double error_{};
};


#endif //FDTDLIFE_POLYNOMIALBIN_H
