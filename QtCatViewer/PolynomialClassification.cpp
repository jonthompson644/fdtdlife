/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PolynomialClassification.h"

// Constructor that generates bins from a set of ranges
PolynomialClassification::PolynomialClassification(const RangeSpec& a, const RangeSpec& b,
                                                   const RangeSpec& c, const RangeSpec& d,
                                                   const RangeSpec& e, const RangeSpec& f) {
    // Create the bins
    for(size_t as = 0; as < a.steps_; as++) {
        for(size_t bs = 0; bs < b.steps_; bs++) {
            for(size_t cs = 0; cs < c.steps_; cs++) {
                for(size_t ds = 0; ds < d.steps_; ds++) {
                    for(size_t es = 0; es < e.steps_; es++) {
                        for(size_t fs = 0; fs < f.steps_; fs++) {
                            bins_.emplace_back(a.lower(as), a.upper(as),
                                               b.lower(bs), b.upper(bs),
                                               c.lower(bs), c.upper(bs),
                                               d.lower(bs), d.upper(bs),
                                               e.lower(bs), e.upper(bs),
                                               f.lower(bs), f.upper(bs));
                            bins_.emplace_back(-a.upper(as), -a.lower(as),
                                               -b.upper(bs), -b.lower(bs),
                                               -c.upper(bs), -c.lower(bs),
                                               -d.upper(bs), -d.lower(bs),
                                               -e.upper(bs), -e.lower(bs),
                                               -f.upper(bs), -f.lower(bs));
                        }
                    }
                }
            }
        }
    }
}

// Add a bin
void PolynomialClassification::add(const PolynomialBin& bin) {
    bins_.push_back(bin);
}

// Add bins
void PolynomialClassification::add(const std::vector<PolynomialBin>& bins) {
    for(auto& bin : bins) {
        bins_.push_back(bin);
    }
}

// Classify the data into a polynomial bin
// Returns the bin index it is assigned to
size_t PolynomialClassification::classify(const box::Polynomial& poly) {
    size_t result = 0;
    double bestError = 0.0;
    for(size_t i = 0; i < bins_.size(); ++i) {
        double error = bins_[i].error(poly);
        if(i == 0 || error < bestError) {
            bestError = error;
            result = i;
        }
    }
    if(!bins_.empty()) {
        bins_[result].assignToBin(bestError);
    }
    return result;
}

