/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CLASSIFICATIONDLG_H
#define FDTDLIFE_CLASSIFICATIONDLG_H

#include <QtIncludes.h>
#include <Gui/InitialisingGui.h>
#include "ClassificationPlotWidget.h"

class CatViewer;

class ClassificationDlg : public QDialog {
Q_OBJECT
public:
    // Construction
    explicit ClassificationDlg(CatViewer* app);

protected slots:
    // Handlers
    void onClassify();
    void onPrintCode();
    void onBinSelected();
    void onMemberSelected();

protected:
    // Variables
    CatViewer* app_;

    // Constants
    enum {
        classificationColId = 0, classificationColQty, classificationColError,
        classificationNumCols
    };
    enum {
        membersColCode = 0, membersColError, membersNumCols
    };

    // Widgets
    InitialisingGui initialising_;
    QPushButton* classifyButton_;
    QPushButton* printCodeButton_;
    QTreeWidget* classificationTree_;
    QTreeWidget* membersTree_;
    ClassificationPlotWidget* plot_;
    QLineEdit* classMinAEdit_;
    QLineEdit* classMinBEdit_;
    QLineEdit* classMinCEdit_;
    QLineEdit* classMinDEdit_;
    QLineEdit* classMinEEdit_;
    QLineEdit* classMinFEdit_;
    QLineEdit* classMaxAEdit_;
    QLineEdit* classMaxBEdit_;
    QLineEdit* classMaxCEdit_;
    QLineEdit* classMaxDEdit_;
    QLineEdit* classMaxEEdit_;
    QLineEdit* classMaxFEdit_;
    QLineEdit* memberAEdit_;
    QLineEdit* memberBEdit_;
    QLineEdit* memberCEdit_;
    QLineEdit* memberDEdit_;
    QLineEdit* memberEEdit_;
    QLineEdit* memberFEdit_;


    // Helper functions
    void fillClassificationTree();
};


#endif //FDTDLIFE_CLASSIFICATIONDLG_H
