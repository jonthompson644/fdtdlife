/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ClassificationDlg.h"
#include "DialogHelper.h"
#include "CatViewer.h"
#include <Fdtd/CatalogueItem.h>

// Constructor
ClassificationDlg::ClassificationDlg(CatViewer* app) :
        app_(app) {
    // Main layout
    auto* mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    // The buttons
    auto* buttonLayout = new QHBoxLayout;
    mainLayout->addLayout(buttonLayout);
    classifyButton_ = DialogHelper::buttonItem(buttonLayout, "Classify");
    printCodeButton_ = DialogHelper::buttonItem(buttonLayout, "Print Code");
    buttonLayout->addStretch();
    // The classifications list
    auto* classificationLayout = new QHBoxLayout;
    mainLayout->addLayout(classificationLayout);
    classificationTree_ = new QTreeWidget;
    classificationLayout->addWidget(classificationTree_);
    classificationTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    classificationTree_->setColumnCount(classificationNumCols);
    classificationTree_->setHeaderLabels({"Id", "Quantity", "Error"});
    classificationTree_->setFixedWidth(250);
    // The members list
    membersTree_ = new QTreeWidget;
    classificationLayout->addWidget(membersTree_);
    membersTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    membersTree_->setColumnCount(membersNumCols);
    membersTree_->setHeaderLabels({"Code", "Error"});
    membersTree_->setFixedWidth(200);
    // The plot
    plot_ = new ClassificationPlotWidget(app_);
    classificationLayout->addWidget(plot_);
    // The class min coefficients
    auto* classMinLayout = new QHBoxLayout;
    mainLayout->addLayout(classMinLayout);
    classMinAEdit_ = DialogHelper::doubleItem(classMinLayout, "Min: A", true);
    classMinBEdit_ = DialogHelper::doubleItem(classMinLayout, "B", true);
    classMinCEdit_ = DialogHelper::doubleItem(classMinLayout, "C", true);
    classMinDEdit_ = DialogHelper::doubleItem(classMinLayout, "D", true);
    classMinEEdit_ = DialogHelper::doubleItem(classMinLayout, "E", true);
    classMinFEdit_ = DialogHelper::doubleItem(classMinLayout, "F", true);
    // The class max coefficients
    auto* classMaxLayout = new QHBoxLayout;
    mainLayout->addLayout(classMaxLayout);
    classMaxAEdit_ = DialogHelper::doubleItem(classMaxLayout, "Max: A", true);
    classMaxBEdit_ = DialogHelper::doubleItem(classMaxLayout, "B", true);
    classMaxCEdit_ = DialogHelper::doubleItem(classMaxLayout, "C", true);
    classMaxDEdit_ = DialogHelper::doubleItem(classMaxLayout, "D", true);
    classMaxEEdit_ = DialogHelper::doubleItem(classMaxLayout, "E", true);
    classMaxFEdit_ = DialogHelper::doubleItem(classMaxLayout, "F", true);
    // The member coefficients
    auto* memberLayout = new QHBoxLayout;
    mainLayout->addLayout(memberLayout);
    memberAEdit_ = DialogHelper::doubleItem(memberLayout, "Member: A", true);
    memberBEdit_ = DialogHelper::doubleItem(memberLayout, "B", true);
    memberCEdit_ = DialogHelper::doubleItem(memberLayout, "C", true);
    memberDEdit_ = DialogHelper::doubleItem(memberLayout, "D", true);
    memberEEdit_ = DialogHelper::doubleItem(memberLayout, "E", true);
    memberFEdit_ = DialogHelper::doubleItem(memberLayout, "F", true);
    // Space at the bottom
    mainLayout->addStretch();
    // Connect handlers
    connect(classifyButton_, SIGNAL(clicked()), SLOT(onClassify()));
    connect(printCodeButton_, SIGNAL(clicked()), SLOT(onPrintCode()));
    connect(classificationTree_, SIGNAL(itemSelectionChanged()), SLOT(onBinSelected()));
    connect(membersTree_, SIGNAL(itemSelectionChanged()), SLOT(onMemberSelected()));
    // Initialise
    fillClassificationTree();
}

// Classify the items
void ClassificationDlg::onClassify() {
    app_->classifyTransmittance();
    fillClassificationTree();
}

// Print the classification code
void ClassificationDlg::onPrintCode() {
    app_->printClassifyCode();
}

// Fill the classification tree
void ClassificationDlg::fillClassificationTree() {
    classificationTree_->clear();
    size_t i = 0;
    {
        InitialisingGui::Set set(initialising_);
        for(auto& bin : app_->classify().bins()) {
            if(bin.count() > 0) {
                auto item = new QTreeWidgetItem(classificationTree_);
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                item->setText(classificationColId, QString::number(i));
                item->setText(classificationColQty, QString::number(bin.count()));
                item->setText(classificationColError, QString::number(bin.error()));
            }
            i++;
        }
    }
    if(i > 0) {
        classificationTree_->setCurrentItem(classificationTree_->topLevelItem(0));
    }
}

// A classification has been selected
void ClassificationDlg::onBinSelected() {
    if(!initialising_) {
        // Clear the plot and members list
        plot_->clear();
        membersTree_->clear();
        // Do we have a selection?
        QTreeWidgetItem* sel = classificationTree_->currentItem();
        if(sel != nullptr) {
            size_t n = 0;
            {
                InitialisingGui::Set set(initialising_);
                // Find the classification selected
                size_t id = sel->text(classificationColId).toULongLong();
                auto& bin = app_->classify().bins()[id];
                // Fill the class coefficients
                classMinAEdit_->setText(QString::number(bin.aMin()));
                classMinBEdit_->setText(QString::number(bin.bMin()));
                classMinCEdit_->setText(QString::number(bin.cMin()));
                classMinDEdit_->setText(QString::number(bin.dMin()));
                classMinEEdit_->setText(QString::number(bin.eMin()));
                classMinFEdit_->setText(QString::number(bin.fMin()));
                classMaxAEdit_->setText(QString::number(bin.aMax()));
                classMaxBEdit_->setText(QString::number(bin.bMax()));
                classMaxCEdit_->setText(QString::number(bin.cMax()));
                classMaxDEdit_->setText(QString::number(bin.dMax()));
                classMaxEEdit_->setText(QString::number(bin.eMax()));
                classMaxFEdit_->setText(QString::number(bin.fMax()));
                // Place all the items in that class in the plot and the members list
                for(auto& item : app_->items()) {
                    if(item->classification() == id) {
                        plot_->addItem(item->code());
                        if(n < 5000) {
                            box::Polynomial fitted;
                            size_t numFrequencies = item->transmittance().size();
                            std::vector<double> frequencies(numFrequencies);
                            for(size_t i = 0; i < numFrequencies; i++) {
                                frequencies[i] = app_->startFrequency() + i * app_->frequencyStep();
                            }
                            fitted.fit(frequencies, item->transmittance(), 5);
                            auto memberItem = new QTreeWidgetItem(membersTree_);
                            memberItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                            memberItem->setText(membersColCode,
                                                QString::number(item->code()));
                            memberItem->setText(membersColError,
                                                QString::number(bin.error(fitted)));
                        }
                        n++;
                    }
                }
            }
            if(n > 0) {
                membersTree_->setCurrentItem(membersTree_->topLevelItem(0));
            }
        }
    }
}

// A member has been selected
void ClassificationDlg::onMemberSelected() {
    if(!initialising_) {
        // Do we have a selection?
        QTreeWidgetItem* sel = membersTree_->currentItem();
        if(sel != nullptr) {
            // Find the code selected
            uint64_t code = sel->text(membersColCode).toULongLong();
            app_->showItem(code);
            // Fill the member coefficients
            auto member = app_->getItem(code);
            box::Polynomial fitted;
            size_t numFrequencies = member->transmittance().size();
            std::vector<double> frequencies(numFrequencies);
            for(size_t i = 0; i < numFrequencies; i++) {
                frequencies[i] = app_->startFrequency() + i * app_->frequencyStep();
            }
            fitted.fit(frequencies, member->transmittance(), 5);
            memberAEdit_->setText(QString::number(fitted.a()));
            memberBEdit_->setText(QString::number(fitted.b()));
            memberCEdit_->setText(QString::number(fitted.c()));
            memberDEdit_->setText(QString::number(fitted.d()));
            memberEEdit_->setText(QString::number(fitted.e()));
            memberFEdit_->setText(QString::number(fitted.f()));
        }
    }
}
