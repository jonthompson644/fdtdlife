cmake_minimum_required(VERSION 3.8)
project(FdTdQtApp)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmakemodules")
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(Qt5_DIR /usr/local/opt/qt/lib/cmake/Qt5)
find_package(Qt5 COMPONENTS Core Widgets Test Network PrintSupport REQUIRED)

find_package(FFmpeg REQUIRED)
include_directories(${FFMPEG_INCLUDE_DIR})

set(SOURCE_FILES main.cpp)

add_executable(FdTdQtApp ${SOURCE_FILES})
target_link_libraries(FdTdQtApp GaLibrary)
target_link_libraries(FdTdQtApp GsLibrary)
target_link_libraries(FdTdQtApp XmlLibrary)
target_link_libraries(FdTdQtApp FdTdLibrary)
target_link_libraries(FdTdQtApp FdTdGui)
target_link_libraries(FdTdQtApp BoxLibrary)
target_link_libraries(FdTdQtApp SourceLibrary)
target_link_libraries(FdTdQtApp SensorLibrary)
target_link_libraries(FdTdQtApp SeqLibrary)
target_link_libraries(FdTdQtApp DomainLibrary)
target_link_libraries(FdTdQtApp DesignerLibrary)
target_link_libraries(FdTdQtApp Qt5::Core Qt5::Widgets Qt5::Test Qt5::Network Qt5::PrintSupport)
target_link_libraries(FdTdQtApp ${FFMPEG_LIBRARIES})

# Uncomment for openMp
if(DEFINED CMAKE_USE_OPEN_MP)
    target_link_libraries(FdTdQtApp /usr/local/opt/libomp/lib/libomp.a)
endif(DEFINED CMAKE_USE_OPEN_MP)
