
QT       += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtApp
TEMPLATE = app
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Gui-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lGui
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Gui-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lGui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lLibrary

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Box-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lBox
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Box-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lBox

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Domain-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lDomain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Domain-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lDomain

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Lens-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lLens
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Lens-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lLens

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Eckt-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lEckt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Eckt-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lEckt

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Ga-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lGa
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Ga-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lGa

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Sensor-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lSensor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Sensor-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lSensor

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Source-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lSource
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Source-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lSource

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Seq-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lSeq
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Seq-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lSeq

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Gs-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lGs
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Gs-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lGs

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Xml-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lXml
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Xml-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lXml

INCLUDEPATH += $$PWD/../Fdtd
DEPENDPATH += $$PWD/../Fdtd

INCLUDEPATH += $$PWD/../Gui
DEPENDPATH += $$PWD/../Gui
INCLUDEPATH += ..


