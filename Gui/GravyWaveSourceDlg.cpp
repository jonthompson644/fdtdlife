/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GravyWaveSourceDlg.h"
#include <Source/GravyWaveSource.h>
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include "SourcesDlg.h"
#include "FdTdLife.h"
#include <sstream>

// Constructor
GravyWaveSourceDlg::GravyWaveSourceDlg(SourcesDlg* dlg, source::GravyWaveSource* source) :
        SourceDlg(dlg, source),
        source_(source) {
    // Line 1
    auto line1Layout = new QHBoxLayout;
    firstFrequencyEdit_ = DialogHelper::textItem(line1Layout,
                                                   "Frequencies, First (Hz)");
    frequencySpacingEdit_ = DialogHelper::textItem(line1Layout, "Spacing (Hz)");
    numFrequenciesEdit_ = DialogHelper::textItem(line1Layout, "N");
    layout_->addLayout(line1Layout);
    // Line 2
    auto line2Layout = new QHBoxLayout;
    azimuthEdit_ = DialogHelper::doubleItem(line2Layout, "Azimuth (0..360 deg)");
    elevationEdit_ = DialogHelper::doubleItem(line2Layout, "Elevation (0..180 deg)");
    layout_->addLayout(line2Layout);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    polarisationEdit_ = DialogHelper::doubleItem(line3Layout,
                                                 "Polarisation (0..180 deg)");
    magneticFieldEdit_ = DialogHelper::doubleItem(line3Layout, "Magnetic Field (T)");
    layout_->addLayout(line3Layout);
    // Line 4
    auto line4Layout = new QHBoxLayout;
    peakAmplitudeEdit_ = DialogHelper::doubleItem(line4Layout, "Amplitude");
    timeOfMaximumEdit_ = DialogHelper::doubleItem(line4Layout, "Time of Peak (s)");
    widthAtHalfHeightEdit_ = DialogHelper::doubleItem(line4Layout, "Width (s)");
    layout_->addLayout(line4Layout);
    // Line 5
    auto line5Layout = new QHBoxLayout;
    std::tie(xCenterEdit_, yCenterEdit_, zCenterEdit_) = DialogHelper::vectorItem(
            line5Layout, "Zone Center (m)");
    layout_->addLayout(line5Layout);
    // Line 6
    auto line6Layout = new QHBoxLayout;
    std::tie(xSizeEdit_, ySizeEdit_, zSizeEdit_) = DialogHelper::vectorItem(
            line6Layout, "Zone Size (m)");
    layout_->addLayout(line6Layout);
    // Line 7
    auto line7Layout = new QHBoxLayout;
    modulationWavelength_ = DialogHelper::textItem(line7Layout, "Modulation Wavelength (m)");
    modulationPhase_ = DialogHelper::textItem(line7Layout, "Phase (deg)");
    modulationAmplitude_ = DialogHelper::textItem(line7Layout, "Amplitude (T)");
    layout_->addLayout(line7Layout);
    // Line 8
    auto line8Layout = new QHBoxLayout;
    copyProfileCsv_ = DialogHelper::buttonItem(line8Layout, "Copy Profile CSV");
    layout_->addLayout(line8Layout);
    // Connect the handlers
    connect(firstFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencySpacingEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numFrequenciesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(azimuthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(elevationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(polarisationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(magneticFieldEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(peakAmplitudeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(timeOfMaximumEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(widthAtHalfHeightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(yCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(zCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ySizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(zSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(modulationWavelength_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(modulationPhase_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(modulationAmplitude_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(copyProfileCsv_, SIGNAL(clicked()), SLOT(onCopyProfileCsv()));
    // Fill the dialog
    GravyWaveSourceDlg::initialise();
}

// Fill the dialog widgets
void GravyWaveSourceDlg::initialise() {
    // Base class first
    SourceDlg::initialise();
    // Now my stuff
    firstFrequencyEdit_->setText(source_->frequencies().first().text().c_str());
    firstFrequencyEdit_->setToolTip(QString::number(source_->frequencies().first().value()));
    frequencySpacingEdit_->setText(source_->frequencies().spacing().text().c_str());
    frequencySpacingEdit_->setToolTip(QString::number(source_->frequencies().spacing().value()));
    numFrequenciesEdit_->setText(source_->frequencies().n().text().c_str());
    numFrequenciesEdit_->setToolTip(QString::number(source_->frequencies().n().value()));
    azimuthEdit_->setText(source_->azimuth().text().c_str());
    azimuthEdit_->setToolTip(QString::number(source_->azimuth().value()));
    elevationEdit_->setText(source_->elevation().text().c_str());
    elevationEdit_->setToolTip(QString::number(source_->elevation().value()));
    polarisationEdit_->setText(source_->polarisation().text().c_str());
    polarisationEdit_->setToolTip(QString::number(source_->polarisation().value()));
    magneticFieldEdit_->setText(source_->magneticField().text().c_str());
    magneticFieldEdit_->setToolTip(QString::number(source_->magneticField().value()));
    peakAmplitudeEdit_->setText(source_->peakAmplitude().text().c_str());
    peakAmplitudeEdit_->setToolTip(QString::number(source_->peakAmplitude().value()));
    timeOfMaximumEdit_->setText(source_->timeOfMaximum().text().c_str());
    timeOfMaximumEdit_->setToolTip(QString::number(source_->timeOfMaximum().value()));
    widthAtHalfHeightEdit_->setText(source_->widthAtHalfHeight().text().c_str());
    widthAtHalfHeightEdit_->setToolTip(QString::number(source_->widthAtHalfHeight().value()));
    xCenterEdit_->setText(source_->zoneCenter().x().text().c_str());
    xCenterEdit_->setToolTip(QString::number(source_->zoneCenter().x().value()));
    yCenterEdit_->setText(source_->zoneCenter().y().text().c_str());
    yCenterEdit_->setToolTip(QString::number(source_->zoneCenter().y().value()));
    zCenterEdit_->setText(source_->zoneCenter().z().text().c_str());
    zCenterEdit_->setToolTip(QString::number(source_->zoneCenter().z().value()));
    xSizeEdit_->setText(source_->zoneSize().x().text().c_str());
    xSizeEdit_->setToolTip(QString::number(source_->zoneSize().x().value()));
    ySizeEdit_->setText(source_->zoneSize().y().text().c_str());
    ySizeEdit_->setToolTip(QString::number(source_->zoneSize().y().value()));
    zSizeEdit_->setText(source_->zoneSize().z().text().c_str());
    zSizeEdit_->setToolTip(QString::number(source_->zoneSize().z().value()));
    modulationWavelength_->setText(source_->modulationWavelength().text().c_str());
    modulationWavelength_->setToolTip(QString::number(source_->modulationWavelength().value()));
    modulationPhase_->setText(source_->modulationPhase().text().c_str());
    modulationPhase_->setToolTip(QString::number(source_->modulationPhase().value()));
    modulationAmplitude_->setText(source_->modulationAmplitude().text().c_str());
    modulationAmplitude_->setToolTip(QString::number(source_->modulationAmplitude().value()));
}

// A source edit field has changed
void GravyWaveSourceDlg::onChange() {
    // Base class first
    SourceDlg::onChange();
    // Now my stuff
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newFrequency = c.testString(firstFrequencyEdit_,
                                         source_->frequencies().first().text(),
                                         FdTdLife::notifyMinorChange);
        auto newSpacing = c.testString(frequencySpacingEdit_,
                                       source_->frequencies().spacing().text(),
                                       FdTdLife::notifyMinorChange);
        auto newNumFrequencies = c.testString(numFrequenciesEdit_,
                                              source_->frequencies().n().text(),
                                              FdTdLife::notifyMinorChange);
        auto newAzimuth = c.testString(azimuthEdit_, source_->azimuth().text(),
                                       FdTdLife::notifyMinorChange);
        auto newElevation = c.testString(elevationEdit_, source_->elevation().text(),
                                         FdTdLife::notifyMinorChange);
        auto newPolarisation = c.testString(polarisationEdit_, source_->polarisation().text(),
                                            FdTdLife::notifyMinorChange);
        auto newMagneticField = c.testString(magneticFieldEdit_, source_->magneticField().text(),
                                             FdTdLife::notifyMinorChange);
        auto newPeakAmplitude = c.testString(peakAmplitudeEdit_, source_->peakAmplitude().text(),
                                             FdTdLife::notifyMinorChange);
        auto newTimeOfMaximum = c.testString(timeOfMaximumEdit_, source_->timeOfMaximum().text(),
                                             FdTdLife::notifyMinorChange);
        auto newWidthAtHalfHeight = c.testString(widthAtHalfHeightEdit_,
                                                 source_->widthAtHalfHeight().text(),
                                                 FdTdLife::notifyMinorChange);
        auto newZoneCenterX = c.testString(xCenterEdit_, source_->zoneCenter().x().text(),
                                           FdTdLife::notifyMinorChange);
        auto newZoneCenterY = c.testString(yCenterEdit_, source_->zoneCenter().y().text(),
                                           FdTdLife::notifyMinorChange);
        auto newZoneCenterZ = c.testString(zCenterEdit_, source_->zoneCenter().z().text(),
                                           FdTdLife::notifyMinorChange);
        auto newZoneSizeX = c.testString(xSizeEdit_, source_->zoneSize().x().text(),
                                           FdTdLife::notifyMinorChange);
        auto newZoneSizeY = c.testString(ySizeEdit_, source_->zoneSize().y().text(),
                                         FdTdLife::notifyMinorChange);
        auto newZoneSizeZ = c.testString(zSizeEdit_, source_->zoneSize().z().text(),
                                         FdTdLife::notifyMinorChange);
        auto newModulationWavelength = c.testString(modulationWavelength_,
                                                    source_->modulationWavelength().text(),
                                                    FdTdLife::notifyMinorChange);
        auto newModulationPhase = c.testString(modulationPhase_,
                                                    source_->modulationPhase().text(),
                                                    FdTdLife::notifyMinorChange);
        auto newModulationAmplitude = c.testString(modulationAmplitude_,
                                               source_->modulationAmplitude().text(),
                                               FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            source_->frequencies().text(newFrequency, newSpacing, newNumFrequencies);
            source_->azimuth(box::Expression(newAzimuth));
            source_->elevation(box::Expression(newElevation));
            source_->polarisation(box::Expression(newPolarisation));
            source_->magneticField(box::Expression(newMagneticField));
            source_->peakAmplitude(box::Expression(newPeakAmplitude));
            source_->timeOfMaximum(box::Expression(newTimeOfMaximum));
            source_->widthAtHalfHeight(box::Expression(newWidthAtHalfHeight));
            source_->zoneCenter(box::VectorExpression<double>(
                    newZoneCenterX, newZoneCenterY, newZoneCenterZ));
            source_->zoneSize(box::VectorExpression<double>(
                    newZoneSizeX, newZoneSizeY, newZoneSizeZ));
            source_->modulationWavelength(box::Expression(newModulationWavelength));
            source_->modulationPhase(box::Expression(newModulationPhase));
            source_->modulationAmplitude(box::Expression(newModulationAmplitude));
            dlg_->model()->doNotification(c.why());
        }
    }
}

// Handle a notification
void GravyWaveSourceDlg::notify(fdtd::Notifier* source, int /*why*/,
                                fdtd::NotificationData* /*data*/) {
    if(dlg_->model() == source) {
    }
}

// Copy CSV data for the magnetic field profile of the zone to the clipboard
void GravyWaveSourceDlg::onCopyProfileCsv() {
    // Copy the data to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    mimeData->setText(source_->getMagneticFileProfileCsv().c_str());
    clipboard->setMimeData(mimeData);
}

