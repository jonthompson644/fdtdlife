/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CuboidArrayDlg.h"
#include "DialogHelper.h"
#include <Domain/CuboidArray.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Constructor
void CuboidArrayDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::CuboidArray*>(shape);
    std::tie(centerXEdit_, centerYEdit_, centerZEdit_) =
            DialogHelper::vectorItem(layout_, "Center x,y,z (m)");
    std::tie(sizeXEdit_, sizeYEdit_, sizeZEdit_) =
            DialogHelper::vectorItem(layout_, "Size x,y,z (m)");
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment x,y,z (m)");
    std::tie(duplicateXEdit_, duplicateYEdit_, duplicateZEdit_) =
            DialogHelper::vectorItem(layout_, "Duplicate x,y,z (m)");
    this->connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(centerYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(sizeXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(sizeYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(sizeZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void CuboidArrayDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    centerXEdit_->setText(shape_->center().x().text().c_str());
    centerYEdit_->setText(shape_->center().y().text().c_str());
    centerZEdit_->setText(shape_->center().z().text().c_str());
    sizeXEdit_->setText(shape_->size().x().text().c_str());
    sizeYEdit_->setText(shape_->size().y().text().c_str());
    sizeZEdit_->setText(shape_->size().z().text().c_str());
    incrementXEdit_->setText(shape_->increment().x().text().c_str());
    incrementYEdit_->setText(shape_->increment().y().text().c_str());
    incrementZEdit_->setText(shape_->increment().z().text().c_str());
    duplicateXEdit_->setText(shape_->duplicate().x().text().c_str());
    duplicateYEdit_->setText(shape_->duplicate().y().text().c_str());
    duplicateZEdit_->setText(shape_->duplicate().z().text().c_str());
    centerXEdit_->setToolTip(QString::number(shape_->center().x().value()));
    centerYEdit_->setToolTip(QString::number(shape_->center().y().value()));
    centerZEdit_->setToolTip(QString::number(shape_->center().z().value()));
    sizeXEdit_->setToolTip(QString::number(shape_->size().x().value()));
    sizeYEdit_->setToolTip(QString::number(shape_->size().y().value()));
    sizeZEdit_->setToolTip(QString::number(shape_->size().z().value()));
    incrementXEdit_->setToolTip(QString::number(shape_->increment().x().value()));
    incrementYEdit_->setToolTip(QString::number(shape_->increment().y().value()));
    incrementZEdit_->setToolTip(QString::number(shape_->increment().z().value()));
    duplicateXEdit_->setToolTip(QString::number(shape_->duplicate().x().value()));
    duplicateYEdit_->setToolTip(QString::number(shape_->duplicate().y().value()));
    duplicateZEdit_->setToolTip(QString::number(shape_->duplicate().z().value()));
}

// An edit box has changed
void CuboidArrayDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newcenterX = c.testString(centerXEdit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newcenterY = c.testString(centerYEdit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newcenterZ = c.testString(centerZEdit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newsizeX = c.testString(sizeXEdit_, shape_->size().x().text(),
                                     FdTdLife::notifyDomainContentsChange);
        auto newsizeY = c.testString(sizeYEdit_, shape_->size().y().text(),
                                     FdTdLife::notifyDomainContentsChange);
        auto newsizeZ = c.testString(sizeZEdit_, shape_->size().z().text(),
                                     FdTdLife::notifyDomainContentsChange);
        auto newincrementX = c.testString(incrementXEdit_,
                                          shape_->increment().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementY = c.testString(incrementYEdit_,
                                          shape_->increment().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementZ = c.testString(incrementZEdit_,
                                          shape_->increment().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newrepeatX = c.testString(duplicateXEdit_,
                                       shape_->duplicate().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newrepeatY = c.testString(duplicateYEdit_,
                                       shape_->duplicate().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newrepeatZ = c.testString(duplicateZEdit_,
                                       shape_->duplicate().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().x().text(newcenterX);
            shape_->center().y().text(newcenterY);
            shape_->center().z().text(newcenterZ);
            shape_->size().x().text(newsizeX);
            shape_->size().y().text(newsizeY);
            shape_->size().z().text(newsizeZ);
            shape_->increment().x().text(newincrementX);
            shape_->increment().y().text(newincrementY);
            shape_->increment().z().text(newincrementZ);
            shape_->duplicate().x().text(newrepeatX);
            shape_->duplicate().y().text(newrepeatY);
            shape_->duplicate().z().text(newrepeatZ);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CuboidArrayDlg::tabChanged() {
    onChange();
}
