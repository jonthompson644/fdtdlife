/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ANALYSERSDLG_H
#define FDTDLIFE_ANALYSERSDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "FdTdLife.h"
#include "PushButtonMenu.h"
#include "DialogFactory.h"
#include "TabbedSubDlg.h"
#include <Fdtd/Notifiable.h>

class FdTdLife;
namespace sensor {class Analyser;}
namespace sensor {class Analysers;}

// Main tab panel for the analysers
class AnalysersDlg : public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    typedef TabbedSubDlg<AnalysersDlg, sensor::Analyser> SubDlg;
    explicit  AnalysersDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;
    void fillAnalysersTree(sensor::Analyser* sel=nullptr);
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onAnalyserSel();
    void onNewAnalyser(int item);
    void onDelete();
    void onCopy();
    void onPaste();
    void onAnalyserChanged(QTreeWidgetItem* item, int column);

protected:
    enum {
        analyserColName=0, analyserColType, numAnalyserCols
    };
    QTreeWidget* analysersTree_ {};
    QVBoxLayout* analyserDlgLayout_ {};
    SubDlg* analyserDlg_ {};
    PushButtonMenu* newButton_ {};
    QPushButton* deleteButton_ {};
    QPushButton* copyButton_ {};
    QPushButton* pasteButton_ {};
    DialogFactory<SubDlg> dlgFactory_;

protected:
    sensor::Analyser* currentAnalyser();
    void showAnalyserDlg();
};


#endif //FDTDLIFE_ANALYSERSDLG_H
