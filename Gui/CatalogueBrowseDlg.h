/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUEBROWSEDLG_H
#define FDTDLIFE_CATALOGUEBROWSEDLG_H

#include "QtIncludes.h"
#include "InitialisingGui.h"
#include <QPrinter>
#include <memory>
#include <map>

namespace fdtd { class CatalogueItem; }
namespace fdtd { class Catalogue; }
class BinaryCodedPlateWidget;

class DataSeriesWidget;

class GuiConfiguration;

class CatalogueBuilderDlg;

class CatalogueBrowseDlg : public QDialog {
Q_OBJECT
public:
    static const int itemsPerPage_ = 5;
    explicit CatalogueBrowseDlg(QWidget* parent, CatalogueBuilderDlg* dlg);

protected slots:
    void onClose();
    void onGo();
    void onStop();
    void onFromPage(int page);

protected:
    CatalogueBuilderDlg* dlg_ {};
    QWidget* printArea_ {};
    QLabel* codeText_[itemsPerPage_] {};
    QLabel* patternText_[itemsPerPage_] {};
    BinaryCodedPlateWidget* bitPatternEdit_[itemsPerPage_] {};
    DataSeriesWidget* transmittanceView_[itemsPerPage_] {};
    DataSeriesWidget* phaseShiftView_[itemsPerPage_] {};
    QLabel* pageNumberText_ {};
    QSpinBox* fromPageSpin_ {};
    QSpinBox* toPageSpin_ {};
    QPushButton* goButton_ {};
    QPushButton* stopButton_ {};
    QPushButton* closeButton_ {};
    int totalPages_ {};
    bool printing_ {};
    bool closing_ {};

protected:
    void createPdf();
    void loadPage(int page);
};


#endif //FDTDLIFE_CATALOGUEITEMPRINTDLG_H
