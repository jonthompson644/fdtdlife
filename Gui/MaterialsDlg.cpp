/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "MaterialsDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "DefaultMaterialDlg.h"
#include "CpmlMaterialDlg.h"
#include "ArcMaterialDlg.h"
#include "NoEditDelegate.h"
#include <Domain/DefaultMaterial.h>
#include <Domain/CpmlMaterial.h>
#include <Domain/ArcMaterial.h>
#include <Box/Constants.h>

// Constructor
MaterialsDlg::MaterialsDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        materialDlg_(nullptr) {
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<MaterialDlg>::Creator<MaterialDlg>(
            domain::Domain::materialModeNormal));
    dlgFactory_.define(new box::Factory<MaterialDlg>::Creator<ArcMaterialDlg>(
            domain::Domain::materialModeArc));
    dlgFactory_.define(new box::Factory<MaterialDlg>::Creator<CpmlMaterialDlg>(
            domain::Domain::materialModeCpmlBoundary));
    dlgFactory_.define(new box::Factory<MaterialDlg>::Creator<DefaultMaterialDlg>(
            domain::Domain::materialModeBackground));
    // The parameters
    auto paramLayout = new QVBoxLayout;
    auto materialsLayout = new QHBoxLayout;
    // The materials tree
    auto treeLayout = new QVBoxLayout;
    materialsLayout->addLayout(treeLayout);
    materialsTree_ = new QTreeWidget;
    materialsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    materialsTree_->setColumnCount(numMaterialCols);
    materialsTree_->setHeaderLabels({"Identifier", "Name", "Permittivity"});
    materialsTree_->setFixedWidth(340);
    materialsTree_->setItemDelegateForColumn(
            materialColIdentifier, new NoEditDelegate(this));
    treeLayout->addWidget(materialsTree_);
    // The materials buttons
    auto materialsButtons = new QHBoxLayout;
    addButton_ = DialogHelper::buttonMenuItem(materialsButtons, "New Material",
                                              std::list<std::string>{"Normal", "ARC"});
    deleteButton_ = DialogHelper::buttonItem(materialsButtons, "Delete");
    materialsButtons->addStretch();
    treeLayout->addLayout(materialsButtons);
    // The place holder for the material dialog
    materialDlgLayout_ = new QHBoxLayout;
    materialsLayout->addLayout(materialDlgLayout_);
    // The materials assembly
    materialsLayout->addStretch();
    paramLayout->addLayout(materialsLayout);
    // Set the layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(addButton_, SIGNAL(clickedMenu(int)), SLOT(onNew(int)));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(materialsTree_, SIGNAL(itemSelectionChanged()), SLOT(onMaterialSelected()));
    connect(materialsTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onMaterialChanged(QTreeWidgetItem * , int)));
    // Intialise the dialog
    fillMaterialTree();
}

// Fill the materials tree
void MaterialsDlg::fillMaterialTree(domain::Material* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        materialsTree_->clear();
        for(auto& material : m_->d()->materials()) {
            auto item = new QTreeWidgetItem(materialsTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
            setMaterialItem(material.get(), item);
            if(selItem == nullptr || material.get() == sel) {
                selItem = item;
            }
        }
    }
    if(selItem != nullptr) {
        materialsTree_->setCurrentItem(selItem);
    }
}

// Set an item in the tree
void MaterialsDlg::setMaterialItem(domain::Material* mat, QTreeWidgetItem* item) {
    item->setText(materialColIdentifier, QString::number(mat->index()));
    item->setText(materialColName, mat->name().c_str());
    item->setText(materialColPermittivity,
                  QString::number(mat->epsilon().value() / box::Constants::epsilon0_));
}

// An entry in the tree has been changed
void MaterialsDlg::onMaterialChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        // Get the material
        int identifier = item->text(materialColIdentifier).toInt();
        domain::Material* mat = m_->d()->getMaterial(identifier);
        if(mat != nullptr) {
            switch(column) {
                case materialColIdentifier:
                    // Identifier not changeable
                    break;
                case materialColName:
                    // Change the name
                    mat->name(item->text(materialColName).toStdString());
                    break;
                case materialColPermittivity: {
                    // The relative permittivity has changed
                    double relativeEpsilon =
                            item->text(materialColPermittivity).toDouble();
                    mat->epsilon(box::Expression(relativeEpsilon * box::Constants::epsilon0_));
                    m_->d()->initialiseMaterials();
                    break;
                }
                default:
                    break;
            }
            fillMaterialTree(mat);
        }
    }
}

// Initialise the dialog
void MaterialsDlg::initialise() {
    // Get the material
    domain::Material* mat = nullptr;
    QTreeWidgetItem* item = materialsTree_->currentItem();
    if(item != nullptr) {
        int identifier = item->text(materialColIdentifier).toInt();
        mat = m_->d()->getMaterial(identifier);
    }
    // Fill the material list
    fillMaterialTree(mat);
}

// Add a new material
void MaterialsDlg::onNew(int addKind) {
    tabChanged();  // Update anything that may have changed for the current item
    domain::Material* mat = nullptr;
    switch(addKind) {
        case addNormal:
            mat = m_->d()->makeMaterial(domain::Domain::materialModeNormal).get();
            break;
        case addArc:
            mat = m_->d()->makeMaterial(domain::Domain::materialModeArc).get();
            break;
        default:
            break;
    }
    if(mat != nullptr) {
        QString name;
        QTextStream n(&name);
        n << "Material " << mat->index();
        mat->name(name.toStdString());
        mat->epsilon(box::Expression(box::Constants::epsilon0_));
        mat->mu(box::Expression(box::Constants::mu0_));
        mat->sigma(box::Expression(box::Constants::sigma0_));
        mat->sigmastar(box::Expression(box::Constants::sigmastar0_));
        mat->color(box::Constants::colourBlack);
        mat->priority(0);
        fillMaterialTree(mat);
        InitialisingGui::Set s(initialising_);
        m_->doNotification(FdTdLife::notifyDomainContentsChange);
    }
}

// Delete a material
void MaterialsDlg::onDelete() {
    // Get the material
    int identifier = materialsTree_->currentItem()->text(
            materialColIdentifier).toInt();
    domain::Material* mat = m_->d()->getMaterial(identifier);
    if(mat != nullptr) {
        m_->d()->remove(mat);
        fillMaterialTree(nullptr);
        InitialisingGui::Set s(initialising_);
        m_->doNotification(FdTdLife::notifyDomainContentsChange);
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void MaterialsDlg::tabChanged() {
    if(materialDlg_ != nullptr) {
        materialDlg_->tabChanged();
    }
}

// A material has been selected
void MaterialsDlg::onMaterialSelected() {
    if(!initialising_) {
        // Get the material
        int identifier = materialsTree_->currentItem()->text(
                materialColIdentifier).toInt();
        domain::Material* mat = m_->d()->getMaterial(identifier);
        // Delete any old dialog
        delete materialDlg_;
        materialDlg_ = nullptr;
        // Make dialog for the material
        materialDlg_ = dlgFactory_.make<MaterialsDlg, domain::Material>(this, mat);
        if(materialDlg_ != nullptr) {
            materialDlgLayout_->addWidget(materialDlg_);
            materialDlg_->initialise();
        }
    }
}

// The currently selected shape has changed and the tree needs updating
void MaterialsDlg::updateCurrentSelection() {
    auto* item = materialsTree_->currentItem();
    if(item != nullptr) {
        int identifier = item->text(materialColIdentifier).toInt();
        domain::Material* mat = m_->d()->getMaterial(identifier);
        if(mat != nullptr) {
            setMaterialItem(mat, item);
        }
    }
}

// Handle change notifications
void MaterialsDlg::notify(fdtd::Notifier* source, int why,
                          fdtd::NotificationData* /*data*/) {
    if(source == model()) {
        switch(why) {
            case FdTdLife::notifyMaterialChange:
            case FdTdLife::notifyDomainContentsChange:
                fillMaterialTree(currentMaterial());
                break;
            default:
                break;
        }
    }
}

// Return the currently selected material
domain::Material* MaterialsDlg::currentMaterial() {
    domain::Material* result = nullptr;
    auto* item = materialsTree_->currentItem();
    if(item != nullptr) {
        int identifier = item->text(materialColIdentifier).toInt();
        result = m_->d()->getMaterial(identifier);
    }
    return result;
}
