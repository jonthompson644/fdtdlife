/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_MATERIALDLG_H
#define FDTDLIFE_MATERIALDLG_H

#include "QtIncludes.h"
class MaterialsDlg;
namespace domain {class Material;}

// Base class for the material sub-dialogs
class MaterialDlg : public QWidget {
    Q_OBJECT

public:
    MaterialDlg() = default;
    virtual void construct(MaterialsDlg* dlg, domain::Material* mat);
    virtual void tabChanged();
    virtual void initialise();

protected slots:
    void onRelativeEpsilonChange();
    void onRelativeMuChange();
    virtual void onChange();

protected:
    domain::Material* mat_ {};
    MaterialsDlg* dlg_ {};
    QGroupBox* materialGroup_ {};
    QVBoxLayout* materialLayout_ {};
    QLineEdit* nameEdit_ {};
    QLineEdit* indexEdit_ {};
    QComboBox* colorList_ {};
    QLineEdit* priorityEdit_ {};
    QLineEdit* epsilonEdit_ {};
    QLineEdit* relativeEpsilonEdit_ {};
    QLineEdit* muEdit_ {};
    QLineEdit* relativeMuEdit_ {};
    QLineEdit* sigmaEdit_ {};
    QLineEdit* sigmastarEdit_ {};
};

#endif //FDTDLIFE_MATERIALDLG_H
