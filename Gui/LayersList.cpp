/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LayersList.h"
#include <Gs/BinaryPlateNxNGene.h>
#include <Gs/SquarePlateGene.h>
#include <Gs/AdmittanceCatLayerGene.h>
#include <Gs/DuplicateLayerGene.h>
#include <Gs/AdmittanceFnLayerGene.h>
#include <Gs/DielectricLayerGene.h>
#include <Fdtd/Model.h>
#include <Gs/GeneticSearch.h>
#include <Fdtd/AdmittanceCatalogue.h>

// Constructor
LayersList::LayersList(gs::GeneticSearch* s) :
        s_(s) {
    setSelectionMode(QAbstractItemView::SingleSelection);
    setFlow(QListView::LeftToRight);
    setIconSize(QSize(iconSize_, iconSize_));
    setFixedHeight(iconSize_ + labelHeight_);
    setContextMenuPolicy(Qt::CustomContextMenu);
    setViewMode(QListWidget::IconMode);
    setFlow(QListWidget::LeftToRight);
    setWrapping(false);
}

// Destructor
LayersList::~LayersList() {
    // Delete any icons
    for(auto i : icons_) {
        delete i;
    }
    icons_.clear();
}

// Returns an icon for the binary plate allele
void LayersList::drawBitPatternIcon(QPainter& painter, size_t repeat, size_t bitsPerHalfSide,
                                    const std::vector<uint64_t>& pattern2Fold) {
    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);
    painter.drawRect(0, 0, iconSize_ - 1, iconSize_ - 1);
    painter.setBrush(Qt::black);
    double dotSize = static_cast<double>(iconSize_) / 2.0 / bitsPerHalfSide / repeat;
    double cellSize = dotSize * bitsPerHalfSide * 2.0;
    for(size_t xr = 0; xr < repeat; xr++) {
        for(size_t yr = 0; yr < repeat; yr++) {
            double x1a = xr * cellSize;
            double y1a = yr * cellSize;
            double x1b = x1a + dotSize;
            double y1b = y1a + dotSize;
            double x2a = x1a + cellSize - dotSize;
            double y2a = y1a + cellSize - dotSize;
            double x2b = x2a + dotSize;
            double y2b = y2a + dotSize;
            for(size_t m = 0; m < bitsPerHalfSide; m++) {
                for(size_t n = 0; n < bitsPerHalfSide; n++) {
                    size_t bitNum = m * bitsPerHalfSide + n;
                    size_t wordNum = bitNum / bitsPerWord_;
                    size_t bitInWord = bitNum % bitsPerWord_;
                    if((pattern2Fold[wordNum] & (1ULL << bitInWord)) != 0) {
                        painter.drawRect(static_cast<int>(std::floor(x1a)),
                                         static_cast<int>(std::floor(y1a)),
                                         static_cast<int>(std::floor(x1b - x1a)),
                                         static_cast<int>(std::floor(y1b - y1a)));
                        painter.drawRect(static_cast<int>(std::floor(x2a)),
                                         static_cast<int>(std::floor(y2a)),
                                         static_cast<int>(std::floor(x2b - x2a)),
                                         static_cast<int>(std::floor(y2b - y2a)));
                        painter.drawRect(static_cast<int>(std::floor(x2a)),
                                         static_cast<int>(std::floor(y1a)),
                                         static_cast<int>(std::floor(x2b - x2a)),
                                         static_cast<int>(std::floor(y1b - y1a)));
                        painter.drawRect(static_cast<int>(std::floor(x1a)),
                                         static_cast<int>(std::floor(y2a)),
                                         static_cast<int>(std::floor(x1b - x1a)),
                                         static_cast<int>(std::floor(y2b - y2a)));
                    }
                    x1a += dotSize;
                    x2a -= dotSize;
                    x1b += dotSize;
                    x2b -= dotSize;
                }
                y1a += dotSize;
                y2a -= dotSize;
                y1b += dotSize;
                y2b -= dotSize;
                x1a = (double) xr * cellSize;
                x2a = x1a + cellSize - dotSize;
                x1b = x1a + dotSize;
                x2b = x2a + dotSize;
            }
        }
    }
}

// Make a new layer widget item for an NxN binary plate
QListWidgetItem* LayersList::makeLayer(gs::BinaryPlateNxNGene* a) {
    QPixmap pixmap(iconSize_, iconSize_);
    QPainter painter(&pixmap);
    painter.save();
    drawBitPatternIcon(painter, a->repeat(), a->bitsPerHalfSide(), a->pattern2Fold());
    icons_.push_back(new QIcon(pixmap));
    painter.restore();
    auto text = QString("%1: |%2um| x%3").arg(icons_.size()).
            arg(a->thickness() * 1000000, 0, 'f', 2).
            arg(a->repeat());
    return new QListWidgetItem(*icons_.back(), text);
}

// Make a new layer widget item for an admittance catalogue layer
QListWidgetItem* LayersList::makeLayer(gs::AdmittanceCatLayerGene* a) {
    QPixmap pixmap(iconSize_, iconSize_);
    QPainter painter(&pixmap);
    painter.save();
    drawBitPatternIcon(painter, a->repeat(), s_->m()->admittanceCat().bitsPerHalfSide(),
                       {a->pattern(s_->m())});
    icons_.push_back(new QIcon(pixmap));
    painter.restore();
    auto text = QString("%1: |%2um| {%3} x%5").arg(icons_.size()).
            arg(a->thickness() * 1000000, 0, 'f', 2).
            arg(a->coding()).
            arg(a->repeat());
    return new QListWidgetItem(*icons_.back(), text);
}

// Make a new layer widget for a square plate layer
QListWidgetItem* LayersList::makeLayer(gs::SquarePlateGene* a) {
    QPixmap pixmap(iconSize_, iconSize_);
    QPainter painter(&pixmap);
    painter.save();
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::white);
    painter.drawRect(0, 0, iconSize_, iconSize_);
    int cellSize = (iconSize_ - 2) / static_cast<int>(a->repeat());
    painter.setPen(Qt::NoPen);
    for(size_t x = 0; x < a->repeat(); x++) {
        for(size_t y = 0; y < a->repeat(); y++) {
            if(a->hole()) {
                painter.setBrush(Qt::black);
            } else {
                painter.setBrush(Qt::white);
            }
            painter.drawRect(static_cast<int>(x) * cellSize + 1,
                             static_cast<int>(y) * cellSize + 1,
                             cellSize, cellSize);
            if(a->hole()) {
                painter.setBrush(Qt::white);
            } else {
                painter.setBrush(Qt::black);
            }
            auto size = static_cast<int>(std::round(static_cast<double>(cellSize) * a->plateSize() / 100.0));
            int margin = (cellSize - size) / 2;
            painter.drawRect(static_cast<int>(x) * cellSize + margin + 1,
                             static_cast<int>(y) * cellSize + margin + 1,
                             size, size);
        }
    }
    icons_.push_back(new QIcon(pixmap));
    painter.restore();
    auto text = QString("%1: |%2um| %3% x%4").arg(icons_.size()).
            arg(a->thickness() * 1000000, 0, 'f', 2).
            arg(a->plateSize(), 0, 'f', 0).
            arg(a->repeat());
    return new QListWidgetItem(*icons_.back(), text);
}

// Make a new layer widget for a dielectric layer
QListWidgetItem* LayersList::makeLayer(gs::DielectricLayerGene* a) {
    QPixmap pixmap(iconSize_, iconSize_);
    QPainter painter(&pixmap);
    painter.save();
    icons_.push_back(new QIcon(pixmap));
    painter.restore();
    auto text = QString("%1: |%2um| %3").arg(icons_.size()).
            arg(a->thickness() * 1000000, 0, 'f', 2).
            arg(a->refractiveIndex(), 3, 'f', 1);
    return new QListWidgetItem(*icons_.back(), text);
}

// Make a new layer widget for an admittance function layer
QListWidgetItem* LayersList::makeLayer(gs::AdmittanceFnLayerGene* a) {
    // Calculate the factors
    double freqPerPixel = s_->frequencyRange() / (iconSize_ - 2);
    double transmittancePerPixel = 1.0 / (iconSize_ - 2);
    // Draw icon
    QPixmap pixmap(iconSize_, iconSize_);
    QPainter painter(&pixmap);
    painter.save();
    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);
    painter.drawRect(0, 0, iconSize_ - 1, iconSize_ - 1);
    painter.setBrush(Qt::black);
    bool first = true;
    int prevX = 0;
    int prevY = 0;
    for(int x = 1; x < iconSize_ - 1; x++) {
        double freq = freqPerPixel * (x - 1) + s_->frequencySpacing();
        std::complex<double> admittance;
        admittance.real(a->real().value(freq));
        admittance.imag(a->imag().value(freq));
        double tanPhi = admittance.imag() / (admittance.real() + 1.0);
        double transmittance = tanPhi / admittance.imag() / sqrt(1.0 + tanPhi * tanPhi);
        int y = static_cast<int>(round((1.0 - transmittance) / transmittancePerPixel)) + 1;
        if(!first) {
            painter.drawLine(prevX, prevY, x, y);
        }
        prevX = x;
        prevY = y;
        first = false;
    }
    icons_.push_back(new QIcon(pixmap));
    painter.restore();
    // Make list item
    auto text = QString("%1: |%2um|").arg(icons_.size()).
            arg(a->thickness() * 1000000, 0, 'f', 2);
    return new QListWidgetItem(*icons_.back(), text);
}

// Insert a layer into the list.  Returns true if
// a layer would have been inserted if doInsert was true.
bool LayersList::insertLayer(gs::Gene* gene, bool doInsert) {
    bool result = false;
    auto* layerGene = dynamic_cast<gs::LayerGene*>(gene);
    if(layerGene != nullptr) {
        if(layerGene->present()) {
            switch(layerGene->geneType()) {
                case gs::GeneTemplate::geneTypeSquarePlate:
                    if(doInsert) {
                        addItem(makeLayer(dynamic_cast<gs::SquarePlateGene*>(layerGene)));
                    }
                    result = true;
                    break;
                case gs::GeneTemplate::geneTypeDielectricLayer:
                    if(doInsert) {
                        addItem(makeLayer(dynamic_cast<gs::DielectricLayerGene*>(layerGene)));
                    }
                    result = true;
                    break;
                case gs::GeneTemplate::geneTypeAdmittanceCatLayer:
                    if(doInsert) {
                        addItem(makeLayer(dynamic_cast<gs::AdmittanceCatLayerGene*>(layerGene)));
                    }
                    result = true;
                    break;
                case gs::GeneTemplate::geneTypeBinaryPlateNxN:
                    if(doInsert) {
                        addItem(makeLayer(dynamic_cast<gs::BinaryPlateNxNGene*>(layerGene)));
                    }
                    result = true;
                    break;
                case gs::GeneTemplate::geneTypeAdmittanceFn:
                    if(doInsert) {
                        addItem(makeLayer(dynamic_cast<gs::AdmittanceFnLayerGene*>(layerGene)));
                    }
                    result = true;
                    break;
                case gs::GeneTemplate::geneTypeDuplicateLayer: {
                    auto* duplicateLayer = dynamic_cast<gs::DuplicateLayerGene*>(layerGene);
                    if(duplicateLayer->duplicateThisLayer() != nullptr) {
                        result = insertLayer(duplicateLayer->duplicateThisLayer());
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }
    return result;
}

// Clear the list ready for re-filling
void LayersList::clearLayers() {
    // Delete any icons
    for(auto i : icons_) {
        delete i;
    }
    icons_.clear();
    // The base class
    QListWidget::clear();
}

// Copy a layer pixmap to the clipboard
void LayersList::copyToClipboard(int layer) {
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setPixmap(icons_[static_cast<size_t>(layer)]->
            pixmap(QSize(iconSize_, iconSize_)));
}
