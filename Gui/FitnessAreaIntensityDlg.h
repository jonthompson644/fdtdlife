/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSAREAINTENSITYDLG_H
#define FDTDLIFE_FITNESSAREAINTENSITYDLG_H

#include "QtIncludes.h"
#include "TabbedSubDlg.h"
namespace gs {class FitnessAreaIntensity;}
namespace gs {class FitnessBase;}
class GeneticSearchTargetDlg;

// Sub-dialog for the fitness functions that implement area under a curve
class FitnessAreaIntensityDlg : public TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase> {
Q_OBJECT
public:
    void construct(GeneticSearchTargetDlg* dlg, gs::FitnessBase* fitnessFunction) override;
    void tabChanged() override;
    void initialise() override;

protected slots:
    void onChange();

protected:
    QLineEdit* fitnessFunctionWeightEdit_ {};
    QLineEdit* frequencyEdit_ {};
    QLineEdit* centerXEdit_ {};
    QLineEdit* centerYEdit_ {};
    QLineEdit* centerZEdit_ {};
    QLineEdit* widthEdit_ {};
    QLineEdit* heightEdit_ {};
    QComboBox* orientationList_ {};
    QComboBox* dataSourceList_ {};
    gs::FitnessAreaIntensity* fitnessFunction_ {};
};


#endif //FDTDLIFE_FITNESSAREAINTENSITYDLG_H
