/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "TransmittanceAnalyserDlg.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void TransmittanceAnalyserDlg::construct(AnalysersDlg* dlg, sensor::Analyser* d) {
    fdtd::Notifiable::construct(dlg->model());
    SpecTableAnalyserDlg::construct(dlg, d);
    analyser_ = dynamic_cast<sensor::TransmittanceAnalyser*>(d);
    // Create the controls
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    sensorList_ = DialogHelper::dropListItem(line2Layout, "Sensor");
    locationList_ = DialogHelper::dropListItem(line2Layout, "Location",
                                               {"Smallest Z", "Largest Z"});
    componentList_ = DialogHelper::dropListItem(line2Layout, "Component",
                                                {"X", "Y"});
    line2Layout->addStretch();
    SpecTableAnalyserDlg::createControls();
    errorEdit_ = DialogHelper::doubleItem(layout_, "Error", true);
    dataView_ = new DataSeriesWidget(dlg->model()->guiConfiguration());
    layout_->addWidget(dataView_);
    // Connect the handlers
    connect(sensorList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(locationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(componentList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    // Initialise the dialog
    initialise();
}

// Initialise the GUI parameters
void TransmittanceAnalyserDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    // Set parameters
    errorEdit_->setText(QString::number(analyser_->error()));
    locationList_->setCurrentIndex(static_cast<int>(analyser_->location()));
    componentList_->setCurrentIndex(static_cast<int>(analyser_->component()));
    dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    fillFitnessPointsTree();
    showData();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void TransmittanceAnalyserDlg::tabChanged() {
    onChange();
}

// An edit box has changed
void TransmittanceAnalyserDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newLocation = static_cast<sensor::Analyser::SensorLocation>(
                c.testInt(locationList_, static_cast<int>(analyser_->location()),
                          FdTdLife::notifyMinorChange));
        auto newComponent = static_cast<sensor::Analyser::SensorComponent>(
                c.testInt(componentList_,
                          static_cast<int>(analyser_->component()),
                          FdTdLife::notifyMinorChange));
        int newSensorId = dlg_->model()->selected(sensorList_);
        c.externalTest(newSensorId != analyser_->sensorId(),
                       FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            analyser_->sensorId(newSensorId);
            analyser_->location(newLocation);
            analyser_->component(newComponent);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// Handle a notification
void TransmittanceAnalyserDlg::notify(fdtd::Notifier* source, int why,
                                      fdtd::NotificationData* /*data*/) {
    if(source == dlg_->model() && why == fdtd::Model::notifySensorChange) {
        InitialisingGui::Set s(initialising_);
        dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    } else if(source == dlg_->model() && why == fdtd::Model::notifyAnalysersCalculate) {
        errorEdit_->setText(QString::number(analyser_->error()));
        showData();
    }
}

// Show the data
void TransmittanceAnalyserDlg::showData() {
    std::vector<double> min;
    std::vector<double> max;
    min.resize(analyser_->reference().size());
    max.resize(analyser_->reference().size());
    for(size_t i=0; i<analyser_->reference().size(); i++) {
        min[i] = analyser_->reference()[i] - analyser_->delta()[i];
        max[i] = analyser_->reference()[i] + analyser_->delta()[i];
    }
    dataView_->setData(analyser_->data(), GuiConfiguration::elementDataA,
                       true);
    dataView_->setData(min, GuiConfiguration::elementDataB,
                       false);
    dataView_->setData(max, GuiConfiguration::elementDataC,
                       false);
    dataView_->setAxisInfo(
            analyser_->minX() / box::Constants::giga_,
            analyser_->stepX() / box::Constants::giga_, "GHz",
            analyser_->minY(), analyser_->maxY(), "");
}
