/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceCatLayerGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "ComboBoxDelegate.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>
#include <Fdtd/AdmittanceCatalogue.h>
#include <Fdtd/CatalogueItem.h>

// Constructor
AdmittanceCatLayerGeneTemplateDlg::AdmittanceCatLayerGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                                     gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    std::tie(repeatBitsEdit_, repeatMinEdit_) =
            DialogHelper::geneBitsSpecIntMinItem(layout_, "Repeat Count:");
    catBitsEdit_ = DialogHelper::intItem(layout_, "Coding bits");
    std::tie(gFactorBitsEdit_, gFactorMinEdit_, gFactorMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "G Factor");
    connect(repeatBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(catBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gFactorMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gFactorMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gFactorBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void AdmittanceCatLayerGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    repeatBitsEdit_->setText(QString::number(geneTemplate_->repeatBits()));
    repeatMinEdit_->setText(QString::number(geneTemplate_->repeatMin()));
    catBitsEdit_->setText(QString::number(geneTemplate_->catBits()));
    gFactorBitsEdit_->setText(QString::number(geneTemplate_->gFactor().bits()));
    gFactorMinEdit_->setText(QString::number(geneTemplate_->gFactor().min()));
    gFactorMaxEdit_->setText(QString::number(geneTemplate_->gFactor().max()));
}

// An edit box has changed
void AdmittanceCatLayerGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newRepeatBits = c.testSizeT(repeatBitsEdit_, geneTemplate_->repeatBits(), FdTdLife::notifySequencerChange);
        auto newRepeatMin = c.testInt(repeatMinEdit_, geneTemplate_->repeatMin(), FdTdLife::notifySequencerChange);
        auto newCatBits = c.testSizeT(catBitsEdit_, geneTemplate_->catBits(), FdTdLife::notifySequencerChange);
        auto newGFactor = c.testGeneBitsSpecDouble(gFactorBitsEdit_, gFactorMinEdit_, gFactorMaxEdit_,
                                                   geneTemplate_->gFactor(), FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->set(geneTemplate_->geneType(),
                               geneTemplate_->fourFoldSymmetry(),
                               geneTemplate_->bitsPerHalfSide(),
                               newRepeatBits, newRepeatMin, newCatBits);
            geneTemplate_->gFactor(newGFactor);
            dlg_->model()->doNotification(c.why());
        }
    }
}
