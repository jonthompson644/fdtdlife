/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "MaterialDlg.h"
#include "MaterialsDlg.h"
#include <Domain/Material.h>
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Box/Constants.h>

// Second stage constructor used by factory
void MaterialDlg::construct(MaterialsDlg* dlg, domain::Material* mat) {
    mat_ = mat;
    dlg_ = dlg;
    auto paramsLayout = new QVBoxLayout;
    materialLayout_ = new QVBoxLayout;
    auto line1Layout = new QHBoxLayout;
    indexEdit_ = DialogHelper::intItem(line1Layout, "Identifier", true);
    colorList_ = DialogHelper::dropListItem(line1Layout, "Display Color",
                                            box::Constants::colourNames_);
    priorityEdit_ = DialogHelper::intItem(line1Layout, "Priority");
    materialLayout_->addLayout(line1Layout);
    nameEdit_ = DialogHelper::textItem(
            materialLayout_, "Name",
            mat->index() < domain::Domain::firstUserMaterial);
    auto epsilonLayout = new QHBoxLayout;
    epsilonEdit_ = DialogHelper::textItem(
            epsilonLayout, "Epsilon",
            mat->index() < domain::Domain::firstUserMaterial);
    relativeEpsilonEdit_ = DialogHelper::doubleItem(
            epsilonLayout, "Relative",
            mat->index() < domain::Domain::firstUserMaterial);
    materialLayout_->addLayout(epsilonLayout);
    auto muLayout = new QHBoxLayout;
    muEdit_ = DialogHelper::textItem(
            muLayout, "Mu",
            mat->index() < domain::Domain::firstUserMaterial);
    relativeMuEdit_ = DialogHelper::doubleItem(
            muLayout, "Relative",
            mat->index() < domain::Domain::firstUserMaterial);
    materialLayout_->addLayout(muLayout);
    auto sigmaLayout = new QHBoxLayout;
    sigmaEdit_ = DialogHelper::textItem(
            sigmaLayout, "Sigma",
            mat->index() < domain::Domain::firstUserMaterial);
    sigmastarEdit_ = DialogHelper::textItem(
            sigmaLayout, "Sigma*",
            mat->index() < domain::Domain::firstUserMaterial);
    materialLayout_->addLayout(sigmaLayout);
    // The group box for the specific material elements
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(materialLayout_);
    stretchLayout->addStretch();
    materialGroup_ = new QGroupBox;
    materialGroup_->setLayout(stretchLayout);
    paramsLayout->addWidget(materialGroup_);
    // The dialog layout
    setLayout(paramsLayout);
    // Connect handlers
    connect(nameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(epsilonEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(relativeEpsilonEdit_, SIGNAL(editingFinished()),
            SLOT(onRelativeEpsilonChange()));
    connect(muEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(relativeMuEdit_, SIGNAL(editingFinished()), SLOT(onRelativeMuChange()));
    connect(sigmaEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sigmastarEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(priorityEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(colorList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void MaterialDlg::tabChanged() {
    onChange();
}

// The relative permittivity has changed
void MaterialDlg::onRelativeEpsilonChange() {
    double epsilon = relativeEpsilonEdit_->text().toDouble() * box::Constants::epsilon0_;
    epsilonEdit_->setText(QString::number(epsilon));
    onChange();
}

// The relative permeability has changed
void MaterialDlg::onRelativeMuChange() {
    double mu = relativeMuEdit_->text().toDouble() * box::Constants::mu0_;
    muEdit_->setText(QString::number(mu));
    onChange();
}

// A material edit field has changed
void MaterialDlg::onChange() {
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newColor = (box::Constants::Colour) c.testInt(
                colorList_, mat_->color(),
                FdTdLife::notifyDomainContentsChange);
        auto newName = c.testString(nameEdit_, mat_->name(),
                                    FdTdLife::notifyDomainContentsChange);
        auto newEpsilon = c.testString(epsilonEdit_, mat_->epsilon().text(),
                                       FdTdLife::notifyMinorChange);
        auto newMu = c.testString(muEdit_, mat_->mu().text(),
                                  FdTdLife::notifyMinorChange);
        auto newSigma = c.testString(sigmaEdit_, mat_->sigma().text(),
                                     FdTdLife::notifyMinorChange);
        auto newSigmaStar = c.testString(sigmastarEdit_, mat_->sigmastar().text(),
                                         FdTdLife::notifyMinorChange);
        auto newPriority = c.testInt(priorityEdit_, mat_->priority(),
                                     FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.why()) {
            InitialisingGui::Set s(dlg_->initialising());
            mat_->name(newName);
            mat_->epsilon(box::Expression(newEpsilon));
            mat_->mu(box::Expression(newMu));
            mat_->sigma(box::Expression(newSigma));
            mat_->sigmastar(box::Expression(newSigmaStar));
            mat_->color(newColor);
            mat_->priority(newPriority);
            dlg_->updateCurrentSelection();
            dlg_->model()->doNotification(c.why());
            mat_->evaluate();
            relativeEpsilonEdit_->setText(
                    QString::number(mat_->epsilon().value() / box::Constants::epsilon0_));
            relativeMuEdit_->setText(
                    QString::number(mat_->mu().value() / box::Constants::mu0_));
            epsilonEdit_->setToolTip(QString::number(mat_->epsilon().value()));
            muEdit_->setToolTip(QString::number(mat_->mu().value()));
            sigmaEdit_->setToolTip(QString::number(mat_->sigma().value()));
            sigmastarEdit_->setToolTip(QString::number(mat_->sigmastar().value()));
            mat_->initialise();
        }
    }
}

// Update the user interface
void MaterialDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    mat_->evaluate();
    nameEdit_->setText(mat_->name().c_str());
    indexEdit_->setText(QString::number(mat_->index()));
    colorList_->setCurrentIndex(mat_->color());
    priorityEdit_->setText(QString::number(mat_->priority()));
    epsilonEdit_->setText(mat_->epsilon().text().c_str());
    epsilonEdit_->setToolTip(QString::number(mat_->epsilon().value()));
    relativeEpsilonEdit_->setText(
            QString::number(mat_->epsilon().value() / box::Constants::epsilon0_));
    muEdit_->setText(mat_->mu().text().c_str());
    muEdit_->setToolTip(QString::number(mat_->mu().value()));
    relativeMuEdit_->setText(QString::number(mat_->mu().value() / box::Constants::mu0_));
    sigmaEdit_->setText(mat_->sigma().text().c_str());
    sigmaEdit_->setToolTip(QString::number(mat_->sigma().value()));
    sigmastarEdit_->setText(mat_->sigmastar().text().c_str());
    sigmastarEdit_->setToolTip(QString::number(mat_->sigmastar().value()));
}
