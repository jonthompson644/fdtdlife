/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CpmlMaterialDlg.h"
#include <Domain/CpmlMaterial.h>
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "MaterialsDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void CpmlMaterialDlg::construct(MaterialsDlg* dlg, domain::Material* mat) {
    MaterialDlg::construct(dlg, mat);
    mat_ = dynamic_cast<domain::CpmlMaterial*>(mat);
    nameEdit_->setReadOnly(true);
    epsilonEdit_->setReadOnly(true);
    relativeEpsilonEdit_->setReadOnly(true);
    muEdit_->setReadOnly(true);
    relativeMuEdit_->setReadOnly(true);
    sigmaEdit_->setReadOnly(true);
    sigmastarEdit_->setReadOnly(true);
    auto line1Layout = new QHBoxLayout;
    sigmaCoeffEdit_ = DialogHelper::doubleItem(line1Layout, "Sigma Coeff");
    kappaMaxEdit_ = DialogHelper::doubleItem(line1Layout, "Kappa Max");
    materialLayout_->addLayout(line1Layout);
    auto line2Layout = new QHBoxLayout;
    aMaxEdit_ = DialogHelper::doubleItem(line2Layout, "A Coeff Max");
    mskEdit_ = DialogHelper::doubleItem(line2Layout, "Sigma/Kappa M");
    maEdit_ = DialogHelper::doubleItem(line2Layout, "A Coeff M");
    materialLayout_->addLayout(line2Layout);
    connect(sigmaCoeffEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(kappaMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(aMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(mskEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(maEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// A material edit field has changed
void CpmlMaterialDlg::onChange() {
    MaterialDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newSigmaCoeff = c.testDouble(sigmaCoeffEdit_, mat_->sigmaCoeff(), FdTdLife::notifyMinorChange);
        auto newKappaMax = c.testDouble(kappaMaxEdit_, mat_->kappaMax(), FdTdLife::notifyMinorChange);
        auto newAMax = c.testDouble(aMaxEdit_, mat_->aMax(), FdTdLife::notifyMinorChange);
        auto newMsk = c.testDouble(mskEdit_, mat_->msk(), FdTdLife::notifyMinorChange);
        auto newMa = c.testDouble(maEdit_, mat_->ma(), FdTdLife::notifyMinorChange);
        // Make the changes
        if (c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            mat_->sigmaCoeff(newSigmaCoeff);
            mat_->kappaMax(newKappaMax);
            mat_->aMax(newAMax);
            mat_->msk(newMsk);
            mat_->ma(newMa);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// Update the user interface
void CpmlMaterialDlg::initialise() {
    MaterialDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    sigmaCoeffEdit_->setText(QString::number(mat_->sigmaCoeff()));
    kappaMaxEdit_->setText(QString::number(mat_->kappaMax()));
    aMaxEdit_->setText(QString::number(mat_->aMax()));
    mskEdit_->setText(QString::number(mat_->msk()));
    maEdit_->setText(QString::number(mat_->ma()));
}
