/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SOURCESDLG_H
#define FDTDLIFE_SOURCESDLG_H


#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "PushButtonMenu.h"
#include <Notifiable.h>

class FdTdLife;
namespace source { class Source; }
class SourceDlg;

// The sources edit dialog
class SourcesDlg : public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    // Construction
    explicit SourcesDlg(FdTdLife* m);

    // API
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    source::Source* getCurrentSource();
    void updateCurrentSelection();

protected slots:
    // Handlers
    void onSourceSelected();
    void onNew(int sourceType);
    void onDelete();

protected:
    // Widgets
    enum {
        sourceColIdentifier = 0, sourceColName, numSourceCols
    };
    enum {
        newSourcePlaneWave = 0, newSourceZone, newSourceGravyWave
    };
    QTreeWidget* sourcesTree_;
    PushButtonMenu* addButton_;
    QPushButton* deleteButton_;
    QVBoxLayout* sourceDlgLayout_;
    SourceDlg* sourceDlg_;

protected:
    // Helpers
    void fillSourceTree(source::Source* sel = nullptr);
};


#endif //FDTDLIFE_SOURCESDLG_H
