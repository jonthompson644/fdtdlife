/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ConfigurationDlg.h"
#include "DialogHelper.h"
#include "GuiConfiguration.h"
#include "PlateAdmittanceEditDlg.h"
#include "GuiChangeDetector.h"

// Constructor
ConfigurationDlg::ConfigurationDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m) {
    updateTimer_ = new QTimer(this);
    updateTimer_->start(2000);
    const QStringList boundaryNames = {"Reflecting", "Periodic", "Absorbing"};
    const QStringList absorbingTypeNames = {"Simple Normally Matched",
                                            "Berenger Split Field", "CPML"};
    // The parameters
    auto paramLayout = new QVBoxLayout;
    // The configuration
    auto configurationGroup = new QGroupBox("Configuration");
    paramLayout->addWidget(configurationGroup);
    auto configurationLayout = new QVBoxLayout;
    configurationGroup->setLayout(configurationLayout);
    // Modelling mode
    auto modellingModeLayout = new QHBoxLayout;
    doFdtd_ = DialogHelper::boolItem(modellingModeLayout, "FDTD");
    doPropagationMatrices_ = DialogHelper::boolItem(modellingModeLayout,
                                                    "Propagation Matrices");
    sEdit_ = DialogHelper::doubleItem(modellingModeLayout, "Stability Factor");
    scatteredFieldZoneSizeEdit_ = DialogHelper::doubleItem(modellingModeLayout,
                                                           "Scattered Zone Size");
    configurationLayout->addLayout(modellingModeLayout);
    // Domain dimensions
    auto dimensionsLayout = new QHBoxLayout;
    auto dimensionsGrid = new QGridLayout;
    std::tie(x1Edit_, y1Edit_, z1Edit_) =
            DialogHelper::vectorItem(dimensionsGrid, 0,
                                     "Bottom Corner x,y,z (m)");
    std::tie(x2Edit_, y2Edit_, z2Edit_) =
            DialogHelper::vectorItem(dimensionsGrid, 1,
                                     "Top Corner x,y,z (m)");
    std::tie(drxEdit_, dryEdit_, drzEdit_) =
            DialogHelper::vectorItem(dimensionsGrid, 2,
                                     "Cell size x,y,z (m)");
    dimensionsLayout->addLayout(dimensionsGrid);
    configurationLayout->addLayout(dimensionsLayout);
    // Boundaries
    auto boundariesLayout1 = new QHBoxLayout;
    boundarySizeEdit_ = DialogHelper::intItem(boundariesLayout1, "Boundary Size");
    configurationLayout->addLayout(boundariesLayout1);
    auto boundariesLayout2 = new QHBoxLayout;
    boundaryXList_ = DialogHelper::dropListItem(boundariesLayout2, "X Boundaries",
                                                boundaryNames);
    boundaryYList_ = DialogHelper::dropListItem(boundariesLayout2, "Y Boundaries",
                                                boundaryNames);
    boundaryZList_ = DialogHelper::dropListItem(boundariesLayout2, "Z Boundaries",
                                                boundaryNames);
    configurationLayout->addLayout(boundariesLayout2);
    // Odds and ends
    auto otherLayout1 = new QHBoxLayout;
    timeSpanEdit_ = DialogHelper::textItem(otherLayout1, "Time Span (s)");
    numThreadsEdit_ = DialogHelper::intItem(otherLayout1,
                                            "Number Of Threads (0=default)");
    pmFormulaList_ = DialogHelper::dropListItem(
            otherLayout1, "Propagation Matrix Formula",
            {"Lee et al", "Ulrich", "Arnaud et al", "Chen", "Admittance Table"});
    editAdmittanceTable_ = DialogHelper::buttonItem(otherLayout1, "Admittance Table");
    configurationLayout->addLayout(otherLayout1);
    auto otherLayout2 = new QHBoxLayout;
    animationPeriodEdit_ = DialogHelper::doubleItem(otherLayout2,
                                                    "Sensor Animation Period (s)");
    whiteBackgroundCheck_ = DialogHelper::boolItem(otherLayout2, "White background");
    useThinSheetSubcellsCheck_ = DialogHelper::boolItem(otherLayout2,
                                                        "Use Thin Sheet Subcells");
    configurationLayout->addLayout(otherLayout2);
    // The information
    auto infoGroup = new QGroupBox("Information");
    paramLayout->addWidget(infoGroup);
    auto infoLayout = new QVBoxLayout;
    infoGroup->setLayout(infoLayout);
    // Line 1
    auto line1Layout = new QHBoxLayout;
    std::tie(nxEdit_, nyEdit_, nzEdit_) = DialogHelper::vectorItem(
            line1Layout, "N Cells (x,y,z)", true);
    infoLayout->addLayout(line1Layout);
    // Line 2
    auto line2Layout = new QHBoxLayout;
    ntEdit_ = DialogHelper::intItem(line2Layout, "N Time Steps", true);
    dtEdit_ = DialogHelper::doubleItem(line2Layout, "Time Step (s)", true);
    infoLayout->addLayout(line2Layout);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    memoryEdit_ = DialogHelper::intItem(line3Layout, "Memory Use (MBytes)",
                                        true);
    usingOpenMpEdit_ = DialogHelper::textItem(line3Layout, "Using openMP",
                                              true);
    numThreadsUsedEdit_ = DialogHelper::intItem(line3Layout, "Num threads used",
                                                true);
    numThreadsAvailableEdit_ = DialogHelper::intItem(line3Layout, "Available threads",
                                                     true);
    infoLayout->addLayout(line3Layout);
    // Line 4
    auto line4Layout = new QHBoxLayout;
    timeStepEdit_ = DialogHelper::intItem(line4Layout, "Cur Time Step", true);
    processingTimeEdit_ = DialogHelper::doubleItem(line4Layout, "Step Time (s)",
                                                   true);
    processingRateEdit_ = DialogHelper::doubleItem(line4Layout, "Cell rate (kcells/s)",
                                                   true);
    maxProcessingRateEdit_ = DialogHelper::doubleItem(line4Layout, "Max rate (kcells/s)",
                                                      true);
    infoLayout->addLayout(line4Layout);
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(x2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(drxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dryEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(drzEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(timeSpanEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(scatteredFieldZoneSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(boundarySizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(boundaryXList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(boundaryYList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(boundaryZList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(doPropagationMatrices_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(doFdtd_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(numThreadsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(animationPeriodEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(whiteBackgroundCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(editAdmittanceTable_, SIGNAL(clicked()), SLOT(onAdmittanceEdit()));
    connect(pmFormulaList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(useThinSheetSubcellsCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(updateTimer_, SIGNAL(timeout()), SLOT(onUpdateTimer()));
}

// Initialise the panel from the configuration
void ConfigurationDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    x1Edit_->setText(m_->p()->p1().x().text().c_str());
    y1Edit_->setText(m_->p()->p1().y().text().c_str());
    z1Edit_->setText(m_->p()->p1().z().text().c_str());
    x2Edit_->setText(m_->p()->p2().x().text().c_str());
    y2Edit_->setText(m_->p()->p2().y().text().c_str());
    z2Edit_->setText(m_->p()->p2().z().text().c_str());
    drxEdit_->setText(m_->p()->dr().x().text().c_str());
    dryEdit_->setText(m_->p()->dr().y().text().c_str());
    drzEdit_->setText(m_->p()->dr().z().text().c_str());
    timeSpanEdit_->setText(m_->p()->tSize().text().c_str());
    sEdit_->setText(QString::number(m_->p()->s()));
    scatteredFieldZoneSizeEdit_->setText(QString::number(m_->p()->scatteredFieldZoneSize()));
    boundaryXList_->setCurrentIndex(static_cast<int>(m_->p()->boundary().x()));
    boundaryYList_->setCurrentIndex(static_cast<int>(m_->p()->boundary().y()));
    boundaryZList_->setCurrentIndex(static_cast<int>(m_->p()->boundary().z()));
    doPropagationMatrices_->setChecked(m_->p()->doPropagationMatrices());
    doFdtd_->setChecked(m_->p()->doFdtd());
    boundarySizeEdit_->setText(QString::number(m_->p()->boundarySize()));
    numThreadsEdit_->setText(QString::number(m_->p()->numThreadsCfg()));
    animationPeriodEdit_->setText(QString::number(m_->p()->animationPeriod()));
    whiteBackgroundCheck_->setChecked(m_->guiConfiguration()->whiteBackground());
    pmFormulaList_->setCurrentIndex(m_->p()->plateAdmittance().formula());
    useThinSheetSubcellsCheck_->setChecked(m_->p()->useThinSheetSubcells());
    x1Edit_->setToolTip(QString::number(m_->p()->p1().x().value()));
    y1Edit_->setToolTip(QString::number(m_->p()->p1().y().value()));
    z1Edit_->setToolTip(QString::number(m_->p()->p1().z().value()));
    x2Edit_->setToolTip(QString::number(m_->p()->p2().x().value()));
    y2Edit_->setToolTip(QString::number(m_->p()->p2().y().value()));
    z2Edit_->setToolTip(QString::number(m_->p()->p2().z().value()));
    drxEdit_->setToolTip(QString::number(m_->p()->dr().x().value()));
    dryEdit_->setToolTip(QString::number(m_->p()->dr().y().value()));
    drzEdit_->setToolTip(QString::number(m_->p()->dr().z().value()));
    timeSpanEdit_->setToolTip(QString::number(m_->p()->tSize().value()));
}

// An element has changed
void ConfigurationDlg::onChange() {
    if(!initialising_) {
        // New values
        fdtd::Configuration* p = m_->p();
        GuiChangeDetector c;
        auto newX1 = c.testString(x1Edit_, p->p1().x().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newY1 = c.testString(y1Edit_, p->p1().y().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newZ1 = c.testString(z1Edit_, p->p1().z().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newX2 = c.testString(x2Edit_, p->p2().x().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newY2 = c.testString(y2Edit_, p->p2().y().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newZ2 = c.testString(z2Edit_, p->p2().z().text(),
                                  FdTdLife::notifyDomainSizeChange);
        auto newDrX = c.testString(drxEdit_, p->dr().x().text(),
                                   FdTdLife::notifyDomainSizeChange);
        auto newDrY = c.testString(dryEdit_, p->dr().y().text(),
                                   FdTdLife::notifyDomainSizeChange);
        auto newDrZ = c.testString(drzEdit_, p->dr().z().text(),
                                   FdTdLife::notifyDomainSizeChange);
        auto newTimeSpan = c.testString(timeSpanEdit_, p->tSize().text(),
                                        FdTdLife::notifyMinorChange);
        auto newS = c.testDouble(sEdit_, p->s(), FdTdLife::notifyMinorChange);
        auto newAnimationPeriod = c.testDouble(animationPeriodEdit_, p->animationPeriod(),
                                               FdTdLife::notifyMinorChange);
        auto newBoundaryX = static_cast<fdtd::Configuration::Boundary>(c.testInt(
                boundaryXList_, static_cast<int>(p->boundary().x()),
                static_cast<int>(FdTdLife::notifyDomainSizeChange)));
        auto newBoundaryY = static_cast<fdtd::Configuration::Boundary>(c.testInt(
                boundaryYList_, static_cast<int>(p->boundary().y()),
                static_cast<int>(FdTdLife::notifyDomainSizeChange)));
        auto newBoundaryZ = static_cast<fdtd::Configuration::Boundary>(c.testInt(
                boundaryZList_, static_cast<int>(p->boundary().z()),
                static_cast<int>(FdTdLife::notifyDomainSizeChange)));
        auto newPmFormula = static_cast<fdtd::PlateAdmittance::Formula>(c.testInt(
                pmFormulaList_, p->plateAdmittance().formula(),
                FdTdLife::notifyMinorChange));
        auto newDoPropagationMatrices = c.testBool(doPropagationMatrices_,
                                                   p->doPropagationMatrices(),
                                                   FdTdLife::notifyMinorChange);
        auto newDoFdtd = c.testBool(doFdtd_, p->doFdtd(), FdTdLife::notifyMinorChange);
        auto newScatteredFieldZoneSize = c.testSizeT(scatteredFieldZoneSizeEdit_,
                                                     p->scatteredFieldZoneSize(),
                                                     FdTdLife::notifyDomainSizeChange);
        auto newBoundarySize = c.testSizeT(boundarySizeEdit_, p->boundarySize(),
                                           FdTdLife::notifyDomainSizeChange);
        auto newNumThreads = c.testInt(numThreadsEdit_, static_cast<int>(p->numThreadsCfg()),
                                       FdTdLife::notifyMinorChange);
        auto newWhiteBackground = c.testBool(whiteBackgroundCheck_,
                                             m_->guiConfiguration()->whiteBackground(),
                                             FdTdLife::notifyMinorChange);
        auto newUseThinSheetSubcells = c.testBool(useThinSheetSubcellsCheck_,
                                                  p->useThinSheetSubcells(),
                                                  FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            p->p1({newX1, newY1, newZ1});
            p->p2({newX2, newY2, newZ2});
            p->dr({newDrX, newDrY, newDrZ});
            p->tSize(box::Expression(newTimeSpan));
            p->s(newS);
            p->scatteredFieldZoneSize(newScatteredFieldZoneSize);
            p->boundary({newBoundaryX, newBoundaryY, newBoundaryZ});
            p->boundarySize(newBoundarySize);
            p->doPropagationMatrices(newDoPropagationMatrices);
            p->doFdtd(newDoFdtd);
            p->numThreadsCfg(newNumThreads);
            p->animationPeriod(newAnimationPeriod);
            m_->guiConfiguration()->set(newWhiteBackground);
            p->plateAdmittance().formula(newPmFormula);
            p->useThinSheetSubcells(newUseThinSheetSubcells);
            // Tell others
            m_->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void ConfigurationDlg::tabChanged() {
    onChange();
}

// Handle a notification
void ConfigurationDlg::notify(fdtd::Notifier* source, int why,
                              fdtd::NotificationData* /*data*/) {
    if(m_ == source) {
        switch(why) {
            case FdTdLife::notifySteppedE:
            case FdTdLife::notifySteppedH:
                break;
            case FdTdLife::notifyStepComplete:
#if 0
                processingTimeEdit_->setText(QString::number(m_->p()->stepProcessingTime()));
                processingRateEdit_->setText(QString::number(m_->p()->cellProcessingRate()));
                maxProcessingRateEdit_->setText(
                        QString::number(m_->p()->maxCellProcessingRate()));
                timeStepEdit_->setText(QString::number(m_->p()->timeStep()));
#endif
                break;
            default:
                nxEdit_->setText(QString::number(m_->p()->geometry()->n().x()));
                nyEdit_->setText(QString::number(m_->p()->geometry()->n().y()));
                nzEdit_->setText(QString::number(m_->p()->geometry()->n().z()));
                timeStepEdit_->setText(QString::number(m_->p()->timeStep()));
                ntEdit_->setText(QString::number(m_->p()->nt()));
                dtEdit_->setText(QString::number(m_->p()->dt()));
                memoryEdit_->setText(QString::number(m_->memoryUse() / 1000000));
                processingTimeEdit_->setText(QString::number(m_->p()->stepProcessingTime()));
                processingRateEdit_->setText(QString::number(m_->p()->cellProcessingRate()));
                maxProcessingRateEdit_->setText(
                        QString::number(m_->p()->maxCellProcessingRate()));
                usingOpenMpEdit_->setText(QString(m_->p()->usingOpenMp() ? "yes" : "no"));
                numThreadsUsedEdit_->setText(QString::number(m_->p()->numThreadsUsed()));
                numThreadsAvailableEdit_->setText(QString::number(m_->p()->maxNumThreads()));
                break;
        }
    }
}

// The admittance table edit button has been pressed
void ConfigurationDlg::onAdmittanceEdit() {
    PlateAdmittanceEditDlg box(this, m_->p()->plateAdmittance().admittanceTable(),
                               m_->p()->plateAdmittance().tableType());
    if(box.exec() == QDialog::Accepted) {
        m_->p()->plateAdmittance().admittanceTable(box.table());
        m_->p()->plateAdmittance().tableType(box.tableType());
    }
}

// Time to update the main status part of the display
void ConfigurationDlg::onUpdateTimer() {
    processingTimeEdit_->setText(QString::number(m_->p()->stepProcessingTime()));
    processingRateEdit_->setText(QString::number(m_->p()->cellProcessingRate()));
    maxProcessingRateEdit_->setText(
            QString::number(m_->p()->maxCellProcessingRate()));
    timeStepEdit_->setText(QString::number(m_->p()->timeStep()));
}
