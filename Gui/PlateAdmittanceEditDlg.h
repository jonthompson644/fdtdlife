/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PLATEADMITTANCEEDITDLG_H
#define FDTDLIFE_PLATEADMITTANCEEDITDLG_H

#include "QtIncludes.h"
#include <PlateAdmittance.h>
#include "InitialisingGui.h"

class PlateAdmittanceEditDlg : public QDialog {
Q_OBJECT
public:
    PlateAdmittanceEditDlg(QWidget* parent, const std::map<double, fdtd::AdmittanceData>& table,
            fdtd::PlateAdmittance::PlateType tableType);
    const std::map<double, fdtd::AdmittanceData>& table() const {return table_;}
    fdtd::PlateAdmittance::PlateType tableType() const {return tableType_;}
protected slots:
    void onPasteFrequency();
    void onPasteReal();
    void onPasteImaginary();
    void onPasteAdmittance();
    void onClearAll();
    void onAccepted();
    void onPlateSizeSelected();
    void onNewPlateSize();
    void onDeletePlateSize();
    void onPlateSizeChanged(QTreeWidgetItem* item, int column);
    void onCopyTable();
    void onPasteTable();
    void onClearTable();
protected:
    enum {admColFrequency=0, admColReal, admColImaginary, numAdmCols};
    enum {plateColSize=0, numPlateCols};
    void pasteToColumn(int colNumber);
    void fillPlateList(double selectSize=0.0);
    void fillAdmittanceList();
    double getCurrentPlateSize();
    bool isPlateSizeSelected();
    void readAdmittanceList();
    void pasteExcelFormat(const QMimeData* mimeData);
    void pasteInternalFormat(const QMimeData* mimeData);
protected:
    InitialisingGui initialising_;
    // The data to be edited
    std::map<double, fdtd::AdmittanceData> table_;  // Admittance data sorted by plate/hole size
    fdtd::PlateAdmittance::PlateType tableType_;  // Kind of admittance data stored in the table
    double currentPlateSize_;
    // The editing widgets
    QDialogButtonBox* buttons_;
    QTreeWidget* plateTree_;
    QTreeWidget* admTree_;
    QPushButton* pasteFrequencyButton_;
    QPushButton* pasteRealButton_;
    QPushButton* pasteImaginaryButton_;
    QPushButton* pasteStepButton_;
    QPushButton* clearAllButton_;
    QLineEdit* cellSizeEdit_;
    QComboBox* tableTypeList_;
    QPushButton* newPlateSizeButton_;
    QPushButton* deletePlateSizeButton_;
    QPushButton* copyTableButton_;
    QPushButton* pasteTableButton_;
    QPushButton* clearTableButton_;
};


#endif //FDTDLIFE_PLATEADMITTANCEEDITDLG_H
