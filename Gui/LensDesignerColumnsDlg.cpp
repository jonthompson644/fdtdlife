/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensDesignerColumnsDlg.h"
#include "DesignersDlg.h"
#include "DialogHelper.h"
#include "NoEditDelegate.h"
#include "GuiChangeDetector.h"
#include <Designer/LensDesigner.h>
#include <Box/StlWriter.h>

// Constructor
LensDesignerColumnsDlg::LensDesignerColumnsDlg(LensDesignerDlg* dlg,
                                               designer::LensDesigner* designer) :
        LensDesignerDlg::SubDlg(dlg, designer),
        Notifiable(dlg->dlg()->model()) {
    // Create the controls
    columns_ = new QTreeWidget;
    columns_->setSelectionMode(QAbstractItemView::SingleSelection);
    columns_->setIndentation(0);
    columns_->setItemDelegateForColumn(resultsColLayer,
                                       new NoEditDelegate(this));
    layout_->addWidget(columns_);
    totals_ = new QTreeWidget;
    totals_->setSelectionMode(QAbstractItemView::NoSelection);
    totals_->setIndentation(0);
    layout_->addWidget(totals_);
    auto optionsLayout = new QHBoxLayout;
    layout_->addLayout(optionsLayout);
    exportSymmetricalHalf_ = DialogHelper::boolItem(optionsLayout, "Export Symmetrical");
    columnNumber_ = DialogHelper::sizeTItem(optionsLayout, "Column Num", 0);
    geneticRange_ = DialogHelper::doubleItem(optionsLayout, "GA Range");
    geneticBits_ = DialogHelper::sizeTItem(optionsLayout, "GA Bits", 0);
    auto buttonLayout = new QHBoxLayout;
    layout_->addLayout(buttonLayout);
    copyResults_ = DialogHelper::buttonItem(buttonLayout, "Copy CSV to clipboard");
    pasteResults_ = DialogHelper::buttonItem(buttonLayout, "Paste CSV from clipboard");
    export_ = DialogHelper::buttonMenuItem(
            buttonLayout, "Export",
            {"Diameter Slice, STL", "Diameter Slice, DXF", "Quadrant, DXF",
             "Diameter Slice, HFSS", "Quadrant, HFSS", "Column, HFSS"});
    generateGa_ = DialogHelper::buttonMenuItem(
            buttonLayout, "Generate GA",
            std::list<std::string>({"Single Column", "Adjust Full"}));
    fillModel_ = DialogHelper::buttonMenuItem(
            buttonLayout, "Make Model",
            std::list<std::string>({"Square Plates", "Dielectric Blocks",
                                    "Finger Patterns"}));
    buttonLayout->addStretch();
    // Connect the handlers
    connect(copyResults_, SIGNAL(clicked()), SLOT(onCopyResults()));
    connect(pasteResults_, SIGNAL(clicked()), SLOT(onPasteResults()));
    connect(export_, SIGNAL(clickedMenu(int)), SLOT(onExport(int)));
    connect(generateGa_, SIGNAL(clickedMenu(int)), SLOT(onGenerateGa(int)));
    connect(fillModel_, SIGNAL(clickedMenu(int)), SLOT(onFillModel(int)));
    connect(columns_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onCellChanged(QTreeWidgetItem * , int)));
    connect(exportSymmetricalHalf_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(columnNumber_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(geneticRange_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(geneticBits_, SIGNAL(editingFinished()), SLOT(onChange()));
    // Fill the panel
    initialise();
}

// Fill the dialog panel with information
void LensDesignerColumnsDlg::initialise() {
    // Base class first
    LensDesignerDlg::SubDlg::initialise();
    // The results list
    fillResultsList();
    fillTotalsList();
    // Other controls
    InitialisingGui::Set s(initialising_);
    exportSymmetricalHalf_->setChecked(dlg_->lensDesigner()->exportSymmetricalHalf());
    columnNumber_->setText(QString::number(dlg_->lensDesigner()->columnNumber()));
    geneticRange_->setText(QString::number(dlg_->lensDesigner()->geneticRange()));
    geneticBits_->setText(QString::number(dlg_->lensDesigner()->geneticBits()));
}

// An element has changed
void LensDesignerColumnsDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newExportSymmetricalHalf = c.testBool(
                exportSymmetricalHalf_,
                dlg_->lensDesigner()->exportSymmetricalHalf(),
                FdTdLife::notifyMinorChange);
        auto newColumnNumber = c.testSizeT(
                columnNumber_, dlg_->lensDesigner()->columnNumber(),
                FdTdLife::notifyMinorChange);
        auto newGeneticRange = c.testDouble(
                geneticRange_, dlg_->lensDesigner()->geneticRange(),
                FdTdLife::notifyMinorChange);
        auto newGeneticBits = c.testSizeT(
                geneticBits_, dlg_->lensDesigner()->geneticBits(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            dlg_->lensDesigner()->exportSymmetricalHalf(newExportSymmetricalHalf);
            dlg_->lensDesigner()->columnNumber(newColumnNumber);
            dlg_->lensDesigner()->geneticRange(newGeneticRange);
            dlg_->lensDesigner()->geneticBits(newGeneticBits);
            // Tell others
            dlg_->dlg()->model()->doNotification(c.why());
        }
    }
}

// Fill the results list
void LensDesignerColumnsDlg::fillResultsList() {
    InitialisingGui::Set s(initialising_);
    columns_->clear();
    columns_->setColumnCount(resultsColFirstColumn +
                             static_cast<int>(dlg_->lensDesigner()->columns().size()));
    QStringList labels;
    labels.push_back("Layer");
    for(size_t i = 0; i < dlg_->lensDesigner()->columns().size(); i++) {
        labels.push_back(QString::number(i));
    }
    columns_->setHeaderLabels(labels);
    for(size_t layer = 0; layer < dlg_->lensDesigner()->numLayers(); layer++) {
        auto item = new QTreeWidgetItem(columns_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
        item->setText(resultsColLayer, QString::number(layer));
        for(auto& c : dlg_->lensDesigner()->columns()) {
            if(layer < c.numLayers()) {
                item->setText(resultsColFirstColumn + static_cast<int>(c.index()),
                              QString::number(c.cell(layer).refractiveIndex()));
            }
        }
    }
    for(int i = 0; i < columns_->columnCount(); i++) {
        columns_->resizeColumnToContents(i);
    }
}

// Fill the totals list
void LensDesignerColumnsDlg::fillTotalsList() {
    InitialisingGui::Set s(initialising_);
    totals_->clear();
    totals_->setColumnCount(totalsColFirstColumn +
                            static_cast<int>(dlg_->lensDesigner()->columns().size()));
    QStringList labels;
    labels.push_back("What");
    for(size_t i = 0; i < dlg_->lensDesigner()->columns().size(); i++) {
        labels.push_back(QString::number(i));
    }
    totals_->setHeaderLabels(labels);
    // The required total phase shift
    auto item = new QTreeWidgetItem(totals_);
    item->setFlags(Qt::NoItemFlags);
    item->setText(totalsColLayer, "Desired");
    for(auto& c : dlg_->lensDesigner()->columns()) {
        item->setText(resultsColFirstColumn + static_cast<int>(c.index()),
                      QString::number(c.lm()));
    }
    // The current total phase shift
    item = new QTreeWidgetItem(totals_);
    item->setFlags(Qt::NoItemFlags);
    item->setText(totalsColLayer, "Current");
    for(auto& c : dlg_->lensDesigner()->columns()) {
        item->setText(resultsColFirstColumn + static_cast<int>(c.index()),
                      QString::number(c.lm()));
    }
    // Column sizes
    for(int i = 0; i < totals_->columnCount(); i++) {
        totals_->resizeColumnToContents(i);
    }
}

// Handle change notifications
void LensDesignerColumnsDlg::notify(fdtd::Notifier* source, int why,
                                    fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        switch(why) {
            case FdTdLife::notifySequencerChange:
            case FdTdLife::notifyMaterialChange:
                fillResultsList();
                fillTotalsList();
                break;
            default:
                break;
        }
    }
}

// Copy the results table to the clipboard
void LensDesignerColumnsDlg::onCopyResults() {
    // Convert the result data into tab separated values
    std::stringstream t;
    t << "Layer";
    for(size_t col = 0; col < dlg_->lensDesigner()->columns().size(); col++) {
        t << "\t" << col;
    }
    t << std::endl;
    for(size_t layer = 0; layer < dlg_->lensDesigner()->numLayers(); layer++) {
        t << layer;
        for(auto& c : dlg_->lensDesigner()->columns()) {
            t << "\t" << c.cell(layer).refractiveIndex();
        }
        t << std::endl;
    }
    // Copy the data to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(t.str().c_str());
}

// Paste from the clipboard
void LensDesignerColumnsDlg::onPasteResults() {
    QClipboard* clipboard = QApplication::clipboard();
    QString text = clipboard->text();
    QStringList lines = text.split('\n');
    std::cout << text.toStdString() << std::endl;
    size_t layer = 0;
    for(auto& line : lines) {
        QStringList values = line.split('\t');
        auto col = dlg_->lensDesigner()->columns().begin();
        for(auto& value : values) {
            double n = value.toDouble();
            if(layer < dlg_->lensDesigner()->numLayers() &&
               col != dlg_->lensDesigner()->columns().end()) {
                col->cell(layer).refractiveIndex(n);
            }
            ++col;
        }
        layer++;
    }
    dlg_->lensDesigner()->updateRefractiveIndices();
    dlg_->dlg()->model()->doNotification(FdTdLife::notifyMaterialChange);
}

// Write out an STL file containing the calculated squares.
void LensDesignerColumnsDlg::onExport(int index) {
    // Get a filename
    QFileInfo info(dlg_->dlg()->model()->getCurrentFileName());
    std::string filter;
    std::string title;
    switch(index) {
        default:
        case exportDiameterSliceStl:
            title = "Export diameter slice to STL file";
            filter = "STL Files (*.stl)";
            break;
        case exportDiameterSliceDxf:
            title = "Export diameter slice layer to DXF files";
            filter = "DXF Files (*.dxf)";
            break;
        case exportQuadrantDxf:
            title = "Export whole layer to DXF files";
            filter = "DXF Files (*.dxf)";
            break;
        case exportDiameterSliceHfss:
            title = "Export diameter slice layer to HFSS python file";
            filter = "HFSS Python Files (*.py)";
            break;
        case exportQuadrantHfss:
            title = "Export whole layer to HFSS python file";
            filter = "HFSS Python Files (*.py)";
            break;
        case exportColumnHfss:
            title = "Export column to HFSS python file";
            filter = "HFSS Python Files (*.py)";
            break;
    }
    QString fileName = QFileDialog::getSaveFileName(this, title.c_str(),
                                                    info.path() + "/" +
                                                    info.completeBaseName(),
                                                    filter.c_str(), nullptr,
                                                    QFileDialog::DontConfirmOverwrite);
    if(!fileName.isEmpty()) {
        QFileInfo selInfo(fileName);
        // Perform the export
        switch(index) {
            default:
            case exportDiameterSliceStl:
                dlg_->lensDesigner()->exportDiameterSliceAsStl(fileName.toStdString());
                break;
            case exportDiameterSliceDxf:
                for(size_t layer = 0; layer < dlg_->lensDesigner()->numLayers(); layer++) {
                    std::stringstream name;
                    name << selInfo.path().toStdString() << "/"
                         << selInfo.completeBaseName().toStdString()
                         << "_" << layer << "." << selInfo.suffix().toStdString();
                    //dlg_->lensDesigner()->exportDiameterSliceLayerAsDxf(name.str(), layer);
                }
                break;
            case exportQuadrantDxf:
                for(size_t layer = 0; layer < dlg_->lensDesigner()->numLayers(); layer++) {
                    std::stringstream name;
                    name << selInfo.path().toStdString() << "/"
                         << selInfo.completeBaseName().toStdString()
                         << "_" << layer << "." << selInfo.suffix().toStdString();
                    dlg_->lensDesigner()->exportQuadrantLayerAsDxf(name.str(), layer);
                }
                break;
            case exportDiameterSliceHfss:
                dlg_->lensDesigner()->exportDiameterSliceAsHfss(fileName.toStdString());
                break;
            case exportQuadrantHfss:
                dlg_->lensDesigner()->exportQuadrantAsHfss(fileName.toStdString());
                break;
            case exportColumnHfss:
                dlg_->lensDesigner()->exportColumnAsHfss(fileName.toStdString());
                break;
        }
    }
}

// A cell has been changed
void LensDesignerColumnsDlg::onCellChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        size_t layer = columns_->indexOfTopLevelItem(item);
        if(layer < dlg_->lensDesigner()->numLayers()) {
            auto pos = std::next(dlg_->lensDesigner()->columns().begin(),
                                 column - resultsColFirstColumn);
            if(pos != dlg_->lensDesigner()->columns().end()) {
                auto& cell = pos->cell(layer);
                bool ok = false;
                double refractiveIndex = item->text(column).toDouble(&ok);
                if(ok) {
                    cell.refractiveIndex(refractiveIndex);
                    dlg_->lensDesigner()->updateRefractiveIndices();
                    dlg_->dlg()->model()->doNotification(FdTdLife::notifyMaterialChange);
                } else {
                    item->setText(column, QString::number(cell.refractiveIndex()));
                }
            }
        }
    }
}

// Load the modelling tool with a genetic algorithm that
// evolves a column.
void LensDesignerColumnsDlg::onGenerateGa(int index) {
    switch(index) {
        case gaCommandColumn:
            dlg_->lensDesigner()->generateColumnGa();
            break;
        case gaCommandAdjustFull:
            dlg_->lensDesigner()->generateFullAdjustGa();
            break;
        default:
            break;
    }
}

// Use the current column contents to fill the FDTD model
void LensDesignerColumnsDlg::onFillModel(int index) {
    switch(index) {
        case fillModelSquarePlates:
            dlg_->lensDesigner()->modelling(
                    designer::LensDesigner::Modelling::metamaterialCells);
            break;
        case fillModelDielectricBlocks:
            dlg_->lensDesigner()->modelling(
                    designer::LensDesigner::Modelling::dielectricBlocks);
            break;
        case fillModelFingerPatterns:
            dlg_->lensDesigner()->modelling(
                    designer::LensDesigner::Modelling::fingerPatterns);
            break;
        default:
            break;
    }
    dlg_->lensDesigner()->loadModel();
}
