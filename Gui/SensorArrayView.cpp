/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Sensor/ArraySensor.h>
#include "SensorArrayView.h"
#include "ViewFrame.h"
#include "SensorArrayViewDlg.h"
#include <Xml/DomObject.h>

// Constructor
SensorArrayView::SensorArrayView(int identifier, int sensorId, const char *sensorData,
                                 ViewFrame *view) :
        TwoDSliceView(identifier, sensorId, sensorData, view),
        currentCellX_(0),
        currentCellY_(0),
        scaling_(scalingManual),
        displayMin_(0.0),
        displayRange_(1.0) {
}

// The mouse has been pressed in our data area
void SensorArrayView::leftClick(QPoint pos) {
    int clickX = pos.x() - dataArea_.left();
    int clickY = pos.y() - dataArea_.top();
    // Is the point within the data area?
    int maxX = cellSizeX_ * repeatX_ * nSliceX_;
    int maxY = cellSizeY_ * repeatY_ * nSliceY_;
    if(clickX < maxX && clickY < maxY) {
        // Convert the clicked position to a cell
        currentCellX_ = (clickX / cellSizeX_) % nSliceX_;
        currentCellY_ = (clickY / cellSizeY_) % nSliceY_;
        spec_->setMetaData(sensor::DataSpec::metaCurrentXYPos,
                           currentCellX_, currentCellY_);
        // Update the view
        view()->update();
    }
}

// Draw the data for this item
void SensorArrayView::paintData(QPainter* painter) {
    std::string units;
    double oldDisplayMin;
    double oldDisplayMax;
    double oldDisplayRange;
    // Work out the scaling (the base class only does this at initialisation)
    switch(scaling_) {
        case scalingManual:
            break;
        case scalingAutomaticMin:
            oldDisplayMin = displayMin_;
            oldDisplayMax = displayMax_;
            spec_->getMetaData(sensor::DataSpec::metaMinimum, &displayMin_, units);
            displayMax_ = displayMin_ + displayRange_;
            if(oldDisplayMin != displayMin_ || oldDisplayMax != displayMax_) {
                view()->notifyModel(true);
            }
            break;
        case scalingFullAutomatic:
            oldDisplayMin = displayMin_;
            oldDisplayMax = displayMax_;
            oldDisplayRange = displayRange_;
            displayRange_ = displayMax_ - displayMin_;
            spec_->getMetaData(sensor::DataSpec::metaMinimum, &displayMin_, units);
            spec_->getMetaData(sensor::DataSpec::metaMaximum, &displayMax_, units);
            if(oldDisplayMin != displayMin_ || oldDisplayMax != displayMax_ ||
                    oldDisplayRange != displayRange_) {
                view()->notifyModel(true);
            }
            break;
    }
    switch (colorMap_) {
        case colorMapExtKindlemann:
            scale_ = (displayMax_ - displayMin_) / brushes_.size();
            offset_ = displayMin_;
            break;
        case colorMapDivergingBY:
            displayMax_ = std::max(std::abs(displayMax_), std::abs(displayMin_));
            displayMin_ = -displayMax_;
            scale_ = (displayMax_ - displayMin_) / brushes_.size();
            offset_ = displayMin_;
            break;
        case colorMapMaterial:
            scale_ = 1.0;
            offset_ = 0.0;
            break;
    }
    // Base class does most of the work
    TwoDSliceView::paintData(painter);
    // Now our stuff
    painter->save();
    if(nSliceX_ > 0 && nSliceY_ > 0 && spec_ != nullptr) {
        // Draw the marker
        painter->setPen(Qt::red);
        painter->setBrush(Qt::NoBrush);
        for(int ni=0; ni<repeatX_; ni++) {
            for(int nj=0; nj<repeatY_; nj++) {
                painter->drawRect((ni * nSliceX_ + currentCellX_) * cellSizeX_ + dataArea_.left(),
                                  ((nj * nSliceY_) + currentCellY_) * cellSizeY_ + dataArea_.top(),
                                  cellSizeX_, cellSizeY_);
            }
        }
        // Work out where to draw the marker information
        QFontInfo info = painter->fontInfo();
        int maxX = cellSizeX_ * repeatX_ * nSliceX_;
        int maxY = cellSizeY_ * repeatY_ * nSliceY_;
        int x = 0;
        int y = 0;
        if(dataArea_.width() - maxX > dataArea_.height() - maxY) {
            x = dataArea_.left() + maxX + info.pixelSize();
            y = dataArea_.top() + info.pixelSize();
        } else {
            x = dataArea_.left() + info.pixelSize();
            y = dataArea_.top() + maxY + info.pixelSize();
        }
        // Draw the marker information
        QPen markerPen(Qt::darkGray, 1);
        painter->setPen(markerPen);
        QString text;
        text = QString("%1,%2").arg(currentCellX_).arg(currentCellY_);
        painter->drawText(x,y,text);
        y += info.pixelSize();
        double frequency;
        spec_->getMetaData(sensor::DataSpec::metaFrequency, &frequency, units);
        text = QString("%1%2").arg(frequency).arg(units.c_str());
        painter->drawText(x,y,text);
    }
    painter->restore();
}

// Make the sub dialog for this view item
ViewItemDlg *SensorArrayView::makeDialog(ViewFrameDlg *dlg) {
    return new SensorArrayViewDlg(dlg, this);
}

// Set parameters
void SensorArrayView::set(TwoDSliceView::ColorMap colorMap, int repeatX,
                          int repeatY, Scaling scaling, double displayMin,
                          double displayMax, double displayRange) {
    TwoDSliceView::set(colorMap, repeatX, repeatY, displayMax);
    scaling_ = scaling;
    displayMin_ = displayMin;
    displayMax_ = displayMax;
    displayRange_ = displayRange;
    // Update the view
    view()->update();
}

// Write the configuration to an XML DOM
void SensorArrayView::writeConfig(xml::DomObject& root) const {
    // Base class first
    TwoDSliceView::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("scaling") << scaling_;
    root << xml::Obj("displaymin") << displayMin_;
    root << xml::Obj("displayrange") << displayRange_;
    root << xml::Close();
}

// Read the configuration from an XML DOM
void SensorArrayView::readConfig(xml::DomObject& root) {
    // Base class first
    TwoDSliceView::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("scaling") >> scaling_;
    root >> xml::Obj("displaymin") >> displayMin_;
    root >> xml::Obj("displayrange") >> displayRange_;
    root >> xml::Close();
}
