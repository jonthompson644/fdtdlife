/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ShapeDlg.h"
#include "ShapesDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include <Domain/Shape.h>

// Second stage constructor used by factory
void ShapeDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    dlg_ = dlg;
    shape_ = shape;
    auto paramsLayout = new QVBoxLayout;
    layout_ = new QVBoxLayout;
    setLayout(paramsLayout);
    topLineLayout_ = new QHBoxLayout;
    nameEdit_ = DialogHelper::textItem(topLineLayout_, "Name");
    materialList_ = DialogHelper::dropListItem(topLineLayout_, "Material");
    ignoreCheck_ = DialogHelper::boolItem(topLineLayout_, "Ignore");
    layout_->addLayout(topLineLayout_);
    // The group box
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(layout_);
    stretchLayout->addStretch();
    shapeGroup_ = new QGroupBox;
    shapeGroup_->setLayout(stretchLayout);
    paramsLayout->addWidget(shapeGroup_);
    // Connect handlers
    connect(materialList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(nameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ignoreCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void ShapeDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    nameEdit_->setText(shape_->name().c_str());
    dlg_->model()->fillMaterialList(materialList_, shape_->material());
    ignoreCheck_->setChecked(shape_->ignore());
}

// A parameter has changed
void ShapeDlg::onChange() {
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        int newMaterialIndex = dlg_->model()->selected(materialList_);
        c.externalTest(newMaterialIndex != shape_->material(),
                       FdTdLife::notifyDomainContentsChange);
        auto newName = c.testString(nameEdit_, shape_->name(),
                                    FdTdLife::notifyMinorChange);
        auto newIgnore = c.testBool(ignoreCheck_, shape_->ignore(),
                                    FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->name(newName);
            shape_->material(newMaterialIndex);
            shape_->ignore(newIgnore);
            dlg_->updateCurrentSelection();
            dlg_->model()->doNotification(c.why());
        }
    }
}
