/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearchPopulationDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchSeqDlg.h"
#include "SequencersDlg.h"
#include "GuiConfiguration.h"
#include "OneDDoubleFitnessDataDlg.h"
#include "TwoDDoubleFitnessDataDlg.h"
#include "OneDComplexFitnessDataDlg.h"
#include "IndividualEditDlg.h"
#include <Gs/GeneticSearch.h>
#include <Gs/Population.h>
#include <Gs/Individual.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Gs/RepeatCellGene.h>
#include <Gs/LayerSpacingGene.h>
#include <Fdtd/Model.h>
#include <Gs/FitnessFunction.h>
#include <Gs/FitnessBase.h>

// Constructor
GeneticSearchPopulationDlg::GeneticSearchPopulationDlg(GeneticSearchSeqDlg* dlg) :
        GeneticSearchSubDlg(dlg),
        Notifiable(dlg->dlg()->model()) {
    auto* populationLayout = new QVBoxLayout;
    populationLayout->setContentsMargins(0, 0, 0, 0);
    populationLayout->setSpacing(4);
    // Row 1 summary info
    auto* row1Layout = new QHBoxLayout;
    populationLayout->addLayout(row1Layout);
    generationNumberEdit_ = DialogHelper::intItem(row1Layout, "Generation");
    minFitnessEdit_ = DialogHelper::doubleItem(row1Layout, "Best", true);
    maxFitnessEdit_ = DialogHelper::doubleItem(row1Layout, "Worst", true);
    meanFitnessEdit_ = DialogHelper::doubleItem(row1Layout, "Mean", true);
    commandButton_ = DialogHelper::buttonMenuItem(row1Layout, "Command",
                                                  std::list<std::string>(
                                                          {"Save Population",
                                                           "Rebuild History"}));
    // The population display
    auto* popDisplayLayout = new QHBoxLayout;
    populationLayout->addLayout(popDisplayLayout);
    populationTree_ = new QTreeWidget;
    populationTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    populationTree_->setColumnCount(numColumns);
    populationTree_->setHeaderLabels({"Id", "State", "Unfitness", "Mother",
                                      "Father", "Repeat Cell"});
    populationTree_->setContextMenuPolicy(Qt::CustomContextMenu);
    popDisplayLayout->addWidget(populationTree_);
    auto popButtonsLayout = new QVBoxLayout;
    stepBackButton_ = DialogHelper::buttonItem(popButtonsLayout, "<");
    firstButton_ = DialogHelper::buttonItem(popButtonsLayout, "|<");
    stepForwardButton_ = DialogHelper::buttonItem(popButtonsLayout, ">");
    lastButton_ = DialogHelper::buttonItem(popButtonsLayout, ">|");
    popButtonsLayout->addStretch();
    popDisplayLayout->addLayout(popButtonsLayout);
#if 0
    // The unfitness progress
    auto* bottomLayout = new QHBoxLayout;
    populationLayout->addLayout(bottomLayout);
    auto* unfitnessProgressLayout = new QVBoxLayout;
    unfitnessProgressView_ = new DataSeriesWidget(dlg_->dlg()->model()->parameters());
    unfitnessProgressView_->setAxisInfo(0.0, 1.0, "", 0.0, 10.0, "");
    unfitnessProgressLayout->addWidget(new QLabel("Progress"));
    unfitnessProgressLayout->addWidget(unfitnessProgressView_);
    bottomLayout->addLayout(unfitnessProgressLayout);
    // The 2D slice view
    twoDSliceView_ = new TwoDSliceWidget(dlg_->dlg()->model()->parameters());
    bottomLayout->addWidget(twoDSliceView_);
    // Transmittance data
    auto* transmittanceLayout = new QVBoxLayout;
    transmittanceView_ = new DataSeriesWidget(dlg_->dlg()->model()->parameters());
    transmittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0, 1.0, "");
    transmittanceLayout->addWidget(new QLabel("Transmittance"));
    transmittanceLayout->addWidget(transmittanceView_);
    bottomLayout->addLayout(transmittanceLayout);
    // Phase shift data
    auto* phaseShiftLayout = new QVBoxLayout;
    phaseShiftView_ = new DataSeriesWidget(dlg_->dlg()->model()->parameters());
    phaseShiftView_->setAxisInfo(0.0, 100.0, "GHz", -180.0, 180.0, "deg");
    phaseShiftLayout->addWidget(new QLabel("Phase Shift"));
    phaseShiftLayout->addWidget(phaseShiftView_);
    bottomLayout->addLayout(phaseShiftLayout);
#endif
    // The individual data zone
    layersList_ = new LayersList(dlg_->sequencer());
    populationLayout->addWidget(layersList_);
    dataLayout_ = new QHBoxLayout;
    dataLayout_->setContentsMargins(0, 0, 0, 0);
    dataLayout_->setSpacing(4);
    populationLayout->addLayout(dataLayout_);
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataTransmittance));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDComplexFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataAdmittance));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataPhaseShift));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataAmplitudeDist));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataPhaseDist));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataTransmittanceDist));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<TwoDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataField));
    dataWidgetFactory_.define(new DataWidgetFactory::Creator<OneDDoubleFitnessDataDlg>(
            gs::GeneticSearch::fitnessDataTimeSeries));
    // Set the layout
    setTabLayout(populationLayout);
    // Connect the handlers
    connect(populationTree_, SIGNAL(itemSelectionChanged()),
            SLOT(onIndividualSelect()));
    connect(stepBackButton_, SIGNAL(clicked()), SLOT(onStepBack()));
    connect(stepForwardButton_, SIGNAL(clicked()), SLOT(onStepForward()));
    connect(firstButton_, SIGNAL(clicked()), SLOT(onFirst()));
    connect(lastButton_, SIGNAL(clicked()), SLOT(onLast()));
    connect(populationTree_, SIGNAL(customContextMenuRequested(
                                            const QPoint&)), SLOT(onPopulationContext(
                                                                          const QPoint&)));
    connect(generationNumberEdit_, SIGNAL(editingFinished()),
            SLOT(onGenerationNumber()));
    connect(populationTree_, SIGNAL(itemDoubleClicked(QTreeWidgetItem * , int)),
            SLOT(onEditIndividual()));
    connect(commandButton_, SIGNAL(clickedMenu(int)), SLOT(onCommand(int)));
    connect(layersList_, SIGNAL(customContextMenuRequested(
                                        const QPoint&)), SLOT(onLayersContext(
                                                                      const QPoint&)));
    // Fill the panel
    initialise();
}

// Initialise the panel from the configuration
void GeneticSearchPopulationDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fillPopulationList();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneticSearchPopulationDlg::tabChanged() {
}

// Return the currently selected individual
std::shared_ptr<gs::Individual> GeneticSearchPopulationDlg::currentIndividual() {
    QTreeWidgetItem* item = populationTree_->currentItem();
    std::shared_ptr<gs::Individual> ind = nullptr;
    if(item != nullptr) {
        int id = item->text(columnId).toInt();
        ind = dlg_->sequencer()->population()->getIndividual(id);
    }
    return ind;
}

// An individual has been selected
void GeneticSearchPopulationDlg::onIndividualSelect() {
    if(!initialising_) {
        // Get the individual
        std::shared_ptr<gs::Individual> ind = currentIndividual();
        if(ind != nullptr) {
            // Fill the layers for the individual
            layersList_->clear();
            for(auto a : ind->chromosome()->genes()) {
                layersList_->insertLayer(a);
            }
            // Now the symmetry layers
            auto symmetryMode = ind->getLayerSymmetry(
                    dlg_->sequencer()->layerSymmetry());
            if(symmetryMode != gs::GeneticSearch::layerSymmetryNone) {
                bool doInsert = (symmetryMode == gs::GeneticSearch::layerSymmetryEven);
                for(auto pos = ind->chromosome()->genes().rbegin();
                    pos != ind->chromosome()->genes().rend();
                    ++pos) {
                    doInsert = layersList_->insertLayer(*pos, doInsert) || doInsert;
                }
            }
            // Plot the graphs for the individual
            plotIndividualData(ind);
            // Fill the model from the individual if we are not running
            if(dlg_->dlg()->model()->isStopped()) {
                dlg_->dlg()->model()->setStopped();
                ind->decodeGenes(dlg_->sequencer());
                dlg_->sequencer()->loadIndividualIntoModel(ind);
                dlg_->dlg()->model()->matchModelConfig();
                dlg_->dlg()->model()->initialiseModel();
                dlg_->dlg()->model()->doNotification(FdTdLife::notifyIndividualLoaded);
            }
        }
    }
}

// Place the individual's data into the data displays
void
GeneticSearchPopulationDlg::plotIndividualData(const std::shared_ptr<gs::Individual>& ind) {
#if 0
    fdtd::FitnessPartResult* fitnessResult = nullptr;
    if(!ind->fitnessResults().empty()) {
        fitnessResult = &ind->fitnessResults().front();
    }
    std::vector<double> minTrans;
    std::vector<double> maxTrans;
    std::vector<double> minPhase;
    std::vector<double> maxPhase;
    std::vector<double> minTransY;
    std::vector<double> maxTransY;
    std::vector<double> minPhaseY;
    std::vector<double> maxPhaseY;
    for(auto& fitnessFunction : dlg_->sequencer()->fitness()->functions()) {
        if(fitnessFunction->dataSource() == fdtd::FitnessBase::dataSourceYComponent) {
            fitnessFunction->getFitnessSpec(minTransY, maxTransY, minPhaseY, maxPhaseY, fitnessResult);
        } else {
            fitnessFunction->getFitnessSpec(minTrans, maxTrans, minPhase, maxPhase, fitnessResult);
        }
    }
    std::vector<double> transmittance;
    ind->getTransmittance(transmittance);
    transmittanceView_->setData(transmittance, ParametersCfg::elementDataA, true);
    if(ind->xyComponents()) {
        ind->getTransmittanceY(transmittance);
        transmittanceView_->setData(transmittance, ParametersCfg::elementDataC);
    }
    if(!minTrans.empty() && !maxTrans.empty()) {
        transmittanceView_->setData(minTrans, ParametersCfg::elementDataB);
        transmittanceView_->setData(maxTrans, ParametersCfg::elementDataB);
    }
    if(ind->xyComponents() && !minTransY.empty() && !maxTransY.empty()) {
        transmittanceView_->setData(minTransY, ParametersCfg::elementDataD);
        transmittanceView_->setData(maxTransY, ParametersCfg::elementDataD);
    }
    transmittanceView_->setAxisInfo(dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                                    dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                                    "GHz", 0.0, 1.0, "");
    phaseShiftView_->setData(ind->phaseShift(), ParametersCfg::elementDataA, true);
    if(ind->xyComponents()) {
        phaseShiftView_->setData(ind->phaseShiftY(), ParametersCfg::elementDataC);
    }
    if(!minPhase.empty() && !maxPhase.empty()) {
        phaseShiftView_->setData(minPhase, ParametersCfg::elementDataB);
        phaseShiftView_->setData(maxPhase, ParametersCfg::elementDataB);
    }
    if(ind->xyComponents() && !minPhaseY.empty() && !maxPhaseY.empty()) {
        phaseShiftView_->setData(minPhaseY, ParametersCfg::elementDataD);
        phaseShiftView_->setData(maxPhaseY, ParametersCfg::elementDataD);
    }
    phaseShiftView_->setAxisInfo(dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                                 dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                                 "GHz", -180.0, 180.0, "deg");
    double miny = 0.0;
    double maxy = 0.0;
    for(auto d : ind->timeSeries()) {
        miny = std::min(miny, d);
        maxy = std::max(maxy, d);
    }
    for(auto d : ind->reference()) {
        miny = std::min(miny, d);
        maxy = std::max(maxy, d);
    }
    twoDSliceView_->setData(ind->twoDSlice(),
            static_cast<int>(ind->twoDSliceSizeX()),
            static_cast<int>(ind->twoDSliceSizeY()));
#endif
    // Discard existing data displays
    for(auto& w : dataItems_) {
        delete w;
    }
    dataItems_.clear();
    // Create new data displays
    for(auto& d : ind->fitnessData()) {
        auto widget = dataWidgetFactory_.make(this, d.get());
        dataItems_.push_back(widget);
        dataLayout_->addWidget(widget);
        widget->initialise();
    }
}

// Fill the list box with the population details
void GeneticSearchPopulationDlg::fillPopulationList() {
    InitialisingGui::Set s(initialising_);
    generationNumberEdit_->clear();
    minFitnessEdit_->clear();
    maxFitnessEdit_->clear();
    meanFitnessEdit_->clear();
    populationTree_->clear();
    generationNumberEdit_->setText(QString::number(dlg_->sequencer()->generationNumber()));
    double min, max, mean;
    dlg_->sequencer()->population()->calculateStats(&min, &max, &mean);
    minFitnessEdit_->setText(QString::number(min));
    maxFitnessEdit_->setText(QString::number(max));
    meanFitnessEdit_->setText(QString::number(mean));
    for(auto& i : dlg_->sequencer()->population()->individuals()) {
        addIndividualToList(i);
    }
    //unfitnessProgressView_->setData(dlg_->sequencer()->unfitnessProgress());
}

// Add an individual to the population tree
void GeneticSearchPopulationDlg::addIndividualToList(
        const std::shared_ptr<gs::Individual>& ind,
        QTreeWidgetItem* item) {
    if(item == nullptr) {
        item = new QTreeWidgetItem(populationTree_);
    }
    item->setText(columnId, QString::number(ind->id()));
    switch(ind->state()) {
        case gs::Individual::stateWaiting:
            item->setText(columnState, "Waiting");
            item->setText(columnFitness, "");
            break;
        case gs::Individual::stateRunning:
            item->setText(columnState, "Running");
            item->setText(columnFitness, "");
            break;
        case gs::Individual::stateEvaluated:
            item->setText(columnState, "Evaluated");
            item->setText(columnFitness, QString::number(ind->unfitness()));
            break;
    }
    item->setText(columnMother, QString::number(ind->mother()));
    item->setText(columnFather, QString::number(ind->father()));
    item->setText(columnRepeatCell,
                  QString::number(ind->getRepeatCell(
                          dlg_->sequencer()->repeatCell())));
}

// The generation number has been changed
void GeneticSearchPopulationDlg::onGenerationNumber() {
    if(dlg_->dlg()->model()->isStopped() && !initialising_) {
        int requiredGeneration = generationNumberEdit_->text().toInt();
        if(requiredGeneration != dlg_->sequencer()->generationNumber()) {
            dlg_->dlg()->model()->setStopped();
            dlg_->sequencer()->populationLoaded(false);
            dlg_->sequencer()->readHistory(requiredGeneration);
            initialise();
        }
    }
}

// The step generation back button has been pressed
void GeneticSearchPopulationDlg::onStepBack() {
    if(dlg_->dlg()->model()->isStopped() && dlg_->sequencer()->generationNumber() > 0) {
        dlg_->dlg()->model()->setStopped();
        dlg_->sequencer()->populationLoaded(false);
        int requiredGeneration = dlg_->sequencer()->generationNumber() - 1;
        dlg_->sequencer()->readHistory(requiredGeneration);
        initialise();
    }
}

// The step generation forward button has been pressed
void GeneticSearchPopulationDlg::onStepForward() {
    if(dlg_->dlg()->model()->isStopped()) {
        dlg_->dlg()->model()->setStopped();
        dlg_->sequencer()->populationLoaded(false);
        int requiredGeneration = dlg_->sequencer()->generationNumber() + 1;
        dlg_->sequencer()->readHistory(requiredGeneration);
        initialise();
    }
}

// The first generation button has been pressed
void GeneticSearchPopulationDlg::onFirst() {
    if(dlg_->dlg()->model()->isStopped()) {
        dlg_->dlg()->model()->setStopped();
        dlg_->sequencer()->populationLoaded(false);
        dlg_->sequencer()->readHistory(0);
        initialise();
    }
}

// The last generation button has been pressed
void GeneticSearchPopulationDlg::onLast() {
    if(dlg_->dlg()->model()->isStopped()) {
        dlg_->dlg()->model()->setStopped();
        dlg_->sequencer()->populationLoaded(true);
        dlg_->sequencer()->readHistory(INT_MAX);
        initialise();
    }
}

// Show the context menu for the population tree
void GeneticSearchPopulationDlg::onPopulationContext(const QPoint& pos) {
    QMenu menu;
    QAction* copyToClipboard = menu.addAction("Copy to clipboard");
    QAction* initImprovementNxN = menu.addAction(
            "Initialise improvement search with NxN shapes");
    QAction* recalcUnfitness = menu.addAction("Recalculate unfitness");
    QAction* edit = menu.addAction("Edit");
    QAction* selectedAction = menu.exec(populationTree_->mapToGlobal(pos));
    if(selectedAction == copyToClipboard) {
        onCopyToClipboard();
    } else if(selectedAction == initImprovementNxN) {
        onInitImprovement();
    } else if(selectedAction == recalcUnfitness) {
        onRecalculateUnfitness();
    } else if(selectedAction == edit) {
        onEditIndividual();
    }
}

// Copy the current individual to the clipboard
void GeneticSearchPopulationDlg::onCopyToClipboard() {
    // Get the individual
    QTreeWidgetItem* item = populationTree_->currentItem();
    std::shared_ptr<gs::Individual> ind = nullptr;
    if(item != nullptr) {
        int id = item->text(columnId).toInt();
        ind = dlg_->sequencer()->population()->getIndividual(id);
        if(ind != nullptr) {
            // Encode the individual into XML
            auto* o = new xml::DomObject("individual");
            *o << *ind;
            xml::DomDocument dom(o);
            xml::Writer writer;
            std::string text;
            writer.writeString(&dom, text);
            // Copy the individual's data to the clipboard
            QClipboard* clipboard = QApplication::clipboard();
            auto mimeData = new QMimeData;
            QByteArray data(text.c_str());
            mimeData->setData("text/plain", data);
            mimeData->setData("fdtdlife/individual", data);
            clipboard->setMimeData(mimeData);
        }
    }
}

// Initialise the model for a genetic search using the current individual
// as a starting point after converting any admittance layers.
void GeneticSearchPopulationDlg::onInitImprovement() {
    // Get the individual
    QTreeWidgetItem* item = populationTree_->currentItem();
    std::shared_ptr<gs::Individual> ind = nullptr;
    if(item != nullptr) {
        int id = item->text(columnId).toInt();
        ind = dlg_->sequencer()->population()->getIndividual(id);
        if(ind != nullptr) {
            // Modify the gene template, changing admittance layers
            for(auto& t : dlg_->sequencer()->geneFactory()->templates()) {
                switch(t->geneType()) {
                    case gs::GeneTemplate::geneTypeAdmittanceCatLayer: {
                        t->set(gs::GeneTemplate::geneTypeBinaryPlateNxN,
                               true,
                               static_cast<int>(
                                       dlg_->sequencer()->m()->p()->geometry()->n().x() /
                                       2U),
                               0, 1, {});
                        break;
                    }
                    default:
                        break;
                }
            }
            // Set the default repeat cell and layer spacing in case we want those fixed
            for(auto& gene : ind->chromosome()->genes()) {
                auto* repeatGene = dynamic_cast<gs::RepeatCellGene*>(gene);
                if(repeatGene != nullptr) {
                    dlg_->sequencer()->repeatCell(repeatGene->repeatCell());
                    break;
                }
            }
            for(auto& gene : ind->chromosome()->genes()) {
                auto* spacingGene = dynamic_cast<gs::LayerSpacingGene*>(gene);
                if(spacingGene != nullptr) {
                    dlg_->sequencer()->layerSpacing(spacingGene->layerSpacing());
                    break;
                }
            }
            // Regenerate the population
            dlg_->sequencer()->resetPopulation();
            // Fix up new individual 0 to be the same as the selected individual
            std::shared_ptr<gs::Individual> newInd =
                    dlg_->sequencer()->population()->getIndividual(0);
            auto indGenePos = ind->chromosome()->genes().begin();
            auto newIndGenePos = newInd->chromosome()->genes().begin();
            while(indGenePos != ind->chromosome()->genes().end() &&
                  newIndGenePos != newInd->chromosome()->genes().end()) {
                gs::Gene* oldGene = *indGenePos;
                gs::Gene* newGene = *newIndGenePos;
                newGene->copyFrom(oldGene, dlg_->sequencer()->m());
                indGenePos++;
                newIndGenePos++;
            }
            newInd->encodeGenes(dlg_->sequencer());
            // Refill the population dialog
            fillPopulationList();
        }
    }
}

// The given individual needs updating in the list.
void GeneticSearchPopulationDlg::updateIndividual(const std::shared_ptr<gs::Individual>& ind) {
    // Find the individual in the list
    QTreeWidgetItem* item = nullptr;
    for(int i = 0; i < populationTree_->topLevelItemCount(); i++) {
        QTreeWidgetItem* thisItem = populationTree_->topLevelItem(i);
        int id = thisItem->text(columnId).toInt();
        if(id == ind->id()) {
            item = thisItem;
        }
    }
    // Update it in the tree widget
    if(item != nullptr) {
        addIndividualToList(ind, item);
    }
}

// Handle a configuration change notification
void GeneticSearchPopulationDlg::notify(fdtd::Notifier* source, int why,
                                        fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        if(why == FdTdLife::notifySequencerChange) {
            fillPopulationList();
        }
    }
}

// Recalculate the unfitness of the selected individual
void GeneticSearchPopulationDlg::onRecalculateUnfitness() {
    // Get the individual
    QTreeWidgetItem* item = populationTree_->currentItem();
    std::shared_ptr<gs::Individual> ind = nullptr;
    if(item != nullptr) {
        int id = item->text(columnId).toInt();
        ind = dlg_->sequencer()->population()->getIndividual(id);
        if(ind != nullptr) {
            ind->evaluateFitness(dlg_->sequencer()->fitness());
            item->setText(columnFitness, QString::number(ind->unfitness()));
        }
    }
}

// Edit the currently selected individual
void GeneticSearchPopulationDlg::onEditIndividual() {
    std::shared_ptr<gs::Individual> ind = currentIndividual();
    if(ind != nullptr) {
        ind->decodeGenes(dlg_->sequencer());
        IndividualEditDlg box(this, dlg_->sequencer(), ind);
        if(box.exec() == QDialog::Accepted) {
            ind->encodeGenes(dlg_->sequencer());
            updateIndividual(ind);
            initialise();
        }
    }
}

// A command has been clicked
void GeneticSearchPopulationDlg::onCommand(int item) {
    switch(item) {
        case commandSavePopulation:
            dlg_->sequencer()->writePopulation();
            break;
        case commandRebuildHistory:
            dlg_->sequencer()->rebuildHistoryIndex();
            break;
        default:
            break;
    }
}

// Show the context menu for the layer tree
void GeneticSearchPopulationDlg::onLayersContext(const QPoint& pos) {
    QMenu menu;
    QAction* copyToClipboard = menu.addAction("Copy to clipboard");
    QAction* selectedAction = menu.exec(layersList_->mapToGlobal(pos));
    if(selectedAction == copyToClipboard) {
        // Get the selected layer
        QListWidgetItem* item = layersList_->currentItem();
        if(item != nullptr) {
            int layer = layersList_->row(item);
            layersList_->copyToClipboard(layer);
        }
    }
}

