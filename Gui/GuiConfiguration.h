/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GUICONFIGURATION_H
#define FDTDLIFE_GUICONFIGURATION_H

#include "QtIncludes.h"
#include <Fdtd/Configuration.h>
#include <Xml/DomObject.h>

// A class that contains global configuration for the GUI
class GuiConfiguration {
public:
    enum Element {
        elementAxis, elementText,
        elementDataA, elementDataB, elementDataC, elementDataD,
        elementBackground, elementBorder,
        elementMarkerA, elementMarkerB, elementMarkerC, elementMarkerD
    };

protected:
    // Attributes
    fdtd::Configuration* m_;
    bool whiteBackground_;

public:
    GuiConfiguration(fdtd::Configuration* m);
    void clear();
    QColor color(Element);
    void set(bool whiteBackground);
    void writeConfig(xml::DomObject& root) const;
    void readConfig(xml::DomObject& root);
    friend xml::DomObject& operator<<(xml::DomObject& o, const GuiConfiguration& p) {p.writeConfig(o); return o;}
    friend xml::DomObject& operator>>(xml::DomObject& o, GuiConfiguration& p) {p.readConfig(o); return o;}
    // Getters
    fdtd::Configuration::Phase phase() const { return m_->phase(); }
    bool whiteBackground() const { return whiteBackground_; }
    // Setters
};


#endif //FDTDLIFE_GUICONFIGURATION_H
