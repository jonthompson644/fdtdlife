/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Configuration.h>
#include "PhaseShiftView.h"
#include "PhaseShiftViewDlg.h"
#include <Xml/DomObject.h>
#include <Box/Constants.h>

// Constructor
PhaseShiftView::PhaseShiftView(int identifier, int sensorId, const char *sensorData,
                                 ViewFrame *view, int numSeries) :
        DataSeriesView(identifier, sensorId, sensorData, view, numSeries),
        xScaleType_(xScaleTypeFrequency) {

}

// Update the view's configuration
void PhaseShiftView::set(XScaleType xScaleType, double xMin, double xMax, bool xAutomatic,
                         double yMin, double yMax, bool yAutomatic) {
    xScaleType_ = xScaleType;
    DataSeriesView::set(xMin, xMax, xAutomatic, yMin, yMax, yAutomatic);
}

// Convert a data value according to the scale type
double PhaseShiftView::convertXData(double value, XScaleType scaleType) {
    double result = value;
    switch(scaleType) {
        case xScaleTypeFrequency:
            break;
        case xScaleTypeWaveNumber:
            result = value * box::Constants::giga_ / box::Constants::c_ / 100.0;
            break;
    }
    return result;
}

// Calculate the scaling factors for the X axis.
void PhaseShiftView::calcXScaling() {
    // Time (x) axis scaling
    info_.xLeft_ = convertXData(info_.xLeft_, xScaleType_);
    info_.xRight_ = convertXData(info_.xRight_, xScaleType_);
    timeScale_ = (double)dataArea_.width() / (double)(info_.xSize_ - 1);
    // Fix the units
    switch(xScaleType_) {
        case xScaleTypeFrequency:
            info_.xUnits_ = "GHz";
            break;
        case xScaleTypeWaveNumber:
            info_.xUnits_ = "/cm";
            break;
    }
}

// Get the data points for an X coordinate
void PhaseShiftView::getData(int x, std::vector<double>& dy, double& dx) {
    spec_->getData(x, dy, dx);
    dx = convertXData(dx, xScaleType_);
}

// Write the configuration to the DOM object
void PhaseShiftView::writeConfig(xml::DomObject& root) const {
    // Base class first
    DataSeriesView::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("xscaletype") << xScaleType_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void PhaseShiftView::readConfig(xml::DomObject& root) {
    // Base class first
    DataSeriesView::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("xscaletype") >> xScaleType_;
    root >> xml::Close();
}

// Make the sub dialog for this view item
ViewItemDlg *PhaseShiftView::makeDialog(ViewFrameDlg *dlg) {
    return new PhaseShiftViewDlg(dlg, this);
}

// Handle meta data setting
void PhaseShiftView::setMetaData(int id, double d1, double s2) {
    DataSeriesView::setMetaData(id, d1, s2);
    sensor::DataSpec::Info info;
    double freqStepSize;
    switch(id) {
        case sensor::DataSpec::metaFrequency:
            // Change the position of the marker to the given frequency (in GHz)
            spec_->getInfo(&info);
            freqStepSize = (info.xRight_ - info.xLeft_) / info.xSize_;
            markerPos_ = static_cast<int>((d1 - info.xLeft_) / freqStepSize);
            if(markerPos_ < 0) {
                markerPos_ = 0;
            } else if(markerPos_ >= static_cast<int>(info.xSize_)) {
                markerPos_ = static_cast<int>(info.xSize_) - 1;
            }
            break;
        default:
            break;
    }
}

