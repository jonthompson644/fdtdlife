/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlateScannerDlg.h"
#include <Seq/SquarePlateScanner.h>
#include "DialogHelper.h"
#include "CharacterizerParamsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
SquarePlateScannerDlg::SquarePlateScannerDlg(CharacterizerParamsDlg* dlg,
                                                   seq::SquarePlateScanner* scanner) :
        ScannerDlg(dlg, scanner),
        scanner_(scanner) {
    // My controls
    auto* line2Layout = DialogHelper::lineLayout(layout_);
    plateStepSizeEdit_ = DialogHelper::doubleItem(line2Layout, "Plate/Hole Step Size (%)");
    holeCheck_ = DialogHelper::boolItem(line2Layout, "Hole");
    std::tie(positionXEdit_, positionYEdit_, positionZEdit_) =
            DialogHelper::vectorItem(layout_, "Position X,Y,Z (m)");
    auto* line5Layout = DialogHelper::lineLayout(layout_);
    cellSizeEdit_ = DialogHelper::doubleItem(line5Layout, "Cell size (m)");
    layerThicknessEdit_ = DialogHelper::doubleItem(line5Layout, "Layer thickness (m)");
    auto* line6Layout = DialogHelper::lineLayout(layout_);
    repeatXEdit_ = DialogHelper::intItem(line6Layout, "Repeat X");
    repeatYEdit_ = DialogHelper::intItem(line6Layout, "Repeat Y");
    auto* line7Layout = DialogHelper::lineLayout(layout_);
    materialList_ = DialogHelper::dropListItem(line7Layout, "Plate Material");
    fillMaterialList_ = DialogHelper::dropListItem(line7Layout, "Fill Material");
    // Connect handlers
    connect(plateStepSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(holeCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(layerThicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(positionXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(positionYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(positionZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(materialList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(fillMaterialList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    initialise();
}

// Initialise the GUI parameters
void SquarePlateScannerDlg::initialise() {
    ScannerDlg::initialise();
    InitialisingGui::Set s(initialising_);
    plateStepSizeEdit_->setText(QString::number(scanner_->plateStepSize()));
    holeCheck_->setChecked(scanner_->hole());
    layerThicknessEdit_->setText(QString::number(scanner_->layerThickness()));
    positionXEdit_->setText(QString::number(scanner_->position().x()));
    positionYEdit_->setText(QString::number(scanner_->position().y()));
    positionZEdit_->setText(QString::number(scanner_->position().z()));
    cellSizeEdit_->setText(QString::number(scanner_->cellSize()));
    repeatXEdit_->setText(QString::number(scanner_->repeatX()));
    repeatYEdit_->setText(QString::number(scanner_->repeatY()));
    dlg_->model()->fillMaterialList(materialList_, scanner_->material());
    dlg_->model()->fillMaterialList(fillMaterialList_, scanner_->fillMaterial());
    update();
}

// An edit box has changed
void SquarePlateScannerDlg::onChange() {
    ScannerDlg::onChange();
    if(!initialising_) {
        GuiChangeDetector c;
        auto newPlateStepSize = c.testDouble(plateStepSizeEdit_, scanner_->plateStepSize(),
                                              FdTdLife::notifySequencerChange);
        auto newHole = c.testBool(holeCheck_, scanner_->hole(),
                                              FdTdLife::notifySequencerChange);
        auto newLayerThickness = c.testDouble(layerThicknessEdit_, scanner_->layerThickness(),
                                              FdTdLife::notifySequencerChange);
        auto newPosition = c.testVectorDouble(positionXEdit_, positionYEdit_, positionZEdit_,
                                              scanner_->position(), FdTdLife::notifySequencerChange);
        auto newCellSize = c.testDouble(cellSizeEdit_, scanner_->cellSize(),
                                        FdTdLife::notifySequencerChange);
        auto newRepeatX = c.testSizeT(repeatXEdit_, scanner_->repeatX(),
                                      FdTdLife::notifySequencerChange);
        auto newRepeatY = c.testSizeT(repeatYEdit_, scanner_->repeatY(),
                                      FdTdLife::notifySequencerChange);
        int newFillMaterial = dlg_->model()->selected(fillMaterialList_);
        c.externalTest(newFillMaterial != scanner_->fillMaterial(), FdTdLife::notifySequencerChange);
        int newMaterial = dlg_->model()->selected(materialList_);
        c.externalTest(newMaterial != scanner_->material(), FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            scanner_->plateStepSize(newPlateStepSize);
            scanner_->hole(newHole);
            scanner_->layerThickness(newLayerThickness);
            scanner_->position(newPosition);
            scanner_->cellSize(newCellSize);
            scanner_->repeatX(newRepeatX);
            scanner_->repeatY(newRepeatY);
            scanner_->material(newMaterial);
            scanner_->fillMaterial(newFillMaterial);
            // Tell others
            dlg_->model()->doNotification(c.why());
            update();
        }
    }
}
