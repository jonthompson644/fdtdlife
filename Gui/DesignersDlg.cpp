/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "DesignersDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "LensDesignerDlg.h"
#include "NoEditDelegate.h"
#include <Designer/Designer.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>

// Constructor
DesignersDlg::DesignersDlg(FdTdLife* m) :
        MainTabPanel(m) {
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensDesignerDlg>(
            designer::Designers::modeLensDesigner));
    // The parameters
    auto paramLayout = new QHBoxLayout;
    auto designersLayout = new QVBoxLayout;
    paramLayout->addLayout(designersLayout);
    // The sequencers tree
    designersTree_ = new QTreeWidget;
    designersTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    designersTree_->setColumnCount(numDesignerCols);
    designersTree_->setHeaderLabels({"Name", "Type"});
    designersTree_->setIndentation(0);
    designersTree_->setFixedWidth(320);
    designersTree_->setItemDelegateForColumn(designerColType,
                                              new NoEditDelegate(this));
    designersLayout->addWidget(designersTree_);
    // Add any buttons here underneath the sequencers tree
    newButton_ = DialogHelper::buttonMenuItem(designersLayout, "New",
                                              m_->designers().factory().modeNames());
    deleteButton_ = DialogHelper::buttonItem(designersLayout, "Delete");
    copyButton_ = DialogHelper::buttonItem(designersLayout, "Copy to clipboard");
    pasteButton_ = DialogHelper::buttonItem(designersLayout, "Paste from clipboard");
    clearButton_ = DialogHelper::buttonItem(designersLayout,"Clear model");
    designersLayout->addStretch();
    // The place holder for the designer dialog
    designerDlgLayout_ = new QVBoxLayout;
    paramLayout->addLayout(designerDlgLayout_);
    paramLayout->addStretch();
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(designersTree_, SIGNAL(itemSelectionChanged()), SLOT(onDesignerSel()));
    connect(newButton_, SIGNAL(clickedMenu(int)), SLOT(onNewDesigner(int)));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    connect(clearButton_, SIGNAL(clicked()), SLOT(onClear()));
    connect(designersTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onDesignerChanged(QTreeWidgetItem * , int)));
    // Fill the dialog
    DesignersDlg::initialise();
}

// Fill the sequencer tree
void DesignersDlg::fillDesignersTree(designer::Designer* sel) {
    designersTree_->clear();
    QTreeWidgetItem* selItem = nullptr;
    auto* d = m_->designers().designer();
    if(d != nullptr) {
        auto* item = new QTreeWidgetItem(designersTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
        item->setText(designerColName, d->name().c_str());
        item->setText(designerColType,
                      m_->designers().factory().modeName(d->mode()).c_str());
        item->setData(designerColName, Qt::UserRole, d->identifier());
        if(d == sel) {
            selItem = item;
        }
    }
    // Select an item
    if(selItem == nullptr && designersTree_->topLevelItemCount() > 0) {
        selItem = designersTree_->topLevelItem(0);
    }
    if(selItem != nullptr) {
        designersTree_->setCurrentItem(selItem);
    }
    // Show the correct dialog
    showDesignerDlg();
}

// Initialise the dialog
void DesignersDlg::initialise() {
    if(!initialising_) {
        InitialisingGui::Set s(initialising_);
        fillDesignersTree();
    }
}

// Fill the user interface using the selected designer
void DesignersDlg::onDesignerSel() {
    if(!initialising_) {
        InitialisingGui::Set s(initialising_);
        showDesignerDlg();
    }
}

// Show the dialog for the currently selected designer
void DesignersDlg::showDesignerDlg() {
    designer::Designer* d = currentDesigner();
    if(d == nullptr) {
        // Delete any old dialog
        delete designerDlg_;
        designerDlg_ = nullptr;
    } else {
        // Does the dialog need changing
        if(designerDlg_ == nullptr || designerDlg_->item() != d) {
            // Replace any old dialog with the new one
            delete designerDlg_;
            designerDlg_ = dlgFactory_.make(this, d);
            if(designerDlg_ != nullptr) {
                designerDlg_->initialise();
                designerDlgLayout_->addWidget(designerDlg_);
            }
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void DesignersDlg::tabChanged() {
    if(designerDlg_ != nullptr) {
        designerDlg_->tabChanged();
    }
}

// A new designer has been clicked
void DesignersDlg::onNewDesigner(int item) {
    tabChanged();  // Update anything that may have changed for the current item
    designer::Designer* d = m_->designers().make(item);
    if(d != nullptr) {
        InitialisingGui::Set s(initialising_);
        std::stringstream name;
        name << "Designer" << d->identifier();
        d->name(name.str());
        fillDesignersTree(d);
    }
}

// Delete a designer
void DesignersDlg::onDelete() {
    QMessageBox box;
    designer::Designer* d = currentDesigner();
    if(d != nullptr) {
        std::stringstream t;
        t << "OK to delete the designer " << d->name();
        box.setText(t.str().c_str());
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            InitialisingGui::Set s(initialising_);
            m_->designers().remove();
            fillDesignersTree();
        }
    }
}

// Get the currently selected designer
designer::Designer* DesignersDlg::currentDesigner() {
    designer::Designer* result = nullptr;
    auto* item = designersTree_->currentItem();
    if(item != nullptr) {
        result = m_->designers().designer();
    }
    return result;
}

// Copy the current designer to the clipboard
void DesignersDlg::onCopy() {
    designer::Designer* d = currentDesigner();
    if(d != nullptr) {
        // Encode the designer into XML
        auto* o = new xml::DomObject("designer");
        *o << *d;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(text.c_str());
        mimeData->setData("text/plain", data);
        mimeData->setData("fdtdlife/designer", data);
        clipboard->setMimeData(mimeData);
    }
}

// Paste a designer from the clipboard
void DesignersDlg::onPaste() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/designer")) {
        try {
            // Read the XML into a DOM
            std::string text = mimeData->data("fdtdlife/designer").toStdString();
            xml::DomDocument dom("designer");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a sequencer from the DOM
            auto d = m_->designers().restore(*dom.getObject());
            *dom.getObject() >> *d;
            InitialisingGui::Set s(initialising_);
            fillDesignersTree(d);
        } catch(xml::Exception& e) {
            std::cout << "Designer paste failed to read XML: " << e.what() << std::endl;
        }
    }
}

// Clear any model contents created by a designer
void DesignersDlg::onClear() {
    m_->designers().clearModel();
    InitialisingGui::Set s(initialising_);
    m_->doNotification(FdTdLife::notifySequencerChange);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifyDomainSizeChange);
}

// An entry in the tree has been changed
void DesignersDlg::onDesignerChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        // Get the material
        designer::Designer* d = m_->designers().designer();
        if(d != nullptr) {
            switch(column) {
                case designerColName:
                    // Change the name
                    d->name(item->text(designerColName).toStdString());
                    break;
                case designerColType:
                    // Type not changeable
                    break;
                default:
                    break;
            }
            InitialisingGui::Set s(initialising_);
            fillDesignersTree(d);
        }
    }
}

