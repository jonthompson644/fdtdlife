/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCECATALOGUEDLG_H
#define FDTDLIFE_ADMITTANCECATALOGUEDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "DataSeriesWidget.h"
#include <memory>

namespace fdtd {class CatalogueItem;}
class BinaryCodedPlateWidget;

// Main dialog panel for the catalogue of layer patterns with admittances
class AdmittanceCatalogueDlg: public MainTabPanel {
Q_OBJECT
public:
    explicit AdmittanceCatalogueDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;

protected slots:
    void onItemSelected();
    void onCopy();
    void onBrowse();

protected:
    void fillCatTree(const std::shared_ptr<fdtd::CatalogueItem>& sel=nullptr);
    std::shared_ptr<fdtd::CatalogueItem> getCurrentItem();
    void clearItemInfo();

protected:
    enum {catColCoding=0, catColPattern, numCatCols};
    enum {admitColFrequency=0, admitColReal, admitColImaginary, numAdmitCols};
    QPushButton* copyButton_;
    QTreeWidget* catTree_;
    QGroupBox* itemGroup_;
    QLineEdit* codingEdit_;
    QLineEdit* cellSizeEdit_;
    QLineEdit* hexPatternEdit_;
    BinaryCodedPlateWidget* bitPatternEdit_;
    QTreeWidget* admittanceTableTree_;
    DataSeriesWidget* transmittanceView_;
    DataSeriesWidget* phaseShiftView_;
    QLineEdit* fileNameEdit_;
    QPushButton* browseButton_;
};


#endif //FDTDLIFE_ADMITTANCECATALOGUEDLG_H
