/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "AnalysersDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "LensPhaseAnalyserDlg.h"
#include "LensReflectionAnalyserDlg.h"
#include "LensTransmissionAnalyserDlg.h"
#include "TransmittanceAnalyserDlg.h"
#include "PhaseShiftAnalyserDlg.h"
#include "FilterAnalyserDlg.h"
#include "NoEditDelegate.h"
#include <Sensor/Analyser.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>

// Constructor
AnalysersDlg::AnalysersDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m) {
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensPhaseAnalyserDlg>(
            sensor::Analysers::modeLensPhase));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensReflectionAnalyserDlg>(
            sensor::Analysers::modeLensReflection));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensTransmissionAnalyserDlg>(
            sensor::Analysers::modeLensTransmission));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<TransmittanceAnalyserDlg>(
            sensor::Analysers::modeTransmittance));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<PhaseShiftAnalyserDlg>(
            sensor::Analysers::modePhaseShift));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<FilterAnalyserDlg>(
            sensor::Analysers::modeFilter));
    // The parameters
    auto paramLayout = new QHBoxLayout;
    auto analysersLayout = new QVBoxLayout;
    paramLayout->addLayout(analysersLayout);
    // The sequencers tree
    analysersTree_ = new QTreeWidget;
    analysersTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    analysersTree_->setColumnCount(numAnalyserCols);
    analysersTree_->setHeaderLabels({"Name", "Type"});
    analysersTree_->setIndentation(0);
    analysersTree_->setFixedWidth(320);
    analysersTree_->setItemDelegateForColumn(analyserColType,
                                             new NoEditDelegate(this));
    analysersLayout->addWidget(analysersTree_);
    // Add any buttons here underneath the sequencers tree
    newButton_ = DialogHelper::buttonMenuItem(analysersLayout, "New",
                                              m_->analysers().factory().modeNames());
    deleteButton_ = DialogHelper::buttonItem(analysersLayout, "Delete");
    copyButton_ = DialogHelper::buttonItem(analysersLayout, "Copy to clipboard");
    pasteButton_ = DialogHelper::buttonItem(analysersLayout, "Paste from clipboard");
    analysersLayout->addStretch();
    // The place holder for the Analyser dialog
    analyserDlgLayout_ = new QVBoxLayout;
    paramLayout->addLayout(analyserDlgLayout_);
    paramLayout->addStretch();
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(analysersTree_, SIGNAL(itemSelectionChanged()),
            SLOT(onAnalyserSel()));
    connect(newButton_, SIGNAL(clickedMenu(int)), SLOT(onNewAnalyser(int)));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    connect(analysersTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onAnalyserChanged(QTreeWidgetItem * , int)));
    // Fill the dialog
    initialise();
}

// Fill the sequencer tree
void AnalysersDlg::fillAnalysersTree(sensor::Analyser* sel) {
    analysersTree_->clear();
    QTreeWidgetItem* selItem = nullptr;
    for(auto& d : m_->analysers()) {
        auto* item = new QTreeWidgetItem(analysersTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
        item->setText(analyserColName, d->name().c_str());
        item->setText(analyserColType,
                      m_->analysers().factory().modeName(d->mode()).c_str());
        item->setData(analyserColName, Qt::UserRole, d->identifier());
        if(d.get() == sel) {
            selItem = item;
        }
    }
    // Select an item
    if(selItem == nullptr && analysersTree_->topLevelItemCount() > 0) {
        selItem = analysersTree_->topLevelItem(0);
    }
    if(selItem != nullptr) {
        analysersTree_->setCurrentItem(selItem);
    }
    // Show the correct dialog
    showAnalyserDlg();
}

// Initialise the dialog
void AnalysersDlg::initialise() {
    if(!initialising_) {
        InitialisingGui::Set s(initialising_);
        fillAnalysersTree();
    }
}

// Fill the user interface using the selected Analyser
void AnalysersDlg::onAnalyserSel() {
    if(!initialising_) {
        InitialisingGui::Set s(initialising_);
        showAnalyserDlg();
    }
}

// Show the dialog for the currently selected Analyser
void AnalysersDlg::showAnalyserDlg() {
    sensor::Analyser* d = currentAnalyser();
    if(d == nullptr) {
        // Delete any old dialog
        delete analyserDlg_;
        analyserDlg_ = nullptr;
    } else {
        // Does the dialog need changing
        if(analyserDlg_ == nullptr || analyserDlg_->item() != d) {
            // Replace any old dialog with the new one
            delete analyserDlg_;
            analyserDlg_ = dlgFactory_.make(this, d);
            if(analyserDlg_ != nullptr) {
                analyserDlg_->initialise();
                analyserDlgLayout_->addWidget(analyserDlg_);
            }
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void AnalysersDlg::tabChanged() {
    if(analyserDlg_ != nullptr) {
        analyserDlg_->tabChanged();
    }
}

// The user has changed some aspect of the configuration
void AnalysersDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(source == m_) {
        switch(why) {
            case FdTdLife::notifyDomainContentsChange:
            case FdTdLife::notifyAnalyserChange:
                initialise();
                break;
            default:
                break;
        }
    }
}

// A new Analyser has been clicked
void AnalysersDlg::onNewAnalyser(int item) {
    tabChanged();  // Update anything that may have changed for the current item
    sensor::Analyser* d = m_->analysers().make(item);
    if(d != nullptr) {
        InitialisingGui::Set s(initialising_);
        std::stringstream name;
        name << "Analyser" << d->identifier();
        d->name(name.str());
        fillAnalysersTree(d);
        m_->doNotification(FdTdLife::notifyAnalyserChange);
    }
}

// Delete a Analyser
void AnalysersDlg::onDelete() {
    QMessageBox box;
    sensor::Analyser* d = currentAnalyser();
    if(d != nullptr) {
        std::stringstream t;
        t << "OK to delete the Analyser " << d->name();
        box.setText(t.str().c_str());
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            InitialisingGui::Set s(initialising_);
            m_->analysers().remove(d);
            fillAnalysersTree();
            m_->doNotification(FdTdLife::notifyAnalyserChange);
        }
    }
}

// Get the currently selected Analyser
sensor::Analyser* AnalysersDlg::currentAnalyser() {
    sensor::Analyser* result = nullptr;
    auto* item = analysersTree_->currentItem();
    if(item != nullptr) {
        result = m_->analysers().find(item->data(
                analyserColName, Qt::UserRole).toInt());
    }
    return result;
}

// Copy the current Analyser to the clipboard
void AnalysersDlg::onCopy() {
    sensor::Analyser* d = currentAnalyser();
    if(d != nullptr) {
        // Encode the Analyser into XML
        auto* o = new xml::DomObject("analyser");
        *o << *d;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(text.c_str());
        mimeData->setData("text/plain", data);
        mimeData->setData("fdtdlife/analyser", data);
        clipboard->setMimeData(mimeData);
    }
}

// Paste a Analyser from the clipboard
void AnalysersDlg::onPaste() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/analyser")) {
        try {
            // Read the XML into a DOM
            std::string text = mimeData->data("fdtdlife/analyser").toStdString();
            xml::DomDocument dom("analyser");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a sequencer from the DOM
            auto d = m_->analysers().restore(*dom.getObject());
            *dom.getObject() >> *d;
            InitialisingGui::Set s(initialising_);
            fillAnalysersTree(d);
            m_->doNotification(FdTdLife::notifyAnalyserChange);
        } catch(xml::Exception& e) {
            std::cout << "Analyser paste failed to read XML: " << e.what() << std::endl;
        }
    }
}

// An entry in the tree has been changed
void AnalysersDlg::onAnalyserChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        // Get the material
        int identifier = item->data(analyserColName, Qt::UserRole).toInt();
        sensor::Analyser* d = m_->analysers().find(identifier);
        if(d != nullptr) {
            switch(column) {
                case analyserColName:
                    // Change the name
                    d->name(item->text(analyserColName).toStdString());
                    break;
                case analyserColType:
                    // Type not changeable
                    break;
                default:
                    break;
            }
            InitialisingGui::Set s(initialising_);
            fillAnalysersTree(d);
            m_->doNotification(FdTdLife::notifyAnalyserChange);
        }
    }
}

