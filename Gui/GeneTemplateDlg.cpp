/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include <Gs/GeneTemplate.h>

// Constructor
GeneTemplateDlg::GeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg, gs::GeneTemplate* geneTemplate) :
        dlg_(dlg),
        geneTemplate_(geneTemplate) {
    auto paramsLayout = new QVBoxLayout;
    layout_ = new QVBoxLayout;
    setLayout(paramsLayout);
    // The assembly
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(layout_);
    stretchLayout->addStretch();
    paramsLayout->addLayout(stretchLayout);
    // Connect handlers
}

// Initialise the GUI parameters
void GeneTemplateDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
}

// A parameter has changed
void GeneTemplateDlg::onChange() {
    if(!dlg_->initialising()) {
        // Nothing yet
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneTemplateDlg::tabChanged() {
    onChange();
}
