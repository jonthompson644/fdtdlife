/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FITNESSANALYSERDLG_H
#define FDTDLIFE_FITNESSANALYSERDLG_H

#include "QtIncludes.h"
#include <Notifiable.h>
#include "TabbedSubDlg.h"

namespace gs { class FitnessAnalyser; }
namespace gs { class FitnessBase; }
class GeneticSearchTargetDlg;

// A sub base dialog for all the lens reflections fitness functions
class FitnessAnalyserDlg
        : public TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>
          , public fdtd::Notifiable {
    Q_OBJECT
public:
    // Construction
    void construct(GeneticSearchTargetDlg* dlg, gs::FitnessBase* fitnessFunction) override;

    // Overrides of TabbedSubDlg
    void initialise() override;
    void tabChanged() override;

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // Handlers
    void onChange();

protected:
    // Members
    QLineEdit* weightEdit_{};
    QComboBox* analyserList_{};
    gs::FitnessAnalyser* fitnessFunction_{};
};


#endif //FDTDLIFE_FITNESSANALYSERDLG_H
