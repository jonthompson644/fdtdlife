/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VARIABLESDLG_H
#define FDTDLIFE_VARIABLESDLG_H

#include "QtIncludes.h"
#include "FdTdLife.h"
#include "MainTabPanel.h"
#include "Notifiable.h"

// A dialog panel for the global parameters of the paremeterisation system
class VariablesDlg: public MainTabPanel, public fdtd::Notifiable  {
    Q_OBJECT

public:
    // Construction
    explicit VariablesDlg(FdTdLife* m);

    // Overrides of MainTabPanel
    void initialise() override;
    void tabChanged() override;

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // Handlers
    void onNewVariable();
    void onDeleteVariable();
    void onVariableChanged(QTreeWidgetItem* item, int column);
    void onVariableMoved();

protected:
    // Members
    enum {
        variablesColName, variablesColText, variablesColEvaluated, variablesColComment,
        numVariablesCols
    };
    QTreeWidget* variables_{};  // The variables list
    QPushButton* newVariable_ {};  // Add a new variable
    QPushButton* deleteVariable_ {};  // Delete a variable

    // Helpers
    void fillVariablesList(fdtd::Variable* sel = nullptr);
    fdtd::Variable* getCurrentVariable();
};


#endif //FDTDLIFE_VARIABLESDLG_H
