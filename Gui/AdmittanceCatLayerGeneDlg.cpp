/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceCatLayerGeneDlg.h"
#include "DialogHelper.h"
#include <Gs/AdmittanceCatLayerGene.h>
#include "IndividualEditDlg.h"
#include "FdTdLife.h"
#include <AdmittanceCatalogue.h>
#include <CatalogueItem.h>
#include <Gs/GeneticSearch.h>

// Constructor
AdmittanceCatLayerGeneDlg::AdmittanceCatLayerGeneDlg(IndividualEditDlg *mainDlg, gs::AdmittanceCatLayerGene *gene) :
        GeneDlg(mainDlg),
        gene_(gene) {
    auto paramLayout = new QVBoxLayout;
    InitialisingGui::Set s(mainDlg_->initialising());
    // Make the elements
    repeatCountEdit_ = DialogHelper::sizeTItem(paramLayout, "Repeat Count", gene_->repeat());
    codingEdit_ = DialogHelper::textItem(paramLayout, "Catalogue Item Code", QString::number(gene_->coding()));
    gFactorEdit_ = DialogHelper::textItem(paramLayout, "G Factor", QString::number(gene_->gFactor()));
    paramLayout->addStretch();
    setLayout(paramLayout);
}

// Update the individual from the dialog contents
void AdmittanceCatLayerGeneDlg::updateIndividual() {
    if(!mainDlg_->initialising()) {
        gene_->repeat(repeatCountEdit_->text().toULong());
        gene_->coding(codingEdit_->text().toULongLong());
        gene_->gFactor(gFactorEdit_->text().toDouble());
    }
}
