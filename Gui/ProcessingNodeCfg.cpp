/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ProcessingNodeCfg.h"
#include "Node.h"
#include "NodeManager.h"
#include "FdTdLife.h"

// Constructor
ProcessingNodeCfg::ProcessingNodeCfg(int id, const QString& host, FdTdLife *model) :
        model_(model),
        node_(nullptr),
        id_(id),
        host_(host) {
    model->addCfg(this);
}

// Constructor
ProcessingNodeCfg::ProcessingNodeCfg(FdTdLife *model) :
        model_(model),
        node_(nullptr),
        id_(-1) {
    model->addCfg(this);
}

// Constructor
ProcessingNodeCfg::ProcessingNodeCfg(FdTdLife *model, fdtd::Node *node) :
        model_(model),
        node_(node),
        id_(node->id()) {
    model->addCfg(this);
    updateFromModel();
}

// Destructor
ProcessingNodeCfg::~ProcessingNodeCfg() {
    model_->removeCfg(this);
}

// Update the config from the model
void ProcessingNodeCfg::updateFromModel() {
    host_ = node_->host();
}

// Update the model from the config
void ProcessingNodeCfg::updateToModel() {
    node_ = model_->processingMgr()->findNode(id_);
    if(node_ != nullptr) {
        host_ = node_->host();
    }
}

// Set the configuration of the shape
void ProcessingNodeCfg::set(const QString& host) {
    host_ = host;
    updateToModel();
}


