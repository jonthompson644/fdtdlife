/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETICSEARCHPARAMSDLG_H
#define FDTDLIFE_GENETICSEARCHPARAMSDLG_H

#include "QtIncludes.h"
#include "GeneticSearchSubDlg.h"
#include <Notifiable.h>

class GeneticSearchSeqDlg;

// Main tab panel for the sequencers
class GeneticSearchParamsDlg: public GeneticSearchSubDlg, fdtd::Notifiable {
Q_OBJECT
public:
    explicit GeneticSearchParamsDlg(GeneticSearchSeqDlg* dlg);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onChange();

protected:
    QLineEdit* populationSizeEdit_;
    QLineEdit* tournamentSizeEdit_;
    QLineEdit* breedingPoolSizeEdit_;
    QComboBox* selectionModeList_;
    QComboBox* algorithmList_;
    QComboBox* crossoverModeList_;
    QCheckBox* keepParentsCheck_;
    QCheckBox* microGaCheck_;
    QCheckBox* averageSensorCheck_;
    QLineEdit* nCellsXYEdit_;
    QLineEdit* cellSizeZEdit_;
    QLineEdit* sensorDistanceEdit_;
    QComboBox* gridMaterialList_;
    QLineEdit* frequencyRangeEdit_;
    QLineEdit* numFrequenciesEdit_;
    QLineEdit* crossOverProbabilityEdit_;
    QLineEdit* mutationProbabilityEdit_;
    QLineEdit* unfitnessThresholdEdit_;
    QLineEdit* repeatCellEdit_;
    QLineEdit* layerSpacingEdit_;
    QComboBox* layerSymmetryList_;
    QCheckBox* xyComponentsCheck_;
    QCheckBox* useExistingDomainCheck_;
    QCheckBox* collectFieldDataCheck_;

};


#endif //FDTDLIFE_GENETICSEARCHPARAMSDLG_H
