/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceTableEditDlg.h"
#include "DialogHelper.h"
#include <Box/Constants.h>

// Constructor
AdmittanceTableEditDlg::AdmittanceTableEditDlg(QWidget *parent,
                                               const fdtd::AdmittanceData &table) :
        QDialog(parent),
        table_(table) {
    auto mainLayout = new QVBoxLayout;
    auto colLayout = new QHBoxLayout;
    mainLayout->addLayout(colLayout);
    // The admittance table
    auto tableLayout = new QVBoxLayout;
    tableTree_ = new QTreeWidget;
    tableTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    tableTree_->setColumnCount(numColumns);
    tableTree_->setHeaderLabels({"Freq (GHz)", "Real", "Imaginary"});
    tableLayout->addWidget(tableTree_);
    cellSizeEdit_ = DialogHelper::doubleItem(tableLayout, "Cell Size (m)");
    colLayout->addLayout(tableLayout);
    // The buttons
    auto buttonLayout = new QVBoxLayout;
    pasteFrequencyButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Frequency");
    pasteRealButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Real");
    pasteImaginaryButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Imaginary");
    clearAllButton_ = DialogHelper::buttonItem(buttonLayout, "Clear All");
    buttonLayout->addStretch();
    colLayout->addLayout(buttonLayout);
    // The ok/cancel buttons
    buttons_ = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttons_);
    connect(buttons_, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons_, &QDialogButtonBox::rejected, this, &QDialog::reject);
    // Attach layout to the dialog
    mainLayout->addStretch();
    setLayout(mainLayout);
    resize(600,300);
    setWindowTitle("Admittance Table Editor");
    // Initialise the table
    for(const auto& item : table_.admittance()) {
        auto row = new QTreeWidgetItem(tableTree_);
        row->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        row->setText(columnFrequency, QString::number(item.first / box::Constants::giga_));
        row->setText(columnReal, QString::number(item.second.real()));
        row->setText(columnImaginary, QString::number(item.second.imag()));
    }
    cellSizeEdit_->setText(QString::number(table_.cellSize()));
    // Connect handlers
    connect(pasteFrequencyButton_, SIGNAL(clicked()), SLOT(onPasteFrequency()));
    connect(pasteRealButton_, SIGNAL(clicked()), SLOT(onPasteReal()));
    connect(pasteImaginaryButton_, SIGNAL(clicked()), SLOT(onPasteImaginary()));
    connect(clearAllButton_, SIGNAL(clicked()), SLOT(onClearAll()));
    connect(this, SIGNAL(accepted()), SLOT(onAccepted()));
}

// Paste text into the specified column
void AdmittanceTableEditDlg::pasteToColumn(int colNumber) {
    QList<QTreeWidgetItem*> selectedItems = tableTree_->selectedItems();
    int rowNumber = 0;
    if(!selectedItems.empty()) {
        rowNumber = tableTree_->indexOfTopLevelItem(selectedItems.front());
    }
    const QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mimeData = clipboard->mimeData();
    if (mimeData->hasText()) {
        // Split into lines
        QStringList lines = mimeData->text().split('\n');
        for (const auto& line : lines) {
            // Create or find the row
            QTreeWidgetItem* row;
            if(rowNumber >= tableTree_->topLevelItemCount()) {
                row = new QTreeWidgetItem(tableTree_);
                row->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            } else {
                row = tableTree_->topLevelItem(rowNumber);
            }
            // Write the text to the specified column
            row->setText(colNumber, line.trimmed());
            rowNumber++;
        }
    } else {
        std::cout << "Nothing of the right format to paste" << std::endl;
    }
}

// Paste into the frequency column
void AdmittanceTableEditDlg::onPasteFrequency() {
    pasteToColumn(columnFrequency);
}

// Paste into the real column
void AdmittanceTableEditDlg::onPasteReal() {
    pasteToColumn(columnReal);
}

// Paste into the imaginary column
void AdmittanceTableEditDlg::onPasteImaginary() {
    pasteToColumn(columnImaginary);
}

// Clear all the data
void AdmittanceTableEditDlg::onClearAll() {
    tableTree_->clear();
}

// The contents of the dialog have been accepted
void AdmittanceTableEditDlg::onAccepted() {
    table_.clear();
    for(int row=0; row<tableTree_->topLevelItemCount(); row++) {
        QTreeWidgetItem* item = tableTree_->topLevelItem(row);
        double frequency = item->text(columnFrequency).toDouble() * box::Constants::giga_;
        double real = item->text(columnReal).toDouble();
        double imaginary = item->text(columnImaginary).toDouble();
        table_.addPoint(frequency, std::complex<double>(real, imaginary));
    }
    table_.cellSize(cellSizeEdit_->text().toDouble());
}

