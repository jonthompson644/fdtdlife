/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERPHASESHIFTSDLG_H
#define FDTDLIFE_LENSDESIGNERPHASESHIFTSDLG_H

#include "QtIncludes.h"
#include "LensDesignerDlg.h"
#include "DataSeriesWidget.h"

namespace designer { class LensDesigner; }

class LensDesignerPhaseShiftsDlg : public LensDesignerDlg::SubDlg {
Q_OBJECT
public:
    // Construction
    LensDesignerPhaseShiftsDlg(LensDesignerDlg* dlg, designer::LensDesigner* designer);

    // Overrides of LensDesignerSubDlg
    void initialise() override;
    void tabChanged() override { onChange(); }

protected slots:
    // Handlers
    void onChange();
    void onGenerate();

protected:
    // Widgets
    QLineEdit* permittivityAbove_{nullptr};
    QLineEdit* permittivityBelow_{nullptr};
    QLineEdit* permittivityIn_{nullptr};
    QLineEdit* focusDistance_{nullptr};
    QLineEdit* centerFrequency_{nullptr};
    QLineEdit* unitCell_{nullptr};
    QLineEdit* lensDiameter_{nullptr};
    QLineEdit* layerSpacing_{nullptr};
    QLineEdit* numLayers_{nullptr};
    QLineEdit* layersPerArcStage_{nullptr};
    QLineEdit* fdtdCellsPerUnitCell_{nullptr};
    QLineEdit* edgeDistance_{nullptr};
    QComboBox* modelling_{nullptr};
    QLineEdit* focalLengthFudgeFactor_{nullptr};
    QLineEdit* plateThickness_{nullptr};
    QLineEdit* errors_{nullptr};
    QPushButton* generate_{nullptr};
    QSpinBox* numArcStages_{nullptr};
    QSpinBox* sliceThickness_{nullptr};
    QLineEdit* bitsPerHalfSide_{nullptr};
    QLineEdit* minFeatureSize_{nullptr};
};

#endif //FDTDLIFE_LENSDESIGNERPHASESHIFTSDLG_H
