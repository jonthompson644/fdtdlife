/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include "ViewFrame.h"
#include "FdTdLife.h"
#include "VideoEncoder.h"
#include "ViewItem.h"
#include "DataSeriesView.h"
#include "TwoDSliceView.h"
#include "AttenuationView.h"
#include "PowerView.h"
#include "SensorArrayView.h"
#include "PhaseShiftView.h"
#include "MaterialSensorView.h"
#include <Xml/DomObject.h>
#include <Xml/Exception.h>

// Constructor
ViewFrame::ViewFrame(xml::DomObject* root, FdTdLife* model) :
        QDialog(model, Qt::Window),
        Notifiable(model),
        model_(model) {
    try {
        *root >> xml::Obj("identifier") >> identifier_;
    } catch (xml::Exception&) {
        identifier_ = model_->allocViewIdentifier();
    }
    construct();
}

// Constructor
ViewFrame::ViewFrame(int identifier, const std::string& name, FdTdLife* model) :
        QDialog(model, Qt::Window),
        Notifiable(model),
        model_(model),
        identifier_(identifier),
        name_(name) {
    construct();
}

// Common stuff for the constructors
void ViewFrame::construct() {
    makeVideo_ = false;
    displayAllFrames_ = true;
    frameDisplayPeriod_ = 200;
    videoEncoder_ = nullptr;
    videoFrame_ = nullptr;
    model_->addCfg(this);
    frameWidth_ = 300;
    frameHeight_ = 300;
    setWindowTitle(name_.c_str());
    resize(frameWidth_, frameHeight_);
    show();
    raise();
    lastFrameTime_.start();
}

// Destructor
ViewFrame::~ViewFrame() {
    while(!items_.empty()) {
        delete items_.front();
    }
    model_->removeCfg(this);
}

// Write the config to the DOM object
void ViewFrame::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("makevideo") << makeVideo_;
    root << xml::Obj("videofilename") << videoFilename_;
    root << xml::Obj("displayallframes") << displayAllFrames_;
    root << xml::Obj("framedisplayperiod") << frameDisplayPeriod_;
    root << xml::Obj("width") << frameWidth_;
    root << xml::Obj("height") << frameHeight_;
    for(auto item : items_) {
        root << xml::Obj("item") << *item;
    }
    root << xml::Close();
}

// Read the config from the DOM object
void ViewFrame::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("makevideo") >> xml::Default(false) >> makeVideo_;
    root >> xml::Obj("videofilename") >> xml::Default("") >> videoFilename_;
    root >> xml::Obj("displayallframes") >> xml::Default(true) >> displayAllFrames_;
    root >> xml::Obj("framedisplayperiod") >> xml::Default(0) >> frameDisplayPeriod_;
    root >> xml::Obj("width") >> xml::Default(frameWidth_) >> frameWidth_;
    root >> xml::Obj("height") >> xml::Default(frameHeight_) >> frameHeight_;
    for(auto& o : root.obj("item")) {
        // Read the item identification
        int sensorId = 0;
        std::string sensorData;
        // Legacy values
        *o >> xml::Obj("sensor") >> xml::Default(sensorId) >> sensorId;
        *o >> xml::Obj("data") >> xml::Default(sensorData) >> sensorData;
        *o >> xml::Attr("sensor") >> xml::Default(sensorId) >> sensorId;
        *o >> xml::Attr("data") >> xml::Default(sensorData) >> sensorData;
        *o >> xml::Obj("sensorId") >> xml::Default(sensorId) >> sensorId;
        // Get the sensorId spec
        sensor::DataSpec* spec = model_->sensors().getDataSpec(sensorId, sensorData);
        if(spec != nullptr) {
            auto item = createViewItem(spec);
            if(item != nullptr) {
                *o >> *item;
            }
        }
    }
    setWindowTitle(name_.c_str());
    resize(frameWidth_, frameHeight_);
    root >> xml::Close();
}

// Create a view item using a data specification
ViewItem* ViewFrame::createViewItem(sensor::DataSpec* spec) {
    ViewItem* result = nullptr;
    switch(spec->mode()) {
        case sensor::DataSpec::modeNone:
            break;
        case sensor::DataSpec::modeTimeSeries:
            result = new DataSeriesView(0, spec->sensor()->identifier(),
                               spec->name().c_str(), this, spec->numChannels());
            break;
        case sensor::DataSpec::modeAttenuation:
            result = new AttenuationView(0, spec->sensor()->identifier(),
                                         spec->name().c_str(), this, spec->numChannels());
            break;
        case sensor::DataSpec::modePower:
            result = new PowerView(0, spec->sensor()->identifier(),
                                         spec->name().c_str(), this, spec->numChannels());
            break;
        case sensor::DataSpec::modePhaseShift:
            result = new PhaseShiftView(0, spec->sensor()->identifier(),
                                         spec->name().c_str(), this, spec->numChannels());
            break;
        case sensor::DataSpec::modeTwoD:
            result = new TwoDSliceView(0, spec->sensor()->identifier(),
                                       spec->name().c_str(), this);
            break;
        case sensor::DataSpec::modeMaterial:
            result = new MaterialSensorView(0, spec->sensor()->identifier(),
                                       spec->name().c_str(), this);
            break;
        case sensor::DataSpec::modeSensorArray:
            result = new SensorArrayView(0, spec->sensor()->identifier(),
                                       spec->name().c_str(), this);
            break;
     }
    initialise();
    return result;
}

// The window is closing
void ViewFrame::closeEvent(QCloseEvent *event) {
    QDialog::closeEvent(event);
}

// Close an open video
void ViewFrame::closeVideo() {
    if(videoEncoder_ != nullptr) {
        videoEncoder_->closeFile();
        delete videoEncoder_;
        videoEncoder_ = nullptr;
        delete videoFrame_;
        videoFrame_ = nullptr;
    }
}

// Open a video if required.  Returns true if we are making a video
bool ViewFrame::openVideo() {
    int sizeX = (width() + 1) & ~1;  // MPEG frame sizes must be a multiple of 2
    int sizeY = (height() + 1) & ~1;
    if(makeVideo_ && videoEncoder_ == nullptr) {
        videoFrame_ = new QImage(sizeX, sizeY, QImage::Format_RGB32);
        videoEncoder_ = new VideoEncoder;
        videoEncoder_->openFile(videoFilename_.c_str(), videoFrame_->width(),
                                videoFrame_->height());
    }
    return videoEncoder_ != nullptr;
}

// Set the configuration
void ViewFrame::set(const std::string& name, bool makeVideo, const std::string& videoFilename,
                    bool displayAllFrames, int frameDisplayPeriod) {
    name_ = name;
    makeVideo_ = makeVideo;
    videoFilename_ = videoFilename;
    displayAllFrames_ = displayAllFrames;
    frameDisplayPeriod_ = frameDisplayPeriod;
    setWindowTitle(name_.c_str());
}

// The model data has changed and the views need updating
void ViewFrame::updateViews() {
    if(displayAllFrames_ || lastFrameTime_.elapsed() >= frameDisplayPeriod_) {
        lastFrameTime_.restart();
        // We need to make the sensors calculate
        model_->sensors().calculate();
        model_->analysers().calculate();
        // Cause screen views to update when they are ready
        update();
        // Write a video frame
        if(openVideo()) {
            QPainter painter(videoFrame_);
            doPaint(&painter);
            videoEncoder_->addFrame(videoFrame_);
        }
    }
}

// Handle a notification
void ViewFrame::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(model_ == source) {
        switch(why) {
            case fdtd::Model::notifySteppedH:
            case FdTdLife::notifyInitialisation:
            case FdTdLife::notifyDataRead:
                updateViews();
                break;
            case FdTdLife::notifyMinorNoViews:
            case FdTdLife::notifyAnalysersCalculate:
                break;
            default:
                updateViews();
                break;
        }
    }
}

// Notify the model
void ViewFrame::notifyModel(bool noViews) {
    model_->doNotification(noViews ? FdTdLife::notifyMinorNoViews : FdTdLife::notifyMinorChange);
}

// Initialise the views
void ViewFrame::initialise() {
    // Sort the items into horizontal and vertical orientations
    std::list<ViewItem*> horzItems;
    std::list<ViewItem*> vertItems;
    for(auto item : items_) {
        QSize size = item->sizeHint();
        if(size.height() > size.width()) {
            vertItems.push_back(item);
        } else {
            horzItems.push_back(item);
        }
    }
    // Determine the horizontal and vertical split pos
    int splitAt = 0;
    if(!vertItems.empty()) {
        if(horzItems.empty()) {
            splitAt = QDialog::width();
        } else {
            splitAt = QDialog::width() / 2;
        }
    }
    // Now allocate vertical items first
    if(!vertItems.empty()) {
        int width = splitAt / (int)vertItems.size();
        int height = QDialog::height();
        int left = 0;
        for(auto item : vertItems) {
            // Offer the size to the item
            QSize size(width, height);
            item->sizeRequest(size);
            // How much width can we grant?
            size.setWidth(std::min(size.width(), (QDialog::width()-left-40)));
            size.setHeight(height);
            // Grant it
            item->initialise(left, 0, size.width(), size.height());
            left += size.width();
        }
        splitAt = left;
    }
    // Finally allocate the horizontal items
    if(!horzItems.empty()) {
        int width = QDialog::width() - splitAt;
        int height = QDialog::height() / (int)horzItems.size();
        int top = 0;
        for(auto item : horzItems) {
            item->initialise(splitAt, top, width, height);
            top += height;
        }
    }
}

// Add an item to the view
void ViewFrame::add(ViewItem* item) {
    items_.push_back(item);
}

// Remove an item from the view
void ViewFrame::remove(ViewItem* item) {
    items_.remove(item);
}

// Paint the view
void ViewFrame::paintEvent(QPaintEvent* /*event*/) {
    // Draw onto the screen
    QPainter screenPainter(this);
    doPaint(&screenPainter);
}

// Draw into the painter
void ViewFrame::doPaint(QPainter* painter) {
    for(auto item : items_) {
        item->paint(painter);
    }
}

// Find the view item that corresponds to the data specification
ViewItem* ViewFrame::findViewItem(sensor::DataSpec *spec) {
    ViewItem* result = nullptr;
    for(auto item : items_) {
        if(item->sensorId() == spec->sensor()->identifier() &&
                std::string(item->sensorData()) == spec->name()) {
            result = item;
        }
    }
    return result;
}

// The widget has been resized
void ViewFrame::resizeEvent(QResizeEvent* event) {
    frameWidth_ = size().width();
    frameHeight_ = size().height();
    initialise();
    QWidget::resizeEvent(event);
}

// The mouse has been pressed
void ViewFrame::mousePressEvent(QMouseEvent* event) {
    QDialog::mousePressEvent(event);
    // Find the region clicked in
    for(auto item : items_) {
        if(item->dataArea().contains(event->pos())) {
            if(event->button() == Qt::LeftButton) {
                item->leftClick(event->pos());
            }
        }
    }
}

// A sensorId is being added
void ViewFrame::sensorAdded(sensor::Sensor* s) {
    // Tell the items
    for(auto item : items_) {
        item->sensorAdded(s);
    }
}

// A sensorId is being removed
void ViewFrame::sensorRemoved(sensor::Sensor* s) {
    // Tell the items
    for(auto item : items_) {
        item->sensorRemoved(s);
    }
}

// Return the colour to use
QColor ViewFrame::color(GuiConfiguration::Element e) const {
    return model_->guiConfiguration()->color(e);
}

// The context menu is required
void ViewFrame::contextMenuEvent(QContextMenuEvent *event) {
    // Which item have we clicked in?
    ViewItem* clickedItem = nullptr;
    for(auto item : items_) {
        QRect globalZone(mapToGlobal(item->zone().topLeft()),
                         mapToGlobal(item->zone().bottomRight()));
        if(globalZone.contains(event->globalPos(), true)) {
            clickedItem = item;
        }
    }
    // Now show the menu
    if(clickedItem != nullptr) {
        QMenu menu;
        QAction* copyToClipboard = menu.addAction("Copy to clipboard");
        QAction* toggleMarkers = menu.addAction("Toggle markers");
        QAction* selectedAction = menu.exec(event->globalPos());
        if(selectedAction == copyToClipboard) {
            QClipboard* clipboard = QApplication::clipboard();
            auto mimeData = new QMimeData;
            mimeData->setImageData(grab(clickedItem->dataArea()).toImage());
            clickedItem->getMimeData(mimeData);
            clipboard->setMimeData(mimeData);
        } else if(selectedAction == toggleMarkers) {
            clickedItem->toggleMarkers();
            update();
        }
    }
}
