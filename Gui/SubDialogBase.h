/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SUBDIALOGBASE_H
#define FDTDLIFE_SUBDIALOGBASE_H

#include "QtIncludes.h"
#include "InitialisingGui.h"

// A template that acts as the base class for many situations
// where a set of sub-dialogs that handle a set of related
// configuration objects are managed.
// T is the type of the owner dialog class.
// C is the type of the configuration object base class.
template<class T, class C>
class SubDialogBase : public QWidget {
public:
    SubDialogBase() {
        layout_ = new QVBoxLayout;
        auto mainLayout = new QVBoxLayout;
        mainLayout->addLayout(layout_);
        mainLayout->addStretch();
        setLayout(mainLayout);
        mainLayout->setContentsMargins(0,0,0,0);
        mainLayout->setSpacing(4);
    }
    virtual void construct(T* dlg, C* cfg) {
        dlg_ = dlg;
        cfg_ = cfg;
    }
    virtual void initialise() {}
    virtual void tabChanged() { onChange(); }
    InitialisingGui& initialising() { return initialising_; }
    T* dlg() { return dlg_; }
protected slots:
    virtual void onChange() {};
protected:
    InitialisingGui initialising_;
    T* dlg_{};
    C* cfg_{};
    QVBoxLayout* layout_{};
};

#endif //FDTDLIFE_SUBDIALOGBASE_H
