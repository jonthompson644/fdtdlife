/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PROCESSINGNODECFG_H
#define FDTDLIFE_PROCESSINGNODECFG_H

#include "QtIncludes.h"

class FdTdLife;
namespace fdtd {class Node;}

// A class that knows how to serialize the processing node configuration
class ProcessingNodeCfg {
public:
    ProcessingNodeCfg(int id, const QString& host, FdTdLife* model);
    explicit ProcessingNodeCfg(FdTdLife* model);
    ProcessingNodeCfg(FdTdLife* model, fdtd::Node* node);
    ~ProcessingNodeCfg();

protected:
    FdTdLife* model_;
    fdtd::Node* node_;
    int id_;
    QString host_;

public:
    void set(const QString& host);
    void updateFromModel();
    void updateToModel();
    // Getters
    int id() const {return id_;}
    const QString& host() const {return host_;}

};


#endif //FDTDLIFE_PROCESSINGNODECFG_H
