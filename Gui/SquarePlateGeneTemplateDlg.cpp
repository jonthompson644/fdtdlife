/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlateGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
SquarePlateGeneTemplateDlg::SquarePlateGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                       gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    std::tie(plateSizeBitsEdit_, plateSizeMinEdit_, plateSizeMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Plate Size (%):");
    std::tie(repeatBitsEdit_, repeatMinEdit_) =
            DialogHelper::geneBitsSpecIntMinItem(layout_, "Repeat Count:");
    holeList_ = DialogHelper::dropListItem(layout_, "Plate/Hole",
                                           {"Evolve", "Plate", "Hole"});
    connect(plateSizeMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(plateSizeMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(plateSizeBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(holeList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void SquarePlateGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    plateSizeMinEdit_->setText(QString::number(geneTemplate_->range().min()));
    plateSizeMaxEdit_->setText(QString::number(geneTemplate_->range().max()));
    plateSizeBitsEdit_->setText(QString::number(geneTemplate_->range().bits()));
    repeatBitsEdit_->setText(QString::number(geneTemplate_->repeatBits()));
    repeatMinEdit_->setText(QString::number(geneTemplate_->repeatMin()));
    holeList_->setCurrentIndex(static_cast<int>(geneTemplate_->hole().function()));
}

// An edit box has changed
void SquarePlateGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newPlateSize = c.testGeneBitsSpecDouble(
                plateSizeBitsEdit_, plateSizeMinEdit_,
                plateSizeMaxEdit_,
                geneTemplate_->range(), FdTdLife::notifySequencerChange);
        auto newRepeatBits = c.testSizeT(repeatBitsEdit_,
                                         geneTemplate_->repeatBits(),
                                         FdTdLife::notifySequencerChange);
        auto newRepeatMin = c.testInt(repeatMinEdit_,
                                      geneTemplate_->repeatMin(),
                                      FdTdLife::notifySequencerChange);
        auto newFunction = static_cast<gs::GeneBitsSpecBoolean::Function>(c.testInt(
                holeList_,
                static_cast<int>(geneTemplate_->hole().function()),
                FdTdLife::notifySequencerChange));
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->set(geneTemplate_->geneType(),
                               geneTemplate_->fourFoldSymmetry(),
                               geneTemplate_->bitsPerHalfSide(),
                               newRepeatBits, newRepeatMin, {});
            geneTemplate_->range(newPlateSize);
            geneTemplate_->hole(gs::GeneBitsSpecBoolean(newFunction));
            dlg_->model()->doNotification(c.why());
        }
    }
}

