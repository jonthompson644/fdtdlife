/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SpecTableAnalyserDlg.h"
#include "DialogHelper.h"

// Second stage constructor used by factory
void SpecTableAnalyserDlg::construct(AnalysersDlg* dlg, sensor::Analyser* d) {
    AnalysersDlg::SubDlg::construct(dlg, d);
    analyser_ = dynamic_cast<sensor::SpecTableAnalyser*>(d);
}

// Create the controls
void SpecTableAnalyserDlg::createControls() {
    auto pointsLayout = new QHBoxLayout;
    layout_->addLayout(pointsLayout);
    fitnessPointsTree_ = new QTreeWidget;
    fitnessPointsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    fitnessPointsTree_->setColumnCount(numFitnessPointsCols);
    fitnessPointsTree_->setHeaderLabels({"Freq (GHz)", "Minimum", "Maximum"});
    fitnessPointsTree_->setFixedWidth(310);
    pointsLayout->addWidget(fitnessPointsTree_);
    fitnessWidget_ = new FitnessWidget(this);
    pointsLayout->addWidget(fitnessWidget_);
    auto fitnessPointButtons = new QHBoxLayout;
    newFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons, "New Point");
    deleteFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons,
                                                         "Delete Point");
    fitnessPointButtons->addStretch();
    layout_->addLayout(fitnessPointButtons);
    // Connect the handlers
    connect(newFitnessPointButton_, SIGNAL(clicked()),
            SLOT(onNewFitnessPoint()));
    connect(deleteFitnessPointButton_, SIGNAL(clicked()),
            SLOT(onDeleteFitnessPoint()));
    connect(fitnessPointsTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onFitnessPointChanged(QTreeWidgetItem * , int)));
}

// Draw the fitness information on the widget
void SpecTableAnalyserDlg::paintFitnessWidget() {
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    // Black background
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(analyser_ != nullptr) {
        // Determine the scaling factors
        double min = 0.0;
        double max = 0.0;
        double maxFreq = 1 * box::Constants::giga_;
        for(const auto& point : analyser_->points()) {
            maxFreq = std::max(maxFreq, point.x());
            min = std::min(min, point.min());
            max = std::max(max, point.max());
        }
        double scale = (double) (h - 2) / (max - min);
        double scaleFreq = (double) w / (maxFreq - 0.0);
        double offset = min;
        double offsetFreq = 0.0;
        // Line colors
        QPen functionPen(Qt::green);
        QPen annotationPen(Qt::darkGray);
        // Draw the data
        int lastFreq = 0;
        int lastMin = h - 1 -
                      (int) std::floor((0.0 - offset) * scale);
        int lastMax = h - 1 -
                      (int) std::floor((0.0 - offset) * scale);
        for(const auto& point : analyser_->points()) {
            auto nextFreq = (int) std::floor((point.x() - offsetFreq) * scaleFreq);
            int nextMin = h - 1 -
                          (int) std::floor((point.min() - offset) * scale);
            int nextMax = h - 1 -
                          (int) std::floor((point.max() - offset) * scale);
            painter.setPen(functionPen);
            painter.drawLine(lastFreq, lastMin, nextFreq, nextMin);
            painter.drawLine(lastFreq, lastMax, nextFreq, nextMax);
            lastFreq = nextFreq;
            lastMin = nextMin;
            lastMax = nextMax;
        }
    }
    // Clean up the context
    painter.restore();
}

// The new fitness point button has been pressed
void SpecTableAnalyserDlg::onNewFitnessPoint() {
    // Current function
    if(item_ != nullptr) {
        // Add an empty point
        analyser_->addPoint(0.0, 0.0, 0.0);
        fillFitnessPointsTree();
    }
}

// The delete fitness point button has been pressed
void SpecTableAnalyserDlg::onDeleteFitnessPoint() {
    // Current function
    if(item_ != nullptr) {
        // Current point
        QTreeWidgetItem* item = fitnessPointsTree_->currentItem();
        if(item != nullptr) {
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            if(row >= 0) {
                analyser_->deletePoint(static_cast<size_t>(row));
                fillFitnessPointsTree();
            }
        }
    }
}

// Put all the fitness points in the fitness tree widget
void SpecTableAnalyserDlg::fillFitnessPointsTree() {
    InitialisingGui::Set s(initialising_);
    fitnessPointsTree_->clear();
    // Current function
    if(item_ != nullptr) {
        for(auto& point : analyser_->points()) {
            auto item = new QTreeWidgetItem(fitnessPointsTree_);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(fitnessPointsColFreq, QString::number(point.x()
                                                                / box::Constants::giga_));
            item->setText(fitnessPointsColMin, QString::number(point.min()));
            item->setText(fitnessPointsColMax, QString::number(point.max()));
        }
    }
    fitnessWidget_->update();
}

// A fitness tree item has been changed
void SpecTableAnalyserDlg::onFitnessPointChanged(QTreeWidgetItem* item, int /*column*/) {
    if(!initialising_) {
        // Current function
        if(analyser_ != nullptr) {
            // Which row
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            // Set the function point in this row
            double freq = item->text(fitnessPointsColFreq).toDouble()
                          * box::Constants::giga_;
            double min = item->text(fitnessPointsColMin).toDouble();
            double max = item->text(fitnessPointsColMax).toDouble();
            analyser_->setPoint(row, freq, min, max);
            fillFitnessPointsTree();
        }
    }
}
