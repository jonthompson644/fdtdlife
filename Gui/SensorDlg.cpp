/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SensorDlg.h"
#include "SensorsDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Sensor/Sensor.h>

// Constructor
SensorDlg::SensorDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        dlg_(dlg),
        sensor_(sensor) {
    auto paramsLayout = new QVBoxLayout;
    layout_ = new QVBoxLayout;
    setLayout(paramsLayout);
    auto line1Layout = new QHBoxLayout;
    nameEdit_ = DialogHelper::textItem(line1Layout, "Name");
    colorList_ = DialogHelper::dropListItem(line1Layout, "Colour",
                                            box::Constants::colourNames_);
    layout_->addLayout(line1Layout);
    auto line2Layout = new QHBoxLayout;
    saveCsvDataCheck_ = DialogHelper::boolItem(line2Layout,
                                               "Write data to CSV file at run end");
    saveCsvNowButton_ = DialogHelper::buttonItem(line2Layout, "Save CSV file now");
    layout_->addLayout(line2Layout);
    // The group box
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(layout_);
    stretchLayout->addStretch();
    shapeGroup_ = new QGroupBox;
    shapeGroup_->setLayout(stretchLayout);
    paramsLayout->addWidget(shapeGroup_);
    // Connect handlers
    connect(colorList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(nameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(saveCsvDataCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(saveCsvNowButton_, SIGNAL(clicked()), SLOT(onSaveCsvNow()));
}

// Initialise the GUI parameters
void SensorDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    nameEdit_->setText(sensor_->name().c_str());
    colorList_->setCurrentIndex(sensor_->colour());
    saveCsvDataCheck_->setChecked(sensor_->saveCsvData());
}

// Save the sensorId CSV file now
void SensorDlg::onSaveCsvNow() {
    sensor::Sensor* sensor = dlg_->getCurrentSensor();
    dlg_->model()->writeSensorCsvFile(sensor);
}

// A sensorId edit field has changed
void SensorDlg::onChange() {
    if(!dlg_->initialising()) {
        sensor::Sensor* sensor = dlg_->getCurrentSensor();
        if(sensor != nullptr) {
            // Get the changes
            GuiChangeDetector c;
            auto newName = c.testString(nameEdit_, sensor->name(),
                                        FdTdLife::notifySensorChange);
            auto newColor = static_cast<box::Constants::Colour>(c.testInt(
                    colorList_, sensor->colour(),
                    FdTdLife::notifyDomainContentsChange));
            auto newSaveCsvData = c.testBool(saveCsvDataCheck_, sensor->saveCsvData(),
                                             FdTdLife::notifyMinorChange);
            // Make the changes
            if(c.isChanged()) {
                InitialisingGui::Set s(dlg_->initialising());
                sensor->name(newName);
                sensor->colour(newColor);
                sensor->saveCsvData(newSaveCsvData);
                dlg_->updateCurrentSelection();
                dlg_->model()->doNotification(c.why());
            }
        }
    }
}

