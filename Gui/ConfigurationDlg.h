/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CONFIGURATIONDLG_H
#define FDTDLIFE_CONFIGURATIONDLG_H

#include "QtIncludes.h"
#include "FdTdLife.h"
#include "MainTabPanel.h"
#include "Notifiable.h"

class MaterialCfg;

// A dialog panel showing the main model configuration
class ConfigurationDlg: public MainTabPanel, public fdtd::Notifiable {
    Q_OBJECT

public:
    // Construction
    explicit ConfigurationDlg(FdTdLife* m);

    // Overrides of MainTabPanel
    void initialise() override;
    void tabChanged() override;

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // GUI handlers
    void onChange();
    void onAdmittanceEdit();
    void onUpdateTimer();

protected:
    // Configuration
    QCheckBox* doPropagationMatrices_ {};
    QCheckBox* doFdtd_ {};
    QLineEdit* x1Edit_ {};
    QLineEdit* y1Edit_ {};
    QLineEdit* z1Edit_ {};
    QLineEdit* x2Edit_ {};
    QLineEdit* y2Edit_ {};
    QLineEdit* z2Edit_ {};
    QLineEdit* drxEdit_ {};
    QLineEdit* dryEdit_ {};
    QLineEdit* drzEdit_ {};
    QLineEdit* timeSpanEdit_ {};
    QLineEdit* sEdit_ {};
    QLineEdit* scatteredFieldZoneSizeEdit_ {};
    QComboBox* boundaryXList_ {};
    QComboBox* boundaryYList_ {};
    QComboBox* boundaryZList_ {};
    QLineEdit* boundarySizeEdit_ {};
    QLineEdit* numThreadsEdit_ {};
    QLineEdit* animationPeriodEdit_ {};
    QCheckBox* whiteBackgroundCheck_ {};
    QPushButton* editAdmittanceTable_ {};
    QComboBox* pmFormulaList_ {};
    QCheckBox* useThinSheetSubcellsCheck_ {};
    // Information
    QLineEdit* timeStepEdit_ {};
    QLineEdit* ntEdit_ {};
    QLineEdit* dtEdit_ {};
    QLineEdit* nxEdit_ {};
    QLineEdit* nyEdit_ {};
    QLineEdit* nzEdit_ {};
    QLineEdit* memoryEdit_ {};
    QLineEdit* processingTimeEdit_ {};
    QLineEdit* processingRateEdit_ {};
    QLineEdit* maxProcessingRateEdit_ {};
    QLineEdit* usingOpenMpEdit_ {};
    QLineEdit* numThreadsUsedEdit_ {};
    QLineEdit* numThreadsAvailableEdit_ {};
    QTimer* updateTimer_{};
};


#endif //FDTDLIFE_CONFIGURATIONDLG_H
