/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LensDimensionsGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
LensDimensionsGeneTemplateDlg::LensDimensionsGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                             gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    std::tie(unitCellBitsEdit_, unitCellMinEdit_, unitCellMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Unit Cell (m):");
    std::tie(layerSpacingBitsEdit_, layerSpacingMinEdit_, layerSpacingMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Layer Spacing (m):");
    std::tie(numRowsBitsEdit_, numRowsMinEdit_) =
            DialogHelper::geneBitsSpecIntegerItem(layout_, "Num Rows:");
    connect(unitCellBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(unitCellMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(unitCellMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layerSpacingBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layerSpacingMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layerSpacingMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numRowsBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numRowsMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void LensDimensionsGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    unitCellBitsEdit_->setText(QString::number(geneTemplate_->unitCell().bits()));
    unitCellMinEdit_->setText(QString::number(geneTemplate_->unitCell().min()));
    unitCellMaxEdit_->setText(QString::number(geneTemplate_->unitCell().max()));
    layerSpacingBitsEdit_->setText(QString::number(geneTemplate_->layerSpacing().bits()));
    layerSpacingMinEdit_->setText(QString::number(geneTemplate_->layerSpacing().min()));
    layerSpacingMaxEdit_->setText(QString::number(geneTemplate_->layerSpacing().max()));
    numRowsBitsEdit_->setText(QString::number(geneTemplate_->numRows().bits()));
    numRowsMinEdit_->setText(QString::number(geneTemplate_->numRows().min()));
}

// An edit box has changed
void LensDimensionsGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newUnitCell = c.testGeneBitsSpecDouble(unitCellBitsEdit_, unitCellMinEdit_, unitCellMaxEdit_,
                                                    geneTemplate_->unitCell(),
                                                    FdTdLife::notifySequencerChange);
        auto newLayerSpacing = c.testGeneBitsSpecDouble(layerSpacingBitsEdit_, layerSpacingMinEdit_,
                                                        layerSpacingMaxEdit_,
                                                        geneTemplate_->layerSpacing(),
                                                        FdTdLife::notifySequencerChange);
        auto newNumRows = c.testGeneBitsSpecInteger(numRowsBitsEdit_, numRowsMinEdit_,
                                                    geneTemplate_->numRows(),
                                                    FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->unitCell(newUnitCell);
            geneTemplate_->layerSpacing(newLayerSpacing);
            geneTemplate_->numRows(newNumRows);
            dlg_->model()->doNotification(c.why());
        }
    }
}

