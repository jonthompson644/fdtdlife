/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERDLG_H
#define FDTDLIFE_LENSDESIGNERDLG_H

#include "QtIncludes.h"
#include "TabbedSubDlg.h"
#include "DialogFactory.h"
#include "DesignersDlg.h"
#include "TabbedSubDlg.h"

namespace designer { class LensDesigner; }
namespace designer { class Designer; }
class LensDesignerPhaseShiftsDlg;

class LensDesignerResultsDlg;

class LensDesignerColumnsDlg;

class LensDesignerColumnsDlg;

class LensDesignerDlg : public DesignersDlg::SubDlg {
Q_OBJECT
public:
    typedef TabbedSubDlg<LensDesignerDlg, designer::LensDesigner> SubDlg;
    // Construction
    LensDesignerDlg() = default;
    void construct(DesignersDlg* dlg, designer::Designer* d) override;

    // Overrides of SequencerDlg
    void tabChanged() override;
    void initialise() override;

    // Getters
    designer::LensDesigner* lensDesigner() { return designer_; }

protected slots:
    void onTabChange(int index);

protected:
    // Members
    designer::LensDesigner* designer_{};
    // Controls
    QTabWidget* tabs_{};
    LensDesignerPhaseShiftsDlg* phaseShifts_{};
    LensDesignerResultsDlg* results_{};
    LensDesignerColumnsDlg* columns_{};
};


#endif //FDTDLIFE_LENSDESIGNERDLG_H
