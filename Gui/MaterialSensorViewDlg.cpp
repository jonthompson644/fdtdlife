/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "MaterialSensorViewDlg.h"
#include "MaterialSensorView.h"
#include "DialogHelper.h"

// Constructor
MaterialSensorViewDlg::MaterialSensorViewDlg(ViewFrameDlg *dlg, MaterialSensorView *view) :
        ViewItemDlg(dlg),
        view_(view) {
    auto paramLayout = new QVBoxLayout;
    // Parameters
    auto coordLayout = new QHBoxLayout;
    paramLayout->addLayout(coordLayout);
    repeatX_ = DialogHelper::intItem(coordLayout, "Repeat X", view_->repeatX());
    repeatY_ = DialogHelper::intItem(coordLayout, "Repeat Y", view_->repeatY());
    // Main layout
    paramLayout->addStretch();
    setLayout(paramLayout);
    // Connect handlers
    connect(repeatX_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatY_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// The tab has changed and focus has moved from this panel
void MaterialSensorViewDlg::tabChanged() {
    onChange();
}

// An configuration has changed
void MaterialSensorViewDlg::onChange() {
    int newRepeatX = repeatX_->text().toInt();
    int newRepeatY = repeatY_->text().toInt();
    view_->set(newRepeatX, newRepeatY);
}

