/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_JIGSAWADMITTANCESDLG_H
#define FDTDLIFE_JIGSAWADMITTANCESDLG_H

#include "QtIncludes.h"
#include "InitialisingGui.h"
#include <Domain/JigsawLayer.h>

class JigsawAdmittancesDlg : public QDialog {
Q_OBJECT
public:
    // Construction
    JigsawAdmittancesDlg(QWidget* parent, domain::JigsawLayer* shape);

protected:
    // Controls
    enum {
        tablesColName, tablesColUnitCell, tablesColPlate, tablesColFinger1, tablesColFinger2,
        tablesColFinger3, numTablesColumns
    };
    domain::JigsawLayer* shape_;
    QDialogButtonBox* buttons_{};
    QTreeWidget* tablesTree_{};
    QPushButton* addTableButton_{};
    QPushButton* deleteTableButton_{};
    QPushButton* editTableButton_{};
    QPushButton* deleteAllTablesButton_{};
    QPushButton* pasteButton_{};
    QPushButton* copyButton_{};
    QPushButton* fillDownButton_{};
    // Members
    InitialisingGui initialising_{};
    int currentColumn_{-1};

protected:
    // Helpers
    void fillTablesTree(domain::JigsawLayer::ShapeAdmittance* sel = nullptr);
    domain::JigsawLayer::ShapeAdmittance* currentTable();
    void setValue(int row, int column, const std::string& value);

protected slots:
    // Signal handlers
    void onItemChanged(QTreeWidgetItem* item, int column);
    void onItemClicked(QTreeWidgetItem* item, int column);
    void onAddTable();
    void onDeleteTable();
    void onEditTable();
    void onPaste();
    void onCopy();
    void onDeleteAll();
    void onFillDown();
};

#endif //FDTDLIFE_JIGSAWADMITTANCESDLG_H
