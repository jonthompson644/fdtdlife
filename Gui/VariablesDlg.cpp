/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariablesDlg.h"
#include "DialogHelper.h"
#include "NoEditDelegate.h"

// Constructor
VariablesDlg::VariablesDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m) {
    // The widgets
    auto* mainLayout = new QVBoxLayout;
    setTabLayout(mainLayout);
    variables_ = new QTreeWidget;
    variables_->setSelectionMode(QAbstractItemView::SingleSelection);
    variables_->setIndentation(0);
    variables_->setColumnCount(numVariablesCols);
    variables_->setHeaderLabels({"Name", "Expression", "Value", "Comment"});
    variables_->setItemDelegateForColumn(
            variablesColEvaluated, new NoEditDelegate(this));
    variables_->setDragEnabled(true);
    variables_->viewport()->setAcceptDrops(true);
    variables_->setDropIndicatorShown(true);
    variables_->setDragDropMode(QAbstractItemView::InternalMove);
    mainLayout->addWidget(variables_);
    auto buttonLayout = new QHBoxLayout;
    mainLayout->addLayout(buttonLayout);
    newVariable_ = DialogHelper::buttonItem(buttonLayout, "New Variable");
    deleteVariable_ = DialogHelper::buttonItem(buttonLayout, "Delete Variable");
    // Connect handlers
    connect(newVariable_, SIGNAL(clicked()), SLOT(onNewVariable()));
    connect(deleteVariable_, SIGNAL(clicked()), SLOT(onDeleteVariable()));
    connect(variables_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onVariableChanged(QTreeWidgetItem * , int)));
    connect(variables_->model(),
            SIGNAL(rowsInserted(const QModelIndex&, int, int)),
            SLOT(onVariableMoved()));
    initialise();
}

// Initialise the panel
void VariablesDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fillVariablesList();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void VariablesDlg::tabChanged() {
}

// Handle a notification
void VariablesDlg::notify(fdtd::Notifier* source, int why,
                          fdtd::NotificationData* /*data*/) {
    if(source == model() && why == fdtd::Model::notifyVariableChange) {
        if(!initialising_) {
            InitialisingGui::Set s(initialising_);
            fillVariablesList(getCurrentVariable());
        }
    }
}

// Fill the parameters list
void VariablesDlg::fillVariablesList(fdtd::Variable* sel) {
    variables_->clear();
    QTreeWidgetItem* selItem = nullptr;
    box::Expression::Context context;
    for(auto& p : model()->variables()) {
        auto item = new QTreeWidgetItem(variables_);
        if(sel == &p) {
            selItem = item;
        }
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable
                       | Qt::ItemIsDragEnabled);
        item->setText(variablesColName, p.name().c_str());
        item->setText(variablesColText, p.text().c_str());
        item->setText(variablesColEvaluated, p.displayValue(context).c_str());
        item->setText(variablesColComment, p.comment().c_str());
        item->setData(variablesColName, Qt::UserRole, p.identifier());
        p.addToContext(context);
    }
    if(selItem != nullptr) {
        variables_->setCurrentItem(selItem);
    }
}

// Create a new variable
void VariablesDlg::onNewVariable() {
    InitialisingGui::Set s(initialising_);
    fillVariablesList(&model()->variables().add("Unnamed", "1.0"));
    model()->doNotification(fdtd::Model::notifyVariableChange);
}

// Delete the current variable
void VariablesDlg::onDeleteVariable() {
    auto* var = getCurrentVariable();
    if(var != nullptr) {
        model()->variables().remove(var);
        InitialisingGui::Set s(initialising_);
        fillVariablesList();
        model()->doNotification(fdtd::Model::notifyVariableChange);
    }
}

// A cell has been changed
void VariablesDlg::onVariableChanged(QTreeWidgetItem* item, int /*column*/) {
    if(!initialising_) {
        auto* var = model()->variables().find(item->data(
                variablesColName, Qt::UserRole).toInt());
        if(var != nullptr) {
            box::Expression::Context context;
            model()->variables().fillContext(context, var);
            InitialisingGui::Set s(initialising_);
            var->name(item->text(variablesColName).toStdString());
            var->text(item->text(variablesColText).toStdString());
            var->comment(item->text(variablesColComment).toStdString());
            fillVariablesList(getCurrentVariable());
            model()->doNotification(fdtd::Model::notifyVariableChange);
        }
    }
}

// A variable has been moved by the drag/drop
void VariablesDlg::onVariableMoved() {
    if(!initialising_) {
        // Due to the careful use of the initialising object, we should only
        // get here when the drag/drop internal move operation has happened.
        // Use the tree order to re-order the gene templates
        std::list<int> newOrder;
        for(int i = 0; i < variables_->topLevelItemCount(); i++) {
            auto item = variables_->topLevelItem(i);
            int identifier = item->data(variablesColName, Qt::UserRole).toInt();
            newOrder.push_back(identifier);
        }
        model()->variables().reorder(newOrder);
        InitialisingGui::Set s(initialising_);
        fillVariablesList();
    }
}

// Return the currently selected variable, null for none
fdtd::Variable* VariablesDlg::getCurrentVariable() {
    fdtd::Variable* result = nullptr;
    auto item = variables_->currentItem();
    if(item != nullptr) {
        int identifier = item->data(variablesColName, Qt::UserRole).toInt();
        result = model()->variables().find(identifier);
    }
    return result;
}

