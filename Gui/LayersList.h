/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LAYERSLIST_H
#define FDTDLIFE_LAYERSLIST_H

#include "QtIncludes.h"

namespace gs { class GeneticSearch; }
namespace gs { class Gene; }
namespace gs { class BinaryPlateNxNGene; }
namespace gs { class SquarePlateGene; }
namespace gs { class AdmittanceCatLayerGene; }
namespace gs { class DuplicateLayerGene; }
namespace gs { class AdmittanceFnLayerGene; }
namespace gs { class DielectricLayerGene; }

// A class that provides a list of gene layers for the population dialog
class LayersList : public QListWidget {
Q_OBJECT
public:
    explicit LayersList(gs::GeneticSearch* s);
    ~LayersList() override;
    bool insertLayer(gs::Gene* gene, bool doInsert=true);
    void clearLayers();
    void copyToClipboard(int layer);
protected:
    void drawBitPatternIcon(QPainter& painter, size_t repeat, size_t bitsPerHalfSide,
                            const std::vector<uint64_t>& pattern2Fold);
    QListWidgetItem* makeLayer(gs::BinaryPlateNxNGene* a);
    QListWidgetItem* makeLayer(gs::AdmittanceCatLayerGene* a);
    QListWidgetItem* makeLayer(gs::SquarePlateGene* a);
    QListWidgetItem* makeLayer(gs::DielectricLayerGene* a);
    QListWidgetItem* makeLayer(gs::AdmittanceFnLayerGene* a);
protected:
    static constexpr int iconSize_ = 64;
    static constexpr int labelHeight_ = 40;
    static constexpr int bitsPerWord_ = 64;
    std::vector<QIcon*> icons_;
    gs::GeneticSearch* s_;
};


#endif //FDTDLIFE_LAYERSLIST_H
