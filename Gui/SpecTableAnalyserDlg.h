/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SPECTABLEANALYSERDLG_H
#define FDTDLIFE_SPECTABLEANALYSERDLG_H

#include <Sensor/SpecTableAnalyser.h>
#include "QtIncludes.h"
#include "AnalysersDlg.h"
#include "DataSeriesWidget.h"

// Mixer class dialog panel for all analysers that use the spec table
class SpecTableAnalyserDlg : public AnalysersDlg::SubDlg {
Q_OBJECT
public:
    // Types
    class FitnessWidget : public QWidget {
    public:
        explicit FitnessWidget(SpecTableAnalyserDlg* dlg) : dlg_(dlg) {}
        QSize sizeHint() const override {return {200,200};}
        void paintEvent(QPaintEvent* /*event*/) override {dlg_->paintFitnessWidget();}
    protected:
        SpecTableAnalyserDlg* dlg_;
    };

    // Construction
    SpecTableAnalyserDlg() = default;
    void construct(AnalysersDlg* dlg, sensor::Analyser* d) override;

protected slots:
    // Handlers
    void onNewFitnessPoint();
    void onDeleteFitnessPoint();
    void onFitnessPointChanged(QTreeWidgetItem* item, int column);

protected:
    // Members
    sensor::SpecTableAnalyser* analyser_{};
    QTreeWidget* fitnessPointsTree_ {};
    QPushButton* newFitnessPointButton_ {};
    QPushButton* deleteFitnessPointButton_ {};
    FitnessWidget* fitnessWidget_ {};
    enum {fitnessPointsColFreq=0, fitnessPointsColMin, fitnessPointsColMax,
        numFitnessPointsCols};

    // Helpers
    void paintFitnessWidget();
    void fillFitnessPointsTree();
    void createControls();
};

#endif //FDTDLIFE_SPECTABLEANALYSERDLG_H
