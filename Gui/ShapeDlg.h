/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SHAPEDLG_H
#define FDTDLIFE_SHAPEDLG_H

#include "QtIncludes.h"
class ShapesDlg;
namespace domain {class Shape;}

// Base class for the shape sub-dialogs
class ShapeDlg : public QWidget {
    Q_OBJECT

public:
    ShapeDlg() = default;
    virtual void construct(ShapesDlg* dlg, domain::Shape* shape);
    virtual void tabChanged() {}
    virtual void initialise();
    domain::Shape* shape() {return shape_;}

public slots:
    virtual void onChange();

protected:
    ShapesDlg* dlg_{};
    domain::Shape* shape_{};
    QVBoxLayout* layout_{};
    QLineEdit* nameEdit_{};
    QComboBox* materialList_{};
    QCheckBox* ignoreCheck_{};
    QGroupBox* shapeGroup_{};
    QHBoxLayout* topLineLayout_{};
};


#endif //FDTDLIFE_SHAPEDLG_H
