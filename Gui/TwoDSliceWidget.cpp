/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "TwoDSliceWidget.h"
#include "GuiConfiguration.h"
#include <sstream>
#include <GtkGui/TwoDSliceWidget.h>

#include "extKindlmannColors.h"

// Constructor
TwoDSliceWidget::TwoDSliceWidget(GuiConfiguration* cfg) :
        cfg_(cfg) {
}

// Return a hint regarding the size of the widget
QSize TwoDSliceWidget::sizeHint() const {
    return {200, 200};
}

// Paint the data
void TwoDSliceWidget::paintEvent(QPaintEvent* /*event*/) {
    // Draw onto the screen
    QPainter painter(this);
    painter.save();
    painter.setFont(QFont("Arial", 8, QFont::Normal));
    painter.setBrush(cfg_->color(GuiConfiguration::elementBackground));
    painter.setPen(Qt::NoPen);
    painter.drawRect(0, 0, width(), height());
    drawData(&painter);
    painter.restore();
}

// Draw the data, including the marker
void TwoDSliceWidget::drawData(QPainter* painter) {
    // Make the colour map
    std::vector<std::unique_ptr<QBrush>> brushes;
    brushes.resize(NUMEXTKINDLEMANNCOLORS);
    for (int i = 0; i < NUMEXTKINDLEMANNCOLORS; i++) {
        brushes[i].reset(new QBrush(QColor(extKindlemannColors[i].r,
                                        extKindlemannColors[i].g,
                                        extKindlemannColors[i].b)));
    }
    double scale = gain_ / (double) NUMEXTKINDLEMANNCOLORS;
    double offset = 0.0;
    // Draw the data
    painter->save();
    if(xSize_ > 0 && ySize_ > 0) {
        std::unique_ptr<QPixmap> pixmap(new QPixmap(xSize_, ySize_));
        std::unique_ptr<QPainter> pixmapPainter(new QPainter(pixmap.get()));
        pixmapPainter->save();
        for (int i = 0; i < xSize_; i++) {
            for (int j = 0; j < ySize_; j++) {
                int idx = j * xSize_ + i;
                auto b = static_cast<int>(floor((data_.at(idx) - offset) / scale));
                if (b < 0) {
                    b = 0;
                }
                if (b >= static_cast<int>(brushes.size())) {
                    b = brushes.size() - 1;
                }
                pixmapPainter->setBrush(*brushes[b]);
                pixmapPainter->setPen(Qt::NoPen);
                pixmapPainter->drawRect(i, j, 1, 1);
            }
        }
        pixmapPainter->restore();
        painter->drawPixmap(QRect{0,0,width(),height()}, *pixmap, QRect{0,0,xSize_,ySize_});
    }
    painter->restore();
}

// Set the data to display
void TwoDSliceWidget::setData(const std::vector<double>& data, int xSize, int ySize) {
    data_ = data;
    xSize_ = xSize;
    ySize_ = ySize;
    update();
}

// Remove all data from the widget
void TwoDSliceWidget::clear() {
    data_.clear();
    xSize_ = 0;
    ySize_ = 0;
    update();
}

// The context menu is required
void TwoDSliceWidget::contextMenuEvent(QContextMenuEvent* event) {
    QMenu menu;
    QAction* gainUp = menu.addAction("Gain Up");
    QAction* gainDown = menu.addAction("Gain Down");
    QAction* selectedAction = menu.exec(event->globalPos());
    if(selectedAction == gainUp) {
        gain_ *= 2.0;
    } else if(selectedAction == gainDown) {
        gain_ /= 2.0;
    }
    update();
}
