/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueBrowseDlg.h"
#include "BinaryCodedPlateWidget.h"
#include "DataSeriesWidget.h"
#include "DialogHelper.h"
#include "CatalogueBuilderDlg.h"
#include "SequencersDlg.h"
#include "GuiConfiguration.h"
#include <CatalogueItem.h>
#include <CatalogueBuilder.h>

// Constructor
CatalogueBrowseDlg::CatalogueBrowseDlg(QWidget* parent, CatalogueBuilderDlg* dlg) :
        QDialog(parent),
        dlg_(dlg) {
    // Calculate the number of pages
    totalPages_ = ((int) dlg_->sequencer()->items().size()
                   + itemsPerPage_ - 1) / itemsPerPage_;
    // The dialog
    setFixedSize(630, 891);
    setWindowTitle("Catalogue Browser");
    auto* mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    // The print progress
    auto* progressLayout = new QHBoxLayout;
    mainLayout->addLayout(progressLayout);
    fromPageSpin_ = DialogHelper::intSpinItem(progressLayout, "From Page", 1,
                                              totalPages_, 1);
    toPageSpin_ = DialogHelper::intSpinItem(progressLayout, "To Page", 1,
                                            totalPages_, 1);
    goButton_ = DialogHelper::buttonItem(progressLayout, "Write PDF");
    stopButton_ = DialogHelper::buttonItem(progressLayout, "Stop");
    closeButton_ = DialogHelper::buttonItem(progressLayout, "Close");
    stopButton_->setDisabled(true);
    // The print area
    printArea_ = new QWidget;
    mainLayout->addWidget(printArea_);
    auto* printLayout = new QVBoxLayout;
    printArea_->setLayout(printLayout);
    for(int i = 0; i < itemsPerPage_; i++) {
        auto* itemLayout = new QHBoxLayout;
        printLayout->addLayout(itemLayout);
        // The information
        auto* infoCol = new QVBoxLayout;
        itemLayout->addLayout(infoCol);
        codeText_[i] = new QLabel("");
        infoCol->addWidget(codeText_[i]);
        patternText_[i] = new QLabel("");
        infoCol->addWidget(patternText_[i]);
        bitPatternEdit_[i] = new BinaryCodedPlateWidget(
                {0},
                dlg_->sequencer()->bitsPerHalfSide() * 2,
                dlg_->sequencer()->fourFoldSymmetry(),
                true, 64);
        infoCol->addWidget(bitPatternEdit_[i]);
        infoCol->addStretch();
        // The data
        auto* transmittanceLayout = new QVBoxLayout;
        transmittanceView_[i] = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
        transmittanceView_[i]->setAxisInfo(0.0, 100.0, "GHz", 0.0,
                                           1.0, "");
        transmittanceLayout->addWidget(new QLabel("Transmittance"));
        transmittanceLayout->addWidget(transmittanceView_[i]);
        itemLayout->addLayout(transmittanceLayout);
        auto* phaseShiftLayout = new QVBoxLayout;
        phaseShiftView_[i] = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
        phaseShiftView_[i]->setAxisInfo(0.0, 100.0, "GHz", -180.0,
                                        180.0, "deg");
        phaseShiftLayout->addWidget(new QLabel("Phase Shift"));
        phaseShiftLayout->addWidget(phaseShiftView_[i]);
        itemLayout->addLayout(phaseShiftLayout);
    }
    printLayout->addStretch();
    auto* pageLayout = new QHBoxLayout;
    pageLayout->addStretch();
    pageNumberText_ = new QLabel("");
    pageLayout->addWidget(pageNumberText_);
    pageLayout->addStretch();
    printLayout->addLayout(pageLayout);
    // Connect handlers
    connect(goButton_, SIGNAL(clicked()), SLOT(onGo()));
    connect(stopButton_, SIGNAL(clicked()), SLOT(onStop()));
    connect(closeButton_, SIGNAL(clicked()), SLOT(onClose()));
    connect(fromPageSpin_, SIGNAL(valueChanged(int)), SLOT(onFromPage(int)));
    // Initial page
    loadPage(fromPageSpin_->value());
}

// Start the pdf creation
void CatalogueBrowseDlg::onGo() {
    goButton_->setDisabled(true);
    stopButton_->setDisabled(false);
    createPdf();
    goButton_->setDisabled(false);
    stopButton_->setDisabled(true);
    if(closing_) {
        accept();
    }
}

// Cancel the printing
void CatalogueBrowseDlg::onStop() {
    printing_ = false;
}

// Close the dialog
void CatalogueBrowseDlg::onClose() {
    if(printing_) {
        closing_ = true;
        printing_ = false;
    } else {
        accept();
    }
}

// The from page number has changed
void CatalogueBrowseDlg::onFromPage(int page) {
    if(toPageSpin_->value() < page) {
        toPageSpin_->setValue(page);
    }
    toPageSpin_->setMinimum(page);
    loadPage(page);
}

// Create a PDF file of the catalogue
void CatalogueBrowseDlg::loadPage(int page) {
    // Find the start position
    auto pos = dlg_->sequencer()->items().begin();
    int curPage = 1;
    int itemOnPage = 0;
    while(pos != dlg_->sequencer()->items().end() && curPage < page) {
        pos++;
        itemOnPage++;
        if(itemOnPage >= itemsPerPage_) {
            curPage++;
            itemOnPage = 0;
        }
    }
    // Now load a page from here
    itemOnPage = 0;
    while(pos != dlg_->sequencer()->items().end() && itemOnPage < itemsPerPage_) {
        codeText_[itemOnPage]->setText(QString::number(pos->second->code()));
        patternText_[itemOnPage]->setText(QString::number(pos->second->pattern(), 16));
        bitPatternEdit_[itemOnPage]->setData({pos->second->pattern()},
                                             dlg_->sequencer()->bitsPerHalfSide() * 2);
        transmittanceView_[itemOnPage]->setData(pos->second->transmittance(),
                                                GuiConfiguration::elementDataA,
                                                true);
        if(!dlg_->sequencer()->fourFoldSymmetry()) {
            transmittanceView_[itemOnPage]->setData(pos->second->transmittanceY(),
                                                    GuiConfiguration::elementDataC);
        }
        transmittanceView_[itemOnPage]->setAxisInfo(
                dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                "GHz", 0.0, 1.0, "");
        phaseShiftView_[itemOnPage]->setData(pos->second->phaseShift(),
                                             GuiConfiguration::elementDataA,
                                             true);
        if(!dlg_->sequencer()->fourFoldSymmetry()) {
            phaseShiftView_[itemOnPage]->setData(pos->second->phaseShiftY(),
                                                 GuiConfiguration::elementDataC);
        }
        phaseShiftView_[itemOnPage]->setAxisInfo(
                dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
                "GHz", -180.0, 180.0, "deg");
        pos++;
        itemOnPage++;
    }
    pageNumberText_->setText(QString("Page %1 of %2").arg(page).arg(totalPages_));
}

// Create a PDF file of the catalogue
void CatalogueBrowseDlg::createPdf() {
    printing_ = true;
    // The printer
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    std::string fileName = dlg_->dlg()->model()->fileName() + ".pdf";
    printer.setOutputFileName(fileName.c_str());
    printer.setPageMargins(12, 16, 2, 20, QPrinter::Millimeter);
    // The painting context
    QPainter painter(&printer);
    painter.scale(13, 13);
    // For each page...
    int page;
    for(page = fromPageSpin_->value(); page <= toPageSpin_->value() && printing_; page++) {
        loadPage(page);
        update();
        QCoreApplication::processEvents();
        printArea_->render(&painter);
        printer.newPage();
        QCoreApplication::processEvents();
    }
    fromPageSpin_->setValue(page);
    printing_ = false;
}
