/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include "ViewItem.h"
#include "ViewFrame.h"
#include "FdTdLife.h"
#include "ViewItemDlg.h"
#include <Sensor/DataSpec.h>
#include <Sensor/Sensor.h>
#include <Xml/DomObject.h>

// Constructor
ViewItem::ViewItem(int identifier, int sensorId, const char* sensorData,
                   ViewFrame* view) :
        view_(view),
        identifier_(identifier),
        sensorId_(sensorId),
        sensorData_(sensorData),
        spec_(nullptr),
        markersOn_(true) {
    view_->add(this);
}

// Destructor
ViewItem::~ViewItem() {
    if(spec_ != nullptr) {
        spec_->removeUser(this);
    }
    view_->remove(this);
}

// Set the display zone for this view
void ViewItem::initialise(int left, int top, int width, int height) {
    // Record the drawing zone
    zone_ = QRect(left, top, width, height);
    // Get the data spec
    if(spec_ != nullptr) {
        spec_->removeUser(this);
    }
    spec_ = view_->model()->sensors().getDataSpec(sensorId_, sensorData_.c_str());
    if(spec_ != nullptr) {
        spec_->addUser(this);
    }
}

// Paint the item onto the screen
void ViewItem::paint(QPainter* painter) {
    painter->save();
    // Clear the zone
    painter->setPen(QPen(QBrush(Qt::darkYellow), 1));
    painter->setBrush(Qt::black);
    painter->drawRect(zone_);
    // Get the item's title
    title_ = "No Data";
    sensor::DataSpec* spec = view_->model()->sensors().getDataSpec(sensorId_, sensorData_.c_str());
    if(spec != nullptr) {
        title_ = QString("%1 - %2").arg(spec->sensor()->name().c_str()).arg(spec->name().c_str());
    }
    // Now paint it in a bar along the top of the zone
    painter->setFont(QFont("Arial", 10, QFont::Bold));
    QFontInfo info = painter->fontInfo();
    titleBar_ = QRect(zone_.left()+1, zone_.top()+1,
                      zone_.width()-2, info.pixelSize());
    painter->setBrush(Qt::darkYellow);
    painter->setPen(Qt::darkYellow);
    painter->drawRect(titleBar_);
    painter->setPen(QPen(QBrush(Qt::darkBlue), 1));
    painter->drawText(titleBar_, Qt::AlignCenter, title_);
    // Draw the data
    dataArea_ = QRect(zone_.left()+2, zone_.top()+2+titleBar_.height(),
                      zone_.width()-2, zone_.height()-titleBar_.height()-2);
    painter->setFont(QFont("Arial", 8, QFont::Normal));
    painter->setClipRect(dataArea_);
    paintData(painter);
    painter->setClipping(false);
    painter->restore();
}

// A sensorId has been removed
void ViewItem::sensorRemoved(sensor::Sensor* s) {
    if(s->identifier() == sensorId_) {
        if(spec_ != nullptr) {
            spec_->removeUser(this);
        }
        spec_ = nullptr;
        std::cout << "ViewItem: removing " << sensorId_ << ":" << sensorData_ << std::endl;
    }
}

// A sensorId has been added
void ViewItem::sensorAdded(sensor::Sensor* s) {
    if(spec_ != nullptr) {
        spec_->removeUser(this);
    }
    if(s->identifier() == sensorId_) {
        for(auto spec : s->dataSpecs()) {
            if(spec->name() == sensorData_) {
                spec_ = spec;
                std::cout << "ViewItem: adding " << sensorId_ << ":" << sensorData_ << std::endl;
            }
        }
    }
    if(spec_ != nullptr) {
        spec_->addUser(this);
    }
}

// Make a default view item dialog
ViewItemDlg *ViewItem::makeDialog(ViewFrameDlg* dlg) {
    return new ViewItemDlg(dlg);
}

// Write configuration to the DOM object
void ViewItem::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    // This is the item identification so is only written by the object
    root << xml::Attr("sensor") << sensorId_;
    root << xml::Attr("data") << sensorData_;
    root << xml::Close();
}

// Read configuration from the DOM object
void ViewItem::readConfig(xml::DomObject& root) {
    // Nothing to read but we must open and close so that reopen works
    root >> xml::Open();
    root >> xml::Close();
}

// A data specification is begin destroyed
void ViewItem::removeSpec(sensor::DataSpec *spec) {
    if(spec_ == spec) {
        spec_ = nullptr;
    }
}


