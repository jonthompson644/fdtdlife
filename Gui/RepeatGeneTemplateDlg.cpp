/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RepeatGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
RepeatGeneTemplateDlg::RepeatGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg, gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    std::tie(repeatCellBitsEdit_, repeatCellMinEdit_, repeatCellMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Repeat Cell (m):");
    connect(repeatCellMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatCellMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatCellBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void RepeatGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    repeatCellMinEdit_->setText(QString::number(geneTemplate_->range().min()));
    repeatCellMaxEdit_->setText(QString::number(geneTemplate_->range().max()));
    repeatCellBitsEdit_->setText(QString::number(geneTemplate_->range().bits()));
}

// An edit box has changed
void RepeatGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newRepeat = c.testGeneBitsSpecDouble(
                repeatCellBitsEdit_, repeatCellMinEdit_, repeatCellMaxEdit_,
                geneTemplate_->range(), FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->range(newRepeat);
            dlg_->model()->doNotification(c.why());
        }
    }
}

