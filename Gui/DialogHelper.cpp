/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include "DialogHelper.h"
#include "PushButtonMenu.h"

// Constructor
DialogHelper::DialogHelper(QGridLayout* layout) :
        layout_(layout) {

}

// Create an edit item
QLineEdit* DialogHelper::editItem(const QString& value, bool readOnly,
                                  QValidator* validator, int y, int x) {
    auto edit = new QLineEdit(value);
    edit->setDisabled(readOnly);
    edit->setValidator(validator);
    layout_->addWidget(edit, y, x);
    return edit;
}

// Create a double edit field in the dialog
QLineEdit* DialogHelper::field(const QString& name, double value, bool readOnly) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    return editItem(QString::number(value), readOnly, new QDoubleValidator, y, 1);
}

// Create an integer edit field in the dialog
QLineEdit* DialogHelper::field(const QString& name, int value, bool readOnly) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    return editItem(QString::number(value), readOnly, new QIntValidator, y, 1);
}

// Create an unsigned binary integer edit field in the dialog
QLineEdit* DialogHelper::field(const QString& name, uint64_t value, bool readOnly) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    return editItem(QString::number(value, /*base=*/2), readOnly,
                    new QRegExpValidator(QRegExp("[01]+")),
                    y, 1);
}

// Create a string edit field in the dialog
QLineEdit* DialogHelper::field(const QString& name, const QString& value, bool readOnly) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    return editItem(value, readOnly, nullptr, y, 1);
}

// Add a widget to the dialog
void DialogHelper::field(const QString& name, QWidget* w) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    layout_->addWidget(w, y, 1);
}

// Add a layout to the dialog
void DialogHelper::field(const QString& name, QLayout* l) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    layout_->addLayout(l, y, 1);
}

// Add a text row the dialog
void DialogHelper::field(const QString& name) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0, 1, 2);
}

// Add a layout to the dialog
void DialogHelper::field(QLayout* l) {
    layout_->addLayout(l, layout_->rowCount(), 0, 1, 2);
}

// Add a widget to the dialog
void DialogHelper::field(QWidget* w) {
    layout_->addWidget(w, layout_->rowCount(), 0, 1, 2);
}

// Create a check box field in the dialog
QCheckBox* DialogHelper::field(const QString& name, bool value, bool readOnly) {
    int y = layout_->rowCount();
    layout_->addWidget(new QLabel(name), y, 0);
    auto edit = new QCheckBox;
    edit->setDisabled(readOnly);
    edit->setChecked(value);
    layout_->addWidget(edit, y, 1);
    return edit;
}

// Create a label header row
void DialogHelper::header(const QStringList& cols) {
    int y = layout_->rowCount();
    int x = 0;
    for(const auto& col : cols) {
        QLabel* label = new QLabel(col);
        layout_->addWidget(label, y, x++);
    }
}

// Create a row of double edit fields in the dialog
QList<QLineEdit*> DialogHelper::field(const QString& name, const QList<double>& values,
                                      bool readOnly) {
    QList<QLineEdit*> result;
    int y = layout_->rowCount();
    int x = 0;
    layout_->addWidget(new QLabel(name), y, x++);
    for(double value : values) {
        result.push_back(editItem(QString::number(value), readOnly,
                                  new QDoubleValidator, y, x++));
    }
    return result;
}

// Create a row of integer edit fields in the dialog
QList<QLineEdit*> DialogHelper::field(const QString& name, const QList<int>& values,
                                      bool readOnly) {
    QList<QLineEdit*> result;
    int y = layout_->rowCount();
    int x = 0;
    layout_->addWidget(new QLabel(name), y, x++);
    for(int value : values) {
        result.push_back(editItem(QString::number(value), readOnly,
                                  new QIntValidator, y, x++));
    }
    return result;
}

// Create a row of size_t edit fields in the dialog
QList<QLineEdit*> DialogHelper::field(const QString& name, const QList<size_t>& values,
                                      bool readOnly) {
    QList<QLineEdit*> result;
    int y = layout_->rowCount();
    int x = 0;
    layout_->addWidget(new QLabel(name), y, x++);
    for(size_t value : values) {
        result.push_back(editItem(QString::number(value), readOnly,
                                  new QIntValidator, y, x++));
    }
    return result;
}

// Create a line with a writeable field and a read back field
std::tuple<QLineEdit*, QLineEdit*> DialogHelper::field(const QString& name,
                                                       double value, double rbv, bool secondIsRbv) {
    auto layout = new QHBoxLayout;
    auto valEdit = new QLineEdit(QString::number(value));
    valEdit->setValidator(new QDoubleValidator);
    auto rbvEdit = new QLineEdit(QString::number(rbv));
    rbvEdit->setDisabled(secondIsRbv);
    layout->addWidget(valEdit);
    layout->addWidget(rbvEdit);
    field(name, layout);
    return std::make_tuple(valEdit, rbvEdit);
}

// Create a line with a writeable field and a read back field
std::tuple<QLineEdit*, QLineEdit*> DialogHelper::field(const QString& name,
                                                       int value, int rbv) {
    auto layout = new QHBoxLayout;
    auto valEdit = new QLineEdit(QString::number(value));
    valEdit->setValidator(new QIntValidator);
    auto rbvEdit = new QLineEdit(QString::number(rbv));
    rbvEdit->setDisabled(true);
    layout->addWidget(valEdit);
    layout->addWidget(rbvEdit);
    field(name, layout);
    return std::make_tuple(valEdit, rbvEdit);
}

// Create a line with a vector
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::field(
        const QString& name, const box::Vector<double>& value, bool readOnly) {
    auto layout = new QHBoxLayout;
    auto xEdit = new QLineEdit(QString::number(value.x()));
    xEdit->setValidator(new QDoubleValidator);
    xEdit->setDisabled(readOnly);
    layout->addWidget(xEdit);
    auto yEdit = new QLineEdit(QString::number(value.y()));
    yEdit->setValidator(new QDoubleValidator);
    yEdit->setDisabled(readOnly);
    layout->addWidget(yEdit);
    auto zEdit = new QLineEdit(QString::number(value.z()));
    zEdit->setValidator(new QDoubleValidator);
    zEdit->setDisabled(readOnly);
    layout->addWidget(zEdit);
    field(name, layout);
    return std::make_tuple(xEdit, yEdit, zEdit);
}

// Create a line with three related items
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::field(
        const QString& name, double a, double b, double c) {
    auto layout = new QHBoxLayout;
    auto aEdit = new QLineEdit(QString::number(a));
    aEdit->setValidator(new QDoubleValidator);
    layout->addWidget(aEdit);
    auto bEdit = new QLineEdit(QString::number(b));
    bEdit->setValidator(new QDoubleValidator);
    layout->addWidget(bEdit);
    auto cEdit = new QLineEdit(QString::number(c));
    cEdit->setValidator(new QDoubleValidator);
    layout->addWidget(cEdit);
    field(name, layout);
    return std::make_tuple(aEdit, bEdit, cEdit);
}

// Create a boolean item in the given box layout
QCheckBox* DialogHelper::boolItem(QBoxLayout* layout, const QString& name,
                                  bool readOnly) {
    auto checkBox = new QCheckBox;
    checkBox->setDisabled(readOnly);
    auto pair = new QHBoxLayout;
    pair->addWidget(new QLabel(name));
    pair->addWidget(checkBox);
    layout->addLayout(pair);
    return checkBox;
}

// Create a text item in the given box layout
QLineEdit* DialogHelper::textItem(QBoxLayout* layout, const QString& name,
                                  bool readOnly) {
    auto edit = new QLineEdit;
    edit->setReadOnly(readOnly);
    if(!name.isEmpty()) {
        auto pair = new QHBoxLayout;
        pair->addWidget(new QLabel(name));
        pair->addWidget(edit);
        layout->addLayout(pair);
    } else {
        layout->addWidget(edit);
    }
    return edit;
}

// Create a text item in the given box layout
QLineEdit* DialogHelper::textItem(QBoxLayout* layout, const QString& name,
                                  const QString& value, bool readOnly) {
    QLineEdit* edit = textItem(layout, name, readOnly);
    edit->setText(value);
    return edit;
}

// Create a double item in the given box layout
QLineEdit* DialogHelper::doubleItem(QBoxLayout* layout, const QString& name,
                                    bool readOnly) {
    QLineEdit* edit = textItem(layout, name, readOnly);
    edit->setValidator(new QDoubleValidator);
    return edit;
}

// Create a label item in the given box layout
QLabel* DialogHelper::labelItem(QBoxLayout* layout, const QString& name) {
    auto edit = new QLabel;
    if(!name.isEmpty()) {
        auto pair = new QHBoxLayout;
        pair->addWidget(new QLabel(name));
        pair->addWidget(edit);
        layout->addLayout(pair);
    } else {
        layout->addWidget(edit);
    }
    return edit;
}

// Create a double item in the given box layout
QLineEdit* DialogHelper::doubleItem(QBoxLayout* layout, const QString& name,
                                    double value, bool readOnly) {
    QLineEdit* edit = doubleItem(layout, name, readOnly);
    edit->setText(QString::number(value));
    return edit;
}

// Create a double item without a label in the given box layout
QLineEdit* DialogHelper::doubleItem(QBoxLayout* layout, double value, bool readOnly) {
    QLineEdit* edit = textItem(layout, "", readOnly);
    edit->setValidator(new QDoubleValidator);
    edit->setText(QString::number(value));
    return edit;
}

// Create a vector item in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::vectorItem(
        QBoxLayout* layout, const QString& name,
        const box::Vector<double>& value, bool readOnly) {
    auto editx = new QLineEdit;
    editx->setReadOnly(readOnly);
    editx->setText(QString::number(value.x()));
    auto edity = new QLineEdit;
    edity->setReadOnly(readOnly);
    edity->setText(QString::number(value.y()));
    auto editz = new QLineEdit;
    editz->setReadOnly(readOnly);
    editz->setText(QString::number(value.z()));
    auto part = new QHBoxLayout;
    part->addWidget(new QLabel(name));
    part->addWidget(editx);
    part->addWidget(edity);
    part->addWidget(editz);
    layout->addLayout(part);
    return std::make_tuple(editx, edity, editz);
}

// Create a polynomial item in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::polynomialItem(
        QBoxLayout* layout, const QString& name, const box::Polynomial& v, bool readOnly) {
    // The edit boxes
    auto* edita = new QLineEdit;
    edita->setReadOnly(readOnly);
    edita->setValidator(new QDoubleValidator);
    edita->setText(QString::number(v.a()));
    auto* editb = new QLineEdit;
    editb->setReadOnly(readOnly);
    editb->setValidator(new QDoubleValidator);
    editb->setText(QString::number(v.b()));
    auto* editc = new QLineEdit;
    editc->setReadOnly(readOnly);
    editc->setValidator(new QDoubleValidator);
    editc->setText(QString::number(v.c()));
    auto* editd = new QLineEdit;
    editd->setReadOnly(readOnly);
    editd->setValidator(new QDoubleValidator);
    editd->setText(QString::number(v.d()));
    auto* edite = new QLineEdit;
    edite->setReadOnly(readOnly);
    edite->setValidator(new QDoubleValidator);
    edite->setText(QString::number(v.e()));
    auto* editf = new QLineEdit;
    editf->setReadOnly(readOnly);
    editf->setValidator(new QDoubleValidator);
    editf->setText(QString::number(v.f()));
    // The layout
    auto* grid = new QGridLayout;
    auto* laya = new QHBoxLayout;
    laya->addWidget(new QLabel("A"));
    laya->addWidget(edita);
    grid->addLayout(laya, 0, 0);
    auto* layb = new QHBoxLayout;
    layb->addWidget(new QLabel("B"));
    layb->addWidget(editb);
    grid->addLayout(layb, 0, 1);
    auto* layc = new QHBoxLayout;
    layc->addWidget(new QLabel("C"));
    layc->addWidget(editc);
    grid->addLayout(layc, 1, 0);
    auto* layd = new QHBoxLayout;
    layd->addWidget(new QLabel("D"));
    layd->addWidget(editd);
    grid->addLayout(layd, 1, 1);
    auto* laye = new QHBoxLayout;
    laye->addWidget(new QLabel("E"));
    laye->addWidget(edite);
    grid->addLayout(laye, 2, 0);
    auto* layf = new QHBoxLayout;
    layf->addWidget(new QLabel("F"));
    layf->addWidget(editf);
    grid->addLayout(layf, 2, 1);
    auto part = new QHBoxLayout;
    part->addWidget(new QLabel(name));
    part->addLayout(grid);
    layout->addLayout(part);
    // Result
    return std::make_tuple(edita, editb, editc, editd, edite, editf);
}

// Create a polynomial item in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::polynomialItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    return polynomialItem(layout, name, box::Polynomial(), readOnly);
}

// Create a gene bits spec double item in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::geneBitsSpecDoubleItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QDoubleValidator);
    auto editMax = new QLineEdit;
    editMax->setReadOnly(readOnly);
    editMax->setValidator(new QDoubleValidator);
    auto* row1 = new QHBoxLayout;
    row1->addWidget(new QLabel("Bits"));
    row1->addWidget(editBits);
    auto* row2 = new QHBoxLayout;
    row2->addWidget(new QLabel("Min"));
    row2->addWidget(editMin);
    auto* row3 = new QHBoxLayout;
    row3->addWidget(new QLabel("Max"));
    row3->addWidget(editMax);
    auto part = new QGridLayout;
    part->addWidget(new QLabel(name), 0, 0);
    part->addLayout(row1, 0, 1);
    part->addLayout(row2, 1, 1);
    part->addLayout(row3, 2, 1);
    layout->addLayout(part);
    return std::make_tuple(editBits, editMin, editMax);
}

// Create a gene bits spec integer item in the given box layout
std::tuple<QLineEdit*, QLineEdit*> DialogHelper::geneBitsSpecIntegerItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QIntValidator);
    auto* row1 = new QHBoxLayout;
    row1->addWidget(new QLabel("Bits"));
    row1->addWidget(editBits);
    auto* row2 = new QHBoxLayout;
    row2->addWidget(new QLabel("Min"));
    row2->addWidget(editMin);
    auto part = new QGridLayout;
    part->addWidget(new QLabel(name), 0, 0);
    part->addLayout(row1, 0, 1);
    part->addLayout(row2, 1, 1);
    layout->addLayout(part);
    return std::make_tuple(editBits, editMin);
}

// Create a gene bits spec double item
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::geneBitsSpecDoubleItem(
        QGridLayout* layout, const QString& name, int row, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    editBits->setFixedWidth(50);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QDoubleValidator);
    auto editMax = new QLineEdit;
    editMax->setReadOnly(readOnly);
    editMax->setValidator(new QDoubleValidator);
    layout->addWidget(new QLabel(name), row, 0);
    layout->addWidget(editBits, row, 1);
    layout->addWidget(editMin, row, 2);
    layout->addWidget(editMax, row, 3);
    return std::make_tuple(editBits, editMin, editMax);
}

// Create a gene bits spec integer item with only a minimum in the given box layout
std::tuple<QLineEdit*, QLineEdit*> DialogHelper::geneBitsSpecIntMinItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QIntValidator);
    auto* row1 = new QHBoxLayout;
    row1->addWidget(new QLabel("Bits"));
    row1->addWidget(editBits);
    auto* row2 = new QHBoxLayout;
    row2->addWidget(new QLabel("Min"));
    row2->addWidget(editMin);
    auto* part = new QGridLayout;
    part->addWidget(new QLabel(name), 0, 0);
    part->addLayout(row1, 0, 1);
    part->addLayout(row2, 1, 1);
    layout->addLayout(part);
    return std::make_tuple(editBits, editMin);
}

// Create a new version gene bits integer item
std::tuple<QLineEdit*, QLineEdit*> DialogHelper::geneBitsIntegerItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QIntValidator);
    auto* row1 = new QHBoxLayout;
    row1->addWidget(new QLabel("Bits"));
    row1->addWidget(editBits);
    auto* row2 = new QHBoxLayout;
    row2->addWidget(new QLabel("Min"));
    row2->addWidget(editMin);
    auto* part = new QGridLayout;
    part->addWidget(new QLabel(name), 0, 0);
    part->addLayout(row1, 0, 1);
    part->addLayout(row2, 1, 1);
    layout->addLayout(part);
    return std::make_tuple(editBits, editMin);
}

// Create a new version gene bits float item
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::geneBitsFloatItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editBits = new QLineEdit;
    editBits->setReadOnly(readOnly);
    editBits->setValidator(new QIntValidator);
    auto editMin = new QLineEdit;
    editMin->setReadOnly(readOnly);
    editMin->setValidator(new QDoubleValidator);
    auto editMax = new QLineEdit;
    editMax->setReadOnly(readOnly);
    editMax->setValidator(new QDoubleValidator);
    auto* row1 = new QHBoxLayout;
    row1->addWidget(new QLabel("Bits"));
    row1->addWidget(editBits);
    auto* row2 = new QHBoxLayout;
    row2->addWidget(new QLabel("Min"));
    row2->addWidget(editMin);
    auto* row3 = new QHBoxLayout;
    row3->addWidget(new QLabel("Max"));
    row3->addWidget(editMax);
    auto part = new QGridLayout;
    part->addWidget(new QLabel(name), 0, 0);
    part->addLayout(row1, 0, 1);
    part->addLayout(row2, 1, 1);
    part->addLayout(row3, 2, 1);
    layout->addLayout(part);
    return std::make_tuple(editBits, editMin, editMax);
}


// Create a vector item in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::vectorItem(
        QBoxLayout* layout, const QString& name, bool readOnly) {
    auto editx = new QLineEdit;
    editx->setReadOnly(readOnly);
    auto edity = new QLineEdit;
    edity->setReadOnly(readOnly);
    auto editz = new QLineEdit;
    editz->setReadOnly(readOnly);
    auto part = new QHBoxLayout;
    part->addWidget(new QLabel(name));
    part->addWidget(editx);
    part->addWidget(edity);
    part->addWidget(editz);
    layout->addLayout(part);
    return std::make_tuple(editx, edity, editz);
}

// Create a vector item with no title in the given box layout
std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> DialogHelper::vectorItem(
        QGridLayout* layout, int row, const QString& name, bool readOnly) {
    auto editx = new QLineEdit;
    editx->setReadOnly(readOnly);
    auto edity = new QLineEdit;
    edity->setReadOnly(readOnly);
    auto editz = new QLineEdit;
    editz->setReadOnly(readOnly);
    layout->addWidget(new QLabel(name), row, 0);
    layout->addWidget(editx, row, 1);
    layout->addWidget(edity, row, 2);
    layout->addWidget(editz, row, 3);
    return std::make_tuple(editx, edity, editz);
}

// Create an integer item in the given box layout
QLineEdit* DialogHelper::intItem(QBoxLayout* layout, const QString& name,
                                 bool readOnly) {
    QLineEdit* edit = textItem(layout, name, readOnly);
    edit->setValidator(new QIntValidator);
    return edit;
}

// Create an integer item in the given box layout with initial value
QLineEdit* DialogHelper::intItem(QBoxLayout* layout, const QString& name,
                                 int value, bool readOnly) {
    QLineEdit* edit = intItem(layout, name, readOnly);
    edit->setText(QString::number(value));
    return edit;
}

// Create an integer item in the given box layout with initial value
QLineEdit* DialogHelper::sizeTItem(QBoxLayout* layout, const QString& name,
                                   size_t value, bool readOnly) {
    QLineEdit* edit = intItem(layout, name, readOnly);
    edit->setText(QString::number(value));
    return edit;
}

// Create an integer spin box item in the given box layout with initial value
QSpinBox* DialogHelper::intSpinItem(QBoxLayout* layout, const QString& name,
                                    int min, int max, int value) {
    auto spin = new QSpinBox;
    spin->setMinimum(min);
    spin->setMaximum(max);
    spin->setValue(value);
    if(!name.isEmpty()) {
        auto pair = new QHBoxLayout;
        pair->addWidget(new QLabel(name));
        pair->addWidget(spin);
        layout->addLayout(pair);
    } else {
        layout->addWidget(spin);
    }
    return spin;
}

// Create a drop list item with an initial string list
QComboBox* DialogHelper::dropListItem(QBoxLayout* layout,
                                      const QStringList& items) {
    auto* list = new QComboBox;
    list->addItems(items);
    layout->addWidget(list);
    return list;
}

// Create a drop list item with an initial string list
QComboBox* DialogHelper::dropListItem(QBoxLayout* layout, const QString& name,
                                      const QStringList& items) {
    auto* list = new QComboBox;
    list->addItems(items);
    auto pair = new QHBoxLayout;
    pair->addWidget(new QLabel(name));
    pair->addWidget(list);
    pair->addStretch();
    layout->addLayout(pair);
    return list;
}

// Create a drop list item with an initial string list
QComboBox* DialogHelper::dropListItem(QBoxLayout* layout, const QString& name,
                                      const std::initializer_list<const char*>& items) {
    QStringList stringItems;
    for(auto& item : items) {
        stringItems.push_back(item);
    }
    return dropListItem(layout, name, stringItems);
}

// Create a drop list item with an initial value
QComboBox* DialogHelper::dropListItem(QBoxLayout* layout, const QString& name,
                                      int value, const QStringList& items) {
    QComboBox* list = dropListItem(layout, name, items);
    list->setCurrentIndex(value);
    return list;
}

// Create a push button item
QPushButton* DialogHelper::buttonItem(QBoxLayout* layout,
                                      const QString& name) {
    auto button = new QPushButton(name);
    layout->addWidget(button);
    return button;
}

// Create a push button item with a menu
PushButtonMenu* DialogHelper::buttonMenuItem(QBoxLayout* layout, const std::string& name,
                                             const std::list<std::string>& items) {
    auto button = new PushButtonMenu(name, items);
    layout->addWidget(button);
    return button;
}

// Create a push button with a menu
PushButtonMenu* DialogHelper::buttonMenuItem(QBoxLayout* layout, const std::string& name,
        const std::list<std::pair<int, std::string>>& items) {
    auto button = new PushButtonMenu(name, items);
    layout->addWidget(button);
    return button;
}

// Create an unsigned binary integer edit field in the dialog
QLineEdit* DialogHelper::binaryItem(QBoxLayout* layout, const QString& name, uint64_t value, bool readOnly) {
    QLineEdit* edit = textItem(layout, name, QString::number(value, 2), readOnly);
    edit->setValidator(new QRegExpValidator(QRegExp("[01]+")));
    return edit;
}


std::tuple<QCheckBox*, QLineEdit*> DialogHelper::field(const QString& name, bool v1, double v2) {
    auto layout = new QHBoxLayout;
    auto boolCheck = new QCheckBox;
    boolCheck->setChecked(v1);
    layout->addWidget(boolCheck);
    auto doubleEdit = new QLineEdit(QString::number(v2));
    doubleEdit->setValidator(new QDoubleValidator);
    layout->addWidget(doubleEdit);
    field(name, layout);
    return std::make_tuple(boolCheck, doubleEdit);
}

// Create a horizontal layout used for making a line of items
QHBoxLayout* DialogHelper::lineLayout(QBoxLayout* layout) {
    auto* result = new QHBoxLayout;
    layout->addLayout(result);
    return result;
}
