/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessAreaIntensityDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessAreaIntensity.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor
void FitnessAreaIntensityDlg::construct(GeneticSearchTargetDlg* dlg,
                                        gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    fitnessFunction_ = dynamic_cast<gs::FitnessAreaIntensity*>(fitnessFunction);
    auto col1 = new QVBoxLayout;
    layout_->addLayout(col1);
    // Line 1
    auto line1Layout = new QHBoxLayout;
    col1->addLayout(line1Layout);
    fitnessFunctionWeightEdit_ = DialogHelper::doubleItem(line1Layout, "Weight");
    dataSourceList_ = DialogHelper::dropListItem(line1Layout, "Source",
                                                 {"Mag", "X", "Y"});
    // Line 2
    frequencyEdit_ = DialogHelper::doubleItem(col1, "Frequency (Hz)");
    // Line 3
    std::tie(centerXEdit_, centerYEdit_, centerZEdit_) =
            DialogHelper::vectorItem(col1, "Center x,y, (m)");
    // Line 4
    auto line4Layout = new QHBoxLayout;
    col1->addLayout(line4Layout);
    widthEdit_ = DialogHelper::doubleItem(line4Layout, "Width (m)");
    heightEdit_ = DialogHelper::doubleItem(line4Layout, "Height (m)");
    // Line 5
    orientationList_ = DialogHelper::dropListItem(col1, "Orientation",
                                                  box::Constants::orientationNames_);
    col1->addStretch();
    connect(fitnessFunctionWeightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(widthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(heightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dataSourceList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(orientationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    initialise();
}

// Initialise the GUI parameters
void FitnessAreaIntensityDlg::initialise() {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::initialise();
    InitialisingGui::Set s(initialising_);
    fitnessFunctionWeightEdit_->setText(QString::number(fitnessFunction_->weight()));
    frequencyEdit_->setText(QString::number(fitnessFunction_->frequency()));
    dataSourceList_->setCurrentIndex(fitnessFunction_->dataSource());
    centerXEdit_->setText(QString::number(fitnessFunction_->center().x()));
    centerYEdit_->setText(QString::number(fitnessFunction_->center().y()));
    centerZEdit_->setText(QString::number(fitnessFunction_->center().z()));
    widthEdit_->setText(QString::number(fitnessFunction_->width()));
    heightEdit_->setText(QString::number(fitnessFunction_->height()));
    orientationList_->setCurrentIndex(fitnessFunction_->orientation());
}

// An edit box has changed
void FitnessAreaIntensityDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(fitnessFunctionWeightEdit_, fitnessFunction_->weight(),
                                      FdTdLife::notifyMinorChange);
        auto newFrequency = c.testDouble(frequencyEdit_, fitnessFunction_->frequency(),
                                         FdTdLife::notifyMinorChange);
        auto newSource = (gs::FitnessBase::DataSource) c.testInt(
                dataSourceList_, fitnessFunction_->dataSource(),
                FdTdLife::notifyMinorChange);
        auto newCenter = c.testVectorDouble(centerXEdit_, centerYEdit_, centerZEdit_,
                                            fitnessFunction_->center(),
                                            FdTdLife::notifyMinorChange);
        auto newWidth = c.testDouble(widthEdit_, fitnessFunction_->width(),
                                     FdTdLife::notifyMinorChange);
        auto newHeight = c.testDouble(heightEdit_, fitnessFunction_->height(),
                                      FdTdLife::notifyMinorChange);
        auto newOrientation = (box::Constants::Orientation) c.testInt(
                orientationList_, fitnessFunction_->orientation(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            fitnessFunction_->set(fitnessFunction_->name(), newWeight, newSource);
            fitnessFunction_->frequency(newFrequency);
            fitnessFunction_->center(newCenter);
            fitnessFunction_->height(newHeight);
            fitnessFunction_->width(newWidth);
            fitnessFunction_->orientation(newOrientation);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessAreaIntensityDlg::tabChanged() {
    onChange();
}

