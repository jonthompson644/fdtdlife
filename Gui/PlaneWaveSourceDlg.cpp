/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PlaneWaveSourceDlg.h"
#include <Source/PlaneWaveSource.h>
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include "SourcesDlg.h"
#include "FdTdLife.h"

// Constructor
PlaneWaveSourceDlg::PlaneWaveSourceDlg(SourcesDlg* dlg, source::PlaneWaveSource* source) :
        SourceDlg(dlg, source),
        source_(source) {
    // Line 1
    auto line1Layout = new QHBoxLayout;
    frequencyEdit_ = DialogHelper::textItem(line1Layout, "First Frequency (Hz)");
    frequencyStepEdit_ = DialogHelper::textItem(line1Layout, "Frequency Step (Hz)");
    numFrequenciesEdit_ = DialogHelper::textItem(line1Layout, "Number Of Frequencies");
    layout_->addLayout(line1Layout);
    // Line 2
    auto line2Layout = new QHBoxLayout;
    polarisationEdit_ = DialogHelper::textItem(
            line2Layout, "Polarisation (-90..90 deg)");
    distributionList_ = DialogHelper::dropListItem(
            line2Layout, "Distribution", {"Flat", "Gaussian"});
    layout_->addLayout(line2Layout);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    amplitudeEdit_ = DialogHelper::textItem(line3Layout, "E Field Amplitude (V/m)");
    azimuthEdit_ = DialogHelper::textItem(line3Layout, "Azimuth (0..90 deg)");
    layout_->addLayout(line3Layout);
    // Line 4
    auto line4Layout = new QHBoxLayout;
    pmlSigmaEdit_ = DialogHelper::doubleItem(line4Layout, "PML Conductivity");
    pmlSigmaFactorEdit_ = DialogHelper::doubleItem(
            line4Layout, "PML Conductivity Factor");
    layout_->addLayout(line4Layout);
    // Line 5
    auto line5Layout = new QHBoxLayout;
    initialTimeEdit_ = DialogHelper::textItem(line5Layout, "Initial Time (s)");
    timeEdit_ = DialogHelper::textItem(line5Layout, "Run For (s)");
    continuousCheck_ = DialogHelper::boolItem(line5Layout, "Continuous");
    layout_->addLayout(line5Layout);
    // Line 6
    auto line6Layout = new QHBoxLayout;
    gaussianBEdit_ = DialogHelper::doubleItem(line6Layout, "Gaussian B");
    gaussianCEdit_ = DialogHelper::doubleItem(line6Layout, "Gaussian C");
    layout_->addLayout(line6Layout);
    // The signal display
    sourceDisplay_ = new TimeSeries(this);
    layout_->addWidget(sourceDisplay_);
    connect(frequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyStepEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numFrequenciesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(polarisationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(amplitudeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(azimuthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(pmlSigmaEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(pmlSigmaFactorEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(continuousCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(timeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(initialTimeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gaussianBEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gaussianCEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(distributionList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    // Fill the dialog
    initialise();
}

// Fill the dialog widgets
void PlaneWaveSourceDlg::initialise() {
    // Base class first
    SourceDlg::initialise();
    // Now my stuff
    InitialisingGui::Set s(dlg_->initialising());
    frequencyEdit_->setText(source_->frequencies().first().text().c_str());
    frequencyEdit_->setToolTip(QString::number(source_->frequencies().first().value()));
    frequencyStepEdit_->setText(source_->frequencies().spacing().text().c_str());
    frequencyStepEdit_->setToolTip(QString::number(source_->frequencies().spacing().value()));
    numFrequenciesEdit_->setText(source_->frequencies().n().text().c_str());
    numFrequenciesEdit_->setToolTip(QString::number(source_->frequencies().n().value()));
    polarisationEdit_->setText(source_->polarisation().text().c_str());
    polarisationEdit_->setToolTip(QString::number(source_->polarisation().value()));
    amplitudeEdit_->setText(source_->amplitude().text().c_str());
    amplitudeEdit_->setToolTip(QString::number(source_->amplitude().value()));
    azimuthEdit_->setText(source_->azimuth().text().c_str());
    azimuthEdit_->setToolTip(QString::number(source_->azimuth().value()));
    pmlSigmaEdit_->setText(QString::number(source_->pmlSigma()));
    pmlSigmaFactorEdit_->setText(QString::number(source_->pmlSigmaFactor()));
    continuousCheck_->setChecked(source_->continuous());
    timeEdit_->setText(source_->time().text().c_str());
    timeEdit_->setToolTip(QString::number(source_->time().value()));
    initialTimeEdit_->setText(source_->initialTime().text().c_str());
    initialTimeEdit_->setToolTip(QString::number(source_->initialTime().value()));
    distributionList_->setCurrentIndex(static_cast<int>(source_->distribution()));
    gaussianBEdit_->setText(QString::number(source_->gaussianB()));
    gaussianCEdit_->setText(QString::number(source_->gaussianC()));
}

// A source edit field has changed
void PlaneWaveSourceDlg::onChange() {
    // Base class first
    SourceDlg::onChange();
    // Now my stuff
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newFrequency = c.testString(frequencyEdit_, source_->frequencies().first().text(),
                                         FdTdLife::notifyMinorChange);
        auto newFrequencyStep = c.testString(frequencyStepEdit_,
                                             source_->frequencies().spacing().text(),
                                             FdTdLife::notifyMinorChange);
        auto newNumFrequencies = c.testString(numFrequenciesEdit_,
                                           source_->frequencies().n().text(),
                                           FdTdLife::notifyMinorChange);
        auto newPolarisation = c.testString(polarisationEdit_, source_->polarisation().text(),
                                            FdTdLife::notifyMinorChange);
        auto newAmplitude = c.testString(amplitudeEdit_, source_->amplitude().text(),
                                         FdTdLife::notifyMinorChange);
        auto newAzimuth = c.testString(azimuthEdit_, source_->azimuth().text(),
                                       FdTdLife::notifyMinorChange);
        auto newPmlSigma = c.testDouble(pmlSigmaEdit_, source_->pmlSigma(),
                                        FdTdLife::notifyMinorChange);
        auto newPmlSigmaFactor = c.testDouble(pmlSigmaFactorEdit_, source_->pmlSigmaFactor(),
                                              FdTdLife::notifyMinorChange);
        auto newTime = c.testString(timeEdit_, source_->time().text(),
                                    FdTdLife::notifyMinorChange);
        auto newInitialTime = c.testString(initialTimeEdit_, source_->initialTime().text(),
                                           FdTdLife::notifyMinorChange);
        auto newContinuous = c.testBool(continuousCheck_, source_->continuous(),
                                        FdTdLife::notifyMinorChange);
        auto newDistribution = static_cast<source::PlaneWaveSource::Distribution>(c.testInt(
                distributionList_, static_cast<int>(source_->distribution()),
                FdTdLife::notifyDomainSizeChange));
        auto newGaussianB = c.testDouble(gaussianBEdit_, source_->gaussianB(),
                                         FdTdLife::notifyMinorChange);
        auto newGaussianC = c.testDouble(gaussianCEdit_, source_->gaussianC(),
                                         FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            source_->frequencies().text(newFrequency, newFrequencyStep, newNumFrequencies);
            source_->polarisation(box::Expression(newPolarisation));
            source_->pmlSigma(newPmlSigma);
            source_->continuous(newContinuous);
            source_->time(box::Expression(newTime));
            source_->pmlSigmaFactor(newPmlSigmaFactor);
            source_->azimuth(box::Expression(newAzimuth));
            source_->initialTime(box::Expression(newInitialTime));
            source_->amplitude(box::Expression(newAmplitude));
            source_->distribution(newDistribution);
            source_->gaussianB(newGaussianB);
            source_->gaussianC(newGaussianC);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// TimeSeries nested class constructor
PlaneWaveSourceDlg::TimeSeries::TimeSeries(PlaneWaveSourceDlg* dlg) :
        dlg_(dlg) {
}

// Layout hint for the preferred size
QSize PlaneWaveSourceDlg::TimeSeries::sizeHint() const {
    return {300, 300};
}

// Paint the time series
void PlaneWaveSourceDlg::TimeSeries::paintEvent(QPaintEvent* /*event*/) {
    source::PlaneWaveSource* s = dlg_->source_;
    // Draw onto the widget
    QPainter painter(this);
    painter.save();
    // Black background
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, width(), height());
    // Do we have data?
    if(s != nullptr && s->incidentN() > 0) {
        // Determine scaling factors
        double signalScale = 1.0;
        if(s->amplitude().value() > 0.0) {
            signalScale = (double) height() / (s->amplitude().value() * 1.25 * 2.0);
        }
        double signalOffset = -s->amplitude().value() * 1.25;
        double timeScale = (double) width() / (double) s->incidentN();
        // Line colors
        QPen pmlPen(Qt::red);
        QPen normalPen(Qt::green);
        QPen amplitudePen(Qt::darkGray);
        // Draw the amplitude boundaries
        auto amplh = (int) std::round((s->amplitude().value() - signalOffset) * signalScale);
        auto ampll = (int) std::round((-s->amplitude().value() - signalOffset) * signalScale);
        painter.setPen(amplitudePen);
        painter.drawLine(0, amplh, width(), amplh);
        painter.drawLine(0, ampll, width(), ampll);
        // Draw the source signal
        int x1, y1, x2, y2;
        x2 = 0;
        y2 = height() - 1 - (int) std::round((s->e()[s->index(0)] -
                                              signalOffset) * signalScale);
        for(size_t t = 1; t < s->incidentN(); t++) {
            x1 = x2;
            y1 = y2;
            x2 = static_cast<int>(std::round(t * timeScale));
            y2 = height() - 1 - static_cast<int>(std::round((s->e()[s->index(t)] -
                                                             signalOffset) * signalScale));
            if(s->inPmlZone(t)) {
                painter.setPen(pmlPen);
            } else {
                painter.setPen(normalPen);
            }
            painter.drawLine(x1, y1, x2, y2);
        }
        painter.setPen(Qt::black);  // Make sure our pen not selected
    }
    painter.restore();
}

// Handle a notification
void PlaneWaveSourceDlg::notify(fdtd::Notifier* source, int /*why*/,
                                fdtd::NotificationData* /*data*/) {
    if(dlg_->model() == source) {
        sourceDisplay_->update();
    }
}

