/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DESIGNERSDLG_H
#define FDTDLIFE_DESIGNERSDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "FdTdLife.h"
#include "PushButtonMenu.h"
#include "DialogFactory.h"
#include "TabbedSubDlg.h"

class FdTdLife;
namespace designer {class Designer;}
namespace designer {class Designers;}

// Main tab panel for the designers
class DesignersDlg: public MainTabPanel {
Q_OBJECT
public:
    typedef TabbedSubDlg<DesignersDlg,designer::Designer> SubDlg;
    explicit DesignersDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;
    void fillDesignersTree(designer::Designer* sel=nullptr);

protected slots:
    void onDesignerSel();
    void onNewDesigner(int item);
    void onDelete();
    void onCopy();
    void onPaste();
    void onClear();
    void onDesignerChanged(QTreeWidgetItem* item, int column);

protected:
    enum {
        designerColName=0, designerColType, numDesignerCols
    };
    QTreeWidget* designersTree_ {};
    QVBoxLayout* designerDlgLayout_ {};
    SubDlg* designerDlg_ {};
    PushButtonMenu* newButton_ {};
    QPushButton* deleteButton_ {};
    QPushButton* copyButton_ {};
    QPushButton* pasteButton_ {};
    QPushButton* clearButton_ {};
    DialogFactory<SubDlg> dlgFactory_;

protected:
    designer::Designer* currentDesigner();
    void showDesignerDlg();
};


#endif //FDTDLIFE_DESIGNERSDLG_H
