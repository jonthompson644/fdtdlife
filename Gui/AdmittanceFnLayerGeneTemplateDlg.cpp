/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnLayerGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
AdmittanceFnLayerGeneTemplateDlg::AdmittanceFnLayerGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                                   gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    groupTab_ = new QTabWidget;
    realPage_ = new QWidget;
    imagPage_ = new QWidget;
    auto* realLayout = new QGridLayout;
    auto* imagLayout = new QGridLayout;
    realPage_->setLayout(realLayout);
    imagPage_->setLayout(imagLayout);
    realLayout->addWidget(new QLabel("Bits"), 0, 1);
    realLayout->addWidget(new QLabel("Min"), 0, 2);
    realLayout->addWidget(new QLabel("Max"), 0, 3);
    std::tie(realABitsEdit_, realAMinEdit_, realAMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "A:", 1);
    std::tie(realBBitsEdit_, realBMinEdit_, realBMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "B:", 2);
    std::tie(realCBitsEdit_, realCMinEdit_, realCMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "C:", 3);
    std::tie(realDBitsEdit_, realDMinEdit_, realDMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "D:", 4);
    std::tie(realEBitsEdit_, realEMinEdit_, realEMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "E:", 5);
    std::tie(realFBitsEdit_, realFMinEdit_, realFMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "F:", 6);
    imagLayout->addWidget(new QLabel("Bits"), 0, 1);
    imagLayout->addWidget(new QLabel("Min"), 0, 2);
    imagLayout->addWidget(new QLabel("Max"), 0, 3);
    std::tie(imagABitsEdit_, imagAMinEdit_, imagAMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "A:", 1);
    std::tie(imagBBitsEdit_, imagBMinEdit_, imagBMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "B:", 2);
    std::tie(imagCBitsEdit_, imagCMinEdit_, imagCMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "C:", 3);
    std::tie(imagDBitsEdit_, imagDMinEdit_, imagDMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "D:", 4);
    std::tie(imagEBitsEdit_, imagEMinEdit_, imagEMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "E:", 5);
    std::tie(imagFBitsEdit_, imagFMinEdit_, imagFMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(imagLayout, "F:", 6);
    groupTab_->addTab(imagPage_, "Imaginary");
    groupTab_->addTab(realPage_, "Real");
    //groupTab_->setTabPosition(QTabWidget::East);
    layout_->addWidget(groupTab_);
    connect(realABitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realAMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realAMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realBBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realBMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realBMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realCBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realCMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realCMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realDBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realDBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realDBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realEBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realEBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realEBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realFBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realFBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(realFBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagABitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagAMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagAMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagBBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagBMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagBMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagCBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagCMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagCMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagDBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagDMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagDMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagEBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagEMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagEMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagFBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagFMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(imagFMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void AdmittanceFnLayerGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    realABitsEdit_->setText(QString::number(geneTemplate_->realA().bits()));
    realAMinEdit_->setText(QString::number(geneTemplate_->realA().min()));
    realAMaxEdit_->setText(QString::number(geneTemplate_->realA().max()));
    realBBitsEdit_->setText(QString::number(geneTemplate_->realB().bits()));
    realBMinEdit_->setText(QString::number(geneTemplate_->realB().min()));
    realBMaxEdit_->setText(QString::number(geneTemplate_->realB().max()));
    realCBitsEdit_->setText(QString::number(geneTemplate_->realC().bits()));
    realCMinEdit_->setText(QString::number(geneTemplate_->realC().min()));
    realCMaxEdit_->setText(QString::number(geneTemplate_->realC().max()));
    realDBitsEdit_->setText(QString::number(geneTemplate_->realD().bits()));
    realDMinEdit_->setText(QString::number(geneTemplate_->realD().min()));
    realDMaxEdit_->setText(QString::number(geneTemplate_->realD().max()));
    realEBitsEdit_->setText(QString::number(geneTemplate_->realE().bits()));
    realEMinEdit_->setText(QString::number(geneTemplate_->realE().min()));
    realEMaxEdit_->setText(QString::number(geneTemplate_->realE().max()));
    realFBitsEdit_->setText(QString::number(geneTemplate_->realF().bits()));
    realFMinEdit_->setText(QString::number(geneTemplate_->realF().min()));
    realFMaxEdit_->setText(QString::number(geneTemplate_->realF().max()));
    imagABitsEdit_->setText(QString::number(geneTemplate_->imagA().bits()));
    imagAMinEdit_->setText(QString::number(geneTemplate_->imagA().min()));
    imagAMaxEdit_->setText(QString::number(geneTemplate_->imagA().max()));
    imagBBitsEdit_->setText(QString::number(geneTemplate_->imagB().bits()));
    imagBMinEdit_->setText(QString::number(geneTemplate_->imagB().min()));
    imagBMaxEdit_->setText(QString::number(geneTemplate_->imagB().max()));
    imagCBitsEdit_->setText(QString::number(geneTemplate_->imagC().bits()));
    imagCMinEdit_->setText(QString::number(geneTemplate_->imagC().min()));
    imagCMaxEdit_->setText(QString::number(geneTemplate_->imagC().max()));
    imagDBitsEdit_->setText(QString::number(geneTemplate_->imagD().bits()));
    imagDMinEdit_->setText(QString::number(geneTemplate_->imagD().min()));
    imagDMaxEdit_->setText(QString::number(geneTemplate_->imagD().max()));
    imagEBitsEdit_->setText(QString::number(geneTemplate_->imagE().bits()));
    imagEMinEdit_->setText(QString::number(geneTemplate_->imagE().min()));
    imagEMaxEdit_->setText(QString::number(geneTemplate_->imagE().max()));
    imagFBitsEdit_->setText(QString::number(geneTemplate_->imagF().bits()));
    imagFMinEdit_->setText(QString::number(geneTemplate_->imagF().min()));
    imagFMaxEdit_->setText(QString::number(geneTemplate_->imagF().max()));
}

// An edit box has changed
void AdmittanceFnLayerGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newRealA = c.testGeneBitsSpecDouble(realABitsEdit_, realAMinEdit_, realAMaxEdit_,
                                                 geneTemplate_->realA(),
                                                 FdTdLife::notifySequencerChange);
        auto newRealB = c.testGeneBitsSpecLogDouble(realBBitsEdit_, realBMinEdit_, realBMaxEdit_,
                                                    geneTemplate_->realB(),
                                                    FdTdLife::notifySequencerChange);
        auto newRealC = c.testGeneBitsSpecLogDouble(realCBitsEdit_, realCMinEdit_, realCMaxEdit_,
                                                    geneTemplate_->realC(),
                                                    FdTdLife::notifySequencerChange);
        auto newRealD = c.testGeneBitsSpecLogDouble(realDBitsEdit_, realDMinEdit_, realDMaxEdit_,
                                                    geneTemplate_->realD(),
                                                    FdTdLife::notifySequencerChange);
        auto newRealE = c.testGeneBitsSpecLogDouble(realEBitsEdit_, realEMinEdit_, realEMaxEdit_,
                                                    geneTemplate_->realE(),
                                                    FdTdLife::notifySequencerChange);
        auto newRealF = c.testGeneBitsSpecLogDouble(realFBitsEdit_, realFMinEdit_, realFMaxEdit_,
                                                    geneTemplate_->realF(),
                                                    FdTdLife::notifySequencerChange);
        auto newImagA = c.testGeneBitsSpecDouble(imagABitsEdit_, imagAMinEdit_, imagAMaxEdit_,
                                                 geneTemplate_->imagA(),
                                                 FdTdLife::notifySequencerChange);
        auto newImagB = c.testGeneBitsSpecLogDouble(imagBBitsEdit_, imagBMinEdit_, imagBMaxEdit_,
                                                    geneTemplate_->imagB(),
                                                    FdTdLife::notifySequencerChange);
        auto newImagC = c.testGeneBitsSpecLogDouble(imagCBitsEdit_, imagCMinEdit_, imagCMaxEdit_,
                                                    geneTemplate_->imagC(),
                                                    FdTdLife::notifySequencerChange);
        auto newImagD = c.testGeneBitsSpecLogDouble(imagDBitsEdit_, imagDMinEdit_, imagDMaxEdit_,
                                                    geneTemplate_->imagD(),
                                                    FdTdLife::notifySequencerChange);
        auto newImagE = c.testGeneBitsSpecLogDouble(imagEBitsEdit_, imagEMinEdit_, imagEMaxEdit_,
                                                    geneTemplate_->imagE(),
                                                    FdTdLife::notifySequencerChange);
        auto newImagF = c.testGeneBitsSpecLogDouble(imagFBitsEdit_, imagFMinEdit_, imagFMaxEdit_,
                                                    geneTemplate_->imagF(),
                                                    FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->realA(newRealA);
            geneTemplate_->realB(newRealB);
            geneTemplate_->realC(newRealC);
            geneTemplate_->realD(newRealD);
            geneTemplate_->realE(newRealE);
            geneTemplate_->realF(newRealF);
            geneTemplate_->imagA(newImagA);
            geneTemplate_->imagB(newImagB);
            geneTemplate_->imagC(newImagC);
            geneTemplate_->imagD(newImagD);
            geneTemplate_->imagE(newImagE);
            geneTemplate_->imagF(newImagF);
            dlg_->model()->doNotification(c.why());
        }
    }
}

