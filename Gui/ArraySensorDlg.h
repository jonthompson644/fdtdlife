/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ARRAYSENSORDLG_H
#define FDTDLIFE_ARRAYSENSORDLG_H

#include "QtIncludes.h"
#include "SensorDlg.h"
#include "PushButtonMenu.h"
namespace sensor {class ArraySensor;}

// Edit dialog for the array sensorId
class ArraySensorDlg : public SensorDlg {
Q_OBJECT
public:
    ArraySensorDlg(SensorsDlg* dlg, sensor::Sensor* sensor);
    void tabChanged() override;
    void initialise() override;
    void modelNotify() override;

protected slots:
    void onChange() override;
    void onManual();
    void onCopyToClipboard(int addKind);

protected:
    void updateUi();

protected:
    QLineEdit* numPointsXEdit_;
    QLineEdit* numPointsYEdit_;
    QLineEdit* frequencyEdit_;
    QCheckBox* animateFrequencyCheck_;
    QLineEdit* frequencyStepEdit_;
    QCheckBox* averageOverCellCheck_;
    QCheckBox* xComponentCheck_;
    QCheckBox* yComponentCheck_;
    QCheckBox* zComponentCheck_;
    QLineEdit* numSamplesEdit_;
    QCheckBox* manualPosCheck_;
    QCheckBox* manualSamplesCheck_;
    QLineEdit* transmittedPosEdit_;
    QLineEdit* reflectedPosEdit_;
    QCheckBox* includeReflectedCheck_;
    QCheckBox* useWindowCheck_;
    enum {csvTransmittedTimeSeries=0, csvReflectedTimeSeries, csvTransmittedSpectra,
            csvReflectedSpectra, xmlTransmittedAdmittance};
    PushButtonMenu* csvButton_;
    sensor::ArraySensor* sensor_;

};



#endif //FDTDLIFE_ARRAYSENSORDLG_H
