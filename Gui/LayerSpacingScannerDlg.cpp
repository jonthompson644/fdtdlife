/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LayerSpacingScannerDlg.h"
#include <Seq/LayerSpacingScanner.h>
#include "DialogHelper.h"
#include "CharacterizerParamsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
LayerSpacingScannerDlg::LayerSpacingScannerDlg(CharacterizerParamsDlg* dlg,
                                                     seq::LayerSpacingScanner* scanner) :
        ScannerDlg(dlg, scanner),
        scanner_(scanner) {
    // My controls
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment X,Y,Z (m)");
    // Connect handlers
    connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    initialise();
}

// Initialise the GUI parameters
void LayerSpacingScannerDlg::initialise() {
    ScannerDlg::initialise();
    InitialisingGui::Set s(initialising_);
    incrementXEdit_->setText(QString::number(scanner_->increment().x()));
    incrementYEdit_->setText(QString::number(scanner_->increment().y()));
    incrementZEdit_->setText(QString::number(scanner_->increment().z()));
}

// An edit box has changed
void LayerSpacingScannerDlg::onChange() {
    ScannerDlg::onChange();
    if(!initialising_) {
        GuiChangeDetector c;
        box::Vector<double> newIncrement = c.testVectorDouble(
                incrementXEdit_, incrementYEdit_, incrementZEdit_,
                scanner_->increment(), FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            scanner_->increment(newIncrement);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}
