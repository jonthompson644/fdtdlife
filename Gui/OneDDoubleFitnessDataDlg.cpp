/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "OneDDoubleFitnessDataDlg.h"
#include "GeneticSearchPopulationDlg.h"
#include "GeneticSearchSeqDlg.h"
#include "SequencersDlg.h"
#include "DataSeriesWidget.h"
#include <Gs/GeneticSearch.h>

// Second stage constructor
void OneDDoubleFitnessDataDlg::construct(GeneticSearchPopulationDlg* dlg,
                                         gs::FitnessData* cfg) {
    // Base class first
    SubDialogBase::construct(dlg, cfg);
    // Transmittance data
    nameLabel_ = new QLabel("");
    dataSeriesWidget_ = new DataSeriesWidget(dlg_->dlg()->dlg()->model()->guiConfiguration());
    layout_->addWidget(nameLabel_);
    layout_->addWidget(dataSeriesWidget_);
    layout_->setContentsMargins(0, 0, 0, 0);
    layout_->setSpacing(4);
}

// Use the data
void OneDDoubleFitnessDataDlg::initialise() {
    nameLabel_->setText(data()->name().c_str());
    if(data()->numDataSets() > 0) {
        dataSeriesWidget_->setData(data()->data(0),
                                   GuiConfiguration::elementDataA, true);
    }
    if(data()->numDataSets() > 1) {
        dataSeriesWidget_->setData(data()->data(1),
                                   GuiConfiguration::elementDataB, false);
    }
    if(data()->numDataSets() > 2) {
        dataSeriesWidget_->setData(data()->data(2),
                                   GuiConfiguration::elementDataC, false);
    }
    dataSeriesWidget_->setAxisInfo(data()->axisXMin(), data()->axisXStep(),
                                   data()->axisXUnits().c_str(),
                                   data()->axisYMin(), data()->axisYMax(),
                                   data()->axisYUnits().c_str());
}
