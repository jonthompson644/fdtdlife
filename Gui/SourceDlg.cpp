/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SourceDlg.h"
#include "SourcesDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Source/Source.h>

// Constructor
SourceDlg::SourceDlg(SourcesDlg* dlg, source::Source* source) :
        Notifiable(dlg->model()),
        dlg_(dlg),
        source_(source) {
    auto paramsLayout = new QVBoxLayout;
    layout_ = new QVBoxLayout;
    setLayout(paramsLayout);
    auto line1Layout = new QHBoxLayout;
    nameEdit_ = DialogHelper::textItem(line1Layout, "Name");
    layout_->addLayout(line1Layout);
    // The group box
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(layout_);
    stretchLayout->addStretch();
    group_ = new QGroupBox;
    group_->setLayout(stretchLayout);
    paramsLayout->addWidget(group_);
    // Connect handlers
    connect(nameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void SourceDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    nameEdit_->setText(source_->name().c_str());
}

// A sensorId edit field has changed
void SourceDlg::onChange() {
    if(!dlg_->initialising()) {
        source::Source* source = dlg_->getCurrentSource();
        if(source != nullptr) {
            // Get the changes
            GuiChangeDetector c;
            auto newName = c.testString(nameEdit_, source->name(), FdTdLife::notifyMinorChange);
            // Make the changes
            if(c.isChanged()) {
                InitialisingGui::Set s(dlg_->initialising());
                source->name(newName);
                dlg_->model()->doNotification(c.why());
            }
        }
    }
}

// The tab is changing, do we need to update anything?
void SourceDlg::tabChanged() {
    onChange();
}
