/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FdTdLife.h"
#include "ConfigurationDlg.h"
#include "VariablesDlg.h"
#include "MaterialsDlg.h"
#include "ShapesDlg.h"
#include "SourcesDlg.h"
#include "SensorsDlg.h"
#include "ViewFrameDlg.h"
#include "GuiConfiguration.h"
#include "ViewFrame.h"
#include "SequencersDlg.h"
#include "DesignersDlg.h"
#include "AnalysersDlg.h"
#include "AdmittanceCatalogueDlg.h"
#include "NodeManager.h"
#include "Node.h"
#include "ProcessingNodeCfg.h"
#include <Gs/GeneticSearch.h>
#include <Xml/DomObject.h>
#include <iostream>
#include <Xml/Reader.h>
#include <Sensor/Sensor.h>
#include <Domain/Material.h>
#include <CatalogueBuilder.h>

// Constructor
FdTdLife::FdTdLife() :
        Notifiable(this),
        modelModified_(false),
        timer_(new QTimer(this)),
        runState_(runStateStopped),
        nextViewIdentifier_(0),
        guiConfiguration_(new GuiConfiguration(p())),
        tabs_(new QTabWidget) {
    timer_->setSingleShot(true);
    setTitle();
    clear();
    matchModelConfig();
    // Load the style sheet
    //std::cout << "Styles available: " << std::endl;
    //for(auto& s : QStyleFactory::keys()) {
    //    std::cout << s.toStdString() << std::endl;
    //}
    //setStyle(QStyleFactory::create("Fusion"));
    QFile f(":/gui.css");  // The :/ syntax reads a resource file!
    if(f.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(&f);
        setStyleSheet(in.readAll());
    }
    // Create the dialog panels
    setCentralWidget(tabs_);
    sourcesDlg_ = new SourcesDlg(this);
    configurationDlg_ = new ConfigurationDlg(this);
    variablesDlg_ = new VariablesDlg(this);
    materialsDlg_ = new MaterialsDlg(this);
    shapesDlg_ = new ShapesDlg(this);
    sensorsDlg_ = new SensorsDlg(this);
    analysersDlg_ = new AnalysersDlg(this);
    viewsDlg_ = new ViewFrameDlg(this);
    sequencersDlg_ = new SequencersDlg(this);
    designersDlg_ = new DesignersDlg(this);
    admittanceCatDlg_ = new AdmittanceCatalogueDlg(this);
    tabs_->addTab(configurationDlg_, "Configuration");
    tabs_->addTab(sourcesDlg_, "Sources");
    tabs_->addTab(materialsDlg_, "Materials");
    tabs_->addTab(shapesDlg_, "Shapes");
    tabs_->addTab(sensorsDlg_, "Sensors");
    tabs_->addTab(analysersDlg_, "Analysers");
    tabs_->addTab(viewsDlg_, "Views");
    tabs_->addTab(sequencersDlg_, "Sequencers");
    tabs_->addTab(admittanceCatDlg_, "Catalogue");
    tabs_->addTab(variablesDlg_, "Variables");
    tabs_->addTab(designersDlg_, "Designers");
    createActions();
    setUnifiedTitleAndToolBarOnMac(true);
    connect(timer_, SIGNAL(timeout()), SLOT(timerExpiry()));
    connect(tabs_, SIGNAL(tabBarClicked(int)), SLOT(onTabChange(int)));
    initialise();
}

// Destructor
FdTdLife::~FdTdLife() {
    clear();
    delete guiConfiguration_;
}

// Set the application title
void FdTdLife::setTitle() {
    QString title("FDTD Life");
    if(!currentFile_.isEmpty()) {
        QFileInfo info(currentFile_);
        title += QString(" - ") + info.baseName();
    }
    setWindowTitle(title);
}

// Clear the model back to a pristine state
void FdTdLife::clear() {
    // Delete the configuration objects
    while(!views_.empty()) {
        delete views_.front();
    }
    while(!nodes_.empty()) {
        delete nodes_.front();
    }
    guiConfiguration_->clear();
    fdtd::Model::clear();
}

// Create the actions
void FdTdLife::createActions() {
    // Icons
    const QIcon newIcon(":/images/new.png");
    const QIcon openIcon(":/images/open.png");
    const QIcon saveIcon(":/images/save.png");
    const QIcon saveasIcon(":/images/saveas.png");
    const QIcon stepIcon(":/images/step.png");
    const QIcon playIcon(":/images/play.png");
    const QIcon replayIcon(":/images/replay.png");
    const QIcon stopIcon(":/images/stop.png");
    const QIcon pauseIcon(":/images/pause.png");
    const QIcon motionIcon(":/images/motion.png");
    const QIcon pasteIcon(":/images/paste.png");
    // Actions
    auto* newAct = new QAction(newIcon, tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new model"));
    auto* saveAct = new QAction(saveIcon, tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the current model configuration"));
    auto* saveAsAct = new QAction(saveasIcon,tr("Save &As"), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the current model with a new file name"));
    auto* openAct = new QAction(openIcon, tr("&Open"), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open a model"));
    auto* playAct = new QAction(playIcon, tr("&Play"), this);
    playAct->setStatusTip(tr("Continuously compute model steps"));
    playAct->setShortcut(QKeySequence("Ctrl+P"));
    auto* stepAct = new QAction(stepIcon, tr("&Step"), this);
    stepAct->setStatusTip(tr("Move the model on one step"));
    stepAct->setShortcut(QKeySequence("Ctrl+F"));
    auto* replayAct = new QAction(replayIcon, tr("&Replay"), this);
    replayAct->setStatusTip(tr("Reset the model to the beginning"));
    replayAct->setShortcut(QKeySequence("Ctrl+R"));
    auto* stopAct = new QAction(stopIcon, tr("S&top"), this);
    stopAct->setStatusTip(tr("Stop the model running"));
    stopAct->setShortcut(QKeySequence("Ctrl+T"));
    auto* writeDataAct = new QAction(saveIcon, tr("&Write Data"), this);
    saveAct->setStatusTip(tr("Write the current model run data"));
    auto* readDataAct = new QAction(saveIcon, tr("Rea&d Data"), this);
    saveAct->setStatusTip(tr("Read run data into the current model"));
    auto* motionAct = new QAction(motionIcon, tr("A&nimate Sensors"), this);
    motionAct->setStatusTip(tr("Animate the sensors"));
    motionAct->setShortcut(QKeySequence("Ctrl+A"));
    // File menu
    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addAction(writeDataAct);
    fileMenu->addAction(readDataAct);
    // Model menu
    QMenu* modelMenu = menuBar()->addMenu(tr("&Model"));
    modelMenu->addAction(playAct);
    modelMenu->addAction(stepAct);
    modelMenu->addAction(stopAct);
    modelMenu->addAction(replayAct);
    modelMenu->addAction(motionAct);
    // The tool bar
    auto mainToolBar = addToolBar(tr("Main"));
    mainToolBar->addAction(newAct);
    mainToolBar->addAction(openAct);
    mainToolBar->addAction(saveAct);
    mainToolBar->addAction(saveAsAct);
    mainToolBar->addAction(playAct);
    mainToolBar->addAction(stepAct);
    mainToolBar->addAction(stopAct);
    mainToolBar->addAction(replayAct);
    mainToolBar->addAction(motionAct);
    // Connections
    connect(newAct, &QAction::triggered, this, &FdTdLife::onNewFile);
    connect(openAct, &QAction::triggered, this, &FdTdLife::onOpenFile);
    connect(saveAct, &QAction::triggered, this, &FdTdLife::onSaveFile);
    connect(saveAsAct, &QAction::triggered, this, &FdTdLife::onSaveAsFile);
    connect(playAct, &QAction::triggered, this, &FdTdLife::onPlayModel);
    connect(stepAct, &QAction::triggered, this, &FdTdLife::onStepModel);
    connect(stopAct, &QAction::triggered, this, &FdTdLife::onStopModel);
    connect(replayAct, &QAction::triggered, this, &FdTdLife::onReplayModel);
    connect(writeDataAct, &QAction::triggered, this, &FdTdLife::onWriteData);
    connect(readDataAct, &QAction::triggered, this, &FdTdLife::onReadData);
    connect(motionAct, &QAction::triggered, this, &FdTdLife::onAnimate);
}

// New file command
void FdTdLife::onNewFile() {
    onTabChange(0);
    // Need to save the current model?
    maybeSave();
    // Start the model from fresh
    clear();
    matchModelConfig();
    // No file
    currentFile_ = "";
    setTitle();
}

// Make the configuration objects match the model objects.
// This is necessary to pick up any default the model
// creates automatically.
void FdTdLife::matchModelConfig() {
    // Make the processing nodes match
    for(auto node : processingMgr_->nodes()) {
        // Is there a config item for this node
        ProcessingNodeCfg* config = findProcessingNodeCfg(node->id());
        if(config == nullptr) {
            new ProcessingNodeCfg(this, node);
        } else {
            config->updateFromModel();
        }
    }
}

// Save the file
void FdTdLife::doSaveFile(QString filename, bool selectFile) {
    // Get the file name if required
    if(filename.isEmpty() || selectFile) {
        filename = QFileDialog::getSaveFileName(
                this, tr("Save FDTD Model"), filename,
                tr("FDTD Life Files (*.fdtd)"));
    }
    std::cout << "Save to " << filename.toStdString() << std::endl;
    // Save the file
    if(!filename.isEmpty()) {
        QFileInfo fileInfo(filename);
        std::string pathName = fileInfo.path().toStdString() + "/";
        std::string fileName = fileInfo.completeBaseName().toStdString();
        writeConfigFile(pathName, fileName);
        currentFile_ = filename;
        modelModified_ = false;
    }
    setTitle();
}

// Write the configuration to the XML DOM
void FdTdLife::writeConfig(xml::DomObject& root) const {
    // Base class for all the model configuration
    fdtd::Model::writeConfig(root);
    // Now the GUI related parameters
    root << xml::Reopen();
    root << xml::Obj("parameterscfg") << *guiConfiguration_;
    // Views
    for(auto& view : views_) {
        root << xml::Obj("view") << *view;
    }
    root << xml::Close();
}

// Read the configuration from the XML DOM
void FdTdLife::readConfig(xml::DomObject& root) {
    // Base class for all the model configuration
    fdtd::Model::readConfig(root);
    // Update the config objects from the model
    matchModelConfig();
    // Now the GUI related parameters
    root >> xml::Reopen();
    if(!root.obj("parameterscfg").empty()) {
        root >> xml::Obj("parameterscfg") >> *guiConfiguration_;
    }
    // Views
    for(auto& o : root.obj("view")) {
        auto view = new ViewFrame(o, this);
        *o >> *view;
    }
    root >> xml::Close();
}

// Save file command
void FdTdLife::onSaveFile() {
    onTabChange(0);
    doSaveFile(currentFile_, false);
}

// Save as file command
void FdTdLife::onSaveAsFile() {
    onTabChange(0);
    doSaveFile(currentFile_, true);
}

// Open file command
void FdTdLife::onOpenFile() {
    onTabChange(0);
    // Need to save the current model?
    maybeSave();
    // Get the file to read
    QString filename = QFileDialog::getOpenFileName(
            this, tr("Open FDTD Model"), currentFile_,
            tr("FDTD Life Files (*.fdtd)"));
    if(!filename.isEmpty()) {
        clear();
        QFileInfo fileInfo(filename);
        std::string pathName = fileInfo.canonicalPath().toStdString() + "/";
        std::string fileName = fileInfo.completeBaseName().toStdString();
        {
            auto* tab = dynamic_cast<MainTabPanel*>(tabs_->widget(tabs_->currentIndex()));
            InitialisingGui::Set s(tab->initialising());
            readConfigFile(pathName, fileName);
        }
        // Now initialise everything
        configurationDlg_->initialise();
        sourcesDlg_->initialise();
        materialsDlg_->initialise();
        shapesDlg_->initialise();
        sensorsDlg_->initialise();
        analysersDlg_->initialise();
        viewsDlg_->initialise();
        sequencersDlg_->initialise();
        designersDlg_->initialise();
        admittanceCatDlg_->initialise();
        currentFile_ = filename;
        modelModified_ = false;
        initialise();
        setTitle();
        doNotification(FdTdLife::notifyDomainSizeChange);
    }
}

// Step model command
void FdTdLife::onStepModel() {
    onTabChange(0);
    step();
}

// Time to perform another step
void FdTdLife::timerExpiry() {
    QCoreApplication::sendPostedEvents();
    QCoreApplication::processEvents();
    switch(runState_) {
        case runStateStopped:
        case runStatePaused:
            break;
        case runStateRunning:
            switch(guiConfiguration_->phase()) {
                case fdtd::Configuration::phaseElectric:
                case fdtd::Configuration::phaseMagnetic:
                    onStepModel();
                    break;
                case fdtd::Configuration::phaseDone:
                    runState_ = runStatePaused;
                    break;
            }
            break;
        case runStateAnimating:
            sensors_.animate();
            timer_->start(std::max((int) (p()->animationPeriod() * 1000.0), 100));
            doNotification(FdTdLife::notifyMinorChange);
            break;
    }
}

// Start the model running
void FdTdLife::onPlayModel() {
    if(runState_ != runStateStopped || start()) {
        runState_ = runStateRunning;
    }
    onTabChange(0);
    timerExpiry();
}

// Restart the model
void FdTdLife::onReplayModel() {
    onTabChange(0);
    stop();
    initialise();
    runState_ = runStateStopped;
}

// Stop the model running
void FdTdLife::onStopModel() {
    onTabChange(0);
    switch(runState_) {
        case runStatePaused:
        case runStateStopped:
            break;
        case runStateRunning:
            sensors_.calculate();
            analysers_.calculate();
            pause();
            timer_->stop();
            runState_ = runStatePaused;
            break;
        case runStateAnimating:
            timer_->stop();
            runState_ = runStatePaused;
            break;
    }
}

// Check to see if we should save the current model
// True is returned if the caller can proceed to destroy
// the current model.
bool FdTdLife::maybeSave() {
    bool result = true;
    if(modelModified_) {
        const QMessageBox::StandardButton ret =
                QMessageBox::warning(this, tr("FDTD Life"),
                                     tr("The current document has been modified.\n"
                                        "Do you want to save your changes?"),
                                     QMessageBox::Save | QMessageBox::Discard |
                                     QMessageBox::Cancel);
        switch(ret) {
            case QMessageBox::Save:
                doSaveFile(currentFile_, false);
                break;
            case QMessageBox::Cancel:
                result = false;
                break;
            default:
                break;
        }
    }
    return result;
}

// Add a view
void FdTdLife::addCfg(ViewFrame* v) {
    views_.push_back(v);
    if(v->identifier() >= nextViewIdentifier_) {
        nextViewIdentifier_ = v->identifier() + 1;
    }
}

// Add a processing node
void FdTdLife::addCfg(ProcessingNodeCfg* v) {
    nodes_.push_back(v);
}

// Remove a view
void FdTdLife::removeCfg(ViewFrame* v) {
    views_.remove(v);
}

// Remove a processing node
void FdTdLife::removeCfg(ProcessingNodeCfg* v) {
    nodes_.remove(v);
}

// Override of the model initialise function
void FdTdLife::initialise() {
    // Call the base class first
    fdtd::Model::initialise();
    // Initialise the view frames
    for(auto view : views_) {
        view->initialise();
    }
    // Model elements may have been created
    matchModelConfig();
    // Update the dialogs
    configurationDlg_->initialise();
    materialsDlg_->initialise();
    shapesDlg_->initialise();
    sensorsDlg_->initialise();
    analysersDlg_->initialise();
    sourcesDlg_->initialise();
    sequencersDlg_->initialise();
    variablesDlg_->initialise();
    designersDlg_->initialise();
    // Update the UI
    doNotification(notifyDomainSizeChange);
    doNotification(notifyInitialisation);
}

// Initialise the mode only, not the configuration
void FdTdLife::initialiseModel() {
    fdtd::Model::initialise();
    // Initialise the view frames
    for(auto view : views_) {
        view->initialise();
    }
    doNotification(notifyInitialisation);
}

// Handle a notification
void FdTdLife::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(this == source) {
        switch(why) {
            case notifyStepComplete:
                if(runState_ == runStateRunning) {
                    if(guiConfiguration_->phase() == fdtd::Configuration::phaseElectric ||
                            guiConfiguration_->phase() == fdtd::Configuration::phaseMagnetic) {
                        timer_->start(1);
                    }
                }
                break;
            case notifyRunComplete: {
                // Do any of the sensors want to write a CSV file?
                if(sensors_.wantsCsv()) {
                    // Make a file
                    std::ofstream* csvFile = openSensorCsvFile();
                    // Call the sensorId file output functions
                    sensors_.writeCsv(csvFile);
                    // Clean up
                    delete csvFile;
                }
                break;
            }
            case FdTdLife::notifyIndividualLoaded:
                materialsDlg_->initialise();
                shapesDlg_->initialise();
                sensorsDlg_->initialise();
                analysersDlg_->initialise();
                sourcesDlg_->initialise();
                configurationDlg_->initialise();
                break;
            case FdTdLife::notifyDomainContentsChange:
                initialise();
                break;
            default:
                break;
        }
    } else if(dynamic_cast<gs::GeneticSearch*>(source) != nullptr) {
        switch(why) {
            case gs::GeneticSearch::notifyIndividualComplete:
                doNotification(notifySequencerChange);
                break;
            case gs::GeneticSearch::notifyGenerationComplete:
                doNotification(notifySequencerChange);
                break;
            case gs::GeneticSearch::notifyIndividualLoaded:
                doNotification(FdTdLife::notifyIndividualLoaded);
                break;
            default:
                break;
        }
    } else if(dynamic_cast<fdtd::CatalogueBuilder*>(source) != nullptr) {
        switch(why) {
            case fdtd::CatalogueBuilder::notifyCatalogueItemComplete:
                doNotification(notifySequencerChange);
                break;
            case fdtd::CatalogueBuilder::notifyBlockComplete:
                doNotification(notifySequencerChange);
                break;
            default:
                break;
        }
    }
}

// The current tab has changed
void FdTdLife::onTabChange(int /*index*/) {
    auto* tab = dynamic_cast<MainTabPanel*>(
            tabs_->widget(tabs_->currentIndex()));
    if(tab != nullptr) {
        tab->tabChanged();
    }
}

// Write a sensorId CSV file now
void FdTdLife::writeSensorCsvFile(sensor::Sensor* sensor) {
    std::ofstream* csvFile = openSensorCsvFile();
    sensor->writeCsvData(csvFile);
    delete csvFile;
}


// Open the sensorId CSV file, returns a file object or null
std::ofstream* FdTdLife::openSensorCsvFile() {
    // If they do, make a file
    std::ofstream* csvFile = nullptr;
    if(!currentFile_.isEmpty()) {
        QFileInfo fileInfo(currentFile_);
        std::cout << "Current file " << currentFile_.toStdString().c_str() << std::endl;
        QString csvFilename = fileInfo.path() + "/" + fileInfo.baseName() + ".csv";
        std::cout << "Opening CSV file " << csvFilename.toStdString().c_str() << std::endl;
        csvFile = new std::ofstream;
        csvFile->open(csvFilename.toStdString().c_str(),
                      std::ios::out | std::ios::trunc);
        if(!csvFile->is_open()) {
            delete csvFile;
            csvFile = nullptr;
        }
    }
    return csvFile;
}

// Find the processing node configuration for a given identifier
ProcessingNodeCfg* FdTdLife::findProcessingNodeCfg(int id) {
    ProcessingNodeCfg* result = nullptr;
    for(auto cfg : nodes_) {
        if(cfg->id() == id) {
            result = cfg;
            break;
        }
    }
    return result;
}

// Fill a materials combo box
void FdTdLife::fillMaterialList(QComboBox* list, int selectedId) const {
    list->clear();
    int index = 0;
    int selectionIndex = 0;
    for(auto& material : d_.materials()) {
        list->insertItem(index, material->name().c_str());
        list->setItemData(index, QVariant(material->index()), Qt::UserRole);
        if(selectedId == material->index()) {
            selectionIndex = index;
        }
        index++;
    }
    list->setCurrentIndex(selectionIndex);
}

// Fill a variables combo box
void FdTdLife::fillVariableList(QComboBox* list, int selectedId) {
    list->clear();
    int index = 0;
    int selectionIndex = 0;
    for(auto& var : variables()) {
        list->insertItem(index, var.name().c_str());
        list->setItemData(index, QVariant(var.identifier()), Qt::UserRole);
        if(selectedId == var.identifier()) {
            selectionIndex = index;
        }
        index++;
    }
    list->setCurrentIndex(selectionIndex);
}

// Fill a sensors combo box
void FdTdLife::fillSensorList(QComboBox* list, int selectedId) {
    list->clear();
    int index = 0;
    int selectionIndex = 0;
    for(auto& s : sensors()) {
        list->insertItem(index, s->name().c_str());
        list->setItemData(index, QVariant(s->identifier()), Qt::UserRole);
        if(selectedId == s->identifier()) {
            selectionIndex = index;
        }
        index++;
    }
    list->setCurrentIndex(selectionIndex);
}

// Fill an analysers combo box
void FdTdLife::fillAnalyserList(QComboBox* list, int selectedId) {
    list->clear();
    int index = 0;
    int selectionIndex = 0;
    for(auto& s : analysers()) {
        list->insertItem(index, s->name().c_str());
        list->setItemData(index, QVariant(s->identifier()), Qt::UserRole);
        if(selectedId == s->identifier()) {
            selectionIndex = index;
        }
        index++;
    }
    list->setCurrentIndex(selectionIndex);
}

// Return the identifier from a combo box filled by one of the fillXXX functions
int FdTdLife::selected(QComboBox* list) const {
    return list->itemData(list->currentIndex(), Qt::UserRole).toInt();
}

// Write the model run data file
void FdTdLife::onWriteData() {
    if(!currentFile_.isEmpty()) {
        QFileInfo info(currentFile_);
        QString fileName = info.path() + "/" + info.baseName() + ".fdtddata";
        writeData(fileName.toStdString());
    }
}

// Read model data into the current model
void FdTdLife::onReadData() {
    if(!currentFile_.isEmpty()) {
        QFileInfo info(currentFile_);
        QString fileName = info.path() + "/" + info.baseName() + ".fdtddata";
        bool result = readData(fileName.toStdString());
        if(!result) {
            QMessageBox box(QMessageBox::Warning, "Read State Data",
                            "Failed to read state data file");
        }
        // Update the UI
        doNotification(notifyDataRead);
    }
}

// Animate the sensors
void FdTdLife::onAnimate() {
    onTabChange(0);
    switch(runState_) {
        case runStatePaused:
            runState_ = runStateAnimating;
            timer_->start(std::max((int) (p()->animationPeriod() * 1000.0), 100));
            break;
        case runStateRunning:
        case runStateAnimating:
        case runStateStopped:
            break;
    }
}

// Return the view with the given identifier
ViewFrame* FdTdLife::getView(int identifier) {
    ViewFrame* result = nullptr;
    for(auto& view : views_) {
        if(view->identifier() == identifier) {
            result = view;
            break;
        }
    }
    return result;
}

// Set the run state to stopped and clear out any waiting jobs
void FdTdLife::setStopped() {
    stop();
    runState_ = runStateStopped;
    processingMgr_->clear();
    processingMgr_->doNotification(fdtd::NodeManager::notifyJobListChanged);
}
