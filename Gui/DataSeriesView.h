/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DATASERIESVIEW_H
#define FDTDLIFE_DATASERIESVIEW_H

#include <Sensor/DataSpec.h>
#include "ViewItem.h"

// Displays a time series in a view frame
class DataSeriesView : public ViewItem {

protected:
    int numSeries_ {};
    int markerPos_ {20};
    sensor::DataSpec::Info info_;
    double dataScale_ {};
    double dataOffset_ {};
    double timeScale_ {};
    double dataTickStep_ {};
    double timeTickStep_ {};
    double timeTickScale_ {};
    double markerValueX_ {};
    std::vector<double> markerValueY_;
    static constexpr int maxNumSeries_ = 4;
    // Configuration
    double xMin_ {0.0};
    double xMax_ {0.0};
    bool xAutomatic_ {true};
    double yMin_ {0.0};
    double yMax_ {0.0};
    bool yAutomatic_ {true};

public:
    DataSeriesView(int identifier, int sensorId, const char* sensorData,
                   ViewFrame* view, int numSeries);
    void paintData(QPainter* painter) override;
    void leftClick(QPoint pos) override;
    void contextMenu(QPoint pos) override;
    QSize sizeHint() override;
    void sizeRequest(QSize& size) override;
    ViewItemDlg* makeDialog(ViewFrameDlg* dlg) override;
    void set(double xMin, double xMax, bool xAutomatic,
             double yMin, double yMax, bool yAutomatic);
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void getMimeData(QMimeData* mimeData) override;
    // Getters
    double xMin() const {return xMin_;}
    double xMax() const {return xMax_;}
    bool xAutomatic() const {return xAutomatic_;}
    double yMin() const {return yMin_;}
    double yMax() const {return yMax_;}
    bool yAutomatic() const {return yAutomatic_;}

protected:
    bool calcScaling();
    virtual void calcXScaling();
    virtual void calcYScaling();
    virtual void getData(int x, std::vector<double>& dy, double& dx);
    void drawVerticalAxis(QPainter* painter);
    void drawHorizontalAxis(QPainter* painter);
    void drawData(QPainter* painter);
    void drawMarkerInfo(QPainter* painter);
};


#endif //FDTDLIFE_DATASERIESVIEW_H
