/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FilterAnalyserDlg.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void FilterAnalyserDlg::construct(AnalysersDlg* dlg, sensor::Analyser* d) {
    AnalysersDlg::SubDlg::construct(dlg, d);
    analyser_ = dynamic_cast<sensor::FilterAnalyser*>(d);
    // Create the controls
    auto line1Layout = new QHBoxLayout;
    layout_->addLayout(line1Layout);
    sensorList_ = DialogHelper::dropListItem(line1Layout, "Sensor");
    locationList_ = DialogHelper::dropListItem(line1Layout, "Location",
                                               {"Smallest Z", "Largest Z"});
    componentList_ = DialogHelper::dropListItem(line1Layout, "Component",
                                                {"X", "Y"});
    propertyList_ = DialogHelper::dropListItem(line1Layout, "property",
                                               {"Transmittance", "Phase Shift"});
    line1Layout->addStretch();
    // Line 2
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    filterTypeList_ = DialogHelper::dropListItem(line2Layout, "Filter Type",
                                                 {"Butterworth", "Chebyshev I",
                                                  "Chebyshev II", "Elliptical", "Bessel"});
    filterModeList_ = DialogHelper::dropListItem(line2Layout, "Mode",
                                                 {"Low Pass", "High Pass"});
    orderEdit_ = DialogHelper::sizeTItem(line2Layout, "Order", 1U);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    layout_->addLayout(line3Layout);
    cutOffFrequencyEdit_ = DialogHelper::doubleItem(line3Layout, "Cut Off Freq (Hz)");
    maxFrequencyEdit_ = DialogHelper::doubleItem(line3Layout, "Maximum Freq (Hz)");
    // Line 4
    auto line4Layout = new QHBoxLayout;
    layout_->addLayout(line4Layout);
    passbandRippleEdit_ = DialogHelper::doubleItem(line4Layout, "Passband Ripple (dB)");
    // The display
    fitnessWidget_ = new FitnessWidget(this);
    layout_->addWidget(fitnessWidget_);
    layout_->addStretch();
    // Results
    errorEdit_ = DialogHelper::doubleItem(layout_, "Error", true);
    dataView_ = new DataSeriesWidget(dlg->model()->guiConfiguration());
    layout_->addWidget(dataView_);
    // Connect the handlers
    connect(sensorList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(locationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(componentList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(dataSourceList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(propertyList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(filterTypeList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(filterModeList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(orderEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cutOffFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(maxFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(passbandRippleEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    // Initialise the dialog
    initialise();
}

// Draw the fitness information on the widget
void FilterAnalyserDlg::paintFitnessWidget() {
    // Make sure the filter has data
    analyser_->initialise();
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    double scale;
    double offset;
    switch(analyser_->property()) {
        case sensor::FilterAnalyser::Property::transmittance:
            scale = h / 1.0;
            offset = 0.0;
            break;
        case sensor::FilterAnalyser::Property::phaseShift:
            scale = h / 360.0;
            offset = 180.0;
            break;
    }
    // Black background
    painter.setPen(Qt::NoPen);
    if(dlg_->model()->guiConfiguration()->whiteBackground()) {
        painter.setBrush(Qt::white);
    } else {
        painter.setBrush(Qt::black);
    }
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(analyser_ != nullptr) {
        size_t numPoints = analyser_->reference().size();
        if(numPoints > 0) {
            // Line colours
            QPen functionPen;
            if(dlg_->model()->guiConfiguration()->whiteBackground()) {
                functionPen.setColor(Qt::black);
            } else {
                functionPen.setColor(Qt::green);
            }
            // Draw the data
            int lastRef = h - static_cast<int>(std::round(
                    (analyser_->reference()[0] + offset) * scale));
            int lastFreq = 0;
            for(size_t i = 1; i < numPoints; ++i) {
                int ref = h - static_cast<int>(std::round(
                        (analyser_->reference()[i] + offset) * scale));
                int freq = static_cast<int>(i) * w / static_cast<int>(numPoints);
                painter.setPen(functionPen);
                painter.drawLine(lastFreq, lastRef, freq, ref);
                lastRef = ref;
                lastFreq = freq;
            }
        }
    }
    // Clean up the context
    painter.restore();
}

// Initialise the GUI parameters
void FilterAnalyserDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    // Set parameters
    errorEdit_->setText(QString::number(analyser_->error()));
    locationList_->setCurrentIndex(static_cast<int>(analyser_->location()));
    componentList_->setCurrentIndex(static_cast<int>(analyser_->component()));
    dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    filterTypeList_->setCurrentIndex(static_cast<int>(analyser_->filterType()));
    filterModeList_->setCurrentIndex(static_cast<int>(analyser_->filterMode()));
    propertyList_->setCurrentIndex(static_cast<int>(analyser_->property()));
    orderEdit_->setText(QString::number(analyser_->order()));
    cutOffFrequencyEdit_->setText(analyser_->cutOffFrequency().text().c_str());
    maxFrequencyEdit_->setText(analyser_->maxFrequency().text().c_str());
    passbandRippleEdit_->setText(analyser_->passbandRipple().text().c_str());
    fitnessWidget_->update();
    cutOffFrequencyEdit_->setToolTip(QString::number(analyser_->cutOffFrequency().value()));
    maxFrequencyEdit_->setToolTip(QString::number(analyser_->maxFrequency().value()));
    passbandRippleEdit_->setToolTip(QString::number(analyser_->passbandRipple().value()));
}

// An edit box has changed
void FilterAnalyserDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newLocation = static_cast<sensor::Analyser::SensorLocation>(
                c.testInt(locationList_, static_cast<int>(analyser_->location()),
                          FdTdLife::notifyMinorChange));
        auto newComponent = static_cast<sensor::Analyser::SensorComponent>(
                c.testInt(componentList_,
                          static_cast<int>(analyser_->component()),
                          FdTdLife::notifyMinorChange));
        int newSensorId = dlg_->model()->selected(sensorList_);
        c.externalTest(newSensorId != analyser_->sensorId(),
                       FdTdLife::notifyMinorChange);
        auto newProperty = static_cast<sensor::FilterAnalyser::Property>(
                c.testInt(propertyList_,
                          static_cast<int>(analyser_->property()),
                          FdTdLife::notifyMinorChange));
        auto newFilterType = static_cast<sensor::FilterAnalyser::FilterType>(
                c.testInt(filterTypeList_,
                          static_cast<int>(analyser_->filterType()),
                          FdTdLife::notifyMinorChange));
        auto newFilterMode = static_cast<sensor::FilterAnalyser::FilterMode>(
                c.testInt(filterModeList_,
                          static_cast<int>(analyser_->filterMode()),
                          FdTdLife::notifyMinorChange));
        auto newOrder = c.testSizeT(orderEdit_, analyser_->order(),
                                    FdTdLife::notifyMinorChange);
        auto newCutOffFrequency = c.testString(cutOffFrequencyEdit_,
                                               analyser_->cutOffFrequency().text(),
                                               FdTdLife::notifyMinorChange);
        auto newMaxFrequency = c.testString(maxFrequencyEdit_,
                                            analyser_->maxFrequency().text(),
                                            FdTdLife::notifyMinorChange);
        auto newPassbandRipple = c.testString(passbandRippleEdit_,
                                              analyser_->passbandRipple().text(),
                                              FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            analyser_->sensorId(newSensorId);
            analyser_->location(newLocation);
            analyser_->component(newComponent);
            analyser_->property(newProperty);
            analyser_->filterType(newFilterType);
            analyser_->filterMode(newFilterMode);
            analyser_->order(newOrder);
            analyser_->cutOffFrequency().text(newCutOffFrequency);
            analyser_->maxFrequency().text(newMaxFrequency);
            analyser_->passbandRipple().text(newPassbandRipple);
            fitnessWidget_->update();
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FilterAnalyserDlg::tabChanged() {
    onChange();
}

// Handle a notification
void FilterAnalyserDlg::notify(fdtd::Notifier* source, int why,
                                      fdtd::NotificationData* /*data*/) {
    if(source == dlg_->model() && why == fdtd::Model::notifySensorChange) {
        InitialisingGui::Set s(initialising_);
        dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    } else if(source == dlg_->model() && why == fdtd::Model::notifyAnalysersCalculate) {
        errorEdit_->setText(QString::number(analyser_->error()));
        showData();
    }
}

// Show the data
void FilterAnalyserDlg::showData() {
    std::vector<double> min;
    std::vector<double> max;
    min.resize(analyser_->reference().size());
    max.resize(analyser_->reference().size());
    for(size_t i=0; i<analyser_->reference().size(); i++) {
        min[i] = analyser_->reference()[i] - analyser_->delta()[i];
        max[i] = analyser_->reference()[i] + analyser_->delta()[i];
    }
    dataView_->setData(analyser_->data(), GuiConfiguration::elementDataA,
                       true);
    dataView_->setData(min, GuiConfiguration::elementDataB,
                       false);
    dataView_->setData(max, GuiConfiguration::elementDataC,
                       false);
    switch(analyser_->property()) {
        case sensor::FilterAnalyser::Property::transmittance:
            dataView_->setAxisInfo(
                    analyser_->minX() / box::Constants::giga_,
                    analyser_->stepX() / box::Constants::giga_, "GHz",
                    analyser_->minY(), analyser_->maxY(), "");
            break;
        case sensor::FilterAnalyser::Property::phaseShift:
            dataView_->setAxisInfo(
                    analyser_->minX() / box::Constants::giga_,
                    analyser_->stepX() / box::Constants::giga_, "GHz",
                    analyser_->minY(), analyser_->maxY(), "rad");
            break;
    }
}
