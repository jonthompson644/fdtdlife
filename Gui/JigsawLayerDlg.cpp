/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "JigsawLayerDlg.h"
#include "DialogHelper.h"
#include <Domain/JigsawLayer.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"
#include "JigsawAdmittancesDlg.h"

// Constructor
void JigsawLayerDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::JigsawLayer*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Layer Position x,y,z (m)");
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    cellSizeEdit_ = DialogHelper::textItem(line2Layout, "Cell Size (m)");
    thicknessEdit_ = DialogHelper::textItem(line2Layout, "Plate Thickness (m)");
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment x,y,z (m)");
    std::tie(duplicateXEdit_, duplicateYEdit_, duplicateZEdit_) =
            DialogHelper::vectorItem(layout_, "Duplicate x,y,z");
    auto line5Layout = new QHBoxLayout;
    layout_->addLayout(line5Layout);
    plateSizeEdit_ = DialogHelper::textItem(line5Layout, "Plate Size (m)");
    protrusionSizeEdit_ = DialogHelper::textItem(line5Layout, "Protrusion Size (m)");
    auto line6Layout = new QHBoxLayout;
    layout_->addLayout(line6Layout);
    protrusionWidthEdit_ = DialogHelper::textItem(line6Layout, "Protrusion Width (m)");
    indentSizeEdit_ = DialogHelper::textItem(line6Layout, "Indent Size (m)");
    auto line7Layout = new QHBoxLayout;
    layout_->addLayout(line7Layout);
    indentWidthEdit_ = DialogHelper::textItem(line7Layout, "Indent Width (m)");
    indentPosEdit_ = DialogHelper::textItem(line7Layout, "Indent Pos (m)");
    auto line8Layout = new QHBoxLayout;
    layout_->addLayout(line8Layout);
    useSecondProtrusionCheck_ = DialogHelper::boolItem(line8Layout,
                                                       "Use Second Protrusion");
    protrusion2SizeEdit_ = DialogHelper::textItem(line8Layout,
                                                  "2nd Protrusion Size (m)");
    auto line9Layout = new QHBoxLayout;
    layout_->addLayout(line9Layout);
    indent2SizeEdit_ = DialogHelper::textItem(line9Layout, "2nd Indent Size (m)");
    indent2PosEdit_ = DialogHelper::textItem(line9Layout, "2nd Indent Pos (m)");
    auto line10Layout = new QHBoxLayout;
    layout_->addLayout(line10Layout);
    useThirdProtrusionCheck_ = DialogHelper::boolItem(line10Layout,
                                                       "Use Third Protrusion");
    protrusion3SizeEdit_ = DialogHelper::textItem(line10Layout,
                                                  "3rd Protrusion Size (m)");
    auto line11Layout = new QHBoxLayout;
    layout_->addLayout(line11Layout);
    indent3SizeEdit_ = DialogHelper::textItem(line11Layout, "3rd Indent Size (m)");
    indent3PosEdit_ = DialogHelper::textItem(line11Layout, "3rd Indent Pos (m)");
    auto line12Layout = new QHBoxLayout;
    layout_->addLayout(line12Layout);
    fillMaterialList_ = DialogHelper::dropListItem(line12Layout, "Fill Material");
    admittancesButton_ = DialogHelper::buttonItem(line12Layout, "Admittance Tables");
    // Finish and connect handlers
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(plateSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(protrusionSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(protrusionWidthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indentWidthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indentPosEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indentSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(thicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fillMaterialList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(useSecondProtrusionCheck_, SIGNAL(stateChanged(int)),
            SLOT(onChange()));
    connect(protrusion2SizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indent2PosEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indent2SizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(useThirdProtrusionCheck_, SIGNAL(stateChanged(int)),
            SLOT(onChange()));
    connect(protrusion3SizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indent3PosEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(indent3SizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(admittancesButton_, SIGNAL(clicked()), SLOT(onAdmittances()));
}

// Initialise the GUI parameters
void JigsawLayerDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    cellSizeEdit_->setText(shape_->cellSizeX().text().c_str());
    plateSizeEdit_->setText(shape_->plateSize().text().c_str());
    protrusionSizeEdit_->setText(shape_->protrusionSize().text().c_str());
    protrusionWidthEdit_->setText(shape_->protrusionWidth().text().c_str());
    indentWidthEdit_->setText(shape_->indentWidth().text().c_str());
    indentPosEdit_->setText(shape_->indentPos().text().c_str());
    indentSizeEdit_->setText(shape_->indentSize().text().c_str());
    thicknessEdit_->setText(shape_->layerThickness().text().c_str());
    dlg_->model()->fillMaterialList(fillMaterialList_, shape_->fillMaterial());
    incrementXEdit_->setText(shape_->increment().x().text().c_str());
    incrementYEdit_->setText(shape_->increment().y().text().c_str());
    incrementZEdit_->setText(shape_->increment().z().text().c_str());
    duplicateXEdit_->setText(shape_->duplicate().x().text().c_str());
    duplicateYEdit_->setText(shape_->duplicate().y().text().c_str());
    duplicateZEdit_->setText(shape_->duplicate().z().text().c_str());
    useSecondProtrusionCheck_->setChecked(shape_->useSecondProtrusion());
    protrusion2SizeEdit_->setText(shape_->protrusion2Size().text().c_str());
    indent2PosEdit_->setText(shape_->indent2Pos().text().c_str());
    indent2SizeEdit_->setText(shape_->indent2Size().text().c_str());
    useThirdProtrusionCheck_->setChecked(shape_->useThirdProtrusion());
    protrusion3SizeEdit_->setText(shape_->protrusion3Size().text().c_str());
    indent3PosEdit_->setText(shape_->indent3Pos().text().c_str());
    indent3SizeEdit_->setText(shape_->indent3Size().text().c_str());
    x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
    y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
    z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
    cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
    plateSizeEdit_->setToolTip(QString::number(shape_->plateSize().value()));
    protrusionSizeEdit_->setToolTip(QString::number(shape_->protrusionSize().value()));
    protrusionWidthEdit_->setToolTip(QString::number(shape_->protrusionWidth().value()));
    indentWidthEdit_->setToolTip(QString::number(shape_->indentWidth().value()));
    indentPosEdit_->setToolTip(QString::number(shape_->indentPos().value()));
    indentSizeEdit_->setToolTip(QString::number(shape_->indentSize().value()));
    incrementXEdit_->setToolTip(QString::number(shape_->increment().x().value()));
    incrementYEdit_->setToolTip(QString::number(shape_->increment().y().value()));
    incrementZEdit_->setToolTip(QString::number(shape_->increment().z().value()));
    duplicateXEdit_->setToolTip(QString::number(shape_->duplicate().x().value()));
    duplicateYEdit_->setToolTip(QString::number(shape_->duplicate().y().value()));
    duplicateZEdit_->setToolTip(QString::number(shape_->duplicate().z().value()));
    protrusion2SizeEdit_->setToolTip(QString::number(shape_->protrusion2Size().value()));
    indent2PosEdit_->setToolTip(QString::number(shape_->indent2Pos().value()));
    indent2SizeEdit_->setToolTip(QString::number(shape_->indent2Size().value()));
    protrusion3SizeEdit_->setToolTip(QString::number(shape_->protrusion3Size().value()));
    indent3PosEdit_->setToolTip(QString::number(shape_->indent3Pos().value()));
    indent3SizeEdit_->setToolTip(QString::number(shape_->indent3Size().value()));
}

// An edit box has changed
void JigsawLayerDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCellSize = c.testString(cellSizeEdit_, shape_->cellSizeX().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newPlateSize = c.testString(plateSizeEdit_,
                                         shape_->plateSize().text(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newProtrusionSize = c.testString(protrusionSizeEdit_,
                                              shape_->protrusionSize().text(),
                                              FdTdLife::notifyDomainContentsChange);
        auto newIndentWidth = c.testString(indentWidthEdit_,
                                           shape_->indentWidth().text(),
                                           FdTdLife::notifyDomainContentsChange);
        auto newIndentPos = c.testString(indentPosEdit_,
                                         shape_->indentPos().text(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newIndentSize = c.testString(indentSizeEdit_,
                                          shape_->indentSize().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newProtrusionWidth = c.testString(protrusionWidthEdit_,
                                               shape_->protrusionWidth().text(),
                                               FdTdLife::notifyDomainContentsChange);
        auto newThickness = c.testString(thicknessEdit_,
                                         shape_->layerThickness().text(),
                                         FdTdLife::notifyDomainContentsChange);
        int newFillMaterialIndex = dlg_->model()->selected(fillMaterialList_);
        c.externalTest(newFillMaterialIndex != shape_->fillMaterial(),
                       FdTdLife::notifyDomainContentsChange);
        auto newincrementX = c.testString(incrementXEdit_,
                                          shape_->increment().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementY = c.testString(incrementYEdit_,
                                          shape_->increment().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementZ = c.testString(incrementZEdit_,
                                          shape_->increment().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateX = c.testString(duplicateXEdit_,
                                          shape_->duplicate().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateY = c.testString(duplicateYEdit_,
                                          shape_->duplicate().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateZ = c.testString(duplicateZEdit_,
                                          shape_->duplicate().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newUseSecondProtrusion = c.testBool(useSecondProtrusionCheck_,
                                                 shape_->useSecondProtrusion(),
                                                 FdTdLife::notifyDomainContentsChange);
        auto newProtrusion2Size = c.testString(protrusion2SizeEdit_,
                                              shape_->protrusion2Size().text(),
                                              FdTdLife::notifyDomainContentsChange);
        auto newIndent2Pos = c.testString(indent2PosEdit_,
                                         shape_->indent2Pos().text(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newIndent2Size = c.testString(indent2SizeEdit_,
                                          shape_->indent2Size().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newUseThirdProtrusion = c.testBool(useThirdProtrusionCheck_,
                                                 shape_->useThirdProtrusion(),
                                                 FdTdLife::notifyDomainContentsChange);
        auto newProtrusion3Size = c.testString(protrusion3SizeEdit_,
                                               shape_->protrusion3Size().text(),
                                               FdTdLife::notifyDomainContentsChange);
        auto newIndent3Pos = c.testString(indent3PosEdit_,
                                          shape_->indent3Pos().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newIndent3Size = c.testString(indent3SizeEdit_,
                                           shape_->indent3Size().text(),
                                           FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->cellSizeX(box::Expression(newCellSize));
            shape_->plateSize().text(newPlateSize);
            shape_->protrusionSize().text(newProtrusionSize);
            shape_->indentWidth().text(newIndentWidth);
            shape_->indentPos().text(newIndentPos);
            shape_->indentSize().text(newIndentSize);
            shape_->protrusionWidth().text(newProtrusionWidth);
            shape_->layerThickness(box::Expression(newThickness));
            shape_->fillMaterial(newFillMaterialIndex);
            shape_->increment().x().text(newincrementX);
            shape_->increment().y().text(newincrementY);
            shape_->increment().z().text(newincrementZ);
            shape_->duplicate().x().text(newduplicateX);
            shape_->duplicate().y().text(newduplicateY);
            shape_->duplicate().z().text(newduplicateZ);
            shape_->useSecondProtrusion(newUseSecondProtrusion);
            shape_->protrusion2Size().text(newProtrusion2Size);
            shape_->indent2Pos().text(newIndent2Pos);
            shape_->indent2Size().text(newIndent2Size);
            shape_->useThirdProtrusion(newUseThirdProtrusion);
            shape_->protrusion3Size().text(newProtrusion3Size);
            shape_->indent3Pos().text(newIndent3Pos);
            shape_->indent3Size().text(newIndent3Size);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void JigsawLayerDlg::tabChanged() {
    onChange();
}

// The admittances button has been clicked.
void JigsawLayerDlg::onAdmittances() {
    JigsawAdmittancesDlg box(this, shape_);
    box.exec();
}
