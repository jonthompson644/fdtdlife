/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SEQUENCERSDLG_H
#define FDTDLIFE_SEQUENCERSDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "FdTdLife.h"
#include "PushButtonMenu.h"
#include "DialogFactory.h"
#include <Notifiable.h>

class FdTdLife;
class SequencerDlg;
namespace seq {class Sequencer;}

// Main tab panel for the sequencers
class SequencersDlg: public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit SequencersDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    void fillSequencerTree(seq::Sequencer* sel=nullptr);

protected slots:
    void onSequencerSel();
    void onNewSequencer(int item);
    void onDelete();
    void onCopy();
    void onPaste();
    void onClear();
    void onCsvFile();
    void onSequencerChanged(QTreeWidgetItem* item, int column);

protected:
    enum {
        sequencerColIdentifier=0, sequencerColName, sequencerColType,
        numSequencerCols
    };
    QTreeWidget* sequencersTree_;
    QVBoxLayout* sequencerDlgLayout_;
    SequencerDlg* sequencerDlg_;
    PushButtonMenu* newButton_;
    QPushButton* deleteButton_;
    QPushButton* copyButton_;
    QPushButton* pasteButton_;
    QPushButton* clearButton_;
    QPushButton* csvFileButton_;
    DialogFactory<SequencerDlg> dlgFactory_;

protected:
    seq::Sequencer* currentSequencer();
    QTreeWidgetItem* findSequencerInTree(seq::Sequencer* sequencer);
};


#endif //FDTDLIFE_SEQUENCERDLG_H
