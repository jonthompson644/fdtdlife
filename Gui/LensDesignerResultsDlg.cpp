/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensDesignerResultsDlg.h"
#include "DesignersDlg.h"
#include "DialogHelper.h"
#include <Designer/LensDesigner.h>
#include <Box/StlWriter.h>

// Constructor
LensDesignerResultsDlg::LensDesignerResultsDlg(LensDesignerDlg* dlg,
                                               designer::LensDesigner* designer) :
        LensDesignerDlg::SubDlg(dlg, designer),
        Notifiable(dlg->dlg()->model()) {
    // Create the controls
    auto line1Layout = new QHBoxLayout;
    layout_->addLayout(line1Layout);
    columns_ = new QTreeWidget;
    columns_->setSelectionMode(QAbstractItemView::SingleSelection);
    columns_->setColumnCount(resultsNumCols);
    columns_->setHeaderLabels({"Column", "c", "a", "Phase Shift"});
    columns_->setIndentation(0);
    line1Layout->addWidget(columns_);
    picture_ = new QLabel;
    line1Layout->addWidget(picture_);
    picture_->setFixedSize({250, 200});
    picture_->setScaledContents(true);
    picture_->setPixmap(QPixmap(":/images/LensletGeometrySummary.png"));
    auto buttonLayout = new QHBoxLayout;
    layout_->addLayout(buttonLayout);
    copyResults_ = DialogHelper::buttonItem(buttonLayout, "Copy Results");
    buttonLayout->addStretch();
    // Connect the handlers
    connect(copyResults_, SIGNAL(clicked()), SLOT(onCopyResults()));
    // Fill the panel
    initialise();
}

// Fill the dialog panel with information
void LensDesignerResultsDlg::initialise() {
    // Base class first
    LensDesignerDlg::SubDlg::initialise();
    // The results list
    fillResultsList();
}

// Fill the results list
void LensDesignerResultsDlg::fillResultsList() {
    columns_->clear();
    {
        InitialisingGui::Set s(initialising_);
        for(auto& c : dlg_->lensDesigner()->columns()) {
            auto item = new QTreeWidgetItem(columns_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(resultsColColumn, QString::number(c.index()));
            item->setText(resultsColC, QString::number(c.lm()));
            item->setText(resultsColA, QString::number(c.lm()));
            item->setText(resultsColPhaseShift, QString::number(c.lm()));
        }
    }
}

// Handle change notifications
void LensDesignerResultsDlg::notify(fdtd::Notifier* source, int why,
                                    fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model() && why == FdTdLife::notifySequencerChange) {
        fillResultsList();
    }
}

// Copy the results table to the clipboard
void LensDesignerResultsDlg::onCopyResults() {
    // Convert the result data into tab separated values
    std::stringstream t;
    t << "Column\tc\ta\tPhase Shift" << std::endl;
    for(auto& c : dlg_->lensDesigner()->columns()) {
        t << c.index() << '\t' << c.lm() << "\t" << c.lm() << "\t"
          << c.lm() << std::endl;
    }
    // Copy the data to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(t.str().c_str());
}

