/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSADMITTANCEDLG_H
#define FDTDLIFE_FITNESSADMITTANCEDLG_H

#include "QtIncludes.h"
#include "TabbedSubDlg.h"
namespace gs {class FitnessAdmittance;}
namespace gs {class FitnessBase;}
class GeneticSearchTargetDlg;

// Sub-dialog for the fitness functions that implement area under a curve
class FitnessAdmittanceDlg : public TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase> {
Q_OBJECT
protected:
    // Types
    class FitnessWidget : public QWidget {
    public:
        explicit FitnessWidget(FitnessAdmittanceDlg* dlg) : dlg_(dlg) {}
        QSize sizeHint() const override {return {200,200};}
        void paintEvent(QPaintEvent* /*event*/) override {dlg_->paintFitnessWidget();}
    protected:
        FitnessAdmittanceDlg* dlg_;
    };
    enum {fitnessPointsColFreq=0, fitnessPointsColReal, fitnessPointsColImag,
        numFitnessPointsCols};

protected:
    void paintFitnessWidget();

public:
    void construct(GeneticSearchTargetDlg* dlg, gs::FitnessBase* fitnessFunction) override;
    void tabChanged() override;
    void initialise() override;

protected slots:
    void onChange();

protected:
    QLineEdit* raEdit_ {};
    QLineEdit* rbEdit_ {};
    QLineEdit* rcEdit_ {};
    QLineEdit* rdEdit_ {};
    QLineEdit* reEdit_ {};
    QLineEdit* rfEdit_ {};
    QLineEdit* iaEdit_ {};
    QLineEdit* ibEdit_ {};
    QLineEdit* icEdit_ {};
    QLineEdit* idEdit_ {};
    QLineEdit* ieEdit_ {};
    QLineEdit* ifEdit_ {};
    QLineEdit* minRepeatCellScaleEdit_ {};
    QLineEdit* numRepeatCellScaleSamplesEdit_ {};
    FitnessWidget* fitnessWidget_ {};
    QLineEdit* fitnessFunctionWeightEdit_ {};
    gs::FitnessAdmittance* fitnessFunction_ {};
};

#endif //FDTDLIFE_FITNESSADMITTANCEDLG_H
