/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETICSEARCHPOPULATIONDLG_H
#define FDTDLIFE_GENETICSEARCHPOPULATIONDLG_H

#include "GeneticSearchSubDlg.h"
#include "DataSeriesWidget.h"
#include "TwoDSliceWidget.h"
#include "SubDialogBase.h"
#include "DialogFactory.h"
#include "PushButtonMenu.h"
#include "LayersList.h"
#include <Notifiable.h>
#include <memory>

class GeneticSearchSeqDlg;
namespace gs { class Individual; }
namespace gs { class FitnessData; }

// Main tab panel for the sequencers
class GeneticSearchPopulationDlg: public GeneticSearchSubDlg, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit GeneticSearchPopulationDlg(GeneticSearchSeqDlg* dlg);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    void updateIndividual(const std::shared_ptr<gs::Individual>& ind);

protected slots:
    void onStepBack();
    void onStepForward();
    void onFirst();
    void onLast();
    void onIndividualSelect();
    void onPopulationContext(const QPoint& pos);
    void onGenerationNumber();
    void onRecalculateUnfitness();
    void onEditIndividual();
    void onCommand(int cmd);
    void onLayersContext(const QPoint& pos);

protected:
    enum Columns {columnId=0, columnState, columnFitness, columnMother,
        columnFather, columnRepeatCell, numColumns};
    enum Commands {commandSavePopulation, commandRebuildHistory};
    QLineEdit* generationNumberEdit_;
    QLineEdit* minFitnessEdit_;
    QLineEdit* maxFitnessEdit_;
    QLineEdit* meanFitnessEdit_;
    QPushButton* stepBackButton_;
    QPushButton* stepForwardButton_;
    QPushButton* firstButton_;
    QPushButton* lastButton_;
    QTreeWidget* populationTree_;
    PushButtonMenu* commandButton_;
    LayersList* layersList_;
    //DataSeriesWidget* unfitnessProgressView_;
    //TwoDSliceWidget* twoDSliceView_;
    //DataSeriesWidget* transmittanceView_;
    //DataSeriesWidget* phaseShiftView_;
    QHBoxLayout* dataLayout_ {};
    std::list<QWidget*> dataItems_;
    typedef DialogFactory<SubDialogBase<GeneticSearchPopulationDlg, gs::FitnessData>> DataWidgetFactory;
    DataWidgetFactory dataWidgetFactory_;

protected:
    void fillPopulationList();
    void addIndividualToList(const std::shared_ptr<gs::Individual>& ind, QTreeWidgetItem* item=nullptr);
    void onCopyToClipboard();
    void onInitImprovement();
    std::shared_ptr<gs::Individual> currentIndividual();
    void plotIndividualData(const std::shared_ptr<gs::Individual>& ind);
};



#endif //FDTDLIFE_GENETICSEARCHPOPULATIONDLG_H
