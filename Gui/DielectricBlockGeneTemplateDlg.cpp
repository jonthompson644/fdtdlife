/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricBlockGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include <Gs/GeneTemplate.h>
#include "FdTdLife.h"

// Constructor
DielectricBlockGeneTemplateDlg::DielectricBlockGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                               gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    std::tie(permittivityBitsEdit_, permittivityMinEdit_, permittivityMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Relative Permittivity:");
    std::tie(centerXEdit_, centerYEdit_, centerZEdit_) =
            DialogHelper::vectorItem(layout_, "Center x,y,z (m)");
    std::tie(sizeXEdit_, sizeYEdit_, sizeZEdit_) =
            DialogHelper::vectorItem(layout_, "Size x,y,z (m)");
    connect(permittivityBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(permittivityMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(permittivityMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sizeXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sizeYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sizeZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void DielectricBlockGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    permittivityMinEdit_->setText(QString::number(geneTemplate_->permittivity().min()));
    permittivityMaxEdit_->setText(QString::number(geneTemplate_->permittivity().max()));
    permittivityBitsEdit_->setText(QString::number(geneTemplate_->permittivity().bits()));
    centerXEdit_->setText(QString::number(geneTemplate_->center().x()));
    centerYEdit_->setText(QString::number(geneTemplate_->center().y()));
    centerZEdit_->setText(QString::number(geneTemplate_->center().z()));
    sizeXEdit_->setText(QString::number(geneTemplate_->size().x()));
    sizeYEdit_->setText(QString::number(geneTemplate_->size().y()));
    sizeZEdit_->setText(QString::number(geneTemplate_->size().z()));
}

// An edit box has changed
void DielectricBlockGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newPermittivity = c.testGeneBitsSpecDouble(
                permittivityBitsEdit_, permittivityMinEdit_, permittivityMaxEdit_,
                geneTemplate_->permittivity(), FdTdLife::notifySequencerChange);
        auto newCenter = c.testVectorDouble(
                centerXEdit_, centerYEdit_, centerZEdit_,
                geneTemplate_->center(),
                FdTdLife::notifySequencerChange);
        auto newSize = c.testVectorDouble(
                sizeXEdit_, sizeYEdit_, sizeZEdit_,
                geneTemplate_->size(),
                FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->permittivity(newPermittivity);
            geneTemplate_->center(newCenter);
            geneTemplate_->size(newSize);
            dlg_->model()->doNotification(c.why());
        }
    }
}
