/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LensDesignerPhaseShiftsDlg.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include "DesignersDlg.h"
#include <Designer/LensDesigner.h>

// Constructor
LensDesignerPhaseShiftsDlg::LensDesignerPhaseShiftsDlg(LensDesignerDlg* dlg,
                                                       designer::LensDesigner* designer) :
        LensDesignerDlg::SubDlg(dlg, designer) {
    // Create the controls
    auto line1Layout = new QHBoxLayout;
    layout_->addLayout(line1Layout);
    permittivityAbove_ = DialogHelper::doubleItem(line1Layout,
                                                  "Relative Permittivity: Above");
    permittivityIn_ = DialogHelper::doubleItem(line1Layout,
                                               "Inside");
    permittivityBelow_ = DialogHelper::doubleItem(line1Layout,
                                                  "Below");
    auto line1aLayout = new QHBoxLayout;
    layout_->addLayout(line1aLayout);
    focusDistance_ = DialogHelper::doubleItem(line1aLayout, "Focal length (m)");
    layerSpacing_ = DialogHelper::doubleItem(line1aLayout, "Layer spacing (m)");
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    centerFrequency_ = DialogHelper::doubleItem(line2Layout, "Center frequency (Hz)");
    unitCell_ = DialogHelper::doubleItem(line2Layout, "Unit cell (m)");
    lensDiameter_ = DialogHelper::doubleItem(line2Layout, "Lens diameter (m)");
    auto line3Layout = new QHBoxLayout;
    layout_->addLayout(line3Layout);
    numLayers_ = DialogHelper::sizeTItem(line3Layout,
                                         "Total number of layers", 0);
    numArcStages_ = DialogHelper::intSpinItem(line3Layout, "Num ARC stages",
                                              0, 4, 1);
    layersPerArcStage_ = DialogHelper::sizeTItem(line3Layout,
                                                 "Layers per ARC stage", 0);
    auto line4Layout = new QHBoxLayout;
    layout_->addLayout(line4Layout);
    edgeDistance_ = DialogHelper::doubleItem(line4Layout, "Edge distance (m)");
    modelling_ = DialogHelper::dropListItem(
            line4Layout, "Modelling",
            {"Metamaterial cells", "Dielectric blocks", "Do Not Use",
             "Finger Patterns"});
    fdtdCellsPerUnitCell_ = DialogHelper::sizeTItem(
            line4Layout, "FDTD cells per unit cell", 0);
    auto line5Layout = new QHBoxLayout;
    layout_->addLayout(line5Layout);
    focalLengthFudgeFactor_ = DialogHelper::doubleItem(line5Layout,
                                                       "Focal Length Fudge Factor");
    plateThickness_ = DialogHelper::doubleItem(line5Layout, "Plate Thickness (m)");
    auto line5aLayout = new QHBoxLayout;
    layout_->addLayout(line5aLayout);
    sliceThickness_ = DialogHelper::intSpinItem(
            line5aLayout, "Slice thickness (unit cells)", 1, 2, 1);
    bitsPerHalfSide_ = DialogHelper::doubleItem(line5aLayout,
                                                "Bits per half side");
    minFeatureSize_ = DialogHelper::doubleItem(line5aLayout,
                                                "Min feature size (m)");
    errors_ = DialogHelper::textItem(layout_, "Errors", true);
    auto line6Layout = new QHBoxLayout;
    layout_->addLayout(line6Layout);
    generate_ = DialogHelper::buttonItem(line6Layout, "Generate");
    line6Layout->addStretch();
    // Connect the handlers
    connect(permittivityAbove_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(permittivityIn_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(permittivityBelow_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(focusDistance_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerFrequency_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(unitCell_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(lensDiameter_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layerSpacing_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numLayers_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layersPerArcStage_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fdtdCellsPerUnitCell_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(edgeDistance_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(focalLengthFudgeFactor_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(plateThickness_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(modelling_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(generate_, SIGNAL(clicked()), SLOT(onGenerate()));
    connect(numArcStages_, SIGNAL(valueChanged(int)), SLOT(onChange()));
    connect(sliceThickness_, SIGNAL(valueChanged(int)), SLOT(onChange()));
    connect(bitsPerHalfSide_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(minFeatureSize_, SIGNAL(editingFinished()), SLOT(onChange()));
    // Fill the panel
    LensDesignerPhaseShiftsDlg::initialise();
}

// Initialise the panel from the configuration
void LensDesignerPhaseShiftsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    permittivityAbove_->setText(QString::number(dlg_->lensDesigner()->permittivityAbove()));
    permittivityIn_->setText(QString::number(dlg_->lensDesigner()->permittivityIn()));
    permittivityBelow_->setText(QString::number(dlg_->lensDesigner()->permittivityBelow()));
    focusDistance_->setText(QString::number(dlg_->lensDesigner()->focusDistance()));
    centerFrequency_->setText(QString::number(dlg_->lensDesigner()->centerFrequency()));
    unitCell_->setText(QString::number(dlg_->lensDesigner()->unitCell()));
    lensDiameter_->setText(QString::number(dlg_->lensDesigner()->lensDiameter()));
    layerSpacing_->setText(QString::number(dlg_->lensDesigner()->layerSpacing()));
    numLayers_->setText(QString::number(dlg_->lensDesigner()->numLayers()));
    layersPerArcStage_->setText(QString::number(dlg_->lensDesigner()->layersPerArcStage()));
    fdtdCellsPerUnitCell_->setText(
            QString::number(dlg_->lensDesigner()->fdtdCellsPerUnitCell()));
    edgeDistance_->setText(QString::number(dlg_->lensDesigner()->edgeDistance()));
    modelling_->setCurrentIndex(static_cast<int>(dlg_->lensDesigner()->modelling()));
    focalLengthFudgeFactor_->setText(
            QString::number(dlg_->lensDesigner()->focalLengthFudgeFactor()));
    plateThickness_->setText(QString::number(dlg_->lensDesigner()->plateThickness()));
    errors_->setText(dlg_->lensDesigner()->error().c_str());
    numArcStages_->setValue(static_cast<int>(dlg_->lensDesigner()->numArcStages()));
    sliceThickness_->setValue(dlg_->lensDesigner()->sliceThickness());
    bitsPerHalfSide_->setText(QString::number(dlg_->lensDesigner()->bitsPerHalfSide()));
    minFeatureSize_->setText(QString::number(dlg_->lensDesigner()->minFeatureSize()));
}

// An element has changed
void LensDesignerPhaseShiftsDlg::onChange() {
    if(!initialising_) {
        // Now my stuff
        GuiChangeDetector c;
        auto newPermittivityAbove = c.testDouble(
                permittivityAbove_,
                dlg_->lensDesigner()->permittivityAbove(),
                FdTdLife::notifyMinorChange);
        auto newPermittivityIn = c.testDouble(
                permittivityIn_,
                dlg_->lensDesigner()->permittivityIn(),
                FdTdLife::notifyMinorChange);
        auto newPermittivityBelow = c.testDouble(
                permittivityBelow_,
                dlg_->lensDesigner()->permittivityBelow(),
                FdTdLife::notifyMinorChange);
        auto newFocusDistance = c.testDouble(focusDistance_,
                                             dlg_->lensDesigner()->focusDistance(),
                                             FdTdLife::notifyMinorChange);
        auto newCenterFrequency = c.testDouble(centerFrequency_,
                                               dlg_->lensDesigner()->centerFrequency(),
                                               FdTdLife::notifyMinorChange);
        auto newUnitCell = c.testDouble(
                unitCell_, dlg_->lensDesigner()->unitCell(),
                FdTdLife::notifyMinorChange);
        auto newLensDiameter = c.testDouble(lensDiameter_,
                                            dlg_->lensDesigner()->lensDiameter(),
                                            FdTdLife::notifyMinorChange);
        auto newLayerSpacing = c.testDouble(layerSpacing_,
                                            dlg_->lensDesigner()->layerSpacing(),
                                            FdTdLife::notifyMinorChange);
        auto newNumLayers = c.testSizeT(
                numLayers_, dlg_->lensDesigner()->numLayers(),
                FdTdLife::notifyMinorChange);
        auto newLayersPerArcStage = c.testSizeT(
                layersPerArcStage_,
                dlg_->lensDesigner()->layersPerArcStage(),
                FdTdLife::notifyMinorChange);
        auto newFdtdCellsPerUnitCell = c.testSizeT(
                fdtdCellsPerUnitCell_,
                dlg_->lensDesigner()->fdtdCellsPerUnitCell(),
                FdTdLife::notifyMinorChange);
        auto newEdgeDistance = c.testDouble(edgeDistance_,
                                            dlg_->lensDesigner()->edgeDistance(),
                                            FdTdLife::notifyMinorChange);
        auto newModelling = static_cast<designer::LensDesigner::Modelling>(c.testInt(
                modelling_, static_cast<int>(dlg_->lensDesigner()->modelling()),
                FdTdLife::notifyMinorChange));
        auto newFocalLengthFudgeFactor = c.testDouble(
                focalLengthFudgeFactor_,
                dlg_->lensDesigner()->focalLengthFudgeFactor(),
                FdTdLife::notifyMinorChange);
        auto newPlateThickness = c.testDouble(plateThickness_,
                                              dlg_->lensDesigner()->plateThickness(),
                                              FdTdLife::notifyMinorChange);
        auto newNumArcStages = static_cast<size_t>(
                c.testInt(numArcStages_,
                          static_cast<int>(dlg_->lensDesigner()->numArcStages()),
                          FdTdLife::notifyMinorChange));
        auto newSliceThickness = static_cast<size_t>(
                c.testInt(sliceThickness_,
                          static_cast<int>(dlg_->lensDesigner()->sliceThickness()),
                          FdTdLife::notifyMinorChange));
        auto newBitsPerHalfSide = c.testSizeT(
                bitsPerHalfSide_,
                dlg_->lensDesigner()->bitsPerHalfSide(),
                FdTdLife::notifyMinorChange);
        auto newMinFeatureSize = c.testDouble(
                minFeatureSize_,dlg_->lensDesigner()->minFeatureSize(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            dlg_->lensDesigner()->permittivityAbove(newPermittivityAbove);
            dlg_->lensDesigner()->permittivityIn(newPermittivityIn);
            dlg_->lensDesigner()->permittivityBelow(newPermittivityBelow);
            dlg_->lensDesigner()->focusDistance(newFocusDistance);
            dlg_->lensDesigner()->centerFrequency(newCenterFrequency);
            dlg_->lensDesigner()->unitCell(newUnitCell);
            dlg_->lensDesigner()->lensDiameter(newLensDiameter);
            dlg_->lensDesigner()->layerSpacing(newLayerSpacing);
            dlg_->lensDesigner()->numLayers(newNumLayers);
            dlg_->lensDesigner()->numArcStages(newNumArcStages);
            dlg_->lensDesigner()->layersPerArcStage(newLayersPerArcStage);
            dlg_->lensDesigner()->fdtdCellsPerUnitCell(newFdtdCellsPerUnitCell);
            dlg_->lensDesigner()->edgeDistance(newEdgeDistance);
            dlg_->lensDesigner()->modelling(newModelling);
            dlg_->lensDesigner()->focalLengthFudgeFactor(newFocalLengthFudgeFactor);
            dlg_->lensDesigner()->plateThickness(newPlateThickness);
            dlg_->lensDesigner()->sliceThickness(newSliceThickness);
            dlg_->lensDesigner()->bitsPerHalfSide(newBitsPerHalfSide);
            dlg_->lensDesigner()->minFeatureSize(newMinFeatureSize);
            dlg_->lensDesigner()->updateRefractiveIndices();
            errors_->setText(dlg_->lensDesigner()->error().c_str());
            // Tell others
            dlg_->dlg()->model()->doNotification(c.why());
        }
    }
}

// Generate a model from the current designer
void LensDesignerPhaseShiftsDlg::onGenerate() {
    dlg_->lensDesigner()->m()->designers().clearModel();
    dlg_->lensDesigner()->generatePhaseShift();
    InitialisingGui::Set s(initialising_);
    errors_->setText(dlg_->lensDesigner()->error().c_str());
    dlg_->lensDesigner()->m()->doNotification(FdTdLife::notifySequencerChange);
    dlg_->lensDesigner()->m()->doNotification(FdTdLife::notifyDomainContentsChange);
    dlg_->lensDesigner()->m()->doNotification(FdTdLife::notifyDomainSizeChange);
}



