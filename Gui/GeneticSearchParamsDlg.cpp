/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearchParamsDlg.h"
#include "GeneticSearchSeqDlg.h"
#include "SequencersDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneticSearch.h>
#include <Fdtd/Model.h>

// Constructor
GeneticSearchParamsDlg::GeneticSearchParamsDlg(GeneticSearchSeqDlg* dlg) :
        GeneticSearchSubDlg(dlg),
        Notifiable(dlg->dlg()->model()) {
    // The parameters
    auto paramLayout = new QVBoxLayout;
    // First row of config
    auto row0Layout = new QHBoxLayout;
    microGaCheck_ = DialogHelper::boolItem(row0Layout, "Micro GA");
    algorithmList_ = DialogHelper::dropListItem(row0Layout, "Algorithm");
    algorithmList_->addItems({"FDTD", "Propagation Matrix"});
    averageSensorCheck_ = DialogHelper::boolItem(row0Layout, "Average Sensor");
    xyComponentsCheck_ = DialogHelper::boolItem(row0Layout, "XY Components");
    useExistingDomainCheck_ = DialogHelper::boolItem(row0Layout, "Use Existing Domain");
    row0Layout->addStretch();
    paramLayout->addLayout(row0Layout);
    // Second row of config
    auto row1Layout = new QHBoxLayout;
    nCellsXYEdit_ = DialogHelper::intItem(row1Layout, "N Cells X&Y");
    cellSizeZEdit_ = DialogHelper::doubleItem(row1Layout, "Cell Size Z (m)");
    populationSizeEdit_ = DialogHelper::intItem(row1Layout, "Pop. Size");
    paramLayout->addLayout(row1Layout);
    // Third row of config
    auto row2Layout = new QHBoxLayout;
    gridMaterialList_ = DialogHelper::dropListItem(row2Layout, "Grid Material");
    frequencyRangeEdit_ = DialogHelper::doubleItem(row2Layout, "Freq Range (Hz)");
    numFrequenciesEdit_ = DialogHelper::intItem(row2Layout, "Num Frequencies");
    paramLayout->addLayout(row2Layout);
    // Fourth row of config
    auto row3Layout = new QHBoxLayout;
    crossOverProbabilityEdit_ = DialogHelper::doubleItem(row3Layout,
                                                         "Crossover Prob");
    mutationProbabilityEdit_ = DialogHelper::doubleItem(row3Layout,
                                                        "Mutation Prob");
    unfitnessThresholdEdit_ = DialogHelper::doubleItem(row3Layout,
                                                       "Completion Threshold");
    paramLayout->addLayout(row3Layout);
    // Fifth row of config
    auto row4Layout = new QHBoxLayout;
    selectionModeList_ = DialogHelper::dropListItem(row4Layout, "Selection Mode",
                                                    {"Fitness Proportional", "Tournament"});
    tournamentSizeEdit_ = DialogHelper::intItem(row4Layout, "Tournament Size");
    layerSymmetryList_ = DialogHelper::dropListItem(row4Layout, "Layer Symmetry",
                                                    {"None", "Even", "Odd"});
    paramLayout->addLayout(row4Layout);
    // Sixth row of config
    auto row5Layout = new QHBoxLayout;
    breedingPoolSizeEdit_ = DialogHelper::intItem(row5Layout, "Breeding Pool Size");
    crossoverModeList_ = DialogHelper::dropListItem(row5Layout, "Crossover Mode");
    crossoverModeList_->addItems({"Single", "Uniform"});
    keepParentsCheck_ = DialogHelper::boolItem(row5Layout, "Keep Parents");
    paramLayout->addLayout(row5Layout);
    // Seventh row of config
    auto row6Layout = new QHBoxLayout;
    sensorDistanceEdit_ = DialogHelper::doubleItem(row6Layout, "Sensor Dist (m)");
    repeatCellEdit_ = DialogHelper::doubleItem(row6Layout, "Repeat Cell(m)");
    layerSpacingEdit_ = DialogHelper::doubleItem(row6Layout, "Layer Spacing(m)");
    paramLayout->addLayout(row6Layout);
    // Seventh row of config
    auto row7Layout = new QHBoxLayout;
    collectFieldDataCheck_ = DialogHelper::boolItem(row7Layout, "Collect Field Data");
    paramLayout->addLayout(row7Layout);
    // Set the layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(keepParentsCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(microGaCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(averageSensorCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(xyComponentsCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(useExistingDomainCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(populationSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(breedingPoolSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(tournamentSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(nCellsXYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sensorDistanceEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(gridMaterialList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(frequencyRangeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numFrequenciesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(crossOverProbabilityEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(mutationProbabilityEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(unfitnessThresholdEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatCellEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(layerSpacingEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(selectionModeList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(algorithmList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(crossoverModeList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(layerSymmetryList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(collectFieldDataCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    // Fill the panel
    initialise();
}

// Initialise the panel from the configuration
void GeneticSearchParamsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    keepParentsCheck_->setChecked(dlg_->sequencer()->keepParents());
    microGaCheck_->setChecked(dlg_->sequencer()->microGa());
    averageSensorCheck_->setChecked(dlg_->sequencer()->averageSensor());
    xyComponentsCheck_->setChecked(dlg_->sequencer()->xyComponents());
    useExistingDomainCheck_->setChecked(dlg_->sequencer()->useExistingDomain());
    populationSizeEdit_->setText(QString::number(dlg_->sequencer()->populationSize()));
    breedingPoolSizeEdit_->setText(QString::number(dlg_->sequencer()->breedingPoolSize()));
    tournamentSizeEdit_->setText(QString::number(dlg_->sequencer()->tournamentSize()));
    selectionModeList_->setCurrentIndex(dlg_->sequencer()->selectionMode());
    algorithmList_->setCurrentIndex(dlg_->sequencer()->algorithm());
    crossoverModeList_->setCurrentIndex(dlg_->sequencer()->crossoverMode());
    nCellsXYEdit_->setText(QString::number(dlg_->sequencer()->nCellsXY()));
    cellSizeZEdit_->setText(QString::number(dlg_->sequencer()->cellSizeZ()));
    sensorDistanceEdit_->setText(QString::number(dlg_->sequencer()->sensorDistance()));
    frequencyRangeEdit_->setText(QString::number(dlg_->sequencer()->frequencyRange()));
    numFrequenciesEdit_->setText(QString::number(dlg_->sequencer()->numFrequencies()));
    crossOverProbabilityEdit_->setText(QString::number(dlg_->sequencer()->crossOverProbability()));
    mutationProbabilityEdit_->setText(QString::number(dlg_->sequencer()->mutationProbability()));
    unfitnessThresholdEdit_->setText(QString::number(dlg_->sequencer()->unfitnessThreshold()));
    repeatCellEdit_->setText(QString::number(dlg_->sequencer()->repeatCell()));
    layerSpacingEdit_->setText(QString::number(dlg_->sequencer()->layerSpacing()));
    layerSymmetryList_->setCurrentIndex(dlg_->sequencer()->layerSymmetry());
    collectFieldDataCheck_->setChecked(dlg_->sequencer()->collectFieldData());
    dlg_->dlg()->model()->fillMaterialList(gridMaterialList_,
                                           dlg_->sequencer()->gridMaterialId());
}

// An element has changed
void GeneticSearchParamsDlg::onChange() {
    if(!initialising_) {
        // New values
        GuiChangeDetector c;
        auto newPopulationSize = c.testSizeT(populationSizeEdit_, dlg_->sequencer()->populationSize(),
                                             FdTdLife::notifySequencerChange);
        auto newNCellsXY = c.testInt(nCellsXYEdit_, dlg_->sequencer()->nCellsXY(),
                                     FdTdLife::notifySequencerChange);
        auto newNumFrequencies = c.testSizeT(numFrequenciesEdit_, dlg_->sequencer()->numFrequencies(),
                                           FdTdLife::notifySequencerChange);
        auto newTournamentSize = c.testInt(tournamentSizeEdit_, dlg_->sequencer()->tournamentSize(),
                                           FdTdLife::notifySequencerChange);
        auto newBreedingPoolSize = c.testInt(breedingPoolSizeEdit_, dlg_->sequencer()->breedingPoolSize(),
                                             FdTdLife::notifySequencerChange);
        auto newSelectionMode = static_cast<gs::GeneticSearch::Selection>(c.testInt(
                selectionModeList_, dlg_->sequencer()->selectionMode(), FdTdLife::notifySequencerChange));
        auto newAlgorithm = static_cast<gs::GeneticSearch::Algorithm>(c.testInt(
                algorithmList_, dlg_->sequencer()->algorithm(), FdTdLife::notifySequencerChange));
        auto newCrossoverMode = static_cast<gs::GeneticSearch::Crossover>(c.testInt(
                crossoverModeList_, dlg_->sequencer()->crossoverMode(), FdTdLife::notifySequencerChange));
        auto newLayerSymmetry = static_cast<gs::GeneticSearch::LayerSymmetry>(c.testInt(
                layerSymmetryList_, dlg_->sequencer()->layerSymmetry(), FdTdLife::notifySequencerChange));
        auto newCellSizeZ = c.testDouble(cellSizeZEdit_, dlg_->sequencer()->cellSizeZ(),
                                         FdTdLife::notifySequencerChange);
        auto newSensorDistance = c.testDouble(sensorDistanceEdit_, dlg_->sequencer()->sensorDistance(),
                                              FdTdLife::notifySequencerChange);
        auto newFrequencyRange = c.testDouble(frequencyRangeEdit_, dlg_->sequencer()->frequencyRange(),
                                              FdTdLife::notifySequencerChange);
        auto newCrossOverProbability = c.testDouble(crossOverProbabilityEdit_,
                                                    dlg_->sequencer()->crossOverProbability(),
                                                    FdTdLife::notifySequencerChange);
        auto newMutationProbability = c.testDouble(mutationProbabilityEdit_,
                                                   dlg_->sequencer()->mutationProbability(),
                                                   FdTdLife::notifySequencerChange);
        auto newUnfitnessThreshold = c.testDouble(unfitnessThresholdEdit_,
                                                  dlg_->sequencer()->unfitnessThreshold(),
                                                  FdTdLife::notifySequencerChange);
        auto newRepeatCell = c.testDouble(repeatCellEdit_, dlg_->sequencer()->repeatCell(),
                                          FdTdLife::notifyMinorChange);
        auto newLayerSpacing = c.testDouble(layerSpacingEdit_, dlg_->sequencer()->layerSpacing(),
                                            FdTdLife::notifyMinorChange);
        auto newKeepParents = c.testBool(keepParentsCheck_, dlg_->sequencer()->keepParents(),
                                         FdTdLife::notifySequencerChange);
        auto newMicroGa = c.testBool(microGaCheck_, dlg_->sequencer()->microGa(),
                                     FdTdLife::notifySequencerChange);
        auto newAverageSensor = c.testBool(averageSensorCheck_, dlg_->sequencer()->averageSensor(),
                                           FdTdLife::notifySequencerChange);
        auto newXyComponents = c.testBool(xyComponentsCheck_, dlg_->sequencer()->xyComponents(),
                                          FdTdLife::notifySequencerChange);
        auto newUseExistingDomain = c.testBool(useExistingDomainCheck_, dlg_->sequencer()->useExistingDomain(),
                                          FdTdLife::notifySequencerChange);
        auto newCollectFieldData = c.testBool(collectFieldDataCheck_, dlg_->sequencer()->collectFieldData(),
                                          FdTdLife::notifySequencerChange);
        int newGridMaterialId = dlg_->dlg()->model()->selected(gridMaterialList_);
        c.externalTest(newGridMaterialId != dlg_->sequencer()->gridMaterialId(),
                       FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            dlg_->sequencer()->set(newPopulationSize, newNCellsXY, newCellSizeZ,
                                   newSensorDistance, newGridMaterialId,
                                   newFrequencyRange, newCrossOverProbability,
                                   newMutationProbability, newUnfitnessThreshold,
                                   newSelectionMode, newAlgorithm, newCrossoverMode,
                                   newTournamentSize, newBreedingPoolSize, newKeepParents,
                                   newMicroGa, newAverageSensor, newRepeatCell, newLayerSpacing);
            dlg_->sequencer()->layerSymmetry(newLayerSymmetry);
            dlg_->sequencer()->numFrequencies(newNumFrequencies);
            dlg_->sequencer()->xyComponents(newXyComponents);
            dlg_->sequencer()->useExistingDomain(newUseExistingDomain);
            dlg_->sequencer()->collectFieldData(newCollectFieldData);
            // Tell others
            dlg_->dlg()->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneticSearchParamsDlg::tabChanged() {
    onChange();
}

// Handle a configuration change notification
void GeneticSearchParamsDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        switch(why) {
            case FdTdLife::notifyDomainContentsChange: {
                InitialisingGui::Set s(initialising_);
                // The materials may have changed
                dlg_->dlg()->model()->fillMaterialList(gridMaterialList_, dlg_->sequencer()->gridMaterialId());
                break;
            }
            default:
                break;
        }
    }
}
