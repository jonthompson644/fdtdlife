/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceCatalogueSelectDlg.h"
#include "DialogHelper.h"
#include <AdmittanceCatalogue.h>
#include <CatalogueItem.h>

// Constructor
AdmittanceCatalogueSelectDlg::AdmittanceCatalogueSelectDlg(QWidget* parent, fdtd::AdmittanceCatalogue* catalogue) :
        QDialog(parent),
        catalogue_(catalogue),
        selectedItem_(nullptr) {
    auto mainLayout = new QVBoxLayout;
    // The catalogue tree
    catTree_ = new QTreeWidget;
    catTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    catTree_->setColumnCount(numCatCols);
    catTree_->setHeaderLabels({"Identifier", "Name"});
    catTree_->setFixedWidth(350);
    mainLayout->addWidget(catTree_);
    for(auto& catItem : catalogue_->items()) {
        auto item = new QTreeWidgetItem(catTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setText(catColCoding, QString::number(catItem->code()));
        item->setText(catColPattern, QString::number(catItem->pattern(), 16));
    }
    connect(catTree_, SIGNAL(itemSelectionChanged()), SLOT(onItemSelected()));
    // OK/Cancel buttons
    buttonBox_ = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(false);
    mainLayout->addWidget(buttonBox_);
    connect(buttonBox_, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox_, &QDialogButtonBox::rejected, this, &QDialog::reject);
    // Finish off the dialog
    setLayout(mainLayout);
    setWindowTitle("Select From Admittance Catalogue");
}

// The catalogue item selection has changed
void AdmittanceCatalogueSelectDlg::onItemSelected() {
    // Set the UI state according to the selected item
    selectedItem_.reset();
    auto item = catTree_->currentItem();
    if(item != nullptr) {
        uint64_t coding = item->text(catColCoding).toULongLong();
        selectedItem_ = catalogue_->get(coding);
    }
    buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(selectedItem_ != nullptr);
}
