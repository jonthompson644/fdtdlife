/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETICSEARCHGENETEMPLATEDLG_H
#define FDTDLIFE_GENETICSEARCHGENETEMPLATEDLG_H

#include "GeneticSearchSubDlg.h"

class GeneticSearchSeqDlg;
class GeneTemplateDlg;
class PushButtonMenu;
namespace gs {class GeneTemplate;}

// Main tab panel for the sequencers
class GeneticSearchGeneTemplateDlg: public GeneticSearchSubDlg {
Q_OBJECT
public:
    explicit GeneticSearchGeneTemplateDlg(GeneticSearchSeqDlg* dlg);
    void initialise() override;
    void tabChanged() override;

protected slots:
    void onGeneTemplateSelected();
    void onNewGeneTemplate(int templateType);
    void onDeleteGeneTemplate();
    void onDuplicateGeneTemplate();
    void onGeneTemplatedInserted(const QModelIndex &parent, int start, int end);

protected:
    enum {
        geneTemplateColPosition=0, geneTemplateColType, numGeneTemplateCols
    };
    enum {
        geneTemplateRepeatCell = 0, geneTemplateLayerSpacing, geneTemplateLayerPresent,
        geneTemplateSquarePlate, geneTemplateDielectricLayer,
        geneTemplateAdmittanceLayer, geneTemplateLayerSymmetry, geneTemplateBinaryPlateNxN,
        geneTemplateDuplicateLayer, geneTemplateAdmittanceFnLayer, geneTemplateDielectricBlock,
        geneTemplateLensDimensions, geneTemplateTransLensSquare, geneTemplateLongLensSquare,
        geneTemplateVariable
    };
    QTreeWidget* geneTemplateTree_;
    PushButtonMenu* newGeneTemplate_;
    QPushButton* deleteGeneTemplateButton_;
    QPushButton* duplicateGeneTemplateButton_;
    QHBoxLayout* geneTemplateDlgLayout_;
    GeneTemplateDlg* geneTemplateDlg_;

protected:
    void fillGeneTemplateTree(gs::GeneTemplate* sel = nullptr);
    gs::GeneTemplate* currentGeneTemplate();
    void initialiseGeneTemplate();
};


#endif //FDTDLIFE_GENETICSEARCHGENETEMPLATEDLG_H
