/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FitnessAnalyserDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessAnalyser.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor
void FitnessAnalyserDlg::construct(GeneticSearchTargetDlg* dlg,
                                   gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    fdtd::Notifiable::construct(dlg->model());
    fitnessFunction_ = dynamic_cast<gs::FitnessAnalyser*>(fitnessFunction);
    weightEdit_ = DialogHelper::doubleItem(layout_, "Weight");
    analyserList_ = DialogHelper::dropListItem(layout_, "Analyser");
    // Connect handlers
    connect(weightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(analyserList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    initialise();
}

// Initialise the GUI parameters
void FitnessAnalyserDlg::initialise() {
    if(fitnessFunction_ != nullptr) {
        InitialisingGui::Set s(initialising_);
        weightEdit_->setText(QString::number(fitnessFunction_->weight()));
        dlg_->model()->fillAnalyserList(analyserList_,
                fitnessFunction_->analyserId());
    }
}

// An edit box has changed
void FitnessAnalyserDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(weightEdit_, fitnessFunction_->weight(),
                                      FdTdLife::notifyMinorChange);
        int newAnalyserId = dlg_->model()->selected(analyserList_);
        c.externalTest(newAnalyserId != fitnessFunction_->analyserId(),
                       FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            fitnessFunction_->set(fitnessFunction_->name(), newWeight,
                                  fitnessFunction_->dataSource());
            fitnessFunction_->analyserId(newAnalyserId);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessAnalyserDlg::tabChanged() {
    onChange();
}

// Handle a notification
void FitnessAnalyserDlg::notify(fdtd::Notifier* source, int why,
                                fdtd::NotificationData* /*data*/) {
    if(source == dlg_->model() && why == fdtd::Model::notifyAnalyserChange) {
        InitialisingGui::Set s(initialising_);
        dlg_->model()->fillAnalyserList(analyserList_,
                fitnessFunction_->analyserId());
    }
}
