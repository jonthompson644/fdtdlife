/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DataSeriesWidget.h"
#include "GuiConfiguration.h"
#include <sstream>
#include <GtkGui/DataSeriesWidget.h>


// Constructor
DataSeriesWidget::DataSeriesWidget(GuiConfiguration* cfg) :
        cfg_(cfg) {
}

// Return a hint regarding the size of the widget
QSize DataSeriesWidget::sizeHint() const {
    return {200, 200};
}

// Paint the data
void DataSeriesWidget::paintEvent(QPaintEvent* /*event*/) {
    // Draw onto the screen
    QPainter painter(this);
    painter.save();
    painter.setFont(QFont("Arial", 8, QFont::Normal));
    painter.setBrush(cfg_->color(GuiConfiguration::elementBackground));
    painter.setPen(Qt::NoPen);
    painter.drawRect(0, 0, width(), height());
    // Can we calculate the scaling
    if(calcScaling()) {
        // Do all the drawing
        drawVerticalAxis(&painter);
        drawHorizontalAxis(&painter);
        drawData(&painter);
    }
    painter.restore();
}

// Calculate the scaling factors and tick mark intervals.
// Returns true if the data is reasonable and drawing should proceed.
bool DataSeriesWidget::calcScaling() {
    bool result = false;
    bool goodSize = !dataY_.empty();
    if(goodSize) {
        for(auto& d : dataY_) {
            goodSize = goodSize && d.data_.size() == dataY_[0].data_.size();
        }
    }
    if(goodSize) {
        result = true;
        // Data (y) axis scaling
        scaleY_ = 1.0;
        if((maxY_ - minY_) > 0.0) {
            scaleY_ = static_cast<double>(height()) / (maxY_ - minY_);
        }
        // Y axis tick marks
        tickStepY_ = pow(10.0, floor(log10(maxY_ - minY_)));
        if((maxY_ - minY_) / tickStepY_ < 3.0) {
            tickStepY_ /= 2.0;
        }
        // Time (x) axis scaling
        scaleX_ = static_cast<double>(width()) / (dataY_[0].data_.size() * stepX_);
        // X axis tick marks
        tickStepX_ = pow(10.0, floor(log10(dataY_[0].data_.size() * stepX_)));
        if((dataY_[0].data_.size() * stepX_) / tickStepX_ < 3.0) {
            tickStepX_ /= 2.0;
        }
        if((dataY_[0].data_.size() * stepX_) / tickStepX_ > 8.0) {
            tickStepX_ *= 2.0;
        }
    }
    return result;
}

// Draw the vertical axis with tick marks and annotation
void DataSeriesWidget::drawVerticalAxis(QPainter* painter) {
    // Draw the vertical axis
    QPen axisPen(cfg_->color(GuiConfiguration::elementAxis), 1);
    QPen textPen(cfg_->color(GuiConfiguration::elementText), 1);
    painter->save();
    double tick = (floor(minY_ / tickStepY_) + 1.0) * tickStepY_;
    while(tick < maxY_) {
        QString text = QString("%1%2").arg(tick).arg(unitsY_);
        int tickY = height() - static_cast<int>(round((tick - minY_) * scaleY_));
        painter->setPen(axisPen);
        painter->drawLine(0, tickY, width(), tickY);
        painter->setPen(textPen);
        painter->drawText(5, tickY, text);
        tick += tickStepY_;
    }
    painter->restore();
}

// Draw the horizontal axis with tick marks and annotation
void DataSeriesWidget::drawHorizontalAxis(QPainter* painter) {
    QPen axisPen(cfg_->color(GuiConfiguration::elementAxis), 1);
    QPen textPen(cfg_->color(GuiConfiguration::elementText), 1);
    painter->save();
    double tick = (floor(minX_ / tickStepX_) + 1.0) * tickStepX_;
    while(tick < (dataY_[0].data_.size() * stepX_)) {
        QString text = QString("%1%2").arg(tick).arg(unitsX_);
        int tickX = static_cast<int>(round((tick - minX_) * scaleX_));
        painter->setPen(axisPen);
        painter->drawLine(tickX, height(), tickX, 0);
        painter->setPen(textPen);
        painter->drawText(tickX - 10, height() - 5, text);
        tick += tickStepX_;
    }
    painter->restore();
}

// Draw the data, including the marker
void DataSeriesWidget::drawData(QPainter* painter) {
    // Draw the data
    painter->save();
    for(auto pos = dataY_.rbegin(); pos != dataY_.rend(); pos++) {
        if(!pos->data_.empty()) {
            QPen aPen(cfg_->color(pos->element_), 1);
            int x1, ya1 = 0, x2, ya2 = 0;
            x2 = 0;
            ya2 = height() - static_cast<int> (round((pos->data_[0] - minY_) * scaleY_));
            for(size_t t = 1; t < pos->data_.size(); t++) {
                x1 = x2;
                ya1 = ya2;
                x2 = static_cast<int>(round(static_cast<double>(t) * stepX_ * scaleX_));
                ya2 = height() - static_cast<int>(round((pos->data_[t] - minY_) * scaleY_));
                painter->setPen(aPen);
                painter->drawLine(x1, ya1, x2, ya2);
            }
        }
    }
    painter->restore();
}

// Set the data to display
void DataSeriesWidget::setData(const std::vector<double>& a,
                               GuiConfiguration::Element element,
                               bool clearFirst) {
    if(clearFirst) {
        dataY_.clear();
    }
    dataY_.emplace_back(a, element);
    update();
}

// Set the data to display
void DataSeriesWidget::setData(const std::vector<std::complex<double>>& a,
                               GuiConfiguration::Element realElement,
                               GuiConfiguration::Element imagElement,
                               bool clearFirst) {
    std::vector<double> r;
    std::vector<double> i;
    for(auto& c : a) {
        r.push_back(c.real());
        i.push_back(c.imag());
    }
    setData(r, realElement, clearFirst);
    setData(i, imagElement, false);
    update();
}

// Set the axis information
void DataSeriesWidget::setAxisInfo(double minX, double stepX, const char* unitsX,
                                   double minY, double maxY, const char* unitsY) {
    minX_ = minX;
    stepX_ = stepX;
    unitsX_ = unitsX;
    minY_ = minY;
    maxY_ = maxY;
    unitsY_ = unitsY;
    update();
}

// Remove all data from the widget
void DataSeriesWidget::clear() {
    dataY_.clear();
    update();
}

// The context menu is required
void DataSeriesWidget::contextMenuEvent(QContextMenuEvent* event) {
    QMenu menu;
    QAction* copyToClipboard = menu.addAction("Copy to clipboard");
    QAction* selectedAction = menu.exec(event->globalPos());
    if(selectedAction == copyToClipboard) {
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        std::stringstream text;
        text << "x";
        for(size_t chan = 0; chan < dataY_.size(); chan++) {
            text << "\ty" << chan;
        }
        text << std::endl;
        double x = minX_;
        for(size_t t = 0; t < dataY_[0].data_.size(); t++) {
            text << x;
            for(auto& d : dataY_) {
                text << "\t" << d.data_[t];
            }
            text << std::endl;
            x += stepX_;
        }
        mimeData->setText(text.str().c_str());
        mimeData->setImageData(grab().toImage());
        clipboard->setMimeData(mimeData);
    }
}
