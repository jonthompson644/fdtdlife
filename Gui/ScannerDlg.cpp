/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ScannerDlg.h"
#include "CharacterizerParamsDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include <Seq/Scanner.h>

// Constructor
ScannerDlg::ScannerDlg(CharacterizerParamsDlg* dlg, seq::Scanner* scanner) :
        dlg_(dlg),
        scanner_(scanner) {
    auto paramsLayout = new QVBoxLayout;
    layout_ = new QVBoxLayout;
    setLayout(paramsLayout);
    // The group box
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(layout_);
    stretchLayout->addStretch();
    scannerGroup_ = new QGroupBox;
    scannerGroup_->setLayout(stretchLayout);
    paramsLayout->addWidget(scannerGroup_);
    // Common stuff
    auto* line1Layout = DialogHelper::lineLayout(layout_);
    startStepEdit_ = DialogHelper::intItem(line1Layout, "Start Step");
    numStepsEdit_ = DialogHelper::intItem(line1Layout, "Number of Steps");
    // Connect handlers
    connect(startStepEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numStepsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void ScannerDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    startStepEdit_->setText(QString::number(scanner_->startStep()));
    numStepsEdit_->setText(QString::number(scanner_->numSteps()));
}

// A parameter has changed
void ScannerDlg::onChange() {
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newStartStep = c.testSizeT(startStepEdit_, scanner_->startStep(),
                                      FdTdLife::notifySequencerChange);
        auto newNumSteps = c.testSizeT(numStepsEdit_, scanner_->numSteps(),
                                      FdTdLife::notifySequencerChange);
        // Make the changes
        if (c.isChanged()) {
            scanner_->startStep(newStartStep);
            scanner_->numSteps(newNumSteps);
            // Tell others
            InitialisingGui::Set s(dlg_->initialising());
            dlg_->model()->doNotification(c.why());
        }
    }
}
