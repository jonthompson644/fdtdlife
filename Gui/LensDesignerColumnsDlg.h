/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H
#define FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H

#include "QtIncludes.h"
#include "LensDesignerDlg.h"
#include <Fdtd/Notifiable.h>

class LensDesignerColumnsDlg : public LensDesignerDlg::SubDlg, public fdtd::Notifiable {
Q_OBJECT
public:
    // Construction
    LensDesignerColumnsDlg(LensDesignerDlg* dlg, designer::LensDesigner* designer);

    // Overrides of LensDesignerSubDlg
    void initialise() override;

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // Handlers
protected slots:
    void onChange();
    void onCopyResults();
    void onPasteResults();
    void onExport(int index);
    void onCellChanged(QTreeWidgetItem* item, int column);
    void onGenerateGa(int index);
    void onFillModel(int index);

protected:
    // Controls
    enum {
        resultsColLayer, resultsColFirstColumn
    };
    enum {
        totalsColLayer, totalsColFirstColumn
    };
    enum {
        gaCommandColumn, gaCommandAdjustFull
    };
    enum {
        exportDiameterSliceStl, exportDiameterSliceDxf, exportQuadrantDxf,
        exportDiameterSliceHfss, exportQuadrantHfss, exportColumnHfss
    };
    enum {
        fillModelSquarePlates, fillModelDielectricBlocks,
        fillModelFingerPatterns
    };
    QTreeWidget* columns_{};  // The results list
    QTreeWidget* totals_{};  // The column phase shift totals
    QPushButton* copyResults_ {};  // Copy the results to the clipboard
    QPushButton* pasteResults_ {};  // Paste results from the clipboard
    QPushButton* export_ {};  // Export the results in various formats
    QPushButton* generateGa_ {};  // Generate a genetic algorithm configuration
    QPushButton* fillModel_ {};  // Fill the model using the current columns
    QCheckBox* exportSymmetricalHalf_{};
    QLineEdit* columnNumber_ {};
    QLineEdit* geneticRange_ {};
    QLineEdit* geneticBits_ {};

    // Helpers
    void fillResultsList();
    void fillTotalsList();
};


#endif //FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H
