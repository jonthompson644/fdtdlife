/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FingerPlateDlg.h"
#include "DialogHelper.h"
#include <Domain/FingerPlate.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"
#include "FingerPlateAdmittancesDlg.h"

// Constructor
void FingerPlateDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::FingerPlate*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Layer Position x,y,z (m)");
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    cellSizeEdit_ = DialogHelper::textItem(line2Layout, "Cell Size (m)");
    thicknessEdit_ = DialogHelper::textItem(line2Layout, "Plate Thickness (m)");
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment x,y,z (m)");
    std::tie(duplicateXEdit_, duplicateYEdit_, duplicateZEdit_) =
            DialogHelper::vectorItem(layout_, "Duplicate x,y,z");
    auto line5Layout = new QHBoxLayout;
    layout_->addLayout(line5Layout);
    plateSizeEdit_ = DialogHelper::textItem(line5Layout, "Plate Size (%)");
    minFeatureSizeEdit_ = DialogHelper::textItem(line5Layout, "Min Feature Size (%)");
    auto line6Layout = new QHBoxLayout;
    layout_->addLayout(line6Layout);
    fingerLeftCheck_ = DialogHelper::boolItem(line6Layout, "Fingers: Left");
    fingerRightCheck_ = DialogHelper::boolItem(line6Layout, "Right");
    fingerTopCheck_ = DialogHelper::boolItem(line6Layout, "Top");
    fingerBottomCheck_ = DialogHelper::boolItem(line6Layout, "Bottom");
    layout_->addStretch();
    auto line12Layout = new QHBoxLayout;
    layout_->addLayout(line12Layout);
    fillMaterialList_ = DialogHelper::dropListItem(line12Layout, "Fill Material");
    admittancesButton_ = DialogHelper::buttonItem(line12Layout, "Admittance Tables");
    // Finish and connect handlers
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(plateSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(minFeatureSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(thicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fillMaterialList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(admittancesButton_, SIGNAL(clicked()), SLOT(onAdmittances()));
    connect(fingerLeftCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(fingerRightCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(fingerTopCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(fingerBottomCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void FingerPlateDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    cellSizeEdit_->setText(shape_->cellSizeX().text().c_str());
    plateSizeEdit_->setText(shape_->plateSize().text().c_str());
    minFeatureSizeEdit_->setText(shape_->minFeatureSize().text().c_str());
    thicknessEdit_->setText(shape_->layerThickness().text().c_str());
    dlg_->model()->fillMaterialList(fillMaterialList_, shape_->fillMaterial());
    incrementXEdit_->setText(shape_->increment().x().text().c_str());
    incrementYEdit_->setText(shape_->increment().y().text().c_str());
    incrementZEdit_->setText(shape_->increment().z().text().c_str());
    duplicateXEdit_->setText(shape_->duplicate().x().text().c_str());
    duplicateYEdit_->setText(shape_->duplicate().y().text().c_str());
    duplicateZEdit_->setText(shape_->duplicate().z().text().c_str());
    fingerLeftCheck_->setChecked(shape_->fingersLeft());
    fingerRightCheck_->setChecked(shape_->fingersRight());
    fingerTopCheck_->setChecked(shape_->fingersTop());
    fingerBottomCheck_->setChecked(shape_->fingersBottom());
    x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
    y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
    z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
    cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
    plateSizeEdit_->setToolTip(QString::number(shape_->plateSize().value()));
    minFeatureSizeEdit_->setToolTip(QString::number(shape_->minFeatureSize().value()));
    incrementXEdit_->setToolTip(QString::number(shape_->increment().x().value()));
    incrementYEdit_->setToolTip(QString::number(shape_->increment().y().value()));
    incrementZEdit_->setToolTip(QString::number(shape_->increment().z().value()));
    duplicateXEdit_->setToolTip(QString::number(shape_->duplicate().x().value()));
    duplicateYEdit_->setToolTip(QString::number(shape_->duplicate().y().value()));
    duplicateZEdit_->setToolTip(QString::number(shape_->duplicate().z().value()));
}

// An edit box has changed
void FingerPlateDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCellSize = c.testString(cellSizeEdit_, shape_->cellSizeX().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newPlateSize = c.testString(plateSizeEdit_,
                                         shape_->plateSize().text(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newMinFeatureSize = c.testString(minFeatureSizeEdit_,
                                              shape_->minFeatureSize().text(),
                                              FdTdLife::notifyDomainContentsChange);
        auto newThickness = c.testString(thicknessEdit_,
                                         shape_->layerThickness().text(),
                                         FdTdLife::notifyDomainContentsChange);
        int newFillMaterialIndex = dlg_->model()->selected(fillMaterialList_);
        c.externalTest(newFillMaterialIndex != shape_->fillMaterial(),
                       FdTdLife::notifyDomainContentsChange);
        auto newincrementX = c.testString(incrementXEdit_,
                                          shape_->increment().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementY = c.testString(incrementYEdit_,
                                          shape_->increment().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementZ = c.testString(incrementZEdit_,
                                          shape_->increment().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateX = c.testString(duplicateXEdit_,
                                          shape_->duplicate().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateY = c.testString(duplicateYEdit_,
                                          shape_->duplicate().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateZ = c.testString(duplicateZEdit_,
                                          shape_->duplicate().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newFingersLeft = c.testBool(fingerLeftCheck_, shape_->fingersLeft(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newFingersRight = c.testBool(fingerRightCheck_, shape_->fingersRight(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newFingersTop = c.testBool(fingerTopCheck_, shape_->fingersTop(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newFingersBottom = c.testBool(fingerBottomCheck_, shape_->fingersBottom(),
                                        FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->cellSizeX(box::Expression(newCellSize));
            shape_->plateSize().text(newPlateSize);
            shape_->minFeatureSize().text(newMinFeatureSize);
            shape_->layerThickness(box::Expression(newThickness));
            shape_->fillMaterial(newFillMaterialIndex);
            shape_->increment().x().text(newincrementX);
            shape_->increment().y().text(newincrementY);
            shape_->increment().z().text(newincrementZ);
            shape_->duplicate().x().text(newduplicateX);
            shape_->duplicate().y().text(newduplicateY);
            shape_->duplicate().z().text(newduplicateZ);
            shape_->fingersLeft(newFingersLeft);
            shape_->fingersRight(newFingersRight);
            shape_->fingersTop(newFingersTop);
            shape_->fingersBottom(newFingersBottom);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FingerPlateDlg::tabChanged() {
    onChange();
}

// The admittances button has been clicked.
void FingerPlateDlg::onAdmittances() {
    FingerPlateAdmittancesDlg box(this, shape_);
    box.exec();
}
