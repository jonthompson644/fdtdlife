/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSPHASEANALYSERDLG_H
#define FDTDLIFE_LENSPHASEANALYSERDLG_H

#include <Sensor/LensPhaseAnalyser.h>
#include <Fdtd/Notifiable.h>
#include "QtIncludes.h"
#include "AnalysersDlg.h"
#include "DataSeriesWidget.h"

class LensPhaseAnalyserDlg : public AnalysersDlg::SubDlg, public fdtd::Notifiable {
    Q_OBJECT
public:
    // Construction
    LensPhaseAnalyserDlg() = default;
    void construct(AnalysersDlg* dlg, sensor::Analyser* d) override;

    // Overrides of AnalysersDlg::SubDlg
    void tabChanged() override;
    void initialise() override;

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // Handlers
    void onChange();

    // Getters
    sensor::LensPhaseAnalyser* lensDesigner() { return analyser_; }

protected:
    // Members
    sensor::LensPhaseAnalyser* analyser_{};
    QLineEdit* frequencyEdit_{};
    QComboBox* sensorList_{};
    QComboBox* locationList_{};
    QComboBox* componentList_{};
    QLineEdit* errorEdit_{};
    DataSeriesWidget* dataView_{};

    // Helpers
    void showData();
};


#endif //FDTDLIFE_LENSPHASEANALYSERDLG_H
