/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TABBEDSUBDLG_H
#define FDTDLIFE_TABBEDSUBDLG_H

#include "QtIncludes.h"
#include "InitialisingGui.h"

// A base class template for tabbed sub-dialogs.
template <class OwnerDlg, class ModelItem>
class TabbedSubDlg : public QWidget {
public:
    TabbedSubDlg() = default;
    TabbedSubDlg(OwnerDlg* dlg, ModelItem* item) {
        construct(dlg, item);
    }
    virtual void construct(OwnerDlg* dlg, ModelItem* item) {
        dlg_ = dlg;
        item_ = item;
        layout_ = new QVBoxLayout;
        auto* topLayout = new QVBoxLayout;
        topLayout->addLayout(layout_);
        topLayout->addStretch();
        setLayout(topLayout);
    }
    virtual void tabChanged() {
    }
    virtual void initialise() {
    }
    OwnerDlg* dlg() {
        return dlg_;
    }
    ModelItem* item() {
        return item_;
    }
protected:
    OwnerDlg* dlg_ {};
    ModelItem* item_ {};
    QVBoxLayout* layout_ {};
    InitialisingGui initialising_;
};

#endif //FDTDLIFE_TABBEDSUBDLG_H
