/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricLayerDlg.h"
#include "DialogHelper.h"
#include <Domain/DielectricLayer.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Constructor
void DielectricLayerDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape),
            shape_ = dynamic_cast<domain::DielectricLayer*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Layer Position x,y,z (m)");
    cellSizeEdit_ = DialogHelper::textItem(layout_, "Cell Size (m)");
    thicknessEdit_ = DialogHelper::textItem(layout_, "Thickness (m)");
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(thicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void DielectricLayerDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    cellSizeEdit_->setText(shape_->cellSizeX().text().c_str());
    thicknessEdit_->setText(shape_->thickness().text().c_str());
    x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
    y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
    z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
    cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
    thicknessEdit_->setToolTip(QString::number(shape_->thickness().value()));
}

// An edit box has changed
void DielectricLayerDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCellSize = c.testString(cellSizeEdit_, shape_->cellSizeX().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newThickness = c.testString(thicknessEdit_,
                                         shape_->thickness().text(),
                                         FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->cellSizeX(box::Expression(newCellSize));
            shape_->thickness(box::Expression(newThickness));
            x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
            y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
            z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
            cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
            thicknessEdit_->setToolTip(QString::number(shape_->thickness().value()));
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void DielectricLayerDlg::tabChanged() {
    onChange();
}
