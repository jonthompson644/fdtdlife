/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FITNESSFILTERDLG_H
#define FDTDLIFE_FITNESSFILTERDLG_H

#include "QtIncludes.h"
#include "TabbedSubDlg.h"
namespace gs {class FitnessFilter;}
namespace gs {class FitnessBase;}
class GeneticSearchTargetDlg;

// Sub-dialog for the filter response fitness function
class FitnessFilterDlg : public TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase> {
Q_OBJECT
protected:
    // Types
    class FitnessWidget : public QWidget {
    public:
        explicit FitnessWidget(FitnessFilterDlg* dlg) : dlg_(dlg) {}
        QSize sizeHint() const override {return {200,200};}
        void paintEvent(QPaintEvent* /*event*/) override {dlg_->paintFitnessWidget();}
        void contextMenuEvent(QContextMenuEvent* event) override {dlg_->fitnessWidgetContext(event);}
    protected:
        FitnessFilterDlg* dlg_;
    };

protected:
    void paintFitnessWidget();
    void fitnessWidgetContext(QContextMenuEvent* event);

public:
    void construct(GeneticSearchTargetDlg* dlg, gs::FitnessBase* fitnessFunction) override;
    void tabChanged() override;
    void initialise() override;

protected slots:
    void onChange();

protected:
    gs::FitnessFilter* fitnessFilter_ {};
    FitnessWidget* fitnessWidget_ {};
    QLineEdit* fitnessFunctionWeightEdit_ {};
    QComboBox* dataSourceList_ {};
    QComboBox* filterTypeList_ {};
    QComboBox* filterModeList_ {};
    QComboBox* propertyList_ {};
    QLineEdit* orderEdit_ {};
    QLineEdit* cutOffFrequencyEdit_ {};
    QLineEdit* maxFrequencyEdit_ {};
    QLineEdit* passbandRippleEdit_ {};
};


#endif //FDTDLIFE_FITNESSFILTERDLG_H
