/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Configuration.h>
#include "AttenuationView.h"
#include "AttenuationViewDlg.h"
#include <Xml/DomObject.h>
#include <cmath>

// Constructor
AttenuationView::AttenuationView(int identifier, int sensorId, const char *sensorData,
                                 ViewFrame *view, int numSeries) :
        PhaseShiftView(identifier, sensorId, sensorData, view, numSeries),
        yScaleType_(yScaleTypeTransmittance) {

}

// Update the view's configuration
void AttenuationView::set(XScaleType xScaleType, double xMin, double xMax, bool xAutomatic,
                         YScaleType yScaleType, double yMin, double yMax, bool yAutomatic) {
    yScaleType_ = yScaleType;
    PhaseShiftView::set(xScaleType, xMin, xMax, xAutomatic, yMin, yMax, yAutomatic);
}

// Convert a data value according to the scale type
double AttenuationView::convertYData(double value, YScaleType scaleType) {
    double result = value;
    switch(scaleType) {
        case yScaleTypeLinear:
            // The data supplied is now transmittance
            result = std::sqrt(result);
            break;
        case yScaleTypeTransmittance:
            // The data supplied is now transmittance
            break;
        case yScaleTypeDecibels:
            // The data supplied is now transmittance
            if(value > 0.0) {
                result = 10.0 * log10(value);
            } else {
                result = 0.0;
            }
            break;
    }
    return result;
}

// Calculate the scaling factors for the Y axis.
void AttenuationView::calcYScaling() {
    // Data (y) axis scaling
    if(!yAutomatic_ && (yMin_!=yMax_)) {
        info_.max_ = yMax_;
        info_.min_ = yMin_;
    }
    info_.max_ = convertYData(info_.max_, yScaleType_);
    info_.min_ = convertYData(info_.min_, yScaleType_);
    dataScale_ = 1.0;
    if (info_.max_ != info_.min_) {
        dataScale_ = (double) dataArea_.height() / (info_.max_ - info_.min_);
    }
    dataOffset_ = info_.min_;
    // Fix the units
    switch(yScaleType_) {
        case yScaleTypeLinear:
        case yScaleTypeTransmittance:
            info_.yUnits_ = "";
            break;
        case yScaleTypeDecibels:
            info_.yUnits_ = "dB";
            break;
    }
}

// Get the data points for an X coordinate
void AttenuationView::getData(int x, std::vector<double>& dy, double& dx) {
    PhaseShiftView::getData(x, dy, dx);
    for(auto& item : dy) {
        item = convertYData(item, yScaleType_);
    }
}

// Write the configuration to the XML DOM
void AttenuationView::writeConfig(xml::DomObject& root) const {
    // Base class first
    PhaseShiftView::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("yscaletype") << yScaleType_;
    root << xml::Close();
}

// Read the configuration from the XML DOM
void AttenuationView::readConfig(xml::DomObject& root) {
    // Base class first
    PhaseShiftView::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("yscaletype") >> yScaleType_ ;
    root >> xml::Close();
}

// Make the sub dialog for this view item
ViewItemDlg *AttenuationView::makeDialog(ViewFrameDlg *dlg) {
    return new AttenuationViewDlg(dlg, this);
}

