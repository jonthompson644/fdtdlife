/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ExtrudedPolygonDlg.h"
#include "DialogHelper.h"
#include <Domain/ExtrudedPolygon.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Constructor
void ExtrudedPolygonDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::ExtrudedPolygon*>(shape);
    // Line 1
    auto* line1Layout = new QHBoxLayout;
    centerXEdit_ = DialogHelper::doubleItem(line1Layout, "Center (m): X");
    centerZEdit_ = DialogHelper::doubleItem(line1Layout, "Z");
    layout_->addLayout(line1Layout);
    // Points tree
    pointsTree_ = new QTreeWidget;
    pointsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    pointsTree_->setColumnCount(pointsNumCols);
    pointsTree_->setHeaderLabels({"X Offset (m)", "Z Offset (m)"});
    pointsTree_->setFixedWidth(310);
    layout_->addWidget(pointsTree_);
    // Buttons
    auto pointButtonsLayout = new QHBoxLayout;
    newPointButton_ = DialogHelper::buttonItem(pointButtonsLayout, "New Point");
    deletePointButton_ = DialogHelper::buttonItem(pointButtonsLayout, "Delete Point");
    layout_->addLayout(pointButtonsLayout);
    // Connet
    connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(newPointButton_, SIGNAL(clicked()), SLOT(onNewPoint()));
    connect(deletePointButton_, SIGNAL(clicked()), SLOT(onDeletePoint()));
    connect(pointsTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onPointChanged(QTreeWidgetItem * , int)));
}

// Initialise the GUI parameters
void ExtrudedPolygonDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    centerXEdit_->setText(QString::number(shape_->center().x()));
    centerZEdit_->setText(QString::number(shape_->center().z()));
    fillPointsTree();
}

// The new point button has been pressed
void ExtrudedPolygonDlg::onNewPoint() {
    auto points = shape_->points();
    points.emplace_back(0.0, 0.0);
    shape_->points(points);
    fillPointsTree();
    dlg_->model()->doNotification(FdTdLife::notifyDomainContentsChange);
}

// The delete point button has been pressed
void ExtrudedPolygonDlg::onDeletePoint() {
    // Current point
    QTreeWidgetItem* item = pointsTree_->currentItem();
    if(item != nullptr) {
        int row = pointsTree_->indexOfTopLevelItem(item);
        auto points = shape_->points();
        if(row >= 0 && row < static_cast<int>(points.size())) {
            points.erase(points.begin() + row);
            shape_->points(points);
            fillPointsTree();
            dlg_->model()->doNotification(FdTdLife::notifyDomainContentsChange);
        }
    }
}

// Put all the fitness points in the fitness tree widget
void ExtrudedPolygonDlg::fillPointsTree() {
    InitialisingGui::Set s(dlg_->initialising());
    pointsTree_->clear();
    for(auto& point : shape_->points()) {
        auto item = new QTreeWidgetItem(pointsTree_);
        item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setText(pointsXCol, QString::number(point.x()));
        item->setText(pointsYCol, QString::number(point.y()));
    }
}

// A point tree item has been changed
void ExtrudedPolygonDlg::onPointChanged(QTreeWidgetItem* item, int /*column*/) {
    if(!dlg_->initialising()) {
        // Which row
        int row = pointsTree_->indexOfTopLevelItem(item);
        // Set the function point in this row
        double x = item->text(pointsXCol).toDouble();
        double y = item->text(pointsYCol).toDouble();
        auto points = shape_->points();
        if(row >= 0 && row < static_cast<int>(points.size())) {
            points[row].x(x);
            points[row].y(y);
            shape_->points(points);
            fillPointsTree();
            InitialisingGui::Set s(dlg_->initialising());
            dlg_->model()->doNotification(FdTdLife::notifyDomainContentsChange);
        }
    }
}

// An edit box has changed
void ExtrudedPolygonDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testDouble(centerXEdit_, shape_->center().x(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testDouble(centerZEdit_, shape_->center().z(),
                                       FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center({newCenterX, 0.0, newCenterZ});
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void ExtrudedPolygonDlg::tabChanged() {
    onChange();
}
