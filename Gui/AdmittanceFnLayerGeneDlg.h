/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCEFNLAYERGENEDLG_H
#define FDTDLIFE_ADMITTANCEFNLAYERGENEDLG_H

#include "QtIncludes.h"
#include "GeneDlg.h"

namespace gs { class AdmittanceFnLayerGene; }

// Dialog for the admittance function layer gene
class AdmittanceFnLayerGeneDlg : public GeneDlg {
Q_OBJECT
public:
    AdmittanceFnLayerGeneDlg(IndividualEditDlg* mainDlg, gs::AdmittanceFnLayerGene* gene);
    void updateIndividual() override;

public slots:
    void onMakeAdmittanceFunction();

protected:
    gs::AdmittanceFnLayerGene* gene_ {};
    QLineEdit* realAEdit_ {};
    QLineEdit* realBEdit_ {};
    QLineEdit* realCEdit_ {};
    QLineEdit* realDEdit_ {};
    QLineEdit* realEEdit_ {};
    QLineEdit* realFEdit_ {};
    QLineEdit* imagAEdit_ {};
    QLineEdit* imagBEdit_ {};
    QLineEdit* imagCEdit_ {};
    QLineEdit* imagDEdit_ {};
    QLineEdit* imagEEdit_ {};
    QLineEdit* imagFEdit_ {};
    QPushButton* makeAdmittanceFunctionButton_ {};
};


#endif //FDTDLIFE_ADMITTANCEFNLAYERGENEDLG_H
