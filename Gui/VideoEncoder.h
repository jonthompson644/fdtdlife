/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VIDEOENCODER_H
#define FDTDLIFE_VIDEOENCODER_H

#include "QtIncludes.h"

#if defined(__USEFFMPEG__)
extern "C" {
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
};
#endif

class VideoEncoder
{
protected:
#if defined(__USEFFMPEG__)
    AVCodec* codec;
    AVCodecContext* c;
    FILE* f;
    AVFrame* picture;
    AVPacket* pkt;
    SwsContext* swsContext;
    int width;
    int height;
    static const uint8_t endcode[];
    int frameNumber;
#endif

public:
    VideoEncoder();
    virtual ~VideoEncoder();
    bool openFile(const QString& filename, int width, int height, int framesPerSecond=25);
    bool addFrame(QImage* image);
    bool closeFile();

protected:
#if defined(__USEFFMPEG__)
    void encode(AVCodecContext* enc_ctx, AVFrame* frame, AVPacket* pkt, FILE* outfile);
#endif
};




#endif // FDTDLIFE_VIDEOENCODER_H
