/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearchTargetDlg.h"
#include "GeneticSearchSeqDlg.h"
#include "SequencersDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "FitnessFunctionDlg.h"
#include "Fitness3dBPointDlg.h"
#include "FitnessAdmittanceDlg.h"
#include "FitnessAreaIntensityDlg.h"
#include "FitnessIntensityLineDlg.h"
#include "FitnessInPhaseLineDlg.h"
#include "FitnessBirefPhaseDiffDlg.h"
#include "FitnessFilterDlg.h"
#include "FitnessAnalyserDlg.h"
#include "PushButtonMenu.h"
#include <Gs/GeneticSearch.h>
#include <Gs/FitnessFunction.h>
#include <Gs/FitnessBase.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Xml/DomObject.h>
#include <Xml/Writer.h>
#include <Gs/Individual.h>

// Constructor
GeneticSearchTargetDlg::GeneticSearchTargetDlg(GeneticSearchSeqDlg* dlg) :
        GeneticSearchSubDlg(dlg),
        Notifiable(dlg->dlg()->model()),
        fitnessFunctionDlg_(nullptr) {
    // The sub-dialog factory
    dlgFactory_.define(new FitnessDlgFactory::Creator<Fitness3dBPointDlg>(
            gs::FitnessFunction::mode3dBPoint));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessAreaIntensityDlg>(
            gs::FitnessFunction::modeAreaIntensity));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessIntensityLineDlg>(
            gs::FitnessFunction::modeIntensityLine));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessInPhaseLineDlg>(
            gs::FitnessFunction::modeInPhaseLine));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessAdmittanceDlg>(
            gs::FitnessFunction::modeAdmittance));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessFunctionDlg>(
            gs::FitnessFunction::modeAttenuation));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessFunctionDlg>(
            gs::FitnessFunction::modePhaseShift));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessBirefPhaseDiffDlg>(
            gs::FitnessFunction::modeBirefPhaseDiff));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessFunctionDlg>(
            gs::FitnessFunction::modePhaseShiftChange));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessFilterDlg>(
            gs::FitnessFunction::modeFilter));
    dlgFactory_.define(new FitnessDlgFactory::Creator<FitnessAnalyserDlg>(
            gs::FitnessFunction::modeAnalyser));
    // The main layout
    auto targetLayout = new QHBoxLayout;
    // The fitness function
    auto fitnessFunctionLayout = new QVBoxLayout;
    targetLayout->addLayout(fitnessFunctionLayout);
    fitnessFunctionTree_ = new QTreeWidget;
    fitnessFunctionTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    fitnessFunctionTree_->setColumnCount(numFitnessFunctionCols);
    fitnessFunctionTree_->setHeaderLabels({"Position", "Type"});
    fitnessFunctionTree_->setFixedWidth(250);
    fitnessFunctionLayout->addWidget(fitnessFunctionTree_);
    newFitnessFunctionButton_ = DialogHelper::buttonMenuItem(
            fitnessFunctionLayout, "New Fitness Function",
            dlg_->sequencer()->fitness()->factory().modeNames());
    deleteFunctionButton_ = DialogHelper::buttonItem(fitnessFunctionLayout,
                                                     "Delete Function");
    copyFunctionButton_ = DialogHelper::buttonItem(fitnessFunctionLayout,
                                                   "Copy Function");
    pasteFunctionButton_ = DialogHelper::buttonItem(fitnessFunctionLayout,
                                                    "Paste Function");
    evaluateNowButton_ = DialogHelper::buttonItem(fitnessFunctionLayout,
                                                  "Evaluate Now");
    // The sub-dialog place holder
    fitnessFunctionDlgLayout_ = new QVBoxLayout;
    targetLayout->addLayout(fitnessFunctionDlgLayout_);
    targetLayout->addStretch();
    // Set the layout
    setTabLayout(targetLayout);
    // Connect the handlers
    connect(newFitnessFunctionButton_, SIGNAL(clickedMenu(int)),
            SLOT(onNewFitnessFunction(int)));
    connect(deleteFunctionButton_, SIGNAL(clicked()), SLOT(onDeleteFunction()));
    connect(copyFunctionButton_, SIGNAL(clicked()), SLOT(onCopyFunction()));
    connect(pasteFunctionButton_, SIGNAL(clicked()), SLOT(onPasteFunction()));
    connect(evaluateNowButton_, SIGNAL(clicked()), SLOT(onEvaluateNow()));
    connect(fitnessFunctionTree_, SIGNAL(itemSelectionChanged()),
            SLOT(onFitnessFunctionSelected()));
    // Fill the panel
    initialise();
}

// Initialise the panel from the configuration
void GeneticSearchTargetDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fillFitnessFunctionTree();
    initialiseFunction();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneticSearchTargetDlg::tabChanged() {
    if(fitnessFunctionDlg_ != nullptr) {
        fitnessFunctionDlg_->tabChanged();
    }
}

// Return the currently selected fitness function
gs::FitnessBase* GeneticSearchTargetDlg::currentFitnessFunction() {
    gs::FitnessBase* result = nullptr;
    QTreeWidgetItem* selected = fitnessFunctionTree_->currentItem();
    if(selected != nullptr) {
        int position = fitnessFunctionTree_->indexOfTopLevelItem(selected);
        if(position >= 0) {
            result = dlg_->sequencer()->fitness()->getFunction(position);
        }
    }
    return result;
}

// Fill the fitness function tree widget
void GeneticSearchTargetDlg::fillFitnessFunctionTree(gs::FitnessBase* sel) {
    QTreeWidgetItem* selItem = nullptr;
    int index = 0;
    {
        InitialisingGui::Set s(initialising_);
        fitnessFunctionTree_->clear();
        for(auto& f : dlg_->sequencer()->fitness()->functions()) {
            auto item = new QTreeWidgetItem(fitnessFunctionTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(fitnessFunctionColPosition, QString::number(index));
            item->setText(fitnessFunctionColType,
                          dlg_->sequencer()->fitness()->factory().modeName(
                                  f->mode()).c_str());
            index++;
            if(f.get() == sel) {
                selItem = item;
            }
        }
    }
    if(selItem != nullptr) {
        fitnessFunctionTree_->setCurrentItem(selItem);
    } else if(!dlg_->sequencer()->fitness()->functions().empty()) {
        fitnessFunctionTree_->setCurrentItem(
                fitnessFunctionTree_->topLevelItem(0));
    }
}

// Initialise the dialog with the current function
void GeneticSearchTargetDlg::initialiseFunction() {
    // Clean up old dialog
    delete fitnessFunctionDlg_;
    fitnessFunctionDlg_ = nullptr;
    // Current function
    gs::FitnessBase* function = currentFitnessFunction();
    if(function != nullptr) {
        // Show the correct dialog
        fitnessFunctionDlg_ = dlgFactory_.make(this, function);
        if(fitnessFunctionDlg_ != nullptr) {
            fitnessFunctionDlgLayout_->addWidget(fitnessFunctionDlg_);
        }
    }
}

// The function selection has changed
void GeneticSearchTargetDlg::onFitnessFunctionSelected() {
    initialiseFunction();
}

// Create a new attenuation function
void GeneticSearchTargetDlg::onNewFitnessFunction(int fitnessFunctionType) {
    gs::FitnessBase* fitness = dlg_->sequencer()->fitness()->make(fitnessFunctionType);
    if(fitness != nullptr) {
        fillFitnessFunctionTree(fitness);
    }
}

// Copy a function to the clipboard
void GeneticSearchTargetDlg::onCopyFunction() {
    gs::FitnessBase* fitnessFunction = currentFitnessFunction();
    if(fitnessFunction != nullptr) {
        // Encode the fitness function into XML
        auto* o = new xml::DomObject("fitnessfunction");
        *o << *fitnessFunction;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string xmlText;
        writer.writeString(&dom, xmlText);
        QByteArray xmlData(xmlText.c_str());
        // Encode the fitness function data
        std::string tsvText = fitnessFunction->makeTsvData(
                dlg_->sequencer()->numFrequencies(),
                dlg_->sequencer()->frequencySpacing(),
                dlg_->sequencer()->frequencySpacing());
        QByteArray tsvData(tsvText.c_str());
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        mimeData->setData("text/plain", tsvData);
        mimeData->setData("fdtdlife/fitnessfunction", xmlData);
        clipboard->setMimeData(mimeData);
    }
}

// Paste a fitness function from the clipboard
void GeneticSearchTargetDlg::onPasteFunction() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/fitnessfunction")) {
        // Read the XML into the function
        std::string text = mimeData->data("fdtdlife/fitnessfunction").toStdString();
        xml::DomDocument dom("fitnessfunction");
        xml::Reader reader;
        try {
            reader.readString(&dom, text);
            gs::FitnessBase* fitnessFunction = dlg_->sequencer()->fitness()->make(
                    *dom.getObject());
            fillFitnessFunctionTree(fitnessFunction);
        } catch(xml::Exception& e) {
            std::cout << "Fitness function paste failed to read XML: " << e.what()
                      << std::endl;
        }
    }
}

// Delete a fitness function
void GeneticSearchTargetDlg::onDeleteFunction() {
    // Current function
    gs::FitnessBase* function = currentFitnessFunction();
    if(function != nullptr) {
        dlg_->sequencer()->fitness()->removeFunction(function);
        fillFitnessFunctionTree();
    }
}

// Handle a configuration change notification
void GeneticSearchTargetDlg::notify(fdtd::Notifier* source, int why,
                                    fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        if(why == FdTdLife::notifySequencerChange) {
            InitialisingGui::Set s(initialising_);
            // The fitness functions may have changed
            fillFitnessFunctionTree();
        }
    }
}

// Evaluate the currently selected fitness function now.
void GeneticSearchTargetDlg::onEvaluateNow() {
    gs::FitnessBase* function = currentFitnessFunction();
    if(function != nullptr) {
        std::unique_ptr<gs::Individual> ind(new gs::Individual(-1, nullptr));
        double unfitness = function->unfitness(ind.get());
        QMessageBox box(QMessageBox::Information, "Unfitness result",
                        QString::number(unfitness), QMessageBox::Ok);
        box.exec();
    }
}
