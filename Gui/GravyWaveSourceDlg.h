/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GRAVYWAVESOURCEDLG_H
#define FDTDLIFE_GRAVYWAVESOURCEDLG_H



#include "QtIncludes.h"
#include "SourceDlg.h"
namespace source {class GravyWaveSource;}

// Edit dialog for the zone source
class GravyWaveSourceDlg : public SourceDlg {
Q_OBJECT
public:
    // Construction
    GravyWaveSourceDlg(SourcesDlg* dlg, source::GravyWaveSource* source);

    // API
    void initialise() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // Handlers
    void onChange() override;
    void onCopyProfileCsv();

protected:
    // Widgets
    QLineEdit* firstFrequencyEdit_ {};
    QLineEdit* frequencySpacingEdit_ {};
    QLineEdit* numFrequenciesEdit_ {};
    QLineEdit* azimuthEdit_ {};
    QLineEdit* elevationEdit_ {};
    QLineEdit* polarisationEdit_ {};
    QLineEdit* magneticFieldEdit_ {};
    QLineEdit* peakAmplitudeEdit_ {};
    QLineEdit* timeOfMaximumEdit_ {};
    QLineEdit* widthAtHalfHeightEdit_ {};
    QLineEdit* xCenterEdit_ {};
    QLineEdit* yCenterEdit_ {};
    QLineEdit* zCenterEdit_ {};
    QLineEdit* xSizeEdit_ {};
    QLineEdit* ySizeEdit_ {};
    QLineEdit* zSizeEdit_ {};
    QLineEdit* modulationWavelength_ {};
    QLineEdit* modulationPhase_ {};
    QLineEdit* modulationAmplitude_ {};
    QPushButton* copyProfileCsv_ {};

    // Members
    source::GravyWaveSource* source_;
};

#endif //FDTDLIFE_GRAVYWAVESOURCEDLG_H
