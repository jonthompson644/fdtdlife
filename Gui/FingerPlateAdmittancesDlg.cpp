/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FingerPlateAdmittancesDlg.h"
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>
#include <Xml/Reader.h>
#include "DialogHelper.h"
#include "AdmittanceTableEditDlg.h"

// Constructor
FingerPlateAdmittancesDlg::FingerPlateAdmittancesDlg(QWidget* parent,
                                                     domain::FingerPlate* shape) :
        QDialog(parent),
        shape_(shape) {
    setWindowTitle("Edit Jigsaw Admittances");
    auto mainLayout = new QVBoxLayout;
    // The admittances table
    auto tableLayout = new QHBoxLayout;
    mainLayout->addLayout(tableLayout);
    tablesTree_ = new QTreeWidget;
    tablesTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    tablesTree_->setColumnCount(numTablesColumns);
    tablesTree_->setHeaderLabels({"Name", "Unit Cell", "Plate %", "Min Feature Size %"});
    tablesTree_->setSizePolicy(QSizePolicy::MinimumExpanding,
                               QSizePolicy::MinimumExpanding);
    tableLayout->addWidget(tablesTree_);
    // The table manipulation buttons
    auto tableButtonsLayout = new QVBoxLayout;
    tableLayout->addLayout(tableButtonsLayout);
    addTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Add Table");
    deleteTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Delete Table");
    editTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Edit Table");
    pasteButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Paste");
    copyButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Copy");
    deleteAllTablesButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Delete All");
    fillDownButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Fill Down");
    tableButtonsLayout->addStretch();
    // The dialog buttons
    buttons_ = new QDialogButtonBox(QDialogButtonBox::Close);
    mainLayout->addWidget(buttons_);
    buttons_->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    // Connections
    connect(tablesTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onItemChanged(QTreeWidgetItem * , int)));
    connect(tablesTree_, SIGNAL(itemClicked(QTreeWidgetItem * , int)),
            SLOT(onItemClicked(QTreeWidgetItem * , int)));
    connect(buttons_, SIGNAL(accepted()), SLOT(accept()));
    connect(buttons_, SIGNAL(rejected()), SLOT(reject()));
    connect(addTableButton_, SIGNAL(clicked()), SLOT(onAddTable()));
    connect(deleteTableButton_, SIGNAL(clicked()), SLOT(onDeleteTable()));
    connect(editTableButton_, SIGNAL(clicked()), SLOT(onEditTable()));
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(deleteAllTablesButton_, SIGNAL(clicked()), SLOT(onDeleteAll()));
    connect(fillDownButton_, SIGNAL(clicked()), SLOT(onFillDown()));
    // Attach the layout to the dialog
    mainLayout->addStretch();
    setLayout(mainLayout);
    // Initialise
    fillTablesTree();
}

// Fill the tables list
void FingerPlateAdmittancesDlg::fillTablesTree(domain::FingerPlate::ShapeAdmittance* sel) {
    tablesTree_->clear();
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set set(initialising_);
        for(auto& s : shape_->admittances()) {
            auto item = new QTreeWidgetItem(tablesTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
            item->setText(tablesColName, s.name().c_str());
            item->setText(tablesColUnitCell,
                          QString::number(s.admittance().cellSize()));
            item->setText(tablesColPlate, QString::number(s.plateSize()));
            item->setText(tablesColMinFeatureSize,
                          QString::number(s.minFeatureSize()));
            if(&s == sel || selItem == nullptr) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem == nullptr && tablesTree_->topLevelItemCount() > 0) {
        selItem = tablesTree_->topLevelItem(0);
    }
    if(selItem != nullptr) {
        tablesTree_->setCurrentItem(selItem);
    }
}

// Get the current point.
domain::FingerPlate::ShapeAdmittance* FingerPlateAdmittancesDlg::currentTable() {
    domain::FingerPlate::ShapeAdmittance* result = nullptr;
    auto item = tablesTree_->currentItem();
    if(item != nullptr) {
        int index = tablesTree_->indexOfTopLevelItem(item);
        if(index >= 0 && static_cast<size_t>(index) < shape_->admittances().size()) {
            auto pos = shape_->admittances().begin();
            std::advance(pos, index);
            result = &(*pos);
        }
    }
    return result;
}

// Add a table to the shape
void FingerPlateAdmittancesDlg::onAddTable() {
    shape_->admittances().emplace_back();
    fillTablesTree(&shape_->admittances().back());
}

// Delete the current table from the shape
void FingerPlateAdmittancesDlg::onDeleteTable() {
    QMessageBox box;
    auto item = tablesTree_->currentItem();
    if(item != nullptr) {
        std::stringstream t;
        box.setText("OK to delete the selected admittance table?");
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            int index = tablesTree_->indexOfTopLevelItem(item);
            if(index >= 0 && static_cast<size_t>(index) < shape_->admittances().size()) {
                auto pos = shape_->admittances().begin();
                std::advance(pos, index);
                shape_->admittances().erase(pos);
                fillTablesTree();
            }
        }
    }
}

// An entry in the tables list has changed
void FingerPlateAdmittancesDlg::onItemChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        // Find the item being changed
        int row = tablesTree_->indexOfTopLevelItem(item);
        setValue(row, column, item->text(column).toStdString());
        fillTablesTree(currentTable());
    }
}

// Sets the value of a column.
void FingerPlateAdmittancesDlg::setValue(int row, int column, const std::string& value) {
    if(row >= 0 && static_cast<size_t>(row) < shape_->admittances().size()) {
        auto pos = std::next(shape_->admittances().begin(), row);
        auto* shape = &*pos;
        switch(column) {
            case tablesColName:
                shape->name(value);
                break;
            case tablesColUnitCell:
                shape->admittance().cellSize(strtod(value.c_str(), nullptr));
                break;
            case tablesColPlate:
                shape->plateSize(strtod(value.c_str(), nullptr));
                break;
            case tablesColMinFeatureSize:
                shape->minFeatureSize(strtod(value.c_str(), nullptr));
                break;
            default:
                break;
        }
    }
}

// Edit the current admittance table
void FingerPlateAdmittancesDlg::onEditTable() {
    auto* sel = currentTable();
    if(sel != nullptr) {
        AdmittanceTableEditDlg box(this, sel->admittance());
        if(box.exec() == QDialog::Accepted) {
            shape_->admittanceTable() = box.table();
        }
    }
}

// Copy the admittance table to the clipboard
void FingerPlateAdmittancesDlg::onCopy() {
    // Encode the admittance table into XML
    auto* o = new xml::DomObject("tables");
    for(auto& t : shape_->admittances()) {
        *o << xml::Obj("table") << t;
    }
    xml::DomDocument dom(o);
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom, text);
    // Copy the data to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    QByteArray data(text.c_str());
    mimeData->setData("fdtdlife/fingerplateadmittance", data);
    clipboard->setMimeData(mimeData);
}

// Paste from the clipboard
// The excel tabbed text mode is:
//     <ignored> <totalplate%> ... <totalplate%>
//     <frequencyGHz> <complexAdmittance> ... <complexAdmittance>
//     ...
//     <frequencyGHz> <complexAdmittance> ... <complexAdmittance>
void FingerPlateAdmittancesDlg::onPaste() {
    const QClipboard* clipboard = QApplication::clipboard();
    const QMimeData* mimeData = clipboard->mimeData();
    for(auto& f : mimeData->formats()) {
        std::cout << "\"" << f.toStdString() << "\"" << std::endl;
    }
    if(mimeData->hasFormat("text/plain")) {
        std::list<domain::FingerPlate::ShapeAdmittance*> tables;
        // Split into lines
        std::string text = mimeData->data("text/plain").toStdString();
        QStringList lines = QString(text.c_str()).split('\n');
        bool firstLine = true;
        for(const auto& line : lines) {
            auto tablePos = tables.begin();
            // Split the line into columns
            QStringList cols = line.split('\t');
            bool firstCol = true;
            long long frequency = 0;
            for(const auto& col : cols) {
                if(firstLine) {
                    if(firstCol) {
                        // Skip the frequency column header
                    } else {
                        // Make or find an item for this table
                        domain::FingerPlate::ShapeAdmittance* table = nullptr;
                        if(!col.isEmpty()) {
                            for(auto& s : shape_->admittances()) {
                                if(s.name() == col.toStdString()) {
                                    table = &s;
                                    table->admittance().clear();
                                    break;
                                }
                            }
                        }
                        if(table == nullptr) {
                            shape_->admittances().emplace_back();
                            table = &shape_->admittances().back();
                        }
                        tables.push_back(table);
                        tables.back()->admittance().cellSize(shape_->cellSizeX().value());
                        tables.back()->name(col.toStdString());
                        tables.back()->plateSize(col.toDouble());
                    }
                } else {
                    if(firstCol) {
                        // Remember the frequency of this row
                        frequency = static_cast<long long>(
                                col.toDouble() * box::Constants::giga_);
                    } else {
                        // Make an entry for this admittance
                        std::string colStr = col.toStdString();
                        int splitPos = col.indexOf('+', 1);
                        if(splitPos < 0) {
                            splitPos = col.indexOf('-', 1);
                        }
                        QString realPart = col.left(splitPos);
                        std::string realStr = realPart.toStdString();
                        int iPos = col.indexOf('i');
                        QString imagPart = col.mid(splitPos, iPos - splitPos);
                        std::string imagStr = imagPart.toStdString();
                        std::complex<double> admittance(realPart.toDouble(),
                                                        imagPart.toDouble());
                        (*tablePos)->admittance().addPoint(frequency, admittance);
                        tablePos++;
                    }
                }
                firstCol = false;
            }
            firstLine = false;
        }
    } else if(mimeData->hasFormat("fdtdlife/fingerplateadmittance")) {
        // Clear the current data
        shape_->admittances().clear();
        // Read the XML
        std::string text = clipboard->mimeData()->data(
                "fdtdlife/fingerplateadmittance").toStdString();
        xml::DomDocument dom("tables");
        xml::Reader reader;
        try {
            reader.readString(&dom, text);
            for(auto& o : dom.getObject()->obj("table")) {
                shape_->admittances().emplace_back();
                *o >> shape_->admittances().back();
            }
        } catch(xml::Exception& e) {
            std::cout << "Finger Plate Admittance table paste failed to read XML: "
                      << e.what() << std::endl;
        }
    }
    fillTablesTree();
}

// Delete all the tables
void FingerPlateAdmittancesDlg::onDeleteAll() {
    QMessageBox box;
    std::stringstream t;
    box.setText("OK to delete the all the admittance tables?");
    box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    box.setDefaultButton(QMessageBox::Ok);
    if(box.exec() == QMessageBox::Ok) {
        shape_->admittances().clear();
        fillTablesTree();
    }
}

// Fill the value down.
void FingerPlateAdmittancesDlg::onFillDown() {
    if(currentColumn_ >= 0) {
        // The row providing the value
        auto* curItem = tablesTree_->currentItem();
        if(curItem != nullptr) {
            std::string value = curItem->text(currentColumn_).toStdString();
            int rowNumber = tablesTree_->indexOfTopLevelItem(curItem);
            for(int i = rowNumber + 1; i < tablesTree_->topLevelItemCount(); i++) {
                setValue(i, currentColumn_, value);
            }
            fillTablesTree(currentTable());
        }
    }
}

// An item has been clicked
void FingerPlateAdmittancesDlg::onItemClicked(QTreeWidgetItem* item, int column) {
    // Record the column for use with fill down
    if(item != nullptr) {
        currentColumn_ = column;
    } else {
        currentColumn_ = -1;
    }
}
