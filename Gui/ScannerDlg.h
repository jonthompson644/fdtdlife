/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SCANNERDLG_H
#define FDTDLIFE_SCANNERDLG_H

#include "QtIncludes.h"
#include "InitialisingGui.h"

class CharacterizerParamsDlg;
namespace seq {class Scanner;}

// Base class for the scanner sub-dialogs
class ScannerDlg : public QWidget {
Q_OBJECT

public:
    explicit ScannerDlg(CharacterizerParamsDlg* dlg, seq::Scanner* scanner);
    virtual void tabChanged() {onChange();}
    virtual void initialise();

public slots:
    virtual void onChange();

public:
    seq::Scanner* scanner() {return scanner_;}

protected:
    CharacterizerParamsDlg* dlg_;
    seq::Scanner* scanner_;
    QVBoxLayout* layout_;
    QGroupBox* scannerGroup_;
    InitialisingGui initialising_;
    QLineEdit* startStepEdit_;
    QLineEdit* numStepsEdit_;
};

#endif //FDTDLIFE_SCANNERDLG_H
