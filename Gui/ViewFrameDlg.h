/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_VIEWFRAMEDLG_H
#define FDTDLIFE_VIEWFRAMEDLG_H

#include "QtIncludes.h"
#include "FdTdLife.h"
#include "MainTabPanel.h"
#include <Notifiable.h>

class ViewFrame;
class ViewItemDlg;
class ViewItem;
namespace fdtd {class DataSpec;}

class ViewFrameDlg : public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit ViewFrameDlg(FdTdLife* m);
    void initialise() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    void tabChanged() override;
    ViewItem* viewItem();
    
protected slots:
    void onViewSelected();
    void onNew();
    void onDelete();
    void onChange();
    void onVideoFilenameBrowse();
    void onCloseVideo();
    void onSensorChanged(QListWidgetItem* item);
    void onSensorSelected();
    
protected:
    enum {viewColIdentifier=0, viewColName, numViewCols};
    QTreeWidget* viewsTree_;
    QPushButton* newButton_;
    QPushButton* deleteButton_;
    QLineEdit* nameEdit_;
    QCheckBox* makeVideoCheck_;
    QPushButton* closeVideoButton_;
    QPushButton* browseVideoFileButton_;
    QLineEdit* videoFilenameEdit_;
    QCheckBox* displayAllFramesCheck_;
    QListWidget* dataSpecsList_;
    QLineEdit* frameDisplayPeriodEdit_;
    QVBoxLayout* viewItemDlgLayout_;
    ViewItemDlg* viewItemDlg_;
    std::map<QListWidgetItem*, sensor::DataSpec*> dataSpecs_;

protected:
    void fillDataSpecList();
    void fillViewTree(ViewFrame* sel=nullptr);
    void makeViewItemDlg(ViewItem* viewItem);
    ViewFrame* getCurrentView();
};


#endif //FDTDLIFE_VIEWFRAMEDLG_H
