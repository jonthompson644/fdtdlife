/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "TwoDSliceDlg.h"
#include "DialogHelper.h"
#include <Sensor/TwoDSlice.h>
#include <Domain/UnitCellShape.h>
#include "FdTdLife.h"
#include "SensorsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
TwoDSliceDlg::TwoDSliceDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        SensorDlg(dlg, sensor),
        sensor_(dynamic_cast<sensor::TwoDSlice*>(sensor)) {
    orientationList_ = DialogHelper::dropListItem(layout_, "Orientation",
                                                  box::Constants::orientationNames_);
    dataSourceList_ = DialogHelper::dropListItem(layout_, "Data Source",
                                                 {"E Mag", "H Mag", "Ex", "Ey", "Ez",
                                                  "Hx", "Hy", "Hz", "Material"});
    auto* offsetLayout = new QHBoxLayout;
    layout_->addLayout(offsetLayout);
    offsetEdit_ = DialogHelper::doubleItem(offsetLayout, "Offset");
    nextLayerButton_ = DialogHelper::buttonItem(offsetLayout, "Layer+");
    prevLayerButton_ = DialogHelper::buttonItem(offsetLayout, "Layer-");
    connect(orientationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(offsetEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dataSourceList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(nextLayerButton_, SIGNAL(clicked()), SLOT(onLayerPlus()));
    connect(prevLayerButton_, SIGNAL(clicked()), SLOT(onLayerMinus()));
}

// Initialise the GUI parameters
void TwoDSliceDlg::initialise() {
    SensorDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    orientationList_->setCurrentIndex(sensor_->orientation());
    dataSourceList_->setCurrentIndex(sensor_->dataSource());
    offsetEdit_->setText(QString::number(sensor_->offset()));
}

// An edit box has changed
void TwoDSliceDlg::onChange() {
    SensorDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newOrientation = (box::Constants::Orientation) c.testInt(
                orientationList_, sensor_->orientation(),
                FdTdLife::notifyDomainContentsChange);
        auto newOffset = c.testDouble(offsetEdit_, sensor_->offset(),
                                      FdTdLife::notifyDomainContentsChange);
        auto newDataSource = (sensor::TwoDSlice::DataSource) c.testInt(
                dataSourceList_, sensor_->dataSource(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            sensor_->orientation(newOrientation);
            sensor_->offset(newOffset);
            sensor_->dataSource(newDataSource);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void TwoDSliceDlg::tabChanged() {
    onChange();
}

// Move the offset to the position of the next layer
void TwoDSliceDlg::onLayerPlus() {
    // We can only do this for Z orientation
    if(sensor_->orientation() == box::Constants::orientationZPlane) {
        double newOffset = sensor_->offset();
        bool found = false;
        double delta = dlg_->model()->p()->dr().z().value() / 2.0;
        // Find the layer shape with the lowest z that is bigger than the current offset
        for(auto& s : dlg_->model()->d()->shapes()) {
            auto* layer = dynamic_cast<domain::UnitCellShape*>(s.get());
            if(layer != nullptr) {
                if(layer->center().value().z() > sensor_->offset() + delta) {
                    if(layer->center().value().z() < newOffset || !found) {
                        newOffset = layer->center().value().z();
                        found = true;
                    }
                }
            }
        }
        if(found) {
            offsetEdit_->setText(QString::number(newOffset));
            onChange();
        }
    }
}

// Move the offset to the position of the previous layer
void TwoDSliceDlg::onLayerMinus() {
    // We can only do this for Z orientation
    if(sensor_->orientation() == box::Constants::orientationZPlane) {
        double newOffset = sensor_->offset();
        bool found = false;
        double delta = dlg_->model()->p()->dr().z().value() / 2.0;
        // Find the layer shape with the biggest z that is smaller than the current offset
        for(auto& s : dlg_->model()->d()->shapes()) {
            auto* layer = dynamic_cast<domain::UnitCellShape*>(s.get());
            if(layer != nullptr) {
                if(layer->center().value().z() < sensor_->offset() - delta) {
                    if(layer->center().value().z() > newOffset || !found) {
                        newOffset = layer->center().value().z();
                        found = true;
                    }
                }
            }
        }
        if(found) {
            offsetEdit_->setText(QString::number(newOffset));
            onChange();
        }
    }
}
