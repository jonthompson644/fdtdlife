/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AreaSensorDlg.h"

#include "DialogHelper.h"
#include <Sensor/AreaSensor.h>
#include "FdTdLife.h"
#include "SensorsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
AreaSensorDlg::AreaSensorDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        SensorDlg(dlg, sensor),
        sensor_(dynamic_cast<sensor::AreaSensor*>(sensor)) {
    auto line1Layout = new QHBoxLayout;
    std::tie(centerXEdit_, centerYEdit_, centerZEdit_) =
            DialogHelper::vectorItem(line1Layout, "Center x,y,z (m)");
    layout_->addLayout(line1Layout);
    auto line2Layout = new QHBoxLayout;
    widthEdit_ = DialogHelper::doubleItem(line2Layout, "Width (m)");
    heightEdit_ = DialogHelper::doubleItem(line2Layout, "Height (m)");
    layout_->addLayout(line2Layout);
    auto line3Layout = new QHBoxLayout;
    orientationList_ = DialogHelper::dropListItem(line3Layout, "Orientation",
            box::Constants::orientationNames_);
    layout_->addLayout(line3Layout);
    connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(widthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(heightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(orientationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void AreaSensorDlg::initialise() {
    SensorDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    centerXEdit_->setText(QString::number(sensor_->center().x()));
    centerYEdit_->setText(QString::number(sensor_->center().y()));
    centerZEdit_->setText(QString::number(sensor_->center().z()));
    heightEdit_->setText(QString::number(sensor_->height()));
    widthEdit_->setText(QString::number(sensor_->width()));
    orientationList_->setCurrentIndex(sensor_->orientation());
}

// An edit box has changed
void AreaSensorDlg::onChange() {
    SensorDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenter = c.testVectorDouble(centerXEdit_, centerYEdit_, centerZEdit_,
                                            sensor_->center(),
                                            FdTdLife::notifyDomainContentsChange);
        auto newWidth = c.testDouble(widthEdit_, sensor_->width(), FdTdLife::notifyDomainContentsChange);
        auto newHeight = c.testDouble(heightEdit_, sensor_->height(), FdTdLife::notifyDomainContentsChange);
        auto newOrientation = (box::Constants::Orientation)c.testInt(orientationList_, sensor_->orientation(),
                                                                     FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            sensor_->center(newCenter);
            sensor_->width(newWidth);
            sensor_->height(newHeight);
            sensor_->orientation(newOrientation);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void AreaSensorDlg::tabChanged() {
    onChange();
}

// The model has changed
void AreaSensorDlg::modelNotify() {
}
