/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessFilterDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessFilter.h>
#include <Gs/GeneticSearch.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GuiChangeDetector.h"
#include "GeneticSearchSeqDlg.h"
#include "GuiConfiguration.h"

// Second stage constructor
void FitnessFilterDlg::construct(GeneticSearchTargetDlg* dlg,
                                 gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    fitnessFilter_ = dynamic_cast<gs::FitnessFilter*>(fitnessFunction);
    auto col1 = new QVBoxLayout;
    layout_->addLayout(col1);
    // Line 1
    auto line1Layout = new QHBoxLayout;
    col1->addLayout(line1Layout);
    fitnessFunctionWeightEdit_ = DialogHelper::doubleItem(line1Layout, "Weight");
    dataSourceList_ = DialogHelper::dropListItem(line1Layout, "source",
                                                 {"Mag", "X", "Y"});
    propertyList_ = DialogHelper::dropListItem(line1Layout, "property",
                                               {"Transmittance", "Phase Shift"});
    // Line 2
    auto line2Layout = new QHBoxLayout;
    col1->addLayout(line2Layout);
    filterTypeList_ = DialogHelper::dropListItem(line2Layout, "Filter Type",
                                                 {"Butterworth", "Chebyshev I",
                                                  "Chebyshev II", "Elliptical", "Bessel"});
    filterModeList_ = DialogHelper::dropListItem(line2Layout, "Mode",
                                                 {"Low Pass", "High Pass"});
    orderEdit_ = DialogHelper::sizeTItem(line2Layout, "Order", 1U);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    col1->addLayout(line3Layout);
    cutOffFrequencyEdit_ = DialogHelper::doubleItem(line3Layout, "Cut Off Freq (Hz)");
    maxFrequencyEdit_ = DialogHelper::doubleItem(line3Layout, "Maximum Freq (Hz)");
    // Line 4
    auto line4Layout = new QHBoxLayout;
    col1->addLayout(line4Layout);
    passbandRippleEdit_ = DialogHelper::doubleItem(line4Layout, "Passband Ripple (dB)");
    // The display
    fitnessWidget_ = new FitnessWidget(this);
    col1->addWidget(fitnessWidget_);
    col1->addStretch();
    connect(fitnessFunctionWeightEdit_, SIGNAL(editingFinished()),
            SLOT(onChange()));
    connect(dataSourceList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(propertyList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(filterTypeList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(filterModeList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(orderEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cutOffFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(maxFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(passbandRippleEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    initialise();
}

// Draw the fitness information on the widget
void FitnessFilterDlg::paintFitnessWidget() {
    // Make sure the filter has data
    fitnessFilter_->initialise(dlg_->dlg()->sequencer()->numFrequencies(),
                               dlg_->dlg()->sequencer()->frequencySpacing(),
                               dlg_->dlg()->sequencer()->frequencySpacing());
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    double scale;
    double offset;
    switch(fitnessFilter_->property()) {
        case gs::FitnessFilter::Property::transmittance:
            scale = h / 1.0;
            offset = 0.0;
            break;
        case gs::FitnessFilter::Property::phaseShift:
            scale = h / 360.0;
            offset = 180.0;
            break;
    }
    // Black background
    painter.setPen(Qt::NoPen);
    if(dlg_->model()->guiConfiguration()->whiteBackground()) {
        painter.setBrush(Qt::white);
    } else {
        painter.setBrush(Qt::black);
    }
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(fitnessFilter_ != nullptr) {
        size_t numPoints = fitnessFilter_->min().size();
        if(numPoints > 0) {
            // Line colours
            QPen functionPen;
            if(dlg_->model()->guiConfiguration()->whiteBackground()) {
                functionPen.setColor(Qt::black);
            } else {
                functionPen.setColor(Qt::green);
            }
            // Draw the data
            int lastMin = h - static_cast<int>(std::round(
                    (fitnessFilter_->min()[0] + offset) * scale));
            int lastMax = h - static_cast<int>(std::round(
                    (fitnessFilter_->max()[0] + offset) * scale));
            int lastFreq = 0;
            for(size_t i = 1; i < numPoints; ++i) {
                int min = h - static_cast<int>(std::round(
                        (fitnessFilter_->min()[i] + offset) * scale));
                int max = h - static_cast<int>(std::round(
                        (fitnessFilter_->max()[i] + offset) * scale));
                int freq = static_cast<int>(i) * w / static_cast<int>(numPoints);
                painter.setPen(functionPen);
                painter.drawLine(lastFreq, lastMin, freq, min);
                painter.drawLine(lastFreq, lastMax, freq, max);
                lastMin = min;
                lastMax = max;
                lastFreq = freq;
            }
        }
    }
    // Clean up the context
    painter.restore();
}

// Initialise the GUI parameters
void FitnessFilterDlg::initialise() {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::initialise();
    InitialisingGui::Set s(initialising_);
    fitnessFunctionWeightEdit_->setText(QString::number(fitnessFilter_->weight()));
    dataSourceList_->setCurrentIndex(static_cast<int>(fitnessFilter_->dataSource()));
    filterTypeList_->setCurrentIndex(static_cast<int>(fitnessFilter_->filterType()));
    filterModeList_->setCurrentIndex(static_cast<int>(fitnessFilter_->filterMode()));
    propertyList_->setCurrentIndex(static_cast<int>(fitnessFilter_->property()));
    orderEdit_->setText(QString::number(fitnessFilter_->order()));
    cutOffFrequencyEdit_->setText(QString::number(fitnessFilter_->cutOffFrequency()));
    maxFrequencyEdit_->setText(QString::number(fitnessFilter_->maxFrequency()));
    passbandRippleEdit_->setText(QString::number(fitnessFilter_->passbandRipple()));
    fitnessWidget_->update();
}

// An edit box has changed
void FitnessFilterDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(fitnessFunctionWeightEdit_,
                                      fitnessFilter_->weight(),
                                      FdTdLife::notifyMinorChange);
        auto newSource = static_cast<gs::FitnessBase::DataSource>(
                c.testInt(dataSourceList_,
                          static_cast<int>(fitnessFilter_->dataSource()),
                          FdTdLife::notifyMinorChange));
        auto newProperty = static_cast<gs::FitnessFilter::Property>(
                c.testInt(propertyList_,
                          static_cast<int>(fitnessFilter_->property()),
                          FdTdLife::notifyMinorChange));
        auto newFilterType = static_cast<gs::FitnessFilter::FilterType>(
                c.testInt(filterTypeList_,
                          static_cast<int>(fitnessFilter_->filterType()),
                          FdTdLife::notifyMinorChange));
        auto newFilterMode = static_cast<gs::FitnessFilter::FilterMode>(
                c.testInt(filterModeList_,
                          static_cast<int>(fitnessFilter_->filterMode()),
                          FdTdLife::notifyMinorChange));
        auto newOrder = c.testSizeT(orderEdit_, fitnessFilter_->order(),
                                    FdTdLife::notifyMinorChange);
        auto newCutOffFrequency = c.testDouble(cutOffFrequencyEdit_,
                                               fitnessFilter_->cutOffFrequency(),
                                               FdTdLife::notifyMinorChange);
        auto newMaxFrequency = c.testDouble(maxFrequencyEdit_,
                                            fitnessFilter_->maxFrequency(),
                                            FdTdLife::notifyMinorChange);
        auto newPassbandRipple = c.testDouble(passbandRippleEdit_,
                                              fitnessFilter_->passbandRipple(),
                                              FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            fitnessFilter_->set(fitnessFilter_->name(), newWeight, newSource);
            fitnessFilter_->property(newProperty);
            fitnessFilter_->filterType(newFilterType);
            fitnessFilter_->filterMode(newFilterMode);
            fitnessFilter_->order(newOrder);
            fitnessFilter_->cutOffFrequency(newCutOffFrequency);
            fitnessFilter_->maxFrequency(newMaxFrequency);
            fitnessFilter_->passbandRipple(newPassbandRipple);
            fitnessWidget_->update();
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessFilterDlg::tabChanged() {
    onChange();
}

// Handle the context menu for the fitness widget
void FitnessFilterDlg::fitnessWidgetContext(QContextMenuEvent* event) {
    if(fitnessFilter_ != nullptr) {
        QMenu menu;
        QAction* copyToClipboard = menu.addAction("Copy to clipboard");
        QAction* selectedAction = menu.exec(event->globalPos());
        if(selectedAction == copyToClipboard) {
            size_t numPoints = fitnessFilter_->min().size();
            if(numPoints > 0) {
                double fStep = fitnessFilter_->s()->frequencySpacing();
                QClipboard* clipboard = QApplication::clipboard();
                auto mimeData = new QMimeData;
                std::stringstream text;
                text << "x\tymin\tymax" << std::endl;
                for(size_t i = 1; i < numPoints; ++i) {
                    double min = fitnessFilter_->min()[i];
                    double max = fitnessFilter_->max()[i];
                    double freq = static_cast<double>(i + 1) * fStep;
                    text << freq << "\t" << min << "\t" << max << std::endl;
                }
                mimeData->setText(text.str().c_str());
                mimeData->setImageData(grab().toImage());
                clipboard->setMimeData(mimeData);
            }
        }
    }
}

