/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessIntensityLineDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessIntensityLine.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor
void FitnessIntensityLineDlg::construct(GeneticSearchTargetDlg* dlg,
                                        gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    fitnessFunction_ = dynamic_cast<gs::FitnessIntensityLine*>(fitnessFunction);
    auto col1 = new QVBoxLayout;
    layout_->addLayout(col1);
    // Line 1
    auto line1Layout = new QHBoxLayout;
    col1->addLayout(line1Layout);
    fitnessFunctionWeightEdit_ = DialogHelper::doubleItem(line1Layout, "Weight");
    dataSourceList_ = DialogHelper::dropListItem(line1Layout, "Source",
            {"Mag", "X", "Y"});
    zEdit_ = DialogHelper::doubleItem(line1Layout, "Z Pos (m)");
    // Line 2
    auto line2Layout = new QHBoxLayout;
    col1->addLayout(line2Layout);
    frequencyEdit_ = DialogHelper::doubleItem(line2Layout, "Frequency (Hz)");
    numPointsXEdit_ = DialogHelper::intItem(line2Layout, "Num Points X");
    // The fitness function
    fitnessPointsTree_ = new QTreeWidget;
    fitnessPointsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    fitnessPointsTree_->setColumnCount(numFitnessPointsCols);
    fitnessPointsTree_->setHeaderLabels({"X Position (m)", "Weight"});
    fitnessPointsTree_->setFixedWidth(310);
    col1->addWidget(fitnessPointsTree_);
    auto fitnessPointButtons = new QHBoxLayout;
    newFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons, "New Point");
    deleteFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons,
            "Delete Point");
    fitnessPointButtons->addStretch();
    col1->addLayout(fitnessPointButtons);
    fitnessWidget_ = new FitnessWidget(this);
    col1->addWidget(fitnessWidget_);
    col1->addStretch();
    connect(fitnessFunctionWeightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(zEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numPointsXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dataSourceList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(newFitnessPointButton_, SIGNAL(clicked()), SLOT(onNewFitnessPoint()));
    connect(deleteFitnessPointButton_, SIGNAL(clicked()), SLOT(onDeleteFitnessPoint()));
    connect(fitnessPointsTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onFitnessPointChanged(QTreeWidgetItem * , int)));
    initialise();
}

// Draw the fitness information on the widget
void FitnessIntensityLineDlg::paintFitnessWidget() {
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    // Black background
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(fitnessFunction_ != nullptr) {
        // Determine the scaling factors
        double min = fitnessFunction_->rangeMin();
        double max = fitnessFunction_->rangeMax();
        double minX = 0.0;
        double maxX = 0.0;
        for(const auto& point : fitnessFunction_->points()) {
            minX = std::min(minX, point.x());
            maxX = std::max(maxX, point.x());
        }
        double scale = (double) (h - 2) / (max - min);
        double scaleX = (double) w / (maxX - minX);
        double offset = min;
        double offsetX = minX;
        // Line colors
        QPen functionPen(Qt::green);
        QPen annotationPen(Qt::darkGray);
        // Draw the data
        int lastX = 0;
        int lastMin = 0;
        for(const auto& point : fitnessFunction_->points()) {
            auto nextX = (int) std::floor((point.x() - offsetX) * scaleX);
            int nextMin = h - 1 -
                          (int) std::floor((point.min() - offset) * scale);
            painter.setPen(functionPen);
            painter.drawLine(lastX, lastMin, nextX, nextMin);
            lastX = nextX;
            lastMin = nextMin;
        }
    }
    // Clean up the context
    painter.restore();
}

// Initialise the GUI parameters
void FitnessIntensityLineDlg::initialise() {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::initialise();
    InitialisingGui::Set s(initialising_);
    fitnessFunctionWeightEdit_->setText(QString::number(fitnessFunction_->weight()));
    zEdit_->setText(QString::number(fitnessFunction_->z()));
    frequencyEdit_->setText(QString::number(fitnessFunction_->frequency()));
    numPointsXEdit_->setText(QString::number(fitnessFunction_->numPointsX()));
    dataSourceList_->setCurrentIndex(fitnessFunction_->dataSource());
    fillFitnessPointsTree();
}

// An edit box has changed
void FitnessIntensityLineDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(fitnessFunctionWeightEdit_, fitnessFunction_->weight(),
                                      FdTdLife::notifyMinorChange);
        auto newZ = c.testDouble(zEdit_, fitnessFunction_->z(),
                                 FdTdLife::notifyDomainContentsChange);
        auto newFrequency = c.testDouble(frequencyEdit_, fitnessFunction_->frequency(),
                                         FdTdLife::notifyMinorChange);
        auto newNumPointsX = c.testSizeT(numPointsXEdit_, fitnessFunction_->numPointsX(),
                                         FdTdLife::notifyMinorChange);
        auto newSource = (gs::FitnessBase::DataSource) c.testInt(
                dataSourceList_, fitnessFunction_->dataSource(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            fitnessFunction_->set(fitnessFunction_->name(), newWeight, newSource);
            fitnessFunction_->frequency(newFrequency);
            fitnessFunction_->numPointsX(newNumPointsX);
            fitnessFunction_->z(newZ);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessIntensityLineDlg::tabChanged() {
    onChange();
}

// The new fitness point button has been pressed
void FitnessIntensityLineDlg::onNewFitnessPoint() {
    // Current function
    if(fitnessFunction_ != nullptr) {
        // Add an empty point
        fitnessFunction_->addPoint(0.0, 0.0, 0.0);
        fillFitnessPointsTree();
    }
}

// The delete fitness point button has been pressed
void FitnessIntensityLineDlg::onDeleteFitnessPoint() {
    // Current function
    if(fitnessFunction_ != nullptr) {
        // Current point
        QTreeWidgetItem* item = fitnessPointsTree_->currentItem();
        if(item != nullptr) {
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            if(row >= 0) {
                fitnessFunction_->deletePoint(row);
                fillFitnessPointsTree();
            }
        }
    }
}

// Put all the fitness points in the fitness tree widget
void FitnessIntensityLineDlg::fillFitnessPointsTree() {
    InitialisingGui::Set s(initialising_);
    fitnessPointsTree_->clear();
    // Current function
    if(fitnessFunction_ != nullptr) {
        for(auto& point : fitnessFunction_->points()) {
            auto item = new QTreeWidgetItem(fitnessPointsTree_);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(fitnessPointsColPositionX, QString::number(point.x()));
            item->setText(fitnessPointsColWeight, QString::number(point.min()));
        }
    }
    fitnessWidget_->update();
}

// A fitness tree item has been changed
void FitnessIntensityLineDlg::onFitnessPointChanged(QTreeWidgetItem* item, int /*column*/) {
    if(!initialising_) {
        // Current function
        if(fitnessFunction_ != nullptr) {
            // Which row
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            // Set the function point in this row
            double x = item->text(fitnessPointsColPositionX).toDouble();
            double min = item->text(fitnessPointsColWeight).toDouble();
            fitnessFunction_->setPoint(row, x, min, 0.0);
            fillFitnessPointsTree();
        }
    }
}
