/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_VIEWFRAME_H
#define FDTDLIFE_VIEWFRAME_H

#include "QtIncludes.h"
#include <set>
#include "GuiConfiguration.h"
#include <Notifiable.h>
#include <string>
#include <Xml/DomObject.h>

class FdTdLife;
class VideoEncoder;
class ViewItem;
namespace xml {class DomObject;}
namespace sensor { class DataSpec; }
namespace sensor { class Sensor;}

class ViewFrame : public QDialog, public fdtd::Notifiable {
Q_OBJECT

public:
    ViewFrame(int identifier, const std::string& name, FdTdLife* model);
    ViewFrame(xml::DomObject* root, FdTdLife* model);
    ~ViewFrame();
    void updateViews();
    void notifyModel(bool noViews=false);
    void initialise();
    void closeVideo();
    bool openVideo();
    void writeConfig(xml::DomObject& root) const;
    void readConfig(xml::DomObject& root);
    friend xml::DomObject& operator<<(xml::DomObject& o, const ViewFrame& v) {v.writeConfig(o); return o;}
    friend xml::DomObject& operator>>(xml::DomObject& o, ViewFrame& v) {v.readConfig(o); return o;}
    void add(ViewItem* item);
    void remove(ViewItem* item);
    ViewItem* findViewItem(sensor::DataSpec* spec);
    ViewItem* createViewItem(sensor::DataSpec* spec);
    void sensorAdded(sensor::Sensor* s);
    void sensorRemoved(sensor::Sensor* s);
    QColor color(GuiConfiguration::Element e) const;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected:
    FdTdLife* model_;
    VideoEncoder* videoEncoder_;
    QImage* videoFrame_;
    std::list<ViewItem*> items_;
    QTime lastFrameTime_;
    // Configuration
    int identifier_;
    std::string name_;
    bool makeVideo_;  // Enables the creation of a video file
    std::string videoFilename_;  // The video file name
    bool displayAllFrames_;  // Display all frames
    int frameDisplayPeriod_;  // Display frames no faster than this (ms)
    int frameWidth_;  // Width of the view frame
    int frameHeight_;  // Height of the view frame

protected:
    void closeEvent(QCloseEvent* event) override;
    void construct();
    void paintEvent(QPaintEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void contextMenuEvent(QContextMenuEvent* event) override;
    void doPaint(QPainter* painter);

public:
    // Getters
    const std::string& name() const {return name_;}
    bool makeVideo() const {return makeVideo_;}
    const std::string& videoFilename() const {return videoFilename_;}
    FdTdLife* model() const {return model_;}
    bool displayAllFrames() const {return displayAllFrames_;}
    int frameDisplayPeriod() const {return frameDisplayPeriod_;}
    int identifier() const {return identifier_;}
    // Setters
    void set(const std::string& name, bool makeVideo, const std::string& videoFilename,
             bool displayAllFrames, int frameDisplayPeriod);
};


#endif //FDTDLIFE_VIEWFRAME_H
