/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PlateAdmittanceEditDlg.h"
#include "DialogHelper.h"
#include <PlateAdmittance.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/DomObject.h>
#include <memory>
#include <Xml/Exception.h>
#include <Box/Constants.h>
#include <Seq/CharacterizerStep.h>
#include <Box/Utility.h>

// Constructor
PlateAdmittanceEditDlg::PlateAdmittanceEditDlg(QWidget* parent,
                                               const std::map<double, fdtd::AdmittanceData>& table,
                                               fdtd::PlateAdmittance::PlateType tableType) :
        QDialog(parent),
        table_(table),
        tableType_(tableType),
        currentPlateSize_(0.0) {
    auto mainLayout = new QVBoxLayout;
    auto colLayout = new QHBoxLayout;
    mainLayout->addLayout(colLayout);
    // The plate size table
    auto plateLayout = new QVBoxLayout;
    colLayout->addLayout(plateLayout);
    plateTree_ = new QTreeWidget;
    plateTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    plateTree_->setColumnCount(numPlateCols);
    plateTree_->setHeaderLabels({"Plate Size (%)"});
    plateLayout->addWidget(plateTree_);
    auto plateButtonsLayout = new QHBoxLayout;
    plateLayout->addLayout(plateButtonsLayout);
    newPlateSizeButton_ = DialogHelper::buttonItem(plateButtonsLayout, "Add Plate Size");
    deletePlateSizeButton_ = DialogHelper::buttonItem(plateButtonsLayout, "Remove Plate Size");
    tableTypeList_ = DialogHelper::dropListItem(plateLayout, "Table Type",
                                                {"Capacitive", "Inductive"});
    auto tableButtonsLayout = new QHBoxLayout;
    plateLayout->addLayout(tableButtonsLayout);
    copyTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Copy Table");
    pasteTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Paste Table");
    clearTableButton_ = DialogHelper::buttonItem(tableButtonsLayout, "Clear Table");
    plateLayout->addStretch();
    // The admittance table
    auto admLayout = new QVBoxLayout;
    colLayout->addLayout(admLayout);
    admTree_ = new QTreeWidget;
    admTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    admTree_->setColumnCount(numAdmCols);
    admTree_->setHeaderLabels({"Freq (GHz)", "Real", "Imaginary"});
    admTree_->setMinimumSize(350, 300);
    admLayout->addWidget(admTree_);
    cellSizeEdit_ = DialogHelper::doubleItem(admLayout, "Cell Size (m)");
    // The admittance buttons
    auto buttonLayout = new QVBoxLayout;
    pasteFrequencyButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Frequency");
    pasteRealButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Real");
    pasteImaginaryButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Imaginary");
    pasteStepButton_ = DialogHelper::buttonItem(buttonLayout, "Paste Admittance");
    clearAllButton_ = DialogHelper::buttonItem(buttonLayout, "Clear Entry");
    buttonLayout->addStretch();
    colLayout->addLayout(buttonLayout);
    // The ok/cancel buttons
    buttons_ = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttons_);
    connect(buttons_, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons_, &QDialogButtonBox::rejected, this, &QDialog::reject);
    // Attach layout to the dialog
    mainLayout->addStretch();
    setLayout(mainLayout);
    //resize(600, 300);
    setWindowTitle("Plate Admittance Table Editor");
    // Initialise the dialog
    fillPlateList();
    // Connect handlers
    connect(plateTree_, SIGNAL(itemSelectionChanged()), SLOT(onPlateSizeSelected()));
    connect(newPlateSizeButton_, SIGNAL(clicked()), SLOT(onNewPlateSize()));
    connect(deletePlateSizeButton_, SIGNAL(clicked()), SLOT(onDeletePlateSize()));
    connect(pasteFrequencyButton_, SIGNAL(clicked()), SLOT(onPasteFrequency()));
    connect(pasteRealButton_, SIGNAL(clicked()), SLOT(onPasteReal()));
    connect(pasteImaginaryButton_, SIGNAL(clicked()), SLOT(onPasteImaginary()));
    connect(pasteStepButton_, SIGNAL(clicked()), SLOT(onPasteAdmittance()));
    connect(clearAllButton_, SIGNAL(clicked()), SLOT(onClearAll()));
    connect(copyTableButton_, SIGNAL(clicked()), SLOT(onCopyTable()));
    connect(pasteTableButton_, SIGNAL(clicked()), SLOT(onPasteTable()));
    connect(clearTableButton_, SIGNAL(clicked()), SLOT(onClearTable()));
    connect(this, SIGNAL(accepted()), SLOT(onAccepted()));
    connect(plateTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onPlateSizeChanged(QTreeWidgetItem * , int)));
}

// A plate size has been selected
void PlateAdmittanceEditDlg::onPlateSizeSelected() {
    if(!initialising_) {
        readAdmittanceList();
        currentPlateSize_ = getCurrentPlateSize();
        std::cout << "Current plate size=" << currentPlateSize_ << std::endl;
        fillAdmittanceList();
    }
}

// Fill the plate tree and associated
void PlateAdmittanceEditDlg::fillPlateList(double selectSize) {
    InitialisingGui::Set s(initialising_);
    plateTree_->clear();
    QTreeWidgetItem* select = nullptr;
    for(auto& pos : table_) {
        auto row = new QTreeWidgetItem(plateTree_);
        row->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        row->setText(plateColSize, QString::number(pos.first));
        if(pos.first == selectSize) {
            select = row;
        }
    }
    tableTypeList_->setCurrentIndex((int) tableType_);
    currentPlateSize_ = selectSize;
    std::cout << "Current plate size=" << currentPlateSize_ << std::endl;
    if(select != nullptr) {
        // Make the selection
        select->setSelected(true);
        fillAdmittanceList();
    } else {
        // No selection clear admittance table
        admTree_->clear();
        cellSizeEdit_->clear();
    }
}

// Return the currently selected plate size
double PlateAdmittanceEditDlg::getCurrentPlateSize() {
    double result = 0.0;
    auto item = plateTree_->currentItem();
    if(item != nullptr) {
        result = item->text(plateColSize).toDouble();
    }
    return result;
}

// Returns true if a plate size is currently selected
bool PlateAdmittanceEditDlg::isPlateSizeSelected() {
    return plateTree_->currentItem() != nullptr;
}

// Fill the admittance table tree and associated
void PlateAdmittanceEditDlg::fillAdmittanceList() {
    admTree_->clear();
    cellSizeEdit_->clear();
    auto admPos = table_.find(currentPlateSize_);
    if(admPos != table_.end()) {
        fdtd::AdmittanceData& data = admPos->second;
        cellSizeEdit_->setText(QString::number(data.cellSize()));
        for(auto& pos : data.admittance()) {
            auto row = new QTreeWidgetItem(admTree_);
            row->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            row->setText(admColFrequency, QString::number(pos.first / box::Constants::giga_));
            row->setText(admColReal, QString::number(pos.second.real()));
            row->setText(admColImaginary, QString::number(pos.second.imag()));
        }
    }
}

// Paste text into the specified admittance table column
void PlateAdmittanceEditDlg::pasteToColumn(int colNumber) {
    if(isPlateSizeSelected()) {
        QList<QTreeWidgetItem*> selectedItems = admTree_->selectedItems();
        int rowNumber = 0;
        if(!selectedItems.empty()) {
            rowNumber = admTree_->indexOfTopLevelItem(selectedItems.front());
        }
        const QClipboard* clipboard = QApplication::clipboard();
        const QMimeData* mimeData = clipboard->mimeData();
        if(mimeData->hasText()) {
            // Split into lines
            QStringList lines = mimeData->text().split('\n');
            for(const auto& line : lines) {
                // Create or find the row
                QTreeWidgetItem* row;
                if(rowNumber >= admTree_->topLevelItemCount()) {
                    row = new QTreeWidgetItem(admTree_);
                    row->setFlags(
                            Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                } else {
                    row = admTree_->topLevelItem(rowNumber);
                }
                // Write the text to the specified column
                row->setText(colNumber, line.trimmed());
                rowNumber++;
            }
        } else {
            std::cout << "Nothing of the right format to paste" << std::endl;
        }
    }
}

// Paste into the frequency column
void PlateAdmittanceEditDlg::onPasteFrequency() {
    pasteToColumn(admColFrequency);
}

// Paste into the real column
void PlateAdmittanceEditDlg::onPasteReal() {
    pasteToColumn(admColReal);
}

// Paste into the imaginary column
void PlateAdmittanceEditDlg::onPasteImaginary() {
    pasteToColumn(admColImaginary);
}

// Clear all the data
void PlateAdmittanceEditDlg::onClearAll() {
    admTree_->clear();
}

// Extract the admittance table data from the tree
void PlateAdmittanceEditDlg::readAdmittanceList() {
    // The current plate size
    auto admPos = table_.find(currentPlateSize_);
    if(admPos != table_.end()) {
        fdtd::AdmittanceData& data = admPos->second;
        data.clear();
        for(int row = 0; row < admTree_->topLevelItemCount(); row++) {
            QTreeWidgetItem* item = admTree_->topLevelItem(row);
            double frequency = item->text(admColFrequency).toDouble() * box::Constants::giga_;
            double real = item->text(admColReal).toDouble();
            double imaginary = item->text(admColImaginary).toDouble();
            data.addPoint(frequency, std::complex<double>(real, imaginary));
        }
        data.cellSize(cellSizeEdit_->text().toDouble());
    }
}

// The contents of the dialog have been accepted
void PlateAdmittanceEditDlg::onAccepted() {
    readAdmittanceList();
    tableType_ = (fdtd::PlateAdmittance::PlateType) tableTypeList_->currentIndex();
}

// Add a new plate size
void PlateAdmittanceEditDlg::onNewPlateSize() {
    readAdmittanceList();
    table_.insert(std::pair<double, fdtd::AdmittanceData>(0.0, {}));
    fillPlateList();
}

// Remove the current plate size
void PlateAdmittanceEditDlg::onDeletePlateSize() {
    table_.erase(currentPlateSize_);
    fillPlateList();
}

// A plate size has been changed in place
void PlateAdmittanceEditDlg::onPlateSizeChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        if(column == plateColSize) {
            double newPlateSize = item->text(plateColSize).toDouble();
            if(table_.find(newPlateSize) == table_.end()) {
                fdtd::AdmittanceData data = table_[currentPlateSize_];
                table_.erase(currentPlateSize_);
                table_.insert(std::pair<double, fdtd::AdmittanceData>(newPlateSize, data));
            }
            fillPlateList(newPlateSize);
        }
    }
}

// Copy the table to the clipboard
void PlateAdmittanceEditDlg::onCopyTable() {
    // Convert the table into XML
    auto* o = new xml::DomObject("plateadmittancetable");
    *o << xml::Obj("tabletype") << tableType_;
    for(auto& pos : table_) {
        *o << xml::Open("admittance");
        *o << xml::Obj("platesize") << pos.first;
        *o << pos.second;
        *o << xml::Close("admittance");
    }
    // Convert the XML into text
    xml::DomDocument dom(o);
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom, text);
    // Add the text to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    QByteArray data(text.c_str());
    mimeData->setData("text/plain", data);
    mimeData->setData("fdtdlife/plateadmittancetable", data);
    clipboard->setMimeData(mimeData);
}

// Paste the excel mode data
// The excel tabbed text mode is:
//     <ignored> <plateSize%> ... <plateSize%>
//     <frequencyGHz> <complexAdmittance> ... <complexAdmittance>
//     ...
//     <frequencyGHz> <complexAdmittance> ... <complexAdmittance>
void PlateAdmittanceEditDlg::pasteExcelFormat(const QMimeData* mimeData) {
    // Clear the current data
    table_.clear();
    tableType_ = fdtd::PlateAdmittance::plateTypeSquarePlate;
    // Read the text
    // Split into lines
    std::string text = mimeData->data("text/plain").toStdString();
    std::vector<std::string> lines = box::Utility::split(text, "\n");
    std::vector<double> plateSizes;
    bool firstLine = true;
    for(const auto& line : lines) {
        // Split the line into columns
        std::vector<std::string> cols = box::Utility::split(line, "\t");
        size_t colNum = 0;
        long long frequency = 0;
        for(const auto& col : cols) {
            if(firstLine) {
                if(colNum == 0) {
                    // Skip the frequency column header
                } else {
                    // Make or find an item for this table
                    if(!col.empty()) {
                        double plateSize = box::Utility::toDouble(col);
                        plateSizes.push_back(plateSize);
                        if(table_.count(plateSize) == 0) {
                            table_.insert(std::pair<double, fdtd::AdmittanceData>(
                                    plateSize, {}));
                        }
                    }
                }
            } else {
                if(colNum == 0) {
                    // Remember the frequency of this row
                    frequency = static_cast<long long>(
                            box::Utility::toDouble(col) * box::Constants::giga_);
                } else {
                    // Make an entry for this admittance
                    std::string::size_type splitPos = col.find_last_of("+-");
                    std::string::size_type iPos = col.find('i');
                    if(splitPos != std::string::npos && iPos != std::string::npos) {
                        std::string realPart = col.substr(0, splitPos);
                        std::string imagPart = col.substr(splitPos, iPos - splitPos);
                        std::complex<double> admittance(box::Utility::toDouble(realPart),
                                                        box::Utility::toDouble(imagPart));
                        table_[plateSizes[colNum-1]].addPoint(frequency, admittance);
                    }
                }
            }
            ++colNum;
        }
        firstLine = false;
    }
}

// Paste the internal XML format
void PlateAdmittanceEditDlg::pasteInternalFormat(const QMimeData* mimeData) {
    // Clear the current data
    table_.clear();
    tableType_ = fdtd::PlateAdmittance::plateTypeSquareHole;
    // Read the XML
    std::string text = mimeData->data("fdtdlife/plateadmittancetable").toStdString();
    xml::DomDocument dom("plateadmittancetable");
    xml::Reader reader;
    try {
        reader.readString(&dom, text);
        *dom.getObject() >> xml::Obj("tabletype") >> tableType_;
        for(auto& o : dom.getObject()->obj("admittance")) {
            double plateSize = 0.0;
            *o >> xml::Obj("platesize") >> plateSize;
            fdtd::AdmittanceData data;
            *o >> data;
            table_[plateSize] = data;
        }
    } catch(xml::Exception& e) {
        std::cout << "Admittance table paste failed to read XML: " << e.what() << std::endl;
    }
}

// Paste a table from the clipboard
void PlateAdmittanceEditDlg::onPasteTable() {
    QClipboard* clipboard = QApplication::clipboard();
    const QMimeData* mimeData = clipboard->mimeData();
    // Get the mime data
    if(mimeData->hasFormat("fdtdlife/plateadmittancetable")) {
        pasteInternalFormat(mimeData);
        fillPlateList();
    } else if(mimeData->hasFormat("text/plain")) {
        pasteExcelFormat(mimeData);
        fillPlateList();
    }
}


// Clear the table
void PlateAdmittanceEditDlg::onClearTable() {
    table_.clear();
    tableType_ = fdtd::PlateAdmittance::plateTypeSquareHole;
    fillPlateList();
}

// Paste a step into the admittance table
// The step is assumed to contain transmittance and phase shift data for
// the current plate size.
void PlateAdmittanceEditDlg::onPasteAdmittance() {
    QClipboard* clipboard = QApplication::clipboard();
    // Get the mime data
    if(clipboard->mimeData()->hasFormat("fdtdlife/admittance")) {
        // Read the XML
        std::string text = clipboard->mimeData()->data("fdtdlife/admittance").toStdString();
        xml::DomDocument dom("admittance");
        xml::Reader reader;
        try {
            reader.readString(&dom, text);
            fdtd::AdmittanceData admittance;
            *dom.getObject() >> admittance;
            if(isPlateSizeSelected()) {
                table_[currentPlateSize_] = admittance;
                fillAdmittanceList();
            }
        } catch(xml::Exception& e) {
            std::cout << "Could not paste admittance data: " << e.what() << std::endl;
        }
    }
}
