/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GuiChangeDetector.h"
#include <algorithm>
#include <Box/Utility.h>


// Constructor
GuiChangeDetector::GuiChangeDetector() :
        changeDetected_(false),
        why_(0) {
}

// Test a string
std::string GuiChangeDetector::testString(QLineEdit* widget, const std::string& original, int why) {
    std::string value = widget->text().toStdString();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a double
double GuiChangeDetector::testDouble(QLineEdit* widget, double original, int why) {
    bool ok;
    double value = widget->text().toDouble(&ok);
    if(!ok) {
        value = original;
    }
    if(!box::Utility::equals(value, original)) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a uint64_t
uint64_t GuiChangeDetector::testUint64T(QLineEdit* widget, uint64_t original, int why, int base) {
    bool ok;
    uint64_t value = (uint64_t) widget->text().toULongLong(&ok, base);
    if(!ok) {
        value = original;
    }
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a size_t
size_t GuiChangeDetector::testSizeT(QLineEdit* widget, size_t original, int why, int base) {
    bool ok;
    size_t value = (size_t) widget->text().toULongLong(&ok, base);
    if(!ok) {
        value = original;
    }
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test an int
int GuiChangeDetector::testInt(QLineEdit* widget, int original, int why, int base) {
    bool ok;
    int value = widget->text().toInt(&ok, base);
    if(!ok) {
        value = original;
    }
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test an integer from a combo list
int GuiChangeDetector::testInt(QComboBox* widget, int original, int why) {
    // The selection number
    int value = widget->currentIndex();
    // Do we need to convert this to an item number using the associated user data?
    if(widget->currentData().isValid()) {
        bool ok = false;
        int convertData = widget->currentData().toInt(&ok);
        if(ok) {
            value = convertData;
        }
    }
    // Has there been a change
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test an integer from a spin box
int GuiChangeDetector::testInt(QSpinBox* widget, int original, int why) {
    int value = widget->value();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a boolean from a check box
bool GuiChangeDetector::testBool(QCheckBox* widget, bool original, int why) {
    bool value = widget->isChecked();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a component selection for a change
box::ComponentSel GuiChangeDetector::testComponentSel(QCheckBox* widgetX, QCheckBox* widgetY,
                                                      QCheckBox* widgetZ,
                                                      const box::ComponentSel& original, int why) {
    box::ComponentSel value(widgetX->isChecked(), widgetY->isChecked(), widgetZ->isChecked());
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Record the result of an external test
void GuiChangeDetector::externalTest(bool result, int why) {
    if(result) {
        changeDetected_ = true;
        why_ = why;
    }
}

// Test a double box::Vector
box::Vector<double> GuiChangeDetector::testVectorDouble(
        QLineEdit* widgetX, QLineEdit* widgetY,
        QLineEdit* widgetZ,
        const box::Vector<double>& original, int why) {
    bool okX;
    bool okY;
    bool okZ;
    box::Vector<double> value = {
            widgetX->text().toDouble(&okX),
            widgetY->text().toDouble(&okY),
            widgetZ->text().toDouble(&okZ)};
    if(!okX || !okY || !okZ) {
        value = original;
    }
    if(!box::Utility::equals(value.x(), original.x())
       || !box::Utility::equals(value.y(), original.y())
       || !box::Utility::equals(value.z(), original.z())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

box::Polynomial GuiChangeDetector::testPolynomial(
        QLineEdit* widgetA, QLineEdit* widgetB,
        QLineEdit* widgetC, QLineEdit* widgetD,
        QLineEdit* widgetE, QLineEdit* widgetF,
        const box::Polynomial& original, int why) {
    bool okA;
    bool okB;
    bool okC;
    bool okD;
    bool okE;
    bool okF;
    box::Polynomial value = {
            widgetA->text().toDouble(&okA),
            widgetB->text().toDouble(&okB),
            widgetC->text().toDouble(&okC),
            widgetD->text().toDouble(&okD),
            widgetE->text().toDouble(&okE),
            widgetF->text().toDouble(&okF)};
    if(!okA || !okB || !okC || !okD || !okE || !okF) {
        value = original;
    }
    if(!box::Utility::equals(value.a(), original.a())
       || !box::Utility::equals(value.b(), original.b())
       || !box::Utility::equals(value.c(), original.c())
       || !box::Utility::equals(value.d(), original.d())
       || !box::Utility::equals(value.e(), original.e())
       || !box::Utility::equals(value.f(), original.f())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a double gene specs object
gs::GeneBitsSpecDouble GuiChangeDetector::testGeneBitsSpecDouble(
        QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
        const gs::GeneBitsSpecDouble& original, int why) {
    bool okBits;
    bool okMin;
    bool okMax;
    gs::GeneBitsSpecDouble value = {
            (size_t) widgetBits->text().toULongLong(&okBits),
            widgetMin->text().toDouble(&okMin),
            widgetMax->text().toDouble(&okMax)};
    if(!okBits || !okMin || !okMax) {
        value = original;
    }
    if(value.bits() != original.bits()
       || !box::Utility::equals(value.min(), original.min())
       || !box::Utility::equals(value.max(), original.max())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test a log double gene specs object
gs::GeneBitsSpecLogDouble GuiChangeDetector::testGeneBitsSpecLogDouble(
        QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
        const gs::GeneBitsSpecLogDouble& original, int why) {
    bool okBits;
    bool okMin;
    bool okMax;
    gs::GeneBitsSpecLogDouble value = {
            (size_t) widgetBits->text().toULongLong(&okBits),
            widgetMin->text().toDouble(&okMin),
            widgetMax->text().toDouble(&okMax)};
    if(!okBits || !okMin || !okMax) {
        value = original;
    }
    if(value.bits() != original.bits()
       || !box::Utility::equals(value.min(), original.min())
       || !box::Utility::equals(value.max(), original.max())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test integer gene bits object
template<class T>
gs::GeneBitsSpecInteger<T> GuiChangeDetector::testGeneBitsSpecInteger(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const gs::GeneBitsSpecInteger<T>& original, int why) {
    bool okBits;
    bool okMin;
    gs::GeneBitsSpecInteger<T> value = {
            widgetBits->text().toULong(&okBits),
            static_cast<T>(widgetMin->text().toLongLong(&okMin))};
    if(!okBits || !okMin) {
        value = original;
    }
    if(value.bits() != original.bits()
       || !box::Utility::equals(value.min(), original.min())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}
template gs::GeneBitsSpecInteger<int> GuiChangeDetector::testGeneBitsSpecInteger<int>(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const gs::GeneBitsSpecInteger<int>& original, int why);
template gs::GeneBitsSpecInteger<size_t> GuiChangeDetector::testGeneBitsSpecInteger<size_t>(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const gs::GeneBitsSpecInteger<size_t>& original, int why);

// Test a new style integer gene bits object
template<class T>
ga::GeneBitsInteger<T> GuiChangeDetector::testGeneBitsInteger(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const ga::GeneBitsInteger<T>& original, int why) {
    bool okBits;
    bool okMin;
    ga::GeneBitsInteger<T> value = {
            widgetBits->text().toULong(&okBits),
            static_cast<T>(widgetMin->text().toLongLong(&okMin))};
    if(!okBits || !okMin) {
        value = original;
    }
    if(value.bits() != original.bits()
       || !box::Utility::equals(value.min(), original.min())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}
template ga::GeneBitsInteger<int> GuiChangeDetector::testGeneBitsInteger<int>(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const ga::GeneBitsInteger<int>& original, int why);
template ga::GeneBitsInteger<size_t> GuiChangeDetector::testGeneBitsInteger<size_t>(
        QLineEdit* widgetBits, QLineEdit* widgetMin,
        const ga::GeneBitsInteger<size_t>& original, int why);

// Test a new style float gene bits object
ga::GeneBitsFloat GuiChangeDetector::testGeneBitsFloat(
        QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
        const ga::GeneBitsFloat& original, int why) {
    bool okBits;
    bool okMin;
    bool okMax;
    ga::GeneBitsFloat value = {
            widgetBits->text().toULong(&okBits),
            widgetMin->text().toDouble(&okMin),
            widgetMax->text().toDouble(&okMax)};
    if(!okBits || !okMin || !okMax) {
        value = original;
    }
    if(value.bits() != original.bits() ||
       !box::Utility::equals(value.min(), original.min()) ||
       !box::Utility::equals(value.max(), original.max())) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}
