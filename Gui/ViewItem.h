/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_VIEWITEM_H
#define FDTDLIFE_VIEWITEM_H

#include "QtIncludes.h"
#include <Sensor/SensorUser.h>
#include <Xml/DomObject.h>
class ViewFrame;
class ViewItemDlg;
class ViewFrameDlg;
namespace sensor { class DataSpec; }
namespace sensor { class Sensor;}

// Base class for all view items
class ViewItem : public sensor::SensorUser {
public:
    ViewItem(int identifier, int sensorId, const char* sensorData,
             ViewFrame* view);
    virtual ~ViewItem();
    virtual void initialise(int left, int top, int width, int height);
    void paint(QPainter* painter);
    virtual void paintData(QPainter* /*painter*/) {}
    virtual void leftClick(QPoint /*pos*/) {}
    virtual void contextMenu(QPoint /*pos*/) {}
    virtual QSize sizeHint() {return QSize(0,0);}
    virtual void sizeRequest(QSize& /*size*/) {}
    virtual void sensorRemoved(sensor::Sensor* s);
    virtual void sensorAdded(sensor::Sensor* s);
    virtual ViewItemDlg* makeDialog(ViewFrameDlg* dlg);
    virtual void writeConfig(xml::DomObject& root) const;
    virtual void readConfig(xml::DomObject& root);
    friend xml::DomObject& operator<<(xml::DomObject& o, const ViewItem& v) {v.writeConfig(o); return o;}
    friend xml::DomObject& operator>>(xml::DomObject& o, ViewItem& v) {v.readConfig(o); return o;}
    void removeSpec(sensor::DataSpec* spec) override;
    virtual void getMimeData(QMimeData* /*mimeData*/) {}

protected:
    ViewFrame* view_;
    QRect zone_;
    QRect titleBar_;
    QRect dataArea_;
    int identifier_;
    int sensorId_;
    std::string sensorData_;
    sensor::DataSpec* spec_;
    QString title_;
    bool markersOn_;

public:
    // Getters
    int identifier() {return identifier_;}
    int sensorId() {return sensorId_;}
    const char* sensorData() {return sensorData_.c_str();}
    QRect zone() {return zone_;}
    QRect titleBar() {return titleBar_;}
    QRect dataArea() {return dataArea_;}
    ViewFrame* view() {return view_;}
    QString title() {return title_;}
    void toggleMarkers() {markersOn_ = !markersOn_;}
};


#endif //FDTDLIFE_VIEWITEM_H
