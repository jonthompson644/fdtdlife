/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCEFNLAYERGENETEMPLATEDLG_H
#define FDTDLIFE_ADMITTANCEFNLAYERGENETEMPLATEDLG_H

#include "QtIncludes.h"
#include "GeneTemplateDlg.h"

namespace gs {class GeneTemplate;}
class GeneticSearchGeneTemplateDlg;

class AdmittanceFnLayerGeneTemplateDlg : public GeneTemplateDlg {
Q_OBJECT
public:
    AdmittanceFnLayerGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg, gs::GeneTemplate* geneTemplate);
    void initialise() override;

protected slots:
    void onChange() override;

protected:
    QLineEdit* realABitsEdit_;
    QLineEdit* realAMinEdit_;
    QLineEdit* realAMaxEdit_;
    QLineEdit* realBBitsEdit_;
    QLineEdit* realBMinEdit_;
    QLineEdit* realBMaxEdit_;
    QLineEdit* realCBitsEdit_;
    QLineEdit* realCMinEdit_;
    QLineEdit* realCMaxEdit_;
    QLineEdit* realDBitsEdit_;
    QLineEdit* realDMinEdit_;
    QLineEdit* realDMaxEdit_;
    QLineEdit* realEBitsEdit_;
    QLineEdit* realEMinEdit_;
    QLineEdit* realEMaxEdit_;
    QLineEdit* realFBitsEdit_;
    QLineEdit* realFMinEdit_;
    QLineEdit* realFMaxEdit_;
    QLineEdit* imagABitsEdit_;
    QLineEdit* imagAMinEdit_;
    QLineEdit* imagAMaxEdit_;
    QLineEdit* imagBBitsEdit_;
    QLineEdit* imagBMinEdit_;
    QLineEdit* imagBMaxEdit_;
    QLineEdit* imagCBitsEdit_;
    QLineEdit* imagCMinEdit_;
    QLineEdit* imagCMaxEdit_;
    QLineEdit* imagDBitsEdit_;
    QLineEdit* imagDMinEdit_;
    QLineEdit* imagDMaxEdit_;
    QLineEdit* imagEBitsEdit_;
    QLineEdit* imagEMinEdit_;
    QLineEdit* imagEMaxEdit_;
    QLineEdit* imagFBitsEdit_;
    QLineEdit* imagFMinEdit_;
    QLineEdit* imagFMaxEdit_;
    QTabWidget* groupTab_;
    QWidget* realPage_;
    QWidget* imagPage_;
};


#endif //FDTDLIFE_ADMITTANCEFNLAYERGENETEMPLATEDLG_H
