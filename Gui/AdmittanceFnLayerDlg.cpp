/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnLayerDlg.h"
#include "DialogHelper.h"
#include <Domain/AdmittanceFnLayer.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Second stage c
void AdmittanceFnLayerDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::AdmittanceFnLayer*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) =
            DialogHelper::vectorItem(layout_, "Layer Position x,y,z (m)");
    std::tie(raEdit_, rbEdit_, rcEdit_, rdEdit_, reEdit_, rfEdit_) =
            DialogHelper::polynomialItem(layout_, "Real:");
    std::tie(iaEdit_, ibEdit_, icEdit_, idEdit_, ieEdit_, ifEdit_) =
            DialogHelper::polynomialItem(layout_, "Imag:");
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(raEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rbEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rcEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rdEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(reEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rfEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(iaEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ibEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(icEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(idEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ieEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ifEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void AdmittanceFnLayerDlg::tabChanged() {
    onChange();
}

// Initialise the GUI parameters
void AdmittanceFnLayerDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    raEdit_->setText(QString::number(shape_->real().a()));
    rbEdit_->setText(QString::number(shape_->real().b()));
    rcEdit_->setText(QString::number(shape_->real().c()));
    rdEdit_->setText(QString::number(shape_->real().d()));
    reEdit_->setText(QString::number(shape_->real().e()));
    rfEdit_->setText(QString::number(shape_->real().f()));
    iaEdit_->setText(QString::number(shape_->imag().a()));
    ibEdit_->setText(QString::number(shape_->imag().b()));
    icEdit_->setText(QString::number(shape_->imag().c()));
    idEdit_->setText(QString::number(shape_->imag().d()));
    ieEdit_->setText(QString::number(shape_->imag().e()));
    ifEdit_->setText(QString::number(shape_->imag().f()));
}

// An edit box has changed
void AdmittanceFnLayerDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newReal = c.testPolynomial(raEdit_, rbEdit_, rcEdit_, rdEdit_,
                                        reEdit_, rfEdit_, shape_->real(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newImag = c.testPolynomial(iaEdit_, ibEdit_, icEdit_, idEdit_,
                                        ieEdit_, ifEdit_, shape_->imag(),
                                        FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->real(newReal);
            shape_->imag(newImag);
            dlg_->model()->doNotification(c.why());
        }
    }
}
