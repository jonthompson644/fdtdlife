/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SQUAREPLATEDLG_H
#define FDTDLIFE_SQUAREPLATEDLG_H

#include "QtIncludes.h"
#include "ShapeDlg.h"

namespace domain { class Shape; }
namespace domain { class SquarePlate; }

// The dialog sub-panel for a square plate
class SquarePlateDlg : public ShapeDlg {
Q_OBJECT
public:
    SquarePlateDlg() = default;
    void construct(ShapesDlg* dlg, domain::Shape* shape) override;
    void tabChanged() override;
    void initialise() override;

protected slots:
    void onChange() override;

protected:
    QLineEdit* x1Edit_{};
    QLineEdit* y1Edit_{};
    QLineEdit* z1Edit_{};
    QLineEdit* cellSizeEdit_{};
    QLineEdit* plateSizeEdit_{};
    QLineEdit* repeatXEdit_{};
    QLineEdit* repeatYEdit_{};
    QLineEdit* offsetXEdit_{};
    QLineEdit* offsetYEdit_{};
    QLineEdit* thicknessEdit_{};
    QComboBox* fillMaterialList_{};
    QLineEdit* incrementXEdit_{};
    QLineEdit* incrementYEdit_{};
    QLineEdit* incrementZEdit_{};
    QLineEdit* duplicateXEdit_{};
    QLineEdit* duplicateYEdit_{};
    QLineEdit* duplicateZEdit_{};
    domain::SquarePlate* shape_{};
};


#endif //FDTDLIFE_SQUAREPLATEDLG_H
