/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "QtHexagon.h"
#include <Box/Constants.h>

// Initialising constructor
QtHexagon::QtHexagon(double r, const QPointF& center) {
    double rSinPiBy3 = r * box::Constants::sinPiBy3_;
    double rBy2 = r / 2.0;
    points_[0] = center + QPointF(-rSinPiBy3, -rBy2);
    points_[1] = center + QPointF(0.0, -r);
    points_[2] = center + QPointF(rSinPiBy3, -rBy2);
    points_[3] = center + QPointF(rSinPiBy3, rBy2);
    points_[4] = center + QPointF(0.0, r);
    points_[5] = center + QPointF(-rSinPiBy3, rBy2);
}

// Copy constructor
QtHexagon::QtHexagon(const QtHexagon& other) {
    *this = other;
}

// Assignment operator
QtHexagon& QtHexagon::operator=(const QtHexagon& other) {
    for(size_t i=0; i<pointsInHex_; i++) {
        points_[i] = other.points_[i];
    }
    return *this;
}

// Offset a hexagon by a size
QtHexagon& QtHexagon::operator+=(const QPointF& offset) {
    for(auto & point : points_) {
        point += offset;
    }
    return *this;
}

// Draw the hexagon into the painter using the current
// line and fill
void QtHexagon::draw(QPainter* painter) {
    painter->drawConvexPolygon(points_, pointsInHex_);
}

// Return a rectangle inside which some text could be written
QRect QtHexagon::textRect() {
    return QRectF(points_[5], points_[2]).toRect();
}
