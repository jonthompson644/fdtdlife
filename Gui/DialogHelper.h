/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DIALOGHELPER_H
#define FDTDLIFE_DIALOGHELPER_H


#include "QtIncludes.h"
#include <Box/Vector.h>
#include <Box/Polynomial.h>
#include <initializer_list>
#include <list>
#include <utility>

class QGridLayout;
class PushButtonMenu;

class DialogHelper {
public:
    explicit DialogHelper(QGridLayout* layout);
    // Old API
    QLineEdit* field(const QString& name, double value, bool readOnly=false);
    QLineEdit* field(const QString& name, int value, bool readOnly=false);
    QLineEdit* field(const QString& name, uint64_t value, bool readOnly=false);
    QLineEdit* field(const QString& name, const QString& value, bool readOnly=false);
    QCheckBox* field(const QString& name, bool value, bool readOnly=false);
    std::tuple<QLineEdit*, QLineEdit*> field(const QString& name, double value, double rbv, bool secondIsRbv=true);
    std::tuple<QLineEdit*, QLineEdit*> field(const QString& name, int value, int rbv);
    std::tuple<QCheckBox*, QLineEdit*> field(const QString& name, bool v1, double v2);
    std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> field(const QString& name, const box::Vector<double>& value, bool readOnly=false);
    std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> field(const QString& name, double a, double b, double c);
    void field(const QString& name, QWidget* w);
    void field(const QString& name, QLayout* l);
    void field(const QString& name);
    void field(QLayout* l);
    void field(QWidget* w);
    void header(const QStringList& cols);
    QList<QLineEdit*> field(const QString& name, const QList<double>& values, bool readOnly=false);
    QList<QLineEdit*> field(const QString& name, const QList<int>& values, bool readOnly=false);
    QList<QLineEdit*> field(const QString& name, const QList<size_t>& values, bool readOnly=false);
    // New API
    static QCheckBox* boolItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static QLineEdit* doubleItem(QBoxLayout* layout, const QString& name="", bool readOnly=false);
    static QLineEdit* doubleItem(QBoxLayout* layout, const QString& name, double value, bool readOnly=false);
    static QLineEdit* doubleItem(QBoxLayout* layout, double value, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> vectorItem(QBoxLayout* layout, const QString& name, const box::Vector<double>& v, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> vectorItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> vectorItem(QGridLayout* layout, int row, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*> polynomialItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*, QLineEdit*> polynomialItem(QBoxLayout* layout, const QString& name, const box::Polynomial& v, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> geneBitsSpecDoubleItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*> geneBitsSpecIntegerItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> geneBitsSpecDoubleItem(QGridLayout* layout, const QString& name, int row, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*> geneBitsSpecIntMinItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*> geneBitsIntegerItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static std::tuple<QLineEdit*, QLineEdit*, QLineEdit*> geneBitsFloatItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static QLineEdit* intItem(QBoxLayout* layout, const QString& name, bool readOnly=false);
    static QLineEdit* intItem(QBoxLayout* layout, const QString& name, int value, bool readOnly=false);
    static QLineEdit* sizeTItem(QBoxLayout* layout, const QString& name, size_t value, bool readOnly=false);
    static QSpinBox* intSpinItem(QBoxLayout* layout, const QString& name, int min, int max, int value);
    static QLineEdit* textItem(QBoxLayout* layout, const QString& name="", bool readOnly=false);
    static QLineEdit* textItem(QBoxLayout* layout, const QString& name, const QString& value, bool readOnly=false);
    static QComboBox* dropListItem(QBoxLayout* layout, const QString& name, const std::initializer_list<const char*>& items);
    static QComboBox* dropListItem(QBoxLayout* layout, const QString& name, const QStringList& items={});
    static QComboBox* dropListItem(QBoxLayout* layout, const QString& name, int value, const QStringList& items={});
    static QComboBox* dropListItem(QBoxLayout* layout, const QStringList& items={});
    static QPushButton* buttonItem(QBoxLayout* layout, const QString& name);
    static PushButtonMenu* buttonMenuItem(QBoxLayout* layout, const std::string& name, const std::list<std::string>& items);
    static PushButtonMenu* buttonMenuItem(QBoxLayout* layout, const std::string& name, const std::list<std::pair<int, std::string>>& items);
    static QLineEdit* binaryItem(QBoxLayout* layout, const QString& name, uint64_t value, bool readOnly=false);
    static QHBoxLayout* lineLayout(QBoxLayout* layout);
    static QLabel* labelItem(QBoxLayout* layout, const QString& name);
protected:
    QGridLayout* layout_;
    QLineEdit* editItem(const QString& value, bool readOnly, QValidator* validator, int y, int x);
};


#endif //FDTDLIFE_DIALOGHELPER_H
