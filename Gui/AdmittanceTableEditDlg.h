/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCETABLEEDITDLG_H
#define FDTDLIFE_ADMITTANCETABLEEDITDLG_H

#include "QtIncludes.h"
#include <map>
#include <complex>
#include <AdmittanceData.h>

class AdmittanceTableEditDlg : public QDialog {
    Q_OBJECT
public:
    AdmittanceTableEditDlg(QWidget* parent, const fdtd::AdmittanceData& table);
    const fdtd::AdmittanceData& table() const {return table_;}
protected slots:
    void onPasteFrequency();
    void onPasteReal();
    void onPasteImaginary();
    void onClearAll();
    void onAccepted();
protected:
    enum {columnFrequency=0, columnReal, columnImaginary, numColumns};
    void pasteToColumn(int colNumber);
protected:
    fdtd::AdmittanceData table_;
    QDialogButtonBox* buttons_;
    QTreeWidget* tableTree_;
    QPushButton* pasteFrequencyButton_;
    QPushButton* pasteRealButton_;
    QPushButton* pasteImaginaryButton_;
    QPushButton* clearAllButton_;
    QLineEdit* cellSizeEdit_;
};


#endif //FDTDLIFE_ADMITTANCETABLEEDITDLG_H
