/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TWODSLICEVIEW_H
#define FDTDLIFE_TWODSLICEVIEW_H


#include <Sensor/DataSpec.h>
#include "ViewItem.h"
#include <Sensor/TwoDSlice.h>

class TwoDSliceView : public ViewItem {

public:
    enum ColorMap {colorMapExtKindlemann, colorMapDivergingBY, colorMapMaterial};
    friend xml::DomObject& operator>>(xml::DomObject& o, ColorMap& v) {o >> (int&)v; return o;}

public:
    // Construction
    TwoDSliceView(int identifier, int sensorId, const char* sensorData,
                  ViewFrame* view);
    ~TwoDSliceView() override;
    // Overrides of ViewItem
    void paintData(QPainter* painter) override;
    void initialise(int left, int top, int width, int height) override;
    QSize sizeHint() override;
    void sizeRequest(QSize& size) override;
    ViewItemDlg* makeDialog(ViewFrameDlg* dlg) override;
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void leftClick(QPoint pos) override;
    // Setters
    void set(ColorMap colorMap, int repeatX, int repeatY, double displayMax);
    // Getters
    ColorMap colorMap() const {return colorMap_;}
    int repeatX() const {return repeatX_;}
    int repeatY() const {return repeatY_;}
    double displayMax() const {return displayMax_;}

protected:
    // Members
    sensor::DataSpec::Info info_;
    QVector<QBrush*> brushes_;
    int cellSizeX_ {};
    int cellSizeY_ {};
    double scale_ {1.0};
    double offset_ {};
    int nSliceX_ {};
    int nSliceY_ {};
    int sliceOffset_ {};
    // Configuration
    ColorMap colorMap_ {colorMapExtKindlemann};   // Which color map to use
    int repeatX_ {1};         // The x repeat factor
    int repeatY_ {1};         // The y repeat factor
    double displayMax_ {1.0};   // The max value to display
    static const int pixmapCellSize_ {16};  // The size of a pixmap cell

    // Helper functions
    bool makeColorMap();
    void clearColorMap();
    void paintSquareData(QPainter* painter);
};


#endif //FDTDLIFE_TWODSLICEVIEW_H
