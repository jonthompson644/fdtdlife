/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FarFieldSensorDlg.h"
#include "DialogHelper.h"
#include <Sensor/FarFieldSensor.h>
#include "FdTdLife.h"
#include "SensorsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
FarFieldSensorDlg::FarFieldSensorDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        SensorDlg(dlg, sensor),
        sensor_(dynamic_cast<sensor::FarFieldSensor*>(sensor)) {
    distanceEdit_ = DialogHelper::doubleItem(layout_, "Far field distance (m)");
    connect(distanceEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void FarFieldSensorDlg::initialise() {
    SensorDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    distanceEdit_->setText(QString::number(sensor_->distance()));
}

// An edit box has changed
void FarFieldSensorDlg::onChange() {
    SensorDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newDistance = c.testDouble(distanceEdit_, sensor_->distance(), FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            sensor_->distance(newDistance);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FarFieldSensorDlg::tabChanged() {
    onChange();
}

// The model has changed
void FarFieldSensorDlg::modelNotify() {
}
