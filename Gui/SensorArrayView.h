/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SENSORARRAYVIEW_H
#define FDTDLIFE_SENSORARRAYVIEW_H


#include "TwoDSliceView.h"

class SensorArrayView : public TwoDSliceView {
public:
    enum Scaling {scalingManual=0, scalingAutomaticMin, scalingFullAutomatic};
    friend xml::DomObject& operator>>(xml::DomObject& o, Scaling& v) {o >> (int&)v; return o;}
public:
    SensorArrayView(int identifier, int sensorId, const char* sensorData,
                  ViewFrame* view);
    void leftClick(QPoint pos) override;
    void paintData(QPainter* painter) override;
    ViewItemDlg* makeDialog(ViewFrameDlg* dlg) override;
    void set(ColorMap colorMap, int repeatX, int repeatY, Scaling scaling,
             double displayMin, double displayMax, double displayRange);
    // Getters
    Scaling scaling() const {return scaling_;}
    double displayMin() const {return displayMin_;}
    double displayRange() const {return displayRange_;}
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
protected:
    int currentCellX_;
    int currentCellY_;
    Scaling scaling_;  // Scaling mode
    double displayMin_;  // Minimum display value
    double displayRange_;  // The range to display
};


#endif //FDTDLIFE_SENSORARRAYVIEW_H
