/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Gs/GeneTemplate.h>
#include "IndividualEditDlg.h"
#include "ComboBoxDelegate.h"
#include "FdTdLife.h"
#include "RepeatCellGeneDlg.h"
#include <Gs/Individual.h>
#include <Gs/Chromosome.h>
#include "LayerSpacingGeneDlg.h"
#include "LayerPresentGeneDlg.h"
#include "DuplicateLayerGeneDlg.h"
#include "SquarePlateGeneDlg.h"
#include "DielectricLayerGeneDlg.h"
#include "DialogHelper.h"
#include "AdmittanceCatLayerGeneDlg.h"
#include "LayerSymmetryGeneDlg.h"
#include "BinaryPlateNxNGeneDlg.h"
#include "AdmittanceFnLayerGeneDlg.h"
#include "DielectricBlockGeneDlg.h"
#include "LensDimensionsGeneDlg.h"
#include "LensSquareGeneDlg.h"
#include "VariableGeneDlg.h"
#include <Gs/RepeatCellGene.h>
#include <Gs/LayerSpacingGene.h>
#include <Gs/LayerPresentGene.h>
#include <Gs/LayerSymmetryGene.h>
#include <Gs/SquarePlateGene.h>
#include <Gs/DielectricLayerGene.h>
#include <Gs/AdmittanceCatLayerGene.h>
#include <Gs/BinaryPlateNxNGene.h>
#include <Gs/DuplicateLayerGene.h>
#include <Gs/AdmittanceFnLayerGene.h>
#include <Gs/DielectricBlockGene.h>
#include <Gs/LensDimensionsGene.h>
#include <Gs/LongLensSquareGene.h>
#include <Gs/VariableGene.h>
#include <Gs/GeneticSearch.h>

// Constructor
IndividualEditDlg::IndividualEditDlg(QWidget* parent, gs::GeneticSearch* s,
                                     const std::shared_ptr<gs::Individual>& ind) :
        QDialog(parent),
        ind_(ind),
        s_(s) {
    auto mainLayout = new QVBoxLayout;
    auto colLayout = new QHBoxLayout;
    auto rightLayout = new QVBoxLayout;
    auto leftLayout = new QVBoxLayout;
    mainLayout->addLayout(colLayout);
    colLayout->addLayout(leftLayout);
    colLayout->addLayout(rightLayout);
    setWindowTitle(QString("Edit Individual %1").arg(ind->id()));
    // The chromosome hex value
    chromosomeEdit_ = new QTextEdit;
    chromosomeEdit_->setDisabled(true);
    chromosomeEdit_->setText(ind_->chromosome()->dataDisplay().c_str());
    leftLayout->addWidget(new QLabel("Chromosome"));
    leftLayout->addWidget(chromosomeEdit_);
    // The genes display
    geneTemplateTree_ = new QTreeWidget;
    geneTemplateTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    geneTemplateTree_->setColumnCount(numColumns);
    geneTemplateTree_->setHeaderLabels({"Id", "Type", "Info"});
    geneTemplateTree_->setItemDelegateForColumn(geneColType,
                                                new ComboBoxDelegate(
                                                        gs::GeneTemplate::geneTypeStrings_,
                                                        this));
    mainLayout->addWidget(new QLabel("Genes"));
    mainLayout->addWidget(geneTemplateTree_);
    // The individual's properties
    auto mainIndPropsLayout = new QHBoxLayout;
    rightLayout->addLayout(mainIndPropsLayout);
    stateCombo_ = DialogHelper::dropListItem(
            mainIndPropsLayout, "Processing State", (int) ind_->state(),
            {"Waiting", "Running", "Evaluated"});
    unfitnessEdit_ = DialogHelper::doubleItem(mainIndPropsLayout, "Unfitness",
                                              ind_->unfitness());
    fitnessResultsTree_ = new QTreeWidget;
    fitnessResultsTree_->setSelectionMode(QAbstractItemView::NoSelection);
    fitnessResultsTree_->setColumnCount(numFitnessCols);
    fitnessResultsTree_->setHeaderLabels(
            {"Pos", "Type", "Result", "Extra Info", "Extra Value"});
    rightLayout->addWidget(fitnessResultsTree_);
    fillFitnessResults();
    // The gene editing part
    geneDlgArea_ = new QStackedWidget;
    mainLayout->addWidget(geneDlgArea_);
    colLayout->addLayout(rightLayout);
    mainLayout->addLayout(colLayout);
    // Now initialise the genes and their panels
    for(auto geneTemplate : s_->geneFactory()->templates()) {
        // Load the gene tree
        auto item = new QTreeWidgetItem(geneTemplateTree_);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setText(geneColIdentifier,
                      QString::number(geneTemplate->identifier()));
        int row = geneTemplateTree_->indexOfTopLevelItem(item);
        QModelIndex index = geneTemplateTree_->model()->index(row, geneColType);
        geneTemplateTree_->model()->setData(index, QVariant((int) geneTemplate->geneType()));
        // Create the gene dialog
        gs::Gene* gene = ind_->chromosome()->getGene(row);
        item->setText(geneColInfo, gene->info(s_).c_str());
        switch(geneTemplate->geneType()) {
            case gs::GeneTemplate::geneTypeRepeatCell:
                geneDlgArea_->addWidget(
                        new RepeatCellGeneDlg(
                                this, dynamic_cast<gs::RepeatCellGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeSquarePlate:
                geneDlgArea_->addWidget(
                        new SquarePlateGeneDlg(
                                this, dynamic_cast<gs::SquarePlateGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeDielectricLayer:
                geneDlgArea_->addWidget(
                        new DielectricLayerGeneDlg(
                                this, dynamic_cast<gs::DielectricLayerGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeLayerSpacing:
                geneDlgArea_->addWidget(
                        new LayerSpacingGeneDlg(
                                this, dynamic_cast<gs::LayerSpacingGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeLayerPresent:
                geneDlgArea_->addWidget(
                        new LayerPresentGeneDlg(
                                this, dynamic_cast<gs::LayerPresentGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeLayerSymmetry:
                geneDlgArea_->addWidget(
                        new LayerSymmetryGeneDlg(
                                this, dynamic_cast<gs::LayerSymmetryGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeAdmittanceCatLayer:
                geneDlgArea_->addWidget(
                        new AdmittanceCatLayerGeneDlg(
                                this, dynamic_cast<gs::AdmittanceCatLayerGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeBinaryPlateNxN:
                geneDlgArea_->addWidget(
                        new BinaryPlateNxNGeneDlg(
                                this, dynamic_cast<gs::BinaryPlateNxNGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeDuplicateLayer:
                geneDlgArea_->addWidget(
                        new DuplicateLayerGeneDlg(
                                this, dynamic_cast<gs::DuplicateLayerGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeAdmittanceFn:
                geneDlgArea_->addWidget(
                        new AdmittanceFnLayerGeneDlg(
                                this, dynamic_cast<gs::AdmittanceFnLayerGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeDielectricBlock:
                geneDlgArea_->addWidget(
                        new DielectricBlockGeneDlg(
                                this, dynamic_cast<gs::DielectricBlockGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeLensDimensions:
                geneDlgArea_->addWidget(
                        new LensDimensionsGeneDlg(
                                this, dynamic_cast<gs::LensDimensionsGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeTransLensSquare:
            case gs::GeneTemplate::geneTypeLongLensSquare:
                geneDlgArea_->addWidget(
                        new LensSquareGeneDlg(
                                this, dynamic_cast<gs::LensSquareGene*>(gene)));
                break;
            case gs::GeneTemplate::geneTypeVariable:
                geneDlgArea_->addWidget(
                        new VariableGeneDlg(
                                this, dynamic_cast<gs::VariableGene*>(gene)));
                break;
            default:
                break;
        }
    }
    geneTemplateTree_->setCurrentItem(geneTemplateTree_->topLevelItem(0));
    // The buttons
    buttons_ = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttons_);
    // Connections
    connect(geneTemplateTree_, SIGNAL(itemSelectionChanged()),
            SLOT(geneSelected()));
    connect(buttons_, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons_, &QDialogButtonBox::rejected, this, &QDialog::reject);
    // Attach the layout to the dialog
    mainLayout->addStretch();
    geneSelected();
    setLayout(mainLayout);
}

// Fill the user interface using the selected gene
void IndividualEditDlg::geneSelected() {
    int row = geneTemplateTree_->indexOfTopLevelItem(geneTemplateTree_->currentItem());
    geneDlgArea_->setCurrentIndex(row);
}

// The dialog has been accepted
void IndividualEditDlg::accept() {
    // Update the individual
    for(int i = 0; i < geneDlgArea_->count(); i++) {
        auto* geneDlg = dynamic_cast<GeneDlg*>(geneDlgArea_->widget(i));
        geneDlg->updateIndividual();
    }
    ind_->state((gs::Individual::State) stateCombo_->currentIndex());
    ind_->unfitness(unfitnessEdit_->text().toDouble());
    // Now the base class
    QDialog::accept();
}

// Fill the fitness results tree with the partial results
void IndividualEditDlg::fillFitnessResults() {
    fitnessResultsTree_->clear();
    for(auto& r : ind_->fitnessResults()) {
        auto item = new QTreeWidgetItem(fitnessResultsTree_);
        item->setFlags(Qt::NoItemFlags);
        item->setText(fitnessColPosition, QString::number(r.position()));
        item->setText(fitnessColType, r.partType());
        item->setText(fitnessColResult, QString::number(r.result()));
        item->setText(fitnessColExtraInfo, r.extraInfo());
        item->setText(fitnessColExtraValue, QString::number(r.extraValue()));
    }
}

