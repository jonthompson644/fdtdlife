/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ZoneSourceDlg.h"
#include <Source/ZoneSource.h>
#include "DialogHelper.h"
#include "GuiChangeDetector.h"
#include "SourcesDlg.h"
#include "FdTdLife.h"

// Constructor
ZoneSourceDlg::ZoneSourceDlg(SourcesDlg* dlg, source::ZoneSource* source) :
        SourceDlg(dlg, source),
        source_(source) {
    // Line 1
    auto line1Layout = new QHBoxLayout;
    firstFrequencyEdit_ = DialogHelper::textItem(line1Layout, "First Frequency (Hz)");
    frequencyStepEdit_ = DialogHelper::textItem(line1Layout, "Frequency Step (Hz)");
    layout_->addLayout(line1Layout);
    // Line 2
    auto line2Layout = new QHBoxLayout;
    numFrequenciesEdit_ = DialogHelper::textItem(line2Layout, "Num Frequencies");
    amplitudeEdit_ = DialogHelper::doubleItem(line2Layout,
                                              "Electric Current Density (A/m)");
    layout_->addLayout(line2Layout);
    // Line 3
    auto line3Layout = new QHBoxLayout;
    azimuthEdit_ = DialogHelper::doubleItem(line3Layout, "Azimuth (-180..180 deg)");
    elevationEdit_ = DialogHelper::doubleItem(line3Layout, "Elevation (-90..90 deg)");
    polarisationEdit_ = DialogHelper::doubleItem(line3Layout,
                                                 "Polarisation (0..360 deg)");
    layout_->addLayout(line3Layout);
    // Line 4
    auto line4Layout = new QHBoxLayout;
    initialTimeEdit_ = DialogHelper::doubleItem(line4Layout, "Initial Time (s)");
    timeEdit_ = DialogHelper::doubleItem(line4Layout, "Run For (s)");
    continuousCheck_ = DialogHelper::boolItem(line4Layout, "Continuous");
    layout_->addLayout(line4Layout);
    // Line 5
    auto line5Layout = new QHBoxLayout;
    std::tie(xCenterEdit_, yCenterEdit_, zCenterEdit_) = DialogHelper::vectorItem(
            line5Layout, "Zone Center (m)");
    layout_->addLayout(line5Layout);
    // Line 6
    auto line6Layout = new QHBoxLayout;
    std::tie(xSizeEdit_, ySizeEdit_, zSizeEdit_) = DialogHelper::vectorItem(
            line6Layout, "Zone Size (m)");
    layout_->addLayout(line6Layout);
    // Connect the handlers
    connect(firstFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyStepEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numFrequenciesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(amplitudeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(azimuthEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(elevationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(polarisationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(continuousCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(timeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(initialTimeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(yCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(zCenterEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ySizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(zSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    // Fill the dialog
    initialise();
}

// Fill the dialog widgets
void ZoneSourceDlg::initialise() {
    // Base class first
    SourceDlg::initialise();
    // Now my stuff
    firstFrequencyEdit_->setText(source_->frequencies().first().text().c_str());
    frequencyStepEdit_->setText(source_->frequencies().spacing().text().c_str());
    numFrequenciesEdit_->setText(source_->frequencies().n().text().c_str());
    amplitudeEdit_->setText(QString::number(source_->amplitude()));
    azimuthEdit_->setText(QString::number(source_->azimuth()));
    elevationEdit_->setText(QString::number(source_->elevation()));
    polarisationEdit_->setText(QString::number(source_->polarisation()));
    continuousCheck_->setChecked(source_->continuous());
    timeEdit_->setText(QString::number(source_->time()));
    initialTimeEdit_->setText(QString::number(source_->initialTime()));
    xCenterEdit_->setText(QString::number(source_->zoneCenter().x()));
    yCenterEdit_->setText(QString::number(source_->zoneCenter().y()));
    zCenterEdit_->setText(QString::number(source_->zoneCenter().z()));
    xSizeEdit_->setText(QString::number(source_->zoneSize().x()));
    ySizeEdit_->setText(QString::number(source_->zoneSize().y()));
    zSizeEdit_->setText(QString::number(source_->zoneSize().z()));
}

// A source edit field has changed
void ZoneSourceDlg::onChange() {
    // Base class first
    SourceDlg::onChange();
    // Now my stuff
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newFirstFrequency = c.testString(firstFrequencyEdit_,
                                              source_->frequencies().first().text(),
                                              FdTdLife::notifyMinorChange);
        auto newFrequencyStep = c.testString(frequencyStepEdit_,
                                             source_->frequencies().spacing().text(),
                                             FdTdLife::notifyMinorChange);
        auto newNumFrequencies = c.testString(numFrequenciesEdit_,
                                              source_->frequencies().n().text(),
                                              FdTdLife::notifyMinorChange);
        auto newAmplitude = c.testDouble(amplitudeEdit_, source_->amplitude(),
                                         FdTdLife::notifyMinorChange);
        auto newAzimuth = c.testDouble(azimuthEdit_, source_->azimuth(),
                                       FdTdLife::notifyMinorChange);
        auto newElevation = c.testDouble(elevationEdit_, source_->elevation(),
                                         FdTdLife::notifyMinorChange);
        auto newPolarisation = c.testDouble(polarisationEdit_, source_->polarisation(),
                                            FdTdLife::notifyMinorChange);
        auto newTime = c.testDouble(timeEdit_, source_->time(),
                                    FdTdLife::notifyMinorChange);
        auto newInitialTime = c.testDouble(initialTimeEdit_, source_->initialTime(),
                                           FdTdLife::notifyMinorChange);
        auto newContinuous = c.testBool(continuousCheck_, source_->continuous(),
                                        FdTdLife::notifyMinorChange);
        auto newZoneCenter = c.testVectorDouble(xCenterEdit_, yCenterEdit_, zCenterEdit_,
                                                source_->zoneCenter(),
                                                FdTdLife::notifyMinorChange);
        auto newZoneSize = c.testVectorDouble(xSizeEdit_, ySizeEdit_, zSizeEdit_,
                                              source_->zoneSize(),
                                              FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            source_->frequencies().text(
                    newFirstFrequency, newFrequencyStep, newNumFrequencies);
            source_->amplitude(newAmplitude);
            source_->azimuth(newAzimuth);
            source_->elevation(newElevation);
            source_->polarisation(newPolarisation);
            source_->continuous(newContinuous);
            source_->time(newTime);
            source_->initialTime(newInitialTime);
            source_->zoneCenter(newZoneCenter);
            source_->zoneSize(newZoneSize);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// Handle a notification
void ZoneSourceDlg::notify(fdtd::Notifier* source, int /*why*/,
                           fdtd::NotificationData* /*data*/) {
    if(dlg_->model() == source) {
    }
}

