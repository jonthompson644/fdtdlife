/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnLayerGeneDlg.h"
#include "DialogHelper.h"
#include "IndividualEditDlg.h"
#include "FdTdLife.h"
#include <Gs/AdmittanceFnLayerGene.h>
#include <Gs/FitnessAdmittance.h>
#include <Gs/FitnessFunction.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Gs/GeneticSearch.h>

// Constructor
AdmittanceFnLayerGeneDlg::AdmittanceFnLayerGeneDlg(IndividualEditDlg* mainDlg,
                                                   gs::AdmittanceFnLayerGene* gene) :
        GeneDlg(mainDlg),
        gene_(gene) {
    auto paramLayout = new QVBoxLayout;
    // Make the elements
    std::tie(realAEdit_, realBEdit_, realCEdit_, realDEdit_, realEEdit_, realFEdit_) =
            DialogHelper::polynomialItem(paramLayout, "Real:", gene_->real());
    std::tie(imagAEdit_, imagBEdit_, imagCEdit_, imagDEdit_, realEEdit_, realFEdit_) =
            DialogHelper::polynomialItem(paramLayout, "Imag:", gene_->imag());
    makeAdmittanceFunctionButton_ = DialogHelper::buttonItem(paramLayout,
                                                             "Make Admittance Fitness Function on Clipboard");
    paramLayout->addStretch();
    setLayout(paramLayout);
    connect(makeAdmittanceFunctionButton_, SIGNAL(clicked()), SLOT(onMakeAdmittanceFunction()));
}

// Update the individual from the dialog contents
void AdmittanceFnLayerGeneDlg::updateIndividual() {
    gene_->real(box::Polynomial(realAEdit_->text().toDouble(), realBEdit_->text().toDouble(),
                                 realCEdit_->text().toDouble(), realDEdit_->text().toDouble(),
                                 realEEdit_->text().toDouble(), realFEdit_->text().toDouble()));
    gene_->imag(box::Polynomial(imagAEdit_->text().toDouble(), imagBEdit_->text().toDouble(),
                                 imagCEdit_->text().toDouble(), imagDEdit_->text().toDouble(),
                                 imagEEdit_->text().toDouble(), imagFEdit_->text().toDouble()));
}

// Make an admittance target function using the admittance of this layer
void AdmittanceFnLayerGeneDlg::onMakeAdmittanceFunction() {
    // Make a temporary fitness function
    gs::FitnessAdmittance* fitnessFunction = gene_->makeAdmittanceFunction(mainDlg_->search());
    // Encode the fitness function into XML
    auto* o = new xml::DomObject("fitnessfunction");
    fitnessFunction->writeConfig(*o);
    xml::DomDocument dom(o);
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom, text);
    // Copy the data to the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    QByteArray data(text.c_str());
    mimeData->setData("text/plain", data);
    mimeData->setData("fdtdlife/fitnessfunction", data);
    clipboard->setMimeData(mimeData);
    // Clean up the temporary fitness function
    mainDlg_->search()->fitness()->removeFunction(fitnessFunction);
}
