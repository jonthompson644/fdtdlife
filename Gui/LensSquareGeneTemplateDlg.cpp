/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LensSquareGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
LensSquareGeneTemplateDlg::LensSquareGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                                   gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    auto* realLayout = new QGridLayout;
    layout_->addLayout(realLayout);
    realLayout->addWidget(new QLabel("Bits"), 0, 1);
    realLayout->addWidget(new QLabel("Min"), 0, 2);
    realLayout->addWidget(new QLabel("Max"), 0, 3);
    std::tie(aBitsEdit_, aMinEdit_, aMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "A:", 1);
    std::tie(bBitsEdit_, bMinEdit_, bMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "B:", 2);
    std::tie(cBitsEdit_, cMinEdit_, cMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "C:", 3);
    std::tie(dBitsEdit_, dMinEdit_, dMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "D:", 4);
    std::tie(eBitsEdit_, eMinEdit_, eMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "E:", 5);
    std::tie(fBitsEdit_, fMinEdit_, fMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(realLayout, "F:", 6);
    connect(aBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(aMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(aMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(bBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(bMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(bMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(dMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(eBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(eMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(eMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void LensSquareGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    aBitsEdit_->setText(QString::number(geneTemplate_->realA().bits()));
    aMinEdit_->setText(QString::number(geneTemplate_->realA().min()));
    aMaxEdit_->setText(QString::number(geneTemplate_->realA().max()));
    bBitsEdit_->setText(QString::number(geneTemplate_->realB().bits()));
    bMinEdit_->setText(QString::number(geneTemplate_->realB().min()));
    bMaxEdit_->setText(QString::number(geneTemplate_->realB().max()));
    cBitsEdit_->setText(QString::number(geneTemplate_->realC().bits()));
    cMinEdit_->setText(QString::number(geneTemplate_->realC().min()));
    cMaxEdit_->setText(QString::number(geneTemplate_->realC().max()));
    dBitsEdit_->setText(QString::number(geneTemplate_->realD().bits()));
    dMinEdit_->setText(QString::number(geneTemplate_->realD().min()));
    dMaxEdit_->setText(QString::number(geneTemplate_->realD().max()));
    eBitsEdit_->setText(QString::number(geneTemplate_->realE().bits()));
    eMinEdit_->setText(QString::number(geneTemplate_->realE().min()));
    eMaxEdit_->setText(QString::number(geneTemplate_->realE().max()));
    fBitsEdit_->setText(QString::number(geneTemplate_->realF().bits()));
    fMinEdit_->setText(QString::number(geneTemplate_->realF().min()));
    fMaxEdit_->setText(QString::number(geneTemplate_->realF().max()));
}

// An edit box has changed
void LensSquareGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newA = c.testGeneBitsSpecDouble(aBitsEdit_, aMinEdit_, aMaxEdit_,
                                                 geneTemplate_->realA(),
                                                 FdTdLife::notifySequencerChange);
        auto newB = c.testGeneBitsSpecLogDouble(bBitsEdit_, bMinEdit_, bMaxEdit_,
                                                    geneTemplate_->realB(),
                                                    FdTdLife::notifySequencerChange);
        auto newC = c.testGeneBitsSpecLogDouble(cBitsEdit_, cMinEdit_, cMaxEdit_,
                                                    geneTemplate_->realC(),
                                                    FdTdLife::notifySequencerChange);
        auto newD = c.testGeneBitsSpecLogDouble(dBitsEdit_, dMinEdit_, dMaxEdit_,
                                                    geneTemplate_->realD(),
                                                    FdTdLife::notifySequencerChange);
        auto newE = c.testGeneBitsSpecLogDouble(eBitsEdit_, eMinEdit_, eMaxEdit_,
                                                    geneTemplate_->realE(),
                                                    FdTdLife::notifySequencerChange);
        auto newF = c.testGeneBitsSpecLogDouble(fBitsEdit_, fMinEdit_, fMaxEdit_,
                                                    geneTemplate_->realF(),
                                                    FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->realA(newA);
            geneTemplate_->realB(newB);
            geneTemplate_->realC(newC);
            geneTemplate_->realD(newD);
            geneTemplate_->realE(newE);
            geneTemplate_->realF(newF);
            dlg_->model()->doNotification(c.why());
        }
    }
}

