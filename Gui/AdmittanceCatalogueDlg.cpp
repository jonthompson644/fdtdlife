/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceCatalogueDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "BinaryCodedPlateWidget.h"
#include <AdmittanceCatalogue.h>
#include <CatalogueItem.h>
#include <Sensor/ArraySensor.h>
#include <Xml/Writer.h>

// Constructor
AdmittanceCatalogueDlg::AdmittanceCatalogueDlg(FdTdLife* m) :
        MainTabPanel(m) {
    // The parameters
    auto paramLayout = new QVBoxLayout;
    auto catLayout = new QHBoxLayout;
    // The left hand column
    auto leftLayout = new QVBoxLayout;
    catLayout->addLayout(leftLayout);
    catLayout->setStretchFactor(leftLayout, 1);
    // The file name
    fileNameEdit_ = DialogHelper::textItem(leftLayout, "File");
    browseButton_ = DialogHelper::buttonItem(leftLayout, "Browse...");
    // The catalogue tree
    catTree_ = new QTreeWidget;
    catTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    catTree_->setColumnCount(numCatCols);
    catTree_->setHeaderLabels({"Code", "Pattern"});
    leftLayout->addWidget(catTree_);
    // The buttons
    auto buttonLayout = new QVBoxLayout;
    copyButton_ = DialogHelper::buttonItem(buttonLayout, "Copy");
    leftLayout->addLayout(buttonLayout);
    // The catalogue item information goes in this layout
    auto itemLayout = new QVBoxLayout;
    // The group box for the catalogue item
    auto stretchLayout = new QVBoxLayout;
    stretchLayout->addLayout(itemLayout);
    stretchLayout->addStretch();
    itemGroup_ = new QGroupBox;
    itemGroup_->setLayout(stretchLayout);
    catLayout->addWidget(itemGroup_);
    catLayout->setStretchFactor(itemGroup_, 2);
    // The catalogue item information
    auto line1Layout = new QHBoxLayout;
    codingEdit_ = DialogHelper::textItem(line1Layout, "Coding");
    cellSizeEdit_ = DialogHelper::doubleItem(line1Layout, "Cell Size (m)");
    itemLayout->addLayout(line1Layout);
    hexPatternEdit_ = DialogHelper::textItem(itemLayout, "Hexadecimal Coding",
                                             true);
    auto tableLayout = new QHBoxLayout;
    itemLayout->addLayout(tableLayout);
    bitPatternEdit_ = new BinaryCodedPlateWidget({0}, 16, true,
                                                 true, 192);
    tableLayout->addWidget(bitPatternEdit_);
    // The admittance data table
    admittanceTableTree_ = new QTreeWidget;
    admittanceTableTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    admittanceTableTree_->setColumnCount(numAdmitCols);
    admittanceTableTree_->setHeaderLabels({"Freq (GHz)", "Real", "Imaginary"});
    tableLayout->addWidget(admittanceTableTree_);
    // The plots
    auto* plotLayout = new QHBoxLayout;
    itemLayout->addLayout(plotLayout);
    auto* transmittanceLayout = new QVBoxLayout;
    transmittanceView_ = new DataSeriesWidget(m_->guiConfiguration());
    transmittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0,
                                    1.0, "");
    transmittanceLayout->addWidget(new QLabel("Transmittance"));
    transmittanceLayout->addWidget(transmittanceView_);
    plotLayout->addLayout(transmittanceLayout);
    auto* phaseShiftLayout = new QVBoxLayout;
    phaseShiftView_ = new DataSeriesWidget(m_->guiConfiguration());
    phaseShiftView_->setAxisInfo(0.0, 100.0, "GHz", -180.0,
                                 180.0, "deg");
    phaseShiftLayout->addWidget(new QLabel("Phase Shift"));
    phaseShiftLayout->addWidget(phaseShiftView_);
    plotLayout->addLayout(phaseShiftLayout);
    // The catalogue assembly
    itemLayout->addStretch();
    paramLayout->addLayout(catLayout);
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(catTree_, SIGNAL(itemSelectionChanged()), SLOT(onItemSelected()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(browseButton_, SIGNAL(clicked()), SLOT(onBrowse()));
    // Fill the dialog
    initialise();
}

// Fill the catalogue tree
void AdmittanceCatalogueDlg::fillCatTree(const std::shared_ptr<fdtd::CatalogueItem>& sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Fill the tree
        catTree_->clear();
        for(auto& catItem : m_->admittanceCat().items()) {
            auto item = new QTreeWidgetItem(catTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(catColCoding, QString::number(catItem->code()));
            item->setText(catColPattern, QString::number(catItem->pattern(), 16));
            if(selItem == nullptr || catItem == sel) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem != nullptr) {
        catTree_->setCurrentItem(selItem);
    }
}

// Initialise the dialog
void AdmittanceCatalogueDlg::initialise() {
    if(!initialising_) {
        // The file name
        fileNameEdit_->setText(m_->admittanceCat().fileName().c_str());
        // Fill the catalogue tree
        fillCatTree();
    }
}

// The file name browse button has been clicked
void AdmittanceCatalogueDlg::onBrowse() {
    std::string fileName = QFileDialog::getOpenFileName(
            this, "Select Admittance Catalogue File",
            m_->admittanceCat().catalogueFilePath().c_str(),
            "FDTD Catalogue Files (*.catalogue)").toStdString();
    if(!fileName.empty()) {
        std::string originalFileName = m_->admittanceCat().fileName();
        QFileInfo catFileInfo(fileName.c_str());
        std::string catPathName = catFileInfo.canonicalPath().toStdString() + "/";
        if(catPathName == m_->pathName()) {
            // Catalogue in same directory as config file
            m_->admittanceCat().fileName(catFileInfo.completeBaseName().toStdString(),
                                         true);
        } else {
            // Catalogue in a different directory
            fileName = catPathName + catFileInfo.completeBaseName().toStdString();
            m_->admittanceCat().fileName(fileName, false);
        }
        if(m_->admittanceCat().fileName() != originalFileName) {
            m_->admittanceCat().readCatalogue();
            initialise();
        }
    }
}

// Get the current shape.
std::shared_ptr<fdtd::CatalogueItem> AdmittanceCatalogueDlg::getCurrentItem() {
    std::shared_ptr<fdtd::CatalogueItem> result;
    auto item = catTree_->currentItem();
    if(item != nullptr) {
        uint64_t coding = item->text(catColCoding).toULongLong();
        result = m_->admittanceCat().get(coding);
    }
    return result;
}

// A catalogue item has been selected
void AdmittanceCatalogueDlg::onItemSelected() {
    if(!initialising_) {
        // Clear the item info
        clearItemInfo();
        // Get the item
        std::shared_ptr<fdtd::CatalogueItem> item = getCurrentItem();
        fdtd::AdmittanceCatalogue* cat = &m_->admittanceCat();
        // Fill dialog from the item
        if(item != nullptr) {
            InitialisingGui::Set s(initialising_);
            codingEdit_->setText(QString::number(item->code()));
            cellSizeEdit_->setText(QString::number(cat->unitCell()));
            hexPatternEdit_->setText(QString::number(item->pattern(), 16));
            bitPatternEdit_->setData({item->pattern()}, cat->bitsPerHalfSide() * 2);
            admittanceTableTree_->clear();
            auto admittance = item->admittance(cat->unitCell(), cat->startFrequency(),
                                               cat->frequencyStep()).admittance();
            for(auto& pos : admittance) {
                auto row = new QTreeWidgetItem(admittanceTableTree_);
                row->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                row->setText(admitColFrequency,
                             QString::number(pos.first / box::Constants::giga_));
                row->setText(admitColReal, QString::number(pos.second.real()));
                row->setText(admitColImaginary, QString::number(pos.second.imag()));
            }
            transmittanceView_->setData(item->transmittance(),
                                        GuiConfiguration::elementDataA, true);
            transmittanceView_->setAxisInfo(cat->startFrequency() / box::Constants::giga_,
                                            cat->frequencyStep() / box::Constants::giga_,
                                            "GHz", 0.0, 1.0, "");
            phaseShiftView_->setData(item->phaseShift(),
                                     GuiConfiguration::elementDataA, true);
            phaseShiftView_->setAxisInfo(cat->startFrequency() / box::Constants::giga_,
                                         cat->frequencyStep() / box::Constants::giga_,
                                         "GHz", -180.0, 180.0,
                                         "deg");
        }
    }
}

// Clear the item information items
void AdmittanceCatalogueDlg::clearItemInfo() {
    InitialisingGui::Set s(initialising_);
    codingEdit_->clear();
    cellSizeEdit_->clear();
    hexPatternEdit_->clear();
    bitPatternEdit_->setData({0}, 16);
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void AdmittanceCatalogueDlg::tabChanged() {
}

// Copy the current catalogue item to the clipboard
void AdmittanceCatalogueDlg::onCopy() {
    std::shared_ptr<fdtd::CatalogueItem> item = getCurrentItem();
    if(item) {
        // Copy the catalogue item's coding to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(QString::number(item->code()).toStdString().c_str());
        mimeData->setData("fdtdlife/catalogueitem", data);
        clipboard->setMimeData(mimeData);
    }
}

