/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPlateNxNDlg.h"
#include "DialogHelper.h"
#include <Domain/BinaryPlateNxN.h>
#include "ShapesDlg.h"
#include "AdmittanceTableEditDlg.h"
#include "BinaryCodedPlateWidget.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Sensor/ArraySensor.h>
#include <iostream>
#include <iomanip>
#include <RecordedData.h>

// Constructor
void BinaryPlateNxNDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::BinaryPlateNxN*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Layer Position x,y,z (m)");
    auto line2Layout = new QHBoxLayout;
    cellSizeEdit_ = DialogHelper::textItem(line2Layout, "Cell Size (m)");
    thicknessEdit_ = DialogHelper::doubleItem(line2Layout, "Plate Thickness (m)");
    layout_->addLayout(line2Layout);
    auto line3Layout = new QHBoxLayout;
    bitsPerHalfSideEdit_ = DialogHelper::intItem(line3Layout, "Bits Per Half Side");
    fillMaterialList_ = DialogHelper::dropListItem(line3Layout, "Fill Material");
    layout_->addLayout(line3Layout);
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment x,y,z (m)");
    std::tie(duplicateXEdit_, duplicateYEdit_, duplicateZEdit_) =
            DialogHelper::vectorItem(layout_, "Duplicate x,y,z (m)");
    hexCodingEdit_ = DialogHelper::textItem(layout_, "Hexadecimal Coding",
                                            true);
    auto bottomLayout = new QHBoxLayout;
    layout_->addLayout(bottomLayout);
    bitPatternEdit_ = new BinaryCodedPlateWidget(shape_->coding(),
                                                 shape_->bitsPerHalfSide() * 2,
                                                 true, false, 256);
    bottomLayout->addWidget(bitPatternEdit_);
    auto bottomRight = new QVBoxLayout;
    editAdmittanceTable_ = DialogHelper::buttonItem(bottomRight,
                                                    "Edit Admittance Table");
    recordAdmittanceTable_ = DialogHelper::buttonItem(bottomRight,
                                                      "Record Admittance Table");
    fourFoldSymmetryCheck_ = DialogHelper::boolItem(bottomRight, "Four fold");
    fourFoldSymmetryCheck_->setChecked(true);
    bottomRight->addStretch();
    bottomLayout->addLayout(bottomRight);
    // Connections to handlers
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(thicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(bitsPerHalfSideEdit_, SIGNAL(editingFinished()),
            SLOT(onBitsPerSideChange()));
    connect(editAdmittanceTable_, SIGNAL(clicked()), SLOT(onAdmittanceEdit()));
    connect(recordAdmittanceTable_, SIGNAL(clicked()),
            SLOT(onRecordAdmittance()));
    connect(bitPatternEdit_, SIGNAL(patternChanged()), SLOT(onPatternChanged()));
    connect(fillMaterialList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(fourFoldSymmetryCheck_, SIGNAL(stateChanged(int)),
            SLOT(onFourFoldSymmetryChange()));
    this->connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(duplicateZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void BinaryPlateNxNDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    cellSizeEdit_->setText(shape_->cellSizeX().text().c_str());
    thicknessEdit_->setText(shape_->layerThickness().text().c_str());
    bitsPerHalfSideEdit_->setText(QString::number(shape_->bitsPerHalfSide()));
    incrementXEdit_->setText(shape_->increment().x().text().c_str());
    incrementYEdit_->setText(shape_->increment().y().text().c_str());
    incrementZEdit_->setText(shape_->increment().z().text().c_str());
    duplicateXEdit_->setText(shape_->duplicate().x().text().c_str());
    duplicateYEdit_->setText(shape_->duplicate().y().text().c_str());
    duplicateZEdit_->setText(shape_->duplicate().z().text().c_str());
    x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
    y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
    z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
    cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
    incrementXEdit_->setToolTip(QString::number(shape_->increment().x().value()));
    incrementYEdit_->setToolTip(QString::number(shape_->increment().y().value()));
    incrementZEdit_->setToolTip(QString::number(shape_->increment().z().value()));
    duplicateXEdit_->setToolTip(QString::number(shape_->duplicate().x().value()));
    duplicateYEdit_->setToolTip(QString::number(shape_->duplicate().y().value()));
    duplicateZEdit_->setToolTip(QString::number(shape_->duplicate().z().value()));
    setHexCodingEdit();
    bitPatternEdit_->setData(shape_->coding(), shape_->bitsPerHalfSide() * 2);
    dlg_->model()->fillMaterialList(fillMaterialList_, shape_->fillMaterial());
}

// Set the hexadecimal coding edit box
void BinaryPlateNxNDlg::setHexCodingEdit() {
    std::stringstream s;
    s << std::hex << std::setfill('0');
    for(auto& c: shape_->coding()) {
        s << std::setw(16) << c << " ";
    }
    hexCodingEdit_->setText(s.str().c_str());
}

// An edit box has changed
void BinaryPlateNxNDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCellSize = c.testString(cellSizeEdit_,
                                        shape_->cellSizeX().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newThickness = c.testString(thicknessEdit_,
                                         shape_->layerThickness().text(),
                                         FdTdLife::notifyDomainContentsChange);
        int newFillMaterialIndex = dlg_->model()->selected(fillMaterialList_);
        c.externalTest(newFillMaterialIndex != shape_->fillMaterial(),
                       FdTdLife::notifyDomainContentsChange);
        auto newincrementX = c.testString(incrementXEdit_,
                                          shape_->increment().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementY = c.testString(incrementYEdit_,
                                          shape_->increment().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementZ = c.testString(incrementZEdit_,
                                          shape_->increment().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateX = c.testString(duplicateXEdit_,
                                          shape_->duplicate().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateY = c.testString(duplicateYEdit_,
                                          shape_->duplicate().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateZ = c.testString(duplicateZEdit_,
                                          shape_->duplicate().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->cellSizeX(box::Expression(newCellSize));
            shape_->layerThickness(box::Expression(newThickness));
            shape_->fillMaterial(newFillMaterialIndex);
            shape_->increment().x().text(newincrementX);
            shape_->increment().y().text(newincrementY);
            shape_->increment().z().text(newincrementZ);
            shape_->duplicate().x().text(newduplicateX);
            shape_->duplicate().y().text(newduplicateY);
            shape_->duplicate().z().text(newduplicateZ);
            bitPatternEdit_->setData(shape_->coding(),
                                     shape_->bitsPerHalfSide() * 2);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The four fold symmetry check box has changed
void BinaryPlateNxNDlg::onFourFoldSymmetryChange() {
    bitPatternEdit_->fourFold(fourFoldSymmetryCheck_->isChecked());
}

// The bits per side has changed
void BinaryPlateNxNDlg::onBitsPerSideChange() {
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newBitsPerHalfSide = c.testSizeT(bitsPerHalfSideEdit_,
                                              shape_->bitsPerHalfSide(),
                                              FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->bitsPerHalfSide(newBitsPerHalfSide);
            bitPatternEdit_->setData(shape_->coding(),
                                     shape_->bitsPerHalfSide() * 2);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void BinaryPlateNxNDlg::tabChanged() {
    onChange();
}

// The admittance table edit button has been pressed
void BinaryPlateNxNDlg::onAdmittanceEdit() {
    AdmittanceTableEditDlg box(this, shape_->admittanceTable());
    if(box.exec() == QDialog::Accepted) {
        shape_->admittanceTable() = box.table();
    }
}

// The binary pattern has been changed
void BinaryPlateNxNDlg::onPatternChanged() {
    InitialisingGui::Set s(dlg_->initialising());
    shape_->coding(bitPatternEdit_->data());
    setHexCodingEdit();
}

// Record the admittance from the first array sensorId in
// this layer's admittance table.
void BinaryPlateNxNDlg::onRecordAdmittance() {
    // Find the sensorId
    for(auto& s : dlg_->model()->sensors()) {
        auto* sensor = dynamic_cast<sensor::ArraySensor*>(s.get());
        if(sensor != nullptr) {
            // Does the sensorId contain any data?
            fdtd::RecordedData data;
            sensor->copyOutputData(0, 0, data);
            if(!data.empty()) {
                double unitCell = shape_->cellSizeX().value();
                double startFrequency = sensor->frequencies().first();
                double frequencySpacing = sensor->frequencies().spacing();
                fdtd::AdmittanceData admittance = data.admittance(unitCell, startFrequency,
                                                                  frequencySpacing);
                shape_->admittanceTable() = admittance;
            }
            // Only record the first sensorId
            break;
        }
    }
}
