/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessAdmittanceDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessAdmittance.h>
#include <Gs/GeneticSearch.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GeneticSearchSeqDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor
void FitnessAdmittanceDlg::construct(GeneticSearchTargetDlg* dlg,
                                     gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    fitnessFunction_ = dynamic_cast<gs::FitnessAdmittance*>(fitnessFunction);
    auto col1 = new QVBoxLayout;
    layout_->addLayout(col1);
    fitnessFunctionWeightEdit_ = DialogHelper::doubleItem(col1, "Weight");
    std::tie(raEdit_, rbEdit_, rcEdit_, rdEdit_, reEdit_, rfEdit_) =
            DialogHelper::polynomialItem(col1, "Real:");
    std::tie(iaEdit_, ibEdit_, icEdit_, idEdit_, ieEdit_, ifEdit_) =
            DialogHelper::polynomialItem(col1, "Imag:");
    auto* repeatCellLayout = new QHBoxLayout;
    col1->addLayout(repeatCellLayout);
    repeatCellLayout->addWidget(new QLabel("Repeat Cell Scale:"));
    auto* repeatCellLayoutCol = new QVBoxLayout;
    repeatCellLayout->addLayout(repeatCellLayoutCol);
    minRepeatCellScaleEdit_ = DialogHelper::doubleItem(repeatCellLayoutCol, "Min");
    numRepeatCellScaleSamplesEdit_ = DialogHelper::doubleItem(repeatCellLayoutCol,
                                                              "N samples");
    fitnessWidget_ = new FitnessWidget(this);
    col1->addWidget(fitnessWidget_);
    col1->addStretch();
    connect(fitnessFunctionWeightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(raEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rbEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rcEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rdEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(reEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(rfEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(iaEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ibEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(icEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(idEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ieEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(ifEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(minRepeatCellScaleEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numRepeatCellScaleSamplesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    initialise();
}

// Draw the fitness information on the widget
void FitnessAdmittanceDlg::paintFitnessWidget() {
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    // Black background
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(fitnessFunction_ != nullptr) {
        // Determine the scaling factors
        double min = fitnessFunction_->rangeMin();
        double max = fitnessFunction_->rangeMax();
        double maxFreq = dlg_->dlg()->sequencer()->numFrequencies() *
                         dlg_->dlg()->sequencer()->frequencySpacing();
        double scale = (double) (h - 2) / (max - min);
        double scaleFreq = (double) w / (maxFreq - 0.0);
        double offset = min;
        double offsetFreq = 0.0;
        // Line colors
        QPen realPen(Qt::green);
        QPen imagPen(Qt::red);
        QPen annotationPen(Qt::darkGray);
        // Draw the data
        int lastFreq = 0;
        int lastReal = 0;
        int lastImag = 0;
        for(size_t i = 0; i < dlg_->dlg()->sequencer()->numFrequencies(); i++) {
            double frequency = (static_cast<double>(i) + 1)
                               * dlg_->dlg()->sequencer()->frequencySpacing();
            auto nextFreq = (int) std::floor((frequency - offsetFreq) * scaleFreq);
            int nextReal = h - 1 -
                           (int) std::floor(
                                   (fitnessFunction_->real().value(frequency) - offset) *
                                   scale);
            int nextImag = h - 1 -
                           (int) std::floor(
                                   (fitnessFunction_->imag().value(frequency) - offset) *
                                   scale);
            painter.setPen(realPen);
            painter.drawLine(lastFreq, lastReal, nextFreq, nextReal);
            painter.setPen(imagPen);
            painter.drawLine(lastFreq, lastImag, nextFreq, nextImag);
            lastFreq = nextFreq;
            lastReal = nextReal;
            lastImag = nextImag;
        }
    }
    // Clean up the context
    painter.restore();
}

// Initialise the GUI parameters
void FitnessAdmittanceDlg::initialise() {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::initialise();
    InitialisingGui::Set s(initialising_);
    fitnessFunctionWeightEdit_->setText(QString::number(fitnessFunction_->weight()));
    raEdit_->setText(QString::number(fitnessFunction_->real().a()));
    rbEdit_->setText(QString::number(fitnessFunction_->real().b()));
    rcEdit_->setText(QString::number(fitnessFunction_->real().c()));
    rdEdit_->setText(QString::number(fitnessFunction_->real().d()));
    reEdit_->setText(QString::number(fitnessFunction_->real().e()));
    rfEdit_->setText(QString::number(fitnessFunction_->real().f()));
    iaEdit_->setText(QString::number(fitnessFunction_->imag().a()));
    ibEdit_->setText(QString::number(fitnessFunction_->imag().b()));
    icEdit_->setText(QString::number(fitnessFunction_->imag().c()));
    idEdit_->setText(QString::number(fitnessFunction_->imag().d()));
    ieEdit_->setText(QString::number(fitnessFunction_->imag().e()));
    ifEdit_->setText(QString::number(fitnessFunction_->imag().f()));
    minRepeatCellScaleEdit_->setText(QString::number(fitnessFunction_->minRepeatCellScale()));
    numRepeatCellScaleSamplesEdit_->setText(
            QString::number(fitnessFunction_->numRepeatCellScaleSamples()));
}

// An edit box has changed
void FitnessAdmittanceDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(fitnessFunctionWeightEdit_, fitnessFunction_->weight(),
                                      FdTdLife::notifyMinorChange);
        auto newReal = c.testPolynomial(raEdit_, rbEdit_, rcEdit_, rdEdit_, reEdit_, rfEdit_,
                                        fitnessFunction_->real(),
                                        FdTdLife::notifyMinorChange);
        auto newImag = c.testPolynomial(iaEdit_, ibEdit_, icEdit_, idEdit_, ieEdit_, ifEdit_,
                                        fitnessFunction_->imag(),
                                        FdTdLife::notifyMinorChange);
        auto newMinRepeatCellScale = c.testDouble(minRepeatCellScaleEdit_,
                                                  fitnessFunction_->minRepeatCellScale(),
                                                  FdTdLife::notifyMinorChange);
        auto newNumRepeatCellScaleSamples = c.testInt(
                numRepeatCellScaleSamplesEdit_,
                fitnessFunction_->numRepeatCellScaleSamples(),
                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            fitnessFunction_->set(fitnessFunction_->name(), newWeight, newReal, newImag,
                                  newMinRepeatCellScale, newNumRepeatCellScaleSamples);
            // Tell others
            fitnessWidget_->update();
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessAdmittanceDlg::tabChanged() {
    onChange();
}

