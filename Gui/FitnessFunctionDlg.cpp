/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FitnessFunctionDlg.h"
#include "DialogHelper.h"
#include <Gs/FitnessBase.h>
#include "FdTdLife.h"
#include "GeneticSearchTargetDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor
void
FitnessFunctionDlg::construct(GeneticSearchTargetDlg* dlg, gs::FitnessBase* fitnessFunction) {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::construct(dlg, fitnessFunction);
    auto col1 = new QVBoxLayout;
    layout_->addLayout(col1);
    auto line1Layout = new QHBoxLayout;
    col1->addLayout(line1Layout);
    fitnessFunctionWeightEdit_ = DialogHelper::doubleItem(line1Layout, "Weight");
    dataSourceList_ = nullptr;
    if(!noSource_) {
        dataSourceList_ = DialogHelper::dropListItem(line1Layout, "source",
                                                     {"Mag", "X", "Y"});
    }
    fitnessPointsTree_ = new QTreeWidget;
    fitnessPointsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    fitnessPointsTree_->setColumnCount(numFitnessPointsCols);
    fitnessPointsTree_->setHeaderLabels({"Freq (GHz)", "Minimum", "Maximum"});
    fitnessPointsTree_->setFixedWidth(310);
    col1->addWidget(fitnessPointsTree_);
    auto fitnessPointButtons = new QHBoxLayout;
    newFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons, "New Point");
    deleteFitnessPointButton_ = DialogHelper::buttonItem(fitnessPointButtons,
                                                         "Delete Point");
    fitnessPointButtons->addStretch();
    col1->addLayout(fitnessPointButtons);
    fitnessWidget_ = new FitnessWidget(this);
    col1->addWidget(fitnessWidget_);
    col1->addStretch();
    connect(fitnessFunctionWeightEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(newFitnessPointButton_, SIGNAL(clicked()), SLOT(onNewFitnessPoint()));
    connect(deleteFitnessPointButton_, SIGNAL(clicked()), SLOT(onDeleteFitnessPoint()));
    connect(fitnessPointsTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onFitnessPointChanged(QTreeWidgetItem * , int)));
    if(!noSource_) {
        connect(dataSourceList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    }
    initialise();
    fillFitnessPointsTree();
}

// Draw the fitness information on the widget
void FitnessFunctionDlg::paintFitnessWidget() {
    // Draw onto the widget
    QPainter painter(fitnessWidget_);
    painter.save();
    int h = fitnessWidget_->height();
    int w = fitnessWidget_->width();
    // Black background
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, w, h);
    // Which function?
    if(item_ != nullptr) {
        // Determine the scaling factors
        double min = item_->rangeMin();
        double max = item_->rangeMax();
        double maxFreq = 1 * box::Constants::giga_;
        for(const auto& point : item_->points()) {
            maxFreq = std::max(maxFreq, point.x());
        }
        double scale = (double) (h - 2) / (max - min);
        double scaleFreq = (double) w / (maxFreq - 0.0);
        double offset = min;
        double offsetFreq = 0.0;
        // Line colors
        QPen functionPen(Qt::green);
        QPen annotationPen(Qt::darkGray);
        // Draw the data
        int lastFreq = 0;
        int lastMin = 0;
        int lastMax = 0;
        for(const auto& point : item_->points()) {
            auto nextFreq = (int) std::floor((point.x() - offsetFreq) * scaleFreq);
            int nextMin = h - 1 -
                          (int) std::floor((point.min() - offset) * scale);
            int nextMax = h - 1 -
                          (int) std::floor((point.max() - offset) * scale);
            painter.setPen(functionPen);
            painter.drawLine(lastFreq, lastMin, nextFreq, nextMin);
            painter.drawLine(lastFreq, lastMax, nextFreq, nextMax);
            lastFreq = nextFreq;
            lastMin = nextMin;
            lastMax = nextMax;
        }
    }
    // Clean up the context
    painter.restore();
}

// The new fitness point button has been pressed
void FitnessFunctionDlg::onNewFitnessPoint() {
    // Current function
    if(item_ != nullptr) {
        // Add an empty point
        item_->addPoint(0.0, 0.0, 0.0);
        fillFitnessPointsTree();
    }
}

// The delete fitness point button has been pressed
void FitnessFunctionDlg::onDeleteFitnessPoint() {
    // Current function
    if(item_ != nullptr) {
        // Current point
        QTreeWidgetItem* item = fitnessPointsTree_->currentItem();
        if(item != nullptr) {
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            if(row >= 0) {
                item_->deletePoint(row);
                fillFitnessPointsTree();
            }
        }
    }
}

// Put all the fitness points in the fitness tree widget
void FitnessFunctionDlg::fillFitnessPointsTree() {
    InitialisingGui::Set s(initialising_);
    fitnessPointsTree_->clear();
    // Current function
    if(item_ != nullptr) {
        for(auto& point : item_->points()) {
            auto item = new QTreeWidgetItem(fitnessPointsTree_);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(fitnessPointsColFreq, QString::number(point.x()
                                                                / box::Constants::giga_));
            item->setText(fitnessPointsColMin, QString::number(point.min()));
            item->setText(fitnessPointsColMax, QString::number(point.max()));
        }
    }
    fitnessWidget_->update();
}

// A fitness tree item has been changed
void FitnessFunctionDlg::onFitnessPointChanged(QTreeWidgetItem* item, int /*column*/) {
    if(!initialising_) {
        // Current function
        if(item_ != nullptr) {
            // Which row
            int row = fitnessPointsTree_->indexOfTopLevelItem(item);
            // Set the function point in this row
            double freq = item->text(fitnessPointsColFreq).toDouble()
                          * box::Constants::giga_;
            double min = item->text(fitnessPointsColMin).toDouble();
            double max = item->text(fitnessPointsColMax).toDouble();
            item_->setPoint(row, freq, min, max);
            fillFitnessPointsTree();
        }
    }
}


// Initialise the GUI parameters
void FitnessFunctionDlg::initialise() {
    TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase>::initialise();
    InitialisingGui::Set s(initialising_);
    fitnessFunctionWeightEdit_->setText(QString::number(item_->weight()));
    if(!noSource_) {
        dataSourceList_->setCurrentIndex((int) item_->dataSource());
    }
}

// An edit box has changed
void FitnessFunctionDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newWeight = c.testDouble(fitnessFunctionWeightEdit_, item_->weight(),
                                      FdTdLife::notifyMinorChange);
        gs::FitnessBase::DataSource newSource = item_->dataSource();
        if(!noSource_) {
            newSource = (gs::FitnessBase::DataSource) c.testInt(
                    dataSourceList_, (int) item_->dataSource(),
                    FdTdLife::notifyMinorChange);
        }
        // Make the changes
        if(c.isChanged()) {
            item_->set(item_->name(), newWeight, newSource);
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void FitnessFunctionDlg::tabChanged() {
    onChange();
}

