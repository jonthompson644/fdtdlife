/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryCodedPlateWidget.h"
#include <utility>
#include <Box/Utility.h>

// Constructor
BinaryCodedPlateWidget::BinaryCodedPlateWidget(std::vector<uint64_t> data, size_t size,
                                               bool fourFold, bool readOnly,
                                               size_t widgetSize) :
        data_(std::move(data)),
        size_(size),
        fourFold_(fourFold),
        pixelsPerBit_(widgetSize / size_),
        readOnly_(readOnly),
        widgetSize_(widgetSize) {
    data_.resize(numDataWords(), 0);
}

// Return a hint regarding the size of the widget
QSize BinaryCodedPlateWidget::sizeHint() const {
    return {static_cast<int>(widgetSize_), static_cast<int>(widgetSize_)};
}

// Paint the data
void BinaryCodedPlateWidget::paintEvent(QPaintEvent* /*event*/) {
    // Draw onto the screen
    QPainter painter(this);
    painter.save();
    painter.setPen(Qt::gray);
    size_t x1 = 0;
    size_t y1 = 0;
    size_t x2 = (size_ - 1) * pixelsPerBit_;
    size_t y2 = (size_ - 1) * pixelsPerBit_;
    QBrush whiteBrush(Qt::white);
    QBrush blackBrush(Qt::black);
    for(size_t m = 0; m < size_ / 2; m++) {
        for(size_t n = 0; n < size_ / 2; n++) {
            size_t bitNum = m * (size_ / 2) + n;
            size_t wordNum = bitNum / bitsPerCodingWord_;
            size_t bitInWord = bitNum % bitsPerCodingWord_;
            if((data_[wordNum] & (1ULL << bitInWord)) != 0) {
                painter.setBrush(blackBrush);
            } else {
                painter.setBrush(whiteBrush);
            }
            painter.drawRect(static_cast<int>(x1), static_cast<int>(y1),
                    static_cast<int>(pixelsPerBit_), static_cast<int>(pixelsPerBit_));
            painter.drawRect(static_cast<int>(x2), static_cast<int>(y2),
                    static_cast<int>(pixelsPerBit_), static_cast<int>(pixelsPerBit_));
            painter.drawRect(static_cast<int>(x2), static_cast<int>(y1),
                    static_cast<int>(pixelsPerBit_), static_cast<int>(pixelsPerBit_));
            painter.drawRect(static_cast<int>(x1), static_cast<int>(y2),
                    static_cast<int>(pixelsPerBit_), static_cast<int>(pixelsPerBit_));
            x1 += pixelsPerBit_;
            x2 -= pixelsPerBit_;
        }
        y1 += pixelsPerBit_;
        y2 -= pixelsPerBit_;
        x1 = 0;
        x2 = (size_ - 1) * pixelsPerBit_;
    }
    painter.restore();
}

// Handle a mouse click
void BinaryCodedPlateWidget::mousePressEvent(QMouseEvent* event) {
    QWidget::mousePressEvent(event);
    if(event->button() == Qt::LeftButton && !readOnly_) {
        // Work out the four fold symmetry coordinates
        int y = event->pos().y() / static_cast<int>(pixelsPerBit_);
        int x = event->pos().x() / static_cast<int>(pixelsPerBit_);
        if(y >= 0 && y < static_cast<int>(size_) &&
           x >= 0 && x < static_cast<int>(size_)) {
            if(y >= static_cast<int>(size_) / 2) {
                y = static_cast<int>(size_) - y - 1;
            }
            if(x >= static_cast<int>(size_) / 2) {
                x = static_cast<int>(size_) - x - 1;
            }
            // One half of the eight fold symmetry
            size_t bitNumber = static_cast<size_t>(y) * size_ / 2 + static_cast<size_t>(x);
            size_t wordNumber = bitNumber / bitsPerCodingWord_;
            size_t bitInWord = bitNumber % bitsPerCodingWord_;
            uint64_t mask = 1ULL << bitInWord;
            data_[wordNumber] = data_[wordNumber] ^ mask;
            // The other half of the eight fold symmetry
            if(x != y && fourFold_) {
                bitNumber = static_cast<size_t>(x) * size_ / 2 + static_cast<size_t>(y);
                wordNumber = bitNumber / bitsPerCodingWord_;
                bitInWord = bitNumber % bitsPerCodingWord_;
                mask = 1ULL << bitInWord;
                data_[wordNumber] = data_[wordNumber] ^ mask;
            }
            update();
            emit patternChanged();
        }
    }
}

// Set the binary data
void BinaryCodedPlateWidget::setData(const std::vector<uint64_t>& value, size_t size) {
    if(size > 0) {
        size_ = size;
        pixelsPerBit_ = widgetSize_ / size_;
        data_ = value;
        data_.resize(numDataWords(), 0);
        update();
    }
}

// Clear the data
void BinaryCodedPlateWidget::clear() {
    data_.clear();
    data_.resize(numDataWords(), 0);
    update();
}

// Returns the coding value for the pattern
uint64_t BinaryCodedPlateWidget::coding() const {
    // Convert the data into a coding value.  This function
    // only works for up to 16x16 patterns.
    uint64_t result = 0;
    if(size_ <= 16) {
        if(fourFold_) {
            // In four fold symmetry we have to convert
            size_t fourFoldBitLocation = 0;
            for(size_t i = 0; i < size_/2; i++) {
                for(size_t j = i; j < size_/2; j++) {
                    size_t twoFoldBitLocation = j * size_/2 + i;
                    size_t wordNum = twoFoldBitLocation / bitsPerCodingWord_;
                    size_t bitNum = twoFoldBitLocation % bitsPerCodingWord_;
                    if(((data_[wordNum] >> bitNum) & 1ULL) != 0ULL) {
                        result |= 1ULL << fourFoldBitLocation;
                    }
                    fourFoldBitLocation++;
                }
            }
        } else {
            // In two fold symmetry, the code is the same as the data
            result = data_[0];
        }
    }
    return result;
}

// The context menu is required
void BinaryCodedPlateWidget::contextMenuEvent(QContextMenuEvent* event) {
    QMenu menu;
    QAction* copyToClipboard = menu.addAction("Copy to clipboard");
    QAction* pasteFromClipboard = menu.addAction("Paste from clipboard");
    QAction* selectedAction = menu.exec(event->globalPos());
    if(selectedAction == copyToClipboard) {
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        mimeData->setText(box::Utility::vectorToString(data_).c_str());
        QRect rect(0,0,size_*pixelsPerBit_,size_*pixelsPerBit_);
        mimeData->setImageData(grab(rect).toImage());
        clipboard->setMimeData(mimeData);
    } else if(selectedAction == pasteFromClipboard) {
        const QMimeData* mimeData = QApplication::clipboard()->mimeData();
        data_ = box::Utility::stringToVector<uint64_t>(mimeData->text().toStdString());
        update();
        emit patternChanged();
    }
}
