/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_MATERIALSDLG_H
#define FDTDLIFE_MATERIALSDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "FdTdLife.h"
#include "PushButtonMenu.h"
#include <DialogFactory.h>
#include <Fdtd/Notifiable.h>

class FdTdLife;
class MaterialDlg;
namespace domain {class Material;}

// Dialog panel for configuring the materials
class MaterialsDlg: public MainTabPanel, public fdtd::Notifiable {
    Q_OBJECT
public:
    explicit MaterialsDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;
    void updateCurrentSelection();
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onNew(int addKind);
    void onDelete();
    void onMaterialSelected();
    void onMaterialChanged(QTreeWidgetItem* item, int column);

protected:
    void fillMaterialTree(domain::Material* sel = nullptr);
    static void setMaterialItem(domain::Material* mat, QTreeWidgetItem* item);
    domain::Material* currentMaterial();

protected:
    enum {materialColIdentifier=0, materialColName, materialColPermittivity, numMaterialCols};
    enum {addNormal=0, addArc};
    QTreeWidget* materialsTree_;
    PushButtonMenu* addButton_;
    QPushButton* deleteButton_;
    QHBoxLayout* materialDlgLayout_;
    MaterialDlg* materialDlg_;
    DialogFactory<MaterialDlg> dlgFactory_;

};


#endif //FDTDLIFE_MATERIALSDLG_H
