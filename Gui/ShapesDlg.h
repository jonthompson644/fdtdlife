/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SHAPESDLG_H
#define FDTDLIFE_SHAPESDLG_H

#include "QtIncludes.h"
#include <Box/Vector.h>
#include "FdTdLife.h"
#include "MainTabPanel.h"
#include "PushButtonMenu.h"
#include "DialogFactory.h"
#include <list>
#include <complex>
#include <Notifiable.h>

class ShapeDlg;
namespace domain { class Shape; }

class ShapesDlg : public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit ShapesDlg(FdTdLife* m);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    void updateCurrentSelection();

protected slots:
    void onShapeSelected();
    void onNewShape(int item);
    void onDelete();
    void onWriteDxf();
    void onWriteAllDxf();
    void onWriteAllStl();
    void onWriteAllHfss();
    void onConvertToBinaryNxN();
    void onCopy();
    void onPaste();
    void onFromCatalogue();

protected:
    domain::Shape* getCurrentShape();
    void fillShapeTree(domain::Shape* sel = nullptr);

protected:
    enum {
        shapeColIdentifier = 0, shapeColName, shapeColType, numShapeCols
    };
    QTreeWidget* shapesTree_ {};
    QPushButton* deleteButton_{};
    QPushButton* writeDxfButton_{};
    QPushButton* writeAllDxfButton_{};
    QPushButton* writeAllStlButton_{};
    QPushButton* writeAllHfssButton_{};
    QPushButton* convertToBinaryNxNButton_{};
    QPushButton* copyButton_{};
    QPushButton* pasteButton_{};
    QHBoxLayout* shapeDlgLayout_{};
    ShapeDlg* shapeDlg_{};
    PushButtonMenu* newShape_{};
    QPushButton* fromCatalogue_{};
    DialogFactory<ShapeDlg> dlgFactory_;
};


#endif //FDTDLIFE_SHAPESDLG_H
