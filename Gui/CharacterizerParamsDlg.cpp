/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CharacterizerParamsDlg.h"
#include "CharacterizerDlg.h"
#include "SequencersDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include "ScannerDlg.h"
#include <Seq/Characterizer.h>
#include <Seq/Scanner.h>
#include <Seq/IdenticalLayersScanner.h>
#include <Seq/LayerSpacingScanner.h>
#include <Seq/BinaryNxNPlateScanner.h>
#include <Seq/SquarePlateScanner.h>
#include <Seq/DielectricLayerScanner.h>
#include <Seq/VariableScanner.h>
#include "IdenticalLayersScannerDlg.h"
#include "LayerSpacingScannerDlg.h"
#include "BinaryNxNPlateScannerDlg.h"
#include "SquarePlateScannerDlg.h"
#include "DielectricLayerScannerDlg.h"
#include "VariableScannerDlg.h"

// Constructor
CharacterizerParamsDlg::CharacterizerParamsDlg(CharacterizerDlg* dlg) :
        CharacterizerSubDlg(dlg),
        Notifiable(dlg->dlg()->model()),
        scannerDlg_(nullptr) {
    auto mainLayout = new QHBoxLayout;
    // The parameters
    auto paramLayout = new QVBoxLayout;
    mainLayout->addLayout(paramLayout);
    scannersTree_ = new QTreeWidget;
    scannersTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    scannersTree_->setColumnCount(numColumns);
    scannersTree_->setHeaderLabels({"Id", "Scanner Type", "Steps"});
    scannersTree_->header()->resizeSection(columnId, 30);
    scannersTree_->setDragEnabled(true);
    scannersTree_->viewport()->setAcceptDrops(true);
    scannersTree_->setDropIndicatorShown(true);
    scannersTree_->setDragDropMode(QAbstractItemView::InternalMove);
    scannersTree_->setIndentation(0);
    paramLayout->addWidget(scannersTree_);
    newButton_ = DialogHelper::buttonMenuItem(paramLayout, "New Scanner",
                                              {"Identical Layers", "Layer Spacing",
                                               "Binary NxN Plate", "Square Plate/Hole",
                                               "Dielectric Layer", "Variable"});
    deleteButton_ = DialogHelper::buttonItem(paramLayout, "Delete");
    paramLayout->addStretch();
    // The place holder for the scanner dialog
    scannerDlgLayout_ = new QVBoxLayout;
    mainLayout->addLayout(scannerDlgLayout_);
    mainLayout->addStretch();
    // Set the layout
    setTabLayout(mainLayout);
    // Connect the handlers
    connect(scannersTree_, SIGNAL(itemSelectionChanged()), SLOT(onScannerSelected()));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(newButton_, SIGNAL(clickedMenu(int)), SLOT(onNew(int)));
    connect(scannersTree_->model(),
            SIGNAL(rowsInserted(
                           const QModelIndex &, int, int)),
            SLOT(onScannerInserted(
                         const QModelIndex &, int, int)));
    // Fill the panel
    initialise();
}

// A new scanner has been clicked
void CharacterizerParamsDlg::onNew(int item) {
    tabChanged();  // Update anything that may have changed for the current item
    seq::Scanner* scanner = nullptr;
    switch(item) {
        case scannerIdenticalLayers:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::identicalLayers).get();
            break;
        case scannerLayerSpacing:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::layerSpacing).get();
            break;
        case scannerBinaryNxNPlate:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::binaryNxNPlate).get();
            break;
        case scannerSquarePlate:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::squarePlate).get();
            break;
        case scannerDielectricLayer:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::dielectricLayer).get();
            break;
        case scannerVariable:
            scanner = dlg_->sequencer()->makeScanner(
                    seq::Characterizer::ScannerMode::variable).get();
            break;
        default:
            break;
    }
    if(scanner != nullptr) {
        fillScannerTree(scanner);
        InitialisingGui::Set s(initialising_);
        dlg_->dlg()->model()->doNotification(FdTdLife::notifySequencerChange);
    }
}

// Fill the scanner tree
void CharacterizerParamsDlg::fillScannerTree(seq::Scanner* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Delete any old dialog
        delete scannerDlg_;
        scannerDlg_ = nullptr;
        // Fill the tree
        scannersTree_->clear();
        for(auto& scanner : dlg_->sequencer()->scanners()) {
            auto item = new QTreeWidgetItem(scannersTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
            item->setText(columnId, QString::number(scanner->id()));
            item->setText(columnType,
                          dlg_->sequencer()->scannerModeName(scanner->mode()).c_str());
            item->setText(columnNumSteps, QString::number(scanner->steps()));
            if(selItem == nullptr || scanner.get() == sel) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem != nullptr) {
        scannersTree_->setCurrentItem(selItem);
    }
}

// Initialise the panel from the configuration
void CharacterizerParamsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fillScannerTree();
    initialiseScanner();
}

// An element has changed
void CharacterizerParamsDlg::onChange() {
    if(!initialising_) {
        // New values
        GuiChangeDetector c;
        // Make the changes
        if(c.isChanged()) {
            // Tell others
            dlg_->dlg()->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CharacterizerParamsDlg::tabChanged() {
    onChange();
}

// Handle a configuration change notification
void CharacterizerParamsDlg::notify(fdtd::Notifier* source, int why,
                                    fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        if(why == FdTdLife::notifyDomainContentsChange) {
            InitialisingGui::Set s(initialising_);
        }
    }
}

// A scanner has been selected
void CharacterizerParamsDlg::onScannerSelected() {
    initialiseScanner();
}

// Return the currently selected scanner
seq::Scanner* CharacterizerParamsDlg::currentScanner() {
    QTreeWidgetItem* treeItem = scannersTree_->currentItem();
    seq::Scanner* item = nullptr;
    if(treeItem != nullptr) {
        int id = treeItem->text(columnId).toInt();
        item = dlg_->sequencer()->getScanner(id);
    }
    return item;
}

// Delete the currently selected scanner
void CharacterizerParamsDlg::onDelete() {
    seq::Scanner* scanner = currentScanner();
    if(scanner != nullptr) {
        std::stringstream msg;
        msg << "OK to delete the scanner " << scanner->id() << " ?";
        QMessageBox box;
        box.setText(msg.str().c_str());
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            dlg_->sequencer()->remove(scanner);
            fillScannerTree();
            InitialisingGui::Set s(initialising_);
            dlg_->dlg()->model()->doNotification(FdTdLife::notifySequencerChange);
        }
    }
}

// Initialise the dialog with the current scanner
void CharacterizerParamsDlg::initialiseScanner() {
    // Clean up old dialog
    delete scannerDlg_;
    scannerDlg_ = nullptr;
    // Current function
    seq::Scanner* item = currentScanner();
    if(item != nullptr) {
        // Show the correct dialog
        switch(item->mode()) {
            case seq::Characterizer::ScannerMode::identicalLayers: {
                auto* scanner = dynamic_cast<seq::IdenticalLayersScanner*>(item);
                scannerDlg_ = new IdenticalLayersScannerDlg(this, scanner);
                break;
            }
            case seq::Characterizer::ScannerMode::layerSpacing: {
                auto* scanner = dynamic_cast<seq::LayerSpacingScanner*>(item);
                scannerDlg_ = new LayerSpacingScannerDlg(this, scanner);
                break;
            }
            case seq::Characterizer::ScannerMode::binaryNxNPlate: {
                auto* scanner = dynamic_cast<seq::BinaryNxNPlateScanner*>(item);
                scannerDlg_ = new BinaryNxNPlateScannerDlg(this, scanner);
                break;
            }
            case seq::Characterizer::ScannerMode::squarePlate: {
                auto* scanner = dynamic_cast<seq::SquarePlateScanner*>(item);
                scannerDlg_ = new SquarePlateScannerDlg(this, scanner);
                break;
            }
            case seq::Characterizer::ScannerMode::dielectricLayer: {
                auto* scanner = dynamic_cast<seq::DielectricLayerScanner*>(item);
                scannerDlg_ = new DielectricLayerScannerDlg(this, scanner);
                break;
            }
            case seq::Characterizer::ScannerMode::variable: {
                auto* scanner = dynamic_cast<seq::VariableScanner*>(item);
                scannerDlg_ = new VariableScannerDlg(this, scanner);
                break;
            }
            default:
                break;
        }
        if(scannerDlg_ != nullptr) {
            scannerDlgLayout_->addWidget(scannerDlg_);
        }
    }
}

// A scanner has been inserted
void CharacterizerParamsDlg::onScannerInserted(const QModelIndex&/*parent*/, int /*start*/,
                                               int /*end*/) {
    if(!initialising_) {
        // Due to the careful use of the initialising object, we should only
        // get here when the drag/drop internal move operation has happened.
        // Use the tree order to re-order the scanners
        std::map<int, int> order;  // The desired order <identifier, orderNumber>
        for(int i = 0; i < scannersTree_->topLevelItemCount(); i++) {
            auto item = scannersTree_->topLevelItem(i);
            int identifier = item->text(columnId).toInt();
            order[identifier] = i;
        }
        dlg_->sequencer()->reorderScanners(order);
    }
}
