/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SequencersDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "GeneticSearchSeqDlg.h"
#include "CatalogueBuilderDlg.h"
#include "CharacterizerDlg.h"
#include "NoEditDelegate.h"
#include <Seq/Sequencer.h>
#include <Gs/GeneticSearch.h>
#include <Seq/Characterizer.h>
#include <Fdtd/CatalogueBuilder.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>

// Constructor
SequencersDlg::SequencersDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        sequencerDlg_(nullptr) {
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SequencerDlg>::Creator<GeneticSearchSeqDlg>(
            seq::Sequencers::modeGeneticSearch));
    dlgFactory_.define(new box::Factory<SequencerDlg>::Creator<CatalogueBuilderDlg>(
            seq::Sequencers::modeCatalogueBuilder));
    dlgFactory_.define(new box::Factory<SequencerDlg>::Creator<CharacterizerDlg>(
            seq::Sequencers::modeCharacterizer));
    // The parameters
    auto paramLayout = new QHBoxLayout;
    auto sequencersLayout = new QVBoxLayout;
    paramLayout->addLayout(sequencersLayout);
    // The sequencers tree
    sequencersTree_ = new QTreeWidget;
    sequencersTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    sequencersTree_->setColumnCount(numSequencerCols);
    sequencersTree_->setHeaderLabels({"Identifier", "Name", "Type"});
    sequencersTree_->setFixedWidth(320);
    sequencersTree_->setItemDelegateForColumn(sequencerColIdentifier,
                                              new NoEditDelegate(this));
    sequencersTree_->setItemDelegateForColumn(sequencerColType,
                                              new NoEditDelegate(this));
    sequencersLayout->addWidget(sequencersTree_);
    // Add any buttons here underneath the sequencers tree
    newButton_ = DialogHelper::buttonMenuItem(sequencersLayout, "New Sequencer",
                                              m_->sequencers().sequencerFactory().modeNames());
    deleteButton_ = DialogHelper::buttonItem(sequencersLayout, "Delete");
    copyButton_ = DialogHelper::buttonItem(sequencersLayout, "Copy to clipboard");
    pasteButton_ = DialogHelper::buttonItem(sequencersLayout, "Paste from clipboard");
    clearButton_ = DialogHelper::buttonItem(sequencersLayout,
                                            "Clear sequencer and discard data");
    csvFileButton_ = DialogHelper::buttonItem(sequencersLayout,
                                              "Make CSV on clipboard from data");
    sequencersLayout->addStretch();
    // The place holder for the sequencer dialog
    sequencerDlgLayout_ = new QVBoxLayout;
    paramLayout->addLayout(sequencerDlgLayout_);
    paramLayout->addStretch();
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(sequencersTree_, SIGNAL(itemSelectionChanged()), SLOT(onSequencerSel()));
    connect(newButton_, SIGNAL(clickedMenu(int)), SLOT(onNewSequencer(int)));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    connect(clearButton_, SIGNAL(clicked()), SLOT(onClear()));
    connect(csvFileButton_, SIGNAL(clicked()), SLOT(onCsvFile()));
    connect(sequencersTree_, SIGNAL(itemChanged(QTreeWidgetItem * , int)),
            SLOT(onSequencerChanged(QTreeWidgetItem * , int)));
    // Fill the dialog
    initialise();
}

// Fill the sequencer tree
void SequencersDlg::fillSequencerTree(seq::Sequencer* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Insert any sequencers not already in the tree (and find the selection item)
        for(auto& seq : m_->sequencers().sequencers()) {
            QTreeWidgetItem* item = findSequencerInTree(seq.get());
            if(item == nullptr) {
                item = new QTreeWidgetItem(sequencersTree_);
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
                item->setText(sequencerColIdentifier,
                              QString::number(seq->identifier()));
                item->setText(sequencerColName, seq->name().c_str());
                item->setText(sequencerColType,
                              m_->sequencers().sequencerModeName(seq->mode()).c_str());
            }
            if(seq.get() == sel) {
                selItem = item;
            }
        }
        // Remove any sequencers in the tree that are no longer required.
        std::list<QTreeWidgetItem*> removeThese;
        for(int i = 0; i < sequencersTree_->topLevelItemCount(); i++) {
            QTreeWidgetItem* item = sequencersTree_->topLevelItem(i);
            int id = item->text(sequencerColIdentifier).toInt();
            seq::Sequencer* seq = m_->sequencers().find(id);
            if(seq == nullptr) {
                removeThese.push_back(item);
            }
        }
        for(auto& item : removeThese) {
            delete item;
        }
    }
    // Select an item
    if(selItem == nullptr && sequencersTree_->topLevelItemCount() > 0) {
        selItem = sequencersTree_->topLevelItem(0);
    }
    if(selItem != nullptr) {
        sequencersTree_->setCurrentItem(selItem);
        onSequencerSel();
    } else {
        // Delete any old dialog
        delete sequencerDlg_;
        sequencerDlg_ = nullptr;
    }
}

// Initialise the dialog
void SequencersDlg::initialise() {
    if(!initialising_) {
        fillSequencerTree();
    }
}

// Fill the user interface using the selected sequencer
void SequencersDlg::onSequencerSel() {
    if(!initialising_) {
        seq::Sequencer* seq = currentSequencer();
        if(seq == nullptr) {
            // Delete any old dialog
            delete sequencerDlg_;
            sequencerDlg_ = nullptr;
        } else {
            // Does the dialog need changing
            if(sequencerDlg_ == nullptr || sequencerDlg_->sequencer() != seq) {
                // Delete any old dialog
                delete sequencerDlg_;
                sequencerDlg_ = dlgFactory_.make(this, seq);
                // Need to change this to depend on the selected sequencer
                if(sequencerDlg_ != nullptr) {
                    sequencerDlg_->initialise();
                    sequencerDlgLayout_->addWidget(sequencerDlg_);
                }
            } else {
                sequencerDlg_->initialise();
            }
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void SequencersDlg::tabChanged() {
    if(sequencerDlg_ != nullptr) {
        sequencerDlg_->tabChanged();
    }
}

// The user has changed some aspect of the configuration
void SequencersDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    auto* sequencer = dynamic_cast<seq::Sequencer*>(source);
    if(sequencer != nullptr) {
        if(why == seq::Sequencer::notifyNameChange) {
            // Find the sequencer in the tree
            QTreeWidgetItem* item = findSequencerInTree(sequencer);
            // Now update the item
            if(item != nullptr) {
                item->setText(sequencerColName, sequencer->name().c_str());
            }
        }
    } else if(source == m_ && why == FdTdLife::notifySequencerChange) {
        InitialisingGui::Set s(initialising_);
        fillSequencerTree();
    }
}

// Find the sequencer in tree.  Returns the tree item or null.
QTreeWidgetItem* SequencersDlg::findSequencerInTree(seq::Sequencer* sequencer) {
    // Find the sequencer in the tree
    QTreeWidgetItem* result = nullptr;
    for(int i = 0; i < sequencersTree_->topLevelItemCount(); i++) {
        QTreeWidgetItem* item = sequencersTree_->topLevelItem(i);
        if(item->text(sequencerColIdentifier).toInt() == sequencer->identifier()) {
            result = item;
        }
    }
    return result;
}

// A new sequencer has been clicked
void SequencersDlg::onNewSequencer(int item) {
    tabChanged();  // Update anything that may have changed for the current item
    seq::Sequencer* sequencer = m_->sequencers().makeSequencer(item).get();
    if(sequencer != nullptr) {
        std::stringstream name;
        name << "Sequencer" << sequencer->identifier();
        sequencer->name(name.str().c_str());
        fillSequencerTree(sequencer);
        InitialisingGui::Set s(initialising_);
        m_->doNotification(FdTdLife::notifyDomainContentsChange);
    }
}

// Delete a shape
void SequencersDlg::onDelete() {
    QMessageBox box;
    seq::Sequencer* sequencer = currentSequencer();
    if(sequencer != nullptr) {
        QString msg;
        QTextStream t(&msg);
        t << "OK to delete the sequencer " << sequencer->name().c_str();
        box.setText(msg);
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            // Delete any old dialog
            delete sequencerDlg_;
            sequencerDlg_ = nullptr;
            // Delete the sequencer
            m_->sequencers().remove(sequencer);
            fillSequencerTree();
            InitialisingGui::Set s(initialising_);
            m_->doNotification(FdTdLife::notifyDomainContentsChange);
        }
    }
}

// Get the currently selected sequencer
seq::Sequencer* SequencersDlg::currentSequencer() {
    int id = sequencersTree_->currentItem()->text(sequencerColIdentifier).toInt();
    return m_->sequencers().find(id);
}

// Copy the current sequencer to the clipboard
void SequencersDlg::onCopy() {
    seq::Sequencer* seq = currentSequencer();
    if(seq != nullptr) {
        // Encode the shape into XML
        auto* o = new xml::DomObject("sequencer");
        *o << *seq;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(text.c_str());
        mimeData->setData("text/plain", data);
        mimeData->setData("fdtdlife/sequencer", data);
        clipboard->setMimeData(mimeData);
    }
}

// Paste a sequencer from the clipboard
void SequencersDlg::onPaste() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/sequencer")) {
        try {
            // Read the XML into a DOM
            std::string text = mimeData->data("fdtdlife/sequencer").toStdString();
            xml::DomDocument dom("sequencer");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a sequencer from the DOM
            auto seq = m_->sequencers().makeSequencer(*dom.getObject());
            *dom.getObject() >> *seq;
            fillSequencerTree(seq.get());
        } catch(xml::Exception& e) {
            std::cout << "Sequencer paste failed to read XML: " << e.what() << std::endl;
        }
    }
}

// Clear the current sequencer and discard all its data
void SequencersDlg::onClear() {
    QMessageBox box;
    seq::Sequencer* sequencer = currentSequencer();
    if(sequencer != nullptr) {
        QString msg;
        QTextStream t(&msg);
        t << "OK to clear the sequencer " << sequencer->name().c_str() << " and discard all its data?";
        box.setText(msg);
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            sequencer->clearAndDiscard();
            InitialisingGui::Set s(initialising_);
            m_->doNotification(FdTdLife::notifySequencerChange);
        }
    }
}

// Put CSV data on the clipboard from the sequencer data
void SequencersDlg::onCsvFile() {
    seq::Sequencer* seq = currentSequencer();
    if(seq != nullptr) {
        // Get the CSV data
        std::string csv = seq->makeCsv();
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(csv.c_str());
        mimeData->setData("text/plain", data);
        clipboard->setMimeData(mimeData);
    }
}

// An entry in the tree has been changed
void SequencersDlg::onSequencerChanged(QTreeWidgetItem* item, int column) {
    if(!initialising_) {
        // Get the material
        int identifier = item->text(sequencerColIdentifier).toInt();
        seq::Sequencer* sequencer = m_->sequencers().find(identifier);
        if(sequencer != nullptr) {
            switch(column) {
                case sequencerColIdentifier:
                    // Identifier not changeable
                    break;
                case sequencerColName:
                    // Change the name
                    sequencer->name(item->text(
                            sequencerColName).toStdString().c_str());
                    break;
                case sequencerColType:
                    // Sequencer type not changeable
                    break;
                default:
                    break;
            }
            fillSequencerTree(sequencer);
        }
    }
}

