/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearchSeqDlg.h"
#include "DialogHelper.h"
#include <Gs/GeneticSearch.h>
#include "FdTdLife.h"
#include "SequencersDlg.h"
#include "GeneticSearchParamsDlg.h"
#include "GeneticSearchTargetDlg.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "GeneticSearchPopulationDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void GeneticSearchSeqDlg::construct(SequencersDlg* dlg, seq::Sequencer* sequencer) {
    SequencerDlg::construct(dlg, sequencer);
    sequencer_ = dynamic_cast<gs::GeneticSearch*>(sequencer);
    tabs_ = new QTabWidget;
    params_ = new GeneticSearchParamsDlg(this);
    target_ = new GeneticSearchTargetDlg(this);
    geneTemplate_ = new GeneticSearchGeneTemplateDlg(this);
    population_ = new GeneticSearchPopulationDlg(this);
    tabs_->addTab(params_, "Parameters");
    tabs_->addTab(target_, "Target");
    tabs_->addTab(geneTemplate_, "Gene Template");
    tabs_->addTab(population_, "Population");
    layout_->addWidget(tabs_);
    connect(tabs_, SIGNAL(tabBarClicked(int)), SLOT(onTabChange(int)));
    initialise();
}

// Initialise the GUI parameters
void GeneticSearchSeqDlg::initialise() {
    SequencerDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    params_->initialise();
    target_->initialise();
    geneTemplate_->initialise();
    population_->initialise();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneticSearchSeqDlg::tabChanged() {
    onChange();
    params_->tabChanged();
    target_->tabChanged();
    geneTemplate_->tabChanged();
    population_->tabChanged();
}

// The current tab has changed
void GeneticSearchSeqDlg::onTabChange(int /*index*/) {
    auto* tab = dynamic_cast<GeneticSearchSubDlg*>(tabs_->widget(tabs_->currentIndex()));
    if(tab != nullptr) {
        tab->tabChanged();
    }
}

