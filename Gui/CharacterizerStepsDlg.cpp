/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CharacterizerStepsDlg.h"
#include "CharacterizerDlg.h"
#include <Seq/Characterizer.h>
#include <Seq/CharacterizerStep.h>
#include <Seq/ScannerPos.h>
#include "SequencersDlg.h"
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Source/PlaneWaveSource.h>

// Constructor
CharacterizerStepsDlg::CharacterizerStepsDlg(CharacterizerDlg* dlg) :
        CharacterizerSubDlg(dlg),
        Notifiable(dlg->dlg()->model()) {
    auto* layout = new QVBoxLayout;
    auto* topLayout = new QHBoxLayout;
    layout->addLayout(topLayout);
    // The items display
    auto* itemsLayout = new QVBoxLayout;
    topLayout->addLayout(itemsLayout);
    itemTree_ = new QTreeWidget;
    itemTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    itemTree_->setColumnCount(numColumns);
    itemTree_->setHeaderLabels({"Step", "Status"});
    itemTree_->setContextMenuPolicy(Qt::CustomContextMenu);
    itemsLayout->addWidget(itemTree_);
    // The item data
    auto* bottomLayout = new QHBoxLayout;
    layout->addLayout(bottomLayout);
    auto* transmittanceLayout = new QVBoxLayout;
    transmittanceView_ = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
    transmittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0,
                                    1.0, "");
    transmittanceLayout->addWidget(new QLabel("Transmittance"));
    transmittanceLayout->addWidget(transmittanceView_);
    bottomLayout->addLayout(transmittanceLayout);
    auto* phaseShiftLayout = new QVBoxLayout;
    phaseShiftView_ = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
    phaseShiftView_->setAxisInfo(0.0, 100.0, "GHz", -180.0,
                                 180.0, "deg");
    phaseShiftLayout->addWidget(new QLabel("Phase Shift"));
    phaseShiftLayout->addWidget(phaseShiftView_);
    bottomLayout->addLayout(phaseShiftLayout);
    // Set the layout
    setTabLayout(layout);
    // Connect the handlers
    connect(itemTree_, SIGNAL(itemSelectionChanged()), SLOT(onItemSelect()));
    connect(itemTree_, SIGNAL(customContextMenuRequested(
                                      const QPoint&)),
            SLOT(onStepContext(
                         const QPoint&)));
    // Fill the panel
    initialise();
}

// Initialise the panel from the configuration
void CharacterizerStepsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    transmittanceView_->clear();
    phaseShiftView_->clear();
    fillItemList();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CharacterizerStepsDlg::tabChanged() {
}

// Return the currently selected item
std::shared_ptr<seq::CharacterizerStep> CharacterizerStepsDlg::currentItem() {
    QTreeWidgetItem* treeItem = itemTree_->currentItem();
    std::shared_ptr<seq::CharacterizerStep> item;
    if(treeItem != nullptr) {
        seq::ScannerPos pos(treeItem->text(columnStep).toStdString());
        item = dlg_->sequencer()->getStep(pos);
    }
    return item;
}

// An item has been selected
void CharacterizerStepsDlg::onItemSelect() {
    if(!initialising_) {
        // Get the item
        std::shared_ptr<seq::CharacterizerStep> item = currentItem();
        if(item != nullptr) {
            // Fill the model from the individual if we are not running
            if(dlg_->dlg()->model()->isStopped()) {
                dlg_->dlg()->model()->setStopped();
                dlg_->sequencer()->loadStepIntoModel(item);
                dlg_->dlg()->model()->matchModelConfig();
                dlg_->dlg()->model()->initialiseModel();
                dlg_->sequencer()->updateFrequencies();
                dlg_->dlg()->model()->doNotification(FdTdLife::notifyIndividualLoaded);
            }
            // Fill the item part of the display
            plotItemData(item);
        }
    }
}

// Place the items's data into the data displays
void CharacterizerStepsDlg::plotItemData(const std::shared_ptr<seq::CharacterizerStep>& item) {
    double frequencySpacing = dlg_->sequencer()->frequencies().spacing();
    transmittanceView_->setData(item->data().transmittance(),
                                GuiConfiguration::elementDataA,
                                true);
    transmittanceView_->setAxisInfo(frequencySpacing / box::Constants::giga_,
                                    frequencySpacing / box::Constants::giga_,
                                    "GHz", 0.0, 1.0, "");
    phaseShiftView_->setData(item->data().phaseShift(), GuiConfiguration::elementDataA,
                             true);
    phaseShiftView_->setAxisInfo(frequencySpacing / box::Constants::giga_,
                                 frequencySpacing / box::Constants::giga_,
                                 "GHz", -180.0, 180.0, "deg");
}

// Fill the list box with the scan steps
void CharacterizerStepsDlg::fillItemList() {
    InitialisingGui::Set s(initialising_);
    itemTree_->clear();
    for(auto& step : dlg_->sequencer()->steps()) {
        addItemToList(step);
    }
}

// Add an item to the catalogue tree
void CharacterizerStepsDlg::addItemToList(const std::shared_ptr<seq::CharacterizerStep>& item,
                                          QTreeWidgetItem* treeItem) {
    if(treeItem == nullptr) {
        treeItem = new QTreeWidgetItem(itemTree_);
    }
    std::string status = "waiting";
    if(!item->empty()) {
        status = "complete";
    } else if(dlg_->sequencer()->currentStep() == item) {
        status = "running";
    }
    treeItem->setText(columnStep, QString(item->step().text().c_str()));
    treeItem->setText(columnStatus, QString(status.c_str()));
}

// Handle a configuration change notification
void CharacterizerStepsDlg::notify(fdtd::Notifier* source, int why,
                                   fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        switch(why) {
            case FdTdLife::notifySequencerChange:
            case FdTdLife::notifyRunComplete:
                fillItemList();
            default:
                break;
        }
    }
}

// Show the context menu for the step tree
void CharacterizerStepsDlg::onStepContext(const QPoint& pos) {
    QMenu menu;
    QAction* copyToClipboard = menu.addAction("Copy admittance to clipboard");
    QAction* selectedAction = menu.exec(itemTree_->mapToGlobal(pos));
    if(selectedAction == copyToClipboard) {
        onCopyToClipboard();
    }
}

// Copy the current step to the clipboard
void CharacterizerStepsDlg::onCopyToClipboard() {
    // Get the item
    std::shared_ptr<seq::CharacterizerStep> item = currentItem();
    if(item != nullptr) {
        // Encode the individual into XML
        auto* o = new xml::DomObject("admittance");
        double unitCell = dlg_->sequencer()->m()->p()->p2().x().value() -
                dlg_->sequencer()->m()->p()->p1().x().value();
        auto source = dynamic_cast<source::PlaneWaveSource*>(
                dlg_->sequencer()->m()->sources().sources().front());
        if(unitCell > 0.0 && source != nullptr) {
            *o << item->data().admittance(unitCell, source->frequencies().first().value(),
                                          source->frequencies().spacing().value());
            xml::DomDocument dom(o);
            xml::Writer writer;
            std::string text;
            writer.writeString(&dom, text);
            QClipboard* clipboard = QApplication::clipboard();
            auto mimeData = new QMimeData;
            QByteArray data(text.c_str());
            mimeData->setData("text/plain", data);
            mimeData->setData("fdtdlife/admittance", data);
            clipboard->setMimeData(mimeData);
        }
    }
}
