/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueBuilderItemsDlg.h"
#include "DialogHelper.h"
#include "CatalogueBuilderDlg.h"
#include "SequencersDlg.h"
#include "GuiConfiguration.h"
#include "BinaryCodedPlateWidget.h"
#include "CatalogueBrowseDlg.h"
#include <CatalogueBuilder.h>
#include <CatalogueItem.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Model.h>
#include <CatalogueItemWrapper.h>

// Constructor
CatalogueBuilderItemsDlg::CatalogueBuilderItemsDlg(CatalogueBuilderDlg* dlg) :
        CatalogueBuilderSubDlg(dlg),
        Notifiable(dlg->dlg()->model()) {
    auto* layout = new QVBoxLayout;
    auto* topLayout = new QHBoxLayout;
    layout->addLayout(topLayout);
    // The items display
    auto* itemsLayout = new QVBoxLayout;
    topLayout->addLayout(itemsLayout);
    auto* progressLayout = new QHBoxLayout;
    itemsLayout->addLayout(progressLayout);
    numUniqueEdit_ = DialogHelper::intItem(progressLayout, "Unique", true);
    numRequiredEdit_ = DialogHelper::intItem(progressLayout, "Total", true);
    numBuiltEdit_ = DialogHelper::intItem(progressLayout, "So Far", true);
    auto buttonsLayout = new QHBoxLayout;
    itemsLayout->addLayout(buttonsLayout);
    browseButton_ = DialogHelper::buttonItem(buttonsLayout, "Browse Catalogue");
    copyButton_ = DialogHelper::buttonItem(buttonsLayout, "Copy Item");
    itemTree_ = new QTreeWidget;
    itemTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    itemTree_->setColumnCount(numColumns);
    itemTree_->setHeaderLabels({"Coding", "Pattern"});
    itemsLayout->addWidget(itemTree_);
    // The item pattern
    auto* patternLayout = new QVBoxLayout;
    topLayout->addLayout(patternLayout);
    patternLayout->addStretch();
    bitPatternEdit_ = new BinaryCodedPlateWidget({0},
                                                 dlg_->sequencer()->bitsPerHalfSide() * 2,
                                                 dlg_->sequencer()->fourFoldSymmetry(),
                                                 true, 192);
    patternLayout->addWidget(bitPatternEdit_);
    // The item data
    auto* bottomLayout = new QHBoxLayout;
    layout->addLayout(bottomLayout);
    auto* transmittanceLayout = new QVBoxLayout;
    transmittanceView_ = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
    transmittanceView_->setAxisInfo(0.0, 100.0, "GHz", 0.0,
                                    1.0, "");
    transmittanceLayout->addWidget(new QLabel("Transmittance"));
    transmittanceLayout->addWidget(transmittanceView_);
    bottomLayout->addLayout(transmittanceLayout);
    auto* phaseShiftLayout = new QVBoxLayout;
    phaseShiftView_ = new DataSeriesWidget(dlg_->dlg()->model()->guiConfiguration());
    phaseShiftView_->setAxisInfo(0.0, 100.0, "GHz", -180.0,
                                 180.0, "deg");
    phaseShiftLayout->addWidget(new QLabel("Phase Shift"));
    phaseShiftLayout->addWidget(phaseShiftView_);
    bottomLayout->addLayout(phaseShiftLayout);
    // Set the layout
    setTabLayout(layout);
    // Connect the handlers
    connect(itemTree_, SIGNAL(itemSelectionChanged()), SLOT(onItemSelect()));
    connect(browseButton_, SIGNAL(clicked()), SLOT(onCreatePdf()));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopyItem()));
    // Fill the panel
    CatalogueBuilderItemsDlg::initialise();
}

// Initialise the panel from the configuration
void CatalogueBuilderItemsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    transmittanceView_->clear();
    phaseShiftView_->clear();
    bitPatternEdit_->clear();
    numBuiltEdit_->setText(QString::number(dlg_->sequencer()->numItems()));
    numRequiredEdit_->setText(QString::number(dlg_->sequencer()->numRequired() +
                                              dlg_->sequencer()->numSymmetries()));
    numUniqueEdit_->setText(QString::number(dlg_->sequencer()->numRequired()));
    fillItemList();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CatalogueBuilderItemsDlg::tabChanged() {
}

// Return the currently selected item
std::shared_ptr<fdtd::CatalogueItem> CatalogueBuilderItemsDlg::currentItem() {
    QTreeWidgetItem* treeItem = itemTree_->currentItem();
    std::shared_ptr<fdtd::CatalogueItem> item;
    if(treeItem != nullptr) {
        uint64_t coding = treeItem->text(columnCoding).toULongLong();
        item = dlg_->sequencer()->getItem(coding);
    }
    return item;
}

// An item has been selected
void CatalogueBuilderItemsDlg::onItemSelect() {
    if(!initialising_) {
        // Get the item
        std::shared_ptr<fdtd::CatalogueItem> item = currentItem();
        if(item != nullptr) {
            // Fill the model from the individual if we are not running
            if(dlg_->dlg()->model()->isStopped()) {
                dlg_->dlg()->model()->setStopped();
                dlg_->sequencer()->loadItemIntoModel(item);
                dlg_->dlg()->model()->matchModelConfig();
                dlg_->dlg()->model()->initialiseModel();
                dlg_->dlg()->model()->doNotification(FdTdLife::notifyIndividualLoaded);
            }
            // Fill the item part of the display
            bitPatternEdit_->setData({item->pattern()},
                                     dlg_->sequencer()->bitsPerHalfSide() * 2);
            plotItemData(item);
        }
    }
}

// Place the items's data into the data displays
void CatalogueBuilderItemsDlg::plotItemData(const std::shared_ptr<fdtd::CatalogueItem>& item) {
    transmittanceView_->setData(item->transmittance(), GuiConfiguration::elementDataA,
                                true);
    if(!dlg_->sequencer()->fourFoldSymmetry()) {
        transmittanceView_->setData(item->transmittanceY(),
                                    GuiConfiguration::elementDataC);
    }
    transmittanceView_->setAxisInfo(
            dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
            dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
            "GHz", 0.0, 1.0, "");
    phaseShiftView_->setData(item->phaseShift(), GuiConfiguration::elementDataA,
                             true);
    if(!dlg_->sequencer()->fourFoldSymmetry()) {
        phaseShiftView_->setData(item->phaseShiftY(), GuiConfiguration::elementDataC);
    }
    phaseShiftView_->setAxisInfo(
            dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
            dlg_->sequencer()->frequencySpacing() / box::Constants::giga_,
            "GHz", -180.0, 180.0, "deg");
}

// Fill the list box with the catalogue items
void CatalogueBuilderItemsDlg::fillItemList() {
    InitialisingGui::Set s(initialising_);
    itemTree_->clear();
    for(auto& pos : dlg_->sequencer()->items()) {
        addItemToList(pos.second);
    }
}

// Add an item to the catalogue tree
void CatalogueBuilderItemsDlg::addItemToList(const std::shared_ptr<fdtd::CatalogueItem>& item,
                                             QTreeWidgetItem* treeItem) {
    if(treeItem == nullptr) {
        treeItem = new QTreeWidgetItem(itemTree_);
    }
    treeItem->setText(columnCoding, QString::number(item->code()));
    treeItem->setText(columnPattern, QString::number(item->pattern()));
}

// Handle a configuration change notification
void CatalogueBuilderItemsDlg::notify(fdtd::Notifier* source, int why,
                                      fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        if(why == FdTdLife::notifySequencerChange) {
            if(!initialising_) {
                initialise();
            }
        }
    }
}

// Create a PDF file of the catalogue
void CatalogueBuilderItemsDlg::onCreatePdf() {
    // The dialog to print
    CatalogueBrowseDlg dlg(this, dlg_);
    dlg.exec();
}

// Copy the current item to the clipboard
void CatalogueBuilderItemsDlg::onCopyItem() {
    // Get the item
    std::shared_ptr<fdtd::CatalogueItem> item = currentItem();
    if(item != nullptr) {
        fdtd::CatalogueItemWrapper completeItem(item, dlg_->sequencer()->catalogue());
        // Encode the item into XML
        auto* o = new xml::DomObject("catalogueitem");
        *o << completeItem;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(text.c_str());
        mimeData->setData("text/plain", data);
        mimeData->setData("fdtdlife/catalogueitem", data);
        clipboard->setMimeData(mimeData);
    }
}
