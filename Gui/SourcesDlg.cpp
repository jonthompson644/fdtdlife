/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include "SourcesDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include "PlaneWaveSourceDlg.h"
#include "ZoneSourceDlg.h"
#include "GravyWaveSourceDlg.h"
#include <Source/Sources.h>
#include <Source/Source.h>
#include <Source/PlaneWaveSource.h>
#include <Source/ZoneSource.h>
#include <Source/GravyWaveSource.h>

// Constructor
SourcesDlg::SourcesDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        sourceDlg_(nullptr) {
    // The parameters
    auto paramLayout = new QVBoxLayout;
    auto sourcesLayout = new QHBoxLayout;
    paramLayout->addLayout(sourcesLayout);
    // The buttons
    auto buttonLayout = new QVBoxLayout;
    addButton_ = DialogHelper::buttonMenuItem(buttonLayout, "New Source",
                                              {"Plane Wave Source", "Zone Source", "Gravitational Wave"});
    deleteButton_ = DialogHelper::buttonItem(buttonLayout, "Delete Source");
    buttonLayout->addStretch();
    sourcesLayout->addLayout(buttonLayout);
    // The sources tree
    sourcesTree_ = new QTreeWidget;
    sourcesTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    sourcesTree_->setColumnCount(numSourceCols);
    sourcesTree_->setHeaderLabels({"Identifier", "Name"});
    sourcesTree_->setFixedWidth(320);
    sourcesLayout->addWidget(sourcesTree_);
    // The place holder for the source dialog
    sourceDlgLayout_ = new QVBoxLayout;
    sourcesLayout->addLayout(sourceDlgLayout_);
    sourcesLayout->addStretch();
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(sourcesTree_, SIGNAL(itemSelectionChanged()), SLOT(onSourceSelected()));
    connect(addButton_, SIGNAL(clickedMenu(int)), SLOT(onNew(int)));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    // Fill the dialog
    initialise();
}

// Initialise the dialog
void SourcesDlg::initialise() {
    if(!initialising_) {
        // Get the shape
        source::Source* source = getCurrentSource();
        // Fill the shape list
        fillSourceTree(source);
    }
}

// Fill the source tree
void SourcesDlg::fillSourceTree(source::Source* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Fill the tree
        sourcesTree_->clear();
        for(auto& source : m_->sources().sources()) {
            auto item = new QTreeWidgetItem(sourcesTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(sourceColIdentifier, QString::number(source->identifier()));
            item->setText(sourceColName, source->name().c_str());
            if(selItem == nullptr || source == sel) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem != nullptr) {
        sourcesTree_->setCurrentItem(selItem);
    }
}

// Get the current sensorId.
source::Source* SourcesDlg::getCurrentSource() {
    source::Source* result = nullptr;
    auto item = sourcesTree_->currentItem();
    if(item != nullptr) {
        int identifier = sourcesTree_->currentItem()->text(sourceColIdentifier).toInt();
        result = m_->sources().getSource(identifier);
    }
    return result;
}

// Fill the user interface using the selected source
void SourcesDlg::onSourceSelected() {
    if(!initialising_) {
        // Get the source
        source::Source* source = getCurrentSource();
        // Delete any old dialog
        delete sourceDlg_;
        sourceDlg_ = nullptr;
        // Make the dialog for the source
        if(source != nullptr) {
            switch(source->mode()) {
                case source::SourceFactory::modePlaneWave:
                    sourceDlg_ = new PlaneWaveSourceDlg(
                            this, dynamic_cast<source::PlaneWaveSource*>(source));
                    break;
                case source::SourceFactory::modeZone:
                    sourceDlg_ = new ZoneSourceDlg(
                            this, dynamic_cast<source::ZoneSource*>(source));
                    break;
                case source::SourceFactory::modeGravyWave:
                    sourceDlg_ = new GravyWaveSourceDlg(
                            this, dynamic_cast<source::GravyWaveSource*>(source));
                    break;
                default:
                    break;
            }
        }
        if(sourceDlg_ != nullptr) {
            sourceDlg_->initialise();
            sourceDlgLayout_->addWidget(sourceDlg_);
        }
    }
}

// Add a new source
void SourcesDlg::onNew(int sourceType) {
    tabChanged();  // Update anything that may have changed for the current item
    source::Source* source = nullptr;
    switch(sourceType) {
        case newSourcePlaneWave:
            source = m_->sources().factory()->make(source::SourceFactory::modePlaneWave, m_);
            break;
        case newSourceZone:
            source = m_->sources().factory()->make(source::SourceFactory::modeZone, m_);
            break;
        case newSourceGravyWave:
            source = m_->sources().factory()->make(source::SourceFactory::modeGravyWave, m_);
            break;
        default:
            break;
    }
    QString name;
    QTextStream n(&name);
    n << "Source " << source->identifier();
    source->name(name.toStdString());
    fillSourceTree(source);
    m_->doNotification(FdTdLife::notifyMinorChange);
}

// Delete a source
void SourcesDlg::onDelete() {
    source::Source* source = getCurrentSource();
    if(source != nullptr) {
        QMessageBox box;
        QString msg;
        QTextStream t(&msg);
        t << "OK to delete the source " << source->name().c_str();
        box.setText(msg);
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            delete source;
            fillSourceTree();
            onSourceSelected();
            m_->doNotification(FdTdLife::notifyMinorChange);
        }
    }
}

// The currently selected sensorId has changed and the tree needs updating
void SourcesDlg::updateCurrentSelection() {
    source::Source* source = getCurrentSource();
    auto item = sourcesTree_->currentItem();
    if(source != nullptr && item != nullptr) {
        item->setText(sourceColName, source->name().c_str());
    }
}

// Handle a notification
void SourcesDlg::notify(fdtd::Notifier* source, int /*why*/, fdtd::NotificationData* /*data*/) {
    if(m_ == source) {
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void SourcesDlg::tabChanged() {
    if(sourceDlg_ != nullptr) {
        sourceDlg_->tabChanged();
    }
}
