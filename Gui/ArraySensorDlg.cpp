/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ArraySensorDlg.h"
#include "DialogHelper.h"
#include <Sensor/ArraySensor.h>
#include <Source/PlaneWaveSource.h>
#include "FdTdLife.h"
#include "SensorsDlg.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneticSearch.h>
#include <Box/Utility.h>
#include <RecordedData.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>

// Constructor
ArraySensorDlg::ArraySensorDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        SensorDlg(dlg, sensor),
        sensor_(dynamic_cast<sensor::ArraySensor*>(sensor)) {
    auto line1Layout = new QHBoxLayout;
    numPointsXEdit_ = DialogHelper::intItem(line1Layout, "Num Points X");
    numPointsYEdit_ = DialogHelper::intItem(line1Layout, "Num Points Y");
    layout_->addLayout(line1Layout);
    frequencyEdit_ = DialogHelper::doubleItem(layout_, "Current Frequency");
    auto line2Layout = new QHBoxLayout;
    averageOverCellCheck_ = DialogHelper::boolItem(line2Layout, "Average Over Cell");
    includeReflectedCheck_ = DialogHelper::boolItem(line2Layout,
                                                    "Include Reflection Data");
    useWindowCheck_ = DialogHelper::boolItem(line2Layout, "Use Window");
    layout_->addLayout(line2Layout);
    auto line3Layout = new QHBoxLayout;
    animateFrequencyCheck_ = DialogHelper::boolItem(line3Layout, "Animate Frequency");
    frequencyStepEdit_ = DialogHelper::doubleItem(line3Layout, "Frequency Step");
    layout_->addLayout(line3Layout);
    auto line3bLayout = new QHBoxLayout;
    manualPosCheck_ = DialogHelper::boolItem(line3bLayout, "Manual Z");
    transmittedPosEdit_ = DialogHelper::doubleItem(line3bLayout, "Transmitted Z (m)");
    reflectedPosEdit_ = DialogHelper::doubleItem(line3bLayout, "Reflected Z (m)");
    layout_->addLayout(line3bLayout);
    auto line3aLayout = new QHBoxLayout;
    xComponentCheck_ = DialogHelper::boolItem(line3aLayout, "Components: X");
    yComponentCheck_ = DialogHelper::boolItem(line3aLayout, "Y");
    zComponentCheck_ = DialogHelper::boolItem(line3aLayout, "Z");
    line3aLayout->addStretch();
    layout_->addLayout(line3aLayout);
    auto line3cLayout = new QHBoxLayout;
    manualSamplesCheck_ = DialogHelper::boolItem(line3cLayout, "Manual Samples");
    numSamplesEdit_ = DialogHelper::intItem(line3cLayout, "Number of Samples");
    line3cLayout->addStretch();
    layout_->addLayout(line3cLayout);
    csvButton_ = DialogHelper::buttonMenuItem(layout_, "Copy to Clipboard",
                                              {"Transmitted Time Series CSV",
                                               "Reflected Time Series CSV",
                                               "Transmitted Spectra CSV",
                                               "Reflected SpectraCSV",
                                               "Transmitted Admittance XML"});
    connect(numPointsXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numPointsYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(transmittedPosEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(reflectedPosEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(animateFrequencyCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(averageOverCellCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(includeReflectedCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(xComponentCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(yComponentCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(zComponentCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(useWindowCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(frequencyStepEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numSamplesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(manualPosCheck_, SIGNAL(stateChanged(int)), SLOT(onManual()));
    connect(manualSamplesCheck_, SIGNAL(stateChanged(int)), SLOT(onManual()));
    connect(csvButton_, SIGNAL(clickedMenu(int)), SLOT(onCopyToClipboard(int)));
}

// Handle a change to one of the manual settings check boxes
void ArraySensorDlg::onManual() {
    onChange();
    updateUi();
}

// Update the UI
void ArraySensorDlg::updateUi() {
    // Set the read only status of the Z position boxes
    transmittedPosEdit_->setReadOnly(!manualPosCheck_->isChecked());
    reflectedPosEdit_->setReadOnly(!manualPosCheck_->isChecked());
    if(!manualPosCheck_->isChecked()) {
        transmittedPosEdit_->setText(QString::number(sensor_->transmittedZPos()));
        reflectedPosEdit_->setText(QString::number(sensor_->reflectedZPos()));
    }
    // Set the read only status of the number of samples box
    numSamplesEdit_->setReadOnly(!manualSamplesCheck_->isChecked());
    if(!manualSamplesCheck_->isChecked()) {
        numSamplesEdit_->setText(QString::number(sensor_->nSamples()));
    }
}

// Initialise the GUI parameters
void ArraySensorDlg::initialise() {
    SensorDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    numPointsXEdit_->setText(QString::number(sensor_->numPointsX()));
    numPointsYEdit_->setText(QString::number(sensor_->numPointsY()));
    frequencyEdit_->setText(QString::number(sensor_->frequency()));
    frequencyStepEdit_->setText(QString::number(sensor_->frequencyStep()));
    animateFrequencyCheck_->setChecked(sensor_->animateFrequency());
    averageOverCellCheck_->setChecked(sensor_->averageOverCell());
    includeReflectedCheck_->setChecked(sensor_->includeReflected());
    xComponentCheck_->setChecked(sensor_->componentSel().x());
    yComponentCheck_->setChecked(sensor_->componentSel().y());
    zComponentCheck_->setChecked(sensor_->componentSel().z());
    numSamplesEdit_->setText(QString::number(sensor_->nSamples()));
    transmittedPosEdit_->setText(QString::number(sensor_->transmittedZPos()));
    manualSamplesCheck_->setChecked(sensor_->manualSamples());
    manualPosCheck_->setChecked(sensor_->manualZPos());
    reflectedPosEdit_->setText(QString::number(sensor_->reflectedZPos()));
    updateUi();
}

// An edit box has changed
void ArraySensorDlg::onChange() {
    SensorDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newNumPointsX = c.testSizeT(numPointsXEdit_, sensor_->numPointsX(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newNumPointsY = c.testSizeT(numPointsYEdit_, sensor_->numPointsY(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newFrequency = c.testDouble(frequencyEdit_, sensor_->frequency(),
                                         FdTdLife::notifyMinorChange);
        auto newAnimateFrequency = c.testBool(animateFrequencyCheck_,
                                              sensor_->animateFrequency(),
                                              FdTdLife::notifyMinorChange);
        auto newFrequencyStep = c.testDouble(frequencyStepEdit_,
                                             sensor_->frequencyStep(),
                                             FdTdLife::notifyMinorChange);
        auto newAverageOverCell = c.testBool(averageOverCellCheck_,
                                             sensor_->averageOverCell(),
                                             FdTdLife::notifyDomainContentsChange);
        auto newIncludeReflected = c.testBool(includeReflectedCheck_,
                                              sensor_->includeReflected(),
                                              FdTdLife::notifyMinorChange);
        auto newUseWindow = c.testBool(useWindowCheck_,
                                              sensor_->useWindow(),
                                              FdTdLife::notifyMinorChange);
        auto newComponentSel = c.testComponentSel(xComponentCheck_, yComponentCheck_,
                                                  zComponentCheck_,
                                                  sensor_->componentSel(),
                                                  FdTdLife::notifyMinorChange);
        auto newManualSamples = c.testBool(manualSamplesCheck_,
                                           sensor_->manualSamples(),
                                           FdTdLife::notifyDomainContentsChange);
        auto newManualZPos = c.testBool(manualPosCheck_, sensor_->manualZPos(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newTransmittedZPos = c.testDouble(transmittedPosEdit_,
                                               sensor_->transmittedZPos(),
                                               FdTdLife::notifyDomainContentsChange);
        auto newReflectedZPos = c.testDouble(reflectedPosEdit_,
                                             sensor_->reflectedZPos(),
                                             FdTdLife::notifyDomainContentsChange);
        auto newNumSamples = c.testSizeT(numSamplesEdit_, sensor_->nSamples(),
                                         FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            sensor_->numPointsX(newNumPointsX);
            sensor_->numPointsY(newNumPointsY);
            sensor_->frequency(newFrequency);
            sensor_->animateFrequency(newAnimateFrequency);
            sensor_->frequencyStep(newFrequencyStep);
            sensor_->averageOverCell(newAverageOverCell);
            sensor_->componentSel(newComponentSel);
            sensor_->manualSamples(newManualSamples);
            sensor_->manualZPos(newManualZPos);
            sensor_->transmittedZPos(newTransmittedZPos);
            sensor_->reflectedZPos(newReflectedZPos);
            sensor_->nSamples(newNumSamples);
            sensor_->includeReflected(newIncludeReflected);
            sensor_->useWindow(newUseWindow);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void ArraySensorDlg::tabChanged() {
    onChange();
}

// The model has changed
void ArraySensorDlg::modelNotify() {
    // Update our read backs
    double oldFrequency = frequencyEdit_->text().toDouble();
    if(box::Utility::equals(oldFrequency, sensor_->frequency())) {
        InitialisingGui::Set s(dlg_->initialising());
        frequencyEdit_->setText(QString::number(sensor_->frequency()));
    }
}

// Copy data to the clipboard
void ArraySensorDlg::onCopyToClipboard(int addKind) {
    // Prepare the clipboard
    QClipboard* clipboard = QApplication::clipboard();
    auto mimeData = new QMimeData;
    // Get the CSV data
    std::string csv;
    switch(addKind) {
        case csvTransmittedTimeSeries:
            csv = sensor_->transmittedTimeSeriesCsv();
            mimeData->setText(csv.c_str());
            break;
        case csvReflectedTimeSeries:
            csv = sensor_->reflectedTimeSeriesCsv();
            mimeData->setText(csv.c_str());
            break;
        case csvTransmittedSpectra:
            csv = sensor_->transmittedSpectraCsv();
            mimeData->setText(csv.c_str());
            break;
        case csvReflectedSpectra:
            csv = sensor_->reflectedSpectraCsv();
            mimeData->setText(csv.c_str());
            break;
        case xmlTransmittedAdmittance: {
            auto* o = new xml::DomObject("admittance");
            double unitCell = dlg_->model()->p()->p2().x().value() -
                              dlg_->model()->p()->p1().x().value();
            auto source = dynamic_cast<source::PlaneWaveSource*>(
                    dlg_->model()->sources().sources().front());
            if(unitCell > 0.0 && source != nullptr) {
                fdtd::RecordedData recordedData;
                sensor_->copyOutputData(0, 0, recordedData);
                *o << recordedData.admittance(unitCell, source->frequencies().first().value(),
                                              source->frequencies().spacing().value());
                xml::DomDocument dom(o);
                xml::Writer writer;
                std::string text;
                writer.writeString(&dom, text);
                QByteArray data(text.c_str());
                mimeData->setData("text/plain", data);
                mimeData->setData("fdtdlife/admittance", data);
            }
            break;
        }
        default:
            break;
    }
    // Copy the data to the clipboard
    clipboard->setMimeData(mimeData);
}

