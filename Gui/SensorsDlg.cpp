/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SensorsDlg.h"
#include "FdTdLife.h"
#include "DialogHelper.h"
#include "TwoDSliceDlg.h"
#include "ArraySensorDlg.h"
#include "MaterialSensorDlg.h"
#include "FarFieldSensorDlg.h"
#include "RadiationPatternDlg.h"
#include "AreaSensorDlg.h"
#include <Sensor/Sensor.h>
#include <Sensor/TwoDSlice.h>
#include <Sensor/ArraySensor.h>
#include <Sensor/MaterialSensor.h>
#include <Sensor/FarFieldSensor.h>
#include <Sensor/RadiationPattern.h>
#include <Sensor/AreaSensor.h>

// Constructor
SensorsDlg::SensorsDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        sensorDlg_(nullptr) {
    // The parameters
    auto paramLayout = new QVBoxLayout;
    auto sensorsLayout = new QHBoxLayout;
    // The shapes buttons
    auto buttonLayout = new QVBoxLayout;
    new2DSliceSensorButton_ = DialogHelper::buttonItem(buttonLayout,
                                                       "New 2D Slice Sensor");
    newArraySensorButton_ = DialogHelper::buttonItem(buttonLayout, "New Array Sensor");
    newMaterialSensorButton_ = DialogHelper::buttonItem(buttonLayout,
                                                        "New Material Sensor");
    newFarFieldSensorButton_ = DialogHelper::buttonItem(buttonLayout,
                                                        "New Far Field Sensor");
    newRadiationPatternButton_ = DialogHelper::buttonItem(buttonLayout,
                                                          "New Radiation Pattern");
    newAreaSensorButton_ = DialogHelper::buttonItem(buttonLayout, "New Area Sensor");
    deleteButton_ = DialogHelper::buttonItem(buttonLayout, "Delete");
    buttonLayout->addStretch();
    sensorsLayout->addLayout(buttonLayout);
    // The sensors tree
    sensorsTree_ = new QTreeWidget;
    sensorsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    sensorsTree_->setColumnCount(numSensorCols);
    sensorsTree_->setHeaderLabels({"Identifier", "Name", "Type"});
    sensorsTree_->setDragDropMode(QAbstractItemView::InternalMove);
    sensorsTree_->setFixedWidth(320);
    sensorsLayout->addWidget(sensorsTree_);
    // The place holder for the sensorId dialog
    sensorDlgLayout_ = new QVBoxLayout;
    sensorsLayout->addLayout(sensorDlgLayout_);
    sensorsLayout->addStretch();
    // The shapes assembly
    paramLayout->addLayout(sensorsLayout);
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(sensorsTree_, SIGNAL(itemSelectionChanged()), SLOT(onSensorSelected()));
    connect(new2DSliceSensorButton_, SIGNAL(clicked()), SLOT(onNew2DSliceSensor()));
    connect(newArraySensorButton_, SIGNAL(clicked()), SLOT(onNewArraySensor()));
    connect(newMaterialSensorButton_, SIGNAL(clicked()), SLOT(onNewMaterialSensor()));
    connect(newFarFieldSensorButton_, SIGNAL(clicked()), SLOT(onNewFarFieldSensor()));
    connect(newRadiationPatternButton_, SIGNAL(clicked()),
            SLOT(onNewRadiationPattern()));
    connect(newAreaSensorButton_, SIGNAL(clicked()), SLOT(onNewAreaSensor()));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    // Fill the dialog
    SensorsDlg::initialise();
}

// Fill the sensorId tree
void SensorsDlg::fillSensorTree(sensor::Sensor* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Delete any old dialog
        delete sensorDlg_;
        sensorDlg_ = nullptr;
        // Fill the tree
        sensorsTree_->clear();
        for(auto& sensor : m_->sensors()) {
            auto item = new QTreeWidgetItem(sensorsTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(sensorColIdentifier,
                    QString::number(sensor->identifier()));
            item->setText(sensorColName, sensor->name().c_str());
            item->setText(sensorColType,
                    m_->sensors().factory().modeName(sensor->mode()).c_str());
            if(selItem == nullptr || sensor.get() == sel) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem != nullptr) {
        sensorsTree_->setCurrentItem(selItem);
    }
}

// Initialise the dialog
void SensorsDlg::initialise() {
    if(!initialising_) {
        // Get the shape
        sensor::Sensor* sensor = getCurrentSensor();
        // Fill the shape list
        fillSensorTree(sensor);
    }
}

// Get the current sensorId.
sensor::Sensor* SensorsDlg::getCurrentSensor() {
    sensor::Sensor* result = nullptr;
    auto item = sensorsTree_->currentItem();
    if(item != nullptr) {
        int identifier = sensorsTree_->currentItem()->text(sensorColIdentifier).toInt();
        result = m_->sensors().find(identifier);
    }
    return result;
}


// Fill the user interface using the selected sensorId
void SensorsDlg::onSensorSelected() {
    if(!initialising_) {
        // Get the sensorId
        sensor::Sensor* sensor = getCurrentSensor();
        // Delete any old dialog
        delete sensorDlg_;
        sensorDlg_ = nullptr;
        // Make dialog for the sensorId
        if(sensor != nullptr) {
            switch(sensor->mode()) {
                case sensor::Sensors::modeTwoD:
                    sensorDlg_ = new TwoDSliceDlg(
                            this, dynamic_cast<sensor::TwoDSlice*>(sensor));
                    break;
                case sensor::Sensors::modeMaterial:
                    sensorDlg_ = new MaterialSensorDlg(
                            this, dynamic_cast<sensor::MaterialSensor*>(sensor));
                    break;
                case sensor::Sensors::modeArraySensor:
                    sensorDlg_ = new ArraySensorDlg(
                            this, dynamic_cast<sensor::ArraySensor*>(sensor));
                    break;
                case sensor::Sensors::modeFarFieldSensor:
                    sensorDlg_ = new FarFieldSensorDlg(
                            this, dynamic_cast<sensor::FarFieldSensor*>(sensor));
                    break;
                case sensor::Sensors::modeRadiationPattern:
                    sensorDlg_ = new RadiationPatternDlg(this, sensor);
                    break;
                case sensor::Sensors::modeArea:
                    sensorDlg_ = new AreaSensorDlg(this, sensor);
                    break;
                default:
                case sensor::Sensors::modeNone:
                    break;
            }
            if(sensorDlg_ != nullptr) {
                sensorDlg_->initialise();
                sensorDlgLayout_->addWidget(sensorDlg_);
                updateUi();
            }
        }
    }
}

// Add a new 2D slice sensorId
void SensorsDlg::onNew2DSliceSensor() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* sensor = m_->sensors().makeSensor(sensor::Sensors::modeTwoD);
    n << "Sensor" << sensor->identifier();
    sensor->name(name.toStdString());
    fillSensorTree(sensor);
    InitialisingGui::Set s(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Add a new array sensorId
void SensorsDlg::onNewArraySensor() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* sensor = m_->sensors().makeSensor(sensor::Sensors::modeArraySensor);
    n << "Sensor" << sensor->identifier();
    sensor->name(name.toStdString());
    fillSensorTree(sensor);
    InitialisingGui::Set s(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Add a new radiation pattern sensorId
void SensorsDlg::onNewRadiationPattern() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* s = m_->sensors().makeSensor(sensor::Sensors::modeRadiationPattern);
    n << "Sensor" << s->identifier();
    s->name(name.toStdString());
    fillSensorTree(s);
    InitialisingGui::Set i(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Add a new area sensorId
void SensorsDlg::onNewAreaSensor() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* s = m_->sensors().makeSensor(sensor::Sensors::modeArea);
    n << "Sensor" << s->identifier();
    s->name(name.toStdString());
    fillSensorTree(s);
    InitialisingGui::Set i(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Add a new material sensorId
void SensorsDlg::onNewMaterialSensor() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* sensor = m_->sensors().makeSensor(sensor::Sensors::modeMaterial);
    n << "Sensor" << sensor->identifier();
    sensor->name(name.toStdString());
    fillSensorTree(sensor);
    InitialisingGui::Set s(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Add a new far field sensorId
void SensorsDlg::onNewFarFieldSensor() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    sensor::Sensor* sensor = m_->sensors().makeSensor(
            sensor::Sensors::modeFarFieldSensor);
    n << "Sensor" << sensor->identifier();
    sensor->name(name.toStdString());
    fillSensorTree(sensor);
    InitialisingGui::Set s(initialising_);
    m_->doNotification(FdTdLife::notifyDomainContentsChange);
    m_->doNotification(FdTdLife::notifySensorChange);
}

// Delete a sensorId
void SensorsDlg::onDelete() {
    QMessageBox box;
    sensor::Sensor* sensor = getCurrentSensor();
    if(sensor != nullptr) {
        QString msg;
        QTextStream t(&msg);
        t << "OK to delete the sensorId " << sensor->name().c_str();
        box.setText(msg);
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            m_->sensors().remove(sensor);
            fillSensorTree();
            InitialisingGui::Set s(initialising_);
            m_->doNotification(FdTdLife::notifyDomainContentsChange);
            m_->doNotification(FdTdLife::notifySensorChange);
        }
    }
}

// The currently selected sensorId has changed and the tree needs updating
void SensorsDlg::updateCurrentSelection() {
    sensor::Sensor* sensor = getCurrentSensor();
    auto item = sensorsTree_->currentItem();
    if(sensor != nullptr && item != nullptr) {
        item->setText(sensorColName, sensor->name().c_str());
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void SensorsDlg::tabChanged() {
    if(sensorDlg_ != nullptr) {
        sensorDlg_->tabChanged();
    }
}

// Update the enable state of the controls
void SensorsDlg::updateUi() {
}

// Handle a notification
void SensorsDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(m_ == source) {
        switch(why) {
            case FdTdLife::notifyInitialisation:
                break;
            case FdTdLife::notifyDomainContentsChange:
                initialise();
                break;
            default:
                break;
        }
    }
}

