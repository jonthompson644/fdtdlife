/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LensDesignerDlg.h"
#include "DesignersDlg.h"
#include <Designer/LensDesigner.h>
#include "DialogHelper.h"
#include "LensDesignerPhaseShiftsDlg.h"
#include "LensDesignerResultsDlg.h"
#include "LensDesignerColumnsDlg.h"

// Second stage constructor used by factory
void LensDesignerDlg::construct(DesignersDlg* dlg, designer::Designer* d) {
    DesignersDlg::SubDlg::construct(dlg, d);
    designer_ = dynamic_cast<designer::LensDesigner*>(d);
    // Create the controls
    tabs_ = new QTabWidget;
    phaseShifts_ = new LensDesignerPhaseShiftsDlg(this, designer_);
    results_ = new LensDesignerResultsDlg(this, designer_);
    columns_ = new LensDesignerColumnsDlg(this, designer_);
    tabs_->addTab(phaseShifts_, "Phase Shift");
    tabs_->addTab(results_, "Results");
    tabs_->addTab(columns_, "Columns");
    layout_->addWidget(tabs_);
    // Connect the handlers
    connect(tabs_, SIGNAL(tabBarClicked(int)), SLOT(onTabChange(int)));
    // Initialise the dialog
    initialise();
}

// Initialise the GUI parameters
void LensDesignerDlg::initialise() {
    InitialisingGui::Set s(dlg_->initialising());
    // Set parameters
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void LensDesignerDlg::tabChanged() {
    onTabChange(0);
}

// The current tab has changed
void LensDesignerDlg::onTabChange(int /*index*/) {
    auto* tab = dynamic_cast<SubDlg*>(tabs_->widget(tabs_->currentIndex()));
    if(tab != nullptr) {
        tab->tabChanged();
    }
}
