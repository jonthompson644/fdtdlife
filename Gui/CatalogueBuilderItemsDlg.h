/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUEBUILDERITEMSDLG_H
#define FDTDLIFE_CATALOGUEBUILDERITEMSDLG_H


#include "CatalogueBuilderSubDlg.h"
#include "DataSeriesWidget.h"
#include <memory>
#include <Notifiable.h>

class CatalogueBuilderDlg;

class BinaryCodedPlateWidget;
namespace fdtd { class CatalogueItem; }

// Main tab panel for the sequencers
class CatalogueBuilderItemsDlg : public CatalogueBuilderSubDlg, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit CatalogueBuilderItemsDlg(CatalogueBuilderDlg* dlg);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onItemSelect();
    void onCreatePdf();
    void onCopyItem();

protected:
    enum Columns {
        columnCoding = 0, columnPattern, numColumns
    };
    QTreeWidget* itemTree_ {};
    DataSeriesWidget* transmittanceView_ {};
    DataSeriesWidget* phaseShiftView_ {};
    QLineEdit* numBuiltEdit_ {};
    QLineEdit* numRequiredEdit_ {};
    QLineEdit* numUniqueEdit_ {};
    BinaryCodedPlateWidget* bitPatternEdit_ {};
    QPushButton* browseButton_ {};
    QPushButton* copyButton_ {};

protected:
    void fillItemList();
    void addItemToList(const std::shared_ptr<fdtd::CatalogueItem>& ind,
                       QTreeWidgetItem* item = nullptr);
    std::shared_ptr<fdtd::CatalogueItem> currentItem();
    void plotItemData(const std::shared_ptr<fdtd::CatalogueItem>& item);
};

#endif //FDTDLIFE_CATALOGUEBUILDERITEMSDLG_H
