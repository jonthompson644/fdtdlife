/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include "ViewFrameDlg.h"
#include "DialogHelper.h"
#include "ViewFrame.h"
#include "ViewItem.h"
#include "ViewItemDlg.h"
#include "GuiChangeDetector.h"
#include <Sensor/Sensor.h>

// Constructor
ViewFrameDlg::ViewFrameDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        viewItemDlg_(nullptr) {
    // The parameters
    auto paramLayout = new QHBoxLayout;
    auto buttonsLayout = new QVBoxLayout;
    auto rightLayout = new QVBoxLayout;
    paramLayout->addLayout(buttonsLayout);
    paramLayout->addLayout(rightLayout);
    setTabLayout(paramLayout);
    // The views buttons
    newButton_ = DialogHelper::buttonItem(buttonsLayout, "New View");
    deleteButton_ = DialogHelper::buttonItem(buttonsLayout, "Delete View");
    buttonsLayout->addStretch();
    // The views tree
    auto viewsLayout = new QHBoxLayout;
    rightLayout->addLayout(viewsLayout);
    viewsTree_ = new QTreeWidget;
    viewsTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    viewsTree_->setColumnCount(numViewCols);
    viewsTree_->setHeaderLabels({"Identifier", "Name"});
    viewsTree_->setFixedWidth(240);
    viewsLayout->addWidget(viewsTree_);
    // The view edit items
    auto itemLayout = new QVBoxLayout;
    viewsLayout->addLayout(itemLayout);
    nameEdit_ = DialogHelper::textItem(itemLayout, "Name");
    auto* videoLine = new QHBoxLayout;
    itemLayout->addLayout(videoLine);
    makeVideoCheck_ = DialogHelper::boolItem(videoLine, "Make Video");
    videoFilenameEdit_ = DialogHelper::textItem(videoLine);
    browseVideoFileButton_ = DialogHelper::buttonItem(videoLine, "Browse");
    closeVideoButton_ = DialogHelper::buttonItem(videoLine, "Close Video");
    auto* displayLine = new QHBoxLayout;
    itemLayout->addLayout(displayLine);
    frameDisplayPeriodEdit_ = DialogHelper::intItem(displayLine, "Min Display Rate (ms)");
    displayAllFramesCheck_ = DialogHelper::boolItem(displayLine, "Display All Frames");
    displayLine->addStretch();
    itemLayout->addStretch();
    // The Data streams check list
    auto itemsLayout = new QHBoxLayout;
    rightLayout->addLayout(itemsLayout);
    dataSpecsList_ = new QListWidget;
    itemsLayout->addWidget(dataSpecsList_);
    dataSpecsList_->setFixedWidth(440);
    // The place holder for the view item dialog
    viewItemDlgLayout_ = new QVBoxLayout;
    itemsLayout->addLayout(viewItemDlgLayout_);
    itemsLayout->addStretch();
    // Connect the handlers
    connect(viewsTree_, SIGNAL(itemSelectionChanged()), SLOT(onViewSelected()));
    connect(newButton_, SIGNAL(clicked()), SLOT(onNew()));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(nameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(makeVideoCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(displayAllFramesCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(videoFilenameEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frameDisplayPeriodEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(browseVideoFileButton_, SIGNAL(clicked()), SLOT(onVideoFilenameBrowse()));
    connect(closeVideoButton_, SIGNAL(clicked()), SLOT(onCloseVideo()));
    connect(dataSpecsList_, SIGNAL(itemChanged(QListWidgetItem*)), SLOT(onSensorChanged(QListWidgetItem*)));
    connect(dataSpecsList_, SIGNAL(itemSelectionChanged()), SLOT(onSensorSelected()));
    // Fill the dialog
    initialise();
}

// Fill the materials tree
void ViewFrameDlg::fillViewTree(ViewFrame* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        viewsTree_->clear();
        for(auto& view : m_->views()) {
            auto item = new QTreeWidgetItem(viewsTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(viewColIdentifier, QString::number(view->identifier()));
            item->setText(viewColName, view->name().c_str());
            if(selItem == nullptr || view == sel) {
                selItem = item;
            }
        }
    }
    if(selItem != nullptr) {
        viewsTree_->setCurrentItem(selItem);
    }
}

// Initialise the dialog
void ViewFrameDlg::initialise() {
    ViewFrame* view = getCurrentView();
    fillViewTree(view);
}

// Return the currently selected view
ViewFrame* ViewFrameDlg::getCurrentView() {
    ViewFrame* view = nullptr;
    QTreeWidgetItem* item = viewsTree_->currentItem();
    if(item != nullptr) {
        int identifier = item->text(viewColIdentifier).toInt();
        view = m_->getView(identifier);
    }
    return view;
}

// Fill the user interface using the selected view
void ViewFrameDlg::onViewSelected() {
    if(!initialising()) {
        QSignalBlocker nameBlock(nameEdit_);
        QSignalBlocker makeVideoBlock(makeVideoCheck_);
        QSignalBlocker videoFilenameBlock(videoFilenameEdit_);
        ViewFrame* view = getCurrentView();
        if(view != nullptr) {
            nameEdit_->setText(view->name().c_str());
            makeVideoCheck_->setChecked(view->makeVideo());
            videoFilenameEdit_->setText(view->videoFilename().c_str());
            displayAllFramesCheck_->setChecked(view->displayAllFrames());
            frameDisplayPeriodEdit_->setText(QString::number(view->frameDisplayPeriod()));
        }
        fillDataSpecList();
    }
}

// Add a new sensorId
void ViewFrameDlg::onNew() {
    tabChanged();  // Update anything that may have changed for the current item
    QString name;
    QTextStream n(&name);
    int identifier = m_->allocViewIdentifier();
    n << "View " << identifier;
    auto view = new ViewFrame(identifier, name.toStdString(), m_);
    fillViewTree(view);
    m_->doNotification(FdTdLife::notifyMinorChange);
}

// Delete a sensorId
void ViewFrameDlg::onDelete() {
    QMessageBox box;
    auto view = getCurrentView();
    QString msg;
    QTextStream t(&msg);
    t << "OK to delete the view " << view->name().c_str();
    box.setText(msg);
    box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    box.setDefaultButton(QMessageBox::Ok);
    if(box.exec() == QMessageBox::Ok) {
        delete view;
        initialise();
        m_->doNotification(FdTdLife::notifyMinorChange);
    }
}

// A view edit field has changed
void ViewFrameDlg::onChange() {
    auto view = getCurrentView();
    if(view != nullptr) {
        // Get the new values
        GuiChangeDetector c;
        auto newName = c.testString(nameEdit_, view->name(), FdTdLife::notifyMinorChange);
        auto newMakeVideo = c.testBool(makeVideoCheck_, view->makeVideo(), FdTdLife::notifyMinorChange);
        auto newVideoFilename = c.testString(videoFilenameEdit_, view->videoFilename(), FdTdLife::notifyMinorChange);
        auto newDisplayAllFrames = c.testBool(displayAllFramesCheck_, view->displayAllFrames(), FdTdLife::notifyMinorChange);
        auto newFrameDisplayPeriod = c.testInt(frameDisplayPeriodEdit_, view->frameDisplayPeriod(), FdTdLife::notifyMinorChange);
        // Make the changes
        if (c.isChanged()) {
            view->set(newName, newMakeVideo, newVideoFilename,
                      newDisplayAllFrames, newFrameDisplayPeriod);
            // Notifications
            viewsTree_->currentItem()->setText(viewColName, view->name().c_str());
            m_->doNotification(c.why());
        }
    }
}

// The close video button has been pressed
void ViewFrameDlg::onCloseVideo() {
    auto view = getCurrentView();
    if(view != nullptr) {
        view->closeVideo();
    }
}

// The video filename browse button has been pressed
void ViewFrameDlg::onVideoFilenameBrowse() {
    QString filename = QFileDialog::getSaveFileName(
            this, tr("Video output file name"), videoFilenameEdit_->text(),
            tr("MPEG Video Files (*.mpg)"));
    if(!filename.isEmpty()) {
        videoFilenameEdit_->setText(filename);
        onChange();
    }
}

// Fill the list view with the current data specifications
void ViewFrameDlg::fillDataSpecList() {
    QSignalBlocker blockSignals(dataSpecsList_);
    dataSpecsList_->clear();
    dataSpecs_.clear();
    auto view = getCurrentView();
    if(view != nullptr) {
        // Put all the specifications in the list
        for(auto& sensor : m_->sensors()) {
            for(auto spec : sensor->dataSpecs()) {
                QString name = QString("%1:%2").arg(spec->sensor()->name().c_str()).arg(spec->name().c_str());
                auto item = new QListWidgetItem(name, dataSpecsList_);
                dataSpecs_[item] = spec;
                auto viewItem = view->findViewItem(spec);
                if(viewItem != nullptr) {
                    item->setCheckState(Qt::Checked);
                } else {
                    item->setCheckState(Qt::Unchecked);
                }
            }
        }
    }
}

// The model has changed, we need to reload the sensorId list
void ViewFrameDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(source == m_) {
        switch(why) {
            case FdTdLife::notifySensorChange:
            case FdTdLife::notifyInitialisation:
                fillDataSpecList();
                break;
            default:
                break;
        }
    }
}

// The item's state has been changed
void ViewFrameDlg::onSensorChanged(QListWidgetItem* item) {
    auto view = getCurrentView();
    if(view != nullptr) {
        // Get the data specification
        auto found = dataSpecs_.find(item);
        if (found != dataSpecs_.end()) {
            sensor::DataSpec *spec = found->second;
            auto viewItem = view->findViewItem(spec);
            // Add or remove?
            if (item->checkState() == Qt::Unchecked && viewItem != nullptr) {
                // Remove the view item
                delete viewItem;
            } else if(item->checkState() == Qt::Checked && viewItem == nullptr) {
                // Create a view item
                view->createViewItem(spec);
            }
            view->initialise();
            view->update();
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void ViewFrameDlg::tabChanged() {
    onChange();
}

// Make a view item sub-dialog
void ViewFrameDlg::makeViewItemDlg(ViewItem* viewItem) {
    delete viewItemDlg_;
    viewItemDlg_ = nullptr;
    if(viewItem != nullptr) {
        viewItemDlg_ = viewItem->makeDialog(this);
        viewItemDlgLayout_->addWidget(viewItemDlg_);
    }
}

// The sensorId selection has changed
void ViewFrameDlg::onSensorSelected() {
    auto item = viewItem();
    makeViewItemDlg(item);
    std::cout << "Sensor selection changed" << std::endl;
}

// Return the current view item
ViewItem *ViewFrameDlg::viewItem() {
    ViewItem* result=nullptr;
    // Get the currently selected sensorId
    auto selectedItems = dataSpecsList_->selectedItems();
    if(selectedItems.size() == 1) {
        // Get the current view
        auto view = getCurrentView();
        if(view != nullptr) {
            // Get the selected data specification
            auto found = dataSpecs_.find(selectedItems.front());
            if (found != dataSpecs_.end()) {
                sensor::DataSpec *spec = found->second;
                result = view->findViewItem(spec);
            }
        }
    }
    return result;
}
