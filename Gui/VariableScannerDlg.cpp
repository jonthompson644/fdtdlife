/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableScannerDlg.h"
#include <Seq/VariableScanner.h>
#include "DialogHelper.h"
#include "CharacterizerParamsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
VariableScannerDlg::VariableScannerDlg(CharacterizerParamsDlg* dlg,
                                       seq::VariableScanner* scanner) :
        ScannerDlg(dlg, scanner),
        Notifiable(dlg->model()),
        scanner_(scanner) {
    // My controls
    auto* line2Layout = DialogHelper::lineLayout(layout_);
    fromEdit_ = DialogHelper::doubleItem(line2Layout, "From");
    toEdit_ = DialogHelper::doubleItem(line2Layout, "To (inclusive)");
    variablesList_ = DialogHelper::dropListItem(layout_, "Variable");
    variablesList_->setMinimumWidth(200);
    // Connect handlers
    connect(fromEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(toEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(variablesList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    initialise();
}

// Initialise the GUI parameters
void VariableScannerDlg::initialise() {
    ScannerDlg::initialise();
    InitialisingGui::Set s(initialising_);
    fromEdit_->setText(QString::number(scanner_->from()));
    toEdit_->setText(QString::number(scanner_->to()));
    dlg_->model()->fillVariableList(variablesList_, scanner_->variableId());
    update();
}

// An edit box has changed
void VariableScannerDlg::onChange() {
    ScannerDlg::onChange();
    if(!initialising_) {
        GuiChangeDetector c;
        auto newFrom = c.testDouble(fromEdit_, scanner_->from(),
                                    FdTdLife::notifySequencerChange);
        auto newTo = c.testDouble(toEdit_, scanner_->to(),
                                  FdTdLife::notifySequencerChange);
        int newVariableId = dlg_->model()->selected(variablesList_);
        c.externalTest(newVariableId != scanner_->variableId(),
                       FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            scanner_->from(newFrom);
            scanner_->to(newTo);
            scanner_->variableId(newVariableId);
            // Tell others
            dlg_->model()->doNotification(c.why());
            update();
        }
    }
}

// A change notification has been received
void VariableScannerDlg::notify(fdtd::Notifier* source, int why,
        fdtd::NotificationData* /*data*/) {
    if(source == dlg_->model() && why == fdtd::Model::notifyVariableChange) {
        dlg_->model()->fillVariableList(variablesList_, scanner_->variableId());
    }
}
