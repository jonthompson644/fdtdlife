/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DIELECTRICLAYERSCANNERDLG_H
#define FDTDLIFE_DIELECTRICLAYERSCANNERDLG_H

#include "QtIncludes.h"
#include "ScannerDlg.h"

namespace seq { class DielectricLayerScanner; }

// Sub-dialog for the NxN Binary Plate scanner
class DielectricLayerScannerDlg : public ScannerDlg {
Q_OBJECT
public:
    DielectricLayerScannerDlg(CharacterizerParamsDlg* dlg, seq::DielectricLayerScanner* scanner);
    void initialise() override;

protected slots:
    void onChange() override;

protected:
    seq::DielectricLayerScanner* scanner_;
    QLineEdit* thicknessStepSizeEdit_;
    QLineEdit* positionXEdit_;
    QLineEdit* positionYEdit_;
    QLineEdit* positionZEdit_;
    QLineEdit* cellSizeEdit_;
    QLineEdit* repeatXEdit_;
    QLineEdit* repeatYEdit_;
    QComboBox* materialList_;

protected:

};

#endif //FDTDLIFE_DIELECTRICLAYERSCANNERDLG_H
