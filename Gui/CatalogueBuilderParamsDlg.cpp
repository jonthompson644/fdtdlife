/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueBuilderParamsDlg.h"
#include "CatalogueBuilderDlg.h"
#include "SequencersDlg.h"
#include "DialogHelper.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <CatalogueBuilder.h>
#include <Model.h>

// Constructor
CatalogueBuilderParamsDlg::CatalogueBuilderParamsDlg(CatalogueBuilderDlg* dlg) :
        CatalogueBuilderSubDlg(dlg),
        Notifiable(dlg->dlg()->model()) {
    // The parameters
    auto paramLayout = new QVBoxLayout;
    // First row of config
    auto row0Layout = new QHBoxLayout;
    bitsPerHalfSideEdit_ = DialogHelper::intItem(row0Layout, "Bits Per Half Side");
    fourFoldSymmetryCheck_ = DialogHelper::boolItem(row0Layout, "Four Fold Symmetry");
    thinSheetSubcellsCheck_ = DialogHelper::boolItem(row0Layout, "Thin Sheet Subcells");
    inclCornerToCornerCheck_ = DialogHelper::boolItem(row0Layout,
                                                      "Include corner-to-corner");
    row0Layout->addStretch();
    paramLayout->addLayout(row0Layout);
    // Second row of config
    auto row1Layout = new QHBoxLayout;
    frequencyRangeEdit_ = DialogHelper::doubleItem(row1Layout, "Freq Range (Hz)");
    numFrequenciesEdit_ = DialogHelper::intItem(row1Layout, "Num Frequencies");
    paramLayout->addLayout(row1Layout);
    // Third row of config
    auto row2Layout = new QHBoxLayout;
    itemsPerBlockEdit_ = DialogHelper::doubleItem(row2Layout, "Items Per Block");
    paramLayout->addLayout(row2Layout);
    // Fourth row of config
    auto row3Layout = new QHBoxLayout;
    cellsPerPixelEdit_ = DialogHelper::intItem(row3Layout, "Cells Per Pixel");
    sensorDistanceEdit_ = DialogHelper::doubleItem(row3Layout, "Sensor Distance (m)");
    unitCellEdit_ = DialogHelper::doubleItem(row3Layout, "Unit Cell Size (m)");
    paramLayout->addLayout(row3Layout);
    // Set the layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(fourFoldSymmetryCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(thinSheetSubcellsCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(inclCornerToCornerCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(bitsPerHalfSideEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(frequencyRangeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(numFrequenciesEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(itemsPerBlockEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellsPerPixelEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sensorDistanceEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(unitCellEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    // Fill the panel
    CatalogueBuilderParamsDlg::initialise();
}

// Initialise the panel from the configuration
void CatalogueBuilderParamsDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fourFoldSymmetryCheck_->setChecked(dlg_->sequencer()->fourFoldSymmetry());
    thinSheetSubcellsCheck_->setChecked(dlg_->sequencer()->thinSheetSubcells());
    inclCornerToCornerCheck_->setChecked(dlg_->sequencer()->inclCornerToCorner());
    bitsPerHalfSideEdit_->setText(QString::number(dlg_->sequencer()->bitsPerHalfSide()));
    frequencyRangeEdit_->setText(QString::number(dlg_->sequencer()->frequencyRange()));
    numFrequenciesEdit_->setText(QString::number(dlg_->sequencer()->numFrequencies()));
    itemsPerBlockEdit_->setText(QString::number(dlg_->sequencer()->itemsPerBlock()));
    cellsPerPixelEdit_->setText(QString::number(dlg_->sequencer()->cellsPerPixel()));
    sensorDistanceEdit_->setText(QString::number(dlg_->sequencer()->sensorDistance()));
    unitCellEdit_->setText(QString::number(dlg_->sequencer()->unitCell()));
}

// An element has changed
void CatalogueBuilderParamsDlg::onChange() {
    if(!initialising_) {
        // New values
        GuiChangeDetector c;
        auto newBitsPerHalfSide = c.testSizeT(bitsPerHalfSideEdit_,
                                              dlg_->sequencer()->bitsPerHalfSide(),
                                              FdTdLife::notifySequencerChange);
        auto newFourFoldSymmetry = c.testBool(fourFoldSymmetryCheck_,
                                              dlg_->sequencer()->fourFoldSymmetry(),
                                              FdTdLife::notifySequencerChange);
        auto newThinSheetSubcells = c.testBool(thinSheetSubcellsCheck_,
                                               dlg_->sequencer()->thinSheetSubcells(),
                                               FdTdLife::notifySequencerChange);
        auto newInclCornerToCorner = c.testBool(inclCornerToCornerCheck_,
                                                dlg_->sequencer()->inclCornerToCorner(),
                                                FdTdLife::notifySequencerChange);
        auto newFrequencyRange = c.testDouble(frequencyRangeEdit_,
                                              dlg_->sequencer()->frequencyRange(),
                                              FdTdLife::notifySequencerChange);
        auto newNumFrequencies = c.testInt(numFrequenciesEdit_,
                                           dlg_->sequencer()->numFrequencies(),
                                           FdTdLife::notifySequencerChange);
        auto newItemsPerBlock = c.testInt(itemsPerBlockEdit_,
                                          dlg_->sequencer()->itemsPerBlock(),
                                          FdTdLife::notifySequencerChange);
        auto newCellsPerPixel = c.testInt(cellsPerPixelEdit_,
                                          dlg_->sequencer()->cellsPerPixel(),
                                          FdTdLife::notifySequencerChange);
        auto newSensorDistance = c.testDouble(sensorDistanceEdit_,
                                              dlg_->sequencer()->sensorDistance(),
                                              FdTdLife::notifySequencerChange);
        auto newUnitCell = c.testDouble(unitCellEdit_,
                                        dlg_->sequencer()->unitCell(),
                                        FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            dlg_->sequencer()->bitsPerHalfSide(newBitsPerHalfSide);
            dlg_->sequencer()->fourFoldSymmetry(newFourFoldSymmetry);
            dlg_->sequencer()->inclCornerToCorner(newInclCornerToCorner);
            dlg_->sequencer()->thinSheetSubcells(newThinSheetSubcells);
            dlg_->sequencer()->frequencyRange(newFrequencyRange);
            dlg_->sequencer()->numFrequencies(newNumFrequencies);
            dlg_->sequencer()->itemsPerBlock(newItemsPerBlock);
            dlg_->sequencer()->cellsPerPixel(newCellsPerPixel);
            dlg_->sequencer()->sensorDistance(newSensorDistance);
            dlg_->sequencer()->unitCell(newUnitCell);
            // Tell others
            dlg_->dlg()->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CatalogueBuilderParamsDlg::tabChanged() {
    onChange();
}

// Handle a configuration change notification
void CatalogueBuilderParamsDlg::notify(fdtd::Notifier* source, int why,
                                       fdtd::NotificationData* /*data*/) {
    if(source == dlg_->dlg()->model()) {
        if(why == FdTdLife::notifyDomainContentsChange) {
            if(!initialising_) {
                initialise();
            }
        }
    }
}
