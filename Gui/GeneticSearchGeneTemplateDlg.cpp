/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneticSearchGeneTemplateDlg.h"
#include "GeneticSearchSeqDlg.h"
#include "SequencersDlg.h"
#include "DialogHelper.h"
#include "PushButtonMenu.h"
#include "SquarePlateGeneTemplateDlg.h"
#include "DielectricLayerGeneTemplateDlg.h"
#include "LayerSpacingGeneTemplateDlg.h"
#include "AdmittanceCatLayerGeneTemplateDlg.h"
#include "RepeatGeneTemplateDlg.h"
#include "LayerPresentGeneTemplateDlg.h"
#include "LayerSymmetryGeneTemplateDlg.h"
#include "BinaryPlateNxNGeneTemplateDlg.h"
#include "DuplicateLayerGeneTemplateDlg.h"
#include "AdmittanceFnLayerGeneTemplateDlg.h"
#include "DielectricBlockGeneTemplateDlg.h"
#include "LensDimensionsGeneTemplateDlg.h"
#include "LensSquareGeneTemplateDlg.h"
#include "VariableGeneTemplateDlg.h"
#include <Gs/GeneticSearch.h>

// Constructor
GeneticSearchGeneTemplateDlg::GeneticSearchGeneTemplateDlg(GeneticSearchSeqDlg* dlg) :
        GeneticSearchSubDlg(dlg),
        geneTemplateDlg_(nullptr) {
    auto templateLayout = new QHBoxLayout;
    // The gene template
    auto geneTemplateLayout = new QVBoxLayout;
    templateLayout->addLayout(geneTemplateLayout);
    geneTemplateTree_ = new QTreeWidget;
    geneTemplateTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    geneTemplateTree_->setColumnCount(numGeneTemplateCols);
    geneTemplateTree_->setHeaderLabels({"Identifier", "Type"});
    geneTemplateTree_->setFixedWidth(250);
    geneTemplateTree_->setDragEnabled(true);
    geneTemplateTree_->viewport()->setAcceptDrops(true);
    geneTemplateTree_->setDropIndicatorShown(true);
    geneTemplateTree_->setDragDropMode(QAbstractItemView::InternalMove);
    geneTemplateLayout->addWidget(geneTemplateTree_);
    newGeneTemplate_ = DialogHelper::buttonMenuItem(
            geneTemplateLayout, "New Gene Template",
            {"Repeat Cell", "Layer Spacing", "Layer Present",
             "Square Plate", "Dielectric Layer",
             "Admittance Cat Layer", "Layer Symmetry", "Binary Plate NxN",
             "Duplicate Layer", "Admittance Fn Layer", "Dielectric Block",
             "Lens Dimensions", "Transverse Lens Square Size",
             "Longitudinal Lens Square Size", "Variable"});
    deleteGeneTemplateButton_ = DialogHelper::buttonItem(geneTemplateLayout,
                                                         "Delete Template");
    duplicateGeneTemplateButton_ = DialogHelper::buttonItem(geneTemplateLayout,
                                                            "Duplicate Template");
    // The sub-dialog place holder
    geneTemplateDlgLayout_ = new QHBoxLayout;
    templateLayout->addLayout(geneTemplateDlgLayout_);
    templateLayout->addStretch();
    // Set the layout
    setTabLayout(templateLayout);
    // Connect the handlers
    connect(geneTemplateTree_, SIGNAL(itemSelectionChanged()),
            SLOT(onGeneTemplateSelected()));
    connect(geneTemplateTree_->model(), SIGNAL(rowsInserted(
                                                       const QModelIndex &, int, int)),
            SLOT(onGeneTemplatedInserted(
                         const QModelIndex &, int, int)));
    connect(newGeneTemplate_, SIGNAL(clickedMenu(int)), SLOT(onNewGeneTemplate(int)));
    connect(deleteGeneTemplateButton_, SIGNAL(clicked()), SLOT(onDeleteGeneTemplate()));
    connect(duplicateGeneTemplateButton_, SIGNAL(clicked()), SLOT(onDuplicateGeneTemplate()));
    // Fill the panel
    initialise();
}

// Initialise the panel from the configuration
void GeneticSearchGeneTemplateDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    fillGeneTemplateTree();
    initialiseGeneTemplate();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void GeneticSearchGeneTemplateDlg::tabChanged() {
    if(geneTemplateDlg_ != nullptr) {
        geneTemplateDlg_->tabChanged();
    }
}

// Get the current gene template.
gs::GeneTemplate* GeneticSearchGeneTemplateDlg::currentGeneTemplate() {
    gs::GeneTemplate* result = nullptr;
    auto item = geneTemplateTree_->currentItem();
    if(item != nullptr) {
        int identifier = geneTemplateTree_->currentItem()->text(
                geneTemplateColPosition).toInt();
        result = dlg_->sequencer()->geneFactory()->getTemplate(identifier);
    }
    return result;
}

// Put all the gene templates in the gene template tree
void GeneticSearchGeneTemplateDlg::fillGeneTemplateTree(gs::GeneTemplate* sel) {
    geneTemplateTree_->clear();
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        for(auto& geneTemplate : dlg_->sequencer()->geneFactory()->templates()) {
            auto item = new QTreeWidgetItem(geneTemplateTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
            item->setText(geneTemplateColPosition,
                          QString::number(geneTemplate->identifier()));
            item->setText(geneTemplateColType, geneTemplate->geneTypeText());
            if(geneTemplate == sel) {
                selItem = item;
            }
        }
    }
    if(selItem != nullptr) {
        geneTemplateTree_->setCurrentItem(selItem);
    } else if(!dlg_->sequencer()->geneFactory()->templates().empty()) {
        geneTemplateTree_->setCurrentItem(geneTemplateTree_->topLevelItem(0));
    }
}

// Initialise the dialog with the current gene template
void GeneticSearchGeneTemplateDlg::initialiseGeneTemplate() {
    // Delete any old dialog
    delete geneTemplateDlg_;
    geneTemplateDlg_ = nullptr;
    // Get the template
    gs::GeneTemplate* geneTemplate = currentGeneTemplate();
    // Make dialog for the shape
    if(geneTemplate != nullptr) {
        switch(geneTemplate->geneType()) {
            case gs::GeneTemplate::geneTypeRepeatCell:
                geneTemplateDlg_ = new RepeatGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeSquarePlate:
                geneTemplateDlg_ = new SquarePlateGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeDielectricLayer:
                geneTemplateDlg_ = new DielectricLayerGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeLayerSpacing:
                geneTemplateDlg_ = new LayerSpacingGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeLayerPresent:
                geneTemplateDlg_ = new LayerPresentGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeLayerSymmetry:
                geneTemplateDlg_ = new LayerSymmetryGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeAdmittanceCatLayer:
                geneTemplateDlg_ = new AdmittanceCatLayerGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeBinaryPlateNxN:
                geneTemplateDlg_ = new BinaryPlateNxNGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeDuplicateLayer:
                geneTemplateDlg_ = new DuplicateLayerGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeAdmittanceFn:
                geneTemplateDlg_ = new AdmittanceFnLayerGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeDielectricBlock:
                geneTemplateDlg_ = new DielectricBlockGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeLensDimensions:
                geneTemplateDlg_ = new LensDimensionsGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeTransLensSquare:
                geneTemplateDlg_ = new LensSquareGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeLongLensSquare:
                geneTemplateDlg_ = new LensSquareGeneTemplateDlg(this, geneTemplate);
                break;
            case gs::GeneTemplate::geneTypeVariable:
                geneTemplateDlg_ = new VariableGeneTemplateDlg(this, geneTemplate);
                break;
            default:
                break;
        }
        if(geneTemplateDlg_ != nullptr) {
            geneTemplateDlg_->initialise();
            geneTemplateDlgLayout_->addWidget(geneTemplateDlg_);
        }
    }
}

// Initialise the dialog with the current gene template
void GeneticSearchGeneTemplateDlg::onGeneTemplateSelected() {
    if(!initialising_) {
        initialiseGeneTemplate();
    }
}

// Create a new gene template
void GeneticSearchGeneTemplateDlg::onNewGeneTemplate(int templateType) {
    gs::GeneTemplate::GeneType geneType;
    switch(templateType) {
        default:
        case geneTemplateRepeatCell:
            geneType = gs::GeneTemplate::geneTypeRepeatCell;
            break;
        case geneTemplateLayerSpacing:
            geneType = gs::GeneTemplate::geneTypeLayerSpacing;
            break;
        case geneTemplateLayerPresent:
            geneType = gs::GeneTemplate::geneTypeLayerPresent;
            break;
        case geneTemplateLayerSymmetry:
            geneType = gs::GeneTemplate::geneTypeLayerSymmetry;
            break;
        case geneTemplateSquarePlate:
            geneType = gs::GeneTemplate::geneTypeSquarePlate;
            break;
        case geneTemplateDielectricLayer:
            geneType = gs::GeneTemplate::geneTypeDielectricLayer;
            break;
        case geneTemplateAdmittanceLayer:
            geneType = gs::GeneTemplate::geneTypeAdmittanceCatLayer;
            break;
        case geneTemplateBinaryPlateNxN:
            geneType = gs::GeneTemplate::geneTypeBinaryPlateNxN;
            break;
        case geneTemplateDuplicateLayer:
            geneType = gs::GeneTemplate::geneTypeDuplicateLayer;
            break;
        case geneTemplateAdmittanceFnLayer:
            geneType = gs::GeneTemplate::geneTypeAdmittanceFn;
            break;
        case geneTemplateDielectricBlock:
            geneType = gs::GeneTemplate::geneTypeDielectricBlock;
            break;
        case geneTemplateLensDimensions:
            geneType = gs::GeneTemplate::geneTypeLensDimensions;
            break;
        case geneTemplateTransLensSquare:
            geneType = gs::GeneTemplate::geneTypeTransLensSquare;
            break;
        case geneTemplateLongLensSquare:
            geneType = gs::GeneTemplate::geneTypeLongLensSquare;
            break;
        case geneTemplateVariable:
            geneType = gs::GeneTemplate::geneTypeVariable;
            break;
    }
    int identifier = dlg_->sequencer()->geneFactory()->allocIdentifier();
    auto t = new gs::GeneTemplate(identifier, dlg_->sequencer()->geneFactory(), geneType);
    dlg_->sequencer()->geneTemplateAdded(t);
    fillGeneTemplateTree(t);
}

// Delete the current gene template
void GeneticSearchGeneTemplateDlg::onDeleteGeneTemplate() {
    auto currentSel = currentGeneTemplate();
    if(currentSel != nullptr) {
        dlg_->sequencer()->geneTemplateRemoved(currentSel);
        delete currentSel;
        fillGeneTemplateTree();
    }
}

// Duplicate the current gene template
void GeneticSearchGeneTemplateDlg::onDuplicateGeneTemplate() {
    auto currentSel = currentGeneTemplate();
    if(currentSel != nullptr) {
        int identifier = dlg_->sequencer()->geneFactory()->allocIdentifier();
        auto t = new gs::GeneTemplate(identifier, dlg_->sequencer()->geneFactory(),
                                      currentSel->geneType());
        t->copyFrom(currentSel);
        dlg_->sequencer()->geneTemplateAdded(t);
        fillGeneTemplateTree(t);
    }
}

// A gene template has been inserted
void GeneticSearchGeneTemplateDlg::onGeneTemplatedInserted(const QModelIndex&/*parent*/,
                                                           int /*start*/, int /*end*/) {
    if(!initialising_) {
        // Due to the careful use of the initialising object, we should only
        // get here when the drag/drop internal move operation has happened.
        // Use the tree order to re-order the gene templates
        std::list<gs::GeneTemplate*> newTemplates;
        for(int i = 0; i < geneTemplateTree_->topLevelItemCount(); i++) {
            auto item = geneTemplateTree_->topLevelItem(i);
            int identifier = item->text(geneTemplateColPosition).toInt();
            newTemplates.push_back(dlg_->sequencer()->geneFactory()->getTemplate(identifier));
        }
        dlg_->sequencer()->geneFactory()->setTemplates(newTemplates);
        fillGeneTemplateTree();
    }
}

