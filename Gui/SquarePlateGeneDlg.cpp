/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlateGeneDlg.h"
#include "DialogHelper.h"
#include <Gs/SquarePlateGene.h>

// Constructor
SquarePlateGeneDlg::SquarePlateGeneDlg(IndividualEditDlg *mainDlg, gs::SquarePlateGene *gene) :
        GeneDlg(mainDlg),
        gene_(gene) {
    auto paramLayout = new QVBoxLayout;
    // Make the elements
    repeatCountEdit_ = DialogHelper::sizeTItem(paramLayout, "Repeat Count", gene_->repeat());
    plateSizeEdit_ = DialogHelper::doubleItem(paramLayout, "Plate Size (%)", gene_->plateSize());
    holeCheck_ = DialogHelper::boolItem(paramLayout, "Hole");
    holeCheck_->setChecked(gene->hole());
    paramLayout->addStretch();
    setLayout(paramLayout);
}

// Update the individual from the dialog contents
void SquarePlateGeneDlg::updateIndividual() {
    gene_->repeat(repeatCountEdit_->text().toULong());
    gene_->plateSize(plateSizeEdit_->text().toDouble());
    gene_->hole(holeCheck_->isChecked());
}
