/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GuiConfiguration.h"
#include <Xml/DomObject.h>

// Constructor
GuiConfiguration::GuiConfiguration(fdtd::Configuration* m) :
        m_(m) {
    clear();
}

// Return the color for the specified element type
QColor GuiConfiguration::color(Element e) {
    QColor result(Qt::black);
    switch(e) {
        case elementAxis:
            if(whiteBackground_) {
                result = QColor(240,240,240);
            } else {
                result = QColor(Qt::darkBlue);
            }
            break;
        case elementText:
            if(whiteBackground_) {
                result = QColor(Qt::black);
            } else {
                result = QColor(Qt::darkGray);
            }
            break;
        case elementDataA:
            result = QColor(0,200,0);
            break;
        case elementDataB:
            result = QColor(200,0,0);
            break;
        case elementDataC:
            result = QColor(0,200,200);
            break;
        case elementDataD:
            result = QColor(200,200,0);
            break;
        case elementBackground:
            if(whiteBackground_) {
                result = QColor(Qt::white);
            } else {
                result = QColor(Qt::black);
            }
            break;
        case elementBorder:
            result = QColor(Qt::black);
            break;
        case elementMarkerA:
            result = QColor(0,255,0);
            break;
        case elementMarkerB:
            result = QColor(255,0,0);
            break;
        case elementMarkerC:
            result = QColor(0,255,255);
            break;
        case elementMarkerD:
            result = QColor(255,255,0);
            break;
    }
    return result;
}

// Set the parameters
void GuiConfiguration::set(bool whiteBackground) {
    whiteBackground_ = whiteBackground;
}

// Write GUI configuration to the DOM
void GuiConfiguration::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("whitebackground") << whiteBackground_;
    root << xml::Close();
}

// Read GUI configuration from the DOM
void GuiConfiguration::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("whitebackground") >> xml::Default(whiteBackground_) >> whiteBackground_;
    root >> xml::Close();
}

// Clear the configuration
void GuiConfiguration::clear() {
    whiteBackground_ = false;
}
