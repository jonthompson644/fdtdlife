/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ZONESOURCEDLG_H
#define FDTDLIFE_ZONESOURCEDLG_H

#include "QtIncludes.h"
#include "SourceDlg.h"
namespace source {class ZoneSource;}

// Edit dialog for the zone source
class ZoneSourceDlg : public SourceDlg {
Q_OBJECT
public:
    // Construction
    ZoneSourceDlg(SourcesDlg* dlg, source::ZoneSource* source);

    // API
    void initialise() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    // Handlers
    void onChange() override;

protected:
    // Widgets
    QLineEdit* firstFrequencyEdit_ {};
    QLineEdit* frequencyStepEdit_ {};
    QLineEdit* numFrequenciesEdit_ {};
    QLineEdit* amplitudeEdit_ {};
    QLineEdit* azimuthEdit_ {};
    QLineEdit* elevationEdit_ {};
    QLineEdit* polarisationEdit_ {};
    QCheckBox* continuousCheck_ {};
    QLineEdit* timeEdit_ {};
    QLineEdit* initialTimeEdit_ {};
    QLineEdit* xCenterEdit_ {};
    QLineEdit* yCenterEdit_ {};
    QLineEdit* zCenterEdit_ {};
    QLineEdit* xSizeEdit_ {};
    QLineEdit* ySizeEdit_ {};
    QLineEdit* zSizeEdit_ {};

    // Members
    source::ZoneSource* source_;
};


#endif //FDTDLIFE_ZONESOURCEDLG_H
