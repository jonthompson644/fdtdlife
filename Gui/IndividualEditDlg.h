/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_INDIVIDUALEDITDLG_H
#define FDTDLIFE_INDIVIDUALEDITDLG_H

#include "QtIncludes.h"
#include "InitialisingGui.h"
#include <memory>

namespace gs { class Individual; }
namespace gs { class GeneticSearch; }

class GeneDlg;

class IndividualEditDlg : public QDialog {
Q_OBJECT
public:
    enum {
        geneColIdentifier = 0, geneColType, geneColInfo, numColumns
    };
    enum {
        fitnessColPosition = 0, fitnessColType, fitnessColResult,
        fitnessColExtraInfo, fitnessColExtraValue, numFitnessCols
    };
public:
    IndividualEditDlg(QWidget* parent, gs::GeneticSearch* s, const std::shared_ptr<gs::Individual>& ind);
    gs::GeneticSearch* search() { return s_; }
    InitialisingGui& initialising() { return initialising_; }
protected:
    std::shared_ptr<gs::Individual> ind_;
    QDialogButtonBox* buttons_ {};
    QTreeWidget* geneTemplateTree_ {};
    QTreeWidget* fitnessResultsTree_ {};
    gs::GeneticSearch* s_ {};
    InitialisingGui initialising_ {};
    QStackedWidget* geneDlgArea_ {};
    QComboBox* stateCombo_ {};
    QLineEdit* unfitnessEdit_ {};
    QTextEdit* chromosomeEdit_ {};
protected:
    void fillFitnessResults();
protected slots:
    void geneSelected();
    void accept() override;
};


#endif //FDTDLIFE_INDIVIDUALEDITDLG_H
