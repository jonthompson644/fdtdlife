/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ArcMaterialDlg.h"
#include "DialogHelper.h"
#include "InitialisingGui.h"
#include "MaterialsDlg.h"
#include <Domain/ArcMaterial.h>
#include "FdTdLife.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void ArcMaterialDlg::construct(MaterialsDlg* dlg, domain::Material* mat) {
    MaterialDlg::construct(dlg, mat);
    mat_ = dynamic_cast<domain::ArcMaterial*>(mat);
    epsilonEdit_->setReadOnly(true);
    relativeEpsilonEdit_->setReadOnly(true);
    muEdit_->setReadOnly(true);
    relativeMuEdit_->setReadOnly(true);
    sigmaEdit_->setReadOnly(true);
    sigmastarEdit_->setReadOnly(true);
    materialList_ = DialogHelper::dropListItem(materialLayout_, "Material");
    connect(materialList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void ArcMaterialDlg::initialise() {
    MaterialDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    dlg_->model()->fillMaterialList(materialList_, mat_->otherMaterialId());
}

// A parameter has changed
void ArcMaterialDlg::onChange() {
    MaterialDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        int newMaterialIndex = dlg_->model()->selected(materialList_);
        c.externalTest(newMaterialIndex != mat_->otherMaterialId(), FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if (c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            mat_->otherMaterialId(newMaterialIndex);
            dlg_->model()->doNotification(c.why());
        }
    }
}
