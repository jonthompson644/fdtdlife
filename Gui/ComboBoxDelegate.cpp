/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ComboBoxDelegate.h"

// Constructor
ComboBoxDelegate::ComboBoxDelegate(const std::list<std::string>& strings, QObject *parent)
        : QStyledItemDelegate(parent) {
    for(auto& str : strings) {
        items_.emplace_back(str.c_str());
    }
}

// Constructor
ComboBoxDelegate::ComboBoxDelegate(const std::vector<const char*>& strings, QObject *parent)
        : QStyledItemDelegate(parent) {
    for(auto& str : strings) {
        items_.emplace_back(str);
    }
}

// Creates the editor widget
QWidget *ComboBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */,
                                        const QModelIndex &/* index */) const {
    auto editor = new QComboBox(parent);
    for(auto& item : items_) {
        editor->addItem(item);
    }
    return editor;
}

// Sets the editor widget with current data
void ComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    auto comboBox = dynamic_cast<QComboBox*>(editor);
    int value = index.model()->data(index, Qt::EditRole).toUInt();
    comboBox->setCurrentIndex(value);
    connect(editor, SIGNAL(currentIndexChanged(int)), SLOT(onIndexChanged()));
}

// Reads the edit widget data
void ComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const {
    auto comboBox = dynamic_cast<QComboBox*>(editor);
    model->setData(index, comboBox->currentIndex(), Qt::EditRole);
}

// Sets the size of editor widget
void ComboBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                            const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

// Paints the edit widget
void ComboBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                             const QModelIndex &index) const {
    QStyleOptionViewItem myOption = option;
    int i = index.data(Qt::EditRole).toInt();
    myOption.text = items_[i];
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}

// The selection has changed
void ComboBoxDelegate::onIndexChanged() {
    emit commitData(dynamic_cast<QWidget*>(sender()));
    emit closeEditor(dynamic_cast<QWidget*>(sender()));
}
