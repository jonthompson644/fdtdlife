#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DATASERIESWIDGET_H
#define FDTDLIFE_DATASERIESWIDGET_H

#include "QtIncludes.h"
#include "GuiConfiguration.h"
#include <complex>

class DataSeriesWidget : public QWidget {
Q_OBJECT
public:
    class Data {
    public:
        Data(std::vector<double> d, GuiConfiguration::Element e) :
                data_(std::move(d)),
                element_(e) {}
        std::vector<double> data_;
        GuiConfiguration::Element element_;
    };

    explicit DataSeriesWidget(GuiConfiguration* cfg);
    QSize sizeHint() const override;
    void paintEvent(QPaintEvent* event) override;
    void contextMenuEvent(QContextMenuEvent* event) override;
    void setData(const std::vector<double>& a, GuiConfiguration::Element element,
                 bool clearFirst = false);
    void setData(const std::vector<std::complex<double>>& a,
                 GuiConfiguration::Element realElement,
                 GuiConfiguration::Element imagElement,
                 bool clearFirst = false);
    void setAxisInfo(double minX, double stepX, const char* unitsX,
                     double minY, double maxY, const char* unitsY);
    void clear();
protected:
    bool calcScaling();
    void drawVerticalAxis(QPainter* painter);
    void drawHorizontalAxis(QPainter* painter);
    void drawData(QPainter* painter);
protected:
    std::vector<Data> dataY_;
    double minY_{};
    double maxY_{};
    QString unitsY_;
    double scaleY_{};
    double tickStepY_{};
    double minX_{};
    double stepX_{};
    double scaleX_{};
    QString unitsX_;
    double tickStepX_{};
    GuiConfiguration* cfg_{};
};


#endif //FDTDLIFE_DATASERIESWIDGET_H
