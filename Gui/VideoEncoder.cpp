/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <QPainter>
#include <iostream>
#include "VideoEncoder.h"
#if defined(__USEFFMPEG__)
extern "C" {
#include "libavutil/imgutils.h"
};
#endif

#if defined(__USEFFMPEG__)
// The constant end code
const uint8_t VideoEncoder::endcode[] = { 0,0,1,0xb7 };
#endif

// Constructor
VideoEncoder::VideoEncoder()
#if defined(__USEFFMPEG__)
    :
        codec(nullptr), c(nullptr), f(nullptr), picture(nullptr),
        pkt(nullptr), swsContext(nullptr), width(0), height(0), frameNumber(0)
#endif
{
}

// Destructor
VideoEncoder::~VideoEncoder() {
    closeFile();
}

// Open the video file and prepare it for writing.
bool VideoEncoder::openFile(const QString& filename, int width, int height,
                            int framesPerSecond) {
    bool result = true;
    // Validate the parameters
    // The width and height must be a multiple of two
    if((width & 1) != 0 || (height & 1) != 0) {
        result = false;
    }
    // Width and height are reasonable
    if(width<1 || width>10000 || height<1 || height>10000) {
        result = false;
    }
    // Filename must not be blank
    if(filename.isEmpty()) {
        result = false;
    }
    // Is the frame rate reasonable
    if(framesPerSecond < 1 || framesPerSecond > 200) {
        result = false;
    }

#if defined(__USEFFMPEG__)
    // Prepare the codec
    if(result) {
        avcodec_register_all();
        codec = avcodec_find_encoder(AV_CODEC_ID_MPEG1VIDEO);
        result = codec != nullptr;
    }
    if(result) {
        c = avcodec_alloc_context3(codec);
        result = c != nullptr;
    }
    if(result) {
        c->bit_rate = width * height * framesPerSecond;
        c->width = width;
        c->height = height;
        c->time_base = (AVRational){1, framesPerSecond};
        c->framerate = (AVRational){framesPerSecond, 1};
        c->gop_size = 10;
        c->max_b_frames = 1;
        c->pix_fmt = AV_PIX_FMT_YUV420P;
        result = avcodec_open2(c, codec, nullptr) == 0;
    }

    // Prepare the frame buffer
    if(result) {
        picture = av_frame_alloc();
        result = picture != nullptr;
    }
    if(result) {
        picture->format = c->pix_fmt;
        picture->width = c->width;
        picture->height = c->height;
        result = av_frame_get_buffer(picture, /*align=*/32) == 0;
    }

    // Prepare the encoding packet
    if(result) {
        pkt = av_packet_alloc();
        result = pkt != nullptr;
    }

    // Open the file
    if(result) {
        f = fopen(filename.toLatin1().data(), "wb");
        result = f != nullptr;
    }

    // Initialise the next frame number
    frameNumber = 0;
    this->width = width;
    this->height = height;
    if(result) {
        std::cout << "Open video file " << filename.toStdString()
                  << ", " << width << "x" << height
                  << ", bit rate=" << c->bit_rate
                  << ", ok=" << result << std::endl;
    } else {
        std::cout << "Video file open failed" << std::endl;
    }
#endif
    return result;
}

// Add a frame to the video file
bool VideoEncoder::addFrame(QImage* image) {
    // Validate the state of the encoder
    bool result = image != nullptr;
#if defined(__USEFFMPEG__)
    result = result || f != nullptr;

    // Validate the image
    if(image->width() != width || image->height() != height) {
        result = false;
    }
    if(image->format() != QImage::Format_RGB32 &&
            image->format() != QImage::Format_ARGB32) {
        result = false;
    }

    // Prepare for the frame
    if(result) {
        fflush(stdout);
        av_frame_make_writable(picture);
    }

    // Prepare for the conversion
    if(result) {
        swsContext = sws_getCachedContext(swsContext, width, height,
                                          AV_PIX_FMT_BGRA, width, height,
                                          AV_PIX_FMT_YUV420P, SWS_BICUBIC,
                                          nullptr, nullptr, nullptr);
        result = swsContext != nullptr;
    }

    // Convert from RGD to YUV
    if(result) {
        const uint8_t *srcplanes[3];
        srcplanes[0]=(uint8_t*)image->bits();
        srcplanes[1]=0;
        srcplanes[2]=0;

        int srcstride[3];
        srcstride[0]=image->bytesPerLine();
        srcstride[1]=0;
        srcstride[2]=0;

        sws_scale(swsContext, srcplanes, srcstride, 0, height,
                  picture->data, picture->linesize);
    }

    // Encode the frame
    if(result) {
        picture->pts = frameNumber++;

        /* encode the image */
        encode(c, picture, pkt, f);
    }
#endif
    return result;
}

// Close the video file
bool VideoEncoder::closeFile() {
    bool result = false;
#if defined(__USEFFMPEG__)
    result = f != nullptr;

    /* Flush the encoder */
    if(result) {
        encode(c, nullptr, pkt, f);
        fwrite(endcode, 1, sizeof(endcode), f);
    }

    /* Close everything down */
    if(f != nullptr) {
        fclose(f);
        f = nullptr;
    }
    if(c != nullptr) {
        avcodec_close(c);
        c = nullptr;
    }
    if(picture != nullptr) {
        av_frame_free(&picture);
        picture = nullptr;
    }
    if(pkt != nullptr) {
        av_packet_free(&pkt);
        pkt = nullptr;
    }
    std::cout << "Close video file ";
#endif
    return result;
}

#if defined(__USEFFMPEG__)
// Gives 1 frame to the codec and then writes out the output to the target file
void VideoEncoder::encode(AVCodecContext* enc_ctx, AVFrame* frame, AVPacket* pkt, FILE* outfile) {
    int ret = avcodec_send_frame(enc_ctx, frame);
    while(ret >= 0) {
        ret = avcodec_receive_packet(enc_ctx, pkt);
        if(ret >= 0) {
            fwrite(pkt->data, 1, (size_t)pkt->size, outfile);
            av_packet_unref(pkt);
        }
    }
}
#endif


