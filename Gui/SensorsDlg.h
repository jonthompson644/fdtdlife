/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SENSORSDLG_H
#define FDTDLIFE_SENSORSDLG_H

#include "QtIncludes.h"
#include "MainTabPanel.h"
#include "FdTdLife.h"
#include <Notifiable.h>

class FdTdLife;
class SensorDlg;
namespace sensor { class Sensor; }

class SensorsDlg : public MainTabPanel, public fdtd::Notifiable {
Q_OBJECT
public:
    // Construction
    explicit SensorsDlg(FdTdLife* m);

    // API
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    sensor::Sensor* getCurrentSensor();
    void updateCurrentSelection();

protected slots:
    // Handlers
    void onSensorSelected();
    void onNew2DSliceSensor();
    void onNewArraySensor();
    void onNewMaterialSensor();
    void onNewFarFieldSensor();
    void onNewRadiationPattern();
    void onNewAreaSensor();
    void onDelete();

protected:
    // Widgets
    enum {
        sensorColIdentifier = 0, sensorColName, sensorColType, numSensorCols
    };
    QTreeWidget* sensorsTree_;
    QPushButton* new2DSliceSensorButton_;
    QPushButton* newArraySensorButton_;
    QPushButton* newMaterialSensorButton_;
    QPushButton* newFarFieldSensorButton_;
    QPushButton* newRadiationPatternButton_;
    QPushButton* newAreaSensorButton_;
    QPushButton* deleteButton_;
    QVBoxLayout* sensorDlgLayout_;
    SensorDlg* sensorDlg_;

protected:
    // Helpers
    void updateUi();
    void fillSensorTree(sensor::Sensor* sel = nullptr);
};


#endif //FDTDLIFE_SENSORSDLG_H
