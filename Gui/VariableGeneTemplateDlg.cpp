/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
VariableGeneTemplateDlg::VariableGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                 gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate),
        Notifiable(dlg->model()) {
    std::tie(valueBitsEdit_, valueMinEdit_, valueMaxEdit_) =
            DialogHelper::geneBitsSpecDoubleItem(layout_, "Variable value:");
    variableList_ = DialogHelper::dropListItem(layout_, "Variable");
    variableList_->setMinimumWidth(200);
    connect(valueMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(valueMaxEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(valueBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(variableList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void VariableGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    valueMinEdit_->setText(QString::number(geneTemplate_->range().min()));
    valueMaxEdit_->setText(QString::number(geneTemplate_->range().max()));
    valueBitsEdit_->setText(QString::number(geneTemplate_->range().bits()));
    dlg_->model()->fillVariableList(variableList_, geneTemplate_->variableId());
}

// An edit box has changed
void VariableGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newValue = c.testGeneBitsSpecDouble(
                valueBitsEdit_, valueMinEdit_, valueMaxEdit_,
                geneTemplate_->range(), FdTdLife::notifySequencerChange);
        int newVariableId = dlg_->model()->selected(variableList_);
        c.externalTest(newVariableId != geneTemplate_->variableId(),
                       FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->range(newValue);
            geneTemplate_->variableId(newVariableId);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// A change notification has been received
void VariableGeneTemplateDlg::notify(fdtd::Notifier* source, int why,
                                     fdtd::NotificationData* /*data*/) {
    if(dlg_->model() == source && why == fdtd::Model::notifyVariableChange) {
        dlg_->model()->fillVariableList(variableList_, geneTemplate_->variableId());
    }
}
