/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ShapesDlg.h"
#include "DialogHelper.h"
#include "CuboidDlg.h"
#include "ExtrudedPolygonDlg.h"
#include "DielectricLayerDlg.h"
#include "SquarePlateDlg.h"
#include "SquareHoleDlg.h"
#include "AdmittanceCatalogueSelectDlg.h"
#include "BinaryPlateNxNDlg.h"
#include "AdmittanceFnLayerDlg.h"
#include "MaterialsSelectDlg.h"
#include "CuboidArrayDlg.h"
#include "CatalogueLayerDlg.h"
#include "JigsawLayerDlg.h"
#include "FingerPlateDlg.h"
#include <Domain/Shape.h>
#include <Domain/Cuboid.h>
#include <Domain/ExtrudedPolygon.h>
#include <Domain/DielectricLayer.h>
#include <Domain/SquarePlate.h>
#include <Domain/SquareHole.h>
#include <Domain/CuboidArray.h>
#include <Box/DxfWriter.h>
#include <Domain/BinaryPlateNxN.h>
#include <Domain/Material.h>
#include <Domain/AdmittanceFnLayer.h>
#include <Domain/CatalogueLayer.h>
#include <Domain/JigsawLayer.h>
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <AdmittanceCatalogue.h>
#include <CatalogueItemWrapper.h>
#include <Box/StlWriter.h>
#include <Box/HfssWriter.h>
#include <Fdtd/Configuration.h>
#include <Source/Source.h>
#include <Source/SourceFactory.h>
#include <GtkGui/ShapesDlg.h>


// Constructor
ShapesDlg::ShapesDlg(FdTdLife* m) :
        MainTabPanel(m),
        Notifiable(m),
        shapeDlg_(nullptr) {
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<CuboidDlg>(
            domain::Domain::shapeModeCuboid));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<DielectricLayerDlg>(
            domain::Domain::shapeModeDielectricLayer));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<SquarePlateDlg>(
            domain::Domain::shapeModeSquarePlate));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<SquareHoleDlg>(
            domain::Domain::shapeModeSquareHole));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<BinaryPlateNxNDlg>(
            domain::Domain::shapeModeBinaryPlateNxN));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<AdmittanceFnLayerDlg>(
            domain::Domain::shapeModeAdmittanceFn));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<ExtrudedPolygonDlg>(
            domain::Domain::shapeModeExtrudedPolygon));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<CuboidArrayDlg>(
            domain::Domain::shapeModeCuboidArray));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<CatalogueLayerDlg>(
            domain::Domain::shapeModeCatalogueLayer));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<JigsawLayerDlg>(
            domain::Domain::shapeModeJigsawLayer));
    dlgFactory_.define(new box::Factory<ShapeDlg>::Creator<FingerPlateDlg>(
            domain::Domain::shapeModeFingerPlate));
    // The parameters
    auto paramLayout = new QVBoxLayout;
    auto shapesLayout = new QHBoxLayout;
    // The shapes buttons
    auto buttonLayout = new QVBoxLayout;
    newShape_ = DialogHelper::buttonMenuItem(
            buttonLayout, "New Shape",
            m_->d()->shapeFactory().modeNames());
    fromCatalogue_ = DialogHelper::buttonItem(buttonLayout, "From Catalogue...");
    deleteButton_ = DialogHelper::buttonItem(buttonLayout, "Delete");
    writeDxfButton_ = DialogHelper::buttonItem(buttonLayout, "Write DXF...");
    writeAllDxfButton_ = DialogHelper::buttonItem(buttonLayout, "Write All DXF...");
    writeAllStlButton_ = DialogHelper::buttonItem(buttonLayout, "Write All STL...");
    writeAllHfssButton_ = DialogHelper::buttonItem(buttonLayout, "Write All HFSS...");
    convertToBinaryNxNButton_ = DialogHelper::buttonItem(buttonLayout,
                                                         "Convert to BinaryNxN");
    copyButton_ = DialogHelper::buttonItem(buttonLayout, "Copy To Clipboard");
    pasteButton_ = DialogHelper::buttonItem(buttonLayout, "Paste From Clipboard");
    buttonLayout->addStretch();
    shapesLayout->addLayout(buttonLayout);
    // The materials tree
    shapesTree_ = new QTreeWidget;
    shapesTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    shapesTree_->setColumnCount(numShapeCols);
    shapesTree_->setHeaderLabels({"Identifier", "Name", "Type"});
    shapesTree_->setDragDropMode(QAbstractItemView::InternalMove);
    shapesTree_->setFixedWidth(320);
    shapesLayout->addWidget(shapesTree_);
    // The place holder for the shape dialog
    shapeDlgLayout_ = new QHBoxLayout;
    shapesLayout->addLayout(shapeDlgLayout_);
    shapesLayout->addStretch();
    // The shapes assembly
    paramLayout->addLayout(shapesLayout);
    // The main layout
    setTabLayout(paramLayout);
    // Connect the handlers
    connect(shapesTree_, SIGNAL(itemSelectionChanged()),
            SLOT(onShapeSelected()));
    connect(fromCatalogue_, SIGNAL(clicked()), SLOT(onFromCatalogue()));
    connect(deleteButton_, SIGNAL(clicked()), SLOT(onDelete()));
    connect(writeDxfButton_, SIGNAL(clicked()), SLOT(onWriteDxf()));
    connect(writeAllDxfButton_, SIGNAL(clicked()), SLOT(onWriteAllDxf()));
    connect(writeAllStlButton_, SIGNAL(clicked()), SLOT(onWriteAllStl()));
    connect(writeAllHfssButton_, SIGNAL(clicked()), SLOT(onWriteAllHfss()));
    connect(convertToBinaryNxNButton_, SIGNAL(clicked()),
            SLOT(onConvertToBinaryNxN()));
    connect(newShape_, SIGNAL(clickedMenu(int)), SLOT(onNewShape(int)));
    connect(copyButton_, SIGNAL(clicked()), SLOT(onCopy()));
    connect(pasteButton_, SIGNAL(clicked()), SLOT(onPaste()));
    // Fill the dialog
    fillShapeTree();
}

// A new shape has been clicked
void ShapesDlg::onNewShape(int item) {
    tabChanged();  // Update anything that may have changed for the current item
    domain::Shape* shape = m_->d()->makeShape(item);
    if(shape != nullptr) {
        std::stringstream name;
        name << "Shape" << shape->identifier();
        shape->name(name.str());
        switch(item) {
            case domain::Domain::shapeModeSquarePlate:
            case domain::Domain::shapeModeJigsawLayer:
            case domain::Domain::shapeModeFingerPlate:
            case domain::Domain::shapeModeSquareHole:
            case domain::Domain::shapeModeBinaryPlateNxN: {
                dynamic_cast<domain::UnitCellShape*>(shape)->cellSizeX(box::Expression(
                        fabs(m_->p()->p1().x().value() - m_->p()->p2().x().value())));
                break;
            }
            default:
                break;
                // Need to add from the admittance catalogue somehow
        }
        shape->name(name.str());
        fillShapeTree(shape);
        InitialisingGui::Set s(initialising_);
        m_->doNotification(FdTdLife::notifyDomainContentsChange);
    }
}

// Create a shape from the admittance catalogue
void ShapesDlg::onFromCatalogue() {
    fdtd::AdmittanceCatalogue* cat = &m_->admittanceCat();
    AdmittanceCatalogueSelectDlg box(this, cat);
    if(box.exec() == QDialog::Accepted) {
        auto* binaryPlate = dynamic_cast<domain::BinaryPlateNxN*>(
                m_->d()->makeShape(domain::Domain::shapeModeBinaryPlateNxN));
        binaryPlate->bitsPerHalfSide(static_cast<size_t>(cat->bitsPerHalfSide()));
        binaryPlate->coding({box.selectedItem()->pattern()});
        binaryPlate->admittanceTable() = box.selectedItem()->admittance(
                cat->unitCell(), cat->startFrequency(), cat->frequencyStep());
        binaryPlate->cellSizeX(box::Expression(
                fabs(m_->p()->p1().x().value() - m_->p()->p2().x().value())));
        std::stringstream name;
        name << "Shape" << binaryPlate->identifier();
        binaryPlate->name(name.str());
        fillShapeTree(binaryPlate);
        InitialisingGui::Set s(initialising_);
        m_->doNotification(FdTdLife::notifyDomainContentsChange);
    }
}

// Fill the materials tree
void ShapesDlg::fillShapeTree(domain::Shape* sel) {
    QTreeWidgetItem* selItem = nullptr;
    {
        InitialisingGui::Set s(initialising_);
        // Delete any old dialog
        delete shapeDlg_;
        shapeDlg_ = nullptr;
        // Fill the tree
        shapesTree_->clear();
        for(auto& shape : m_->d()->shapes()) {
            auto item = new QTreeWidgetItem(shapesTree_);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setText(shapeColIdentifier,
                          QString::number(shape->identifier()));
            item->setText(shapeColName, shape->name().c_str());
            item->setText(shapeColType,
                          m_->d()->shapeFactory().modeName(shape->mode()).c_str());
            if(selItem == nullptr || shape.get() == sel) {
                selItem = item;
            }
        }
    }
    // Select an item
    if(selItem != nullptr) {
        shapesTree_->setCurrentItem(selItem);
    }
}

// Initialise the dialog
void ShapesDlg::initialise() {
    if(!initialising_) {
        // Get the shape
        domain::Shape* shape = getCurrentShape();
        // Fill the shape list
        fillShapeTree(shape);
    }
}

// A shape has been selected
void ShapesDlg::onShapeSelected() {
    if(!initialising_) {
        // Get the shape
        domain::Shape* shape = getCurrentShape();
        if(shape == nullptr) {
            // Delete any old dialog
            delete shapeDlg_;
            shapeDlg_ = nullptr;
        } else {
            // Does the dialog need changing
            if(shapeDlg_ == nullptr || shapeDlg_->shape() != shape) {
                // Delete any old dialog
                delete shapeDlg_;
                shapeDlg_ = dlgFactory_.make(this, shape);
                // Need to change this to depend on the selected shape
                if(shapeDlg_ != nullptr) {
                    shapeDlg_->initialise();
                    shapeDlgLayout_->addWidget(shapeDlg_);
                }
            } else {
                shapeDlg_->initialise();
            }
        }
    }
}

// Delete a shape
void ShapesDlg::onDelete() {
    QMessageBox box;
    domain::Shape* shape = getCurrentShape();
    if(shape != nullptr) {
        QString msg;
        QTextStream t(&msg);
        t << "OK to delete the shape " << shape->name().c_str();
        box.setText(msg);
        box.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Ok);
        if(box.exec() == QMessageBox::Ok) {
            m_->d()->remove(shape);
            fillShapeTree();
            InitialisingGui::Set s(initialising_);
            m_->doNotification(FdTdLife::notifyDomainContentsChange);
        }
    }
}

// Get the current shape.
domain::Shape* ShapesDlg::getCurrentShape() {
    domain::Shape* result = nullptr;
    auto item = shapesTree_->currentItem();
    if(item != nullptr) {
        int identifier = shapesTree_->currentItem()->text(shapeColIdentifier).toInt();
        result = m_->d()->getShape(identifier);
    }
    return result;
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void ShapesDlg::tabChanged() {
    if(shapeDlg_ != nullptr) {
        shapeDlg_->tabChanged();
    }
}

// The user has changed some aspect of the configuration
void ShapesDlg::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(source == m_) {
        switch(why) {
            case FdTdLife::notifyDomainContentsChange:
            case FdTdLife::notifyShapeChange:
                initialise();
                break;
            default:
                break;
        }
    }
}

// The currently selected shape has changed and the tree needs updating
void ShapesDlg::updateCurrentSelection() {
    domain::Shape* shape = getCurrentShape();
    auto item = shapesTree_->currentItem();
    if(shape != nullptr && item != nullptr) {
        item->setText(shapeColName, shape->name().c_str());
    }
}

// Write out a DXF file containing the current shape
void ShapesDlg::onWriteDxf() {
    domain::Shape* shape = getCurrentShape();
    if(shape != nullptr) {
        QFileInfo info(m_->getCurrentFileName());
        QString fileName = QFileDialog::getSaveFileName(this, "Save DXF File",
                                                        info.path(),
                                                        "DXF Files (*.dxf)");
        if(!fileName.isEmpty()) {
            MaterialsSelectDlg box(this, m_->d());
            if(box.exec() == QDialog::Accepted) {
                box::DxfWriter dxf(fileName.toStdString());
                shape->write(dxf, box.selectedMaterials());
            }
        }
    }
}

// Write out a DXF file containing all the shapes
void ShapesDlg::onWriteAllDxf() {
    QFileInfo info(m_->getCurrentFileName());
    QString fileName = QFileDialog::getSaveFileName(this, "Save DXF File",
                                                    info.path() + "/" +
                                                    info.completeBaseName(),
                                                    "DXF Files (*.dxf)");
    if(!fileName.isEmpty()) {
        MaterialsSelectDlg box(this, m_->d());
        if(box.exec() == QDialog::Accepted) {
            box::DxfWriter dxf(fileName.toStdString());
            m_->writeExport(dxf, box.selectedMaterials());
        }
    }
}

// Write out an STL file containing all the shapes
void ShapesDlg::onWriteAllStl() {
    QFileInfo info(m_->getCurrentFileName());
    QString fileName = QFileDialog::getSaveFileName(this, "Save STL File",
                                                    info.path() + "/" +
                                                    info.completeBaseName(),
                                                    "STL Files (*.stl)");
    if(!fileName.isEmpty()) {
        MaterialsSelectDlg box(this, m_->d());
        if(box.exec() == QDialog::Accepted) {
            box::StlWriter stl(fileName.toStdString());
            m_->writeExport(stl, box.selectedMaterials());
        }
    }
}

// Write out an HFSS script file containing all the shapes
void ShapesDlg::onWriteAllHfss() {
    QFileInfo info(m_->getCurrentFileName());
    QString fileName = QFileDialog::getSaveFileName(this, "Save HFSS Script File",
                                                    info.path() + "/" +
                                                    info.completeBaseName(),
                                                    "Python Files (*.py)");
    if(!fileName.isEmpty()) {
        MaterialsSelectDlg box(this, m_->d());
        if(box.exec() == QDialog::Accepted) {
            box::HfssWriter hfss(
                    fileName.toStdString(),
                    box::HfssWriter::Clipping::none,
                    m_->p()->boundary().x() == fdtd::Configuration::Boundary::periodic,
                    m_->p()->boundary().y() == fdtd::Configuration::Boundary::periodic,
                    m_->sources().sources().size() == 1
                    && m_->sources().sources().front()->mode() ==
                       source::SourceFactory::modePlaneWave);
            m_->writeExport(hfss, box.selectedMaterials());
        }
    }
}

// Convert any layers into binary NxN layers
void ShapesDlg::onConvertToBinaryNxN() {
    // Is the domain suitable for converting
    if(m_->p()->geometry()->n().x() == m_->p()->geometry()->n().y() &&
       (m_->p()->geometry()->n().x() & 1UL) == 0) {
        // Get domain information
        size_t bitsPerSide = m_->p()->geometry()->n().x();
        double cellSize = m_->p()->p2().x().value() - m_->p()->p1().x().value();
        double thickness = m_->p()->dr().z().value();
        size_t requiredWords =
                domain::BinaryPlateNxN::requiredWords(bitsPerSide / 2);
        // For each shape in the domain
        auto pos = m_->d()->shapes().begin();
        while(pos != m_->d()->shapes().end()) {
            domain::Shape* shape = pos->get();
            auto* layer = dynamic_cast<domain::UnitCellShape*>(shape);
            pos++;
            // Only convert layers
            if(layer != nullptr) {
                // Don't bother converting binary plates
                if(dynamic_cast<domain::BinaryPlateNxN*>(layer) == nullptr) {
                    size_t z = m_->p()->geometry()->cellRound(layer->center().value()).z();
                    // The coding data
                    std::vector<uint64_t> coding;
                    coding.resize(requiredWords, 0);
                    for(size_t x = 0; x < bitsPerSide / 2; x++) {
                        for(size_t y = 0; y < bitsPerSide / 2; y++) {
                            size_t bitNumber = y * bitsPerSide / 2 + x;
                            size_t wordNumber =
                                    bitNumber / domain::BinaryPlateNxN::bitsPerCodingWord_;
                            size_t bitInWord =
                                    bitNumber % domain::BinaryPlateNxN::bitsPerCodingWord_;
                            domain::Material* mat = m_->d()->getMaterialAt(x, y, z);
                            if(mat != nullptr) {
                                if(layer->material() == mat->index()) {
                                    coding[wordNumber] |= 1ULL << bitInWord;
                                }
                            }
                        }
                    }
                    // Create the new layer, delete the old layer
                    auto* plate = dynamic_cast<domain::BinaryPlateNxN*>(m_->d()->makeShape(
                            domain::Domain::shapeModeBinaryPlateNxN));
                    plate->name(layer->name());
                    plate->material(layer->material());
                    plate->bitsPerHalfSide(bitsPerSide / 2);
                    plate->coding(coding);
                    plate->center() = layer->center();
                    plate->cellSizeX(box::Expression(cellSize));
                    plate->layerThickness(box::Expression(thickness));
                    delete layer;
                }
            }
        }
    }
    fillShapeTree();
}

// Copy a shape to the clipboard
void ShapesDlg::onCopy() {
    domain::Shape* shape = getCurrentShape();
    if(shape != nullptr) {
        // Encode the shape into XML
        auto* o = new xml::DomObject("shape");
        *o << *shape;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        QClipboard* clipboard = QApplication::clipboard();
        auto mimeData = new QMimeData;
        QByteArray data(text.c_str());
        mimeData->setData("text/plain", data);
        mimeData->setData("fdtdlife/shape", data);
        clipboard->setMimeData(mimeData);
    }
}

// Paste a shape from the clipboard
void ShapesDlg::onPaste() {
    const QMimeData* mimeData = QApplication::clipboard()->mimeData();
    // Do we have a recognised format?
    if(mimeData->hasFormat("fdtdlife/shape")) {
        try {
            // Read the XML into a DOM
            std::string text = mimeData->data("fdtdlife/shape").toStdString();
            xml::DomDocument dom("shape");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a shape from the DOM
            domain::Shape* shape = m_->d()->makeShape(*dom.getObject());
            *dom.getObject() >> *shape;
            fillShapeTree(shape);
        } catch(xml::Exception& e) {
            std::cout << "Shape paste failed to read XML: " << e.what() << std::endl;
        }
    } else if(mimeData->hasFormat("fdtdlife/catalogueitem")) {
        try {
            // Read the XML into a DOM
            std::string text = mimeData->data(
                    "fdtdlife/catalogueitem").toStdString();
            xml::DomDocument dom("catalogueitem");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a catalogue item from the DOM
            fdtd::CatalogueItemWrapper item;
            *dom.getObject() >> item;
            // Now create a shape from the item
            auto* shape = dynamic_cast<domain::BinaryPlateNxN*>(m_->d()->makeShape(
                    domain::Domain::shapeModeBinaryPlateNxN));
            shape->name("From catalogue");
            shape->material(domain::Domain::defaultMaterial);
            shape->bitsPerHalfSide(item.bitsPerHalfSide());
            shape->coding({item.pattern()});
            shape->cellSizeX(box::Expression(item.unitCell()));
            shape->layerThickness(box::Expression(0.000001));
            shape->admittanceTable() = item.admittance();
            fillShapeTree(shape);
        } catch(xml::Exception& e) {
            std::cout << "Shape paste failed to read XML: " << e.what() << std::endl;
        }
    }
}
