/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquareHoleDlg.h"
#include "DialogHelper.h"
#include <Domain/SquareHole.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Constructor
void SquareHoleDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::SquareHole*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Layer Position x,y,z (m)");
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    cellSizeEdit_ = DialogHelper::textItem(line2Layout, "Cell Size (m)");
    holeSizeEdit_ = DialogHelper::textItem(line2Layout, "Plate Size (%)");
    std::tie(incrementXEdit_, incrementYEdit_, incrementZEdit_) =
            DialogHelper::vectorItem(layout_, "Increment x,y,z (m)");
    std::tie(duplicateXEdit_, duplicateYEdit_, duplicateZEdit_) =
            DialogHelper::vectorItem(layout_, "Duplicate x,y,z");
    auto line5Layout = new QHBoxLayout;
    layout_->addLayout(line5Layout);
    thicknessEdit_ = DialogHelper::doubleItem(line5Layout, "Plate Thickness (m)");
    fillMaterialList_ = DialogHelper::dropListItem(line5Layout, "Fill Material");
    auto line6Layout = new QHBoxLayout;
    layout_->addLayout(line6Layout);
    repeatXEdit_ = DialogHelper::textItem(line6Layout, "Repeat X");
    repeatYEdit_ = DialogHelper::textItem(line6Layout, "Repeat Y");
    auto line7Layout = new QHBoxLayout;
    layout_->addLayout(line7Layout);
    offsetXEdit_ = DialogHelper::doubleItem(line7Layout, "Offset X (m)");
    offsetYEdit_ = DialogHelper::doubleItem(line7Layout, "Offset Y (m)");
    // Finish and connect handlers
    connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(cellSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(holeSizeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(offsetXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(offsetYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(thicknessEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(fillMaterialList_, SIGNAL(currentIndexChanged(int)),
            SLOT(onChange()));
    connect(incrementXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(incrementZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(duplicateZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void SquareHoleDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(shape_->center().x().text().c_str());
    y1Edit_->setText(shape_->center().y().text().c_str());
    z1Edit_->setText(shape_->center().z().text().c_str());
    cellSizeEdit_->setText(shape_->cellSizeX().text().c_str());
    holeSizeEdit_->setText(shape_->holeSize().text().c_str());
    repeatXEdit_->setText(shape_->repeatX().text().c_str());
    repeatYEdit_->setText(shape_->repeatY().text().c_str());
    offsetXEdit_->setText(QString::number(shape_->holeOffsetX()));
    offsetYEdit_->setText(QString::number(shape_->holeOffsetY()));
    thicknessEdit_->setText(shape_->layerThickness().text().c_str());
    dlg_->model()->fillMaterialList(fillMaterialList_, shape_->fillMaterial());
    incrementXEdit_->setText(shape_->increment().x().text().c_str());
    incrementYEdit_->setText(shape_->increment().y().text().c_str());
    incrementZEdit_->setText(shape_->increment().z().text().c_str());
    duplicateXEdit_->setText(shape_->duplicate().x().text().c_str());
    duplicateYEdit_->setText(shape_->duplicate().y().text().c_str());
    duplicateZEdit_->setText(shape_->duplicate().z().text().c_str());
    x1Edit_->setToolTip(QString::number(shape_->center().x().value()));
    y1Edit_->setToolTip(QString::number(shape_->center().y().value()));
    z1Edit_->setToolTip(QString::number(shape_->center().z().value()));
    cellSizeEdit_->setToolTip(QString::number(shape_->cellSizeX().value()));
    holeSizeEdit_->setToolTip(QString::number(shape_->holeSize().value()));
    incrementXEdit_->setToolTip(QString::number(shape_->increment().x().value()));
    incrementYEdit_->setToolTip(QString::number(shape_->increment().y().value()));
    incrementZEdit_->setToolTip(QString::number(shape_->increment().z().value()));
    duplicateXEdit_->setToolTip(QString::number(shape_->duplicate().x().value()));
    duplicateYEdit_->setToolTip(QString::number(shape_->duplicate().y().value()));
    duplicateZEdit_->setToolTip(QString::number(shape_->duplicate().z().value()));
    repeatXEdit_->setToolTip(QString::number(shape_->repeatX().value()));
    repeatYEdit_->setToolTip(QString::number(shape_->repeatY().value()));
}

// An edit box has changed
void SquareHoleDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenterX = c.testString(x1Edit_, shape_->center().x().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterY = c.testString(y1Edit_, shape_->center().y().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCenterZ = c.testString(z1Edit_, shape_->center().z().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newCellSize = c.testString(cellSizeEdit_, shape_->cellSizeX().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newHoleSize = c.testString(holeSizeEdit_, shape_->holeSize().text(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newOffsetX = c.testDouble(offsetXEdit_, shape_->holeOffsetX(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newOffsetY = c.testDouble(offsetYEdit_, shape_->holeOffsetY(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newThickness = c.testString(thicknessEdit_,
                                         shape_->layerThickness().text(),
                                         FdTdLife::notifyDomainContentsChange);
        auto newRepeatX = c.testString(repeatXEdit_, shape_->repeatX().text(),
                                       FdTdLife::notifyDomainContentsChange);
        auto newRepeatY = c.testString(repeatYEdit_, shape_->repeatY().text(),
                                       FdTdLife::notifyDomainContentsChange);
        int newFillMaterialIndex = dlg_->model()->selected(fillMaterialList_);
        c.externalTest(newFillMaterialIndex != shape_->fillMaterial(),
                       FdTdLife::notifyDomainContentsChange);
        auto newincrementX = c.testString(incrementXEdit_,
                                          shape_->increment().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementY = c.testString(incrementYEdit_,
                                          shape_->increment().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newincrementZ = c.testString(incrementZEdit_,
                                          shape_->increment().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateX = c.testString(duplicateXEdit_,
                                          shape_->duplicate().x().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateY = c.testString(duplicateYEdit_,
                                          shape_->duplicate().y().text(),
                                          FdTdLife::notifyDomainContentsChange);
        auto newduplicateZ = c.testString(duplicateZEdit_,
                                          shape_->duplicate().z().text(),
                                          FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->center().text(newCenterX, newCenterY, newCenterZ);
            shape_->cellSizeX(box::Expression(newCellSize));
            shape_->holeSize(box::Expression(newHoleSize));
            shape_->repeatX(box::Expression(newRepeatX));
            shape_->repeatY(box::Expression(newRepeatY));
            shape_->holeOffsetX(newOffsetX);
            shape_->holeOffsetY(newOffsetY);
            shape_->layerThickness(box::Expression(newThickness));
            shape_->fillMaterial(newFillMaterialIndex);
            shape_->increment().x().text(newincrementX);
            shape_->increment().y().text(newincrementY);
            shape_->increment().z().text(newincrementZ);
            shape_->duplicate().x().text(newduplicateX);
            shape_->duplicate().y().text(newduplicateY);
            shape_->duplicate().z().text(newduplicateZ);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void SquareHoleDlg::tabChanged() {
    onChange();
}

