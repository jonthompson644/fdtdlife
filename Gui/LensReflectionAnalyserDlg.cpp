/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensReflectionAnalyserDlg.h"
#include "DialogHelper.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by factory
void LensReflectionAnalyserDlg::construct(AnalysersDlg* dlg, sensor::Analyser* d) {
    AnalysersDlg::SubDlg::construct(dlg, d);
    fdtd::Notifiable::construct(dlg->model());
    analyser_ = dynamic_cast<sensor::LensReflectionAnalyser*>(d);
    // Create the controls
    auto line1Layout = new QHBoxLayout;
    layout_->addLayout(line1Layout);
    frequencyEdit_ = DialogHelper::textItem(line1Layout, "Frequency");
    sensorList_ = DialogHelper::dropListItem(line1Layout, "Sensor");
    auto line2Layout = new QHBoxLayout;
    layout_->addLayout(line2Layout);
    locationList_ = DialogHelper::dropListItem(line2Layout, "Location",
                                               {"Smallest Z", "Largest Z"});
    componentList_ = DialogHelper::dropListItem(line2Layout, "Component",
                                                {"X", "Y"});
    auto line3Layout = new QHBoxLayout;
    layout_->addLayout(line3Layout);
    referenceTimeEdit_ = DialogHelper::textItem(line3Layout, "Reference Time");
    captureRefButton_ = DialogHelper::buttonItem(line3Layout, "Capture Reference Now");
    errorEdit_ = DialogHelper::doubleItem(layout_, "Error", true);
    dataView_ = new DataSeriesWidget(dlg->model()->guiConfiguration());
    layout_->addWidget(dataView_);
    // Connect the handlers
    connect(frequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(referenceTimeEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(sensorList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(locationList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(componentList_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(captureRefButton_, SIGNAL(clicked()), SLOT(onCaptureRef()));
    // Initialise the dialog
    initialise();
}

// Initialise the GUI parameters
void LensReflectionAnalyserDlg::initialise() {
    InitialisingGui::Set s(initialising_);
    // Set parameters
    frequencyEdit_->setText(analyser_->frequency().text().c_str());
    referenceTimeEdit_->setText(analyser_->referenceTime().text().c_str());
    errorEdit_->setText(QString::number(analyser_->error()));
    locationList_->setCurrentIndex(static_cast<int>(analyser_->location()));
    componentList_->setCurrentIndex(static_cast<int>(analyser_->component()));
    frequencyEdit_->setToolTip(QString::number(analyser_->frequency().value()));
    referenceTimeEdit_->setToolTip(QString::number(analyser_->referenceTime().value()));
    dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    showData();
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void LensReflectionAnalyserDlg::tabChanged() {
    onChange();
}

// An edit box has changed
void LensReflectionAnalyserDlg::onChange() {
    if(!initialising_) {
        GuiChangeDetector c;
        auto newFrequency = c.testString(frequencyEdit_,
                                         analyser_->frequency().text(),
                                         FdTdLife::notifyMinorChange);
        auto newReferenceTime = c.testString(referenceTimeEdit_,
                                         analyser_->referenceTime().text(),
                                         FdTdLife::notifyMinorChange);
        auto newLocation = static_cast<sensor::Analyser::SensorLocation>(
                c.testInt(locationList_, static_cast<int>(analyser_->location()),
                          FdTdLife::notifyMinorChange));
        auto newComponent = static_cast<sensor::Analyser::SensorComponent>(
                c.testInt(componentList_,
                          static_cast<int>(analyser_->component()),
                          FdTdLife::notifyMinorChange));
        int newSensorId = dlg_->model()->selected(sensorList_);
        c.externalTest(newSensorId != analyser_->sensorId(),
                       FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            analyser_->frequency(box::Expression(newFrequency));
            analyser_->referenceTime(box::Expression(newReferenceTime));
            analyser_->sensorId(newSensorId);
            analyser_->location(newLocation);
            analyser_->component(newComponent);
            frequencyEdit_->setToolTip(QString::number(analyser_->frequency().value()));
            referenceTimeEdit_->setToolTip(QString::number(analyser_->referenceTime().value()));
            // Tell others
            dlg_->model()->doNotification(c.why());
        }
    }
}

// Handle a notification
void LensReflectionAnalyserDlg::notify(fdtd::Notifier* source, int why,
                                  fdtd::NotificationData* /*data*/) {
    if(source == dlg_->model() && why == fdtd::Model::notifySensorChange) {
        InitialisingGui::Set s(initialising_);
        dlg_->model()->fillSensorList(sensorList_, analyser_->sensorId());
    } else if(source == dlg_->model() && why == fdtd::Model::notifyAnalysersCalculate) {
        errorEdit_->setText(QString::number(analyser_->error()));
        showData();
    }
}

// Handle a notification
void LensReflectionAnalyserDlg::showData() {
    dataView_->setData(analyser_->data(), GuiConfiguration::elementDataA,
                       true);
    dataView_->setData(analyser_->reference(), GuiConfiguration::elementDataB,
                       false);
    dataView_->setAxisInfo(
            analyser_->minX() / box::Constants::milli_,
            analyser_->stepX() / box::Constants::milli_, "m",
            analyser_->minY(), analyser_->maxY(), "V/m");
}

// Capture the reference data now
void LensReflectionAnalyserDlg::onCaptureRef() {
    analyser_->captureReferenceNow();
    referenceTimeEdit_->setText(analyser_->referenceTime().text().c_str());
    referenceTimeEdit_->setToolTip(QString::number(analyser_->referenceTime().value()));
    showData();
}
