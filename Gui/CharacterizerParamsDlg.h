/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CHARACTERIZERPARAMSDLG_H
#define FDTDLIFE_CHARACTERIZERPARAMSDLG_H

#include "QtIncludes.h"
#include "CharacterizerSubDlg.h"
#include <Notifiable.h>
#include "PushButtonMenu.h"

class CharacterizerDlg;
class ScannerDlg;
namespace seq {class Scanner;}

// Sub dialog for the catalogue builder parameters page
class CharacterizerParamsDlg: public CharacterizerSubDlg, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit CharacterizerParamsDlg(CharacterizerDlg* dlg);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onChange();
    void onScannerSelected();
    void onNew(int item);
    void onDelete();
    void onScannerInserted(const QModelIndex &parent, int start, int end);

protected:
    enum {columnId=0, columnType, columnNumSteps, numColumns};
    QTreeWidget* scannersTree_;
    enum {scannerIdenticalLayers=0, scannerLayerSpacing, scannerBinaryNxNPlate,
            scannerSquarePlate, scannerDielectricLayer, scannerVariable};
    PushButtonMenu* newButton_;
    QPushButton* deleteButton_;
    ScannerDlg* scannerDlg_;
    QVBoxLayout* scannerDlgLayout_;

protected:
    void fillScannerTree(seq::Scanner* sel=nullptr);
    seq::Scanner* currentScanner();
    void initialiseScanner();
};

#endif //FDTDLIFE_CHARACTERIZERPARAMSDLG_H
