/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENETICSEARCHTARGETDLG_H
#define FDTDLIFE_GENETICSEARCHTARGETDLG_H

#include "GeneticSearchSubDlg.h"
#include "Notifiable.h"
#include "DialogFactory.h"
#include "TabbedSubDlg.h"

class GeneticSearchSeqDlg;
class PushButtonMenu;
namespace gs {class FitnessBase;}

// Main tab panel for the sequencers
class GeneticSearchTargetDlg: public GeneticSearchSubDlg, public fdtd::Notifiable {
Q_OBJECT
public:
    explicit GeneticSearchTargetDlg(GeneticSearchSeqDlg* dlg);
    void initialise() override;
    void tabChanged() override;
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected slots:
    void onNewFitnessFunction(int fitnessFunctionType);
    void onDeleteFunction();
    void onCopyFunction();
    void onPasteFunction();
    void onFitnessFunctionSelected();
    void onEvaluateNow();

protected:
    enum {
        fitnessFunctionColPosition=0, fitnessFunctionColType, numFitnessFunctionCols
    };
    enum {
        fitnessFunctionAttenuation=0, fitnessFunctionPhaseShift, fitnessFunctionPhaseShiftChange,
        fitnessFunction3dBPoint, fitnessFunctionAdmittance, fitnessFunctionBirefPhaseDiff,
        fitnessFunctionAreaIntensity, fitnessFunctionIntensityLine, fitnessFunctionInPhaseLine
    };
    QTreeWidget* fitnessFunctionTree_;
    PushButtonMenu* newFitnessFunctionButton_;
    QPushButton* deleteFunctionButton_;
    QPushButton* pasteFunctionButton_;
    QPushButton* copyFunctionButton_;
    QPushButton* evaluateNowButton_;
    QVBoxLayout* fitnessFunctionDlgLayout_;
    typedef TabbedSubDlg<GeneticSearchTargetDlg, gs::FitnessBase> FitnessBaseDlg;
    FitnessBaseDlg* fitnessFunctionDlg_;
    typedef DialogFactory<FitnessBaseDlg> FitnessDlgFactory;
    FitnessDlgFactory dlgFactory_;

protected:
    gs::FitnessBase* currentFitnessFunction();
    void fillFitnessFunctionTree(gs::FitnessBase* sel = nullptr);
    void initialiseFunction();
};


#endif //FDTDLIFE_GENETICSEARCHTARGETDLG_H
