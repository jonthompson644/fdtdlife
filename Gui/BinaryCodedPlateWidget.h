/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYCODEDPLATEWIDGET_H
#define FDTDLIFE_BINARYCODEDPLATEWIDGET_H

#include "QtIncludes.h"

class BinaryCodedPlateWidget : public QWidget {
Q_OBJECT
public:
    // Construction
    BinaryCodedPlateWidget(std::vector<uint64_t>  data, size_t size,
                           bool fourFold = true, bool readOnly = false,
                           size_t widgetSize = 384);

    // API
    const std::vector<uint64_t>& data() const { return data_; }
    uint64_t coding() const;
    void setData(const std::vector<uint64_t>& value, size_t size);
    void clear();
    void fourFold(bool v) { fourFold_ = v; }

    // Overrides of QWidget
    QSize sizeHint() const override;
    void paintEvent(QPaintEvent* event) override;
    void contextMenuEvent(QContextMenuEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;

public slots:
    // Slots
signals:
    // Signals
    void patternChanged();

protected:
    // Members
    std::vector<uint64_t> data_;
    size_t size_;
    bool fourFold_;
    size_t pixelsPerBit_;
    bool readOnly_;
    size_t widgetSize_; // The width and height in pixels of the widget
    static const size_t bitsPerCodingWord_ = 64;  // Number of bits in each word of the coding

    // Helpers
    size_t numDataWords() {
        return (size_ * size_ / 4 + bitsPerCodingWord_ - 1) / bitsPerCodingWord_;
    }
};


#endif //FDTDLIFE_BINARYCODEDPLATEWIDGET_H
