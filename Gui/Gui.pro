#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T12:04:21
#
#-------------------------------------------------

QT       += widgets network printsupport

TARGET = Gui
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AdmittanceCatalogueDlg.cpp \
    AdmittanceCatalogueSelectDlg.cpp \
    AdmittanceCatLayerGeneDlg.cpp \
    AdmittanceCatLayerGeneTemplateDlg.cpp \
    AdmittanceFnLayerDlg.cpp \
    AdmittanceFnLayerGeneDlg.cpp \
    AdmittanceFnLayerGeneTemplateDlg.cpp \
    AdmittanceTableEditDlg.cpp \
    AreaSensorDlg.cpp \
    ArraySensorDlg.cpp \
    AttenuationView.cpp \
    AttenuationViewDlg.cpp \
    BinaryCodedPlateDlg.cpp \
    BinaryCodedPlateGeneDlg.cpp \
    BinaryCodedPlateGeneTemplateDlg.cpp \
    BinaryCodedPlateWidget.cpp \
    BinaryPlateNxNDlg.cpp \
    BinaryPlateNxNGeneDlg.cpp \
    BinaryPlateNxNGeneTemplateDlg.cpp \
    BoundaryMaterialDlg.cpp \
    CatalogueBrowseDlg.cpp \
    CatalogueBuilderDlg.cpp \
    CatalogueBuilderItemsDlg.cpp \
    CatalogueBuilderParamsDlg.cpp \
    CatalogueBuilderSubDlg.cpp \
    ClusterDlg.cpp \
    ComboBoxDelegate.cpp \
    CpmlMaterialDlg.cpp \
    CuboidDlg.cpp \
    DataSeriesContextDlg.cpp \
    DataSeriesView.cpp \
    DataSeriesViewDlg.cpp \
    DataSeriesWidget.cpp \
    DefaultMaterialDlg.cpp \
    DialogHelper.cpp \
    DielectricBlockGeneDlg.cpp \
    DielectricBlockGeneTemplateDlg.cpp \
    DielectricLayerDlg.cpp \
    DielectricLayerGeneDlg.cpp \
    DielectricLayerGeneTemplateDlg.cpp \
    DuplicateLayerGeneDlg.cpp \
    DuplicateLayerGeneTemplateDlg.cpp \
    ExtrudedPlaneDlg.cpp \
    FarFieldSensorDlg.cpp \
    FdTdLife.cpp \
    Fitness3dBPointDlg.cpp \
    FitnessAdmittanceDlg.cpp \
    FitnessAreaIntensityDlg.cpp \
    FitnessBaseDlg.cpp \
    FitnessFunctionDlg.cpp \
    FitnessFilterDlg.cpp \
    GeneDlg.cpp \
    GeneTemplateDlg.cpp \
    GeneticSearchGeneTemplateDlg.cpp \
    GeneticSearchParamsDlg.cpp \
    GeneticSearchPopulationDlg.cpp \
    GeneticSearchSeqDlg.cpp \
    GeneticSearchSubDlg.cpp \
    GeneticSearchTargetDlg.cpp \
    GravyWaveSourceDlg.cpp \
    GuiChangeDetector.cpp \
    IndividualEditDlg.cpp \
    InitialisingGui.cpp \
    LayerPresentGeneDlg.cpp \
    LayerPresentGeneTemplateDlg.cpp \
    LayersList.cpp \
    LayerSpacingGeneDlg.cpp \
    LayerSpacingGeneTemplateDlg.cpp \
    LayerSymmetryGeneDlg.cpp \
    LayerSymmetryGeneTemplateDlg.cpp \
    MainTabPanel.cpp \
    MaterialDlg.cpp \
    MaterialsDlg.cpp \
    MaterialSensorDlg.cpp \
    MaterialSensorView.cpp \
    MaterialSensorViewDlg.cpp \
    MpiProcessPool.cpp \
    ParametersCfg.cpp \
    ParametersDlg.cpp \
    PhaseShiftView.cpp \
    PhaseShiftViewDlg.cpp \
    PlaneWaveSourceDlg.cpp \
    PlateAdmittanceEditDlg.cpp \
    PlateStackDlg.cpp \
    PowerView.cpp \
    PowerViewDlg.cpp \
    ProcessingNodeCfg.cpp \
    PushButtonMenu.cpp \
    RadiationPatternDlg.cpp \
    RepeatCellGeneDlg.cpp \
    RepeatGeneTemplateDlg.cpp \
    SensorArrayView.cpp \
    SensorArrayViewDlg.cpp \
    SensorDlg.cpp \
    SensorsDlg.cpp \
    SequencerDlg.cpp \
    SequencersDlg.cpp \
    ShapeDlg.cpp \
    ShapesDlg.cpp \
    SourceDlg.cpp \
    SourcesDlg.cpp \
    SquareHoleDlg.cpp \
    SquarePlateDlg.cpp \
    SquarePlateGeneDlg.cpp \
    SquarePlateGeneTemplateDlg.cpp \
    TwoDSliceDlg.cpp \
    TwoDSliceView.cpp \
    TwoDSliceViewDlg.cpp \
    VideoEncoder.cpp \
    ViewFrame.cpp \
    ViewFrameDlg.cpp \
    ViewItem.cpp \
    ViewItemDlg.cpp \
    ZoneSourceDlg.cpp \
    FitnessIntensityLineDlg.cpp \
    FitnessInPhaseLineDlg.cpp \
    ArcMaterialDlg.cpp \
    ExtrudedPolygonDlg.cpp \
    CharacterizerDlg.cpp \
    CharacterizerParamsDlg.cpp \
    CharacterizerStepsDlg.cpp \
    CharacterizerSubDlg.cpp \
    IdenticalLayersScannerDlg.cpp \
    ScannerDlg.cpp \
    LayerSpacingScannerDlg.cpp \
    BinaryNxNPlateScannerDlg.cpp \
    MaterialsSelectDlg.cpp \
    SquarePlateScannerDlg.cpp \
    DielectricLayerScannerDlg.cpp \
    LensDesignerDlg.cpp \
    LensDesignerNumLayersTemplateDlg.cpp \
    LensDesignerParamsDlg.cpp \
    LensDesignerPopulationDlg.cpp \
    LensDesignerQuadraticTemplateDlg.cpp \
    LensDesignerSubDlg.cpp \
    LensDesignerTemplateDlg.cpp \
    LensDimensionsGeneDlg.cpp \
    LensDimensionsGeneTemplateDlg.cpp \
    LensSquareGeneDlg.cpp \
    LensSquareGeneTemplateDlg.cpp \
    OneDComplexFitnessDataDlg.cpp \
    OneDDoubleFitnessDataDlg.cpp \
    TwoDDoubleFitnessDataDlg.cpp \
    TwoDSliceWidget.cpp \
    FitnessBirefPhaseDiffDlg.cpp

HEADERS += \
    AdmittanceCatalogueDlg.h \
    AdmittanceCatalogueSelectDlg.h \
    AdmittanceCatLayerGeneDlg.h \
    AdmittanceCatLayerGeneTemplateDlg.h \
    AdmittanceFnLayerDlg.h \
    AdmittanceFnLayerGeneDlg.h \
    AdmittanceFnLayerGeneTemplateDlg.h \
    AdmittanceTableEditDlg.h \
    AreaSensorDlg.h \
    ArraySensorDlg.h \
    AttenuationView.h \
    AttenuationViewDlg.h \
    BinaryCodedPlateDlg.h \
    BinaryCodedPlateGeneDlg.h \
    BinaryCodedPlateGeneTemplateDlg.h \
    BinaryCodedPlateWidget.h \
    BinaryPlateNxNDlg.h \
    BinaryPlateNxNGeneDlg.h \
    BinaryPlateNxNGeneTemplateDlg.h \
    BoundaryMaterialDlg.h \
    CatalogueBrowseDlg.h \
    CatalogueBuilderDlg.h \
    CatalogueBuilderItemsDlg.h \
    CatalogueBuilderParamsDlg.h \
    CatalogueBuilderSubDlg.h \
    ClusterDlg.h \
    ComboBoxDelegate.h \
    CpmlMaterialDlg.h \
    CuboidDlg.h \
    DataSeriesContextDlg.h \
    DataSeriesView.h \
    DataSeriesViewDlg.h \
    DataSeriesWidget.h \
    DefaultMaterialDlg.h \
    DialogHelper.h \
    DielectricBlockGeneDlg.h \
    DielectricBlockGeneTemplateDlg.h \
    DielectricLayerDlg.h \
    DielectricLayerGeneDlg.h \
    DielectricLayerGeneTemplateDlg.h \
    divergingBYColors.h \
    DuplicateLayerGeneDlg.h \
    DuplicateLayerGeneTemplateDlg.h \
    extKindlmannColors.h \
    ExtrudedPlaneDlg.h \
    FarFieldSensorDlg.h \
    FdTdLife.h \
    Fitness3dBPointDlg.h \
    FitnessAdmittanceDlg.h \
    FitnessAreaIntensityDlg.h \
    FitnessBaseDlg.h \
    FitnessFunctionDlg.h \
    FitnessFilterDlg.h \
    GeneDlg.h \
    GeneTemplateDlg.h \
    GeneticSearchGeneTemplateDlg.h \
    GeneticSearchParamsDlg.h \
    GeneticSearchPopulationDlg.h \
    GeneticSearchSeqDlg.h \
    GeneticSearchSubDlg.h \
    GeneticSearchTargetDlg.h \
    GravyWaveSourceDlg.h \
    GuiChangeDetector.h \
    IndividualEditDlg.h \
    InitialisingGui.h \
    LayerPresentGeneDlg.h \
    LayerPresentGeneTemplateDlg.h \
    LayersList.h \
    LayerSpacingGeneDlg.h \
    LayerSpacingGeneTemplateDlg.h \
    LayerSymmetryGeneDlg.h \
    LayerSymmetryGeneTemplateDlg.h \
    MainTabPanel.h \
    materialColors.h \
    MaterialDlg.h \
    MaterialsDlg.h \
    MaterialSensorDlg.h \
    MaterialSensorView.h \
    MaterialSensorViewDlg.h \
    MpiProcessPool.h \
    ParametersCfg.h \
    ParametersDlg.h \
    PhaseShiftView.h \
    PhaseShiftViewDlg.h \
    PlaneWaveSourceDlg.h \
    PlateAdmittanceEditDlg.h \
    PlateStackDlg.h \
    PowerView.h \
    PowerViewDlg.h \
    ProcessingNodeCfg.h \
    PushButtonMenu.h \
    QtIncludes.h \
    RadiationPatternDlg.h \
    RepeatCellGeneDlg.h \
    RepeatGeneTemplateDlg.h \
    SensorArrayView.h \
    SensorArrayViewDlg.h \
    SensorDlg.h \
    SensorsDlg.h \
    SequencerDlg.h \
    SequencersDlg.h \
    ShapeDlg.h \
    ShapesDlg.h \
    SourceDlg.h \
    SourcesDlg.h \
    SquareHoleDlg.h \
    SquarePlateDlg.h \
    SquarePlateGeneDlg.h \
    SquarePlateGeneTemplateDlg.h \
    TwoDSliceDlg.h \
    TwoDSliceView.h \
    TwoDSliceViewDlg.h \
    VideoEncoder.h \
    ViewFrame.h \
    ViewFrameDlg.h \
    ViewItem.h \
    ViewItemDlg.h \
    ZoneSourceDlg.h \
    FitnessIntensityLineDlg.h \
    FitnessInPhaseLineDlg.h \
    ArcMaterialDlg.h \
    ExtrudedPolygonDlg.h \
    CharacterizerDlg.h \
    CharacterizerParamsDlg.h \
    CharacterizerStepsDlg.h \
    CharacterizerSubDlg.h \
    IdenticalLayersScannerDlg.h \
    ScannerDlg.h \
    LayerSpacingScannerDlg.h \
    BinaryNxNPlateScannerDlg.h \
    MaterialsSelectDlg.h \
    SquarePlateScannerDlg.h \
    DielectricLayerScannerDlg.h \
    LensDesignerDlg.h \
    LensDesignerNumLayersTemplateDlg.h \
    LensDesignerParamsDlg.h \
    LensDesignerPopulationDlg.h \
    LensDesignerQuadraticTemplateDlg.h \
    LensDesignerSubDlg.h \
    LensDesignerTemplateDlg.h \
    LensDimensionsGeneDlg.h \
    LensDimensionsGeneTemplateDlg.h \
    LensSquareGeneDlg.h \
    LensSquareGeneTemplateDlg.h \
    SubDialogBase.h \
    OneDComplexFitnessDataDlg.h \
    OneDDoubleFitnessDataDlg.h \
    TwoDDoubleFitnessDataDlg.h \
    TwoDSliceWidget.h \
    FitnessBirefPhaseDiffDlg.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Gui.pro \
    Gui.pro \
    Gui.pro

DISTFILES += \
    Gui.pro.user \
    CMakeLists.txt \
    Makefile \
    Gui.pro.user \
    Makefile \
    Gui.pro.user \
    Makefile

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/ -lLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/ -lLibrary

INCLUDEPATH += $$PWD/../Fdtd
DEPENDPATH += $$PWD/../Fdtd
INCLUDEPATH += ..

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/libLibrary.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/libLibrary.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Release/release/Library.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-Library-Desktop_Qt_5_11_1_MSVC2017_64bit2-Debug/debug/Library.lib

RESOURCES += \
    resources/resources.qrc
