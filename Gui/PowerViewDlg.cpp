/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PowerViewDlg.h"
#include "PowerView.h"

// Constructor
PowerViewDlg::PowerViewDlg(ViewFrameDlg *dlg, PowerView *view) :
        ViewItemDlg(dlg),
        view_(view) {
    auto mainLayout = new QVBoxLayout;
    // X Axis Specification
    auto xAxisLayout = new QHBoxLayout;
    xAutomatic_ = new QCheckBox("Automatic");
    xAutomatic_->setChecked(view->xAutomatic());
    xMin_ = new QLineEdit;
    xMin_->setValidator(new QDoubleValidator);
    xMin_->setText(QString::number(view->xMin()));
    xMax_ = new QLineEdit;
    xMax_->setValidator(new QDoubleValidator);
    xMax_->setText(QString::number(view->xMax()));
    xScaleType_ = new QComboBox;
    xScaleType_->addItems({"Frequency", "Wave Number"});
    xScaleType_->setCurrentIndex((int)view->xScaleType());
    mainLayout->addWidget(new QLabel("X Axis Scaling"));
    xAxisLayout->addWidget(new QLabel("Type:"));
    xAxisLayout->addWidget(xScaleType_);
    xAxisLayout->addWidget(new QLabel("Max:"));
    xAxisLayout->addWidget(xMax_);
    xAxisLayout->addWidget(new QLabel("Min:"));
    xAxisLayout->addWidget(xMin_);
    xAxisLayout->addWidget(xAutomatic_);
    mainLayout->addLayout(xAxisLayout);
    // Y Axis Specification
    auto yAxisLayout = new QHBoxLayout;
    yAutomatic_ = new QCheckBox("Automatic");
    yAutomatic_->setChecked(view->yAutomatic());
    yMin_ = new QLineEdit;
    yMin_->setValidator(new QDoubleValidator);
    yMin_->setText(QString::number(view->yMin()));
    yMax_ = new QLineEdit;
    yMax_->setValidator(new QDoubleValidator);
    yMax_->setText(QString::number(view->yMax()));
    mainLayout->addWidget(new QLabel("Y Axis Scaling"));
    yAxisLayout->addWidget(new QLabel("Max:"));
    yAxisLayout->addWidget(yMax_);
    yAxisLayout->addWidget(new QLabel("Min:"));
    yAxisLayout->addWidget(yMin_);
    yAxisLayout->addWidget(yAutomatic_);
    mainLayout->addLayout(yAxisLayout);
    // Main layout
    mainLayout->addStretch();
    setLayout(mainLayout);
    // Connect handlers
    connect(xScaleType_, SIGNAL(currentIndexChanged(int)), SLOT(onChange()));
    connect(xMin_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xMax_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(xAutomatic_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(yMin_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(yMax_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(yAutomatic_, SIGNAL(stateChanged(int)), SLOT(onChange()));
}

// The tab has changed and focus has moved from this panel
void PowerViewDlg::tabChanged() {
    onChange();
}

// An configuration has changed
void PowerViewDlg::onChange() {
    auto newXScaleType = (PowerView::XScaleType)xScaleType_->currentIndex();
    double newXMin = xMin_->text().toFloat();
    double newXMax = xMax_->text().toFloat();
    bool newXAutomatic = xAutomatic_->isChecked();
    double newYMin = yMin_->text().toFloat();
    double newYMax = yMax_->text().toFloat();
    bool newYAutomatic = yAutomatic_->isChecked();
    view_->set(newXScaleType, newXMin, newXMax, newXAutomatic,
               view_->yScaleType(), newYMin, newYMax, newYAutomatic);
}


