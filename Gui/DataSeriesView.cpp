/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DataSeriesView.h"
#include "DataSeriesContextDlg.h"
#include "ViewFrame.h"
#include "FdTdLife.h"
#include "DataSeriesViewDlg.h"
#include <Xml/DomObject.h>
#include <sstream>

// Required by C++11
constexpr int DataSeriesView::maxNumSeries_;

// Constructor
DataSeriesView::DataSeriesView(int identifier, int sensorId, const char* sensorData,
                               ViewFrame* view, int numSeries) :
        ViewItem(identifier, sensorId, sensorData, view) {
    numSeries_ = std::min(numSeries, maxNumSeries_);
    markerValueY_.resize((size_t)numSeries_, 0.0);
}

// Draw the data for this item
void DataSeriesView::paintData(QPainter* painter) {
    // Do we have the drawing spec?
    if(spec_ != nullptr) {
        // Can we calculate the scaling
        if(calcScaling()) {
            painter->save();
            // Do all the drawing
            painter->setBrush(view_->color(GuiConfiguration::elementBackground));
            painter->setPen(view_->color(GuiConfiguration::elementBackground));
            painter->drawRect(dataArea_);
            drawVerticalAxis(painter);
            drawHorizontalAxis(painter);
            drawData(painter);
            if(markersOn_) {
                drawMarkerInfo(painter);
            }
            painter->restore();
        }
    }
}

// The mouse has been pressed in our data area
void DataSeriesView::leftClick(QPoint pos) {
    // Convert the x clicked position to a marker position
    markerPos_ = (int)round((double)(pos.x() - dataArea_.left()) / timeScale_);
    std::vector<double> dy;
    dy.resize((size_t)numSeries_);
    getData(markerPos_, dy, markerValueX_);
    spec_->setMetaData(sensor::DataSpec::metaCurrentXYPos, markerValueX_, 0.0);
    view()->notifyModel();
}

// Calculate the scaling factors and tick mark intervals.
// Returns true if the data is reasonable and drawing should proceed.
bool DataSeriesView::calcScaling() {
    bool result = false;
    if(spec_ != nullptr) {
        spec_->getInfo(&info_);
        if(info_.xSize_ > 0 && info_.xSize_ <= 645536) {
            result = true;
            // Data (y) axis scaling
            calcYScaling();
            // Y axis tick marks
            dataTickStep_ = pow(10.0, floor(log10(info_.max_ - info_.min_)));
            if ((info_.max_ - info_.min_) / dataTickStep_ < 3.0) {
                dataTickStep_ /= 2.0;
            }
            // Time (x) axis scaling
            calcXScaling();
            // X axis tick marks
            if(info_.xLeft_ != info_.xRight_) {
                double diff = abs(info_.xLeft_ - info_.xRight_);
                timeTickScale_ = (double) dataArea_.width() / diff;
                timeTickStep_ = pow(10.0, floor(log10(diff)));
                if (diff / timeTickStep_ < 3.0) {
                    timeTickStep_ /= 2.0;
                }
                if (diff / timeTickStep_ > 8.0) {
                    timeTickStep_ *= 2.0;
                }
            }
        }
    }
    return result;
}

// Calculate the scaling factors for the Y axis.
void DataSeriesView::calcYScaling() {
    // Data (y) axis scaling
    if(!yAutomatic_ && (yMin_!=yMax_)) {
        info_.max_ = yMax_;
        info_.min_ = yMin_;
    }
    dataScale_ = 1.0;
    if (info_.max_ != info_.min_) {
        dataScale_ = (double) dataArea_.height() / (info_.max_ - info_.min_);
    }
    dataOffset_ = info_.min_;
}

// Calculate the scaling factors for the X axis.
void DataSeriesView::calcXScaling() {
    // Time (x) axis scaling
    timeScale_ = (double) dataArea_.width() / (double)(info_.xSize_ - 1);
}

// Draw the vertical axis with tick marks and annotation
void DataSeriesView::drawVerticalAxis(QPainter* painter) {
    // Draw the vertical axis
    QPen axisPen(view_->color(GuiConfiguration::elementAxis), 1);
    QPen textPen(view_->color(GuiConfiguration::elementText), 1);
    painter->save();
    double tick = (floor(info_.min_ / dataTickStep_) + 1.0) * dataTickStep_;
    for(int i=0; i<20 && tick < info_.max_; i++) {
        QString text = QString("%1%2").arg(tick).arg(info_.yUnits_.c_str());
        int tickY = dataArea_.bottom() - (int)round((tick - dataOffset_) * dataScale_);
        painter->setPen(axisPen);
        painter->drawLine(dataArea_.left(), tickY, dataArea_.right(), tickY);
        painter->setPen(textPen);
        painter->drawText(dataArea_.left()+5, tickY, text);
        tick += dataTickStep_;
    }
    painter->restore();
}

// Draw the horizontal axis with tick marks and annotation
void DataSeriesView::drawHorizontalAxis(QPainter* painter) {
    QPen axisPen(view_->color(GuiConfiguration::elementAxis), 1);
    QPen textPen(view_->color(GuiConfiguration::elementText), 1);
    painter->save();
    if(info_.xSize_ == 1) {
        QString text = QString("%1%2").arg(info_.xLeft_).arg(info_.xUnits_.c_str());
        int tickX = dataArea_.left() + dataArea_.width() / 2;
        painter->setPen(axisPen);
        painter->drawLine(tickX, dataArea_.bottom(), tickX, dataArea_.top());
        painter->setPen(textPen);
        painter->drawText(tickX - 10, dataArea_.bottom() - 5, text);
    } else {
        double tick = (floor(info_.xLeft_ / timeTickStep_) + 1.0) * timeTickStep_;
        if(info_.xLeft_ < info_.xRight_) {
            for(int i = 0; i < 20 && tick < info_.xRight_; i++) {
                QString text = QString("%1%2").arg(tick).arg(info_.xUnits_.c_str());
                int tickX = dataArea_.left() + (int) round((tick - info_.xLeft_) * timeTickScale_);
                painter->setPen(axisPen);
                painter->drawLine(tickX, dataArea_.bottom(), tickX, dataArea_.top());
                painter->setPen(textPen);
                painter->drawText(tickX - 10, dataArea_.bottom() - 5, text);
                tick += timeTickStep_;
            }
        }
    }
    painter->restore();
}

// Get the data points for an X coordinate
void DataSeriesView::getData(int x, std::vector<double>& dy, double& dx) {
    spec_->getData(x, dy, dx);
}

// Draw the data, including the marker
void DataSeriesView::drawData(QPainter* painter) {
    // Make the pens
    std::vector<QPen> pen;
    std::vector<QPen> markerPen;
    for(size_t i=0; i<(size_t)numSeries_; i++) {
        pen.emplace_back(view_->color((GuiConfiguration::Element)(GuiConfiguration::elementDataA+i)), 1);
        markerPen.emplace_back(view_->color((GuiConfiguration::Element)(GuiConfiguration::elementMarkerA+i)), 1);
    }
    // Draw the data
    painter->save();
    int x1, x2;
    std::vector<int> y1, y2;
    std::vector<double> dy;
    y1.resize((size_t)numSeries_);
    y2.resize((size_t)numSeries_);
    dy.resize((size_t)numSeries_);
    double dx = 0.0;
    getData(0, dy, dx);
    x2 = dataArea_.left();
    for(size_t i=0; i<(size_t)numSeries_; i++) {
        y2[i] = dataArea_.bottom() - (int)round((dy[i] - dataOffset_) * dataScale_);
        y2[i] = std::min(y2[i], dataArea_.bottom());
        y2[i] = std::max(y2[i], dataArea_.top());
    }
    if(info_.xSize_ == 1) {
        // Only one data point, draw a bar
        int leftX = dataArea_.left() + dataArea_.width() / 4;
        int rightX = leftX + dataArea_.width() / 2;
        int bottomY = dataArea_.bottom();
        for(int i = numSeries_ - 1; i >= 0; i--) {
            if(info_.channelUsed((size_t) i)) {
                painter->setPen(pen[i]);
                painter->drawLine(leftX, bottomY, leftX, y2[i]);
                painter->drawLine(leftX, y2[i], rightX, y2[i]);
                painter->drawLine(rightX, y2[i], rightX, bottomY);
                markerValueY_[i] = dy[i];
            }
        }
        markerValueX_ = dx;
    } else {
        for(size_t t = 1; t < info_.xSize_; t++) {
            x1 = x2;
            for(size_t i = 0; i < (size_t) numSeries_; i++) {
                y1[i] = y2[i];
            }
            x2 = (int) round((double) t * timeScale_) + dataArea_.left();
            x2 = std::min(x2, dataArea_.right());
            x2 = std::max(x2, dataArea_.left());
            getData(t, dy, dx);
            for(size_t i = 0; i < (size_t) numSeries_; i++) {
                y2[i] = dataArea_.bottom() - (int) round((dy[i] - dataOffset_) * dataScale_);
                y2[i] = std::min(y2[i], dataArea_.bottom());
                y2[i] = std::max(y2[i], dataArea_.top());
            }
            for(int i = numSeries_ - 1; i >= 0; i--) {
                if(info_.channelUsed((size_t) i)) {
                    painter->setPen(pen[i]);
                    painter->drawLine(x1, y1[i], x2, y2[i]);
                }
            }
            if(markersOn_ && static_cast<int>(t) == markerPos_) {
                markerValueX_ = dx;
                for(size_t i = 0; i < (size_t) numSeries_; i++) {
                    markerValueY_[i] = dy[i];
                    if(info_.channelUsed(i)) {
                        painter->setPen(markerPen[i]);
                        painter->drawRect(x2 - 2, y2[i] - 2, 4, 4);
                    }
                }
            }
        }
    }
    painter->restore();
}

// Draw the marker information
void DataSeriesView::drawMarkerInfo(QPainter* painter) {
    QFontInfo info = painter->fontInfo();
    QPen markerPen(view_->color(GuiConfiguration::elementText), 1);
    painter->save();
    painter->setPen(markerPen);
    int x = dataArea_.center().x();
    int y = dataArea_.top() + info.pixelSize();
    QString text;
    text = QString("%1%2").arg(markerValueX_).arg(info_.xUnits_.c_str());
    painter->drawText(x,y,text);
    for(size_t i=0; i<(size_t)numSeries_; i++) {
        y += info.pixelSize();
        if(info_.channelUsed((size_t)i)) {
            text = QString("%1%2").arg(markerValueY_[i]).arg(info_.yUnits_.c_str());
        } else {
            text = "-";
        }
        painter->drawText(x,y,text);
    }
    painter->restore();
}

// Display the context dialog
void DataSeriesView::contextMenu(QPoint /*pos*/) {
    DataSeriesContextDlg box(this);
    box.exec();
}

// Return a desired view size.
// Note that at this point, initialise has not been called.
QSize DataSeriesView::sizeHint() {
    QSize result = QSize(20,10); // We just want a horizontal area
    return result;
}

// We are being offered the given size.  We can modify this
// to indicate what size we would like.
void DataSeriesView::sizeRequest(QSize& size) {
    size.setWidth(std::max(20, size.width()));
    size.setHeight(std::max(10, size.height()));
}

// Make the sub dialog for this view item
ViewItemDlg *DataSeriesView::makeDialog(ViewFrameDlg *dlg) {
    return new DataSeriesViewDlg(dlg, this);
}

// Update the view's configuration
void DataSeriesView::set(double xMin, double xMax, bool xAutomatic,
                         double yMin, double yMax, bool yAutomatic) {
    xMin_ = xMin;
    xMax_ = xMax;
    xAutomatic_ = xAutomatic;
    yMin_ = yMin;
    yMax_ = yMax;
    yAutomatic_ = yAutomatic;
}

// Write the configuration to an XML file
void DataSeriesView::writeConfig(xml::DomObject& root) const {
    // Base class first
    ViewItem::writeConfig(root);
    // Now my things
    root << xml::Reopen();
    root << xml::Obj("xmin") << xMin_;
    root << xml::Obj("xmax") << xMax_;
    root << xml::Obj("xautomatic") << xAutomatic_;
    root << xml::Obj("ymin") << yMin_;
    root << xml::Obj("ymax") << yMax_;
    root << xml::Obj("yautomatic") << yAutomatic_;
    root << xml::Close();
}

// Read the configuration from an XML file
void DataSeriesView::readConfig(xml::DomObject& root) {
    // Base class first
    ViewItem::readConfig(root);
    // Now my things
    root >> xml::Reopen();
    root >> xml::Obj("xmin") >> xMin_;
    root >> xml::Obj("xmax") >> xMax_;
    root >> xml::Obj("xautomatic") >> xAutomatic_;
    root >> xml::Obj("ymin") >> yMin_;
    root >> xml::Obj("ymax") >> yMax_;
    root >> xml::Obj("yautomatic") >> yAutomatic_;
    root >> xml::Close();
}

// Put the data into the MIME data object
void DataSeriesView::getMimeData(QMimeData* mimeData) {
    std::stringstream text;
    text << "x";
    for(int i=0; i<numSeries_; i++) {
        text << "\ty" << i+1;
    }
    text << std::endl;
    std::vector<double> dy;
    dy.resize((size_t)numSeries_);
    double dx = 0.0;
    for(size_t t=0; t<info_.xSize_; t++) {
        getData(t, dy, dx);
        text << dx;
        for(size_t i=0; i<(size_t)numSeries_; i++) {
            text << "\t" << dy[i];
        }
        text << std::endl;
    }
    mimeData->setText(text.str().c_str());
}
