/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GUICHANGEDETECTOR_H
#define FDTDLIFE_GUICHANGEDETECTOR_H

#include "QtIncludes.h"
#include <string>
#include <cstdint>
#include <Box/Vector.h>
#include <Box/ComponentSel.h>
#include <Box/Polynomial.h>
#include <Gs/GeneBitsSpecDouble.h>
#include <Gs/GeneBitsSpecLogDouble.h>
#include <Gs/GeneBitsSpecInteger.h>
#include <Ga/GeneBitsInteger.h>
#include <Ga/GeneBitsFloat.h>

// A class used to detect changes a user may have made in a dialog panel
class GuiChangeDetector {
public:
    GuiChangeDetector();
    std::string testString(QLineEdit* widget, const std::string& original, int why);
    double testDouble(QLineEdit* widget, double original, int why);
    uint64_t testUint64T(QLineEdit* widget, uint64_t original, int why, int base = 10);
    size_t testSizeT(QLineEdit* widget, size_t original, int why, int base = 10);
    int testInt(QLineEdit* widget, int original, int why, int base = 10);
    int testInt(QComboBox* widget, int original, int why);
    int testInt(QSpinBox* widget, int original, int why);
    bool testBool(QCheckBox* widget, bool original, int why);
    box::Vector<double> testVectorDouble(QLineEdit* widgetX, QLineEdit* widgetY,
                                          QLineEdit* widgetZ,
                                          const box::Vector<double>& original, int why);
    box::ComponentSel testComponentSel(QCheckBox* widgetX, QCheckBox* widgetY,
                                       QCheckBox* widgetZ,
                                       const box::ComponentSel& original, int why);
    box::Polynomial testPolynomial(QLineEdit* widgetA, QLineEdit* widgetB,
                                    QLineEdit* widgetC, QLineEdit* widgetD,
                                    QLineEdit* widgetE, QLineEdit* widgetF,
                                    const box::Polynomial& original, int why);
    gs::GeneBitsSpecDouble testGeneBitsSpecDouble(
            QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
            const gs::GeneBitsSpecDouble& original, int why);
    gs::GeneBitsSpecLogDouble testGeneBitsSpecLogDouble(
            QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
            const gs::GeneBitsSpecLogDouble& original, int why);
    template <class T>
    gs::GeneBitsSpecInteger<T> testGeneBitsSpecInteger(
            QLineEdit* widgetBits, QLineEdit* widgetMin,
            const gs::GeneBitsSpecInteger<T>& original, int why);
    template <class T>
    ga::GeneBitsInteger<T> testGeneBitsInteger(
            QLineEdit* widgetBits, QLineEdit* widgetMin,
            const ga::GeneBitsInteger<T>& original, int why);
    ga::GeneBitsFloat testGeneBitsFloat(
            QLineEdit* widgetBits, QLineEdit* widgetMin, QLineEdit* widgetMax,
            const ga::GeneBitsFloat& original, int why);
    void externalTest(bool result, int why);
    bool isChanged() const { return changeDetected_; }
    int why() const { return why_; }
protected:
    bool changeDetected_;
    int why_;
};


#endif //FDTDLIFE_GUICHANGEDETECTOR_H
