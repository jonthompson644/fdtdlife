/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VECTORFIELDVIEW_H
#define FDTDLIFE_VECTORFIELDVIEW_H

#include "QtIncludes.h"
#include <Fdtd/Model.h>
#include <Fdtd/Notifiable.h>

class SourcesDlg;
class ConfigurationDlg;
class VariablesDlg;
class MaterialsDlg;
class ShapesDlg;
class SensorsDlg;
class GuiConfiguration;
class ViewFrame;
class ViewFrameDlg;
class SequencersDlg;
class DesignersDlg;
class AdmittanceCatalogueDlg;
class ProcessingNodeCfg;
class AnalysersDlg;

// The main Qt FDTD model
class FdTdLife: public QMainWindow, public fdtd::Model, public fdtd::Notifiable {
    Q_OBJECT

public:
    FdTdLife();
    ~FdTdLife() override;

private slots:
    void onNewFile();
    void onSaveFile();
    void onSaveAsFile();
    void onOpenFile();
    void onStepModel();
    void onPlayModel();
    void onReplayModel();
    void onStopModel();
    void timerExpiry();
    void onTabChange(int index);
    void onWriteData();
    void onReadData();
    void onAnimate();

public:
    enum {
        notifyInitialisation=notifyNextAvailableCode, notifyDataRead,
        notifyMinorChange, notifyDomainSizeChange, notifyDomainContentsChange,
        notifyMinorNoViews, notifyIndividualLoaded
    };

protected:
    bool modelModified_;
    QString currentFile_;
    QTimer* timer_;
    enum {runStateStopped, runStatePaused, runStateRunning, runStateAnimating} runState_;

    std::list<ViewFrame*> views_;
    int nextViewIdentifier_;
    std::list<ProcessingNodeCfg*> nodes_;
    GuiConfiguration* guiConfiguration_;

    SourcesDlg* sourcesDlg_;
    ConfigurationDlg* configurationDlg_;
    VariablesDlg* variablesDlg_;
    MaterialsDlg* materialsDlg_;
    ShapesDlg* shapesDlg_;
    SensorsDlg* sensorsDlg_;
    AnalysersDlg* analysersDlg_;
    ViewFrameDlg* viewsDlg_;
    SequencersDlg* sequencersDlg_;
    DesignersDlg* designersDlg_;
    AdmittanceCatalogueDlg* admittanceCatDlg_;
    QTabWidget* tabs_;
    
    void createActions();
    bool maybeSave();
    void doSaveFile(QString filename, bool selectFile);
    void clear() override;
    void setTitle();
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;
    std::ofstream* openSensorCsvFile();
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;

public:
    void initialise() override;
    void initialiseModel();
    bool isStopped() const {
        return runState_ == runStatePaused || runState_ == runStateStopped;
    }
    int allocViewIdentifier() {return nextViewIdentifier_++;}

public:
    std::list<ViewFrame*>& views() {return views_;}
    std::list<ProcessingNodeCfg*>& nodes() {return nodes_;}
    GuiConfiguration* guiConfiguration() {return guiConfiguration_;}
    void addCfg(ViewFrame *v);
    void addCfg(ProcessingNodeCfg* node);
    void removeCfg(ViewFrame *v);
    void removeCfg(ProcessingNodeCfg* node);
    ProcessingNodeCfg* findProcessingNodeCfg(int id);
    void fillMaterialList(QComboBox* list, int selectedId) const;
    void fillVariableList(QComboBox* list, int selectedId);
    void fillSensorList(QComboBox* list, int selectedId);
    void fillAnalyserList(QComboBox* list, int selectedId);
    int selected(QComboBox* list) const;
    void writeSensorCsvFile(sensor::Sensor* sensor);
    void matchModelConfig();
    QString getCurrentFileName() const {return currentFile_;}
    ViewFrame* getView(int identifier);
    void setStopped();
};


#endif //FDTDLIFE_VECTORFIELDVIEW_H
