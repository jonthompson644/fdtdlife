/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CuboidDlg.h"
#include "DialogHelper.h"
#include <Domain/Cuboid.h>
#include "ShapesDlg.h"
#include "GuiChangeDetector.h"

// Constructor
void CuboidDlg::construct(ShapesDlg* dlg, domain::Shape* shape) {
    ShapeDlg::construct(dlg, shape);
    shape_ = dynamic_cast<domain::Cuboid*>(shape);
    std::tie(x1Edit_, y1Edit_, z1Edit_) = DialogHelper::vectorItem(
            layout_, "Bottom Corner x,y,z (m)");
    std::tie(x2Edit_, y2Edit_, z2Edit_) = DialogHelper::vectorItem(
            layout_, "Top Corner x,y,z (m)");
    this->connect(x1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(y1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(z1Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(x2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(y2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
    this->connect(z2Edit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void CuboidDlg::initialise() {
    ShapeDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    x1Edit_->setText(QString::number(shape_->p1().x()));
    y1Edit_->setText(QString::number(shape_->p1().y()));
    z1Edit_->setText(QString::number(shape_->p1().z()));
    x2Edit_->setText(QString::number(shape_->p2().x()));
    y2Edit_->setText(QString::number(shape_->p2().y()));
    z2Edit_->setText(QString::number(shape_->p2().z()));
}

// An edit box has changed
void CuboidDlg::onChange() {
    ShapeDlg::onChange();
    if(!dlg_->initialising() && dlg_->model()->isStopped()) {
        // Get the new values
        GuiChangeDetector c;
        auto newP1 = c.testVectorDouble(x1Edit_, y1Edit_, z1Edit_,
                                        shape_->p1(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newP2 = c.testVectorDouble(x2Edit_, y2Edit_, z2Edit_,
                                        shape_->p2(),
                                        FdTdLife::notifyDomainContentsChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            shape_->p1(newP1);
            shape_->p2(newP2);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void CuboidDlg::tabChanged() {
    onChange();
}
