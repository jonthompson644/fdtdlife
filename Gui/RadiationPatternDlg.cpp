/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RadiationPatternDlg.h"
#include "DialogHelper.h"
#include <Sensor/RadiationPattern.h>
#include "FdTdLife.h"
#include "SensorsDlg.h"
#include "GuiChangeDetector.h"

// Constructor
RadiationPatternDlg::RadiationPatternDlg(SensorsDlg* dlg, sensor::Sensor* sensor) :
        SensorDlg(dlg, sensor),
        sensor_(dynamic_cast<sensor::RadiationPattern*>(sensor)) {
    auto line1Layout = new QHBoxLayout;
    std::tie(centerXEdit_, centerYEdit_, centerZEdit_) =
            DialogHelper::vectorItem(line1Layout, "Center x,y,z (m)");
    layout_->addLayout(line1Layout);
    auto line2Layout = new QHBoxLayout;
    positiveSemicircleCheck_ = DialogHelper::boolItem(line2Layout, "Positive Half");
    distanceEdit_ = DialogHelper::doubleItem(line2Layout, "Distance (m)");
    numPointsSpin_ = DialogHelper::intSpinItem(line2Layout, "Num Points", 1, 1000, 10);
    layout_->addLayout(line2Layout);
    auto line3Layout = new QHBoxLayout;
    patternFrequencyEdit_ = DialogHelper::doubleItem(line3Layout, "Radiation Pattern: Frequency (Hz)");
    patternPolarisationEdit_ = DialogHelper::doubleItem(line3Layout, "Polarisation (deg)");
    layout_->addLayout(line3Layout);
    connect(centerXEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerYEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(centerZEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(distanceEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(patternFrequencyEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(patternPolarisationEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(positiveSemicircleCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(numPointsSpin_, SIGNAL(valueChanged(int)), SLOT(onChange()));
}

// Initialise the GUI parameters
void RadiationPatternDlg::initialise() {
    SensorDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    centerXEdit_->setText(QString::number(sensor_->center().x()));
    centerYEdit_->setText(QString::number(sensor_->center().y()));
    centerZEdit_->setText(QString::number(sensor_->center().z()));
    positiveSemicircleCheck_->setChecked(sensor_->positiveSemicircle());
    numPointsSpin_->setValue((int) sensor_->numPoints());
    distanceEdit_->setText(QString::number(sensor_->distance()));
    patternFrequencyEdit_->setText(QString::number(sensor_->patternFrequency()));
    patternPolarisationEdit_->setText(QString::number(sensor_->patternPolarisation()));
}

// An edit box has changed
void RadiationPatternDlg::onChange() {
    SensorDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newCenter = c.testVectorDouble(centerXEdit_, centerYEdit_, centerZEdit_,
                                            sensor_->center(),
                                            FdTdLife::notifyDomainContentsChange);
        auto newPositiveSemicircle = c.testBool(positiveSemicircleCheck_,
                                                sensor_->positiveSemicircle(),
                                                FdTdLife::notifyDomainContentsChange);
        auto newDistance = c.testDouble(distanceEdit_, sensor_->distance(),
                                        FdTdLife::notifyDomainContentsChange);
        auto newNumPoints = (size_t)c.testInt(numPointsSpin_, (int)sensor_->numPoints(),
                                      FdTdLife::notifyDomainContentsChange);
        auto newPatternFrequency = c.testDouble(patternFrequencyEdit_, sensor_->patternFrequency(),
                                                FdTdLife::notifyMinorChange);
        auto newPatternPolarisation = c.testDouble(patternPolarisationEdit_, sensor_->patternPolarisation(),
                                                FdTdLife::notifyMinorChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            sensor_->center(newCenter);
            sensor_->positiveSemicircle(newPositiveSemicircle);
            sensor_->distance(newDistance);
            sensor_->numPoints(newNumPoints);
            sensor_->patternFrequency(newPatternFrequency);
            sensor_->patternPolarisation(newPatternPolarisation);
            dlg_->model()->doNotification(c.why());
        }
    }
}

// The tab has lost focus for some reason, there may be changes
// that need processing
void RadiationPatternDlg::tabChanged() {
    onChange();
}

// The model has changed
void RadiationPatternDlg::modelNotify() {
}
