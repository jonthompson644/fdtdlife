/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricLayerGeneDlg.h"
#include "DialogHelper.h"
#include <Gs/DielectricLayerGene.h>

// Constructor
DielectricLayerGeneDlg::DielectricLayerGeneDlg(IndividualEditDlg *mainDlg, gs::DielectricLayerGene *gene) :
        GeneDlg(mainDlg),
        gene_(gene) {
    auto paramLayout = new QVBoxLayout;
    // Make the elements
    refractiveIndexEdit_ = DialogHelper::doubleItem(paramLayout, "Refractive Index", gene_->refractiveIndex());
    thicknessEdit_ = DialogHelper::doubleItem(paramLayout, "Thickness", gene_->thickness());
    paramLayout->addStretch();
    setLayout(paramLayout);
}

// Update the individual from the dialog contents
void DielectricLayerGeneDlg::updateIndividual() {
    gene_->refractiveIndex(refractiveIndexEdit_->text().toDouble());
    gene_->thickness(thicknessEdit_->text().toDouble());
}
