/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include <Sensor/TwoDSlice.h>
#include "TwoDSliceView.h"
#include "extKindlmannColors.h"
#include "materialColors.h"
#include "divergingBYColors.h"
#include "ViewFrame.h"
#include "FdTdLife.h"
#include "TwoDSliceViewDlg.h"
#include "QtHexagon.h"
#include <Xml/DomObject.h>
#include <Box/Constants.h>

// Constructor
TwoDSliceView::TwoDSliceView(int identifier, int sensorId, const char* sensorData,
                             ViewFrame* view) :
        ViewItem(identifier, sensorId, sensorData, view) {
}

// Destructor
TwoDSliceView::~TwoDSliceView() {
    clearColorMap();
}

// Initialise ready for the run
void TwoDSliceView::initialise(int left, int top, int width, int height) {
    // Base class first
    ViewItem::initialise(left, top, width, height);
    // Now my stuff
    makeColorMap();
}

// Draw the data for this item
void TwoDSliceView::paintData(QPainter* suppliedPainter) {
    if(nSliceX_ > 0 && nSliceY_ > 0 && spec_ != nullptr) {
        if(nSliceX_ > 8192 || nSliceY_ > 8192) {
            std::cout << "Bad 2D sizes" << std::endl;
        } else {
            paintSquareData(suppliedPainter);
        }
    }
}

// Draw the square data for this item
void TwoDSliceView::paintSquareData(QPainter* suppliedPainter) {
    // Work out the cell sizes
    cellSizeX_ = dataArea_.width() / nSliceX_;
    cellSizeY_ = dataArea_.height() / nSliceY_;
    cellSizeX_ = std::min(cellSizeX_, cellSizeY_);
    cellSizeY_ = cellSizeX_;
    int cellSize = pixmapCellSize_;
    if(cellSizeX_ > 16) {
        cellSize = cellSizeX_;
    }
    // Create a pixmap to draw into.
    std::unique_ptr<QPixmap> pixmap;
    std::unique_ptr<QPainter> pixmapPainter;
    pixmap.reset(new QPixmap(nSliceX_ * cellSize, nSliceY_ * cellSize));
    pixmapPainter.reset(new QPainter(pixmap.get()));
    QPainter* painter = pixmapPainter.get();
    // Now paint into the pixmap
    painter->save();
    for(int i = 0; i < nSliceX_; i++) {
        for(int j = 0; j < nSliceY_; j++) {
            double d, dx, dy;
            spec_->getData(static_cast<size_t>(i), static_cast<size_t>(j),
                           &d, &dx, &dy);
            auto b = static_cast<int>(floor((d - offset_) / scale_));
            if(b < 0) {
                b = 0;
            }
            if(b >= brushes_.size()) {
                b = brushes_.size() - 1;
            }
            painter->setBrush(*brushes_[b]);
            if(cellSizeX_ > 8) {
                painter->setPen(Qt::blue);
            } else {
                painter->setPen(Qt::NoPen);
            }
            painter->drawRect(i * cellSize,
                              j * cellSize,
                              cellSize, cellSize);
            if(cellSizeX_ > 16) {
                QString text;
                QTextStream t(&text);
                t << i << "," << j << "," << sliceOffset_ << "\n";
                QString val = QString::number(d, 'g', 4);
                t << val.replace("e", "\ne");
                painter->setBrush(Qt::gray);
                painter->setPen(Qt::gray);
                painter->setFont(QFont("arial", 8));
                painter->drawText(QRect(i * cellSize,
                                        j * cellSize,
                                        cellSize, cellSize), text);
            }
        }
    }
    // Copy the pixmap onto the screen with stretching
    suppliedPainter->drawPixmap(dataArea_, *pixmap,
                                QRect(0, 0, nSliceX_ * cellSize, nSliceY_ * cellSize));
    // Make sure no color map brush is left selected.
    painter->setBrush(Qt::gray);
    painter->restore();
}

// Clear the color map
void TwoDSliceView::clearColorMap() {
    for(auto brush: brushes_) {
        delete brush;
    }
    brushes_.clear();
}

// Make the color map brushes
bool TwoDSliceView::makeColorMap() {
    bool result = false;
    clearColorMap();
    if(spec_ != nullptr) {
        spec_->getInfo(&info_);
        result = true;
        nSliceX_ = static_cast<int>(info_.xSize_);
        nSliceY_ = static_cast<int>(info_.ySize_);
        sliceOffset_ = info_.zPos_;
        switch(colorMap_) {
            case colorMapExtKindlemann:
                brushes_.resize(NUMEXTKINDLEMANNCOLORS);
                for(int i = 0; i < NUMEXTKINDLEMANNCOLORS; i++) {
                    brushes_[i] = new QBrush(QColor(extKindlemannColors[i].r,
                                                    extKindlemannColors[i].g,
                                                    extKindlemannColors[i].b));
                }
                scale_ = displayMax_ /
                         static_cast<double>(NUMEXTKINDLEMANNCOLORS);
                offset_ = 0;
                break;
            case colorMapDivergingBY:
                brushes_.resize(NUMDIVERGINGBYCOLORS);
                for(int i = 0; i < NUMDIVERGINGBYCOLORS; i++) {
                    brushes_[i] = new QBrush(QColor(divergingBYColors[i].r,
                                                    divergingBYColors[i].g,
                                                    divergingBYColors[i].b));
                }
                scale_ = displayMax_ * 2.0 /
                         static_cast<double>(NUMDIVERGINGBYCOLORS);
                offset_ = -displayMax_;
                break;
            case colorMapMaterial:
                brushes_.resize(NUMMATERIALCOLORS);
                for(int i = 0; i < NUMMATERIALCOLORS; i++) {
                    brushes_[i] = new QBrush(QColor(materialColors[i].r,
                                                    materialColors[i].g,
                                                    materialColors[i].b));
                }
                scale_ = 1.0;
                offset_ = 0.0;
                break;
        }
    }
    return result;
}

// Return a desired view size.
// Note that at this point, initialise has not been called.
QSize TwoDSliceView::sizeHint() {
    QSize result = QSize(0, 0);
    spec_ = view_->model()->sensors().getDataSpec(sensorId_, sensorData_.c_str());
    if(spec_ != nullptr) {
        spec_->getInfo(&info_);
        result = QSize(static_cast<int>(info_.xSize_),
                       static_cast<int>(info_.ySize_));
    }
    return result;
}

// We are being offered the given size.  We can modify this
// to indicate what size we would like.
void TwoDSliceView::sizeRequest(QSize& size) {
    // Is this a vertical or horizontal panel?
    if(info_.xSize_ < info_.ySize_) {
        // Vertical panel, calculate x size depending on the y
        int yCellSize = std::max(
                (size.height() - 2) / static_cast<int>(info_.ySize_) / ((repeatY_ < 1) ? 1 : repeatY_),
                1);
        size.setWidth(yCellSize * static_cast<int>(info_.xSize_) * repeatX_ + 2);
    } else {
        // Horizontal panel, calculate the y size depending on the x
        int xCellSize = std::max(
                (size.width() - 2) / static_cast<int>(info_.xSize_) / ((repeatX_ < 1) ? 1 : repeatX_),
                1);
        size.setHeight(xCellSize * static_cast<int>(info_.ySize_) * repeatY_ + 2);
    }
}

// Make the sub dialog for this view item
ViewItemDlg* TwoDSliceView::makeDialog(ViewFrameDlg* dlg) {
    return new TwoDSliceViewDlg(dlg, this);
}

// Update the view's configuration
void TwoDSliceView::set(ColorMap colorMap, int repeatX, int repeatY,
                        double displayMax) {
    colorMap_ = colorMap;
    repeatX_ = repeatX;
    repeatY_ = repeatY;
    displayMax_ = displayMax;
}

// Write the configuration to an XML DOM
void TwoDSliceView::writeConfig(xml::DomObject& root) const {
    // Base class first
    ViewItem::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("colormap") << colorMap_;
    root << xml::Obj("repeatx") << repeatX_;
    root << xml::Obj("repeaty") << repeatY_;
    root << xml::Obj("displaymax") << displayMax_;
    root << xml::Close();
}

// Read the configuration from an XML DOM
void TwoDSliceView::readConfig(xml::DomObject& root) {
    // Base class first
    ViewItem::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("colormap") >> xml::Default(colorMap_) >> colorMap_;
    root >> xml::Obj("repeatx") >> xml::Default(repeatX_) >> repeatX_;
    root >> xml::Obj("repeaty") >> xml::Default(repeatY_) >> repeatY_;
    root >> xml::Obj("displaymax") >> xml::Default(displayMax_) >> displayMax_;
    root >> xml::Close();
}

// The mouse has been pressed in our data area
void TwoDSliceView::leftClick(QPoint pos) {
    // Convert the x clicked position to a data coordinate
    double cellSizeX = static_cast<double>(dataArea_.width()) / nSliceX_;
    double cellSizeY = static_cast<double>(dataArea_.height()) / nSliceY_;
    size_t posX = pos.x() / cellSizeX;
    size_t posY = pos.y() / cellSizeY;
    double data;
    double dx;
    double dy;
    spec_->getData(posX, posY, &data, &dx, &dy);
    std::cout << "Data at " << posX << "," << posY << " is " << data << std::endl;
}

