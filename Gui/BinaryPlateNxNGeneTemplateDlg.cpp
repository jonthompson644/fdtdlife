/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPlateNxNGeneTemplateDlg.h"
#include "DialogHelper.h"
#include "GeneticSearchGeneTemplateDlg.h"
#include "FdTdLife.h"
#include "GuiChangeDetector.h"
#include <Gs/GeneTemplate.h>

// Constructor
BinaryPlateNxNGeneTemplateDlg::BinaryPlateNxNGeneTemplateDlg(GeneticSearchGeneTemplateDlg* dlg,
                                                             gs::GeneTemplate* geneTemplate) :
        GeneTemplateDlg(dlg, geneTemplate) {
    fourFoldSymmetryCheck_ = DialogHelper::boolItem(layout_, "Four fold symmetry");
    std::tie(repeatBitsEdit_, repeatMinEdit_) =
            DialogHelper::geneBitsSpecIntMinItem(layout_, "Repeat Count:");
    bitsPerHalfSideEdit_ = DialogHelper::intItem(layout_, "Bits Per Half Side");
    connect(fourFoldSymmetryCheck_, SIGNAL(stateChanged(int)), SLOT(onChange()));
    connect(repeatBitsEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(repeatMinEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
    connect(bitsPerHalfSideEdit_, SIGNAL(editingFinished()), SLOT(onChange()));
}

// Initialise the GUI parameters
void BinaryPlateNxNGeneTemplateDlg::initialise() {
    GeneTemplateDlg::initialise();
    InitialisingGui::Set s(dlg_->initialising());
    fourFoldSymmetryCheck_->setChecked(geneTemplate_->fourFoldSymmetry());
    repeatBitsEdit_->setText(QString::number(geneTemplate_->repeatBits()));
    repeatMinEdit_->setText(QString::number(geneTemplate_->repeatMin()));
    bitsPerHalfSideEdit_->setText(QString::number(geneTemplate_->bitsPerHalfSide()));
}

// An edit box has changed
void BinaryPlateNxNGeneTemplateDlg::onChange() {
    GeneTemplateDlg::onChange();
    if(!dlg_->initialising()) {
        // Get the new values
        GuiChangeDetector c;
        auto newFourFoldSymmetry = c.testBool(fourFoldSymmetryCheck_, geneTemplate_->fourFoldSymmetry(),
                                              FdTdLife::notifySequencerChange);
        auto newRepeatBits = c.testSizeT(repeatBitsEdit_, geneTemplate_->repeatBits(),
                                         FdTdLife::notifySequencerChange);
        auto newRepeatMin = c.testInt(repeatMinEdit_, geneTemplate_->repeatMin(),
                                      FdTdLife::notifySequencerChange);
        auto newBitsPerHalfSide = c.testInt(bitsPerHalfSideEdit_, geneTemplate_->bitsPerHalfSide(),
                                            FdTdLife::notifySequencerChange);
        // Make the changes
        if(c.isChanged()) {
            InitialisingGui::Set s(dlg_->initialising());
            geneTemplate_->set(geneTemplate_->geneType(),
                               newFourFoldSymmetry, newBitsPerHalfSide, newRepeatBits,
                               newRepeatMin, {});
            dlg_->model()->doNotification(c.why());
        }
    }
}

