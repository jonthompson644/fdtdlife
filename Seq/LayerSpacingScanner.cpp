/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "LayerSpacingScanner.h"
#include "Characterizer.h"
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Xml/DomDocument.h>

// Load the model with this step
void seq::LayerSpacingScanner::loadIntoModel(size_t step) {
    // Calculate the current layer spacing
    spacing_ = increment_ * static_cast<double>(step);
    nextOffset_ = box::Vector<double>(0.0);
}

// Write the configuration to the XML
void seq::LayerSpacingScanner::write(xml::DomObject& o) const {
    // Base class first
    Scanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Obj("incx") << increment_.x();
    o << xml::Obj("incy") << increment_.y();
    o << xml::Obj("incz") << increment_.z();
    o << xml::Close();
}

// Read the configuration from the XML
void seq::LayerSpacingScanner::read(xml::DomObject& o) {
    // Base class first
    Scanner::read(o);
    // Now my stuff
    double v;
    o >> xml::Reopen();
    o >> xml::Obj("incx") >> v; increment_.x(v);
    o >> xml::Obj("incy") >> v; increment_.y(v);
    o >> xml::Obj("incz") >> v; increment_.z(v);
    o >> xml::Close();
}

// Return the next offset and increment it ready for the next call
box::Vector<double> seq::LayerSpacingScanner::nextOffset() {
    box::Vector<double> result = nextOffset_;
    nextOffset_ += spacing_;
    std::cout << "nextOffset: " << spacing_ << std::endl;
    return result;
}
