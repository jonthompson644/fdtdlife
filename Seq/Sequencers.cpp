/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AEfrafaND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Sequencers.h"
#include "Sequencer.h"
#include "SingleSeq.h"
#include "Characterizer.h"
#include <Gs/GeneticSearch.h>
#include <Fdtd/CatalogueBuilder.h>

// Constructor
seq::Sequencers::Sequencers(fdtd::Model* m) :
        m_(m) {
    sequencerFactory_.define(new box::Factory<Sequencer>::Creator<SingleSeq>(
            modeSingle, "Single Job"));
    sequencerFactory_.define(new box::Factory<Sequencer>::Creator<gs::GeneticSearch>(
            modeGeneticSearch, "Genetic Search"));
    sequencerFactory_.define(new box::Factory<Sequencer>::Creator<fdtd::CatalogueBuilder>(
            modeCatalogueBuilder, "Catalogue Builder"));
    sequencerFactory_.define(new box::Factory<Sequencer>::Creator<Characterizer>(
            modeCharacterizer, "Characterizer"));
}

// Clear all the sequencers
void seq::Sequencers::clear() {
    sequencers_.clear();
    sequencerFactory_.clear();
}

// Make a sequencer using the factory
std::shared_ptr<seq::Sequencer> seq::Sequencers::makeSequencer(int mode) {
    std::shared_ptr<Sequencer> s(sequencerFactory_.make(mode, m_));
    add(s);
    return s;
}

// Make a sequencer using the factory
std::shared_ptr<seq::Sequencer> seq::Sequencers::makeSequencer(int id, int mode) {
    std::shared_ptr<Sequencer> s(sequencerFactory_.createOne(id, mode, m_));
    add(s);
    return s;
}

// Make a sequencer using the factory
std::shared_ptr<seq::Sequencer> seq::Sequencers::makeSequencer(xml::DomObject& o) {
    std::shared_ptr<Sequencer> s(sequencerFactory_.restore(o, m_));
    add(s);
    return s;
}

// Add a sequencer
void seq::Sequencers::add(const std::shared_ptr<Sequencer>& sequencer) {
    sequencers_.push_back(sequencer);
}

// Remove a sequencer
void seq::Sequencers::remove(Sequencer* sequencer) {
    sequencers_.remove_if([sequencer](const std::shared_ptr<Sequencer>& s) {
        return s.get() == sequencer;
    });
}

// Return the sequencer with the given identifier
seq::Sequencer* seq::Sequencers::find(int identifier) {
    Sequencer* result = nullptr;
    for(auto& s : sequencers_) {
        if(s->identifier() == identifier) {
            result = s.get();
            break;
        }
    }
    return result;
}

// Write out the model configuration as an XML file
void seq::Sequencers::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& s : sequencers_) {
        root << xml::Obj("sequencer") << *s;
    }
    root << xml::Close();
}

// Read the configuration
void seq::Sequencers::readConfig(xml::DomObject& root) {
    clear();
    root >> xml::Open();
    for(auto& p : root.obj("sequencer")) {
        try {
            auto s = makeSequencer(*p);
            *p >> *s;
        } catch(box::Factory<Sequencer>::Exception& e) {
            std::cout << e.what() << std::endl;
        }
    }
    root >> xml::Close();
}

// Run all the sequencers
bool seq::Sequencers::run() {
    for(auto& s : sequencers_) {
        s->run();
    }
    return !sequencers_.empty();
}

// Stop all the sequencers
void seq::Sequencers::stop() {
    for(auto& s : sequencers_) {
        s->stop();
    }
}

// Pause all the sequencers
void seq::Sequencers::pause() {
    for(auto& s : sequencers_) {
        s->pause();
    }
}

// An full FDTD step (e and h) is complete
void seq::Sequencers::stepComplete() {
    for(auto& s : sequencers_) {
        s->stepComplete();
    }
}
