/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CHARACTERIZERSTEP_H
#define FDTDLIFE_CHARACTERIZERSTEP_H

#include <vector>
#include <Xml/DomObject.h>
#include <Fdtd/RecordedData.h>
#include "ScannerPos.h"
namespace sensor {class ArraySensor;}

namespace seq {

    // A class that represents a single step in a characterisation
    class CharacterizerStep {
    public:
        // Construction
        explicit CharacterizerStep(ScannerPos step);
        CharacterizerStep() = default;

        // API
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const CharacterizerStep& c) {c.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, CharacterizerStep& c) {c.read(o); return o;}
        void recordData(sensor::ArraySensor* sensor);
        bool empty() const {return data_.empty();}
        std::string status(CharacterizerStep* currentStep) const;

        // Getters
        const ScannerPos& step() const {return step_;}
        const fdtd::RecordedData& data() const {return data_;}

        // Setters
        void data(const fdtd::RecordedData& v) {data_ = v;}

    protected:
        // Members
        ScannerPos step_;
        fdtd::RecordedData data_;
    };
}


#endif //FDTDLIFE_CHARACTERIZERSTEP_H
