/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Characterizer.h"
#include "Scanner.h"
#include "ScannerPos.h"
#include "CharacterizerJob.h"
#include "CharacterizerStep.h"
#include "IdenticalLayersScanner.h"
#include "LayerSpacingScanner.h"
#include "BinaryNxNPlateScanner.h"
#include "SquarePlateScanner.h"
#include "DielectricLayerScanner.h"
#include "VariableScanner.h"
#include <Fdtd/Model.h>
#include <Fdtd/NodeManager.h>
#include <Sensor/ArraySensor.h>
#include <algorithm>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <cstdio>
#include <sstream>

// Constructor
seq::Characterizer::Characterizer() {
    // Initialise the scanner factory
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<IdenticalLayersScanner>(
            ScannerMode::identicalLayers, "Identical Layers"));
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<LayerSpacingScanner>(
            ScannerMode::layerSpacing, "Layer Spacing"));
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<BinaryNxNPlateScanner>(
            ScannerMode::binaryNxNPlate, "Binary Plate"));
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<SquarePlateScanner>(
            ScannerMode::squarePlate, "Square Plate/Hole"));
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<DielectricLayerScanner>(
            ScannerMode::dielectricLayer, "Dielectric Layer"));
    scannerFactory_.define(new box::Factory<Scanner, ScannerMode>::Creator<VariableScanner>(
            ScannerMode::variable, "Variable"));
}

// Start running the model
void seq::Characterizer::run() {
    auto stepPos = steps_.begin();
    // Work out the dimensions
    bool allZero = true;
    ScannerPos dims;
    for(auto& scanner : scanners_) {
        dims.add(scanner->numSteps());
        allZero = allZero && scanner->numSteps() == 0;
    }
    // Get the start positions
    startPos_.clear();
    for(auto& scanner : scanners_) {
        startPos_.add(scanner->startStep());
    }
    // Initialise the current position
    currentPos_.clear();
    currentPos_.resize(dims.size());
    // Iterate over the dimensions
    bool going = !allZero;
    while(going) {
        // Add the start position to the current position to make the current step
        ScannerPos step = currentPos_ + startPos_;
        // Does the step object apply to this step?
        if(stepPos != steps_.end()) {
            if((*stepPos)->step() != step) {
                // No, remove any further step objects
                steps_.erase(stepPos, steps_.end());
                stepPos = steps_.end();
            }
        }
        // Create a step object if necessary
        if(stepPos == steps_.end()) {
            steps_.push_back(std::make_shared<CharacterizerStep>(step));
            --stepPos;
        }
        // Create a job for this position if necessary
        if((*stepPos)->empty()) {
            auto job = std::make_shared<CharacterizerJob>(
                    *stepPos, m_->processingMgr()->allocJobId(), this);
            m_->processingMgr()->addJob(job);
        }
        ++stepPos;
        // Advance the position until we are done
        going = false;
        for(size_t i = 0; i < dims.size(); i++) {
            currentPos_[i]++;
            if(currentPos_[i] >= dims[i]) {
                currentPos_[i] = 0;
            } else {
                going = true;
                break;
            }
        }
    }
    m_->doNotification(fdtd::Model::notifySequencerChange);
}

// A job has completed
void seq::Characterizer::jobComplete(const std::shared_ptr<fdtd::Job>& /*job*/) {
    // Is this the master sequencer...
    if(!slave_) {
        // A step is complete
        doNotification(notifyCharacterizerStepComplete);
        writeSteps();
        // Are all the steps complete?
        bool allDone = true;
        for(auto& step : steps_) {
            allDone = allDone && !step->empty();
        }
        if(allDone) {
            doNotification(notifyCharacterizerComplete);
        }
    }
}

// Remove a scanner from this characterizer
void seq::Characterizer::remove(Scanner* scanner) {
    auto pos = std::find_if(scanners_.begin(), scanners_.end(),
                            [scanner](const std::unique_ptr<Scanner>& a) {
                                return a.get() == scanner;
                            });
    if(pos != scanners_.end()) {
        scanners_.erase(pos);
    }
}

// Load this step into the model
void seq::Characterizer::loadStepIntoModel(
        const std::shared_ptr<seq::CharacterizerStep>& step) {
    m_->clearSeqGenerated();
    auto stepPos = step->step().begin();
    for(auto& scanner : scanners_) {
        scanner->loadIntoModel(*stepPos);
        ++stepPos;
    }
    currentStep_ = step;
}

// Load this step into the model
std::string seq::Characterizer::stepDescription(
        const std::shared_ptr<seq::CharacterizerStep>& step) {
    std::stringstream str;
    auto stepPos = step->step().begin();
    for(auto& scanner : scanners_) {
        str << scanner->description(*stepPos) << " ";
        ++stepPos;
    }
    return str.str();
}

// Record the data from the first array sensorId into the item
void seq::Characterizer::recordData(const std::shared_ptr<CharacterizerStep>& step) {
    // Find the sensorId
    sensor::ArraySensor* sensor = findSensor();
    // Record the data
    if(sensor != nullptr) {
        step->recordData(sensor);
        // And the frequencies
        frequencies_ = sensor->frequencies();
    }
}

// Find the sensorId we are to use
sensor::ArraySensor* seq::Characterizer::findSensor() {
    // Find the sensorId
    sensor::ArraySensor* sensor = nullptr;
    for(auto& s : m_->sensors()) {
        sensor = dynamic_cast<sensor::ArraySensor*>(s.get());
        if(sensor != nullptr) {
            break;
        }
    }
    return sensor;
}

// The given step now contains data, install this in the step list
void seq::Characterizer::addStep(const std::shared_ptr<seq::CharacterizerStep>& step) {
    // Find the step in the list
    auto stepPos = steps_.begin();
    while((*stepPos)->step() != step->step()) {
        ++stepPos;
    }
    // If we found it...
    if(stepPos != steps_.end()) {
        // Is it a different object? (ie is this result from an MPI task)
        if((*stepPos).get() != step.get()) {
            // Copy the data across
            (*stepPos)->data(step->data());
        } else {
            // Otherwise, the data is already in the object!
        }
    }
}

// Write the current state of the steps to a file
bool seq::Characterizer::writeSteps() {
    bool result = false;
    if(!m_->fileName().empty()) {
        try {
            // Make the DOM
            auto* root = new xml::DomObject("steps");
            xml::DomDocument dom(root);
            for(auto& step : steps_) {
                *root << xml::Obj("step") << *step;
            }
            xml::Writer writer;
            writer.writeFile(&dom, productFilename() + ".steps");
            result = true;
        } catch(xml::Exception& e) {
            std::cout << "Failed to write steps XML file: " << e.what() << std::endl;
        }
    }
    return result;
}

// Read the steps from a file
bool seq::Characterizer::readSteps() {
    bool result = false;
    if(!m_->fileName().empty()) {
        // Open the file
        std::ifstream file;
        file.open(productFilename() + ".steps", std::ios::in | std::ios::binary);
        if(file.is_open()) {
            // Read the whole file
            std::stringstream buffer;
            buffer << file.rdbuf();
            // Parse into a DOM
            xml::Reader reader;
            xml::DomDocument dom("steps");
            try {
                reader.readString(&dom, buffer.str());
                // Now process the DOM
                steps_.clear();
                for(auto& o : dom.getObject()->obj("step")) {
                    steps_.push_back(std::make_shared<CharacterizerStep>());
                    *o >> *steps_.back();
                }
            } catch(xml::Exception& e) {
                std::cout << "Failed to read steps XML file: " << e.what() << std::endl;
            }
            // Get the frequencies
            m_->sources().initialise();
            m_->sensors().initialise();
            updateFrequencies();
            // The result
            result = true;
        }
    }
    return result;
}

// Write the configuration to the DOM object
void seq::Characterizer::writeConfig(xml::DomObject& root) const {
    // Base class first
    Sequencer::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    for(auto& scanner : scanners_) {
        root << xml::Obj("scanner") << *scanner;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void seq::Characterizer::readConfig(xml::DomObject& root) {
    // Base class first
    Sequencer::readConfig(root);
    // Now my stuff
    scanners_.clear();
    root >> xml::Reopen();
    for(auto& o : root.obj("scanner")) {
        *o >> *makeScanner(*o);
    }
    root >> xml::Close();
    // Read the steps
    readSteps();
}

// The job is being stopped
void seq::Characterizer::stop() {
    // Base class first
    Sequencer::stop();
    // Now me
    writeSteps();
}

// The job is being paused
void seq::Characterizer::pause() {
    // Base class first
    Sequencer::pause();
    // Now me
    writeSteps();
}

// Return the step associated with the given position
std::shared_ptr<seq::CharacterizerStep> seq::Characterizer::getStep(
        const seq::ScannerPos& pos) {
    std::shared_ptr<seq::CharacterizerStep> result;
    for(auto& step : steps_) {
        if(step->step() == pos) {
            result = step;
        }
    }
    return result;
}

// Return the scanner with the given id
seq::Scanner* seq::Characterizer::getScanner(int id) {
    seq::Scanner* result = nullptr;
    for(auto& s : scanners_) {
        if(s->id() == id) {
            result = s.get();
        }
    }
    return result;
}

// Clear the sequencer and discard all its data
void seq::Characterizer::clearAndDiscard() {
    steps_.clear();
    ::remove((productFilename() + ".steps").c_str());
    m_->clearSeqGenerated();
}

// Make a scanner using the factory
const std::unique_ptr<seq::Scanner>& seq::Characterizer::makeScanner(ScannerMode mode) {
    scanners_.emplace_back(scannerFactory_.make(mode, this));
    return scanners_.back();
}

// Add a scanner to the list
const std::unique_ptr<seq::Scanner>& seq::Characterizer::makeScanner(xml::DomObject& o) {
    scanners_.emplace_back(scannerFactory_.restore(o, this));
    return scanners_.back();
}

// Reorder the scanners according to the order map.
// The order map contains <identifier, orderNumber>,
// the new order number indexed by identifier
void seq::Characterizer::reorderScanners(const std::map<int, int>& order) {
    try {
        std::sort(scanners_.begin(), scanners_.end(),
                  [order](const std::unique_ptr<Scanner>& a,
                          const std::unique_ptr<Scanner>& b) {
                      return order.at(a->id()) < order.at(b->id());
                  });
    } catch(std::out_of_range&) {
        std::cout << "Scanner not in order map" << std::endl;
    }
}

// Convert the captured characterizer data into CSV format
// The data is written as one big table, first column is frequency,
// then each result writes two columns, transmittance and phase shift.
std::string seq::Characterizer::makeCsv() {
    std::stringstream file;
    // The main header row
    file << name_;
    for(auto& step: steps_) {
        file << "\t" << step->step() << "\t\t";
    }
    file << std::endl;
    // The sub-header row
    file << "Frequency";
    for(size_t i = 0; i < steps_.size(); i++) {
        file << "\tTransmittance\tPhaseShift\tReflectance";
    }
    file << std::endl;
    // A row for each frequency
    size_t i = 0;
    for(auto frequency : frequencies_) {
        file << frequency;
        for(auto& step: steps_) {
            file << "\t";
            if(i < step->data().transmittance().size()) {
                file << step->data().transmittance()[i];
            }
            file << "\t";
            if(i < step->data().phaseShift().size()) {
                file << step->data().phaseShift()[i];
            }
            file << "\t";
            if(i < step->data().reflectance().size()) {
                file << step->data().reflectance()[i];
            }
        }
        i++;
        file << std::endl;
    }
    return file.str();
}

// Update the frequencies the characterisation is carried out over.
void seq::Characterizer::updateFrequencies() {
    sensor::ArraySensor* sensor = findSensor();
    if(sensor != nullptr) {
        frequencies_ = sensor->frequencies();
    }
}

// Conver the scanner mode to a string
std::string seq::toString(seq::Characterizer::ScannerMode mode)
{
    std::string result;
    switch(mode) {
        case Characterizer::ScannerMode::identicalLayers:
            result = "IdenticalLayers";
            break;
        case Characterizer::ScannerMode::layerSpacing:
            result = "LayerSpacing";
            break;
        case Characterizer::ScannerMode::binaryNxNPlate:
            result = "BinaryNxNPlate";
            break;
        case Characterizer::ScannerMode::squarePlate:
            result = "SquarePlate";
            break;
        case Characterizer::ScannerMode::dielectricLayer:
            result = "DielectricLayer";
            break;
        case Characterizer::ScannerMode::variable:
            result = "Variable";
            break;
    }
    return result;
}


