/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CHARACTERIZER_H
#define FDTDLIFE_CHARACTERIZER_H

#include "Sequencer.h"
#include "ScannerPos.h"
#include <Box/Factory.h>
#include <Box/FrequencySeq.h>
#include <Box/Vector.h>
#include <vector>
#include <list>
#include <memory>
#include <map>
namespace sensor {class ArraySensor;}

namespace seq {
    class CharacterizerStep;
    class Scanner;

    // A sequencing class that varies features of the model over
    // ranges, running the model at each step.
    class Characterizer : public Sequencer {
    public:
        // Constants
        enum {notifyCharacterizerStepComplete=seq::Sequencer::notifyNextAvailableCode,
            notifyCharacterizerComplete};
        enum class ScannerMode {identicalLayers=1, layerSpacing,
            binaryNxNPlate, squarePlate, dielectricLayer, variable};
        friend std::string toString(ScannerMode mode);

        // Construction
        Characterizer();

        // Overrides of Sequencer
        void run() override;
        void stop() override;
        void pause() override;
        void clearAndDiscard() override;
        void jobComplete(const std::shared_ptr<fdtd::Job>& job) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        std::string makeCsv() override;

        // API
        const std::unique_ptr<Scanner>& makeScanner(ScannerMode mode);
        const std::string& scannerModeName(ScannerMode mode) {return scannerFactory_.modeName(mode);}
        box::Factory<Scanner, ScannerMode>& scannerFactory() { return scannerFactory_; }
        void remove(Scanner* scanner);
        void loadStepIntoModel(const std::shared_ptr<seq::CharacterizerStep>& step);
        std::string stepDescription(const std::shared_ptr<seq::CharacterizerStep>& step);
        void recordData(const std::shared_ptr<CharacterizerStep>& step);
        void addStep(const std::shared_ptr<CharacterizerStep>& step);
        bool writeSteps();
        bool readSteps();
        std::shared_ptr<CharacterizerStep> getStep(const ScannerPos& pos);
        Scanner* getScanner(int id);
        sensor::ArraySensor* findSensor();
        void reorderScanners(const std::map<int, int>& order);
        void updateFrequencies();

        // Getters
        [[nodiscard]] const std::vector<std::unique_ptr<Scanner>>& scanners() const {return scanners_;}
        [[nodiscard]] const std::list<std::shared_ptr<CharacterizerStep>>& steps() const {return steps_;}
        [[nodiscard]] const std::shared_ptr<CharacterizerStep>& currentStep() const {return currentStep_;}
        [[nodiscard]] const box::FrequencySeq& frequencies() const {return frequencies_;}

    protected:
        // Members
        box::Factory<Scanner, ScannerMode> scannerFactory_;
        std::vector<std::unique_ptr<Scanner>> scanners_;
        std::list<std::shared_ptr<CharacterizerStep>> steps_;
        std::shared_ptr<CharacterizerStep> currentStep_;
        box::FrequencySeq frequencies_;
        ScannerPos currentPos_;
        ScannerPos startPos_;

        // Helpers
        const std::unique_ptr<Scanner>& makeScanner(xml::DomObject& o);
    };

    std::string toString(Characterizer::ScannerMode mode);
}


#endif //FDTDLIFE_CHARACTERIZER_H
