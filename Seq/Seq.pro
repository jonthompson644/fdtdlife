#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Seq
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Sequencer.cpp \
    Sequencers.cpp \
    SingleSeq.cpp \
    Characterizer.cpp \
    CharacterizerJob.cpp \
    CharacterizerStep.cpp \
    IdenticalLayersScanner.cpp \
    Scanner.cpp \
    ScannerPos.cpp \
    LayerSpacingScanner.cpp \
    BinaryNxNPlateScanner.cpp \
    SquarePlateScanner.cpp \
    DielectricLayerScanner.cpp

HEADERS += \
    Sequencer.h \
    Sequencers.h \
    SingleSeq.h \
    Characterizer.h \
    CharacterizerJob.h \
    CharacterizerStep.h \
    IdenticalLayersScanner.h \
    Scanner.h \
    ScannerPos.h \
    LayerSpacingScanner.h \
    BinaryNxNPlateScanner.h \
    LayerScanner.h \
    SquarePlateScanner.h \
    DielectricLayerScanner.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Seq.pro

DISTFILES += \
    Seq.pro.user \
    CMakeLists.txt \
    Makefile
