/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Sequencer.h"
#include <Fdtd/Model.h>
#include "SingleSeq.h"
#include "Characterizer.h"
#include "Sequencers.h"
#include <Xml/DomObject.h>
#include <cstdio>

// Constructor
seq::Sequencer::Sequencer() {
}

// Second stage constructor used by the factory
void seq::Sequencer::construct(int mode, int identifier, fdtd::Model* model) {
    identifier_ = identifier;
    m_ = model;
    mode_ = mode;
    Notifier::connectMgr(model);
}

// Write the configuration to the DOM object
void seq::Sequencer::writeConfig(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("mode") << mode_;
    p << xml::Obj("name") << name_;
    p << xml::Obj("identifier") << identifier_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void seq::Sequencer::readConfig(xml::DomObject& p) {
    // Don't need to read mode_ or identifier_ as they were read by the factory
    p >> xml::Open();
    p >> xml::Obj("name") >> xml::Default(name_) >> name_;
    p >> xml::Close();
}

// Return the root filename to be used for any product files
std::string seq::Sequencer::productFilename() const {
    std::string result = m_->pathName() + m_->fileName();
    if(!name_.empty()) {
        result = result + "_" + name_;
    }
    return result;
}

// Rename a file
void seq::Sequencer::renameFiles(const std::string& oldFileRoot,
                                  const std::string& newFileRoot,
                                  const std::list<std::string>& extensions) {
    for(auto& ext : extensions) {
        std::string oldFileName = oldFileRoot + ext;
        std::string newFileName = newFileRoot + ext;
        rename(oldFileName.c_str(), newFileName.c_str());
    }
}
