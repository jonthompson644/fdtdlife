/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryNxNPlateScanner.h"
#include "Characterizer.h"
#include <Fdtd/Model.h>
#include <Domain/BinaryPlateNxN.h>
#include <Xml/Writer.h>
#include <Box/BinaryPattern.h>
#include <Box/BinaryPattern4Fold.h>
#include <cmath>

// Precalculated no corner-to-corner sizes
std::initializer_list<size_t> seq::BinaryNxNPlateScanner::twoFoldSizes_ = {
        0, 2, 12

};
std::initializer_list<size_t> seq::BinaryNxNPlateScanner::fourFoldSizes_ = {
        0, 2, 5
};

// Second stage of construction called by the factory
void seq::BinaryNxNPlateScanner::construct(Characterizer::ScannerMode mode, int id,
                                           Characterizer* s) {
    Scanner::construct(mode, id, s);
    cellSize_ = s->m()->p()->dr().x().value();
    calcTotalPatterns();
}

// Load the model with this step
void seq::BinaryNxNPlateScanner::loadIntoModel(size_t step) {
    // Work out the coding for this step
    box::BinaryPattern coding(bitsPerHalfSide_);
    if(fourFoldSymmetry_) {
        box::BinaryPattern4Fold coding4(bitsPerHalfSide_);
        coding4.pattern({step});
        coding = coding4;
    } else {
        coding.pattern({step});
    }
    // Create the layer
    if(shapeId_ == -1) {
        shapeId_ = s_->m()->d()->allocShapeIdentifier();
    }
    auto plate = dynamic_cast<domain::BinaryPlateNxN*>(s_->m()->d()->makeShape(
            domain::Domain::shapeModeBinaryPlateNxN));
    plate->bitsPerHalfSide(bitsPerHalfSide_);
    plate->layerThickness(box::Expression(layerThickness_));
    plate->fillMaterial(fillMaterial_);
    plate->center().value(position_);
    plate->cellSizeX(box::Expression(cellSize_));
    plate->material(material_);
    plate->coding(coding.pattern());
    plate->name("CharacterizerLayer");
    plate->seqGenerated(true);
}

// Write the configuration to the XML
void seq::BinaryNxNPlateScanner::write(xml::DomObject& o) const {
    // Base class first
    Scanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    o << xml::Obj("fourfoldsymmetry") << fourFoldSymmetry_;
    o << xml::Obj("includecornertocorner") << includeCornerToCorner_;
    o << xml::Obj("layerthickness") << layerThickness_;
    o << xml::Obj("fillmaterial") << fillMaterial_;
    o << xml::Obj("position") << position_;
    o << xml::Obj("cellsize") << cellSize_;
    o << xml::Obj("repeatx") << repeatX_;
    o << xml::Obj("repeaty") << repeatY_;
    o << xml::Obj("material") << material_;
    o << xml::Close();
}

// Read the configuration from the XML
void seq::BinaryNxNPlateScanner::read(xml::DomObject& o) {
    // Base class first
    Scanner::read(o);
    // Now my stuff
    o >> xml::Reopen();
    o >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
    o >> xml::Obj("fourfoldsymmetry") >> fourFoldSymmetry_;
    o >> xml::Obj("includecornertocorner") >> includeCornerToCorner_;
    o >> xml::Obj("layerthickness") >> layerThickness_;
    o >> xml::Obj("fillmaterial") >> fillMaterial_;
    o >> xml::Obj("position") >> position_;
    o >> xml::Obj("cellsize") >> cellSize_;
    o >> xml::Obj("repeatx") >> repeatX_;
    o >> xml::Obj("repeaty") >> repeatY_;
    o >> xml::Obj("material") >> material_;
    o >> xml::Close();
    calcTotalPatterns();
}

// Calculate the total number of patterns represented by this scanner.
void seq::BinaryNxNPlateScanner::calcTotalPatterns() {
    totalPatterns_ = 0;
    if(fourFoldSymmetry_) {
        if(bitsPerHalfSide_ < fourFoldSizes_.size()) {
            totalPatterns_ = fourFoldSizes_.begin()[bitsPerHalfSide_];
        } else {
            box::BinaryPattern4Fold pattern(bitsPerHalfSide_);
            auto possPatterns = static_cast<size_t>(std::pow(
                    2.0, pattern.requiredNumBits()));
            if(includeCornerToCorner_) {
                totalPatterns_ = possPatterns;
            } else {
                box::BinaryPattern twoFold;
                for(size_t i = 0; i < possPatterns; i++) {
                    pattern.pattern({i});
                    twoFold = pattern;
                    if(!twoFold.hasCornerToCorner()) {
                        totalPatterns_++;
                    }
                    if(i % 100 == 0) {
                        std::cout << "Checking pattern " << i << std::endl;
                    }
                }
            }
        }
    } else {
        if(bitsPerHalfSide_ < twoFoldSizes_.size()) {
            totalPatterns_ = twoFoldSizes_.begin()[bitsPerHalfSide_];
        } else {
            auto possPatterns = static_cast<size_t>(std::pow(
                    2.0, bitsPerHalfSide_ * bitsPerHalfSide_));
            if(includeCornerToCorner_) {
                totalPatterns_ = possPatterns;
            } else {
                box::BinaryPattern pattern(bitsPerHalfSide_);
                for(size_t i = 0; i < possPatterns; i++) {
                    pattern.pattern({i});
                    if(!pattern.hasCornerToCorner()) {
                        totalPatterns_++;
                    }
                    if(i % 100 == 0) {
                        std::cout << "Checking pattern " << i << std::endl;
                    }
                }
            }
        }
    }
}

