/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Scanner.h"
#include "Characterizer.h"

// Second stage of construction called by the factory
void seq::Scanner::construct(Characterizer::ScannerMode mode, int id, seq::Characterizer* s) {
    id_ = id;
    mode_ = mode;
    s_ = s;
}

// Write the object to the XML DOM
void seq::Scanner::write(xml::DomObject& o) const {
    o << xml::Open();
    o << xml::Obj("identifier") << id_;
    o << xml::Obj("mode") << mode_;
    o << xml::Obj("startstep") << startStep_;
    o << xml::Obj("numsteps") << numSteps_;
    o << xml::Close();
}

// Read the object from the XML DOM
void seq::Scanner::read(xml::DomObject& o) {
    o >> xml::Open();
    o >> xml::Obj("identifier") >> id_;
    o >> xml::Obj("mode") >> mode_;
    o >> xml::Obj("startstep") >> startStep_;
    o >> xml::Obj("numsteps") >> numSteps_;
    o >> xml::Close();
}
