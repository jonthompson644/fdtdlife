/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableScanner.h"
#include "Characterizer.h"
#include <Fdtd/Model.h>
#include <iomanip>

// Write the configuration to the XML
void seq::VariableScanner::write(xml::DomObject& o) const {
    // Base class first
    Scanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Obj("from") << from_;
    o << xml::Obj("to") << to_;
    o << xml::Obj("variableid") << variableId_;
    o << xml::Close();
}

// Read the configuration from the XML
void seq::VariableScanner::read(xml::DomObject& o) {
    // Base class first
    Scanner::read(o);
    // Now my stuff
    o >> xml::Reopen();
    o >> xml::Obj("from") >> from_;
    o >> xml::Obj("to") >> to_;
    o >> xml::Obj("variableid") >> variableId_;
    o >> xml::Close();
}

// Load the model with this step
void seq::VariableScanner::loadIntoModel(size_t step) {
    // Work out the variable value for this step
    double value = calcValue(step);
    // Set the variable
    auto* var = s_->m()->variables().find(variableId_);
    if(var != nullptr) {
        std::stringstream text;
        text << std::setprecision(12) << value;
        var->text(text.str());
    }
}

// Return a string representing the state of this scanner for the specified step
std::string seq::VariableScanner::description(size_t step) {
    double value = calcValue(step);
    auto* var = s_->m()->variables().find(variableId_);
    std::stringstream str;
    if(var != nullptr) {
        str << var->name();
    } else {
        str << "<<missing>>";
    }
    str << "=" << value;
    return str.str();
}

// Calculate the value of the variable for the specified step
double seq::VariableScanner::calcValue(size_t step) {
    double value = from_;
    if(numSteps_ > 1) {
        double increment = (to_ - from_) / static_cast<double>(numSteps_ - 1);
        value += static_cast<double>(step) * increment;
    }
    return value;
}
