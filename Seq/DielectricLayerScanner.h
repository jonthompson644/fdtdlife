/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DIELECTRICLAYERSCANNER_H
#define FDTDLIFE_DIELECTRICLAYERSCANNER_H

#include "LayerScanner.h"
#include <Box/Vector.h>
#include <Domain/Domain.h>


namespace seq {

    // A scanner class that varies the thickness of
    // a dielectric layer
    class DielectricLayerScanner : public LayerScanner {
    public:
        // Construction
        DielectricLayerScanner() = default;

        // Overrides of Scanner
        void loadIntoModel(size_t step) override;
        void write(xml::DomObject& o) const override;
        void read(xml::DomObject& o) override;

        // Getters
        double thicknessStepSize() const {return thicknessStepSize_;}
        const box::Vector<double>& position() const {return position_;}
        double cellSize() const {return cellSize_;}
        int material() const {return material_;}
        size_t repeatX() const { return repeatX_; }
        size_t repeatY() const { return repeatY_; }

        // Setters
        void thicknessStepSize(double v) {thicknessStepSize_ = v;}
        void position(const box::Vector<double>& v) {position_ = v;}
        void cellSize(double v) {cellSize_ = v;}
        void material(int v) {material_ = v;}
        void repeatX(size_t v) { repeatX_ = v; };
        void repeatY(size_t v) { repeatY_ = v; };

    protected:
        // Members
        double thicknessStepSize_ {};
        box::Vector<double> position_;
        double cellSize_ {};
        int material_ {domain::Domain::defaultMaterial};
        size_t repeatX_ {1};
        size_t repeatY_ {1};
    };

}

#endif //FDTDLIFE_DIELECTRICLAYERSCANNER_H
