/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlateScanner.h"
#include "Characterizer.h"
#include <Fdtd/Model.h>
#include <Domain/SquarePlate.h>
#include <Domain/SquareHole.h>
#include <Xml/Writer.h>

// Second stage of construction called by the factory
void seq::SquarePlateScanner::construct(Characterizer::ScannerMode mode, int id,
                                        seq::Characterizer* s) {
    LayerScanner::construct(mode, id, s);
    cellSize_ = s->m()->p()->dr().x().value();
}

// Load the model with this step
void seq::SquarePlateScanner::loadIntoModel(size_t step) {
    // Work out the plate size for this step
    double plateSize = plateStepSize_ * step;
    // Create the layer
    if(shapeId_ == -1) {
        shapeId_ = s_->m()->d()->allocShapeIdentifier();
    }
    // Make the shape
    if(hole_) {
        auto plate = dynamic_cast<domain::SquareHole*>(s_->m()->d()->makeShape(
                shapeId_, domain::Domain::shapeModeSquareHole));
        plate->holeSize(box::Expression(plateSize));
        plate->layerThickness(box::Expression(layerThickness_));
        plate->fillMaterial(fillMaterial_);
        plate->center().value(position_);
        plate->cellSizeX(box::Expression(cellSize_));
        plate->repeatX(box::Expression(repeatX_));
        plate->repeatY(box::Expression(repeatY_));
        plate->material(material_);
        plate->name("CharacterizerLayer");
        plate->seqGenerated(true);
    } else {
        auto plate = dynamic_cast<domain::SquarePlate*>(s_->m()->d()->makeShape(
                shapeId_, domain::Domain::shapeModeSquarePlate));
        plate->plateSize(box::Expression(plateSize));
        plate->layerThickness(box::Expression(layerThickness_));
        plate->fillMaterial(fillMaterial_);
        plate->center().value(position_);
        plate->cellSizeX(box::Expression(cellSize_));
        plate->repeatX(box::Expression(repeatX_));
        plate->repeatY(box::Expression(repeatY_));
        plate->material(material_);
        plate->name("CharacterizerLayer");
        plate->seqGenerated(true);
    }
}

// Write the configuration to the XML
void seq::SquarePlateScanner::write(xml::DomObject& o) const {
    // Base class first
    LayerScanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Obj("hole") << hole_;
    o << xml::Obj("platestepsize") << plateStepSize_;
    o << xml::Obj("layerthickness") << layerThickness_;
    o << xml::Obj("fillmaterial") << fillMaterial_;
    o << xml::Obj("position") << position_;
    o << xml::Obj("cellsize") << cellSize_;
    o << xml::Obj("repeatx") << repeatX_;
    o << xml::Obj("repeaty") << repeatY_;
    o << xml::Obj("material") << material_;
    o << xml::Close();
}

// Read the configuration from the XML
void seq::SquarePlateScanner::read(xml::DomObject& o) {
    // Base class first
    LayerScanner::read(o);
    // Now my stuff
    o >> xml::Reopen();
    o >> xml::Obj("hole") >> hole_;
    o >> xml::Obj("platestepsize") >> plateStepSize_;
    o >> xml::Obj("layerthickness") >> layerThickness_;
    o >> xml::Obj("fillmaterial") >> fillMaterial_;
    o >> xml::Obj("position") >> position_;
    o >> xml::Obj("cellsize") >> cellSize_;
    o >> xml::Obj("repeatx") >> repeatX_;
    o >> xml::Obj("repeaty") >> repeatY_;
    o >> xml::Obj("material") >> material_;
    o >> xml::Close();
}

