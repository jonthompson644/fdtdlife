/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SCANNER_H
#define FDTDLIFE_SCANNER_H

#include <Xml/DomObject.h>
#include "Characterizer.h"

namespace seq {

    // Base class for all scanner classes
    class Scanner {
    public:
        // Construction
        Scanner() = default;
        virtual ~Scanner() = default;
        virtual void construct(Characterizer::ScannerMode mode, int id, Characterizer* s);

        // API
        virtual void write(xml::DomObject& o) const;
        virtual void read(xml::DomObject& o);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Scanner& s) {s.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Scanner& s) {s.read(o); return o;}
        virtual void loadIntoModel(size_t /*step*/) {}
        virtual size_t steps() {return numSteps_;}  // Return the total number of steps for this scanner
        virtual std::string description(size_t /*step*/) { return std::string(); }

        // Getters
        [[nodiscard]] int id() const {return id_;}
        [[nodiscard]] Characterizer::ScannerMode mode() const {return mode_;}
        [[nodiscard]] size_t startStep() const {return startStep_;}
        [[nodiscard]] size_t numSteps() const {return numSteps_;}
        [[nodiscard]] Characterizer* s() const { return s_; }

        // Setters
        void startStep(size_t v) {startStep_ = v;}
        void numSteps(size_t v) {numSteps_ = v;}

    protected:
        // Members
        int id_ {-1};
        Characterizer::ScannerMode mode_ {-1};
        Characterizer* s_ {};
        size_t startStep_ {0};
        size_t numSteps_ {1};
    };
}


#endif //FDTDLIFE_SCANNER_H
