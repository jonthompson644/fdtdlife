/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYNXNPLATESCANNER_H
#define FDTDLIFE_BINARYNXNPLATESCANNER_H

#include "Scanner.h"
#include <Box/Vector.h>
#include <Domain/Domain.h>
#include <initializer_list>

namespace seq {
    // A scanner class that iterates through the patterns of an NxN binary plate.
    class BinaryNxNPlateScanner : public Scanner {
    public:
        // Construction
        BinaryNxNPlateScanner() = default;

        // Overrides of Scanner
        void loadIntoModel(size_t step) override;
        void write(xml::DomObject& o) const override;
        void read(xml::DomObject& o) override;
        void construct(Characterizer::ScannerMode mode, int id, Characterizer* s) override;

        // Getters
        [[nodiscard]] size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
        [[nodiscard]] bool fourFoldSymmetry() const { return fourFoldSymmetry_; }
        [[nodiscard]] bool includeCornerToCorner() const { return includeCornerToCorner_; }
        [[nodiscard]] double layerThickness() const { return layerThickness_; }
        [[nodiscard]] int fillMaterial() const { return fillMaterial_; }
        [[nodiscard]] const box::Vector<double>& position() const { return position_; }
        [[nodiscard]] double cellSize() const { return cellSize_; }
        [[nodiscard]] size_t repeatX() const { return repeatX_; }
        [[nodiscard]] size_t repeatY() const { return repeatY_; }
        [[nodiscard]] int material() const { return material_; }
        [[nodiscard]] size_t totalPatterns() const { return totalPatterns_; }

        // Setters
        void bitsPerHalfSide(size_t v) { bitsPerHalfSide_ = v;}
        void fourFoldSymmetry(bool v) { fourFoldSymmetry_ = v;}
        void includeCornerToCorner(bool v) { includeCornerToCorner_ = v;}
        void layerThickness(double v) { layerThickness_ = v; }
        void fillMaterial(int v) { fillMaterial_ = v; }
        void position(const box::Vector<double>& v) { position_ = v; }
        void cellSize(double v) { cellSize_ = v; }
        void repeatX(size_t v) { repeatX_ = v; }
        void repeatY(size_t v) { repeatY_ = v; }
        void material(int v) { material_ = v; }
        void calcTotalPatterns();

    protected:
        // Configuration
        size_t bitsPerHalfSide_ {5};
        bool fourFoldSymmetry_ {true};
        bool includeCornerToCorner_ {true};
        double layerThickness_ {1e-6};
        int fillMaterial_ {domain::Domain::defaultMaterial};
        box::Vector<double> position_;
        double cellSize_ {};
        size_t repeatX_ {1};
        size_t repeatY_ {1};
        int material_ {domain::Domain::defaultMaterial};

        // Attributes
        int shapeId_ {-1};
        size_t totalPatterns_ {};  // The number of patterns that can be scanned
        static std::initializer_list<size_t> twoFoldSizes_;
        static std::initializer_list<size_t> fourFoldSizes_;
        // Helpers
    };
}


#endif //FDTDLIFE_BINARYNXNPLATESCANNER_H
