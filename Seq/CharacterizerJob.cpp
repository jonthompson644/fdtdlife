#include <utility>

#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CharacterizerJob.h"
#include "Characterizer.h"
#include "CharacterizerStep.h"
#include <Fdtd/Model.h>

// Constructor
seq::CharacterizerJob::CharacterizerJob(int id,
                                        seq::Sequencer* sequencer) :
        Job(id, sequencer, modeCharacterizer),
        sequencer_(dynamic_cast<Characterizer*>(sequencer)) {
}

// Constructor
seq::CharacterizerJob::CharacterizerJob(std::shared_ptr<seq::CharacterizerStep> step,
                                        int id, Characterizer* sequencer) :
        Job(id, sequencer, modeCharacterizer),
        sequencer_(sequencer),
        step_(std::move(step)) {
}

// Process this job
void seq::CharacterizerJob::process(fdtd::Model* model, bool localHost) {
    Job::process(model, localHost);
    // Load and initialise the model
    sequencer_->loadStepIntoModel(step_);
    model->initialise();
    model->p()->phase(fdtd::Configuration::phaseElectric);
}

// Return a string that indicates the purpose of the job
std::string seq::CharacterizerJob::displayName() {
    std::stringstream buf;
    buf << "Characterizer step: ";
    for(auto& s : step_->step()) {
        buf << s << " ";
    }
    return buf.str();
}

// The job has completed
void seq::CharacterizerJob::complete(fdtd::Model* /*model*/) {
    if(localHost_) {
        sequencer_->recordData(step_);
    }
    sequencer_->addStep(step_);
    sequencer_->doNotification(seq::Sequencer::notifyJobComplete);
}

// Write the job to the XML DOM
void seq::CharacterizerJob::write(xml::DomObject& root) const {
    // Base class
    Job::write(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("step") << *step_;
    root << xml::Close();
}

// Read the job from the XML DOM
void seq::CharacterizerJob::read(xml::DomObject& root) {
    // Base class
    Job::read(root);
    // Create a step object if we do not already have one
    if(!step_) {
        step_ = std::make_shared<CharacterizerStep>();
    }
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("step") >> *step_;
    root >> xml::Close();
}
