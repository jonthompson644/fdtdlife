/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LAYERSPACINGSCANNER_H
#define FDTDLIFE_LAYERSPACINGSCANNER_H


#include "Scanner.h"
#include <Box/Vector.h>

namespace seq {

    // A scanner class that varies the spacing between layers.
    // This scanner does not actually create anything in the model,
    // rather its output is used to control scanners further down the
    // list.
    class LayerSpacingScanner : public Scanner {
    public:
        // Construction
        LayerSpacingScanner() = default;

        // Overrides of Scanner
        void loadIntoModel(size_t step) override;
        void write(xml::DomObject& o) const override;
        void read(xml::DomObject& o) override;

        // API
        box::Vector<double> nextOffset();

        // Getters
        const box::Vector<double>& increment() const {return increment_;}
        const box::Vector<double>& spacing() const {return spacing_;}

        // Setters
        void increment(const box::Vector<double>& v) {increment_ = v;}

    protected:
        // Members
        box::Vector<double> increment_;  // The spacing increment
        box::Vector<double> spacing_;  // The current layer spacing
        box::Vector<double> nextOffset_;  // The next layer offset to supply
    };
}

#endif //FDTDLIFE_LAYERSPACINGSCANNER_H
