/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SCANNERPOS_H
#define FDTDLIFE_SCANNERPOS_H

#include <vector>
#include <string>
#include <initializer_list>
#include <Xml/DomObject.h>
#include <iostream>

namespace seq {

    // A representation of a position in a scan
    class ScannerPos {
    public:
        // Construction
        explicit ScannerPos(std::vector<size_t>  pos);
        ScannerPos(const std::initializer_list<size_t>& pos);
        explicit ScannerPos(const std::string& pos);
        ScannerPos() = default;

        // API
        friend xml::DomObject& operator<<(xml::DomObject& o, const ScannerPos& p) {return o << p.pos_;}
        friend xml::DomObject& operator>>(xml::DomObject& o, ScannerPos& p) {return o >> p.pos_;}
        friend std::ostream& operator<<(std::ostream& s, const ScannerPos& v) {s << v.text(); return s;}
        void parse(const std::string& text);
        std::string text() const;
        bool operator==(const ScannerPos& other) const {return pos_ == other.pos_;}
        bool operator!=(const ScannerPos& other) const {return pos_ != other.pos_;}
        ScannerPos operator+(const ScannerPos& other) const;
        std::vector<size_t>::iterator begin() {return pos_.begin();}
        std::vector<size_t>::iterator end() {return pos_.end();}
        std::vector<size_t>::const_iterator begin() const {return pos_.begin();}
        std::vector<size_t>::const_iterator end() const {return pos_.end();}
        void add(size_t v) {pos_.push_back(v);}
        void clear() {pos_.clear();}
        void resize(size_t s) {pos_.resize(s, 0);}
        size_t size() const {return pos_.size();}
        size_t& operator[](size_t index) {return pos_[index];}
        const size_t& operator[](size_t index) const {return pos_[index];}

        // Getters
        const std::vector<size_t>& pos() const {return pos_;}

        // Setters
        void pos(const std::vector<size_t>& v) {pos_ = v;}

    protected:
        // Members
        std::vector<size_t> pos_;
    };
}


#endif //FDTDLIFE_SCANNERPOS_H
