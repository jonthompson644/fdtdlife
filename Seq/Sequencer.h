/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SEQUENCER_H
#define FDTDLIFE_SEQUENCER_H

#include <Box/Configurable.h>
#include <Xml/DomObject.h>
#include <Fdtd/Notifier.h>
#include <list>
#include <string>
#include <memory>

namespace fdtd { class Job; }
namespace fdtd { class Model; }

namespace seq {

    // Base class for all model sequencer classes
    class Sequencer : public box::Configurable, public fdtd::Notifier {
    public:
        // Constants
        enum {
            notifyJobComplete = 0, notifySequenceComplete, notifyNextAvailableCode,
            notifyNameChange
        };

        // Construction
        Sequencer();
        virtual void construct(int mode, int identifier, fdtd::Model* model);

        // API
        virtual void run() = 0;
        virtual void jobComplete(const std::shared_ptr<fdtd::Job>& job) = 0;
        virtual void stop() {}
        virtual void pause() {}
        virtual void clearAndDiscard() {}
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        virtual std::string makeCsv() { return ""; }
        virtual void stepComplete() {}
        friend xml::DomObject& operator<<(xml::DomObject& o, const Sequencer& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Sequencer& v) {
            v.readConfig(o);
            return o;
        }

        // Getters
        int identifier() const { return identifier_; }
        const std::string& name() const { return name_; }
        int mode() const { return mode_; }
        fdtd::Model* m() const { return m_; }

        // Setters
        virtual void name(const std::string& v) { name_ = v; }
        void slave(bool v) { slave_ = v; }

    protected:
        // Variables
        fdtd::Model* m_{};    // The model
        int mode_{-1};   // Which kind of sequencer
        bool slave_{};   // Set to true to prevent any further jobs being created

        // Configuration
        int identifier_{-1};   // The identifier
        std::string name_;   // The name of the sequencer

        // Helpers
        std::string productFilename() const;
        static void renameFiles(const std::string& oldFileRoot, const std::string& newFileRoot,
                                const std::list<std::string>& extensions);
    };

}

#endif //FDTDLIFE_SEQUENCER_H
