/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricLayerScanner.h"
#include "Characterizer.h"
#include <Fdtd/Model.h>
#include <Domain/DielectricLayer.h>
#include <Xml/Writer.h>

// Load the model with this step
void seq::DielectricLayerScanner::loadIntoModel(size_t step) {
    // Work out the thickness for this step
    double thickness = thicknessStepSize_ * step;
    // Create the layer
    if(shapeId_ == -1) {
        shapeId_ = s_->m()->d()->allocShapeIdentifier();
    }
    auto layer = dynamic_cast<domain::DielectricLayer*>(s_->m()->d()->makeShape(
            shapeId_, domain::Domain::shapeModeDielectricLayer));
    layer->thickness(box::Expression(thickness));
    layer->center().value(position_);
    layer->cellSizeX(box::Expression(cellSize_));
    layer->material(material_);
    layer->name("CharacterizerLayer");
    layer->seqGenerated(true);
}

// Write the configuration to the XML
void seq::DielectricLayerScanner::write(xml::DomObject& o) const {
    // Base class first
    Scanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Obj("thicknessstepsize") << thicknessStepSize_;
    o << xml::Obj("position") << position_;
    o << xml::Obj("cellsize") << cellSize_;
    o << xml::Obj("material") << material_;
    o << xml::Obj("repeatx") << repeatX_;
    o << xml::Obj("repeaty") << repeatY_;
    o << xml::Close();
}

// Read the configuration from the XML
void seq::DielectricLayerScanner::read(xml::DomObject& o) {
    // Base class first
    Scanner::read(o);
    // Now my stuff
    o >> xml::Reopen();
    o >> xml::Obj("thicknessstepsize") >> thicknessStepSize_;
    o >> xml::Obj("position") >> position_;
    o >> xml::Obj("cellsize") >> cellSize_;
    o >> xml::Obj("material") >> material_;
    o >> xml::Obj("repeatx") >> repeatX_;
    o >> xml::Obj("repeaty") >> repeatY_;
    o >> xml::Close();
}

