/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SEQUENCERS_H
#define FDTDLIFE_SEQUENCERS_H

#include <Xml/DomObject.h>
#include <Box/Factory.h>
#include <memory>

namespace fdtd {class Model;}

namespace seq {
    class Sequencer;

    // A class that manages the sequencers
    class Sequencers {
    public:
        enum Mode {
            modeSingle = 0, modeGeneticSearch, modeCatalogueBuilder,
            modeCharacterizer
        };

        // Construction
        explicit Sequencers(fdtd::Model* m);

        // API
        void remove(Sequencer* sequencer);
        Sequencer* find(int identifier);
        void clear();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        bool empty() const {return sequencers_.empty();}
        bool run();
        void stop();
        void pause();
        friend xml::DomObject& operator<<(xml::DomObject& o, const Sequencers& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Sequencers& v) {v.readConfig(o); return o;}
        std::shared_ptr<Sequencer> makeSequencer(int mode);
        std::shared_ptr<Sequencer> makeSequencer(int id, int mode);
        std::shared_ptr<Sequencer> makeSequencer(xml::DomObject& o);
        std::string sequencerModeName(int mode) {return sequencerFactory_.modeName(mode);}
        void stepComplete();

        // Getters
        const std::list<std::shared_ptr<Sequencer>>& sequencers() const {return sequencers_;}
        box::Factory<Sequencer>& sequencerFactory() {return sequencerFactory_;}

    protected:
        // Members
        fdtd::Model* m_;
        std::list<std::shared_ptr<Sequencer>> sequencers_;
        box::Factory<Sequencer> sequencerFactory_;

        // Helpers
        void add(const std::shared_ptr<Sequencer>& sequencer);
    };

}


#endif //FDTDLIFE_SEQUENCERS_H
