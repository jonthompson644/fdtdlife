/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "IdenticalLayersScanner.h"
#include "Characterizer.h"
#include "LayerSpacingScanner.h"
#include "LayerScanner.h"
#include <Fdtd/Model.h>
#include <Domain/Shape.h>
#include <Domain/UnitCellShape.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

// Load the model with this step
void seq::IdenticalLayersScanner::loadIntoModel(size_t step) {
    // Find the layer spacing we should use (the last spacing scanner before me)
    LayerSpacingScanner* spacing = nullptr;
    for(auto& s : s_->scanners()) {
        if(s.get() == this) {
            // This is me, stop the search
            break;
        } else if(s->mode() == Characterizer::ScannerMode::layerSpacing) {
            spacing = dynamic_cast<LayerSpacingScanner*>(s.get());
        }
    }
    // Find the layer we are copying (from the last layer scanner before me)
    domain::UnitCellShape* layer = nullptr;
    for(auto& s : s_->scanners()) {
        if(s.get() == this) {
            // This is me, stop the search
            break;
        } else {
            auto* layerScanner = dynamic_cast<LayerScanner*>(s.get());
            if(layerScanner != nullptr) {
                // A spacing scanner before me, use its spacing
                layer = dynamic_cast<domain::UnitCellShape*>(
                        s_->m()->d()->getShape(layerScanner->shapeId()));
            }
        }
    }
    if(layer != nullptr) {
        // Encode the layer into XML
        xml::DomObject o("shape");
        o << *dynamic_cast<domain::Shape*>(layer);
        // Move the prototype layer into position
        if(spacing != nullptr) {
            layer->center().value(layer->center().value() + spacing->nextOffset());
        }
        // Use the XML to create more layers
        try {
            for(size_t i = 1; i < step; i++) {  // The zeroth layer is the prototype
                o.reset();
                auto* newLayer = dynamic_cast<domain::UnitCellShape*>(
                        s_->m()->d()->makeShape(o));
                o >> *dynamic_cast<domain::Shape*>(newLayer);
                // Now move the new layer into position
                newLayer->seqGenerated(true);
                if(spacing != nullptr) {
                    newLayer->center().value(
                            newLayer->center().value() + spacing->nextOffset());
                }
                // And rename it
                std::stringstream s;
                s << "IdenticalLayer" << i;
                newLayer->name(s.str());
            }
        } catch(xml::Exception& e) {
            std::cout << "Shape copy failed to read XML: " << e.what() << std::endl;
        }
    }
}

// Write the configuration to the XML
void seq::IdenticalLayersScanner::write(xml::DomObject& o) const {
    // Base class first
    Scanner::write(o);
    // Now my stuff
    o << xml::Reopen();
    o << xml::Close();
}

// Read the configuration from the XML
void seq::IdenticalLayersScanner::read(xml::DomObject& o) {
    // Base class first
    Scanner::read(o);
    // Now my stuff
    o >> xml::Reopen();
    o >> xml::Close();
}
