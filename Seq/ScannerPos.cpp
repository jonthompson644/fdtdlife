#include <utility>

#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ScannerPos.h"

// Constructor
seq::ScannerPos::ScannerPos(std::vector<size_t> pos) :
        pos_(std::move(pos)) {
}

// Constructor
seq::ScannerPos::ScannerPos(const std::initializer_list<size_t>& pos) {
    for(auto& v : pos) {
        pos_.push_back(v);
    }
}

// Constructor
seq::ScannerPos::ScannerPos(const std::string& pos) {
    parse(pos);
}

// Convert from text
void seq::ScannerPos::parse(const std::string& text) {
    pos_.clear();
    std::stringstream s(text);
    while(!s.eof()) {
        size_t v;
        s >> v;
        if(!s.eof()) {
            pos_.push_back(v);
        }
    }
}

// Convert to text
std::string seq::ScannerPos::text() const {
    std::stringstream str;
    for(auto& v : pos_) {
        str << v << " ";
    }
    return str.str();
}

// Add the other position to this one
seq::ScannerPos seq::ScannerPos::operator+(const ScannerPos& other) const {
    ScannerPos result = *this;
    if(other.size() == result.size()) {
        for(size_t i=0; i<result.size(); i++) {
            result[i] += other[i];
        }
    }
    return result;
}
