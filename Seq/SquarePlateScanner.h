/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SQUAREPLATESCANNER_H
#define FDTDLIFE_SQUAREPLATESCANNER_H

#include "LayerScanner.h"
#include <Box/Vector.h>
#include <Domain/Domain.h>

namespace seq {
    // A scanner class that iterates through the sizes of a square plate or hole.
    class SquarePlateScanner : public LayerScanner {
    public:
        // Construction
        SquarePlateScanner() = default;

        // Overrides of Scanner
        void loadIntoModel(size_t step) override;
        void write(xml::DomObject& o) const override;
        void read(xml::DomObject& o) override;
        void construct(Characterizer::ScannerMode mode, int id, Characterizer* s) override;

        // Getters
        [[nodiscard]] bool hole() const { return hole_; }
        [[nodiscard]] double layerThickness() const { return layerThickness_; }
        [[nodiscard]] int fillMaterial() const { return fillMaterial_; }
        [[nodiscard]] const box::Vector<double>& position() const { return position_; }
        [[nodiscard]] double cellSize() const { return cellSize_; }
        [[nodiscard]] size_t repeatX() const { return repeatX_; }
        [[nodiscard]] size_t repeatY() const { return repeatY_; }
        [[nodiscard]] int material() const { return material_; }
        [[nodiscard]] double plateStepSize() const { return plateStepSize_; }

        // Setters
        void hole(bool v) { hole_ = v;}
        void layerThickness(double v) { layerThickness_ = v; };
        void fillMaterial(int v) { fillMaterial_ = v; };
        void position(const box::Vector<double>& v) { position_ = v; };
        void cellSize(double v) { cellSize_ = v; };
        void repeatX(size_t v) { repeatX_ = v; };
        void repeatY(size_t v) { repeatY_ = v; };
        void material(int v) { material_ = v; };
        void plateStepSize(double v) {plateStepSize_ = v;}

    protected:
        // Configuration
        double layerThickness_ {1e-6};
        int fillMaterial_ {domain::Domain::defaultMaterial};
        box::Vector<double> position_;
        double cellSize_ {};
        size_t repeatX_ {1};
        size_t repeatY_ {1};
        int material_ {domain::Domain::defaultMaterial};
        bool hole_ {};
        double plateStepSize_ {};
    };
}


#endif //FDTDLIFE_SQUAREPLATESCANNER_H
