/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DxfWriter.h"
#include "Constants.h"
#include <iomanip>

// Open the DXF file and write the header
box::DxfWriter::DxfWriter(const std::string& fileName, Clipping clipping) :
        ExportWriter(fileName, clipping) {
    if(file_.is_open()) {
        // Open the extra information file
        extraFile_.open(fileName + "_extra", std::ios::out | std::ios::binary);
        // Header section
        writeItem(0, "SECTION");
        writeItem(2, "HEADER");
        writeItem(9, "$ACADVER");
        writeItem(1, "AC1009");
        writeItem(0, "ENDSEC");
        // Entities section
        writeItem(0, "SECTION");
        writeItem(2, "ENTITIES");
    }
    // Initialise the stack
    stack_.emplace_back(State::normal);
}

// Write the trailer and close the DXF file
box::DxfWriter::~DxfWriter() {
    if(file_.is_open()) {
        writeItem(0, "ENDSEC");
        writeItem(0, "EOF");
        // Is the stack empty?
        if(stack_.back().state_ != State::normal) {
            std::cout << "DXF export stack not empty" << std::endl;
        }
    }
}

// Write a string item to the file
void box::DxfWriter::writeItem(int code, const std::string& item) {
    file_ << " " << code << std::endl;
    file_ << item << std::endl;
}

// Write an integer item to the file
void box::DxfWriter::writeItem(int code, int item) {
    file_ << " " << code << std::endl;
    file_ << item << std::endl;
}

// Write a float item to the file
void box::DxfWriter::writeItem(int code, double item) {
    file_ << std::fixed << std::setprecision(6);
    file_ << " " << code << std::endl;
    file_ << item << std::endl;
}

// Write a square to the file
void box::DxfWriter::writeCuboid(Vector<double> p1, Vector<double> p2,
                                 const std::string& shapeName,
                                 const std::string& material) {
    if(file_.is_open()) {
        // Enforce any clipping
        if(clipCuboid(p1, p2)) {
            // Plant the cuboid
            double thickness = fabs(p2.z() - p1.z());
            writeItem(0, "POLYLINE");
            writeItem(8, shapeName);
            writeItem(66, 1);  // Entities follow flag (obsolete but CST writes it)
            writeItem(39, thickness);  // Thickness
            writeItem(10, 0.0);
            writeItem(20, 0.0);
            writeItem(30, 0.0);
            writeItem(70, 1);  // Closed polyline
            writeVertex(p1.x(), p1.y(), p1.z(), material, false, thickness);
            writeVertex(p1.x(), p2.y(), p1.z(), material, false, thickness);
            writeVertex(p2.x(), p2.y(), p1.z(), material, false, thickness);
            writeVertex(p2.x(), p1.y(), p1.z(), material, true, thickness);
            writeItem(8, shapeName);  // CST writes another code 8 here
            // Write out the extra record
            extraFile_ << shapeName << ", z=" << p1.z()
                       << ", thickness=" << thickness
                       << ", material=" << material << std::endl;
        }
    }
}

// Write a vertex for a polyline
void box::DxfWriter::writeVertex(double x, double y, double z, const std::string& material,
                                 bool last, double thickness) {
    writeItem(0, "VERTEX");
    writeItem(8, material);
    writeItem(39, thickness);  // Thickness
    writeItem(10, x);  // Top left
    writeItem(20, y);
    writeItem(30, z);
    writeItem(70, 0);  // Flags
    writeItem(42, 0);  // Bulge
    if(last) {
        writeItem(0, "SEQEND");  // End
    } else {
        writeItem(0, 0);  // End
    }
}

// The start of a set of rectangles to be combined
void box::DxfWriter::combineStart() {

}

// The end of a combined set
void box::DxfWriter::combineEnd() {

}

// The start of a set of rectangles to be subtracted
void box::DxfWriter::subtractStart() {

}

// The end of a subtraction
void box::DxfWriter::subtractEnd() {

}

// The start of a set of shapes to be grouped
void box::DxfWriter::groupStart() {

}

// The end of a shape group
void box::DxfWriter::groupEnd() {

}

// The start of a set of shapes to be duplicated
void box::DxfWriter::duplicateStart() {

}

// The end of a duplication
void box::DxfWriter::duplicateEnd(Vector<double> /*increment*/, size_t /*n*/) {

}

