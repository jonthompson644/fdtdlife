/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CIRCULARBUFFER_H
#define FDTDLIFE_CIRCULARBUFFER_H

#include <vector>
#include "Range.h"

namespace box {

    // A template class the provides an efficient circular buffer
    template<class T>
    class CircularBuffer {
    public:
        // Iterator
        class Iterator {
        public:
            Iterator(CircularBuffer* buffer, size_t pos) :
                    buffer_(buffer),
                    pos_(pos) {
            }
            bool operator!=(const Iterator& other) {
                return pos_ != other.pos_;
            }
            void operator++() {
                if(pos_ < buffer_->size_) { pos_++; }
            }
            T& operator*() {
                return (*buffer_)[pos_];
            }
        protected:
            CircularBuffer* buffer_;
            size_t pos_;
        };

        // Const Iterator
        class ConstIterator {
        public:
            ConstIterator(const CircularBuffer* buffer, size_t pos) :
                    buffer_(buffer),
                    pos_(pos) {
            }
            bool operator!=(const ConstIterator& other) {
                return pos_ != other.pos_;
            }
            void operator++() {
                if(pos_ < buffer_->size_) { pos_++; }
            }
            T operator*() {
                return (*buffer_)[pos_];
            }
        protected:
            const CircularBuffer* buffer_;
            size_t pos_;
        };

        // Construction
        CircularBuffer() :
                start_(0),
                size_(0) {
        };

        // Change the size of the circular buffer
        void resize(size_t size) {
            data_.resize(size);
            clear();
        }

        // Make the buffer empty
        void clear() {
            start_ = 0;
            size_ = 0;
        }

        // Fill the buffer with a value
        void fill(T v) {
            for(auto& d : data_) {
                d = v;
            }
            size_ = data_.size();
        }

        // Access the data as an array
        T& operator[](size_t index) {
            return data_[(start_ + index) % data_.size()];
        }

        // Access the data as an array
        T operator[](size_t index) const {
            return data_[(start_ + index) % data_.size()];
        }

        // Place data at the end of the buffer
        void push(double v) {
            data_[(start_ + size_) % data_.size()] = v;
            if(size_ < data_.size()) {
                size_++;
            } else {
                start_ = (start_ + 1) % data_.size();
            }
        }

        // Take data from the front of the buffer
        double pop() {
            double result = data_[start_];
            start_ = (start_ + 1) % data_.size();
            size_--;
            return result;
        }

        // Return an iterator to the beginning
        Iterator begin() {
            return Iterator(this, 0);
        };

        // Return an iterator to the beginning
        ConstIterator begin() const {
            return ConstIterator(this, 0);
        };

        // Return an iterator to the end
        Iterator end() {
            return Iterator(this, size_);
        };

        // Return an iterator to the end
        ConstIterator end() const {
            return ConstIterator(this, size_);
        };

        // Return the number of elements in the buffer
        size_t size() const {
            return size_;
        }

        // Is the buffer empty
        bool empty() const {
            return size_ == 0;
        }

        // Return the range of the data in the buffer
        Range<T> range() const {
            Range<T> result;
            result.evaluate(data_);
            return result;
        }

        // Copy the buffer data into a vector
        void copyInto(std::vector<T>& data) const {
            data.resize(size_);
            for(size_t i=0; i<size_; i++) {
                data[i] = (*this)[i];
            }
        }

    protected:
        std::vector<T> data_;
        size_t start_;
        size_t size_;
    };
}


#endif //FDTDLIFE_CIRCULARBUFFER_H
