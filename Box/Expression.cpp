/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Expression.h"

#include <utility>
#include <iomanip>
#include <algorithm>

// Constructor
box::Expression::Expression(std::string text) :
        text_(std::move(text)) {
}

// Constructor
box::Expression::Expression(double v) {
    value(v);
}

// Evaluate the expression using the given context parameters
// The BNF is:
// expression ::= sumterm, { ('+' | '-'), sumterm };
// sumterm ::= multterm, { ('*' | '/'), multterm };
// multterm ::= ( ('+' | '-'), multterm ) | powerterm;
// powerterm ::= term, { '^', multterm };
// term ::= constant | variable |
//          function, '(', expression, { ',', expression }, ')' |
//          '(', expression, ')';
double box::Expression::evaluate(Context& context) {
    context.text(text_);
    value_ = evaluateExpr1(context);
    std::string token;
    if(context.getToken(token) != TokenType::none) {
        throw Exception("Extra text at end of expression \"" + token + "\"");
    }
    return value_;
}

// Parse and evaluate an expression containing + or - operators
double box::Expression::evaluateExpr1(Context& context) {
    std::string token;
    // First operand
    double result = evaluateExpr2(context);
    // Operator
    while(context.getToken(token) == TokenType::punctuation) {
        if(token == "+") {
            result += evaluateExpr2(context);
        } else if(token == "-") {
            result -= evaluateExpr2(context);
        } else {
            break;
        }
    }
    context.putToken(token);
    return result;
}

// Parse and evaluate an expression containing * or / operators
double box::Expression::evaluateExpr2(Context& context) {
    std::string token;
    // First operand
    double result = evaluateExpr3(context);
    // Operator
    while(context.getToken(token) == TokenType::punctuation) {
        if(token == "*") {
            result *= evaluateExpr3(context);
        } else if(token == "/") {
            result /= evaluateExpr3(context);
        } else {
            break;
        }
    }
    context.putToken(token);
    return result;
}

// Parse and evaluate an expression containing monadic +/- operators
double box::Expression::evaluateExpr3(Context& context) {
    std::string token;
    // Operator
    double result;
    if(context.getToken(token) == TokenType::punctuation) {
        if(token == "+") {
            result = evaluateExpr3(context);
        } else if(token == "-") {
            result = -evaluateExpr3(context);
        } else {
            context.putToken(token);
            result = evaluateExpr4(context);
        }
    } else {
        context.putToken(token);
        result = evaluateExpr4(context);
    }
    return result;
}

// Parse and evaluate an expression containing ^ operators
double box::Expression::evaluateExpr4(Context& context) {
    std::string token;
    // First operand
    double result = evaluateExpr5(context);
    // Operator
    while(context.getToken(token) == TokenType::punctuation) {
        if(token == "^") {
            result = std::pow(result, evaluateExpr3(context));
        } else {
            break;
        }
    }
    context.putToken(token);
    return result;
}

// Evaluate a single term in an expression
double box::Expression::evaluateExpr5(Context& context) {
    double result = 0.0;
    std::string token;
    // The term
    switch(context.getToken(token)) {
        case TokenType::none:
            // Error
            throw Exception("Unexpected end of expression");
        case TokenType::constant:
            // Just return the constant value
            result = std::strtod(token.c_str(), nullptr);
            break;
        case TokenType::identifier: {
            std::string identifier = token;
            if(context.getToken(token) == TokenType::punctuation && token == "(") {
                // Get the function parameters
                std::vector<double> params;
                params.push_back(evaluateExpr1(context));
                while(context.getToken(token) == TokenType::punctuation && token == ",") {
                    params.push_back(evaluateExpr1(context));
                }
                context.putToken(token);
                // It is a function name
                if(identifier == "sqrt") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::sqrt(params[0]);
                } else if(identifier == "sin") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::sin(params[0]);
                } else if(identifier == "cos") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::cos(params[0]);
                } else if(identifier == "tan") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::tan(params[0]);
                } else if(identifier == "exp") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::exp(params[0]);
                } else if(identifier == "round") {
                    if(params.size() != 1) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::round(params[0]);
                } else if(identifier == "min") {
                    if(params.size() != 2) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::min(params[0], params[1]);
                } else if(identifier == "max") {
                    if(params.size() != 2) {
                        throw Exception("Incorrect number of parameters");
                    }
                    result = std::max(params[0], params[1]);
                } else {
                    throw Exception("Unknown function \"" + identifier + "\"");
                }
                // Closing parenthesis
                if(context.getToken(token) != TokenType::punctuation || token != ")") {
                    // Missing end of function
                    throw Exception("Missing closing \")\" for function");
                }
            } else {
                // It is a parameter
                context.putToken(token);
                // Look up the parameter
                if(!context.lookup(identifier, result)) {
                    throw Exception("Unknown variable \"" + identifier + "\"");
                }
            }
            break;
        }
        case TokenType::punctuation:
            if(token == "(") {
                // Sub function
                result = evaluateExpr1(context);
                // Closing parenthesis
                if(context.getToken(token) != TokenType::punctuation || token != ")") {
                    // Missing end of function
                    throw Exception("Missing closing \")\" for nested expression");
                }
            } else {
                throw Exception("Unexpected token \"" + token + "\"");
            }
            break;
    }
    return result;
}

// Swap the two expressions over
void box::Expression::swap(box::Expression& a, box::Expression& b) {
    std::swap(a.text_, b.text_);
    std::swap(a.value_, b.value_);
}

// Set the value and the text
void box::Expression::value(double v) {
    std::stringstream t;
    t << std::setprecision(12) << v;
    value_ = v;
    text_ = t.str();
}

// Context object; constructor
box::Expression::Context::Context() {
    clear();
}

// Context object; clear to the initial state
void box::Expression::Context::clear() {
    parameters_.clear();
    // Initialise with the global constants
    parameters_.emplace_back("e", 2.71828183);
    parameters_.emplace_back("pi", 3.14159265);
    parameters_.emplace_back("epsilon0", 8.85418782e-12);
    parameters_.emplace_back("mu0", 1.25663706e-6);
    parameters_.emplace_back("c", 299792458.0);
}

// Context object; add a list of parameters to the context
void box::Expression::Context::add(const std::list<Parameter>& p) {
    parameters_.insert(parameters_.end(), p.begin(), p.end());
}

// Context object; add a single parameter to the context
void box::Expression::Context::add(const Parameter& p) {
    parameters_.push_back(p);
}

// Context object; lookup a parameter
bool box::Expression::Context::lookup(const std::string& n, double& v) {
    bool result = false;
    auto pos = std::find_if(parameters_.begin(), parameters_.end(),
                            [n](const Parameter& p) {
                                return p.name() == n;
                            });
    if(pos != parameters_.end()) {
        result = true;
        v = pos->value();
    }
    return result;
}

// Context object; Return the next token from the text string
box::Expression::TokenType box::Expression::Context::getToken(std::string& token) {
    TokenType result = TokenType::none;
    token.erase();
    //  Skip any leading spaces
    while(!text_.empty() && std::isspace(text_.front())) {
        text_.erase(0, 1);
    }
    // Is there a token left?
    if(!text_.empty()) {
        // Get the first character
        char ch = text_.front();
        text_.erase(0, 1);
        token.push_back(ch);
        if(std::isdigit(ch)) {
            // A numeric constant
            result = TokenType::constant;
            // Integer part of the mantissa
            while(!text_.empty() && std::isdigit(text_.front())) {
                token.push_back(text_.front());
                text_.erase(0, 1);
            }
            // Fractional part of the mantissa
            if(!text_.empty() && text_.front() == '.') {
                token.push_back(text_.front());
                text_.erase(0, 1);
                while(!text_.empty() && std::isdigit(text_.front())) {
                    token.push_back(text_.front());
                    text_.erase(0, 1);
                }
            }
            // The exponent
            if(!text_.empty() && (text_.front() == 'e' || text_.front() == 'E')) {
                token.push_back(text_.front());
                text_.erase(0, 1);
                // A possible sign
                if(!text_.empty() && (text_.front() == '+' || text_.front() == '-')) {
                    token.push_back(text_.front());
                    text_.erase(0, 1);
                }
                // The digits
                while(!text_.empty() && std::isdigit(text_.front())) {
                    token.push_back(text_.front());
                    text_.erase(0, 1);
                }
            }
        } else if(std::ispunct(ch)) {
            // A punctuation token
            result = TokenType::punctuation;
        } else if(std::isalpha(ch)) {
            // An identifier
            result = TokenType::identifier;
            while(!text_.empty() && std::isalnum(text_.front())) {
                token.push_back(text_.front());
                text_.erase(0, 1);
            }
        }
    }
    return result;
}

// Context object; return a token to the string
void box::Expression::Context::putToken(const std::string& token) {
    text_ = token + ' ' + text_;
}

// Context object; set the parse string
void box::Expression::Context::text(const std::string& t) {
    text_ = t;
}


