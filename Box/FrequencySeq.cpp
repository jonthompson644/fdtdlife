/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FrequencySeq.h"
#include "Utility.h"
#include <cmath>
#include <algorithm>
#include <Xml/DomObject.h>

// Initialising constructor
box::FrequencySeq::FrequencySeq(double first, double spacing, size_t n) :
        first_(first),
        spacing_(spacing),
        n_(n) {
}

// Copy constructor
box::FrequencySeq::FrequencySeq(const box::FrequencySeq& other) {
    *this = other;
}

// Return an iterator to the first frequency
box::FrequencySeq::Iterator box::FrequencySeq::begin() const {
    return {this, 0};
}

// Return an iterator to the last frequency
box::FrequencySeq::Iterator box::FrequencySeq::end() const {
    return {this, n_};
}

// Return the channel number associated with a frequency
size_t box::FrequencySeq::channel(double frequency) const {
    size_t channel = 0;
    if(spacing_ > 0.0 && n_ > 0) {
        if(frequency < first_) {
            channel = 0;
        } else {
            channel = (size_t) std::floor((frequency - first_) / spacing_);
            if(channel >= n_) {
                channel = n_ - 1;
            }
        }
    }
    return channel;
}

// Return the last frequency
double box::FrequencySeq::last() const {
    return spacing_ * static_cast<double>(n_ - 1) + first_;
}

// Return the frequency of a channel number
double box::FrequencySeq::frequency(size_t channel) const {
    return spacing_ * channel + first_;
}

// Work out the minimum length of a time series, we need space for one whole
// cycle of the frequency spacing (not the start frequency otherwise
// we may not have enough resolution for the frequency steps).
size_t box::FrequencySeq::requiredSamples(double samplePeriod) const {
    size_t samples = 0;
    if(samplePeriod > 0.0) {
        if(spacing_ > 0.0) {
            samples = (size_t) (std::floor(1.0 / spacing_ / samplePeriod));
        } else if(first_ > 0.0) {
            samples = (size_t) (std::floor(1.0 / first_ / samplePeriod));
        }
    }
    // Limit number of samples to avoid silliness.
    if(samples > 1e6) {
        samples = 1e6;
    }
    return samples;
}

// Return true if the frequency sequence is valid
bool box::FrequencySeq::valid() const {
    return n_ > 0 && first_ > 0.0;
}

// Make this sequence a union with the other
box::FrequencySeq& box::FrequencySeq::operator|=(const box::FrequencySeq& other) {
    if(valid()) {
        double biggestLast = std::max(last(), other.last());
        first_ = std::min(first_, other.first_);
        spacing_ = std::min(spacing_, other.spacing_);
        n_ = (size_t) std::ceil((biggestLast - first_) / spacing_) + 1;
    } else {
        *this = other;
    }
    return *this;
}

// Write configuration to the DOM
void box::FrequencySeq::writeConfig(xml::DomObject& o) const {
    o << xml::Open();
    o << xml::Obj("spacing") << spacing_;
    o << xml::Obj("frequency") << first_;
    o << xml::Obj("n") << n_;
    o << xml::Close();
}

// Read configuration from the DOM
void box::FrequencySeq::readConfig(xml::DomObject& o) {
    o >> xml::Open();
    o >> xml::Obj("spacing") >> spacing_;
    o >> xml::Obj("frequency") >> first_;
    o >> xml::Obj("n") >> n_;
    o >> xml::Close();
}

// Return true if the two frequency sequences are identical
bool box::FrequencySeq::operator==(const box::FrequencySeq& other) const {
    return box::Utility::equals(first_, other.first_) &&
           box::Utility::equals(spacing_, other.spacing_) &&
           n_ == other.n_;
}

// Clear the frequency sequence
void box::FrequencySeq::clear() {
    first_ = 0.0;
    spacing_ = 0.0;
    n_ = 0;
}
