/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_EXPRESSION_H
#define FDTDLIFE_EXPRESSION_H

#include <string>
#include <list>
#include <utility>
#include <Xml/DomObject.h>

namespace box {

    // A class that manages an expression string
    class Expression {
    public:
        // A class that represents a parameter and its current value
        class Parameter {
        public:
            // Construction
            explicit Parameter(std::string name, double value = 0.0) :
                    name_(std::move(name)),
                    value_(value) {
            }

            // Getters
            [[nodiscard]] const std::string& name() const { return name_; }
            [[nodiscard]] double value() const { return value_; }

            // Setters
            void name(const std::string& v) { name_ = v; }
            void value(double v) { value_ = v; }

            // API
            Parameter& operator=(double v) { value_ = v; return *this; }
            Parameter& operator+=(double v) { value_ += v; return *this; }

        protected:
            // Members
            std::string name_;   // The name of the parameter
            double value_{0.0};  // Its current value
        };

        // An exception class that is thrown when evaluation fails
        class Exception : public std::exception {
        public:
            std::string what_;
        public:
            explicit Exception(std::string what) :
                    what_(std::move(what)) {}
            Exception(const Exception& other) noexcept :
                    what_(other.what_) {}
            ~Exception() override = default;
            [[nodiscard]] const char* what() const noexcept override { return what_.c_str(); }
        };

        // The token type enumeration
        enum class TokenType {
            none, constant, identifier, punctuation
        };

        // The context object for an expression evaluation
        class Context {
        public:
            explicit Context();
            void add(const std::list<Parameter>& p);
            void add(const Parameter& p);
            void text(const std::string& t);
            bool lookup(const std::string& n, double& v);
            TokenType getToken(std::string& token);
            void putToken(const std::string& token);
            void clear();
        protected:
            std::list<Parameter> parameters_;
            std::string text_;
        };

        // Construction
        explicit Expression(std::string text);
        explicit Expression(double v);
        Expression() = default;
        Expression(const Expression& other) = default;

        // API
        double evaluate(Context& context);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Expression& v) {
            o << v.text_;
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Expression& v) {
            o >> v.text_;
            return o;
        }
        box::Expression& operator=(double v) {value(v); return *this;}
        box::Expression& operator=(const box::Expression& v) {
            if(this != &v) {
                text_ = v.text_;
                value_ = v.value_;
            }
            return *this;
        }
        static void swap(Expression& a, Expression& b);
        bool operator==(const box::Expression& v) const {
            return text_ == v.text_;
        }

        // Getters
        [[nodiscard]] const std::string& text() const { return text_; }
        [[nodiscard]] double value() const {return value_;}

        // Setters
        void text(const std::string& v) { text_ = v; }
        void value(double v);

    protected:
        // Members
        std::string text_ {"0"};  // The expression text
        double value_ {0.0};  // The last calculated value

        // Helper functions
        static double evaluateExpr1(Context& context);
        static double evaluateExpr2(Context& context);
        static double evaluateExpr3(Context& context);
        static double evaluateExpr4(Context& context);
        static double evaluateExpr5(Context& context);
    };
}


#endif //FDTDLIFE_EXPRESSION_H
