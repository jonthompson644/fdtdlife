/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SignalGenerator.h"
#include "Constants.h"
#include <cmath>

// Constructor
box::SignalGenerator::SignalGenerator() :
        samplePeriod_(0.0),
        frequencies_(1 * box::Constants::giga_, 1 * box::Constants::giga_, 1),
        amplitude_(1.0),
        initialTime_(0.0) {
}

// Initialise ready to generate
void box::SignalGenerator::initialise(double samplePeriod, const FrequencySeq& frequencies,
                                      double initialTime, double amplitude) {
    samplePeriod_ = samplePeriod;
    frequencies_ = frequencies;
    initialTime_ = initialTime;
    amplitude_ = amplitude;
}

// Return the next sample
double box::SignalGenerator::sample(int timeStep, double distance) {
    double result = 0.0;
    double t = time(timeStep);
    for(size_t i=0; i<frequencies_.n(); i++) {
        double omega = 2.0 * Constants::pi_ * frequencies_.frequency(i);
        result += std::sin(omega * (distance / Constants::c_ + t));
    }
    return result * amplitude_ / (double)frequencies_.n();
}

// Return the time for the given step
double box::SignalGenerator::time(int timeStep) {
    return (double)timeStep * samplePeriod_ + initialTime_;
}

