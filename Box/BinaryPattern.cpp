/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPattern.h"
#include "BinaryPattern4Fold.h"
#include <algorithm>
#include <sstream>
#include <utility>

// Constructor
box::BinaryPattern::BinaryPattern(size_t n, uint64_t pattern) {
    BinaryPattern::n(n);
    if(pattern_.size() == 1) {
        pattern_ = {pattern};
    }
}

// Constructor
box::BinaryPattern::BinaryPattern(size_t n, const std::vector<uint64_t>& pattern) {
    BinaryPattern::n(n);
    if(pattern_.size() == pattern.size()) {
        pattern_ = pattern;
    }
}

// Return the bit at the specified coordinates
bool box::BinaryPattern::get(size_t x, size_t y) const {
    size_t word, bit;
    index(x, y, word, bit);
    return ((pattern_[word] >> bit) & 1U) != 0;
}

// Set the bit at the specified coordinates
void box::BinaryPattern::set(size_t x, size_t y, bool v) {
    size_t word, bit;
    index(x, y, word, bit);
    size_t mask = static_cast<size_t>(1) << bit;
    pattern_[word] = pattern_[word] & ~mask;
    if(v) {
        pattern_[word] = pattern_[word] | mask;
    }
}

// Return the word and bit numbers containing the desired bit
void box::BinaryPattern::index(size_t x, size_t y, size_t& word, size_t& bit) const {
    size_t p = x + y * n_;
    word = p / bitsPerWord_;
    bit = p % bitsPerWord_;
}

// Translate the bits in the full 2Nx2N pattern by n bits in the x direction.
// We do this by reflecting the bits in the quadrant about the y axis
void box::BinaryPattern::translateX() {
    BinaryPattern translated(n_);
    for(size_t y=0; y<n_; y++) {
        for(size_t x=0; x<n_; x++) {
            translated.set(n_ - x - 1, y, get(x, y));
        }
    }
    *this = translated;
}

// Translate the bits in the full 2Nx2N pattern by n bits in the y direction.
// We do this by reflecting the bits in the quadrant about the x axis
void box::BinaryPattern::translateY() {
    BinaryPattern translated(n_);
    for(size_t y=0; y<n_; y++) {
        for(size_t x=0; x<n_; x++) {
            translated.set(x, n_ - y - 1, get(x, y));
        }
    }
    *this = translated;
}

// Rotate the bits in the full 2Nx2N pattern by 90 degrees clockwise
// We do this by swapping x and y
void box::BinaryPattern::rotate90() {
    BinaryPattern rotated(n_);
    for(size_t j=0; j<n_; j++) {
        for(size_t i=0; i<n_; i++) {
            rotated.set(j, i, get(i, j));
        }
    }
    *this = rotated;
}

// Output the pattern to a stream
std::ostream& box::operator<<(std::ostream& s, const box::BinaryPattern& v) {
    for(size_t y=0; y<v.n_; y++) {
        for(size_t x=0; x<v.n_; x++) {
            s << (v.get(x,y) ? '*' : '.');
        }
        s << std::endl;
    }
    return s;
}

// Clear the pattern to zero, leaves the dimension alone
void box::BinaryPattern::clear() {
    std::fill(pattern_.begin(), pattern_.end(), 0U);
}

// Initialise the pattern
void box::BinaryPattern::initialise(size_t n, const std::vector<uint64_t>& pattern) {
    n_ = n;
    pattern_ = pattern;
}

// Return true if the pattern has any corner to corner features
// We only need to test every other row otherwise everything is
// tested twice
bool box::BinaryPattern::hasCornerToCorner() const {
    bool result = false;
    for(size_t x = 0; x < n_ && !result; x++) {
        for(size_t y = 0; y < n_ && !result; y+=2) {
            result = testCornerToCorner(x, y);
        }
    }
    return result;
}

// Does the specified location have any corner-to-corner neighbours
bool box::BinaryPattern::testCornerToCorner(size_t x, size_t y) const {
    bool result = false;
    // My state
    bool me = get(x, y);
    if(me) {
        // Get the neighbours (note that the code is the top left quadrant
        // of a two fold symmetrical pattern, hence the odd looking wrap
        // around values)
        size_t nextY = (y == n_ - 1) ? y : y + 1;
        size_t prevY = (y == 0) ? 0 : y - 1;
        size_t nextX = (x == n_ - 1) ? x : x + 1;
        size_t prevX = (x == 0) ? 0 : x - 1;
        // There are four side neighbours
        bool n = get(x, prevY);
        bool e = get(nextX, y);
        bool s = get(x, nextY);
        bool w = get(prevX, y);
        // There are four corner neighbours
        bool ne = get(nextX, prevY);
        bool se = get(nextX, nextY);
        bool sw = get(prevX, nextY);
        bool nw = get(prevX, prevY);
        // Work out if we have a corner to corner
        result = ne && !n && !e;
        result = result || (se && !s && !e);
        result = result || (sw && !s && !w);
        result = result || (nw && !n && !w);
    }
    return result;
}

// Converts a four fold pattern to a two fold
box::BinaryPattern& box::BinaryPattern::operator=(const BinaryPattern4Fold& other) {
    n_ = other.n();
    // The sizes
    size_t patternSize2Fold = n_ * n_;
    // The bit location table
    std::vector<uint64_t> bitLocation;
    bitLocation.resize(patternSize2Fold);
    uint64_t v = 0;
    for(size_t i = 0; i < n_; i++) {
        for(size_t j = i; j<n_; j++) {
            bitLocation[i * n_ + j] = v;
            bitLocation[j * n_ + i] = v;
            v++;
        }
    }
    // Convert to two fold
    size_t numWords = (patternSize2Fold + bitsPerWord_ - 1) / bitsPerWord_;
    pattern_.resize(numWords);
    clear();
    for(uint64_t i = 0; i < patternSize2Fold; i++) {
        uint64_t wordNum2 = i / static_cast<uint64_t>(bitsPerWord_);
        uint64_t bitNum2 = i % static_cast<uint64_t>(bitsPerWord_);
        uint64_t wordNum4 = bitLocation[i] / static_cast<uint64_t>(bitsPerWord_);
        uint64_t bitNum4 = bitLocation[i] % static_cast<uint64_t>(bitsPerWord_);
        uint64_t bit = (other.pattern()[wordNum4] >> bitNum4) & static_cast<uint64_t>(1);
        pattern_[wordNum2] |= (bit << bitNum2);
    }
    return *this;
}

// Return the value as a hexadecimal string
std::string box::BinaryPattern::hexadecimal() const {
    std::stringstream s;
    s << std::hex;
    bool first = true;
    for(auto& v : pattern_) {
        if(!first) {
            s << " ";
        }
        first = false;
        s << v;
    }
    return s.str();
}

// Equality operator
bool box::BinaryPattern::operator==(const box::BinaryPattern& other) const {
    return n_ == other.n_ && pattern_ == other.pattern_;
}

// Inequality operator
bool box::BinaryPattern::operator!=(const box::BinaryPattern& other) const {
    return !(*this == other);
}

// Set the size of the binary pattern
void box::BinaryPattern::n(size_t v) {
    if(n_ != v) {
        n_ = v;
        pattern_.clear();
        size_t numWords = (n_ * n_ + bitsPerWord_ - 1) / bitsPerWord_;
        pattern_.resize(numWords);
    }
}
