/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_LINE2D_H
#define FDTDLIFE_LINE2D_H

#include "Point2D.h"

namespace box {

    // A template class that represents a two dimensional line
    // that passes through two points
    template<class T>
    class Line2D {
    public:
        // Construction
        Line2D() = default;
        Line2D(const Line2D<T>& v) :
                a_(v.a_),
                b_(v.b_) {
        }
        Line2D(const Point2D<T>& a, const Point2D<T>& b) :
                a_(a),
                b_(b) {
        }

        // Operators
        Line2D& operator=(const Line2D& v) {
            a_ = v.a_;
            b_ = v.b_;
            return *this;
        }

        // Make a list of the points where this line crosses the given line list
        void intersections(const std::vector<Line2D<T>>& lines,
                std::vector<Point2D<T>>& points) const {
            points.clear();
            for(auto& line : lines) {
                Point2D<T> p;
                if(intersection(line, p)) {
                    points.push_back(p);
                }
            }
        }

        // Calculate the intersection point between this line and the other.
        // Returns true if the intersection exists.
        bool intersection(const box::Line2D<T>& line, box::Point2D<T>& point) const {
            bool result = false;
            T m1 = m();
            T c1 = c();
            T m2 = line.m();
            T c2 = line.c();
            if(isVertical()) {
                if(!line.isVertical()) {
                    point.x(a_.x());
                    point.y(m2 * point.x() + c2);
                    result = true;
                }
            } else if(line.isVertical()) {
                point.x(line.a().x());
                point.y(m1 * point.x() + c1);
                result = true;
            } else {
                if(m1 != m2) {
                    point.x((c2 - c1) / (m1 - m2));
                    point.y(m1 * point.x() + c1);
                    result = true;
                }
            }
            if(result) {
                result = isIn(point) && line.isIn(point);
            }
            return result;
        }

        // Return true if the point is within the rectangle defined by this line
        bool isIn(const box::Point2D<T>& p) const {
            T minx = std::min(a_.x(), b_.x());
            T maxx = std::max(a_.x(), b_.x());
            T miny = std::min(a_.y(), b_.y());
            T maxy = std::max(a_.y(), b_.y());
            return p.x() >= minx && p.x() <= maxx && p.y() >= miny && p.y() <= maxy;
        }


        // Calculate the gradient (returns Inf for a vertical line)
        [[nodiscard]] T m() const {
            return (b_.y() - a_.y()) / (b_.x() - a_.x());
        }

        // Calculate the y intercept (returns Inf for a vertical line)
        [[nodiscard]] T c() const {
            return a_.y() - m() * a_.x();
        }

        // Return true if the line is vertical (has an infinite gradient)
        [[nodiscard]] bool isVertical() const {
            return b_.x() == a_.x();
        }

        // Getters
        [[nodiscard]] const Point2D<T>& a() const { return a_; }
        [[nodiscard]] const Point2D<T>& b() const { return b_; }

        // Setters
        void a(const Point2D<T>& v) { a_ = v; }
        void b(const Point2D<T>& v) { b_ = v; }

    protected:
        // Data
        Point2D<T> a_;   // The first point
        Point2D<T> b_;   // The second point
    };
}

#endif //FDTDLIFE_LINE2D_H
