/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DFTSPECTRUM_H
#define FDTDLIFE_DFTSPECTRUM_H

#include "CircularBuffer.h"
#include "FrequencySeq.h"
#include "Range.h"
#include <complex>
#include <vector>

namespace box {

    // A class that converts a time series into a spectrum
    // using the Geortzel algorithm.
    class GeortzelSpectrum {
    public:
        // Construction
        GeortzelSpectrum() = default;

        // API
        void initialise(const FrequencySeq& frequencies,
                        size_t numThreadsToUse, double samplePeriod,
                        bool useWindow, size_t bufferSize);
        void calculate(const box::CircularBuffer<double>& timeSeries);
        void calculate(size_t channel, const box::CircularBuffer<double>& timeSeries);
        bool empty() { return spectrum_.empty(); }
        std::vector<std::complex<double>>::iterator begin() { return spectrum_.begin(); }
        std::vector<std::complex<double>>::iterator end() { return spectrum_.end(); }
        std::complex<double> operator[](size_t index) const { return spectrum_[index]; }

        // Getters
        [[nodiscard]] FrequencySeq frequencies() const { return frequencies_; }
        [[nodiscard]] double samplePeriod() const { return samplePeriod_; }
        [[nodiscard]] const std::vector<std::complex<double>>& spectrum() const {
            return spectrum_;
        }
        [[nodiscard]] const std::vector<double>& amplitude() const { return amplitude_; }
        [[nodiscard]] const std::vector<double>& phase() const { return phase_; }

    protected:
        // Configuration
        FrequencySeq frequencies_;  // The frequencies in the spectrum
        size_t numThreadsToUse_{};  // The number of openMP threads to use
        double samplePeriod_{};  // The period of the samples in the time series
        std::vector<double> window_;  // The window function to use

        // Members
        std::vector<std::complex<double>> spectrum_;  // The resulting complex spectrum data
        std::vector<double> amplitude_;  // The amplitude spectrum
        std::vector<double> phase_;  // The phase spectrum (in degrees)

        // Helpers
        void doThread(size_t threadNum, const box::CircularBuffer<double>& timeSeries);
    };

}

#endif //FDTDLIFE_DFTSPECTRUM_H
