/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CONSTANTS_H
#define FDTDLIFE_CONSTANTS_H

#include <initializer_list>
#include <Xml/DomObject.h>

namespace box {
    class Constants {
    public:
        static constexpr double pi_ = 3.141592653;  // Mathematical constant PI
        static constexpr double c_ = 299792458.0;  // Speed of light in vacuum (m/s)
        static constexpr double deg2rad_ = pi_ / 180.0;  // Convert degrees to radians
        static constexpr double epsilon0_ = 8.85418782e-12;  // Permittivity of free space
        static constexpr double mu0_ = 1.25663706e-6;  // Permeability of free space
        static constexpr double sigma0_ = 0.0;  // The electric conductivity of free space
        static constexpr double sigmastar0_ = 0.0;  // The magnetic conductivity of free space
        static constexpr double epsilonMetal_ = -1e12;  // Permittivity of a typical metal
        static constexpr double sigmaMetal_ = 5e7;  // Sigma of a metal
        static constexpr double giga_ = 1e9;  // Multiplier for the giga prefix
        static constexpr double tera_ = 1e12;  // Multiplier for the tera prefix
        static constexpr double pico_ = 1e-12;  // Multiplier for the pico prefix
        static constexpr double kilo_ = 1e3;  // Multiplier for the kilo prefix
        static constexpr double mega_ = 1e6;  // Multiplier for the mega prefix
        static constexpr double milli_ = 0.001;  // Multiplier for milli prefix
        static constexpr double micro_ = 0.000001;  // Multiplier for micro prefix
        static constexpr double cosPiBy3_ = 0.5;  // Cosine of pi / 3
        static constexpr double sinPiBy3_ = 0.8660254;  // Sine of pi / 3
        static constexpr double percent_ = 0.01;  // Multiplier for the percent constant
        static constexpr double half_ = 0.5;  // Multiplier for a half
        static const size_t bitsPerByte_ = 8U;  // Bits in a byte
        static const size_t byteMask_ = 0xffU;  // Mask for a byte

        enum Orientation {
            orientationXPlane, orientationYPlane, orientationZPlane
        };
        static const std::initializer_list<const char*> orientationNames_;

        enum Colour {
            colourBlack, colourRed, colourGreen, colourBlue, colourYellow,
            colourMagenta, colourCyan, colourWhite
        };
        static const std::initializer_list<const char*> colourNames_;
        friend xml::DomObject& operator>>(xml::DomObject& o, Colour& v) {o >> reinterpret_cast<int&>(v); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Orientation& v) {o >> reinterpret_cast<int&>(v); return o;}
    };
}

#endif //FDTDLIFE_CONSTANTS_H
