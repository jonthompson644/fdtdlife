/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_EXPORTWRITER_H
#define FDTDLIFE_EXPORTWRITER_H

#include <string>
#include <fstream>
#include "Vector.h"

namespace box {

    // Base class for all export format writers
    class ExportWriter {
    public:
        enum class Clipping {
            none, xAxis, yAxis, zAxis, xyAxis
        };
        // Construction
        explicit ExportWriter(const std::string& fileName, Clipping clipping = Clipping::none);
        virtual ~ExportWriter() = default;

        // API
        virtual void createMaterial(double /*epsilon*/, double /*mu*/,
                                    const std::string& /*name*/) {}
        virtual void writeCuboid(Vector<double> p1, Vector<double> p2,
                                 const std::string& shapeName,
                                 const std::string& material) = 0;
        virtual void combineStart() {}
        virtual void combineEnd() {}
        virtual void subtractStart() {}
        virtual void subtractEnd() {}
        virtual void groupStart() {}
        virtual void groupEnd() {}
        virtual void duplicateStart() {}
        virtual void duplicateEnd(Vector<double> /*increment*/, size_t /*n*/) {}

    protected:
        // Members
        std::ofstream file_;
        Clipping clipping_{Clipping::none};
        double minDimension_{1e-6};

        // Helpers
        bool clipCuboid(Vector<double>& a, Vector<double>& b);
    };
}


#endif //FDTDLIFE_EXPORTWRITER_H
