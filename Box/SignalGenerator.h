/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SIGNALGENERATOR_H
#define FDTDLIFE_SIGNALGENERATOR_H

#include <cstdlib>
#include "FrequencySeq.h"

namespace box {

    // A class that generates a signal made up of a number
    // of sine waves.
    class SignalGenerator {
    public:
        // Construction
        SignalGenerator();

        // API
        void initialise(double samplePeriod, const FrequencySeq& frequencies,
                        double initialTime, double amplitude);
        double sample(int timeStep, double distance);
        double time(int timeStep);

        // Getters
        [[nodiscard]] double samplePeriod() const { return samplePeriod_; }
        [[nodiscard]] const FrequencySeq& frequencies() const { return frequencies_; }
        [[nodiscard]] double initialTime() const { return initialTime_; }
        [[nodiscard]] double amplitude() const {return amplitude_;}

    protected:
        // Configuration
        double samplePeriod_;   // Time in seconds between samples
        FrequencySeq frequencies_;  // The frequencies in the signal
        double amplitude_;  // The maximum amplitude of the signal
        double initialTime_;  // The time of the first sample
    };

}


#endif //FDTDLIFE_SIGNALGENERATOR_H
