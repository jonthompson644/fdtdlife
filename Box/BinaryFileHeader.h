/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYFILEHEADER_H
#define FDTDLIFE_BINARYFILEHEADER_H

#include <cstddef>

namespace box {

    class BinaryFileHeader {
    private:
        struct Data {
            char identity_[32];
            long version_;
            size_t recordSize_;
            long userInt_[8];
            double userFloat_[8];
            long spare_[8];
        } data_;
    public:
        BinaryFileHeader(const char* identity, long version, size_t recordSize);
        BinaryFileHeader();
        bool is(const char* identity);
        char* data();
        size_t size();
        long version() const { return data_.version_; }
        size_t recordSize() const { return data_.recordSize_; }
        long userInt(int i) const { return data_.userInt_[i]; }
        void userInt(int i, long v) { data_.userInt_[i] = v; }
        double userFloat(int i) const { return data_.userFloat_[i]; }
        void userFloat(int i, double v) { data_.userFloat_[i] = v; }
    };

}


#endif //FDTDLIFE_BINARYFILEHEADER_H
