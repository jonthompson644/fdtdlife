/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DXFWRITER_H
#define FDTDLIFE_DXFWRITER_H

#include "ExportWriter.h"
#include "Rectangle.h"
#include <deque>

namespace box {

    // A class that writes out Autocad DXF file format
    class DxfWriter : public ExportWriter {
    public:
        // Types
        enum class State {
            normal, combining, subtracting, grouping, duplicating
        };
        class Context {
        public:
            explicit Context(State state) : state_(state) {}
            State state_ {State::normal};
            std::list<box::Rectangle<double>> items_;  // Rectangles that are waiting
        };

        // Construction
        explicit DxfWriter(const std::string& fileName, Clipping clipping = Clipping::none);
        ~DxfWriter() override;

        // Overrides of ExportWriter
        void writeCuboid(Vector<double> p1, Vector<double> p2,
                         const std::string& shapeName,
                         const std::string& material) override;
        void combineStart() override;
        void combineEnd() override;
        void subtractStart() override;
        void subtractEnd() override;
        void groupStart() override;
        void groupEnd() override;
        void duplicateStart() override;
        void duplicateEnd(Vector<double> increment, size_t n) override;

    protected:
        // Helpers
        void writeItem(int code, const std::string& item);
        void writeItem(int code, int item);
        void writeItem(int code, double item);
        void writeVertex(double x, double y, double z, const std::string& material,
                         bool last = false, double thickness = 0.0);

        // Members
        std::ofstream extraFile_;
        std::deque<Context> stack_;  // The context stack
    };
}


#endif //FDTDLIFE_DXFWRITER_H
