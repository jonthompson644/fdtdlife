#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Box
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BinaryFileHeader.cpp \
    Constants.cpp \
    GeortzelSpectrum.cpp \
    SignalGenerator.cpp \
    ThreadInfo.cpp \
    Trackable.cpp \
    DxfWriter.cpp \
    BinaryPattern.cpp \
    FrequencySeq.cpp \
    BinaryPattern4Fold.cpp \
    ExportWriter.cpp \
    StlWriter.cpp \
    Polynomial.cpp \
    Utility.cpp

HEADERS += \
    BinaryFileHeader.h \
    CircularBuffer.h \
    Configurable.h \
    Constants.h \
    GeortzelSpectrum.h \
    OpenMP.h \
    SignalGenerator.h \
    ThreadInfo.h \
    Trackable.h \
    DxfWriter.h \
    Vector.h \
    BinaryPattern.h \
    FrequencySeq.h \
    Range.h \
    Rectangle.h \
    Line2D.h \
    Point2D.h \
    UniqueFactory.h \
    BinaryPattern4Fold.h \
    ExportWriter.h \
    StlWriter.h \
    Polynomial.h \
    Utility.h

unix {cpp
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Box.pro

DISTFILES += \
    Box.pro.user \
    CMakeLists.txt \
    Makefile
