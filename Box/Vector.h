/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VECTOR_H
#define FDTDLIFE_VECTOR_H

#include <cmath>
#include <iostream>
#include <utility>
#include <Xml/DomObject.h>

namespace box {

    // A template class that represents a position in 3D space.
    template<typename T, typename = void>
    class Vector {
    protected:
        T x_;   // The coordinates of the position
        T y_;
        T z_;
    public:
        // Default constructor
        Vector() :
                x_(0),
                y_(0),
                z_(0) {}
        // Initialising constructors
        Vector(T x, T y, T z) :
                x_(x),
                y_(y),
                z_(z) {}
        Vector(T v) :
                x_(v),
                y_(v),
                z_(v) {}
        // Copy constructor
        Vector(const Vector<T>& other) { *this = other; }
        // Assignment operator
        Vector& operator=(const Vector<T>& other) {
            x_ = other.x();
            y_ = other.y();
            z_ = other.z();
            return *this;
        }
        Vector& operator=(T v) {
            x_ = v;
            y_ = v;
            z_ = v;
            return *this;
        }
        // Accessors
        [[nodiscard]] T x() const { return x_; }
        [[nodiscard]] T y() const { return y_; }
        [[nodiscard]] T z() const { return z_; }
        void x(T v) { x_ = v; }
        void y(T v) { y_ = v; }
        void z(T v) { z_ = v; }
        // Arithmetic
        Vector<T> operator+(const Vector<T>& other) const {
            return {x_ + other.x(), y_ + other.y(), z_ + other.z()};
        }
        Vector<T> operator-(const Vector<T>& other) const {
            return {x_ - other.x(), y_ - other.y(), z_ - other.z()};
        }
        Vector<T> operator+(T v) const { return {x_ + v, y_ + v, z_ + v}; }
        Vector<T> operator-(T v) const { return {x_ - v, y_ - v, z_ - v}; }
        Vector<T> operator-() const { return {-x_, -y_, -z_}; }
        Vector<T> operator*(T m) const { return {x_ * m, y_ * m, z_ * m}; }
        Vector<T> operator*(const Vector<T>& m) const {
            return {x_ * m.x_, y_ * m.y_, z_ * m.z_};
        }
        Vector<T> operator/(T m) const { return {x_ / m, y_ / m, z_ / m}; }
        Vector<T> operator/(const Vector<T>& m) const {
            return {x_ / m.x_, y_ / m.y_, z_ / m.z_};
        }
        Vector<T>& operator+=(const Vector<T>& other) {
            x_ += other.x();
            y_ += other.y();
            z_ += other.z();
            return *this;
        }
        bool operator==(Vector<T> other) const {
            return x_ == other.x_ && y_ == other.y_ && z_ == other.z_;
        }
        bool operator>=(Vector<T> other) const {
            return x_ >= other.x_ && y_ >= other.y_ && z_ >= other.z_;
        }
        bool operator<=(Vector<T> other) const {
            return x_ <= other.x_ && y_ <= other.y_ && z_ <= other.z_;
        }
        bool operator!=(Vector<T> other) const {
            return x_ != other.x_ || y_ != other.y_ || z_ != other.z_;
        }
        [[nodiscard]] Vector<T> modulo(Vector<T> m) const {
            return Vector<T>(static_cast<T>(fmod(x_, m.x_)), static_cast<T>(fmod(y_, m.y_)),
                             static_cast<T>(fmod(z_, m.z_)));
        }
        [[nodiscard]] double mag() const { return sqrt(x_ * x_ + y_ * y_ + z_ * z_); }
        [[nodiscard]] Vector<T> exp() const {
            return Vector<T>(static_cast<T>(std::exp(x_)), static_cast<T>(std::exp(y_)),
                             static_cast<T>(std::exp(z_)));
        }
        [[nodiscard]] Vector<T> abs() const {
            return Vector<T>(static_cast<T>(std::abs(x_)), static_cast<T>(std::abs(y_)),
                             static_cast<T>(std::abs(z_)));
        }
        [[nodiscard]] Vector<T> min(const Vector<T>& other) const {
            return Vector<T>(std::min(x_, other.x_), std::min(y_, other.y_),
                             std::min(z_, other.z_));
        }
        [[nodiscard]] Vector<T> max(const Vector<T>& other) const {
            return Vector<T>(std::max(x_, other.x_), std::max(y_, other.y_),
                             std::max(z_, other.z_));
        }
        // Casting
        template<class S>
        explicit operator Vector<S>() {
            return {static_cast<S>(x_), static_cast<S>(y_),
                    static_cast<S>(z_)};
        }
        template<class S>
        Vector<S> ceil() const {
            return {static_cast<S>(std::ceil(x_)), static_cast<S>(std::ceil(y_)),
                    static_cast<S>(std::ceil(z_))};
        }
        template<class S>
        Vector<S> floor() const {
            return {static_cast<S>(std::floor(x_)), static_cast<S>(std::floor(y_)),
                    static_cast<S>(std::floor(z_))};
        }
        template<class S>
        Vector<S> round() const {
            return {static_cast<S>(std::round(x_)), static_cast<S>(std::round(y_)),
                    static_cast<S>(std::round(z_))};
        }
        // Signed magnitude and polarisation assumes propagation is mostly along the z axis
        [[nodiscard]] double signedMag() const {
            double v = sqrt(x_ * x_ + y_ * y_ + z_ * z_);
            double d = x_;
            if(d == 0.0) { d = y_; }
            if(d < 0) { v *= -1; }
            return v;
        }
        [[nodiscard]] double polarisation() const {
            return atan2((x_ < 0 ? y_ * -1 : y_), std::abs(x_));
        }
        // Non-member operators
        friend std::ostream& operator<<(std::ostream& s, const Vector<T>& v) {
            s << v.x_ << "," << v.y_ << "," << v.z_;
            return s;
        }
        friend Vector<T> exp(Vector<T> v) { return v.exp(); }
        friend Vector<T> operator/(T a, Vector<T> b) { return Vector<T>(a) / b; }
        friend Vector<T> operator-(T a, Vector<T> b) { return Vector<T>(a) - b; }
        friend Vector<T> operator+(T a, Vector<T> b) { return Vector<T>(a) + b; }
        friend Vector<T> operator*(T a, Vector<T> b) { return Vector<T>(a) * b; }
        friend xml::DomObject& operator<<(xml::DomObject& o, const Vector<T>& v) {
            return o << xml::Open() << xml::Obj("x") << v.x_ << xml::Obj("y")
                     << v.y_ << xml::Obj("z") << v.z_ << xml::Close();
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Vector<T>& v) {
            return o >> xml::Open() >> xml::Obj("x") >> v.x_ >> xml::Obj("y")
                     >> v.y_ >> xml::Obj("z") >> v.z_ >> xml::Close();
        }
    };

    // A specialisation for strings, not really a coordinate but useful anyway
    template<>
    class Vector<std::string> {
    protected:
        std::string x_;   // The coordinates of the position
        std::string y_;
        std::string z_;
    public:
        // Default constructor
        Vector() = default;
        // Initialising constructors
        Vector(std::string x, std::string y, std::string z) :
                x_(std::move(x)),
                y_(std::move(y)),
                z_(std::move(z)) {}
        explicit Vector(const std::string& v) :
                x_(v),
                y_(v),
                z_(v) {}
        // Copy constructor
        Vector(const Vector<std::string>& other) { *this = other; }
        // Assignment operator
        Vector<std::string>& operator=(const Vector<std::string>& other) {
            x_ = other.x();
            y_ = other.y();
            z_ = other.z();
            return *this;
        }
        Vector<std::string>& operator=(const std::string& v) {
            x_ = v;
            y_ = v;
            z_ = v;
            return *this;
        }
        // Accessors
        [[nodiscard]] const std::string& x() const { return x_; }
        [[nodiscard]] const std::string& y() const { return y_; }
        [[nodiscard]] const std::string& z() const { return z_; }
        void x(const std::string& v) { x_ = v; }
        void y(const std::string& v) { y_ = v; }
        void z(const std::string& v) { z_ = v; }
        // Arithmetic
        bool operator==(const Vector<std::string>& other) const {
            return x_ == other.x_ && y_ == other.y_ && z_ == other.z_;
        }
        bool operator!=(const Vector<std::string>& other) const {
            return x_ != other.x_ || y_ != other.y_ || z_ != other.z_;
        }
        // Non-member operators
        friend std::ostream& operator<<(std::ostream& s, const Vector<std::string>& v) {
            s << v.x_ << "," << v.y_ << "," << v.z_;
            return s;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, const Vector<std::string>& v) {
            return o << xml::Open() << xml::Obj("x") << v.x_
                     << xml::Obj("y") << v.y_
                     << xml::Obj("z") << v.z_ << xml::Close();
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Vector<std::string>& v) {
            return o >> xml::Open() >> xml::Obj("x") >> v.x_
                     >> xml::Obj("y") >> v.y_
                     >> xml::Obj("z") >> v.z_ >> xml::Close();
        }
    };

    // A specialisation for enumerations
    template<typename T>
    class Vector<T, typename std::enable_if<std::is_enum<T>::value>::type> {
    protected:
        T x_{};   // The coordinates of the position
        T y_{};
        T z_{};
    public:
        // Default constructor
        Vector() = default;
        // Initialising constructors
        Vector(T x, T y, T z) :
                x_(x),
                y_(y),
                z_(z) {}
        explicit Vector(T v) :
                x_(v),
                y_(v),
                z_(v) {}
        // Copy constructor
        Vector(const Vector<T>& other) { *this = other; }
        // Assignment operator
        Vector& operator=(const Vector<T>& other) {
            x_ = other.x();
            y_ = other.y();
            z_ = other.z();
            return *this;
        }
        Vector& operator=(T v) {
            x_ = v;
            y_ = v;
            z_ = v;
            return *this;
        }
        // Accessors
        T x() const { return x_; }
        T y() const { return y_; }
        T z() const { return z_; }
        void x(T v) { x_ = v; }
        void y(T v) { y_ = v; }
        void z(T v) { z_ = v; }
        // Comparison operators
        bool operator==(const Vector<T>& other) const {
            return x_ == other.x_ && y_ == other.y_ && z_ == other.z_;
        }
        bool operator!=(const Vector<T>& other) const {
            return x_ != other.x_ || y_ != other.y_ || z_ != other.z_;
        }
        // Non-member operators
        friend std::ostream& operator<<(std::ostream& s, const Vector<T>& v) {
            s << static_cast<int>(v.x_) << "," << static_cast<int>(v.y_)
              << "," << static_cast<int>(v.z_);
            return s;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, const Vector<T>& v) {
            return o << xml::Open() << xml::Obj("x") << v.x_
                     << xml::Obj("y") << v.y_
                     << xml::Obj("z") << v.z_ << xml::Close();
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Vector<T>& v) {
            return o >> xml::Open() >> xml::Obj("x") >> v.x_
                     >> xml::Obj("y") >> v.y_
                     >> xml::Obj("z") >> v.z_ >> xml::Close();
        }
    };
}


#endif //FDTDLIFE_VECTOR_H
