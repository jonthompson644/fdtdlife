/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryFileHeader.h"
#include <cstring>

// Construct a file header for writing
box::BinaryFileHeader::BinaryFileHeader(const char *identity, long version,
                                        size_t recordSize) {
    memset(&data_, 0, sizeof(Data));
    strncpy(data_.identity_, identity, 31);
    data_.version_ = version;
    data_.recordSize_ = recordSize;
}

// Construct a file header for reading
box::BinaryFileHeader::BinaryFileHeader() {
    memset(&data_, 0, sizeof(Data));
}

// Check the file header's identity
bool box::BinaryFileHeader::is(const char *identity) {
    return strncmp(data_.identity_, identity, 32) == 0;
}

// Return a data pointer suitable for the read and write functions
char *box::BinaryFileHeader::data() {
    return reinterpret_cast<char*>(&data_);
}

// Returns a data size suitable for the read and write functions
size_t box::BinaryFileHeader::size() {
    return sizeof(Data);
}
