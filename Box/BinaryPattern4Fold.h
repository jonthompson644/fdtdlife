/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYPATTERN4FOLD_H
#define FDTDLIFE_BINARYPATTERN4FOLD_H

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <vector>

namespace box {
    class BinaryPattern;

    // A class that represents the binary codes of 2Nx2N 4 fold symmetrical patterns.
    // The pattern member effectively contains the pattern of the top left triangle
    // of the full 2Nx2N pattern.
    class BinaryPattern4Fold {
    public:
        // Construction
        explicit BinaryPattern4Fold(size_t n = 0, uint64_t pattern = 0U);
        BinaryPattern4Fold(const BinaryPattern4Fold& other) = default;
        // API
        void clear();
        // Operators
        BinaryPattern4Fold& operator=(const BinaryPattern4Fold& other) = default;
        BinaryPattern4Fold& operator=(const BinaryPattern& other);
        // Getters
        size_t n() const { return n_; }
        const std::vector<uint64_t>& pattern() const { return pattern_; }
        size_t requiredNumBits() const {return requiredNumBits_;}
        // Setters
        void n(size_t v) { n_ = v; calcRequiredNumBits(); }
        void pattern(const std::vector<uint64_t>& v) { pattern_ = v; }
    protected:
        // Members
        size_t n_;
        std::vector<uint64_t> pattern_;
        size_t requiredNumBits_;
        // Constants
        static const size_t bitsPerWord_ = 64;
        // Helpers
        void calcRequiredNumBits();
    };

}


#endif //FDTDLIFE_BINARYPATTERN4FOLD_H
