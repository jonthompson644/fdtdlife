/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_RANGE_H
#define FDTDLIFE_RANGE_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <complex>

namespace box {

    // A template class that represents a range of values from a vector.
    template <class T>
    class Range {
    protected:
        T min_;   // The range of values
        T max_;
        bool empty_{true};
    public:
        // Default constructor
        Range() : min_(0), max_(0) {}
        // Initialising constructor
        Range(T min, T max) : min_(min), max_(max) {}
        // Copy constructor
        Range(const Range &other) {*this = other;}
        // Assignment operator
        Range& operator=(const Range& other) {
            min_=other.min_, max_=other.max_;
            return *this;
        }
        // Accessors
        T min() const {return min_;}
        T max() const {return max_;}
        void min(T v) {min_ = v;}
        void max(T v) {max_ = v;}
        // Operations
        void evaluate(std::vector<T> a) {
            if(a.empty()) {
                min_ = 0;
                max_ = 0;
                empty_ = true;
            } else {
                min_ = a[0];
                max_ = a[0];
                for(auto b : a) {
                    min_ = std::min(min_, b);
                    max_ = std::max(max_, b);
                }
                empty_ = false;
            }
        }
        void evaluate(std::vector<std::complex<T>> a) {
            if(a.empty()) {
                min_ = 0;
                max_ = 0;
                empty_ = true;
            } else {
                min_ = a[0].real();
                max_ = a[0].real();
                for(auto b : a) {
                    min_ = std::min(min_, b.real());
                    min_ = std::min(min_, b.imag());
                    max_ = std::max(max_, b.real());
                    max_ = std::max(max_, b.imag());
                }
                empty_ = false;
            }
        }
        void evaluate(T v) {
            if(empty_) {
                min_ = v;
                max_ = v;
            } else {
                min_ = std::min(min_, v);
                max_ = std::max(max_, v);
            }
            empty_ = false;
        }
        Range operator|(const Range& other) {
            Range result;
            result.min_ = std::min(min_, other.min_);
            result.max_ = std::max(max_, other.max_);
            return result;
        }
        Range& operator|=(const Range& other) {
            min_ = std::min(min_, other.min_);
            max_ = std::max(max_, other.max_);
            return *this;
        }
        // Non member functions
        friend std::ostream& operator << (std::ostream& s, const Range<T>& v) {
            s << v.min_ << "..." << v.max_;
            return s;
        }
    };
}


#endif //FDTDLIFE_RANGE_H
