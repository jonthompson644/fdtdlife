/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYPATTERN_H
#define FDTDLIFE_BINARYPATTERN_H

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <vector>

namespace box {
    class BinaryPattern4Fold;

    // A class that represents the binary codes of 2Nx2N 2 fold symmetrical patterns
    // The pattern member effectively contains the pattern of the top left NxN quadrant
    // of the full 2Nx2N pattern.
    class BinaryPattern {
    public:
        // Construction
        explicit BinaryPattern(size_t n = 0, uint64_t pattern = 0);
        BinaryPattern(size_t n, const std::vector<uint64_t>& pattern);
        BinaryPattern(const BinaryPattern& other) = default;
        // API
        [[nodiscard]] bool get(size_t x, size_t y) const;
        void set(size_t x, size_t y, bool v);
        void translateX();
        void translateY();
        void rotate90();
        void clear();
        void initialise(size_t n, const std::vector<uint64_t>& pattern);
        [[nodiscard]] bool hasCornerToCorner() const;
        [[nodiscard]] std::string hexadecimal() const;
        // Operators
        BinaryPattern& operator=(const BinaryPattern& other) = default;
        BinaryPattern& operator=(const BinaryPattern4Fold& other);
        bool operator!=(const BinaryPattern& other) const;
        bool operator==(const BinaryPattern& other) const;
        // Getters
        [[nodiscard]] const std::vector<uint64_t>& pattern() const {return pattern_;}
        [[nodiscard]] size_t n() const {return n_;}
        // Setters
        void pattern(const std::vector<uint64_t>& v) {pattern_ = v;}
        void n(size_t v);
    protected:
        // Members
        size_t n_ {0};
        std::vector<uint64_t> pattern_;
        // Helpers
        void index(size_t x, size_t y, size_t& word, size_t& bit) const;
        [[nodiscard]] bool testCornerToCorner(size_t x, size_t y) const;
        // Constants
        static const size_t bitsPerWord_ = 64;
    public:
        // Non member functions
        friend std::ostream& operator<<(std::ostream& s, const BinaryPattern& v);
    };
}


#endif //FDTDLIFE_BINARYPATTERN_H
