/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_UTILITY_H
#define FDTDLIFE_UTILITY_H

#include <string>
#include <vector>
#include <sstream>
#include <complex>

namespace box {

    // Utility functions
    class Utility {
    public:
        static bool equals(double a, double b);
        static double interpolate(double x1, double x2, double y1, double y2, double x);
        static std::complex<double> interpolate(double x1, double x2, std::complex<double> y1,
                                                std::complex<double> y2, double x);
        static bool endsWith(const std::string& str, const std::string& suffix);
        static bool startsWith(const std::string& str, const std::string& prefix);
        static std::vector<std::string> split(const std::string& str, const std::string& sep);
        static std::string strip(const std::string& str, const std::string& chars);
        static std::string stripFront(const std::string& str, const std::string& chars);
        static std::string stripBack(const std::string& str, const std::string& chars);
        static double toDouble(const std::string& str);
        static std::complex<double> toComplex(const std::string& str);
        static int toInt(const std::string& str);
        static size_t toSizeT(const std::string& str);
        static std::string fromDouble(double value);
        static std::string fromComplex(const std::complex<double>& value);
        static std::string fromSizeT(size_t value);
        static std::string fromInt(int value);
        static bool exists(const std::string& filename);
        static std::string stem(const std::string& filename);
        static std::string path(const std::string& filename);
        static std::string extension(const std::string& filename);
        static std::string makeFilename(const std::string& path, const std::string& stem,
                                        const std::string& extension);
        static double square(double v) { return v * v; }

        template<class T>
        static std::string vectorToString(const std::vector<T>& a) {
            std::stringstream t;
            t << std::hex;
            for(auto c : a) {
                t << c << " ";
            }
            return t.str();
        }

        template<class T>
        static std::vector<T> stringToVector(const std::string& a) {
            std::stringstream t(a);
            T v;
            std::vector<T> result;
            t >> std::hex;
            while(!t.eof()) {
                t >> v;
                if(!t.eof()) {
                    result.push_back(v);
                }
            }
            return result;
        }
    };

}


#endif //FDTDLIFE_UTILITY_H
