/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeortzelSpectrum.h"
#include "OpenMP.h"
#include "Constants.h"

// Initialise ready to perform calculations
void box::GeortzelSpectrum::initialise(const FrequencySeq& frequencies,
                                       size_t numThreadsToUse, double samplePeriod,
                                       bool useWindow, size_t bufferSize) {
    // Record information
    frequencies_ = frequencies;
    numThreadsToUse_ = numThreadsToUse;
    samplePeriod_ = samplePeriod;
    // Make the spectrum array the right size
    spectrum_.resize(frequencies_.n());
    // And the phase/power results
    amplitude_.resize(frequencies_.n());
    phase_.resize(frequencies_.n());
    // Make the window
    window_.resize(bufferSize);
    for(size_t i = 0; i < bufferSize; ++i) {
        if(useWindow) {
            window_[i] = 0.5 * (1 - cos(2 * box::Constants::pi_ * i / bufferSize));
        } else {
            window_[i] = 1.0;
        }
    }
}

// Perform the transform using openMP if available
void box::GeortzelSpectrum::calculate(const box::CircularBuffer<double>& timeSeries) {
#pragma omp parallel default(shared)
    {
        doThread((size_t) omp_get_thread_num(), timeSeries);
    }
}

// Perform some of the transform on one core
void box::GeortzelSpectrum::doThread(size_t threadNum,
                                     const box::CircularBuffer<double>& timeSeries) {
    // Work out which channels this thread should do
    size_t chansPerThread = frequencies_.n() / numThreadsToUse_;
    size_t chanStart = threadNum * chansPerThread;
    size_t chanEnd = chanStart + chansPerThread;
    if(threadNum == numThreadsToUse_ - 1) {
        chanEnd = frequencies_.n();
    }
    // Iterate over the channels
    for(size_t n = chanStart; n < chanEnd; n++) {
        calculate(n, timeSeries);
    }
}

// Perform the transform for one channel
void box::GeortzelSpectrum::calculate(
        size_t channel, const box::CircularBuffer<double>& timeSeries) {
    // Calculate the valid angular frequency nearest this channel
    double freq = frequencies_.frequency(channel);
    double k = round(freq * timeSeries.size() * samplePeriod_);
    double omega = 2.0 * box::Constants::pi_ * k / timeSeries.size();
    // Precalculate some values
    double cosine = cos(omega);
    double sine = sin(omega);
    double coeff = 2 * cosine;
    double scaling = (double) timeSeries.size() / 2.0;
    // Now the filter loop
    double q1 = 0.0;
    double q2 = 0.0;
    for(size_t i = 0; i < timeSeries.size(); ++i) {
        double q0 = q1 * coeff - q2 + timeSeries[i] * window_[i];
        q2 = q1;
        q1 = q0;
    }
    // Calculate real and imaginary
    spectrum_[channel].real((q1 - q2 * cosine) / scaling);
    spectrum_[channel].imag((q2 * sine) / scaling);
    // And the power and phase
    amplitude_[channel] = std::abs(spectrum_[channel]);
    phase_[channel] = std::arg(spectrum_[channel]);
}







