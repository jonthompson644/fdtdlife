/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_RECTANGLE_H
#define FDTDLIFE_RECTANGLE_H

#include <iostream>
#include <list>

namespace box {

    // A template class that represents a rectangle in 2D space.
    // The top and left are inside the rectangle, bottom and right are outside.
    template<class T>
    class Rectangle {
    protected:
        T top_{};   // The location of the rectangle
        T left_{};
        T bottom_{};
        T right_{};
    public:
        // Default constructor
        Rectangle() = default;
        // Initialising constructor
        Rectangle(T left, T top, T right, T bottom) :
                top_(top),
                left_(left),
                bottom_(bottom),
                right_(right) {}
        // Copy constructor
        Rectangle(const Rectangle& other) { *this = other; }
        // Assignment operator
        Rectangle& operator=(const Rectangle& other) {
            if(this != &other) {
                top_ = other.top_;
                left_ = other.left_;
                bottom_ = other.bottom_;
                right_ = other.right_;
            }
            return *this;
        }
        // Accessors
        T top() const { return top_; }
        T left() const { return left_; }
        T bottom() const { return bottom_; }
        T right() const { return right_; }
        void top(T v) { top_ = v; }
        void left(T v) { left_ = v; }
        void bottom(T v) { bottom_ = v; }
        void right(T v) { right_ = v; }
        // Operations
        Rectangle operator/(T v) {
            return {left_ / v, top_ / v, right_ / v, bottom_ / v};
        }
        Rectangle clip(const Rectangle& other) const {
            Rectangle result;
            result.top_ = std::max(other.top_, top_);
            result.left_ = std::max(other.left_, left_);
            result.bottom_ = std::min(other.bottom_, bottom_);
            result.right_ = std::min(other.right_, right_);
            return result;
        }
        [[nodiscard]] bool isValid() const {
            return left_ < right_ && top_ < bottom_;
        }
        [[nodiscard]] double centerX() const {
            return ((double) left_ + (double) (right_ - 1)) / 2.0;
        }
        [[nodiscard]] double centerY() const {
            return ((double) top_ + (double) (bottom_ - 1)) / 2.0;
        }
        bool isIn(T x, T y) const {
            return x >= left_ && x < right_ && y >= top_ && y < bottom_;
        }
        static Rectangle square(T x, T y, T size) {
            T hs = size / 2;
            return Rectangle(x - hs, y - hs, x + hs, y + hs);
        }
        static Rectangle rectangle(T x, T y, T width, T height) {
            T hw = width / 2;
            T hh = height / 2;
            return Rectangle(x - hw, y - hh, x + hw, y + hh);
        }
        static bool isInAny(const std::list<Rectangle<T>>& rects, T x, T y) {
            bool result = false;
            for(auto& r : rects) {
                result = result || r.isIn(x, y);
            }
            return result;
        }
        // Non member functions
        friend std::ostream& operator<<(std::ostream& s, const Rectangle<T>& v) {
            s << v.left_ << "," << v.top_ << "..." << v.right_ << "," << v.bottom_;
            return s;
        }
    };
}


#endif //FDTDLIFE_RECTANGLE_H
