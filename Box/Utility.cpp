/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Utility.h"
#include <cfloat>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <sys/stat.h>

// Compare two doubles for equality.
// Got this from here:
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
// It doesn't work for numbers that are supposed to be zero but aren't quite
// But we shouldn't have that case here, any zeroes should be exactly zero.
bool box::Utility::equals(double a, double b) {
    double diff = fabs(a - b);
    double absA = fabs(a);
    double absB = fabs(b);
    double largest = std::max(absA, absB) * DBL_EPSILON * 20.0;
    bool result = (diff <= largest);
    return result;
}

// Interpolate between two points, x1,y1 and x2,y2 to find the y at x
double box::Utility::interpolate(double x1, double x2, double y1, double y2, double x) {
    double result;
    if(box::Utility::equals(x1, x2)) {
        result = (y1 + y2) / 2.0;
    } else {
        result = ((y2 - y1) / (x2 - x1)) * (x - x1) + y1;
    }
    return result;
}

// Interpolate between two points, x1,y1 and x2,y2 to find the y at x
std::complex<double> box::Utility::interpolate(
        double x1, double x2, std::complex<double> y1,
        std::complex<double> y2, double x) {
    return {interpolate(x1, x2, y1.real(), y2.real(), x),
            interpolate(x1, x2, y1.imag(), y2.imag(), x)};
}

// Returns true if the string ends with the suffix
bool box::Utility::endsWith(const std::string& str, const std::string& suffix) {
    bool result = false;
    if(str.length() >= suffix.length()) {
        result = str.compare(str.length() - suffix.length(), suffix.length(), suffix)
                 == 0;
    }
    return result;
}

// Returns true if the string starts with the prefix
bool box::Utility::startsWith(const std::string& str, const std::string& prefix) {
    bool result = false;
    if(str.length() >= prefix.length()) {
        result = str.compare(0, prefix.length(), prefix)
                 == 0;
    }
    return result;
}

// Split a string into an array of parts
std::vector<std::string> box::Utility::split(const std::string& str, const std::string& sep) {
    std::vector<std::string> result;
    if(!str.empty()) {
        std::string::size_type lastPos = 0;
        std::string::size_type pos = str.find(sep);
        while(pos != std::string::npos) {
            result.emplace_back(str.substr(lastPos, pos - lastPos));
            lastPos = pos + sep.size();
            pos = str.find(sep, lastPos);
        }
        result.emplace_back(str.substr(lastPos));
    }
    return result;
}

// Strip the specified characters from the ends of the string
std::string box::Utility::strip(const std::string& str, const std::string& chars) {
    return stripBack(stripFront(str, chars), chars);
}

// Strip the specified characters from the front of the string
std::string box::Utility::stripFront(const std::string& str, const std::string& chars) {
    std::string result;
    if(!str.empty()) {
        std::string::size_type pos = str.find_first_not_of(chars);
        if(pos != std::string::npos) {
            result = str.substr(pos);
        }
    }
    return result;
}

// Strip the specified characters from the back of the string
std::string box::Utility::stripBack(const std::string& str, const std::string& chars) {
    std::string result;
    if(!str.empty()) {
        std::string::size_type pos = str.size();
        while(pos > 0 && chars.find(str[pos - 1]) != std::string::npos) {
            pos--;
        }
        result = str.substr(0, pos);
    }
    return result;
}

// Convert the string into a double
double box::Utility::toDouble(const std::string& str) {
    return std::strtod(str.c_str(), nullptr);
}

// Convert the string into a complex
// Uses the excel format of x+yi (or x+yj)
std::complex<double> box::Utility::toComplex(const std::string& str) {
    char* nextCh;
    std::complex<double> result;
    // The real part
    result.real(std::strtod(str.c_str(), &nextCh));
    if(*nextCh != '\0') {
        // There's an imaginary part
        result.imag(std::strtod(nextCh, nullptr));
    }
    return result;
}

// Convert the string into an integer
int box::Utility::toInt(const std::string& str) {
    return static_cast<int>(std::strtol(str.c_str(), nullptr, 10));
}

// Convert the string into a size_t
size_t box::Utility::toSizeT(const std::string& str) {
    return static_cast<size_t>(std::strtoull(str.c_str(), nullptr, 10));
}

// Convert a double to a string
std::string box::Utility::fromDouble(double value) {
    std::stringstream s;
    s << value;
    return s.str();
}

// Convert a complex to a string
std::string box::Utility::fromComplex(const std::complex<double>& value) {
    std::stringstream s;
    s << value.real();
    if(value.imag() >= 0.0) {
        s << "+";
    }
    s << value.imag() << "i";
    return s.str();
}

// Convert a size_t to a string
std::string box::Utility::fromSizeT(size_t value) {
    std::stringstream s;
    s << value;
    return s.str();
}

// Convert an int to a string
std::string box::Utility::fromInt(int value) {
    std::stringstream s;
    s << value;
    return s.str();
}

// Return true if the file exists
bool box::Utility::exists(const std::string& filename) {
    struct stat buffer;
    return stat(filename.c_str(), &buffer) == 0;
}

// Return the directory path part of the filename
std::string box::Utility::path(const std::string& filename) {
    std::string result;
    int lastSlash = filename.find_last_of('/');
    if(lastSlash >= 0) {
        result = filename.substr(0, lastSlash);
    }
    return result;
}

// Return the name part of the filename
std::string box::Utility::stem(const std::string& filename) {
    std::string result = filename;
    int lastSlash = result.find_last_of('/');
    if(lastSlash >= 0) {
        result = result.substr(lastSlash + 1);
    }
    int lastDot = result.find_last_of('.');
    if(lastDot >= 0) {
        result = result.substr(0, lastDot);
    }
    return result;
}

// Return the extension part of the filename
std::string box::Utility::extension(const std::string& filename) {
    std::string result;
    int lastDot = filename.find_last_of('.');
    if(lastDot >= 0) {
        result = filename.substr(lastDot + 1);
    }
    return result;
}

// Makes a filename from its component parts
std::string box::Utility::makeFilename(const std::string& path, const std::string& stem,
                                       const std::string& extension) {
    std::string result = path;
    if(result.back() != '/') {
        result += "/";
    }
    result += stem + "." + extension;
    return result;
}
