/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "StlWriter.h"
#include "Utility.h"

// Constructor
box::StlWriter::StlWriter(const std::string& fileName, Clipping clipping) :
        ExportWriter(fileName, clipping) {
}

// Write a cuboid shape to the file
void box::StlWriter::writeCuboid(Vector<double> a, Vector<double> b,
                                 const std::string& shapeName,
                                 const std::string& materialName) {
    if(file_.is_open()) {
        // Enforce any clipping
        if(clipCuboid(a, b)) {
            // The shape header
            file_ << std::scientific << "solid " << materialName << ":" << shapeName
                  << std::endl;
            // Min z face
            writeFacet({a.x(), a.y(), a.z()}, {b.x(), a.y(), a.z()}, {b.x(), b.y(), a.z()});
            writeFacet({b.x(), b.y(), a.z()}, {a.x(), b.y(), a.z()}, {a.x(), a.y(), a.z()});
            // Max z face
            writeFacet({a.x(), a.y(), b.z()}, {a.x(), b.y(), b.z()}, {b.x(), b.y(), b.z()});
            writeFacet({b.x(), b.y(), b.z()}, {b.x(), a.y(), b.z()}, {a.x(), a.y(), b.z()});
            // Min x face
            writeFacet({a.x(), a.y(), a.z()}, {a.x(), b.y(), a.z()}, {a.x(), b.y(), b.z()});
            writeFacet({a.x(), b.y(), b.z()}, {a.x(), a.y(), b.z()}, {a.x(), a.y(), a.z()});
            // Max x face
            writeFacet({b.x(), a.y(), a.z()}, {b.x(), a.y(), b.z()}, {b.x(), b.y(), b.z()});
            writeFacet({b.x(), b.y(), b.z()}, {b.x(), b.y(), a.z()}, {b.x(), a.y(), a.z()});
            // Min y face
            writeFacet({a.x(), a.y(), a.z()}, {a.x(), a.y(), b.z()}, {b.x(), a.y(), b.z()});
            writeFacet({b.x(), a.y(), b.z()}, {b.x(), a.y(), a.z()}, {a.x(), a.y(), a.z()});
            // Max y face
            writeFacet({a.x(), b.y(), a.z()}, {b.x(), b.y(), a.z()}, {b.x(), b.y(), b.z()});
            writeFacet({b.x(), b.y(), b.z()}, {a.x(), b.y(), b.z()}, {a.x(), b.y(), a.z()});
            // The shape footer
            file_ << "endsolid " << materialName << ":" << shapeName << std::endl << std::endl;
        }
    }
}

// Write a single triangular facet to the file
void box::StlWriter::writeFacet(const Vector<double>& a, const Vector<double>& b,
                                const Vector<double>& c) {
    // Calculate the normal
    Vector<double> u = a - c;
    Vector<double> v = a - b;
    Vector<double> normal(u.y() * v.z() - u.z() * v.y(), u.z() * v.x() - u.x() * v.z(),
                          u.x() * v.y() - u.y() * v.x());
    if(normal.mag() == 0.0) {
        std::cout << "StlWriter: Writing facet with illegal normal" << std::endl;
    }
    normal = normal / normal.mag();
    // The facet header
    file_ << "facet normal " << normal.x() << " " << normal.y() << " " << normal.z()
          << std::endl;
    file_ << "  outer loop" << std::endl;
    file_ << "    vertex " << a.x() << " " << a.y() << " " << a.z() << std::endl;
    file_ << "    vertex " << b.x() << " " << b.y() << " " << b.z() << std::endl;
    file_ << "    vertex " << c.x() << " " << c.y() << " " << c.z() << std::endl;
    file_ << "  endloop" << std::endl;
    file_ << "endfacet" << std::endl;
}
