/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FACTORY_H
#define FDTDLIFE_FACTORY_H

#include <Xml/DomObject.h>
#include <map>
#include <set>
#include <list>
#include <string>
#include <utility>
#include <memory>

namespace box {

    template<class T, class M=int>
    class Factory {
    public:
        // Creator base class
        class CreatorBase {
        public:
            CreatorBase(M mode, std::string name) :
                    name_(std::move(name)),
                    mode_(mode) {}
            virtual ~CreatorBase() = default;
            virtual T* create() = 0;
            std::string name_;
            M mode_;
        };

        // Creator template
        template<class S>
        class Creator : public CreatorBase {
        public:
            explicit Creator(M mode, const std::string& name = "") :
                    CreatorBase(mode, name) {}
            T* create() override { return new S; };
        };

        // Exception class
        class Exception : public std::exception {
        public:
            std::string what_;
        public:
            explicit Exception(std::string what) :
                    what_(std::move(what)) {}
            Exception(const Exception& other) noexcept:
                    what_(other.what_) {}
            ~Exception() override = default;
            [[nodiscard]] const char* what() const noexcept override { return what_.c_str(); }
        };

        // Construction
        explicit Factory(int firstId = 0, std::string modeXmlName = "mode") :
                nextId_(firstId),
                modeXmlName_(std::move(modeXmlName)) {
        }

        // API
        void clear(int firstId = 0) {
            nextId_ = firstId;
        }
        template<class... Args>
        T* make(M mode, Args... args) {
            return createOne(nextId_, mode, args...);
        }
        template<class... Args>
        T* restore(xml::DomObject& o, Args... args) {
            int id{};
            M mode{};
            o >> xml::Obj("identifier") >> id;
            o >> xml::Obj(modeXmlName_) >> mode;
            return createOne(id, mode, args...);
        }
        template<class... Args>
        T* restore(int defaultMode, xml::DomObject& o, Args... args) {
            int id{};
            M mode{};
            o >> xml::Obj("identifier") >> id;
            o >> xml::Obj(modeXmlName_) >> xml::Default(defaultMode) >> mode;
            return createOne(id, mode, args...);
        }
        template<class... Args>
        T* duplicate(xml::DomObject& o, Args... args) {
            M mode{};
            o >> xml::Obj("mode") >> mode;
            return createOne(nextId_, mode, args...);
        }
        [[nodiscard]] const std::string& modeName(M mode) const {
            if(creators_.count(mode) == 0) {
                std::stringstream s;
                s << "Mode " << static_cast<int>(mode) << " is not defined";
                throw Exception(s.str());
            }
            return creators_.at(mode)->name_;
        }
        void define(CreatorBase* creator) {
            if(creators_.count(creator->mode_) != 0) {
                std::stringstream s;
                s << "Mode " << static_cast<int>(creator->mode_) << " is already defined";
                throw Exception(s.str());
            }
            creators_[creator->mode_].reset(creator);
        }
        template<class... Args>
        T* createOne(int id, M mode, Args... args) {
            if(id >= nextId_) {
                nextId_ = id + 1;
            }
            if(creators_.count(mode) == 0) {
                std::stringstream s;
                s << "Mode " << static_cast<int>(mode) << " is not defined";
                throw Exception(s.str());
            }
            T* result = creators_.at(mode)->create();
            result->construct(mode, id, args...);
            return result;
        }
        int allocId() {
            return nextId_++;
        }
        [[nodiscard]] std::list<std::pair<M, std::string>> modeNames(
                const std::set<M>& include = {}) const {
            std::list<std::pair<M, std::string>> result;
            for(auto& c : creators_) {
                if(include.empty() || include.count(c.first)) {
                    result.emplace_back(c.first, c.second->name_);
                }
            }
            return result;
        }
        M nameToMode(const std::string& name) {
            M result{};
            bool found {false};
            for(auto& c : creators_) {
                if(c.second->name_ == name) {
                    result = c.first;
                    found = true;
                }
            }
            if(!found) {
                std::stringstream s;
                s << "Type " << name << " is not defined";
                throw Exception(s.str());
            }
            return result;
        }

    protected:
        // Members
        int nextId_;
        std::string modeXmlName_;
        std::map<M, std::unique_ptr<CreatorBase>> creators_;
    };

}

#endif //FDTDLIFE_FACTORY_H
