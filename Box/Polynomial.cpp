/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Polynomial.h"
#include <memory>

// Initialising constructor
box::Polynomial::Polynomial(double a, double b, double c, double d, double e, double f) :
        a_(a),
        b_(b),
        c_(c),
        d_(d),
        e_(e),
        f_(f) {
}

// Calculate the value of the polynomial at the specified x
double box::Polynomial::value(double x) const {
    return a_ + b_ * x + c_ * x * x + d_ * x * x * x +
           e_ * x * x * x * x + f_ * x * x * x * x * x;
}

// Write the polynomial to the XML file
void box::Polynomial::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("a") << a_;
    p << xml::Obj("b") << b_;
    p << xml::Obj("c") << c_;
    p << xml::Obj("d") << d_;
    p << xml::Obj("e") << e_;
    p << xml::Obj("f") << f_;
    p << xml::Close();
}

// Read the polynomial from the XML file
void box::Polynomial::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("a") >> xml::Default(a_) >> a_;
    p >> xml::Obj("b") >> xml::Default(b_) >> b_;
    p >> xml::Obj("c") >> xml::Default(c_) >> c_;
    p >> xml::Obj("d") >> xml::Default(d_) >> d_;
    p >> xml::Obj("e") >> xml::Default(e_) >> e_;
    p >> xml::Obj("f") >> xml::Default(f_) >> f_;
    p >> xml::Close();
}

// Equality operator
bool box::Polynomial::operator==(const Polynomial& other) const {
    return a_ == other.a_ && b_ == other.b_ && c_ == other.c_ &&
    d_ == other.d_ && e_ == other.e_ && f_ == other.f_;
}

// Equality operator
bool box::Polynomial::operator!=(const Polynomial& other) const {
    return !(*this == other);
}

// Fit the polynomial to the given data
// The code for this function is adapted from
// https://gist.github.com/chrisengelsma/108f7ab0a746323beaaf7d6634cf4add
// by Chris Endelsma which is published under the MIT license:
//
// * Copyright (c) 2020 Chris Engelsma
// *
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// *
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// *
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
bool box::Polynomial::fit(const std::vector<double>& x, const std::vector<double>& y,
                          int order) {
    bool result = false;
    if(x.size() == y.size() && !x.empty()) {
        int orderp1 = order + 1;
        int orderp2 = order + 2;
        int torderp1 = 2 * order + 1;
        double tmp;

        // sigma = vector that stores values of sigma(xi^2n)
        std::vector<double> sigma(torderp1);
        for (int i = 0; i < torderp1; ++i) {
            sigma[i] = 0;
            for(double j : x) {
                sigma[i] += pow(j, i);
            }
        }

        // a = vector to store final coefficients.
        std::vector<double> a(orderp1);

        // b = normal augmented matrix that stores the equations.
        std::vector<std::vector<double> > b(orderp1, std::vector<double> (orderp2, 0));

        for (int i = 0; i <= order; ++i) {
            for(int j = 0; j <= order; ++j) {
                b[i][j] = sigma[i + j];
            }
        }

        // Y = vector to store values of sigma(xi^n * yi)
        std::vector<double> Y(orderp1);
        for (int i = 0; i < orderp1; ++i) {
            Y[i] = 0.0;
            for (size_t j = 0; j < x.size(); ++j) {
                Y[i] += pow(x[j], i)*y[j];
            }
        }

        // Load values of Y as last column of B
        for (int i = 0; i <= order; ++i) {
            b[i][orderp1] = Y[i];
        }

        // Pivotisation of the B matrix.
        for (int i = 0; i < orderp1; ++i) {
            for(int k = i + 1; k < orderp1; ++k) {
                if(b[i][i] < b[k][i]) {
                    for(int j = 0; j <= orderp1; ++j) {
                        tmp = b[i][j];
                        b[i][j] = b[k][j];
                        b[k][j] = tmp;
                    }
                }
            }
        }

        // Performs the Gaussian elimination.
        // (1) Make all elements below the pivot equals to zero
        //     or eliminate the variable.
        for (int i=0; i<order; ++i) {
            for(int k = i + 1; k < orderp1; ++k) {
                double t = b[k][i] / b[i][i];
                for(int j = 0; j <= orderp1; ++j) {
                    b[k][j] -= t * b[i][j];         // (1)
                }
            }
        }

        // Back substitution.
        // (1) Set the variable as the rhs of last equation
        // (2) Subtract all lhs values except the target coefficient.
        // (3) Divide rhs by coefficient of variable being calculated.
        for (int i=order; i >= 0; --i) {
            a[i] = b[i][orderp1];                   // (1)
            for (int j = 0; j<orderp1; ++j) {
                if(j != i) {
                    a[i] -= b[i][j] * a[j];       // (2)
                }
            }
            a[i] /= b[i][i];                  // (3)
        }

        // Extract the coefficients
        clear();
        if(orderp1 >= 1) {
            a_ = a[0];
        }
        if(orderp1 >= 2) {
            b_ = a[1];
        }
        if(orderp1 >= 3) {
            c_ = a[2];
        }
        if(orderp1 >= 4) {
            d_ = a[3];
        }
        if(orderp1 >= 5) {
            e_ = a[4];
        }
        if(orderp1 >= 6) {
            f_ = a[5];
        }

        result = true;
    }
    return result;
}

// Clear the polynomial coefficients
void box::Polynomial::clear() {
    a_ = 0.0;
    b_ = 0.0;
    c_ = 0.0;
    d_ = 0.0;
    e_ = 0.0;
    f_ = 0.0;
}
