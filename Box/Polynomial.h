/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_POLYNOMIAL_H
#define FDTDLIFE_POLYNOMIAL_H

#include <Xml/DomObject.h>

namespace box {

    // A class that represents a polynomial.
    // Currently a + b*x + c*x^2 + d*x^3 + e*x^4 + f*x^5
    class Polynomial {
    public:
        Polynomial() = default;
        Polynomial(double a, double b, double c, double d, double e, double f);
        Polynomial(const Polynomial& other) = default;
        Polynomial& operator=(const Polynomial& other) = default;
        bool operator==(const Polynomial& other) const;
        bool operator!=(const Polynomial& other) const;
        [[nodiscard]] double value(double x) const;
        void writeXml(xml::DomObject& p) const;
        void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Polynomial& p) {
            p.writeXml(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Polynomial& p) {
            p.readXml(o);
            return o;
        }
        bool fit(const std::vector<double>& x, const std::vector<double>& y, int order);
        void clear();
        // Getters
        [[nodiscard]] double a() const { return a_; }
        [[nodiscard]] double b() const { return b_; }
        [[nodiscard]] double c() const { return c_; }
        [[nodiscard]] double d() const { return d_; }
        [[nodiscard]] double e() const { return e_; }
        [[nodiscard]] double f() const { return f_; }
        // Setters
        void a(double v) { a_ = v; }
        void b(double v) { b_ = v; }
        void c(double v) { c_ = v; }
        void d(double v) { d_ = v; }
        void e(double v) { e_ = v; }
        void f(double v) { f_ = v; }
    protected:
        double a_{};
        double b_{};
        double c_{};
        double d_{};
        double e_{};
        double f_{};
    };

}


#endif //FDTDLIFE_POLYNOMIAL_H
