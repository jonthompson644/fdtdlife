/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_HFSSWRITER_H
#define FDTDLIFE_HFSSWRITER_H

#include "ExportWriter.h"
#include <set>
#include <deque>

namespace box {

    // A class that writes out a python script for use with HFSS
    class HfssWriter : public ExportWriter {
    public:
        // Types
        enum class State {
            normal, combining, subtracting, grouping, duplicating
        };
        class Context {
        public:
            explicit Context(State state) : state_(state) {}
            State state_ {State::normal};
            std::list<std::string> items_;  // Items that are waiting to be operated on
            bool perfectE_ {false};  // The items should be perfect E boundaries
        };
        class Material {
        public:
            std::string hfssName_;  // The name to use in HFSS
            std::set<std::string> names_;  // The FDTD names using this material
            double epsilon_;
            double mu_;
        };
        // Construction
        explicit HfssWriter(const std::string& fileName, Clipping clipping,
                            bool periodicX, bool periodicY, bool planeWaves);
        ~HfssWriter() override;

        // Overrides of ExportWriter
        void createMaterial(double epsilon, double mu, const std::string& name) override;
        void writeCuboid(Vector<double> p1, Vector<double> p2,
                         const std::string& shapeName,
                         const std::string& material) override;
        void combineStart() override;
        void combineEnd() override;
        void subtractStart() override;
        void subtractEnd() override;
        void groupStart() override;
        void groupEnd() override;
        void duplicateStart() override;
        void duplicateEnd(Vector<double> increment, size_t n) override;

    protected:
        // Data
        std::deque<Context> stack_;  // The context stack
        bool hasDomain_{};  // Set to true if domain box information has been recorded
        box::Vector<double> domainTopLeft_;  // Top left of the domain box
        box::Vector<double> domainBottomRight_;  // Bottom right of the domain box
        bool periodicX_;  // Indicates periodic X boundary
        bool periodicY_;  // Indicates periodic Y boundary
        bool planeWaves_;  // Indicates plane wave source
        std::set<std::string> names_;  // The item names used
        std::list<std::string> perfectE_;  // The items to add to a perfect E boundary
        std::list<Material> materials_;  // Material translation table
        std::list<std::string> subtractFromDomain_;  // The items to subtract from the domain

        // Helpers
        std::string makeName(const std::string& name);
        void popStack();
        void plantItems(std::list<std::string>::iterator from,
                std::list<std::string>::iterator to);
    };
}


#endif //FDTDLIFE_HFSSWRITER_H
