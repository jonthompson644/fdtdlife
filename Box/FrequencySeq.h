/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FREQUENCYSEQ_H
#define FDTDLIFE_FREQUENCYSEQ_H

#include <Xml/DomObject.h>
#include <cstdlib>

namespace box {
    // A class that defines a sequence of frequencies
    class FrequencySeq {
    public:
        // Iterator
        class Iterator {
        public:
            Iterator(const FrequencySeq* seq, size_t pos) :
                    seq_(seq),
                    pos_(pos) {}
            bool operator!=(const Iterator& other) const { return pos_ != other.pos_; }
            void operator++() { if(pos_ < seq_->n_) { pos_++; }}
            double operator*() { return seq_->spacing_ * pos_ + seq_->first_; }
        protected:
            const FrequencySeq* seq_;
            size_t pos_;
        };

        // Construction
        FrequencySeq() = default;
        FrequencySeq(double first, double spacing, size_t n);
        FrequencySeq(const FrequencySeq& other);

        // Operators
        FrequencySeq& operator=(const FrequencySeq& other) = default;
        FrequencySeq& operator|=(const FrequencySeq& other);
        bool operator==(const FrequencySeq& other) const;
        bool operator!=(const FrequencySeq& other) const { return !(*this == other); }

        // Iterator API
        [[nodiscard]] Iterator begin() const;
        [[nodiscard]] Iterator end() const;

        // API
        [[nodiscard]] size_t channel(double frequency) const;
        [[nodiscard]] double last() const;
        [[nodiscard]] double frequency(size_t channel) const;
        [[nodiscard]] size_t requiredSamples(double samplePeriod) const;
        [[nodiscard]] bool valid() const;
        [[nodiscard]] bool empty() const { return n_ == 0; }
        void clear();
        void writeConfig(xml::DomObject& o) const;
        void readConfig(xml::DomObject& o);
        friend xml::DomObject& operator<<(xml::DomObject& o, const FrequencySeq& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, FrequencySeq& v) {
            v.readConfig(o);
            return o;
        }

        // Getters
        [[nodiscard]] double first() const { return first_; }
        [[nodiscard]] double spacing() const { return spacing_; }
        [[nodiscard]] size_t n() const { return n_; }

        // Setters
        void first(double v) { first_ = v; }
        void spacing(double v) { spacing_ = v; }
        void n(size_t v) { n_ = v; }

    protected:
        // Members
        double first_{};
        double spacing_{};
        size_t n_{};
    };
}


#endif //FDTDLIFE_FREQUENCYSEQ_H
