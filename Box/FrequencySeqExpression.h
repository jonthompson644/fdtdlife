/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FREQUENCYSEQEXPRESSION_H
#define FDTDLIFE_FREQUENCYSEQEXPRESSION_H

#include "Expression.h"
#include "FrequencySeq.h"
#include <Xml/DomObject.h>

namespace box {

    class FrequencySeqExpression {
    public:
        // Construction
        FrequencySeqExpression(double first, double spacing, size_t n) :
                first_(first),
                spacing_(spacing),
                n_(n) {
        }
        FrequencySeqExpression() = default;
        
        // API
        box::FrequencySeq value() {
            return {first_.value(), spacing_.value(), static_cast<size_t>(n_.value())};
        }
        void evaluate(box::Expression::Context& c) {
            first_.evaluate(c);
            spacing_.evaluate(c);
            n_.evaluate(c);
        }
        void text(const std::string& first, const std::string& spacing, const std::string& n) {
            first_.text(first);
            spacing_.text(spacing);
            n_.text(n);
        }
        void value(const box::FrequencySeq& v) {
            first_.value(v.first());
            spacing_.value(v.spacing());
            n_.value(static_cast<double>(v.n()));
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, const FrequencySeqExpression& v) {
            o << xml::Open();
            o << xml::Obj("spacing") << v.spacing_;
            o << xml::Obj("frequency") << v.first_;
            o << xml::Obj("n") << v.n_;
            o << xml::Close();
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, FrequencySeqExpression& v) {
            o >> xml::Open();
            o >> xml::Obj("spacing") >> v.spacing_;
            o >> xml::Obj("frequency") >> v.first_;
            o >> xml::Obj("n") >> v.n_;
            o >> xml::Close();
            return o;
        }

        // Getters
        box::Expression& first() { return first_; }
        box::Expression& spacing() { return spacing_; }
        box::Expression& n() { return n_; }
        [[nodiscard]] const box::Expression& first() const { return first_; }
        [[nodiscard]] const box::Expression& spacing() const { return spacing_; }
        [[nodiscard]] const box::Expression& n() const { return n_; }

        // Setters
        void first(const box::Expression& v) { first_ = v; }
        void spacing(const box::Expression& v) { spacing_ = v; }
        void n(const box::Expression& v) { n_ = v; }

    protected:
        // Members
        box::Expression first_;
        box::Expression spacing_;
        box::Expression n_;
    };
}

#endif //FDTDLIFE_FREQUENCYSEQEXPRESSION_H
