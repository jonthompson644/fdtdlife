/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VECTOREXPRESSION_H
#define FDTDLIFE_VECTOREXPRESSION_H

#include "Expression.h"
#include "Vector.h"

namespace box {

    template<class T>
    class VectorExpression {
    public:
        // Construction
        VectorExpression(const std::string& x, const std::string& y, const std::string& z) :
                x_(x),
                y_(y),
                z_(z) {
        }
        VectorExpression(double x, double y, double z) :
                x_(x),
                y_(y),
                z_(z) {
        }
        explicit VectorExpression(const box::Vector<std::string>& v) :
                x_(v.x()),
                y_(v.y()),
                z_(v.z()) {
        }
        VectorExpression() = default;

        // API
        box::Vector<T> value() const {
            return {
                    static_cast<T>(x_.value()),
                    static_cast<T>(y_.value()),
                    static_cast<T>(z_.value())
            };
        }
        void evaluate(box::Expression::Context& c) {
            x_.evaluate(c);
            y_.evaluate(c);
            z_.evaluate(c);
        }
        void text(const std::string& x, const std::string& y, const std::string& z) {
            x_.text(x);
            y_.text(y);
            z_.text(z);
        }
        [[nodiscard]] box::Vector<std::string> text() const {
            return {x_.text(), y_.text(), z_.text()};
        }
        void value(const box::Vector<T>& v) {
            x_.value(static_cast<double>(v.x()));
            y_.value(static_cast<double>(v.y()));
            z_.value(static_cast<double>(v.z()));
        }
        static void swap(VectorExpression<T>& a, VectorExpression<T>& b) {
            Expression::swap(a.x_, b.x_);
            Expression::swap(a.y_, b.y_);
            Expression::swap(a.z_, b.z_);
        }
        bool operator==(const VectorExpression<T>& other) const {
            return x_ == other.x_ && y_ == other.y_ && z_ == other.z_;
        }

        // Getters
        box::Expression& x() { return x_; }
        box::Expression& y() { return y_; }
        box::Expression& z() { return z_; }
        [[nodiscard]] const box::Expression& x() const { return x_; }
        [[nodiscard]] const box::Expression& y() const { return y_; }
        [[nodiscard]] const box::Expression& z() const { return z_; }

        // Setters
        void x(const box::Expression& v) { x_ = v; }
        void y(const box::Expression& v) { y_ = v; }
        void z(const box::Expression& v) { z_ = v; }

    protected:
        // Members
        box::Expression x_;
        box::Expression y_;
        box::Expression z_;
    };
}


#endif //FDTDLIFE_VECTOREXPRESSION_H
