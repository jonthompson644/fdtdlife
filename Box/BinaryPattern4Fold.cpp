/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPattern4Fold.h"
#include "BinaryPattern.h"
#include <algorithm>

// Constructor
box::BinaryPattern4Fold::BinaryPattern4Fold(size_t n, uint64_t pattern) :
        n_(n),
        pattern_({pattern}),
        requiredNumBits_(0) {
    calcRequiredNumBits();
}

// Clear the pattern
void box::BinaryPattern4Fold::clear() {
    std::fill(pattern_.begin(), pattern_.end(), 0U);
}

// Converts a two fold pattern into 4 fold
box::BinaryPattern4Fold& box::BinaryPattern4Fold::operator=(const box::BinaryPattern& other) {
    n_ = other.n();
    calcRequiredNumBits();
    // The sizes
    size_t patternSize2Fold = n_ * n_;
    // The bit location table
    std::vector<uint64_t> bitLocation;
    bitLocation.resize(patternSize2Fold);
    uint64_t v = 0;
    for(size_t i = 0; i < n_; i++) {
        for(size_t j = i; j<n_; j++) {
            bitLocation[i * n_ + j] = v;
            bitLocation[j * n_ + i] = v;
            v++;
        }
    }
    // Convert to four fold
    size_t numWords = (requiredNumBits_ + bitsPerWord_ - 1) / bitsPerWord_;
    pattern_.resize(numWords);
    clear();
    for(uint64_t i = 0; i < patternSize2Fold; i++) {
        uint64_t wordNum2 = i / bitsPerWord_;
        uint64_t bitNum2 = i % bitsPerWord_;
        uint64_t wordNum4 = bitLocation[(size_t)i] / (uint64_t)bitsPerWord_;
        uint64_t bitNum4 = bitLocation[(size_t)i] % (uint64_t)bitsPerWord_;
        uint64_t bit = (other.pattern()[(size_t)wordNum2] >> bitNum2) & (uint64_t) 1;
        pattern_[(size_t)wordNum4] |= (bit << bitNum4);
    }
    return *this;
}

// Calculate the number of bits required to hold the four fold pattern
void box::BinaryPattern4Fold::calcRequiredNumBits() {
    requiredNumBits_ = 0;
    for(size_t i=0; i<n_; i++) {
        requiredNumBits_ += (i+1);
    }
}




