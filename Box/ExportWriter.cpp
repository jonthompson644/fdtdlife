/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ExportWriter.h"

// Constructor
box::ExportWriter::ExportWriter(const std::string& fileName, Clipping clipping) :
        clipping_(clipping) {
    file_.open(fileName, std::ios::out | std::ios::binary);
}

// Clip the cuboid if necessary
// Returns true if there is something to export after the clipping.
bool box::ExportWriter::clipCuboid(Vector<double>& a, Vector<double>& b) {
    bool result = true;
    if(clipping_ == Clipping::xAxis || clipping_ == Clipping::xyAxis) {
        if(a.x() < 0.0 && b.x() < 0.0) {
            // Both corners in clipping zone, ignore this cuboid
            result = false;
        } else if(a.x() < 0.0) {
            // Clip the a corner
            a.x(0.0);
            result = b.x() >= minDimension_;
        } else if(b.x() < 0.0) {
            // Clip the b corner
            b.x(0.0);
            result = a.x() >= minDimension_;
        } else {
            // Nothing to clip
        }
    }
    if(clipping_ == Clipping::yAxis || clipping_ == Clipping::xyAxis) {
        if(a.y() < 0.0 && b.y() < 0.0) {
            // Both corners in clipping zone, ignore this cuboid
            result = false;
        } else if(a.y() < 0.0) {
            // Clip the a corner
            a.y(0.0);
            result = b.y() >= minDimension_;
        } else if(b.y() < 0.0) {
            // Clip the b corner
            b.y(0.0);
            result = a.y() >= minDimension_;
        } else {
            // Nothing to clip
        }
    }
    if(clipping_ == Clipping::zAxis) {
        if(a.z() < 0.0 && b.z() < 0.0) {
            // Both corners in clipping zone, ignore this cuboid
            result = false;
        } else if(a.z() < 0.0) {
            // Clip the a corner
            a.z(0.0);
            result = b.z() >= minDimension_;
        } else if(b.z() < 0.0) {
            // Clip the b corner
            b.z(0.0);
            result = a.z() >= minDimension_;
        } else {
            // Nothing to clip
        }
    }
    return result;
}
