/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_POINT2D_H
#define FDTDLIFE_POINT2D_H

namespace box {

    // A template class for the coordinates of a point in two dimensions
    template<class T>
    class Point2D {
    public:
        // Construction
        Point2D() :
                x_(0),
                y_(0) {
        }
        Point2D(T x, T y) :
                x_(x),
                y_(y) {
        }
        Point2D(const Point2D<T>& v) :
                x_(v.x_),
                y_(v.y_) {
        }

        // Operators
        Point2D& operator=(const Point2D& v) {
            x_ = v.x_;
            y_ = v.y_;
            return *this;
        }
        Point2D operator+(const Point2D& v) const {
            return {x_ + v.x_, y_ + v.y_};
        }

        // Getters
        [[nodiscard]] T x() const { return x_; }
        [[nodiscard]] T y() const { return y_; }

        // Setters
        void x(T v) { x_ = v; }
        void y(T v) { y_ = v; }

    protected:
        // Data
        T x_;
        T y_;
    };
}


#endif //FDTDLIFE_POINT2D_H
