/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "HfssWriter.h"
#include "Utility.h"
#include <iomanip>

// Open the HFSS script file and write the header
box::HfssWriter::HfssWriter(const std::string& fileName, Clipping clipping,
                            bool periodicX, bool periodicY, bool planeWaves) :
        ExportWriter(fileName, clipping),
        periodicX_(periodicX),
        periodicY_(periodicY),
        planeWaves_(planeWaves)
{
    if(file_.is_open())
    {
        file_ << std::setprecision(10);
        file_ << R"-(import ScriptEnv)-" << std::endl;
        file_ << R"-(ScriptEnv.Initialize("Ansoft.ElectronicsDesktop"))-" << std::endl;
        file_ << R"-(oDesktop.RestoreWindow())-" << std::endl;
        file_ << R"-(oProject = oDesktop.GetActiveProject())-" << std::endl;
        file_ << R"-(oDefMgr = oProject.GetDefinitionManager())-" << std::endl;
        file_ << R"-(oDesign = oProject.GetActiveDesign())-" << std::endl;
        file_ << R"-(oEditor = oDesign.SetActiveEditor("3D Modeler"))-" << std::endl;
        file_ << R"-(oBoundary = oDesign.GetModule("BoundarySetup"))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def createBox(name, x, y, z, dx, dy, dz, material):)-" << std::endl;
        file_ << R"-(    if oDefMgr.DoesMaterialExist(material):)-" << std::endl;
        file_ << R"-(        mat = material)-" << std::endl;
        file_ << R"-(    else:)-" << std::endl;
        file_ << R"-(        mat = "copper")-" << std::endl;
        file_ << R"-(    oEditor.CreateBox()-" << std::endl;
        file_ << R"-(        [ "NAME:BoxParameters",)-" << std::endl;
        file_ << R"-(        "XPosition:=", "%smm" % x,)-" << std::endl;
        file_ << R"-(        "YPosition:=", "%smm" % y,)-" << std::endl;
        file_ << R"-(        "ZPosition:=", "%smm" % z,)-" << std::endl;
        file_ << R"-(        "XSize:=", "%smm" % dx,)-" << std::endl;
        file_ << R"-(        "YSize:=", "%smm" % dy,)-" << std::endl;
        file_ << R"-(        "ZSize:=", "%smm" % dz],)-" << std::endl;
        file_ << R"-(        [ "NAME:Attributes",)-" << std::endl;
        file_ << R"-(        "Name:=", "%s" % name,)-" << std::endl;
        file_ << R"-(        "Flags:=", "",)-" << std::endl;
        file_ << R"-(        "Color:=", "(143 175 143)",)-" << std::endl;
        file_ << R"-(        "Transparency:=", "0",)-" << std::endl;
        file_ << R"-(        "PartCoordinateSystem:=", "Global",)-" << std::endl;
        file_ << R"-(        "UDMId:=", "",)-" << std::endl;
        file_ << R"-(        "MaterialValue:=", "\"%s\"" % mat,)-" << std::endl;
        file_ << R"-(        "SurfaceMaterialValue:=", "\"\"",)-" << std::endl;
        file_ << R"-(        "SolveInside:=", True,)-" << std::endl;
        file_ << R"-(        "IsMaterialEditable:=", True,)-" << std::endl;
        file_ << R"-(        "UseMaterialAppearance:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def createGroup(items):)-" << std::endl;
        file_ << R"-(    oEditor.CreateGroup()-" << std::endl;
        file_ << R"-(        [ "NAME:GroupParameters",)-" << std::endl;
        file_ << R"-(        "ParentGroupID:=", "Model",)-" << std::endl;
        file_ << R"-(        "Parts:=", "%s" % ",".join(items),)-" << std::endl;
        file_ << R"-(        "SubmodelInstances:=", "",)-" << std::endl;
        file_ << R"-(        "Groups:=", ""]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def createRect(name, x, y, z, dx, dy):)-" << std::endl;
        file_ << R"-(    oEditor.CreateRectangle()-" << std::endl;
        file_ << R"-(        ["NAME:RectangleParameters",)-" << std::endl;
        file_ << R"-(        "IsCovered:=", True,)-" << std::endl;
        file_ << R"-(        "XStart:=", "%smm" % x,)-" << std::endl;
        file_ << R"-(        "YStart:=", "%smm" % y,)-" << std::endl;
        file_ << R"-(        "ZStart:=", "%smm" % z,)-" << std::endl;
        file_ << R"-(        "Width:=", "%smm" % dx,)-" << std::endl;
        file_ << R"-(        "Height:=", "%smm" % dy,)-" << std::endl;
        file_ << R"-(        "WhichAxis:=", "Z"],)-" << std::endl;
        file_ << R"-(        ["NAME:Attributes",)-" << std::endl;
        file_ << R"-(        "Name:=", "%s" % name,)-" << std::endl;
        file_ << R"-(        "Flags:=", "",)-" << std::endl;
        file_ << R"-(        "Color:=", "(143 175 143)",)-" << std::endl;
        file_ << R"-(        "Transparency:=", 0,)-" << std::endl;
        file_ << R"-(        "PartCoordinateSystem:=", "Global",)-" << std::endl;
        file_ << R"-(        "UDMId:=", "",)-" << std::endl;
        file_ << R"-(        "MaterialValue:=", "\"vacuum\"",)-" << std::endl;
        file_ << R"-(        "SurfaceMaterialValue:=", "\"\"",)-" << std::endl;
        file_ << R"-(        "SolveInside:=", True,)-" << std::endl;
        file_ << R"-(        "IsMaterialEditable:=", True,)-" << std::endl;
        file_ << R"-(        "UseMaterialAppearance:=", False,)-" << std::endl;
        file_ << R"-(        "IsLightweight:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def combine(shapes):)-" << std::endl;
        file_ << R"-(    oEditor.Unite()-" << std::endl;
        file_ << R"-(        ["NAME:Selections",)-" << std::endl;
        file_ << R"-(        "Selections:=", "%s" % ",".join(shapes)],)-" << std::endl;
        file_ << R"-(        ["NAME:UniteParameters",)-" << std::endl;
        file_ << R"-(        "KeepOriginals:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def subtract(parent, children):)-" << std::endl;
        file_ << R"-(    oEditor.Subtract()-" << std::endl;
        file_ << R"-(        ["NAME:Selections",)-" << std::endl;
        file_ << R"-(        "Blank Parts:=", "%s" % parent,)-" << std::endl;
        file_ << R"-(        "Tool Parts:=", "%s" % ",".join(children)],)-" << std::endl;
        file_ << R"-(        ["NAME:SubtractParameters",)-" << std::endl;
        file_ << R"-(        "KeepOriginals:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def GetFaceByPos(name, x, y, z):)-" << std::endl;
        file_ << R"-(    return oEditor.GetFaceByPosition()-" << std::endl;
        file_ << R"-(        ["NAME:FaceParameters",)-" << std::endl;
        file_ << R"-(        "BodyName:=", "%s" % name,)-" << std::endl;
        file_ << R"-(        "XPosition:=", "%smm" % x,)-" << std::endl;
        file_ << R"-(        "YPosition:=", "%smm" % y,)-" << std::endl;
        file_ << R"-(        "ZPosition:=", "%smm" % z]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def assignFloquetPort(name, item, x1, y1, x2, y2, z):)-" << std::endl;
        file_ << R"-(    face = GetFaceByPos(item, (x1+x2)/2.0, (y1+y2)/2.0, z))-"
              << std::endl;
        file_ << R"-(    oBoundary.AssignFloquetPort()-" << std::endl;
        file_ << R"-(        ["NAME:%s" % name,)-" << std::endl;
        file_ << R"-(        "Faces:=", [face],)-" << std::endl;
        file_ << R"-(        "NumModes:=", 2,)-" << std::endl;
        file_ << R"-(        "DoDeembed:=", False,)-" << std::endl;
        file_ << R"-(        "RenormalizeAllTerminals:=", True,)-" << std::endl;
        file_ << R"-(        ["NAME:Modes",)-" << std::endl;
        file_ << R"-(            ["NAME:Mode1",)-" << std::endl;
        file_ << R"-(                "ModeNum:=", 1,)-" << std::endl;
        file_ << R"-(                "UseIntLine:=", False,)-" << std::endl;
        file_ << R"-(                "CharImp:=", "Zpi"],)-" << std::endl;
        file_ << R"-(            ["NAME:Mode2",)-" << std::endl;
        file_ << R"-(                "ModeNum:=", 2,)-" << std::endl;
        file_ << R"-(                "UseIntLine:=", False,)-" << std::endl;
        file_ << R"-(                "CharImp:=", "Zpi"]],)-" << std::endl;
        file_ << R"-(        "ShowReporterFilter:=", False,)-" << std::endl;
        file_ << R"-(        "ReporterFilter:=", [False,False],)-" << std::endl;
        file_ << R"-(        "UseScanAngles:=", True,)-" << std::endl;
        file_ << R"-(        "Phi:=", "0deg",)-" << std::endl;
        file_ << R"-(        "Theta:=", "0deg",)-" << std::endl;
        file_ << R"-(            ["NAME:LatticeAVector",)-" << std::endl;
        file_ << R"-(                "Start:=", ["%smm" % x1,"%smm" % y1,"%smm" % z],)-"
              << std::endl;
        file_ << R"-(                "End:=", ["%smm" % x2,"%smm" % y1,"%smm" % z]],)-"
              << std::endl;
        file_ << R"-(            ["NAME:LatticeBVector",)-" << std::endl;
        file_ << R"-(                "Start:=", ["%smm" % x1,"%smm" % y1,"%smm" % z],)-"
              << std::endl;
        file_ << R"-(                "End:=", ["%smm" % x1,"%smm" % y2,"%smm" % z]],)-"
              << std::endl;
        file_ << R"-(            ["NAME:ModesList",)-" << std::endl;
        file_ << R"-(                ["NAME:Mode",)-" << std::endl;
        file_ << R"-(                    "ModeNumber:=", 1,)-" << std::endl;
        file_ << R"-(                    "IndexM:=", 0,)-" << std::endl;
        file_ << R"-(                    "IndexN:=", 0,)-" << std::endl;
        file_ << R"-(                    "KC2:=", 0,)-" << std::endl;
        file_ << R"-(                    "PropagationState:=", "Propagating",)-" << std::endl;
        file_ << R"-(                    "Attenuation:=", 0,)-" << std::endl;
        file_ << R"-(                    "PolarizationState:=", "TE",)-" << std::endl;
        file_ << R"-(                    "AffectsRefinement:=", True],)-" << std::endl;
        file_ << R"-(                ["NAME:Mode",)-" << std::endl;
        file_ << R"-(                    "ModeNumber:=", 2,)-" << std::endl;
        file_ << R"-(                    "IndexM:=", 0,)-" << std::endl;
        file_ << R"-(                    "IndexN:=", 0,)-" << std::endl;
        file_ << R"-(                    "KC2:=", 0,)-" << std::endl;
        file_ << R"-(                    "PropagationState:=", "Propagating",)-" << std::endl;
        file_ << R"-(                    "Attenuation:=", 0,)-" << std::endl;
        file_ << R"-(                    "PolarizationState:=", "TM",)-" << std::endl;
        file_ << R"-(                    "AffectsRefinement:=", True]]]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def splitAtPlane(items, negativeOnly, toolItem, x, y, z):)-" << std::endl;
        file_ << R"-(    whichSide = "PositiveOnly")-" << std::endl;
        file_ << R"-(    if negativeOnly:)-" << std::endl;
        file_ << R"-(        whichSide = "NegativeOnly")-" << std::endl;
        file_ << R"-(    face = GetFaceByPos(toolItem, x, y, z))-" << std::endl;
        file_ << R"-(    oEditor.Split()-" << std::endl;
        file_ << R"-(        ["NAME:Selections",)-" << std::endl;
        file_ << R"-(            "Selections:=", "%s" % ",".join(items),)-" << std::endl;
        file_ << R"-(            "NewPartsModelFlag:=", "Model",)-" << std::endl;
        file_ << R"-(            "ToolPart:=", "%s" % toolItem],)-" << std::endl;
        file_ << R"-(        ["NAME:SplitToParameters",)-" << std::endl;
        file_ << R"-(            "SplitPlane:=", "Dummy",)-" << std::endl;
        file_ << R"-(            "WhichSide:=", "%s" % whichSide,)-" << std::endl;
        file_ << R"-(            "ToolType:=", "FaceTool",)-" << std::endl;
        file_ << R"-(            "ToolEntityID:=", face,)-" << std::endl;
        file_ << R"-(            "SplitCrossingObjectsOnly:=", True,)-" << std::endl;
        file_ << R"-(            "DeleteInvalidObjects:=", True]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def duplicate(items, dx, dy, dz, n):)-" << std::endl;
        file_ << R"-(    oEditor.DuplicateAlongLine()-" << std::endl;
        file_ << R"-(        ["NAME:Selections",)-" << std::endl;
        file_ << R"-(            "Selections:=", "%s" % ",".join(items),)-" << std::endl;
        file_ << R"-(            "NewPartsModelFlag:=", "Model"],)-" << std::endl;
        file_ << R"-(        ["NAME:DuplicateToAlongLineParameters",)-" << std::endl;
        file_ << R"-(            "CreateNewObjects:=", True,)-" << std::endl;
        file_ << R"-(            "XComponent:=", "%smm" % dx,)-" << std::endl;
        file_ << R"-(            "YComponent:=", "%smm" % dy,)-" << std::endl;
        file_ << R"-(            "ZComponent:=", "%smm" % dz,)-" << std::endl;
        file_ << R"-(            "NumClones:=", "%s" % n],)-" << std::endl;
        file_ << R"-(        ["NAME:Options",)-" << std::endl;
        file_ << R"-(            "DuplicateAssignments:=", False],)-" << std::endl;
        file_ << R"-(        ["CreateGroupsForNewObjects:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def assignPerfectE(name, items):)-" << std::endl;
        file_ << R"-(    oBoundary.AssignPerfectE()-" << std::endl;
        file_ << R"-(        ["NAME:%s" % name,)-" << std::endl;
        file_ << R"-(            "Objects:=", items,)-" << std::endl;
        file_ << R"-(            "InfGroundPlane:=", False]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def assignMaster(name, item, x1, y1, z1, x2, y2, z2, reverseV):)-"
              << std::endl;
        file_ << R"-(    face = GetFaceByPos(item, (x1+x2)/2.0, (y1+y2)/2.0, (z1+z2)/2.0))-"
              << std::endl;
        file_ << R"-(    oBoundary.AssignMaster()-" << std::endl;
        file_ << R"-(        ["NAME:%s" % name,)-" << std::endl;
        file_ << R"-(            "Faces:=", [face],)-" << std::endl;
        file_ << R"-(            ["NAME:CoordSysVector",)-" << std::endl;
        file_ << R"-(                "Origin:=", ["%smm" % x1, "%smm" % y1, "%smm" %z1],)-"
              << std::endl;
        file_ << R"-(                "UPos:=", ["%smm" % x2, "%smm" % y2, "%smm" %z1]],)-"
              << std::endl;
        file_ << R"-(            "ReverseV:=", reverseV]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def assignSlave(name, master, item, x1, y1, z1, x2, y2, z2, reverseV):)-"
              << std::endl;
        file_ << R"-(    face = GetFaceByPos(item, (x1+x2)/2.0, (y1+y2)/2.0, (z1+z2)/2.0))-"
              << std::endl;
        file_ << R"-(    oBoundary.AssignSlave()-" << std::endl;
        file_ << R"-(        ["NAME:%s" % name,)-" << std::endl;
        file_ << R"-(            "Faces:=", [face],)-" << std::endl;
        file_ << R"-(            ["NAME:CoordSysVector",)-" << std::endl;
        file_ << R"-(                "Origin:=", ["%smm" % x1, "%smm" % y1, "%smm" %z1],)-"
              << std::endl;
        file_ << R"-(                "UPos:=", ["%smm" % x2, "%smm" % y2, "%smm" %z1]],)-"
              << std::endl;
        file_ << R"-(            "ReverseV:=", reverseV,)-" << std::endl;
        file_ << R"-(            "Master:=", master,)-" << std::endl;
        file_ << R"-(            "UseScanAngles:=", True,)-" << std::endl;
        file_ << R"-(            "Phi:=", "0deg",)-" << std::endl;
        file_ << R"-(            "Theta:=", "0deg"]))-" << std::endl;
        file_ << std::endl;

        file_ << R"-(def addMaterial(name, epsilon, mu):)-" << std::endl;
        file_ << R"-(    oDefMgr.AddMaterial()-" << std::endl;
        file_ << R"-(        ["NAME:%s" % name,)-" << std::endl;
        file_ << R"-(            "CoordinateSystemType:=", "Cartesian",)-" << std::endl;
        file_ << R"-(            "BulkOrSurfaceType:=", 1,)-" << std::endl;
        file_ << R"-(            ["NAME:PhysicsTypes",)-" << std::endl;
        file_ << R"-(                "set:=", ["Electromagnetic"]],)-" << std::endl;
        file_ << R"-(            "permittivity:=", "%s" % epsilon,)-" << std::endl;
        file_ << R"-(            "permeability:=", "%s" % mu]))-" << std::endl;
        file_ << std::endl;
    }

    // Initialise the stack
    stack_.emplace_back(State::normal);
}

// Destructor.
box::HfssWriter::~HfssWriter()
{
    if(file_.is_open())
    {
        // Is the stack empty?
        if(stack_.back().state_ != State::normal)
        {
            std::cout << "HFSS export stack not empty" << std::endl;
        }
        // Do extra stuff if we have a domain box
        // Clip to the domain sides
        if(hasDomain_)
        {
            // A list that selects all the objects except the domain
            file_ << "select = [";
            bool first = true;
            for(auto& s : stack_.back().items_)
            {
                if(s != "Domain")
                {
                    if(!first)
                    {
                        file_ << ",";
                    }
                    first = false;
                    file_ << "\"" << s << "\"";
                }
            }
            file_ << "]" << std::endl;
            // Clip the objects to the domain box
            file_ << "splitAtPlane(select,True,\"Domain\","
                  << (domainTopLeft_.x() + domainBottomRight_.x()) / 2 << ","
                  << domainBottomRight_.y() << ","
                  << (domainTopLeft_.z() + domainBottomRight_.z()) / 2 << ")" << std::endl;
            file_ << "splitAtPlane(select,True,\"Domain\","
                  << (domainTopLeft_.x() + domainBottomRight_.x()) / 2 << ","
                  << domainTopLeft_.y() << ","
                  << (domainTopLeft_.z() + domainBottomRight_.z()) / 2 << ")" << std::endl;
            file_ << "splitAtPlane(select,False,\"Domain\","
                  << domainBottomRight_.x() << ","
                  << (domainTopLeft_.y() + domainBottomRight_.y()) / 2 << ","
                  << (domainTopLeft_.z() + domainBottomRight_.z()) / 2 << ")" << std::endl;
            file_ << "splitAtPlane(select,False,\"Domain\","
                  << domainTopLeft_.x() << ","
                  << (domainTopLeft_.y() + domainBottomRight_.y()) / 2 << ","
                  << (domainTopLeft_.z() + domainBottomRight_.z()) / 2 << ")" << std::endl;
            // Assign periodic boundaries
            if(periodicX_)
            {
                file_ << R"-(assignMaster("Master1","Domain",)-"
                      << domainBottomRight_.x() << "," << domainTopLeft_.y() << ","
                      << domainTopLeft_.z() << ","
                      << domainBottomRight_.x() << "," << domainBottomRight_.y() << ","
                      << domainBottomRight_.z() << ",True)" << std::endl;
                file_ << R"-(assignSlave("Slave1","Master1","Domain",)-"
                      << domainTopLeft_.x() << "," << domainTopLeft_.y() << ","
                      << domainTopLeft_.z() << ","
                      << domainTopLeft_.x() << "," << domainBottomRight_.y() << ","
                      << domainBottomRight_.z() << ",False)" << std::endl;
            }
            if(periodicY_)
            {
                file_ << R"-(assignMaster("Master2","Domain",)-"
                      << domainTopLeft_.x() << "," << domainBottomRight_.y() << ","
                      << domainTopLeft_.z() << ","
                      << domainBottomRight_.x() << "," << domainBottomRight_.y() << ","
                      << domainBottomRight_.z() << ",False)" << std::endl;
                file_ << R"-(assignSlave("Slave2","Master2","Domain",)-"
                      << domainTopLeft_.x() << "," << domainTopLeft_.y() << ","
                      << domainTopLeft_.z() << ","
                      << domainBottomRight_.x() << "," << domainTopLeft_.y() << ","
                      << domainBottomRight_.z() << ",True)" << std::endl;
            }
            // Create Floquet ports
            if(periodicX_ && periodicY_ && planeWaves_)
            {
                file_ << R"-(assignFloquetPort("Floquet1","Domain",)-"
                      << domainTopLeft_.x() << "," << domainTopLeft_.y() << ","
                      << domainBottomRight_.x() << "," << domainBottomRight_.y() << ","
                      << domainTopLeft_.z() << ")" << std::endl;
                file_ << R"-(assignFloquetPort("Floquet2","Domain",)-"
                      << domainTopLeft_.x() << "," << domainTopLeft_.y() << ","
                      << domainBottomRight_.x() << "," << domainBottomRight_.y() << ","
                      << domainBottomRight_.z() << ")" << std::endl;
            }
        }
        // Create any perfect E boundary conditions
        if(!perfectE_.empty())
        {
            file_ << "select = [";
            plantItems(perfectE_.begin(), perfectE_.end());
            file_ << "]" << std::endl;
            file_ << "assignPerfectE(\"" << stack_.back().items_.front() << "\",select)"
                  << std::endl;
        }
    }
}

// Make the name of an item
std::string box::HfssWriter::makeName(const std::string& name)
{
    std::stringstream result;
    result << name;
    if(names_.count(name) > 0)
    {
        for(size_t i = 0; i < (stack_.size() - 1); ++i)
        {
            result << "__" << stack_[i].items_.size();
        }
        result << "__" << stack_.back().items_.size() + 1;
    } else
    {
        names_.insert(name);
    }
    return result.str();
}

// Write a cuboid to the file
void box::HfssWriter::writeCuboid(Vector<double> p1, Vector<double> p2,
                                  const std::string& shapeName,
                                  const std::string& material)
{
    if(file_.is_open())
    {
        // Enforce any clipping
        if(clipCuboid(p1, p2))
        {
            // Center and size
            Vector<double> size = (p2 - p1);
            // Box or rectangle?
            if(stack_.back().state_ != State::normal)
            {
                stack_.back().perfectE_ = stack_.back().perfectE_ ||
                                          (size.z() <= 0.0015 && stack_.back().items_.empty());
            }
            // The name
            std::string itemName = makeName(shapeName);
            stack_.back().items_.push_back(itemName);
            // Draw the item
            if(stack_.back().perfectE_)
            {
                // For things that are very thin in the z direction use a rectangle
                file_ << "createRect(\"" << itemName << "\","
                      << p1.x() << "," << p1.y() << "," << p1.z() << ","
                      << size.x() << "," << size.y() << ")" << std::endl;
            } else
            {
                // Otherwise plant a cuboid
                std::string matName = material;
                auto matPos = std::find_if(materials_.begin(), materials_.end(),
                                           [material](const Material& m)
                                           {
                                               return m.names_.count(material) > 0;
                                           });
                if(matPos != materials_.end())
                {
                    matName = matPos->hfssName_;
                }
                file_ << "createBox(\"" << itemName << "\","
                      << p1.x() << "," << p1.y() << "," << p1.z() << ","
                      << size.x() << "," << size.y() << "," << size.z() << ",\""
                      << matName << "\")" << std::endl;
            }
            // Is this the domain box?
            if(stack_.back().state_ == State::normal && shapeName == "Domain"
               && material == "Background")
            {
                hasDomain_ = true;
                domainTopLeft_ = p1;
                domainBottomRight_ = p2;
            }
        }
    }
}

// Shapes from now until the combine end or completion are
// to be combined into a single entity
void box::HfssWriter::combineStart()
{
    stack_.emplace_back(State::combining);
}

// Combine the shapes from the previous combine start into a single entity
void box::HfssWriter::combineEnd()
{
    if(stack_.back().state_ == State::combining)
    {
        if(stack_.back().items_.size() > 1)
        {
            // Plant the combine
            file_ << "combine([";
            bool first = true;
            for(auto& name : stack_.back().items_)
            {
                if(!first)
                {
                    file_ << ",";
                }
                file_ << "\"" << name << "\"";
                first = false;
            }
            file_ << "])" << std::endl;
        }
        popStack();
    }
}

// Shapes from now until subtract end are to be 
// subtracted into a single entity
void box::HfssWriter::subtractStart()
{
    stack_.emplace_back(State::subtracting);
}

// Complete a subtraction
void box::HfssWriter::subtractEnd()
{
    if(stack_.back().state_ == State::subtracting)
    {
        if(stack_.back().items_.size() > 1)
        {
            // Plant the subtract
            file_ << "subtract(";
            bool first = true;
            bool second = true;
            for(auto& name : stack_.back().items_)
            {
                if(first)
                {
                    file_ << "\"" << name << "\",[";
                    first = false;
                } else
                {
                    if(!second)
                    {
                        file_ << ",";
                    }
                    file_ << "\"" << name << "\"";
                    second = false;
                }
            }
            file_ << "])" << std::endl;
        }
        popStack();
    }
}

// Shapes from now until group end are formed into a group
void box::HfssWriter::groupStart()
{
    stack_.emplace_back(State::grouping);
}

// For the items into a group
void box::HfssWriter::groupEnd()
{
    if(stack_.back().state_ == State::grouping)
    {
        if(stack_.back().items_.size() == 1)
        {
            // Assign to a perfect E boundary?
            if(stack_.back().perfectE_)
            {
                perfectE_.push_back(stack_.back().items_.front());
            }
        } else if(!stack_.back().items_.empty())
        {
            // A list that selects the objects for the group
            file_ << "select = [";
            plantItems(stack_.back().items_.begin(), stack_.back().items_.end());
            file_ << "]" << std::endl;
            // Create the group
            file_ << "createGroup(select)" << std::endl;
            // Assign to a perfect E boundary?
            if(stack_.back().perfectE_)
            {
                for(auto& item : stack_.back().items_)
                {
                    perfectE_.push_back(item);
                }
            }
        }
        popStack();
    }
}

// Pop the context stack
void box::HfssWriter::popStack()
{
    bool nothingToReturn = stack_.back().items_.empty();
    std::string name = stack_.back().items_.front();
    bool perfectE = stack_.back().perfectE_;
    stack_.pop_back();
    if(!nothingToReturn)
    {
        stack_.back().items_.push_back(name);
        if(stack_.back().state_ != State::normal)
        {
            stack_.back().perfectE_ = perfectE;
        }
    }
}

// Start a duplicating block
void box::HfssWriter::duplicateStart()
{
    stack_.emplace_back(State::duplicating);
}

// Duplicate the items along the vector indicated
void box::HfssWriter::duplicateEnd(box::Vector<double> increment, size_t n)
{
    if(stack_.back().state_ == State::duplicating)
    {
        if(!stack_.back().items_.empty())
        {
            // Plant the duplicate
            file_ << "duplicate([";
            plantItems(stack_.back().items_.begin(), stack_.back().items_.end());
            file_ << "]," << increment.x() << "," << increment.y()
                  << "," << increment.z() << "," << n << ")" << std::endl;
        }
        // Pop the stack and add all the items to the returned list
        // plus all the duplicated items
        auto items = stack_.back().items_;
        bool perfectE = stack_.back().perfectE_;
        stack_.pop_back();
        for(auto& item : items)
        {
            stack_.back().items_.push_back(item);
            for(size_t i = 1; i < n; i++)
            {
                std::stringstream str;
                str << item << "_" << i;
                stack_.back().items_.push_back(str.str());
            }
        }
        if(stack_.back().state_ != State::normal)
        {
            stack_.back().perfectE_ = perfectE;
        }
    }
}

// Plant the indicated list of items
void box::HfssWriter::plantItems(std::list<std::string>::iterator from,
                                 std::list<std::string>::iterator to)
{
    bool first = true;
    int numOnLine = 0;
    for(auto pos = from; pos != to; ++pos)
    {
        if(numOnLine >= 5)
        {
            file_ << std::endl << "    ";
            numOnLine = 0;
        }
        if(!first)
        {
            file_ << ",";
        }
        std::string item = *pos;
        file_ << "\"" << item << "\"";
        first = false;
        numOnLine++;
    }
}

// Create a material with the specified properties
void box::HfssWriter::createMaterial(double epsilon, double mu, const std::string& name)
{
    // Find the FDTD material in the table
    auto pos = std::find_if(materials_.begin(), materials_.end(),
                            [name](const Material& m)
                            {
                                return m.names_.count(name) > 0;
                            });
    if(pos == materials_.end())
    {
        // Not in the table, can we use one of the others?
        pos = std::find_if(materials_.begin(), materials_.end(),
                           [epsilon, mu](const Material& m)
                           {
                               return box::Utility::equals(epsilon, m.epsilon_) &&
                                      box::Utility::equals(mu, m.mu_);
                           });
        if(pos == materials_.end())
        {
            // No, create another one
            pos = materials_.emplace(materials_.end());
            pos->hfssName_ = "FdtdMaterial" + std::to_string(materials_.size());
            pos->epsilon_ = epsilon;
            pos->mu_ = mu;
            file_ << "addMaterial(\"" << pos->hfssName_ << "\","
                  << pos->epsilon_ << "," << pos->mu_ << ")" << std::endl;
        }
        pos->names_.emplace(name);
    }
}
