/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ReferencedComponent.h"
#include <Box/GeortzelSpectrum.h>
#include <Box/Constants.h>

// Constructor
sensor::ReferencedComponent::ReferencedComponent() {
}

// Initialise the component ready for a run
void sensor::ReferencedComponent::initialise(size_t numChannels) {
    admittance_.resize(numChannels);
    transmittance_.resize(numChannels);
    phaseShift_.resize(numChannels);
}

// Calculate the spectrum information from data and reference spectra
void sensor::ReferencedComponent::calculate(const box::GeortzelSpectrum& data,
                                            const box::GeortzelSpectrum& reference) {
    // Perform the calculation for each channel
    for(size_t i = 0; i < admittance_.size(); i++) {
        calculate(i, data[i], reference[i]);
    }
}

// Calculate the spectrum information for one channel
void sensor::ReferencedComponent::calculate(size_t channel, std::complex<double> data,
                                            std::complex<double> reference) {
    if(std::abs(reference) > 0) {
        double ratio = std::abs(data) / std::abs(reference);
        double phase = std::arg(data) - std::arg(reference);
        if(phase > box::Constants::pi_) {
            phase -= (2.0 * box::Constants::pi_);
        }
        if(phase < -box::Constants::pi_) {
            phase += (2.0 * box::Constants::pi_);
        }
        double tanp = std::tan(phase);
        double den = ratio * std::sqrt(1 + tanp * tanp);
        transmittance_[channel] = ratio * ratio;
        phaseShift_[channel] = phase / box::Constants::deg2rad_;
        admittance_[channel] = {2.0 / den - 1.0, -2.0 * tanp / den};
    } else {
        transmittance_[channel] = 0.0;
        phaseShift_[channel] = 0.0;
        admittance_[channel] = 0.0;
    }
}

// Calculate the range information for the arrays
void sensor::ReferencedComponent::evaluateRanges() {
    admittanceRange_.evaluate(admittance_);
    transmittanceRange_.evaluate(transmittance_);
    phaseShiftRange_.evaluate(phaseShift_);
}

// Set the transmittance and phase shift calculated elsewhere
void sensor::ReferencedComponent::set(size_t channel, double transmittance, double phaseShift,
                                      const std::complex<double>& admittance) {
    if(channel < transmittance_.size()) {
        transmittance_[channel] = transmittance;
    }
    if(channel < phaseShift_.size()) {
        phaseShift_[channel] = phaseShift;
    }
    if(channel < admittance_.size()) {
        admittance_[channel] = admittance;
    }
}


