/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ArraySensor.h"
#include <Fdtd/Model.h>
#include <Source/PlaneWaveSource.h>
#include <Box/BinaryFileHeader.h>
#include <Xml/DomObject.h>
#include <Fdtd/EField.h>
#include <algorithm>

// Constructor
sensor::ArraySensor::ArraySensor() {
    attenuation2DSpec_ = new DataSpec(this, "2DAttenuation",
                                      DataSpec::modeSensorArray, {""});
    phaseShift2DSpec_ = new DataSpec(this, "2DPhaseShift",
                                     DataSpec::modeSensorArray, {""});
    attenuationFront_ = new DataSpec(this, "FrontAttenuation",
                                     DataSpec::modeAttenuation,
                                     {"X", "PM", "Y", "Z"});
    phaseShiftFront_ = new DataSpec(this, "FrontPhaseShift",
                                    DataSpec::modePhaseShift,
                                    {"X", "PM", "Y", "Z"});
    attenuationBack_ = new DataSpec(this, "BackAttenuation",
                                    DataSpec::modeAttenuation,
                                    {"X", "PM", "Y", "Z"});
    phaseShiftBack_ = new DataSpec(this, "BackPhaseShift",
                                   DataSpec::modePhaseShift,
                                   {"X", "PM", "Y", "Z"});
    timeSeriesFront_ = new DataSpec(this, "FrontTimeSeries",
                                    DataSpec::modeTimeSeries,
                                    {"Signal", "Reference"});
    timeSeriesBack_ = new DataSpec(this, "BackTimeSeries",
                                   DataSpec::modeTimeSeries,
                                   {"Signal", "Reference"});
    polarisation_ = new DataSpec(this, "Polarisation",
                                 DataSpec::modePhaseShift,
                                 {"Transmitted", "Reflected"});
    skew_ = new DataSpec(this, "Skew",
                         DataSpec::modeTimeSeries,
                         {"Transmitted", "Reflected"});
    admittance_ = new DataSpec(this, "Admittance",
                               DataSpec::modeTimeSeries,
                               {"Real", "Imag"});
    amplitudeDistribution_ = new DataSpec(this, "AmplitudeDistribution",
                                          DataSpec::modeTimeSeries,
                                          {"Transmitted", "Reflected"});
    phaseDistribution_ = new DataSpec(this, "PhaseDistribution",
                                      DataSpec::modeTimeSeries,
                                      {"Transmitted", "Reflected"});
    specs_.push_back(attenuation2DSpec_);
    specs_.push_back(phaseShift2DSpec_);
    specs_.push_back(attenuationFront_);
    specs_.push_back(phaseShiftFront_);
    specs_.push_back(attenuationBack_);
    specs_.push_back(phaseShiftBack_);
    specs_.push_back(timeSeriesFront_);
    specs_.push_back(timeSeriesBack_);
    specs_.push_back(polarisation_);
    specs_.push_back(skew_);
    specs_.push_back(admittance_);
    specs_.push_back(amplitudeDistribution_);
    specs_.push_back(phaseDistribution_);
}

// Destructor
sensor::ArraySensor::~ArraySensor() {
    delete attenuation2DSpec_;
    delete phaseShift2DSpec_;
    delete attenuationFront_;
    delete phaseShiftFront_;
    delete attenuationBack_;
    delete phaseShiftBack_;
    delete timeSeriesBack_;
    delete timeSeriesFront_;
    delete polarisation_;
    delete skew_;
    delete admittance_;
    delete phaseDistribution_;
    delete amplitudeDistribution_;
}

// Initialise the sensorId ready for a run
void sensor::ArraySensor::initialise() {
    fdtdUsed_ = false;
    propagationMatrixUsed_ = false;
    // Clear any existing sensorId points
    points_.clear();
    // Record the Z coordinates of the reflected and transmitted sensors
    if(!manualZPos_) {
        transmittedPosZ_ = m_->p()->p2().z().value() - m_->p()->dr().z().value();
        reflectedPosZ_ = m_->p()->p1().z().value()
                         - m_->p()->dr().z().value() * m_->p()->scatteredFieldZoneSize() / 2;
    }
    reflectedZCell_ = m_->p()->geometry()->cellFloor(
            box::Vector<double>(0, 0, reflectedPosZ_)).z();
    auto zeroZ = m_->p()->geometry()->cellFloor(box::Vector<double>(0.0)).z();
    reflectedReferenceZCell_ = zeroZ + (zeroZ - reflectedZCell_);
    transmittedZCell_ = m_->p()->geometry()->cellFloor(
            box::Vector<double>(0, 0, transmittedPosZ_)).z();
    // Get the source frequency information
    frequencies_ = m_->sources().frequencyInfo();
    calculateChannelNumber();
    // Work out the length of the time series, we need space for one whole
    // cycle of the frequency spacing (not the start frequency otherwise
    // we may not have enough resolution for the frequency steps).
    if(!manualSamples_) {
        nSamples_ = frequencies_.requiredSamples(m_->p()->dt());
    }
    nSamples_ = std::max(nSamples_, 1UL);
    // Calculate where the sensorId points will be
    box::Vector<double> domainSize = m_->p()->size();
    spacingX_ = domainSize.x() / numPointsX_;
    spacingY_ = domainSize.y() / numPointsY_;
    box::Vector<double> startPos = m_->p()->p1().value();
    box::Vector<double> halfSpacing(spacingX_ / 2.0, spacingY_ / 2.0, 0.0);
    box::Vector<double> halfDr(m_->p()->dr().x().value() / 2,
                               m_->p()->dr().y().value() / 2, 0.0);
    startPos += halfSpacing;
    // Create a sensorId point for each desired position
    for(size_t nx = 0; nx < numPointsX_; nx++) {
        for(size_t ny = 0; ny < numPointsY_; ny++) {
            box::Vector<double> pos = startPos + box::Vector<double>(
                    nx * spacingX_, ny * spacingY_, 0.0);
            box::Vector<double> topLeftPos = pos;
            box::Vector<double> bottomRightPos = pos;
            if(averageOverCell_) {
                topLeftPos = topLeftPos - halfSpacing;
                bottomRightPos = bottomRightPos + halfSpacing;
            } else {
                topLeftPos = topLeftPos - halfDr;
                bottomRightPos = bottomRightPos + halfDr;
            }
            auto topLeftCell = m_->p()->geometry()->cellRound(topLeftPos);
            auto bottomRightCell = m_->p()->geometry()->cellRound(bottomRightPos);
            points_.emplace_back(ArraySensorPoint(this,
                                                  {topLeftCell.x(),
                                                   topLeftCell.y(),
                                                   bottomRightCell.x(),
                                                   bottomRightCell.y()}, pos));
        }
    }
}

// Collect data for the sensorId
void sensor::ArraySensor::collect() {
    fdtdUsed_ = true;
    if(points_.size() == numPointsX_ * numPointsY_) {
        for(size_t x = 0; x < numPointsX_; x++) {
            for(size_t y = 0; y < numPointsY_; y++) {
                auto idx = index(x, y);
                points_[idx].collect();
            }
        }
    }
}

// Set data in the sensorId as though it was collected.
// Mostly intended for use by test cases.
// If there are fewer data points provided than required, zeroes are inserted.
void sensor::ArraySensor::setData(const std::vector<box::Vector<double>>& transmitted,
                                  const std::vector<box::Vector<double>>& transmittedRef,
                                  const std::vector<box::Vector<double>>& reflected,
                                  const std::vector<box::Vector<double>>& reflectedRef) {
    fdtdUsed_ = true;
    if(points_.size() == numPointsX_ * numPointsY_) {
        for(size_t x = 0; x < numPointsX_; x++) {
            for(size_t y = 0; y < numPointsY_; y++) {
                auto idx = index(x, y);
                box::Vector<double> transmittedPoint;
                box::Vector<double> transmittedRefPoint;
                box::Vector<double> reflectedPoint;
                box::Vector<double> reflectedRefPoint;
                if(idx < transmitted.size()) {
                    transmittedPoint = transmitted[idx];
                }
                if(idx < transmittedRef.size()) {
                    transmittedRefPoint = transmittedRef[idx];
                }
                if(idx < reflected.size()) {
                    reflectedPoint = reflected[idx];
                }
                if(idx < reflectedRef.size()) {
                    reflectedRefPoint = reflectedRef[idx];
                }
                points_[idx].setData(transmittedPoint, transmittedRefPoint,
                                     reflectedPoint, reflectedRefPoint);
            }
        }
    }
}


// Make the sensorId calculations
void sensor::ArraySensor::calculate() {
    if(points_.size() == numPointsX_ * numPointsY_) {
        for(size_t x = 0; x < numPointsX_; x++) {
            for(size_t y = 0; y < numPointsY_; y++) {
                auto idx = index(x, y);
                points_[idx].calculate(x == currentPointX_ && y == currentPointY_,
                                       channel_);
            }
        }
    }
}

// Return sensorId information
void sensor::ArraySensor::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == attenuation2DSpec_ || spec == phaseShift2DSpec_) {
        info->xSize_ = numPointsX_;
        info->ySize_ = numPointsY_;
        info->min_ = -100.0;
        info->max_ = 0.0;
        info->xUnits_ = "nm";
        info->yUnits_ = "nm";
    } else if(spec == attenuationBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillTransAttenInfo(info);
        }
        info->channelUsed_.resize(4);
        info->channelUsed_[0] = fdtdUsed_;
        info->channelUsed_[1] = propagationMatrixUsed_;
        info->channelUsed_[2] = fdtdUsed_ && componentSel_.y();
        info->channelUsed_[3] = fdtdUsed_ && componentSel_.z();
    } else if(spec == attenuationFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillReflAttenInfo(info);
        }
        info->channelUsed_.resize(4);
        info->channelUsed_[0] = fdtdUsed_;
        info->channelUsed_[1] = propagationMatrixUsed_;
        info->channelUsed_[2] = fdtdUsed_ && componentSel_.y();
        info->channelUsed_[3] = fdtdUsed_ && componentSel_.z();
    } else if(spec == phaseShiftBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillTransPhaseShInfo(info);
        }
        info->channelUsed_.resize(4);
        info->channelUsed_[0] = fdtdUsed_;
        info->channelUsed_[1] = propagationMatrixUsed_;
        info->channelUsed_[2] = fdtdUsed_ && componentSel_.y();
        info->channelUsed_[3] = fdtdUsed_ && componentSel_.z();
    } else if(spec == phaseShiftFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillReflPhaseShInfo(info);
        }
        info->channelUsed_.resize(4);
        info->channelUsed_[0] = fdtdUsed_;
        info->channelUsed_[1] = propagationMatrixUsed_;
        info->channelUsed_[2] = fdtdUsed_ && componentSel_.y();
        info->channelUsed_[3] = fdtdUsed_ && componentSel_.z();
    } else if(spec == timeSeriesBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillTransTimeSeriesInfo(info);
        }
    } else if(spec == timeSeriesFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillReflTimeSeriesInfo(info);
        }
    } else if(spec == polarisation_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillPolarisationInfo(info);
        }
    } else if(spec == skew_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillSkewInfo(info);
        }
    } else if(spec == admittance_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].fillAdmittanceInfo(info);
        }
    } else if(spec == amplitudeDistribution_) {
        info->xSize_ = numPointsX_;
        info->min_ = 0.0;
        info->max_ = 0.0;
        for(size_t x = 0; x < numPointsX_; x++) {
            size_t n = index(x, 0);
            double p1 = points_.at(n).transmittedData().spectrumX().amplitude().at(channel_);
            double p2 = points_.at(n).reflectedData().spectrumX().amplitude().at(channel_);
            if(x == 0) {
                info->min_ = p1;
                info->max_ = p1;
            } else {
                info->min_ = std::min(info->min_, p1);
                info->max_ = std::max(info->max_, p1);
            }
            info->min_ = std::min(info->min_, p2);
            info->max_ = std::max(info->max_, p2);
        }
        info->xUnits_ = "mm";
        info->yUnits_ = "V/m";
        info->xLeft_ = 0;
        info->xRight_ = static_cast<double>(numPointsX_ - 1) * spacingX_
                        / box::Constants::milli_;
    } else if(spec == phaseDistribution_) {
        info->xSize_ = numPointsX_;
        info->min_ = 0.0;
        info->max_ = 0.0;
        for(size_t x = 0; x < numPointsX_; x++) {
            size_t n = index(x, 0);
            double p1 = points_.at(n).transmittedData().spectrumX().phase().at(channel_);
            double p2 = points_.at(n).reflectedData().spectrumX().phase().at(channel_);
            if(x == 0) {
                info->min_ = p1;
                info->max_ = p1;
            } else {
                info->min_ = std::min(info->min_, p1);
                info->max_ = std::max(info->max_, p1);
            }
            info->min_ = std::min(info->min_, p2);
            info->max_ = std::max(info->max_, p2);
        }
        info->xUnits_ = "mm";
        info->yUnits_ = "deg";
        info->xLeft_ = 0;
        info->xRight_ = static_cast<double>(numPointsX_ - 1) * spacingX_
                        / box::Constants::milli_;
    } else {
        Sensor::getInfo(spec, info);
    }
}

// Return sensorId data for display
void sensor::ArraySensor::getData(DataSpec* spec, size_t x, std::vector<double>& dy,
                                  double& dx) {
    if(spec == attenuationBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getTransAttenData(
                    x, dy, dx);
        }
    } else if(spec == attenuationFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getReflAttenData(
                    x, dy, dx);
        }
    } else if(spec == phaseShiftBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getTransPhaseShData(
                    x, dy, dx);
        }
    } else if(spec == phaseShiftFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getReflPhaseShData(
                    x, dy, dx);
        }
    } else if(spec == timeSeriesFront_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getReflTimeSeriesData(
                    x, dy, dx);
        }
    } else if(spec == timeSeriesBack_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getTransTimeSeriesData(
                    x, dy, dx);
        }
    } else if(spec == polarisation_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getPolarisationData(
                    x, dy, dx);
        }
    } else if(spec == skew_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getSkewData(
                    x, dy, dx);
        }
    } else if(spec == admittance_) {
        if(!points_.empty()) {
            points_[index(currentPointX_, currentPointY_)].getAdmittanceData(
                    x, dy, dx);
        }
    } else if(spec == phaseDistribution_) {
        if(!dy.empty()) {
            dy[0] = points_.at(x).transmittedData().spectrumX().phase().at(channel_);
            dy[1] = points_.at(x).reflectedData().spectrumX().phase().at(channel_);
            dx = spacingX_ * x * 1000.0;
        }
    } else if(spec == amplitudeDistribution_) {
        if(!dy.empty()) {
            dy[0] = points_.at(x).transmittedData().spectrumX().amplitude().at(channel_);
            dy[1] = points_.at(x).reflectedData().spectrumX().amplitude().at(channel_);
            dx = spacingX_ * x * 1000.0;
        }
    } else {
        Sensor::getData(spec, x, dy, dx);
    }
}

// Return sensorId data for display
void sensor::ArraySensor::getData(DataSpec* spec, size_t x, size_t y, double* d, double* dx,
                                  double* dy) {
    if(!points_.empty()) {
        size_t n = index(x, y);
        auto& sensor = points_[n];
        *d = 0.0;
        auto c = sensor.column();
        auto p = m_->p()->geometry()->cellCenter(c);
        *dx = p.x();
        *dy = p.y();
        if(spec == attenuation2DSpec_) {
            *d = sensor.transmittanceX(channel_);
        } else if(spec == phaseShift2DSpec_) {
            *d = sensor.phaseShiftX(channel_);
        }
    }
}

// Return sensorId meta-data for display
void sensor::ArraySensor::getMetaData(DataSpec* spec, int id, double* d,
                                      std::string& units) {
    *d = 0.0;
    if(spec == attenuation2DSpec_) {
        switch(id) {
            case DataSpec::metaFrequency:
                *d = frequency_ / box::Constants::giga_;
                units = "GHz";
                break;
            case DataSpec::metaMinimum: {
                *d = points_[channel_].transmitted().x().transmittanceRange().min();
                units = "";
                break;
            }
            case DataSpec::metaMaximum:
                *d = points_[channel_].transmitted().x().transmittanceRange().max();
                units = "";
                break;
            default:
                break;
        }
    } else if(spec == phaseShift2DSpec_) {
        switch(id) {
            case DataSpec::metaFrequency:
                *d = frequency_ / box::Constants::giga_;
                units = "GHz";
                break;
            case DataSpec::metaMinimum:
                *d = points_[channel_].transmitted().x().phaseShiftRange().min();
                units = "";
                break;
            case DataSpec::metaMaximum:
                *d = points_[channel_].transmitted().x().phaseShiftRange().max();
                units = "";
                break;
            default:
                break;
        }
    }
}

// Is the sensorId at the specified location
bool sensor::ArraySensor::isAt(const box::Vector<size_t>& pos) {
    bool result = false;
    // A significant optimisation is to only check for the
    // sensorId point at the reflected and transmitted planes
    if(pos.z() == reflectedZCell_ || pos.z() == transmittedZCell_) {
        for(auto& p : points_) {
            result = result || p.isAt(pos);
        }
    }
    return result;
}

// Return the list of point coordinates this sensorId detects at
std::list<box::Vector<double>> sensor::ArraySensor::pointCoordinates() {
    std::list<box::Vector<double>> result;
    for(const auto& cell : points_) {
        auto c = cell.column();
        auto p = m_->p()->geometry()->cellCenter(c);
        result.push_back(p);
    }
    return result;
}

// Write propagation matrix data into the sensorId
void sensor::ArraySensor::writeData(DataSet dataSet, size_t channel,
                                    const box::Vector<double>& pos, double transAtten,
                                    double transPhShift, double reflAtten,
                                    double reflPhShift) {
    // The cell position
    auto cellPos = m_->p()->geometry()->cellFloor(pos);
    cellPos.z(transmittedZCell_);
    // Find a sensorId point in the cell
    for(auto& sensor : points_) {
        if(sensor.isAt(cellPos)) {
            switch(dataSet) {
                case dataSetFdtd:
                    break;
                case dataSetPropMatrix:
                    propagationMatrixUsed_ = true;
                    sensor.writePropMatrix(channel, transAtten, transPhShift, reflAtten,
                                           reflPhShift);
                    break;
            }
        }
    }
}

// Set the sensorId configuration
void sensor::ArraySensor::validatePoints() {
    numPointsX_ = std::max(numPointsX_, static_cast<size_t>(1U));
    numPointsY_ = std::max(numPointsY_, static_cast<size_t>(1U));
    currentPointX_ = std::max(currentPointX_, static_cast<size_t>(0U));
    currentPointX_ = std::min(currentPointX_, numPointsX_ - 1U);
    currentPointY_ = std::max(currentPointY_, static_cast<size_t>(0U));
    currentPointY_ = std::min(currentPointY_, numPointsY_ - 1U);
}

// Return an index into the point array
size_t sensor::ArraySensor::index(size_t x, size_t y) const {
    size_t result = x + y * numPointsX_;
    if(result >= points_.size()) {
        std::cout << "ArraySensor index out of range" << std::endl;
        result = 0;
    }
    return result;
}

// Calculate the channel number the main 2D sensorId is collecting
void sensor::ArraySensor::calculateChannelNumber() {
    channel_ = frequencies_.channel(frequency_);
}

// Set a meta data item
void sensor::ArraySensor::setMetaData(DataSpec* spec, int id,
                                      double d1, double d2) {
    if(id == DataSpec::metaCurrentXYPos) {
        if(spec == attenuation2DSpec_ || spec == phaseShift2DSpec_) {
            currentPointX_ = static_cast<size_t>(d1);
            currentPointY_ = static_cast<size_t>(d2);
            calculate();
        } else if(spec == attenuationFront_ || spec == attenuationBack_ ||
                  spec == phaseShiftFront_ || spec == phaseShiftBack_) {
            frequency_ = d1 * box::Constants::giga_;
            calculateChannelNumber();
            calculate();
        }
    }
}

// Write the sensorId state information to the binary file
bool sensor::ArraySensor::writeData(std::ofstream& file) {
    bool result = true;
    box::BinaryFileHeader header("ARRAYSENSOR", 1, 0);
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    for(auto& cell : points_) {
        result = result && cell.writeData(file);
    }
    return result;
}

// Read the sensorId state information from the binary file
bool sensor::ArraySensor::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("ARRAYSENSOR")) {
        result = true;
        switch(header.version()) {
            case 1:
                for(auto& cell : points_) {
                    result = result && cell.readData(file);
                }
                calculate();
                break;
            default:
                break;
        }
    }
    return result;
}

// Animate the frequency of the 2D sensorId
void sensor::ArraySensor::animate() {
    if(animateFrequency_) {
        // Advance the frequency
        frequency_ += frequencyStep_;
        if(frequency_ > frequencies_.last()) {
            frequency_ = frequencies_.first();
        }
        // Perform the calculations
        calculateChannelNumber();
        calculate();
        // Move the markers
        attenuationBack_->setMetaData(DataSpec::metaFrequency,
                                      frequency_ / box::Constants::giga_, 0.0);
        attenuationFront_->setMetaData(DataSpec::metaFrequency,
                                       frequency_ / box::Constants::giga_, 0.0);
        phaseShiftBack_->setMetaData(DataSpec::metaFrequency,
                                     frequency_ / box::Constants::giga_, 0.0);
        phaseShiftFront_->setMetaData(DataSpec::metaFrequency,
                                      frequency_ / box::Constants::giga_, 0.0);
    }
}

// Write out data into a CSV file
void sensor::ArraySensor::writeCsvData(std::ofstream* csvFile) {
    // Write out the final fourier transform results to the given CSV file
    if(csvFile != nullptr) {
        *csvFile << "Array sensorId data" << std::endl;
        *csvFile << "Freq";
        for(size_t i = 0; i < points_.size(); i++) {
            *csvFile << ", PM Refl Transmittance, PM Refl PhShift";
            *csvFile << ", PM Tran Transmittance, PM Tran PhShift";
            *csvFile << ", FDTD Refl Transmittance, FDTD Refl PhShift";
            *csvFile << ", FDTD Tran Transmittance, FDTD Tran PhShift";
            *csvFile << ", FDTD Tran Pol, FDTD Refl Pol";
            *csvFile << ", FDTD Tran Skew, FDTD Refl Skew";
            *csvFile << ", Admittance Real, Admittance Imag";
        }
        *csvFile << std::endl;
        for(size_t i = 0; i < frequencies_.n(); i++) {
            *csvFile << (frequencies_.frequency(i) / box::Constants::giga_);
            for(auto& p: points_) {
                *csvFile << ", " << p.reflectedPropMatrix().transmittance()[i];
                *csvFile << ", " << p.reflectedPropMatrix().phaseShift()[i];
                *csvFile << ", " << p.transmittedPropMatrix().transmittance()[i];
                *csvFile << ", " << p.transmittedPropMatrix().phaseShift()[i];
                *csvFile << ", " << p.reflected().x().transmittance()[i];
                *csvFile << ", " << p.reflected().x().phaseShift()[i];
                *csvFile << ", " << p.transmitted().x().transmittance()[i];
                *csvFile << ", " << p.transmitted().x().phaseShift()[i];
                *csvFile << ", " << p.transmitted().polarisation()[i];
                *csvFile << ", " << p.reflected().polarisation()[i];
                *csvFile << ", " << p.transmitted().skew()[i];
                *csvFile << ", " << p.reflected().skew()[i];
                *csvFile << ", " << p.transmitted().x().admittance()[i].real();
                *csvFile << ", " << p.transmitted().x().admittance()[i].imag();
            }
            *csvFile << std::endl;
        }
    }
}

// Return the CSV data for the transmitted data time series
std::string sensor::ArraySensor::transmittedTimeSeriesCsv() {
    std::stringstream result;
    // The block header row
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << y << "," << x << "\t\t\t";
        }
    }
    result << std::endl;
    // The column header row
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << "x\ty\tz\t";
        }
    }
    result << std::endl;
    // The data
    for(size_t n = 0; n < nSamples_; n++) {
        for(size_t y = 0; y < numPointsY_; y++) {
            for(size_t x = 0; x < numPointsX_; x++) {
                size_t idx = index(x, y);
                if(idx < points_.size()) {
                    points_[idx].calculate(true, n);
                    result << points_[idx].transmittedData().timeSeriesX()[n] << "\t";
                    result << points_[idx].transmittedData().timeSeriesY()[n] << "\t";
                    result << points_[idx].transmittedData().timeSeriesZ()[n] << "\t";
                }
            }
        }
        result << std::endl;
    }
    return result.str();
}

// Return the CSV data for the reflected data time series
std::string sensor::ArraySensor::reflectedTimeSeriesCsv() {
    std::stringstream result;
    // The block header row
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << y << "," << x << "\t\t\t";
        }
    }
    result << std::endl;
    // The column header row
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << "x\ty\tz\t";
        }
    }
    result << std::endl;
    // The data
    for(size_t n = 0; n < nSamples_; n++) {
        for(size_t y = 0; y < numPointsY_; y++) {
            for(size_t x = 0; x < numPointsX_; x++) {
                size_t idx = index(x, y);
                if(idx < points_.size()) {
                    points_[idx].calculate(true, n);
                    result << points_[idx].reflectedData().timeSeriesX()[n] << "\t";
                    result << points_[idx].reflectedData().timeSeriesY()[n] << "\t";
                    result << points_[idx].reflectedData().timeSeriesZ()[n] << "\t";
                }
            }
        }
        result << std::endl;
    }
    return result.str();
}

// Return the CSV data for the transmitted spectra
std::string sensor::ArraySensor::transmittedSpectraCsv() {
    std::stringstream result;
    // The block header row
    result << "\t";
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << y << "," << x << "\t\t\t";
        }
    }
    result << std::endl;
    // The column header row
    result << "frequency\t";
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << "x\ty\tz\t";
        }
    }
    result << std::endl;
    // The data
    for(size_t n = 0; n < frequencies_.n(); n++) {
        result << frequencies_.frequency(n) << "\t";
        for(size_t y = 0; y < numPointsY_; y++) {
            for(size_t x = 0; x < numPointsX_; x++) {
                size_t idx = index(x, y);
                points_[idx].calculate(true, n);
                result << points_[idx].transmittedData().spectrumX()[n].real() << "+"
                       << points_[idx].transmittedData().spectrumX()[n].imag() << "i\t";
                result << points_[idx].transmittedData().spectrumY()[n].real() << "+"
                       << points_[idx].transmittedData().spectrumY()[n].imag() << "i\t";
                result << points_[idx].transmittedData().spectrumZ()[n].real() << "+"
                       << points_[idx].transmittedData().spectrumZ()[n].imag() << "i\t";
            }
        }
        result << std::endl;
    }
    return result.str();
}

// Return the CSV data for the reflected spectra
std::string sensor::ArraySensor::reflectedSpectraCsv() {
    std::stringstream result;
    // The block header row
    result << "\t";
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << y << "," << x << "\t\t\t";
        }
    }
    result << std::endl;
    // The column header row
    result << "frequency\t";
    for(size_t y = 0; y < numPointsY_; y++) {
        for(size_t x = 0; x < numPointsX_; x++) {
            result << "x\ty\tz\t";
        }
    }
    result << std::endl;
    // The data
    for(size_t n = 0; n < frequencies_.n(); n++) {
        result << frequencies_.frequency(n) << "\t";
        for(size_t y = 0; y < numPointsY_; y++) {
            for(size_t x = 0; x < numPointsX_; x++) {
                size_t idx = index(x, y);
                points_[idx].calculate(true, n);
                result << points_[idx].reflectedData().spectrumX()[n].real() << "+"
                       << points_[idx].reflectedData().spectrumX()[n].imag() << "i\t";
                result << points_[idx].reflectedData().spectrumY()[n].real() << "+"
                       << points_[idx].reflectedData().spectrumY()[n].imag() << "i\t";
                result << points_[idx].reflectedData().spectrumZ()[n].real() << "+"
                       << points_[idx].reflectedData().spectrumZ()[n].imag() << "i\t";
            }
        }
        result << std::endl;
    }
    return result.str();
}

// Copy the output data into the given vector objects
void sensor::ArraySensor::copyOutputData(size_t x, size_t y, std::vector<double>& attenuation,
                                         std::vector<double>& phaseShift, bool propMatrix,
                                         std::vector<double>& timeSeries,
                                         std::vector<double>& reference,
                                         bool& xyComponents, std::vector<double>& attenuationY,
                                         std::vector<double>& phaseShiftY) {
    size_t idx = index(x, y);
    points_[idx].copyOutputData(attenuation, phaseShift, propMatrix,
                                timeSeries, reference,
                                xyComponents, attenuationY, phaseShiftY);
}

// Copy the output data into the given object
void sensor::ArraySensor::copyOutputData(size_t x, size_t y, fdtd::RecordedData& data) {
    size_t idx = index(x, y);
    if(idx < points_.size()) {
        points_[idx].copyOutputData(data);
    }
}

// Write the configuration to the DOM object
void sensor::ArraySensor::writeConfig(xml::DomObject& p) const {
    // Base class first
    Sensor::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("nsamples") << nSamples_;
    p << xml::Obj("numpointsx") << numPointsX_;
    p << xml::Obj("numpointsy") << numPointsY_;
    p << xml::Obj("frequency") << frequency_;
    p << xml::Obj("animate") << animateFrequency_;
    p << xml::Obj("animatestep") << frequencyStep_;
    p << xml::Obj("averageovercell") << averageOverCell_;
    p << xml::Obj("manualsamples") << manualSamples_;
    p << xml::Obj("manualzpos") << manualZPos_;
    p << xml::Obj("transmittedposz") << transmittedPosZ_;
    p << xml::Obj("reflectedposz") << reflectedPosZ_;
    p << xml::Obj("calculatex") << componentSel_.x();
    p << xml::Obj("calculatey") << componentSel_.y();
    p << xml::Obj("calculatez") << componentSel_.z();
    p << xml::Obj("includereflected") << includeReflected_;
    p << xml::Obj("usewindow") << useWindow_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::ArraySensor::readConfig(xml::DomObject& p) {
    // Base class first
    Sensor::readConfig(p);
    // Now my things
    bool v;
    p >> xml::Reopen();
    p >> xml::Obj("nsamples") >> nSamples_;
    p >> xml::Obj("numpointsx") >> xml::Default(1) >> numPointsX_;
    p >> xml::Obj("numpointsy") >> xml::Default(1) >> numPointsY_;
    p >> xml::Obj("frequency") >> frequency_;
    p >> xml::Obj("animate") >> xml::Default(false) >> animateFrequency_;
    p >> xml::Obj("animatestep") >> xml::Default(0.0) >> frequencyStep_;
    p >> xml::Obj("averageovercell") >> xml::Default(false) >> averageOverCell_;
    p >> xml::Obj("manualsamples") >> xml::Default(false) >> manualSamples_;
    p >> xml::Obj("manualzpos") >> xml::Default(false) >> manualZPos_;
    p >> xml::Obj("transmittedposz") >> xml::Default(0.0) >> transmittedPosZ_;
    p >> xml::Obj("reflectedposz") >> xml::Default(0.0) >> reflectedPosZ_;
    p >> xml::Obj("calculatex") >> xml::Default(true) >> v;
    componentSel_.x(v);
    p >> xml::Obj("calculatey") >> xml::Default(false) >> v;
    componentSel_.y(v);
    p >> xml::Obj("calculatez") >> xml::Default(false) >> v;
    componentSel_.z(v);
    p >> xml::Obj("includereflected") >> xml::Default(false) >> includeReflected_;
    p >> xml::Obj("usewindow") >> xml::Default(false) >> useWindow_;
    p >> xml::Close();
}

// Return a transmitted electric field value
box::Vector<double> sensor::ArraySensor::transmittedValue(size_t x, size_t y) {
    return m_->e()->value({x, y, transmittedZCell_});
}

// Return a reflected electric field value
box::Vector<double> sensor::ArraySensor::reflectedValue(size_t x, size_t y) {
    return m_->e()->value({x, y, reflectedZCell_});
}

// Return the transmittance for a given frequency
void sensor::ArraySensor::transmittanceAt(size_t x, size_t y, double frequency,
                                          double& transX, double& transY, bool reflected) {
    size_t bin = frequencies_.channel(frequency);
    points_[index(x, y)].transmittanceAt(bin, transX, transY, reflected);
}

// Return the phase at a given frequency
void sensor::ArraySensor::phaseShiftAt(size_t x, size_t y, double frequency,
                                       double& phaseX, double& phaseY, bool reflected) {
    size_t bin = frequencies_.channel(frequency);
    points_[index(x, y)].phaseShiftAt(bin, phaseX, phaseY, reflected);
}

// Return the phase at a given frequency
void sensor::ArraySensor::phaseAt(size_t x, size_t y, double frequency,
                                  double& phaseX, double& phaseY, bool reflected) {
    size_t bin = frequencies_.channel(frequency);
    points_[index(x, y)].phaseAt(bin, phaseX, phaseY, reflected);
}

// Return the amplitude at a given frequency
void sensor::ArraySensor::amplitudeAt(size_t x, size_t y, double frequency,
                                      double& ampX, double& ampY, bool reflected) {
    size_t bin = frequencies_.channel(frequency);
    points_[index(x, y)].amplitudeAt(bin, ampX, ampY, reflected);
}
