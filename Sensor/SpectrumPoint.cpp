/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SpectrumPoint.h"
#include <algorithm>

// Constructor
sensor::SpectrumPoint::SpectrumPoint() :
    numSamples_(0) {
}

// Initialise ready for a run
void sensor::SpectrumPoint::initialise(const box::FrequencySeq& frequencies,
                                       size_t numThreadsToUse, double samplePeriod,
                                       const box::ComponentSel& componentSel, 
                                       size_t numSamples, bool useWindow) {
    // Work out the number of samples required in the time series
    numSamples_ = numSamples;
    // Initialise the time series
    timeSeriesX_.resize(numSamples_);
    timeSeriesX_.fill(0.0);
    timeSeriesY_.resize(numSamples_);
    timeSeriesY_.fill(0.0);
    timeSeriesZ_.resize(numSamples_);
    timeSeriesZ_.fill(0.0);
    // Initialise the spectrum analysis
    spectrumX_.initialise(frequencies, numThreadsToUse, samplePeriod, useWindow, numSamples_);
    spectrumY_.initialise(frequencies, numThreadsToUse, samplePeriod, useWindow, numSamples_);
    spectrumZ_.initialise(frequencies, numThreadsToUse, samplePeriod, useWindow, numSamples_);
    // Record which components
    componentSel_ = componentSel;
}

// Collect a pair of data samples
void sensor::SpectrumPoint::collect(double x, double y, double z) {
    timeSeriesX_.push(x);
    timeSeriesY_.push(y);
    timeSeriesZ_.push(z);
}

// Calculate the spectra
void sensor::SpectrumPoint::calculate() {
    if(componentSel_.x()) {
        spectrumX_.calculate(timeSeriesX_);
    }
    if(componentSel_.y()) {
        spectrumY_.calculate(timeSeriesY_);
    }
    if(componentSel_.z()) {
        spectrumZ_.calculate(timeSeriesZ_);
    }
}

// Calculate one channel of the spectra
void sensor::SpectrumPoint::calculate(size_t channel) {
    if(componentSel_.x()) {
        spectrumX_.calculate(channel, timeSeriesX_);
    }
    if(componentSel_.y()) {
        spectrumY_.calculate(channel, timeSeriesY_);
    }
    if(componentSel_.z()) {
        spectrumZ_.calculate(channel, timeSeriesZ_);
    }
}

// Return the range of the data stored in the time series
void sensor::SpectrumPoint::timeSeriesMinMax(double& min, double& max) {
    min = 0.0;
    max = 0.0;
    if(!timeSeriesX_.empty()) {
        min = timeSeriesX_[0];
        max = min;
    }
    for(auto s : timeSeriesX_) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : timeSeriesY_) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : timeSeriesZ_) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
}

// Return the range of the data stored in the power spectra
void sensor::SpectrumPoint::powerMinMax(double& min, double& max) {
    min = 0.0;
    max = 0.0;
    if(!spectrumX_.empty()) {
        min = spectrumX_.amplitude()[0];
        max = min;
    }
    for(auto s : spectrumX_.amplitude()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : spectrumY_.amplitude()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : spectrumZ_.amplitude()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
}

// Return the range of the data stored in the power spectra
void sensor::SpectrumPoint::phaseMinMax(double& min, double& max) {
    min = 0.0;
    max = 0.0;
    if(!spectrumX_.empty()) {
        min = spectrumX_.phase()[0];
        max = min;
    }
    for(auto s : spectrumX_.phase()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : spectrumY_.phase()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : spectrumZ_.phase()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
}
