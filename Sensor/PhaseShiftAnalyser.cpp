/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PhaseShiftAnalyser.h"
#include "ArraySensor.h"
#include <Fdtd/Model.h>

// Calculate the error using the current state of the sensorId
void sensor::PhaseShiftAnalyser::calculate() {
    error_ = 0.0;
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        // Get the information from the sensorId
        size_t numSamples = sensor->frequencies().n();
        data_.resize(numSamples);
        reference_.resize(numSamples);
        delta_.resize(numSamples);
        dataType_ = DataType::phaseShift;
        minX_ = sensor->frequencies().first();
        stepX_ = sensor->frequencies().spacing();
        // Make the reference from the points
        SpecTableAnalyser::Point last(0.0, 0.0, 0.0);
        double nextSample = minX_;
        size_t sampleN = 0;
        for(auto& point : points_) {
            // The equation of the straight lines between the two points
            double minM = (point.min() - last.min()) / (point.x() - last.x());
            double minC = last.min() - minM * last.x();
            double maxM = (point.max() - last.max()) / (point.x() - last.x());
            double maxC = last.max() - maxM * last.x();
            // Now fill the frequency samples between the two points
            while(nextSample > last.x() &&
                  nextSample <= point.x() && sampleN < numSamples) {
                double min = minM * nextSample + minC;
                double max = maxM * nextSample + maxC;
                reference_[sampleN] = (max + min) / 2.0;
                delta_[sampleN] = (max - min) / 2.0;
                nextSample += stepX_;
                sampleN++;
            }
            last = point;
        }
        // Record the data and calculate the error
        double ax = 0.0;
        double ay = 0.0;
        for(size_t i = 0; i < numSamples; i++) {
            // Get the point
            sensor->phaseShiftAt(0, 0, i * stepX_ + minX_, ax, ay,
                                    location_ != SensorLocation::highestZ);
            double measured = component_ == SensorComponent::y ? ay : ax;
            data_[i] = measured;
            // The error
            double reference = reference_[i];
            double delta = delta_[i];
            double diff = 0.0;
            if(measured > (reference + delta)) {
                diff = measured - reference - delta;
            } else if(measured < (reference - delta)) {
                diff = reference - delta - measured;
            }
            error_ += diff * diff;
            // Calculate the min/max
            if(i == 0) {
                minY_ = measured;
                maxY_ = measured;
            } else {
                minY_ = std::min(minY_, measured);
                maxY_ = std::max(maxY_, measured);
            }
        }
    }
}

