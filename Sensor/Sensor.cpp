/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Sensor.h"
#include <Fdtd/Model.h>
#include <Xml/DomObject.h>

// Second stage constructor used by the factory template
void sensor::Sensor::construct(int mode, int identifier, fdtd::Model* m) {
    m_ = m;
    identifier_ = identifier;
    mode_ = mode;
}

// Write the configuration to the DOM object
void sensor::Sensor::writeConfig(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("mode2") << mode_;
    p << xml::Obj("name") << name_;
    p << xml::Obj("identifier") << identifier_;
    p << xml::Obj("color") << colour_;
    p << xml::Obj("savecsvdata") << saveCsvData_;
    p << xml::Obj("seqgenerated") << seqGenerated_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::Sensor::readConfig(xml::DomObject& p) {
    // Don't need to read mode_ or identifier_ as it was read by the factory
    p >> xml::Open();
    p >> xml::Obj("name") >> name_;
    p >> xml::Obj("color") >> colour_;
    p >> xml::Obj("savecsvdata") >> saveCsvData_;
    p >> xml::Obj("seqgenerated") >> xml::Default(false) >> seqGenerated_;
    p >> xml::Close();
}

// Return a data specification for a sensorId
sensor::DataSpec* sensor::Sensor::getDataSpec(const std::string& data) {
    DataSpec* result = nullptr;
    for(auto& spec : specs_) {
        if(spec->name() == data) {
            result = spec;
        }
    }
    return result;
}
