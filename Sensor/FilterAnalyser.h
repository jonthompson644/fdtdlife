/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FILTERANALYSER_H
#define FDTDLIFE_FILTERANALYSER_H

#include <list>
#include <Xml/DomObject.h>
#include <Box/Expression.h>
#include "Analyser.h"

namespace sensor {

    // A base class that provides a table of points for analysers.
    class FilterAnalyser : public Analyser {
    public:
        // Types
        enum class FilterType {
            butterworth, chebyshevI, chebyshevII, elliptical, bessel
        };
        enum class Property {
            transmittance, phaseShift
        };
        enum class FilterMode {
            lowPass, highPass
        };

        // Construction
        FilterAnalyser() = default;

        // Overrides of Analyser
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void calculate() override;
        void initialise() override;
        void evaluate(box::Expression::Context& c) override;

        // Getters
        FilterType filterType() const { return filterType_; }
        FilterMode filterMode() const { return filterMode_; }
        Property property() const { return property_; }
        size_t order() const { return order_; }
        box::Expression& cutOffFrequency() { return cutOffFrequency_; }
        box::Expression& maxFrequency() { return maxFrequency_; }
        box::Expression& passbandRipple() { return passbandRipple_; }

        // Setters
        void filterType(FilterType v) { filterType_ = v; }
        void filterMode(FilterMode v) { filterMode_ = v; }
        void property(Property v) { property_ = v; }
        void order(size_t v) { order_ = v; }

    protected:
        // Configuration
        FilterType filterType_{FilterType::butterworth};  // Which filter type
        Property property_{Property::transmittance};  // Which property to test against
        FilterMode filterMode_{FilterMode::lowPass}; // Which mode of filter operation
        size_t order_{1};  // The order of the filter
        box::Expression cutOffFrequency_{10e9};  // The (3dB) cut off frequency
        box::Expression maxFrequency_{20e9};  // The maximum frequency to test for
        box::Expression passbandRipple_{1.0};  // Passband ripple in dB (chebyshev filters)
    };

}

#endif //FDTDLIFE_FILTERANALYSER_H
