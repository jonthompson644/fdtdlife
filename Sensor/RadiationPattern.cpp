/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RadiationPattern.h"
#include <Box/Constants.h>
#include <cmath>
#include <Fdtd/Model.h>
#include <Fdtd/Configuration.h>
#include <Source/Sources.h>
#include <Xml/DomObject.h>
#include <algorithm>

// Construction
sensor::RadiationPattern::RadiationPattern() :
        frequencies_(1 * box::Constants::giga_, 1 * box::Constants::giga_, 1) {
    timeSeriesSpec_ = new DataSpec(this, "TimeSeries",
                                   DataSpec::modeTimeSeries,
                                   {"X", "Y"});
    radiationPatternSpec_ = new DataSpec(this, "RadiationPattern",
                                         DataSpec::modeTimeSeries,
                                         {"X", "Y"});
    specs_.push_back(timeSeriesSpec_);
    specs_.push_back(radiationPatternSpec_);
    setPatternSpectrumBin();
}

// Prepare the sensorId for a run
void sensor::RadiationPattern::initialise() {
    // The cell coordinate of the center
    centerCell_ = m_->p()->geometry()->cellFloor(center_);
    // Work out the length of the time series we need to capture
    frequencies_ = m_->sources().frequencyInfo();
    numSamples_ = frequencies_.requiredSamples(m_->p()->dt());
    // Make the correct number of points
    points_.resize(numPoints_);
    coPattern_.resize(numPoints_);
    crossPattern_.resize(numPoints_);
    // Set up the recording points on a semi-circle surrounding the center point
    double deltaTheta = box::Constants::pi_ / (double) (numPoints_ - 1);  // Radians
    for(size_t i = 0; i < numPoints_; i++) {
        // Coordinates of the sensorId point
        box::Vector<double> sensorPoint;
        sensorPoint.x(distance_ * std::cos((double) i * deltaTheta));
        double dx = sensorPoint.x() - center_.x();
        sensorPoint.z(std::sqrt(distance_ * distance_ - dx * dx));
        if(!positiveSemicircle_) {
            sensorPoint.z(-sensorPoint.z());
        }
        sensorPoint.z(sensorPoint.z() + center_.z());
        sensorPoint.y(center_.y());
        // Convert to the FDTD cell coordinates
        box::Vector<size_t> cell = m_->p()->geometry()->cellFloor(sensorPoint);
        points_[i].initialise(cell, frequencies_,
                              (size_t) m_->p()->numThreadsUsed(),
                              m_->p()->dt(), numSamples_);
    }
}

// Write the configuration to the DOM object
void sensor::RadiationPattern::writeConfig(xml::DomObject& p) const {
    // Base class first
    Sensor::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("positivesemicircle") << positiveSemicircle_;
    p << xml::Obj("centerx") << center_.x();
    p << xml::Obj("centery") << center_.y();
    p << xml::Obj("centerz") << center_.z();
    p << xml::Obj("distance") << distance_;
    p << xml::Obj("numpoints") << numPoints_;
    p << xml::Obj("patternfrequency") << patternFrequency_;
    p << xml::Obj("patternpolarisation") << patternPolarisation_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::RadiationPattern::readConfig(xml::DomObject& p) {
    // Base class first
    Sensor::readConfig(p);
    // Now my things
    double v;
    p >> xml::Reopen();
    p >> xml::Obj("centerx") >> v;
    center_.x(v);
    p >> xml::Obj("centery") >> v;
    center_.y(v);
    p >> xml::Obj("centerz") >> v;
    center_.z(v);
    p >> xml::Obj("positivesemicircle") >> positiveSemicircle_;
    p >> xml::Obj("distance") >> distance_;
    p >> xml::Obj("numpoints") >> numPoints_;
    p >> xml::Obj("patternfrequency") >> xml::Default(1 * box::Constants::giga_)
      >> patternFrequency_;
    p >> xml::Obj("patternpolarisation") >> xml::Default(0.0) >> patternPolarisation_;
    p >> xml::Close();
    // Calculate dependent variables
    setPatternSpectrumBin();
}

// Is the sensorId at the specified location
bool sensor::RadiationPattern::isAt(const box::Vector<size_t>& pos) {
    bool result = pos == centerCell_;
    if(pos.y() == centerCell_.y()) {
        for(auto& p : points_) {
            result = result || p.isAt(pos);
        }
    }
    return result;
}

// Collect data for the sensorId
void sensor::RadiationPattern::collect() {
    for(auto& p : points_) {
        p.collect(m_);
    }
}

// Return sensorId information
void sensor::RadiationPattern::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == timeSeriesSpec_) {
        size_t sampledPoint = numPoints_ / 2;
        points_[sampledPoint].getMinMax(info->min_, info->max_);
        info->xSize_ = (int) numSamples_;
        info->yUnits_ = "V/m";
        info->xUnits_ = "ps";
        info->xLeft_ = -(numSamples_ * m_->p()->dt()) / box::Constants::pico_;
        info->xRight_ = 0;
    } else if(spec == radiationPatternSpec_) {
        getPatternMinMax(info->min_, info->max_);
        info->xSize_ = (int) numPoints_;
        info->yUnits_ = "V/m";
        info->xUnits_ = "deg";
        info->xLeft_ = -90.0;
        info->xRight_ = 90.0;
    } else {
        Sensor::getInfo(spec, info);
    }
}

// Return 1D sensorId data for display
void sensor::RadiationPattern::getData(DataSpec* spec, size_t x, std::vector<double>& dy,
                                       double& dx) {
    if(spec == timeSeriesSpec_) {
        size_t sampledPoint = numPoints_ / 2;
        if(x < points_[sampledPoint].point().numSamples()) {
            if(!dy.empty()) {
                dy[0] = points_[sampledPoint].point().timeSeriesX()[x];
            }
            if(dy.size() > 1) {
                dy[1] = points_[sampledPoint].point().timeSeriesY()[x];
            }
            dx = m_->p()->dt() * (double) (points_[sampledPoint].point().numSamples() - x);
        }
    } else if(spec == radiationPatternSpec_) {
        if(x < numPoints_) {
            if(!dy.empty()) {
                dy[0] = coPattern_[x];
            }
            if(dy.size() > 1) {
                dy[1] = crossPattern_[x];
            }
            dx = x * 180.0 / (double) (numPoints_ - 1) - 90.0;
        }
    } else {
        Sensor::getData(spec, x, dy, dx);
    }
}

// Make the sensorId calculations
void sensor::RadiationPattern::calculate() {
    for(auto& p : points_) {
        p.calculate();
    }
    assemblePatterns();
}

// Assemble the current radiation patterns
void sensor::RadiationPattern::assemblePatterns() {
    if(points_.size() == numPoints_) {
        for(size_t i = 0; i < numPoints_; i++) {
            points_[i].powerAt(patternSpectrumBin_, patternPolarisation_, coPattern_[i],
                               crossPattern_[i]);
        }
    }
}

// Return the minimum and maximum values for the current radiation pattern
void sensor::RadiationPattern::getPatternMinMax(double& min, double& max) {
    min = 0.0;
    max = 0.0;
    if(!points_.empty()) {
        min = coPattern_[0];
        max = min;
        for(double v : coPattern_) {
            min = std::min(v, min);
            max = std::max(v, max);
        }
        for(double v : crossPattern_) {
            min = std::min(v, min);
            max = std::max(v, max);
        }
    }
}

// Calculate the spectrum bin that is currently forming the radiation pattern
void sensor::RadiationPattern::setPatternSpectrumBin() {
    patternSpectrumBin_ = frequencies_.channel(patternFrequency_);
}

// The frequency at which to calculate the radiation pattern has changed
void sensor::RadiationPattern::patternFrequency(double v) {
    patternFrequency_ = v;
    setPatternSpectrumBin();
    assemblePatterns();
}



