/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "MaterialSensor.h"
#include <Domain/Material.h>
#include <Fdtd/Model.h>
#include <Source/Sources.h>
#include <Source/Source.h>

// Constructor
sensor::MaterialSensor::MaterialSensor() :
        materialSpec_(this, "material", DataSpec::modeMaterial,
                      {"material"}) {
    specs_.push_back(&materialSpec_);
}

// Return the model value at the specified cell coordinates
double sensor::MaterialSensor::getValue(const box::Vector<size_t>& p) {
    double result = 0.0;
    domain::Material* mat;
    // Count the material types in the projection
    std::map<int, size_t> materials;
    size_t maxDepth = 0;
    switch(orientation_) {
        case box::Constants::orientationXPlane:
            maxDepth = m_->p()->geometry()->n().x();
            for(size_t i = 0; i < m_->p()->geometry()->n().x(); i++) {
                mat = m_->d()->getMaterialAt({i, p.y(), p.z()});
                if(mat != nullptr) {
                    if(materials.count(mat->index()) > 0) {
                        materials[mat->index()]++;
                    } else {
                        materials[mat->index()] = 1;
                    }
                }
            }
            break;
        case box::Constants::orientationYPlane:
            maxDepth = m_->p()->geometry()->n().y();
            for(size_t j = 0; j < m_->p()->geometry()->n().y(); j++) {
                mat = m_->d()->getMaterialAt({p.x(), j, p.z()});
                if(mat != nullptr) {
                    if(materials.count(mat->index()) > 0) {
                        materials[mat->index()]++;
                    } else {
                        materials[mat->index()] = 1;
                    }
                }
            }
            break;
        case box::Constants::orientationZPlane:
            maxDepth = m_->p()->geometry()->n().z();
            for(size_t k = 0; k < m_->p()->geometry()->n().z(); k++) {
                mat = m_->d()->getMaterialAt({p.x(), p.y(), k});
                if(mat != nullptr) {
                    if(materials.count(mat->index()) > 0) {
                        materials[mat->index()]++;
                    } else {
                        materials[mat->index()] = 1;
                    }
                }
            }
            break;
    }
    // Use the material with the largest count.
    // Only return the default material if there is nothing else in the projection.
    // Only return a boundary material if it completely fills the cross-section
    size_t biggestCount = 0;
    int biggestIndex = domain::Domain::defaultMaterial;
    for(auto found : materials) {
        if(found.first == domain::Domain::cpmlMaterial) {
            if(found.second >= maxDepth) {
                // Only return a boundary material if it completely fills the depth
                biggestCount = found.second;
                biggestIndex = found.first;
            }
        } else if(found.first != domain::Domain::defaultMaterial) {
            if(found.second > biggestCount) {
                // Return the material with the biggest count
                biggestCount = found.second;
                biggestIndex = found.first;
            }
        }
    }
    mat = m_->d()->getMaterial(biggestIndex);
    if(mat != nullptr) {
        result = static_cast<double>(static_cast<int>(mat->color()));
    }
    // Is there a source overlaying this?
    source::Source* source = m_->sources().getAt(p);
    if(source != nullptr) {
        result = static_cast<double>(static_cast<int>(source->colour()));
    }
    // Is there a sensorId overlaying this?
    Sensor* sensor = m_->sensors().getAt(p);
    if(sensor != nullptr) {
        result = static_cast<double>(static_cast<int>(sensor->colour()));
    }
    return result;
}

// Return the data stream information
void sensor::MaterialSensor::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == &materialSpec_) {
        info->xSize_ = nSliceX_;
        info->ySize_ = nSliceY_;
        info->zPos_ = static_cast<int>(sliceOffset_);
        info->min_ = min_;
        info->max_ = max_;
        info->xUnits_ = "nm";
        info->yUnits_ = "nm";
    } else {
        return TwoDSlice::getInfo(spec, info);
    }
}

// Return a data point from the stream
void sensor::MaterialSensor::getData(DataSpec* spec, size_t x, size_t y, double* d,
                                     double* dx, double* dy) {
    if(spec == &materialSpec_) {
        if(data_ != nullptr) {
            *d = data_[index(x, y)];
            switch(orientation_) {
                case box::Constants::orientationXPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().y().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().z().value() * y / box::Constants::giga_;
                    break;
                case box::Constants::orientationYPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().x().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().z().value() * y / box::Constants::giga_;
                    break;
                case box::Constants::orientationZPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().x().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().y().value() * y / box::Constants::giga_;
                    break;
            }
        }
    } else {
        TwoDSlice::getData(spec, x, y, d, dx, dy);
    }
}
