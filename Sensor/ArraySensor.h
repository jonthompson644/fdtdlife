/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ARRAYSENSOR_H
#define FDTDLIFE_ARRAYSENSOR_H

#include "ArraySensorPoint.h"
#include "Sensor.h"
#include <Box/FrequencySeq.h>
#include <Box/ComponentSel.h>
#include <map>
#include <string>

namespace fdtd { class RecordedData; }

namespace sensor {

    // An array sensorId that places many detectors both in front and behind the test system
    class ArraySensor : public Sensor {
    public:
        enum DataSet {
            dataSetFdtd = 0, dataSetPropMatrix
        };
    public:
        // Construction
        ArraySensor();
        ~ArraySensor() override;

        // Overrides of Sensor
        void initialise() override;
        void collect() override;
        void calculate() override;
        void getInfo(DataSpec* spec, DataSpec::Info* info) override;
        void getData(DataSpec* spec, size_t x, std::vector<double>& dy, double& dx) override;
        void getData(DataSpec* spec, size_t x, size_t y, double* d, double* dx,
                     double* dy) override;
        void getMetaData(DataSpec* spec, int id, double* d, std::string& units) override;
        void setMetaData(DataSpec* spec, int id, double d1, double d2) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        bool writeData(std::ofstream& file) override;
        bool readData(std::ifstream& file) override;
        void animate() override;
        void writeCsvData(std::ofstream* csvFile) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;

        // API
        std::list<box::Vector<double>> pointCoordinates();
        void writeData(DataSet dataSet, size_t channel, const box::Vector<double>& pos,
                       double transAtten, double transPhShift,
                       double reflAtten, double reflPhShift);
        void setData(const std::vector<box::Vector<double>>& transmitted,
                     const std::vector<box::Vector<double>>& transmittedRef,
                     const std::vector<box::Vector<double>>& reflected,
                     const std::vector<box::Vector<double>>& reflectedRef);
        size_t index(size_t x, size_t y) const;
        std::string transmittedTimeSeriesCsv();
        std::string reflectedTimeSeriesCsv();
        std::string transmittedSpectraCsv();
        std::string reflectedSpectraCsv();

        // Getters
        size_t nSamples() const { return nSamples_; }
        size_t numPointsX() const { return numPointsX_; }
        size_t numPointsY() const { return numPointsY_; }
        size_t currentPointX() const { return currentPointX_; }
        size_t currentPointY() const { return currentPointY_; }
        double frequency() const { return frequency_; }
        bool animateFrequency() const { return animateFrequency_; }
        double frequencyStep() const { return frequencyStep_; }
        bool averageOverCell() const { return averageOverCell_; }
        const box::FrequencySeq& frequencies() const { return frequencies_; }
        double reflectedZPos() const { return reflectedPosZ_; }
        double transmittedZPos() const { return transmittedPosZ_; }
        bool manualSamples() const { return manualSamples_; }
        bool manualZPos() const { return manualZPos_; }
        size_t reflectedReferenceZ() const { return reflectedReferenceZCell_; }
        size_t reflectedZ() const { return reflectedZCell_; }
        size_t transmittedZ() const { return transmittedZCell_; }
        const box::ComponentSel& componentSel() const { return componentSel_; }
        bool includeReflected() const { return includeReflected_; }
        double spacingX() const { return spacingX_; }
        double spacingY() const { return spacingY_; }
        bool useWindow() const { return useWindow_; }

        // Setters
        void nSamples(size_t v) { nSamples_ = v; }
        void reflectedZPos(double v) { reflectedPosZ_ = v; }
        void transmittedZPos(double v) { transmittedPosZ_ = v; }
        void manualZPos(bool v) { manualZPos_ = v; }
        void manualSamples(bool v) { manualSamples_ = v; }
        void averageOverCell(bool v) { averageOverCell_ = v; }
        void useWindow(bool v) { useWindow_ = v; }
        void numPointsX(size_t v) {
            numPointsX_ = v;
            validatePoints();
        }
        void numPointsY(size_t v) {
            numPointsY_ = v;
            validatePoints();
        }
        void currentPointX(size_t v) {
            currentPointX_ = v;
            validatePoints();
        }
        void currentPointY(size_t v) {
            currentPointY_ = v;
            validatePoints();
        }
        void frequency(double v) {
            frequency_ = v;
            calculateChannelNumber();
        }
        void animateFrequency(bool v) { animateFrequency_ = v; }
        void frequencyStep(double v) {
            frequencyStep_ = v;
            calculateChannelNumber();
        }
        void componentSel(const box::ComponentSel& v) { componentSel_ = v; }
        void includeReflected(bool v) { includeReflected_ = v; }

        // Data access
        void copyOutputData(size_t x, size_t y, std::vector<double>& attenuation,
                            std::vector<double>& phaseShift, bool propMatrix,
                            std::vector<double>& timeSeries, std::vector<double>& reference,
                            bool& xyComponents, std::vector<double>& attenuationY,
                            std::vector<double>& phaseShiftY);
        void copyOutputData(size_t x, size_t y, fdtd::RecordedData& data);
        virtual box::Vector<double> transmittedValue(size_t x, size_t y);
        virtual box::Vector<double> reflectedValue(size_t x, size_t y);
        void transmittanceAt(size_t x, size_t y, double frequency, double& transX,
                             double& transY, bool reflected);
        void phaseShiftAt(size_t x, size_t y, double frequency, double& transX,
                          double& transY, bool reflected);
        const std::vector<ArraySensorPoint>& points() const { return points_; }
        void phaseAt(size_t x, size_t y, double frequency, double& transX,
                double& transY, bool reflected=false);
        void amplitudeAt(size_t x, size_t y, double frequency, double& transX,
                double& transY, bool reflected=false);
    protected:
        // Data
        DataSpec* attenuation2DSpec_{};
        DataSpec* phaseShift2DSpec_{};
        DataSpec* attenuationFront_{};
        DataSpec* phaseShiftFront_{};
        DataSpec* attenuationBack_{};
        DataSpec* phaseShiftBack_{};
        DataSpec* timeSeriesFront_{};
        DataSpec* timeSeriesBack_{};
        DataSpec* polarisation_{};
        DataSpec* skew_{};
        DataSpec* admittance_{};
        DataSpec* phaseDistribution_{};
        DataSpec* amplitudeDistribution_{};
        std::vector<ArraySensorPoint> points_;
        size_t nSamples_{0};   // The number of samples in the time series
        size_t reflectedZCell_{};   // The FDTD cell Z coordinate of the reflected sensorId
        size_t reflectedReferenceZCell_{};  // The FDTD cell Z coord of the reflected reference
        size_t transmittedZCell_{};  // The FDTD cell Z coordinate of the transmitted sensorId
        size_t numPointsX_{1};   // The number of sensorId points in the X direction
        size_t numPointsY_{1};   // The number of sensorId points in the Y direction
        double frequency_{1 * box::Constants::giga_};  // The frequency for the sensorId displays
        size_t channel_{0};  // The channel the frequency corresponds to
        size_t currentPointX_{0};  // The sensorId point with the current focus
        size_t currentPointY_{0};  // The sensorId point with the current focus
        box::FrequencySeq frequencies_;  // The sequence of frequencies to detect
        bool animateFrequency_{false};  // Set to true to animate the frequency
        double frequencyStep_{1 * box::Constants::giga_};  // The amount to step the frequency
        bool averageOverCell_{false};  // If true average over the whole sensorId cell
        double transmittedPosZ_{0.0};   // The location of the transmitted sensorId in the Z dir
        double reflectedPosZ_{0.0};  // The location of the reflected sensorId in the Z direction
        bool manualZPos_{false};  // Set to true for manual setting of the Z position
        bool manualSamples_{false};  // Set to true for manual setting of the number of samples
        bool fdtdUsed_{false};
        bool propagationMatrixUsed_{false};
        box::ComponentSel componentSel_;  // Which components are selected
        bool includeReflected_{};  // Include reflected data when storing
        double spacingX_{};  // The spacing of the points in the x direction
        double spacingY_{};  // The spacing of the points in the y direction
        bool useWindow_{};  // Use the Hann window function with spectrograms

        // Helper functions
        void calculateChannelNumber();
        void validatePoints();
    };

}


#endif //FDTDLIFE_ARRAYSENSOR_H
