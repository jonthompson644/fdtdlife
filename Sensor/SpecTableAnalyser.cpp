/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SpecTableAnalyser.h"

// Write the object to the XML DOM
void sensor::SpecTableAnalyser::writeConfig(xml::DomObject& p) const {
    // Base class first
    Analyser::writeConfig(p);
    // Now my stuff
    p << xml::Reopen();
    for(auto& point : points_) {
        p << xml::Obj("point") << point;
    }
    p << xml::Close();
}

// Read the object from the XML DOM
void sensor::SpecTableAnalyser::readConfig(xml::DomObject& p) {
    // Base class first
    Analyser::readConfig(p);
    // Now my stuff
    p >> xml::Reopen();
    points_.clear();
    for(auto& o : p.obj("point")) {
        points_.emplace_back();
        *o >> points_.back();
    }
    p >> xml::Close();
}

// Delete a point from the list
void sensor::SpecTableAnalyser::deletePoint(int row) {
    auto pos = std::next(points_.begin(), row);
    if(pos != points_.end()) {
        points_.erase(pos);
        points_.sort();
    }
}

// Add a point to the list
void sensor::SpecTableAnalyser::addPoint(double x, double min, double max) {
    points_.emplace_back(x, min, max);
    points_.sort();
}

// Set the parameters of a point
void sensor::SpecTableAnalyser::setPoint(int row, double x, double min, double max) {
    auto pos = std::next(points_.begin(), row);
    if(pos != points_.end()) {
        pos->x(x);
        pos->min(min);
        pos->max(max);
        points_.sort();
    }
}
