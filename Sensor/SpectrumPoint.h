/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SPECTRUMPOINT_H
#define FDTDLIFE_SPECTRUMPOINT_H

#include <Box/CircularBuffer.h>
#include <Box/GeortzelSpectrum.h>
#include <Box/FrequencySeq.h>
#include <Box/ComponentSel.h>

namespace sensor {

    // A class that collects a timeseries of the x,y components of
    // a wave travelling in a particular direction and generates
    // a spectrum for that wave over a given set of frequencies.
    class SpectrumPoint {
    public:
        // Construction
        SpectrumPoint();

        // API
        void initialise(const box::FrequencySeq& frequencies, size_t numThreadsToUse,
                        double samplePeriod, const box::ComponentSel& componentSel, 
                        size_t numSamples, bool useWindow);
        void collect(double x, double y, double z);
        void calculate();
        void calculate(size_t channel);
        void timeSeriesMinMax(double& min, double& max);
        void powerMinMax(double& min, double& max);
        void phaseMinMax(double& min, double& max);

        // Getters
        size_t numSamples() const {return numSamples_;}
        const box::CircularBuffer<double>& timeSeriesX() const {return timeSeriesX_;}
        const box::CircularBuffer<double>& timeSeriesY() const {return timeSeriesY_;}
        const box::CircularBuffer<double>& timeSeriesZ() const {return timeSeriesZ_;}
        const box::GeortzelSpectrum& spectrumX() const {return spectrumX_;}
        const box::GeortzelSpectrum& spectrumY() const {return spectrumY_;}
        const box::GeortzelSpectrum& spectrumZ() const {return spectrumZ_;}

    protected:
        // Members
        size_t numSamples_;  // The length of the time series
        box::CircularBuffer<double> timeSeriesX_;   // X component data
        box::CircularBuffer<double> timeSeriesY_;   // Y component data
        box::CircularBuffer<double> timeSeriesZ_;   // Z component data
        box::GeortzelSpectrum spectrumX_;  // The X component spectrum
        box::GeortzelSpectrum spectrumY_;  // The Y component spectrum
        box::GeortzelSpectrum spectrumZ_;  // The Z component spectrum
        box::ComponentSel componentSel_;  // Which components to calculate
    };
}


#endif //FDTDLIFE_SPECTRUMPOINT_H
