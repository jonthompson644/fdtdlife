/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_AREASENSOR_H
#define FDTDLIFE_AREASENSOR_H

#include "Sensor.h"
#include <Box/Constants.h>
#include <Box/FrequencySeq.h>
#include "SpectrumPoint.h"

namespace sensor {

    // A sensorId that measures the total signal in an area
    class AreaSensor : public Sensor {
    public:
        // Construction
        AreaSensor();

        // Overrides of Sensor
        void initialise() override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        void collect() override;
        void calculate() override;
        void getInfo(DataSpec *spec, DataSpec::Info *info) override;
        void getData(DataSpec* spec, size_t x, std::vector<double>& dy, double& dx) override;

        // API
        void at(double frequency, double& x, double& y);

        // Getters
        [[nodiscard]] const box::Vector<double>& center() const {return center_;}
        [[nodiscard]] box::Constants::Orientation orientation() const {return orientation_;}
        [[nodiscard]] double width() const {return width_;}
        [[nodiscard]] double height() const {return height_;}
        [[nodiscard]] const box::FrequencySeq& frequencies() const {return frequencies_;}
        [[nodiscard]] const box::Vector<size_t>& topLeftCell() const { return topLeftCell_; }
        [[nodiscard]] const box::Vector<size_t>& bottomRightCell() const { return bottomRightCell_; }
        [[nodiscard]] const SpectrumPoint point() const { return point_; }

        // Setters
        void center(const box::Vector<double>& v) {center_ = v;}
        void orientation(box::Constants::Orientation v) {orientation_ = v;}
        void width(double v) {width_ = v;}
        void height(double v) {height_ = v;}

    protected:
        // Configuration
        box::Vector<double> center_;   // The position of the center of the area
        box::Constants::Orientation orientation_{box::Constants::orientationXPlane};
        double width_{0.0};  // The width of the area
        double height_{0.0};  // The height of the area

        // Members
        box::Vector<size_t> topLeftCell_;  // The FDTD top left cell
        box::Vector<size_t> bottomRightCell_;  // The FDTD bottom right cell
        box::FrequencySeq frequencies_;  // The set of frequencies in the spectra
        SpectrumPoint point_;  // The time series and spectrum data for the area
        DataSpec* timeSeriesSpec_{};  // The definition of the time series
        DataSpec* attenuationSpec_{};  // The definition of the attenuation spectrum
        DataSpec* phaseShiftSpec_{};  // The definition of the phase shift spectrum
    };
}


#endif //FDTDLIFE_AREASENSOR_H
