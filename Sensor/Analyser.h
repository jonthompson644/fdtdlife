/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ANALYSER_H
#define FDTDLIFE_ANALYSER_H

#include <Box/Expression.h>
namespace fdtd { class Model; }
namespace xml { class DomObject; }

namespace sensor {

    class ArraySensor;

    // Base class for all analysers
    class Analyser {
    public:
        // Construction
        Analyser() = default;
        virtual void construct(int mode, int id, fdtd::Model* m);
        virtual ~Analyser() = default;

        // Types
        enum class SensorLocation {
            lowestZ, highestZ
        };
        enum class SensorComponent {
            x, y
        };
        enum class DataType {
            transmittance, phaseShift, amplitudeDistribution, phaseDistribution
        };

        // API
        virtual void initialise() {}
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        virtual void evaluate(box::Expression::Context& /*c*/) {}
        virtual void collect() {}
        virtual void calculate() {}
        virtual void stepComplete() {}
        friend xml::DomObject& operator<<(xml::DomObject& o, const Analyser& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Analyser& s) {
            s.readConfig(o);
            return o;
        }

        // Getters
        int sensorId() const { return sensorId_; }
        fdtd::Model* m() { return m_; }
        int mode() const {return mode_;}
        int identifier() const {return identifier_;}
        double error() const {return error_;}
        const std::string& name() const { return name_; }
        SensorLocation location() const { return location_; }
        SensorComponent component() const { return component_; }
        double minY() const { return minY_; }
        double maxY() const { return maxY_; }
        double minX() const { return minX_; }
        double stepX() const { return stepX_; }
        const std::vector<double>& data() const {return data_;}
        const std::vector<double>& reference() const {return reference_;}
        const std::vector<double>& delta() const {return delta_;}
        DataType dataType() const {return dataType_;}

        // Setters
        void sensorId(int v) { sensorId_ = v; }
        void name(const std::string& v) { name_ = v; }
        void location(SensorLocation v) { location_ = v; }
        void component(SensorComponent v) { component_ = v; }

    protected:
        // Configuration
        int mode_{-1};  // The type of the analyser
        int identifier_{-1};  // The identifier of the analyser
        int sensorId_{-1};   // The sensorId to use for the data
        double error_{};  // The calculated error from the ideal
        SensorLocation location_{SensorLocation::lowestZ};  // Which half of the sensorId
        SensorComponent component_{SensorComponent::x};  // Which sensorId component
        std::string name_;  // The name of the analyser

        // Other parameters
        fdtd::Model* m_{};  // The model
        DataType dataType_{};  // The type of data the analyser works on

        // Data
        std::vector<double> data_;  // The last data used
        std::vector<double> reference_;  // The reference data used
        std::vector<double> delta_;  // The allowed range around the reference (+/-)
        double minY_{};  // Minimum data value
        double maxY_{};  // Maximum data value
        double minX_{};  // First data point X
        double stepX_{};  // Size between data points

        // Helpers
        sensor::ArraySensor* getSensor();
    };
}


#endif //FDTDLIFE_ANALYSER_H
