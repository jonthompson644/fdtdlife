/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ReferencedSpectrum.h"
#include "SpectrumPoint.h"
#include <Box/Constants.h>
#include <Box/Vector.h>

// Constructor
sensor::ReferencedSpectrum::ReferencedSpectrum() :
        numChannels_(0) {
}

// Initialise the spectrum ready for a run
void sensor::ReferencedSpectrum::initialise(size_t numChannels,
                                            const box::ComponentSel& componentSel) {
    numChannels_ = numChannels;
    x_.initialise(numChannels);
    y_.initialise(numChannels);
    z_.initialise(numChannels);
    polarisation_.resize(numChannels);
    skew_.resize(numChannels);
    // Record which components
    componentSel_ = componentSel;
}

// Perform the calculations
void sensor::ReferencedSpectrum::calculate(const SpectrumPoint& data, const SpectrumPoint& reference) {
    // The components
    if(componentSel_.x()) {
        x_.calculate(data.spectrumX(), reference.spectrumX());
    }
    if(componentSel_.y()) {
        y_.calculate(data.spectrumY(), reference.spectrumY());
    }
    if(componentSel_.z()) {
        z_.calculate(data.spectrumZ(), reference.spectrumZ());
    }
    // The polarisation and skew
    if(componentSel_.all()) {
        for(size_t i = 0; i < numChannels_; i++) {
            otherCalculations(i, data.spectrumX()[i], data.spectrumY()[i], data.spectrumZ()[i]);
        }
    }
}

// Perform the other calculations
void sensor::ReferencedSpectrum::otherCalculations(size_t channel, const std::complex<double>& dataX,
                                                   const std::complex<double>& dataY,
                                                   const std::complex<double>& dataZ) {
    double dpol = atan2(dataY.real(), dataX.real()) / box::Constants::deg2rad_;
    if(dpol <= -90.0) { dpol += 180.0; }
    if(dpol > 90.0) { dpol -= 180.0; }
    double dmag = box::Vector<double>(std::abs(dataX), std::abs(dataY), std::abs(dataZ)).mag();
    double dskew = std::abs(dataZ) / dmag;
    polarisation_[channel] = dpol;
    skew_[channel] = dskew;
}

// Perform calculations for a single channel
void sensor::ReferencedSpectrum::calculate(size_t channel, const SpectrumPoint& data, const SpectrumPoint& reference) {
    const std::complex<double>& dataX = data.spectrumX()[channel];
    const std::complex<double>& dataY = data.spectrumY()[channel];
    const std::complex<double>& dataZ = data.spectrumZ()[channel];
    const std::complex<double>& referenceX = reference.spectrumX()[channel];
    const std::complex<double>& referenceY = reference.spectrumY()[channel];
    const std::complex<double>& referenceZ = reference.spectrumZ()[channel];
    // The components
    if(componentSel_.x()) {
        x_.calculate(channel, dataX, referenceX);
    }
    if(componentSel_.y()) {
        y_.calculate(channel, dataY, referenceY);
    }
    if(componentSel_.z()) {
        z_.calculate(channel, dataZ, referenceZ);
    }
    // The polarisation and skew
    if(componentSel_.all()) {
        otherCalculations(channel, dataX, dataY, dataZ);
    }
}

// Evaluate the ranges of the data
void sensor::ReferencedSpectrum::evaluateRanges() {
    x_.evaluateRanges();
    y_.evaluateRanges();
    z_.evaluateRanges();
}
