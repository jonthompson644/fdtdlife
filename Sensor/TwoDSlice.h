/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TWODSLICE_H
#define FDTDLIFE_TWODSLICE_H

#include "Sensor.h"
#include <Box/Constants.h>

namespace fdtd { class Model; }
namespace fdtd { class Configuration; }

namespace sensor {


    // A sensorId that captures a 2D slice from the model
    class TwoDSlice : public Sensor {

    public:
        // Types
        enum DataSource {
            eMagnitude, hMagnitude, ex, ey, ez, hx, hy, hz, material
        };

    public:
        // Construction
        TwoDSlice();
        ~TwoDSlice() override;
        // Methods
        void initialise() override;
        void initialCollect() override { collect(); }  // snapshot sensors collect at init
        void collect() override;
        size_t index(size_t sx, size_t sy) const;
        void getInfo(DataSpec* spec, DataSpec::Info* info) override;
        void getData(DataSpec* spec, size_t x, size_t y, double* d, double* dx,
                     double* dy) override;
        bool writeData(std::ofstream& file) override;
        bool readData(std::ifstream& file) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;

    protected:
        // Private functions
        void nullArrays();
        void clear();
        virtual double getValue(const box::Vector<size_t>& p);
        void findMinMax();

    protected:
        DataSpec sliceSpec_;
        // Configuration
        box::Constants::Orientation orientation_{box::Constants::orientationXPlane};
        double offset_{};  // Offset along the orientation axis
        DataSource dataSource_{eMagnitude};  // What to display in this view
        // Derived data
        size_t nSliceX_{};  // The size of the slice in the x...
        size_t nSliceY_{};  // ...and y directions
        size_t sliceOffset_{};  // The cell offset on the orientation axis
        double min_{};  // The minimum value
        double max_{};  // The maximum value
        double* data_{};  // The data

    public:
        // Getters
        box::Constants::Orientation orientation() const { return orientation_; }
        double offset() const { return offset_; }
        DataSource dataSource() const { return dataSource_; }
        double min() const { return min_; }
        double max() const { return max_; }
        size_t nSliceX() const { return nSliceX_; }
        size_t nSliceY() const { return nSliceY_; }
        double* data() { return data_; }
        size_t sliceOffset() const { return sliceOffset_; }
        //Setters
        void orientation(box::Constants::Orientation v) { orientation_ = v; }
        void offset(double v) { offset_ = v; }
        void dataSource(DataSource v) { dataSource_ = v; }
    };

}


#endif //FDTDLIFE_TWODSLICE_H
