/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Analysers.h"
#include "LensPhaseAnalyser.h"
#include "LensReflectionAnalyser.h"
#include "LensTransmissionAnalyser.h"
#include "TransmittanceAnalyser.h"
#include "PhaseShiftAnalyser.h"
#include "FilterAnalyser.h"
#include <Fdtd/Model.h>

// Constructor
sensor::Analysers::Analysers(fdtd::Model* m) :
        m_(m) {
    factory_.define(new box::Factory<Analyser>::Creator<LensPhaseAnalyser>(
            modeLensPhase, "Lens Phase"));
    factory_.define(new box::Factory<Analyser>::Creator<LensReflectionAnalyser>(
            modeLensReflection, "Lens Reflection"));
    factory_.define(new box::Factory<Analyser>::Creator<LensTransmissionAnalyser>(
            modeLensTransmission, "Lens Transmission"));
    factory_.define(new box::Factory<Analyser>::Creator<TransmittanceAnalyser>(
            modeTransmittance, "Transmittance"));
    factory_.define(new box::Factory<Analyser>::Creator<PhaseShiftAnalyser>(
            modePhaseShift, "Phase Shift"));
    factory_.define(new box::Factory<Analyser>::Creator<FilterAnalyser>(
            modeFilter, "Filter Characteristic"));
}

// Make an analyser from a DOM object
sensor::Analyser* sensor::Analysers::restore(xml::DomObject& o) {
    analysers_.emplace_back(factory_.restore(o, m_));
    return analysers_.back().get();
}

// Make an analyser given a mode
sensor::Analyser* sensor::Analysers::make(int mode) {
    analysers_.emplace_back(factory_.make(mode, m_));
    return analysers_.back().get();
}

// Return the designer with the given identifier
sensor::Analyser* sensor::Analysers::find(int identifier) {
    Analyser* result = nullptr;
    for(auto& d : analysers_) {
        if(d->identifier() == identifier) {
            result = d.get();
            break;
        }
    }
    return result;
}

// Remove an analyser from the list.
void sensor::Analysers::remove(sensor::Analyser* analyser) {
    analysers_.remove_if([analyser](std::unique_ptr<Analyser>& d) {
        return d.get() == analyser;
    });
}

// Write the configuration to the DOM
void sensor::Analysers::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& d : analysers_) {
        root << xml::Obj("analyser") << *d;
    }
    root << xml::Close();
}

// Read the configuration from the DOM
void sensor::Analysers::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    for(auto& o : root.obj("analyser")) {
        auto* d = restore(*o);
        *o >> *d;
    }
    root >> xml::Close();
}

// Clear the analysers
void sensor::Analysers::clear() {
    analysers_.clear();
}

// Initialise the analysers
void sensor::Analysers::initialise() {
    box::Expression::Context c;
    m_->variables().fillContext(c);
    for(auto& a : analysers_) {
        a->initialise();
        a->evaluate(c);
    }
}

// Calculate the analysers
void sensor::Analysers::calculate() {
    for(auto& a : analysers_) {
        a->collect();
        a->calculate();
    }
    m_->doNotification(fdtd::Model::notifyAnalysersCalculate);
}

// A processing step has completed
void sensor::Analysers::stepComplete() {
    for(auto& a : analysers_) {
        a->stepComplete();
    }
}
