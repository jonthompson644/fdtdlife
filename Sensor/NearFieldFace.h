/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_NEARFIELDFACE_H
#define FDTDLIFE_NEARFIELDFACE_H

#include <Box/Vector.h>
#include "NearFieldCell.h"

namespace fdtd {class Model;}

namespace sensor {

    // A class that represents one face of the near field surface
    class NearFieldFace {
    public:
        // Construction
        NearFieldFace() = default;
        // API
        void initialise(const box::Vector<size_t>& p1, const box::Vector<size_t>& p2, size_t nSamples);
        void collect(fdtd::Model* m);
        void calculate();
        double getEMag(size_t x, size_t y);
        size_t width() {return w_;}
        size_t height() {return h_;}
        bool isAt(const box::Vector<size_t>& pos);
    protected:
        // Data
        box::Vector<size_t> p1_;
        box::Vector<size_t> p2_;
        size_t w_ {};
        size_t h_ {};
        enum {xFace, yFace, zFace} face_ {};
        std::vector<std::vector<NearFieldCell>> cells_;
    };

}


#endif //FDTDLIFE_NEARFIELDFACE_H
