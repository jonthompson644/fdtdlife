/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_RADIATIONPATTERNPOINT_H
#define FDTDLIFE_RADIATIONPATTERNPOINT_H

#include <vector>
#include <Box/Vector.h>
#include <Box/FrequencySeq.h>
#include "SpectrumPoint.h"

namespace fdtd { class Model; }

namespace sensor {

    // A collection point on the radiation pattern sensorId semi-circle
    class RadiationPatternPoint {
    public:
        // Construction
        RadiationPatternPoint() = default;

        // API
        void initialise(const box::Vector<size_t>& cellPosition,
                        const box::FrequencySeq& frequencies, size_t numThreadsToUse,
                        double samplePeriod, size_t numSamples);
        bool isAt(const box::Vector<size_t>& pos);
        void collect(fdtd::Model* m);
        void getMinMax(double& min, double& max);
        void calculate();
        void powerAt(size_t spectrumBin, double polarisation, double& power, double& crossPower);

        // Getters
        const box::Vector<size_t>& cellPosition() const { return cellPosition_; }
        const SpectrumPoint& point() const { return point_; }

    protected:
        // Members
        SpectrumPoint point_;  // The time series and their spectra
        box::Vector<size_t> cellPosition_;   // Cell position of this point
        box::FrequencySeq frequencies_;   // The frequencies in the spectrum
    };

}


#endif //FDTDLIFE_RADIATIONPATTERNPOINT_H
