/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "NearFieldCell.h"
#include <Fdtd/Model.h>
#include <Fdtd/EField.h>
#include <Fdtd/HField.h>

// Constructor
sensor::NearFieldCell::NearFieldCell() :
        needsClear_(true) {
}

// Copy constructor
sensor::NearFieldCell::NearFieldCell(const sensor::NearFieldCell& other) :
        needsClear_(true) {
    *this = other;
}

// Destructor
sensor::NearFieldCell::~NearFieldCell() {
    clearMemory();
}

// Delete allocated memory
void sensor::NearFieldCell::clearMemory() {
    delete[] timeSeriesEPrevX_;
    delete[] timeSeriesEX_;
    delete[] timeSeriesHX_;
    delete[] timeSeriesEPrevY_;
    delete[] timeSeriesEY_;
    delete[] timeSeriesHY_;
    delete[] timeSeriesEPrevZ_;
    delete[] timeSeriesEZ_;
    delete[] timeSeriesHZ_;
    timeSeriesEPrevX_ = nullptr;
    timeSeriesEX_ = nullptr;
    timeSeriesHX_ = nullptr;
    timeSeriesEPrevY_ = nullptr;
    timeSeriesEY_ = nullptr;
    timeSeriesHY_ = nullptr;
    timeSeriesEPrevZ_ = nullptr;
    timeSeriesEZ_ = nullptr;
    timeSeriesHZ_ = nullptr;
}

// Assignment operator
sensor::NearFieldCell& sensor::NearFieldCell::operator=(const NearFieldCell& /*other*/) {
    // We don't need to copy anything, assignments only take place before
    // the call to initialise
    return *this;
}

// Initialise ready for a run
void sensor::NearFieldCell::initialise(const box::Vector<size_t>& pos, size_t nSamples) {
    nSamples_ = nSamples;
    nextSample_ = 0;
    prevSample_ = 0;
    pos_ = pos;
    clearMemory();
    timeSeriesEPrevX_ = new double[nSamples_];
    timeSeriesEX_ = new double[nSamples_];
    timeSeriesHX_ = new double[nSamples_];
    timeSeriesEPrevY_ = new double[nSamples_];
    timeSeriesEY_ = new double[nSamples_];
    timeSeriesHY_ = new double[nSamples_];
    timeSeriesEPrevZ_ = new double[nSamples_];
    timeSeriesEZ_ = new double[nSamples_];
    timeSeriesHZ_ = new double[nSamples_];
    needsClear_ = true;
}

// Collect data
void sensor::NearFieldCell::collect(fdtd::Model* m) {
    // Do we need to clear the memory
    if(needsClear_) {
        memset(timeSeriesEPrevX_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesEX_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesHX_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesEPrevY_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesEY_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesHY_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesEPrevZ_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesEZ_, 0, sizeof(double)*nSamples_);
        memset(timeSeriesHZ_, 0, sizeof(double)*nSamples_);
    }
    // The H field
    box::Vector<double> h000 = m->h()->value(pos_);
    box::Vector<double> h001 = m->h()->value(pos_+box::Vector<size_t>(0,0,1));
    box::Vector<double> h010 = m->h()->value(pos_+box::Vector<size_t>(0,1,0));
    box::Vector<double> h011 = m->h()->value(pos_+box::Vector<size_t>(0,1,1));
    box::Vector<double> h100 = m->h()->value(pos_+box::Vector<size_t>(1,0,0));
    box::Vector<double> h101 = m->h()->value(pos_+box::Vector<size_t>(1,0,1));
    box::Vector<double> h110 = m->h()->value(pos_+box::Vector<size_t>(1,1,0));
    timeSeriesHX_[nextSample_] = (h000.x() + h001.x() + h010.x() + h011.x()) / 4.0;
    timeSeriesHY_[nextSample_] = (h000.y() + h100.y() + h001.y() + h101.y()) / 4.0;
    timeSeriesHZ_[nextSample_] = (h000.z() + h100.z() + h010.z() + h110.z()) / 4.0;
    // The E field
    box::Vector<double>e000 = m->e()->value(pos_);
    box::Vector<double>e100 = m->e()->value(pos_+box::Vector<size_t>(1,0,0));
    box::Vector<double>e010 = m->e()->value(pos_+box::Vector<size_t>(0,1,0));
    box::Vector<double>e001 = m->e()->value(pos_+box::Vector<size_t>(0,0,1));
    timeSeriesEX_[nextSample_] = (timeSeriesEPrevX_[prevSample_] + e000.x() + e100.x()) / 4.0;
    timeSeriesEY_[nextSample_] = (timeSeriesEPrevY_[prevSample_] + e000.y() + e010.y()) / 4.0;
    timeSeriesEZ_[nextSample_] = (timeSeriesEPrevZ_[prevSample_] + e000.z() + e001.z()) / 4.0;
    // The previous E field for the next sample
    timeSeriesEPrevX_[nextSample_] = e000.x() + e100.x();
    timeSeriesEPrevY_[nextSample_] = e000.y() + e010.y();
    timeSeriesEPrevZ_[nextSample_] = e000.z() + e001.z();
    // The sample here:
    prevSample_ = nextSample_;
    nextSample_ = (nextSample_ + 1) % nSamples_;
}

// Return the electric field magnitude
double sensor::NearFieldCell::getEMag() {
    double result = 0.0;
    if(!needsClear_) {
        result = box::Vector<double>(timeSeriesEX_[prevSample_], timeSeriesEY_[prevSample_],
                                     timeSeriesEZ_[prevSample_]).signedMag();
    }
    return result;
}

