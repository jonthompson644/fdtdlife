/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensTransmissionAnalyser.h"
#include "ArraySensor.h"
#include <Fdtd/Model.h>

// Constructor
sensor::LensTransmissionAnalyser::LensTransmissionAnalyser() {
    dataType_ = DataType::amplitudeDistribution;
}

// Write the configuration to the XML DOM
void sensor::LensTransmissionAnalyser::writeConfig(xml::DomObject& root) const {
    // Base class first
    Analyser::writeConfig(root);
    // Now me
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Close();
}

// Read the configuration from the XML DOM
void sensor::LensTransmissionAnalyser::readConfig(xml::DomObject& root) {
    // Base class first
    Analyser::readConfig(root);
    // Now me
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Close();
}

// Evaluate any expressions
void sensor::LensTransmissionAnalyser::evaluate(box::Expression::Context& c) {
    // Base class first
    Analyser::evaluate(c);
    // Now my stuff
    frequency_.evaluate(c);
}

// Collect data from the sensorId
void sensor::LensTransmissionAnalyser::collect() {
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        // Get the data from the sensorId
        size_t numPoints = sensor->numPointsX();
        data_.resize(numPoints);
        reference_.resize(numPoints);
        if(sensor->points().size() >= numPoints) {
            minX_ = sensor->points()[sensor->index(0, 0)].center().x();
            double maxX = minX_;
            if(numPoints > 0) {
                maxX = sensor->points()[sensor->index(numPoints - 1, 0)].center().x();
            }
            stepX_ = 0.0;
            if(numPoints > 1) {
                stepX_ = (maxX - minX_) / static_cast<double>(numPoints - 1);
            }
            // Record the data
            double ax = 0.0;
            double ay = 0.0;
            for(size_t i = 0; i < numPoints; i++) {
                // Get the point
                sensor->amplitudeAt(i, 0, frequency_.value(), ax, ay,
                                    location_ != SensorLocation::highestZ);
                double measured = component_ == SensorComponent::y ? ay : ax;
                data_[i] = measured;
            }
            // Sensor x spacing
            xSpacing_ = sensor->spacingX();
        }
    }
}

// Calculate the error using the current state of the sensorId
void sensor::LensTransmissionAnalyser::calculate() {
    error_ = 0.0;
    size_t numPoints = data_.size();
    for(size_t i = 0; i < numPoints; i++) {
        // Get the point
        double measured = data_[i];
        // Calculate the min/max
        if(i == 0) {
            minY_ = measured;
            maxY_ = measured;
        } else {
            minY_ = std::min(minY_, measured);
            maxY_ = std::max(maxY_, measured);
        }
    }
    // Calculate the gaussian fit parameters, a and c (b is zero)
    gaussianA_ = maxY_;
    double beforeY = 0.0;
    double afterY = 0.0;
    double halfWidth = static_cast<double>(numPoints) * xSpacing_ / 2.0;
    double afterX = -halfWidth;
    for(size_t i = 0; i < numPoints; i++) {
        afterY = data_[i];
        if(afterY >= gaussianA_ / 2.0) {
            break;
        }
        beforeY = afterY;
        afterX += xSpacing_;
    }
    double xPerY = xSpacing_ / (afterY - beforeY);
    double xAtHalfMax = afterX - (afterY - gaussianA_ / 2) * xPerY;
    double fwhm = -2.0 * xAtHalfMax;
    gaussianC_ = fwhm / 2.35482;
    // Generate the fitted gaussian reference and calculate the unfitness
    double x = -halfWidth;
    for(size_t i = 0; i < numPoints; i++) {
        double reference = gaussianA_ / exp((x * x) / (2 * gaussianC_ * gaussianC_));
        double measured = data_[i];
        reference_[i] = reference;
        double diff = reference - measured;
        error_ += (diff * diff);
        x += xSpacing_;
    }
}

// Set the data as though it was collected from the sensorId.
// Used mainly by the test suite
void sensor::LensTransmissionAnalyser::setData(const std::vector<double>& data,
                                               double xSpacing) {
    // Calculate metadata
    size_t numPoints = data.size();
    data_.resize(numPoints);
    reference_.resize(numPoints);
    stepX_ = (m_->p()->p2().x().value() - m_->p()->p1().x().value()) / numPoints;
    minX_ = m_->p()->p1().x().value() + stepX_ / 2;
    // Record the data
    data_ = data;
    xSpacing_ = xSpacing;
    // Now calculate using this data
    calculate();
    m_->doNotification(fdtd::Model::notifyAnalysersCalculate);
}
