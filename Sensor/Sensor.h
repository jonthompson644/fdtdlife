/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SENSOR_H
#define FDTDLIFE_SENSOR_H

#include <Box/Configurable.h>
#include <Fdtd/Configuration.h>
#include "DataSpec.h"
#include <Box/Constants.h>
#include <Xml/DomObject.h>
#include <list>
#include <iostream>
#include <fstream>

namespace xml { class DomObject; }
namespace fdtd { class Model; }

namespace sensor {

    // Base class for all sensors
    class Sensor : public box::Configurable {
    public:
        // Construction
        Sensor() = default;
        virtual void construct(int mode, int identifier, fdtd::Model* m);
        virtual ~Sensor() = default;

        // Methods
        virtual void initialise() {}
        virtual void initialCollect() {}
        virtual void finalCalculate() {}
        virtual void collect() {}
        virtual void calculate() {}
        virtual void getInfo(DataSpec* /*spec*/, DataSpec::Info* /*info*/) {}
        virtual void getData(DataSpec* /*spec*/, size_t /*x*/, std::vector<double>& /*dx*/,
                             double& /*dx*/) {}
        virtual void getData(DataSpec* /*spec*/, size_t /*x*/, size_t /*y*/, double* /*d*/,
                             double* /*dx*/, double* /*dy*/) {}
        virtual void getMetaData(DataSpec* /*spec*/, int /*id*/, double* /*d*/,
                                 std::string& /*units*/) {}
        virtual void setMetaData(DataSpec* /*spec*/, int /*id*/, double /*d1*/,
                                 double /*d2*/) {}
        virtual bool isAt(const box::Vector<size_t>& /*pos*/) { return false; }
        virtual void writeCsvData(std::ofstream* /*csvFile*/) {}
        virtual bool writeData(std::ofstream& /*file*/) { return false; }
        virtual bool readData(std::ifstream& /*file*/) { return false; }
        virtual void animate() {}
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Sensor& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Sensor& s) {
            s.readConfig(o);
            return o;
        }
        DataSpec* getDataSpec(const std::string& data);

        // Getters
        [[nodiscard]] int identifier() const { return identifier_; }
        [[nodiscard]] const std::string& name() const { return name_; }
        [[nodiscard]] box::Constants::Colour colour() const { return colour_; }
        [[nodiscard]] const std::list<DataSpec*>& dataSpecs() const { return specs_; }
        [[nodiscard]] bool saveCsvData() const { return saveCsvData_; }
        [[nodiscard]] int mode() const { return mode_; }
        [[nodiscard]] fdtd::Model* m() const { return m_; }
        [[nodiscard]] bool seqGenerated() const { return seqGenerated_; }

        // Setters
        void name(const std::string& v) { name_ = v; }
        void colour(box::Constants::Colour v) { colour_ = v; }
        void saveCsvData(bool v) { saveCsvData_ = v; }
        void seqGenerated(bool v) { seqGenerated_ = v; }

    protected:
        // Members
        fdtd::Model* m_{};  // The model
        int mode_{};  // Which kind of sensorId
        std::list<DataSpec*> specs_;
        int identifier_{-1};  // The sensorId identifier
        std::string name_;  // The name of the sensorId
        box::Constants::Colour colour_{};  // The color to show on the domain display
        bool saveCsvData_{};  // Save data to a CSV file at the end of a run
        bool seqGenerated_{};  // This material was generated by a sequencer
    };
}


#endif //FDTDLIFE_SENSOR_H
