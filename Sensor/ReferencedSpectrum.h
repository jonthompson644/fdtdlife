/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_REFERENCEDSPECTRUM_H
#define FDTDLIFE_REFERENCEDSPECTRUM_H

#include "ReferencedComponent.h"
#include <Box/ComponentSel.h>

namespace sensor {

    class SpectrumPoint;

    // A class that processes three referenced components
    class ReferencedSpectrum {
    public:
        // Constructor
        ReferencedSpectrum();

        // API
        void initialise(size_t numChannels, const box::ComponentSel& componentSel);
        void calculate(const SpectrumPoint& data, const SpectrumPoint& reference);
        void calculate(size_t channel, const SpectrumPoint& data, const SpectrumPoint& reference);
        void evaluateRanges();

        // Getters
        const ReferencedComponent& x() const {return x_;}
        const ReferencedComponent& y() const {return y_;}
        const ReferencedComponent& z() const {return z_;}
        const std::vector<double> polarisation() const {return polarisation_;}
        const std::vector<double> skew() const {return skew_;}
        size_t numChannels() const {return numChannels_;}

    protected:
        // Members
        ReferencedComponent x_;
        ReferencedComponent y_;
        ReferencedComponent z_;
        std::vector<double> polarisation_;
        std::vector<double> skew_;
        size_t numChannels_;
        box::ComponentSel componentSel_;

        // Helpers
        void otherCalculations(size_t channel, const std::complex<double>& x,
                               const std::complex<double>& y, const std::complex<double>& z);
    };

}


#endif //FDTDLIFE_REFERENCEDSPECTRUM_H
