/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Sensors.h"
#include "Sensor.h"
#include "ArraySensor.h"
#include "TwoDSlice.h"
#include "MaterialSensor.h"
#include "FarFieldSensor.h"
#include "RadiationPattern.h"
#include "AreaSensor.h"
#include <Xml/DomObject.h>
#include <Fdtd/Model.h>

// Constructor
sensor::Sensors::Sensors(fdtd::Model* m) :
        m_(m) {
    // Initialise the shape factory
    factory_.define(new box::Factory<Sensor>::Creator<TwoDSlice>(
            modeTwoD, "2D Slice"));
    factory_.define(new box::Factory<Sensor>::Creator<ArraySensor>(
            modeArraySensor, "Array Sensor"));
    factory_.define(new box::Factory<Sensor>::Creator<MaterialSensor>(
            modeMaterial, "2D Material"));
    factory_.define(new box::Factory<Sensor>::Creator<FarFieldSensor>(
            modeFarFieldSensor, "Far Field"));
    factory_.define(new box::Factory<Sensor>::Creator<ArraySensor>(
            modeGeortzelDft, "Georztel DFT"));  // Obsolete
    factory_.define(new box::Factory<Sensor>::Creator<RadiationPattern>(
            modeRadiationPattern, "Radiation Pattern"));
    factory_.define(new box::Factory<Sensor>::Creator<AreaSensor>(
            modeArea, "Area Sensor"));
}

// Clear all the sensors
void sensor::Sensors::clear() {
    sensors_.clear();
    factory_.clear();
}

// Make a shape of the specified type
sensor::Sensor* sensor::Sensors::makeSensor(int mode) {
    sensors_.emplace_back(factory_.make(mode, m_));
    return sensors_.back().get();
}

// Make a shape of the specified type using a given identifier
sensor::Sensor* sensor::Sensors::makeSensor(int id, int mode) {
    sensors_.emplace_back(factory_.createOne(id, mode, m_));
    return sensors_.back().get();
}

// Make a shape using the given XML DOM but using a new identifier
sensor::Sensor* sensor::Sensors::makeSensor(xml::DomObject& o) {
    sensors_.emplace_back(factory_.duplicate(o, m_));
    return sensors_.back().get();
}

// Remove a sensorId from the controlled list.
void sensor::Sensors::remove(sensor::Sensor* sensor) {
    auto pos = std::find_if(sensors_.begin(), sensors_.end(),
            [sensor](std::unique_ptr<Sensor>& s){return s.get() == sensor;});
    if(pos != sensors_.end()) {
        sensors_.erase(pos);
    }
}

// Initialise all the sensors
void sensor::Sensors::initialise() {
    for(auto& sensor: sensors_) {
        sensor->initialise();
    }
}

// Collect data into the sensors
void sensor::Sensors::collect() {
    for(auto& sensor: sensors_) {
        sensor->collect();
    }
}

// Make the collect during initialisation for all sensors
void sensor::Sensors::initialCollect() {
    for(auto& sensor: sensors_) {
        sensor->initialCollect();
    }
}

// Make the final calculation when the model stops
void sensor::Sensors::finalCalculate() {
    for(auto& sensor: sensors_) {
        sensor->finalCalculate();
    }
}

// Perform calculations for all sensors
void sensor::Sensors::calculate() {
    for(auto& sensor: sensors_) {
        sensor->calculate();
    }
}

// Return the sensorId at a specific location
sensor::Sensor* sensor::Sensors::getAt(const box::Vector<size_t>& p) {
    Sensor* result = nullptr;
    for(auto& sensor : sensors_) {
        if(sensor->isAt(p)) {
            result = sensor.get();
        }
    }
    return result;
}

// Return a data specification for a sensorId
sensor::DataSpec* sensor::Sensors::getDataSpec(int identifier, const std::string& data) {
    DataSpec* result = nullptr;
    Sensor* sensor = find(identifier);
    if(sensor != nullptr) {
        for(auto& spec : sensor->dataSpecs()) {
            if(spec->name() == data) {
                result = spec;
            }
        }
    }
    return result;
}

// Return the sensorId with the given identifier
sensor::Sensor* sensor::Sensors::find(int identifier) {
    sensor::Sensor* result = nullptr;
    for(auto& s : sensors_) {
        if(s->identifier() == identifier) {
            result = s.get();
            break;
        }
    }
    return result;
}

// Return the sensorId with the given name
sensor::Sensor* sensor::Sensors::find(const std::string& name) {
    sensor::Sensor* result = nullptr;
    for(auto& s : sensors_) {
        if(s->name() == name) {
            result = s.get();
            break;
        }
    }
    return result;
}

// Return a list of array sensors.
// TODO: This function should not be necessary, remove.
void sensor::Sensors::getArraySensors(std::list<sensor::ArraySensor*>& arraySensors) {
    for(auto& s : sensors_) {
        auto t = dynamic_cast<sensor::ArraySensor*>(s.get());
        if(t != nullptr) {
            arraySensors.push_back(t);
        }
    }
}

// Write the binary data of all sensors
bool sensor::Sensors::writeData(std::ofstream& file) {
    bool result = true;
    for(auto& sensor : sensors_) {
        result = result && sensor->writeData(file);
    }
    return result;
}

// Read the binary data of all sensors
bool sensor::Sensors::readData(std::ifstream& file) {
    bool result = true;
    for(auto& sensor : sensors_) {
        result = result && sensor->readData(file);
    }
    return result;
}

// Animate the sensors
void sensor::Sensors::animate() {
    for(auto& sensor: sensors_) {
        sensor->animate();
    }
}

// Write sensorId configuration to an XML DOM
void sensor::Sensors::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& s : sensors_) {
        root << xml::Obj("sensorId") << *s;
    }
    root << xml::Close();
}

// Read sensorId configuration from an XML DOM
void sensor::Sensors::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    for(auto p : root.obj("sensorId")) {
        sensors_.emplace_back(factory_.restore(*p, m_));
        *p >> *sensors_.back();
    }
    root >> xml::Close();
}

// Return true if any of the sensors want to write CSV data
bool sensor::Sensors::wantsCsv() {
    bool result = false;
    for(auto& sensor : sensors_) {
        result = result || sensor->saveCsvData();
    }
    return result;
}

// Write out any CSV data
void sensor::Sensors::writeCsv(std::ofstream* file) {
    for(auto& sensor : sensors_) {
        if(sensor->saveCsvData()) {
            sensor->writeCsvData(file);
        }
    }
}

// Clear sensors created by a sequencer
void sensor::Sensors::clearSeqGenerated() {
    auto pos = sensors_.begin();
    while(pos != sensors_.end()) {
        auto next = std::next(pos);
        if((*pos)->seqGenerated()) {
            sensors_.erase(pos);
        }
        pos = next;
    }
}

