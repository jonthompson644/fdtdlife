#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Sensor
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AreaSensor.cpp \
    ArraySensor.cpp \
    ArraySensorPoint.cpp \
    DataSpec.cpp \
    FarFieldSensor.cpp \
    MaterialSensor.cpp \
    NearFieldCell.cpp \
    NearFieldFace.cpp \
    RadiationPattern.cpp \
    RadiationPatternPoint.cpp \
    Sensor.cpp \
    SensorFactory.cpp \
    SensorUser.cpp \
    SpectrumPoint.cpp \
    TwoDSlice.cpp \
    ReferencedComponent.cpp \
    ReferencedSpectrum.cpp \
    Sensors.cpp

HEADERS += \
    AreaSensor.h \
    ArraySensor.h \
    ArraySensorPoint.h \
    DataSpec.h \
    FarFieldSensor.h \
    MaterialSensor.h \
    NearFieldCell.h \
    NearFieldFace.h \
    RadiationPattern.h \
    RadiationPatternPoint.h \
    Sensor.h \
    SensorFactory.h \
    SensorUser.h \
    SpectrumPoint.h \
    TwoDSlice.h \
    ReferencedComponent.h \
    ReferencedSpectrum.h \
    Sensors.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Sensor.pro

DISTFILES += \
    Sensor.pro.user \
    CMakeLists.txt \
    Makefile
