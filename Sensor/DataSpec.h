/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DATASPEC_H
#define FDTDLIFE_DATASPEC_H

#include <string>
#include <list>
#include <vector>

// A class that defines data a sensorId can provide
namespace sensor {

    class SensorUser;
    class Sensor;
    class DataSpec {

    public:
        enum Mode {modeNone=-1, modeTimeSeries=0, modeTwoD,
            modeAttenuation, modeSensorArray,
            modePhaseShift, modeMaterial, modePower};
        enum Meta {metaFrequency=100, metaCurrentXYPos, metaMinimum, metaMaximum,
            metaSeriesName0, metaSeriesName1, metaSeriesName2, metaSeriesName3};
            // Note that the metaSeriesNameX symbols must be at the end and
            // kept in sequence as they are generated mathematically.
        struct Info {
            Info() : min_(0.0), max_(0.0), xSize_(0), ySize_(0), useScaleCfg_(false),
                     xLeft_(0.0), xRight_(0.0), zPos_(0) {}
            double min_;
            double max_;
            size_t xSize_;
            size_t ySize_;
            std::string xUnits_;
            std::string yUnits_;
            bool useScaleCfg_;
            double xLeft_;
            double xRight_;
            int zPos_;
            std::vector<bool> channelUsed_;
            bool channelUsed(size_t chan) {return (chan < channelUsed_.size()) ? channelUsed_[chan] : true;}
        };


    public:
        DataSpec(Sensor* sensor, const char* name, Mode mode,
                const std::vector<std::string>& channelNames);
        ~DataSpec();
        void getInfo(Info* info);
        void getData(size_t x, std::vector<double>& dy, double& dx);
        void getData(size_t x, size_t y, double* d, double* dx, double* dy);
        void getMetaData(int id, double* d, std::string& units);
        void setMetaData(int id, double d1, double d2);
        void addUser(SensorUser* user);
        void removeUser(SensorUser* user);

    private:
        std::string name_;   // Display name of the data
        Mode mode_;  // The data display mode
        std::vector<std::string> channelNames_;  // The names of the channels
        Sensor* sensor_;  // The sensorId
        std::list<SensorUser*> users_;  // The sensorId users

    public:
        // Getters
        Mode mode() const {return mode_;}
        size_t numChannels() const {return channelNames_.size();}
        const std::string& name() const {return name_;}
        Sensor* sensor() {return sensor_;}
        const std::string& channelName(size_t i) const {return channelNames_[i];}
    };
}



#endif //FDTDLIFE_DATASPEC_H
