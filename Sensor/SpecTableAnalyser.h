/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SPECTABLEANALYSER_H
#define FDTDLIFE_SPECTABLEANALYSER_H

#include <list>
#include <Xml/DomObject.h>
#include <Box/Expression.h>
#include "Analyser.h"

namespace sensor {

    // A base class that provides a table of points for analysers.
    class SpecTableAnalyser : public Analyser {
    public:
        // Types
        class Point {
        public:
            // Construction
            Point(double x, double min, double max) :
                    x_(x),
                    min_(min),
                    max_(max) {}
            Point() = default;

            // Getters
            double x() const { return x_; }
            double min() const { return min_; }
            double max() const { return max_; }

            // Setters
            void x(double v) { x_ = v; }
            void min(double v) { min_ = v; }
            void max(double v) { max_ = v; }

            // API
            void writeConfig(xml::DomObject& root) const {
                root << xml::Open();
                root << xml::Obj("x") << x_;
                root << xml::Obj("min") << min_;
                root << xml::Obj("max") << max_;
                root << xml::Close();
            }
            void readConfig(xml::DomObject& root) {
                root >> xml::Open();
                root >> xml::Obj("x") >> x_;
                root >> xml::Obj("min") >> min_;
                root >> xml::Obj("max") >> max_;
                root >> xml::Close();
            }
            friend xml::DomObject& operator<<(xml::DomObject& o, const Point& v) {
                v.writeConfig(o);
                return o;
            }
            friend xml::DomObject& operator>>(xml::DomObject& o, Point& v) {
                v.readConfig(o);
                return o;
            }
            bool operator<(const Point& other) const {
                return x_ < other.x_;
            }

        protected:
            // Members
            double x_;
            double min_;
            double max_;
        };

        // Construction
        SpecTableAnalyser() = default;

        // Overrides of Analyser
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;

        // API
        void addPoint(double x, double min, double max);
        void deletePoint(int row);
        void setPoint(int row, double x, double min, double max);

        // Getters
        std::list<Point>& points() { return points_; }

    protected:
        // Parameters
        std::list<Point> points_;   // The specification points
    };

}


#endif //FDTDLIFE_SPECTABLEANALYSER_H
