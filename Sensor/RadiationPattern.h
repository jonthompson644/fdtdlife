/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_RADIATIONPATTERN_H
#define FDTDLIFE_RADIATIONPATTERN_H

#include "Sensor.h"
#include "RadiationPatternPoint.h"
#include <Box/FrequencySeq.h>

namespace sensor {

    // A sensorId that measures the radiation pattern around
    // a point a specified distance in the Y plane.
    class RadiationPattern : public Sensor {
    public:
        // Construction
        RadiationPattern();

        // Overrides of Sensor
        void initialise() override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        void collect() override;
        void getInfo(DataSpec *spec, DataSpec::Info *info) override;
        void getData(DataSpec* spec, size_t x, std::vector<double>& dy, double& dx) override;
        void calculate() override;

        // Getters
        bool positiveSemicircle() const {return positiveSemicircle_;}
        const box::Vector<double>& center() const {return center_;}
        double distance() const {return distance_;}
        size_t numPoints() const {return numPoints_;}
        const std::vector<RadiationPatternPoint>& points() const {return points_;}
        const box::Vector<size_t>& centerCell() const {return centerCell_;}
        size_t numSamples() const {return numSamples_;}
        double patternFrequency() const {return patternFrequency_;}
        double patternPolarisation() const {return patternPolarisation_;}
        const box::FrequencySeq& frequencies() const {return frequencies_;}
        const std::vector<double>& coPattern() const {return coPattern_;}
        const std::vector<double>& crossPattern() const {return crossPattern_;}

        // Setters
        void positiveSemicircle(bool v) {positiveSemicircle_ = v;}
        void center(const box::Vector<double>& v) {center_ = v;}
        void distance(double v) {distance_ = v;}
        void numPoints(size_t v) {numPoints_ = v;}
        void patternFrequency(double v);
        void patternPolarisation(double v) {patternPolarisation_ = v;}

    protected:
        // Configuration
        bool positiveSemicircle_{true};  // The sensorId semicircle is in the +ve z direction
        box::Vector<double> center_;   // The position of the center of the circle
        double distance_{0.001};   // The distance from the center of the sensors
        size_t numPoints_{};   // The number of points on the semi-circle to sense
        double patternFrequency_{1 * box::Constants::giga_};  // The frequency at which to calc
        double patternPolarisation_{};  // The polarisation at which to calculate

        // Members
        std::vector<RadiationPatternPoint> points_;
        box::Vector<size_t> centerCell_;  // The cell coordinate of the center
        size_t numSamples_{};  // The length of the time series
        DataSpec* timeSeriesSpec_{};  // The definition of the time series
        DataSpec* radiationPatternSpec_{};  // The definition of the time series
        size_t patternSpectrumBin_{};  // The spectrum bin for the radiation pattern
        box::FrequencySeq frequencies_;  // The set of frequencies in the spectrum
        std::vector<double> coPattern_;  // The calc radiation pattern at the polarisation
        std::vector<double> crossPattern_;  // The calc radiation pattern at the cross pol

        // Helpers
        void getPatternMinMax(double& min, double& max);
        void setPatternSpectrumBin();
        void assemblePatterns();
    };

}


#endif //FDTDLIFE_RADIATIONPATTERN_H
