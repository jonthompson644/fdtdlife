/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DataSpec.h"
#include "SensorUser.h"
#include "Sensor.h"

// Constructor
sensor::DataSpec::DataSpec(Sensor* sensor, const char* name, Mode mode,
                           const std::vector<std::string>& channelNames) :
        name_(name),
        mode_(mode),
        channelNames_(channelNames),
        sensor_(sensor) {
}

// Destructor
sensor::DataSpec::~DataSpec() {
    for(auto user : users_) {
        user->removeSpec(this);
    }
}

// Find the range of the data
void sensor::DataSpec::getInfo(Info* info) {
    sensor_->getInfo(this, info);
}

// Get a data point
void sensor::DataSpec::getData(size_t x, std::vector<double>& dy, double& dx) {
    sensor_->getData(this, x, dy, dx);
}

// Get a data point
void sensor::DataSpec::getData(size_t x, size_t y, double* d, double* dx, double* dy) {
    sensor_->getData(this, x, y, d, dx, dy);
}

// Get a meta data item
void sensor::DataSpec::getMetaData(int id, double* d, std::string& units) {
    sensor_->getMetaData(this, id, d, units);
}

// Set a meta data item
void sensor::DataSpec::setMetaData(int id, double d1, double d2) {
    sensor_->setMetaData(this, id, d1, d2);
    for(auto user : users_) {
        user->setMetaData(id, d1, d2);
    }
}

// Add a sensorId user
void sensor::DataSpec::addUser(SensorUser* user) {
    users_.remove(user);
    users_.push_back(user);
}

// Remove a sensorId user
void sensor::DataSpec::removeUser(SensorUser* user) {
    users_.remove(user);
}
