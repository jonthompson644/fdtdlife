/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ARRAYSENSORPOINT_H
#define FDTDLIFE_ARRAYSENSORPOINT_H

#include <map>
#include <vector>
#include <complex>
#include <Box/Vector.h>
#include <Box/Rectangle.h>
#include "DataSpec.h"
#include "SpectrumPoint.h"
#include "ReferencedSpectrum.h"

namespace fdtd { class Model; }
namespace fdtd { class RecordedData; }

namespace sensor {

    class ArraySensor;

    // Represents a single sensor point in an array sensorId
    class ArraySensorPoint {
    public:
        // Construction
        ArraySensorPoint(sensor::ArraySensor* sensor, const box::Rectangle<size_t>& rect,
                         const box::Vector<double>& center);

        // API
        void collect();
        void calculate(bool calculateAll, size_t calculateOnly);
        bool isAt(const box::Vector<size_t>& pos);
        double transmittanceX(size_t channel);
        double phaseShiftX(size_t channel);
        void writePropMatrix(size_t channel, double transTransmittance, double transPhaseShift,
                             double reflTransmittance, double reflPhaseShift);
        void fillTransAttenInfo(DataSpec::Info* info) const;
        void fillReflAttenInfo(DataSpec::Info* info) const;
        void fillTransPhaseShInfo(DataSpec::Info* info) const;
        void fillReflPhaseShInfo(DataSpec::Info* info) const;
        void fillTransTimeSeriesInfo(DataSpec::Info* info) const;
        void fillReflTimeSeriesInfo(DataSpec::Info* info) const;
        void fillPolarisationInfo(DataSpec::Info* info) const;
        void fillSkewInfo(DataSpec::Info* info) const;
        void fillAdmittanceInfo(DataSpec::Info* info) const;
        void getTransAttenData(size_t x, std::vector<double>& dy, double& dx);
        void getReflAttenData(size_t x, std::vector<double>& dy, double& dx);
        void getTransPhaseShData(size_t x, std::vector<double>& dy, double& dx);
        void getReflPhaseShData(size_t x, std::vector<double>& dy, double& dx);
        void getTransTimeSeriesData(size_t x, std::vector<double>& dy, double& dx);
        void getReflTimeSeriesData(size_t x, std::vector<double>& dy, double& dx);
        void getPolarisationData(size_t x, std::vector<double>& dy, double& dx);
        void getSkewData(size_t x, std::vector<double>& dy, double& dx);
        void getAdmittanceData(size_t x, std::vector<double>& dy, double& dx);
        bool writeData(std::ofstream& file);
        bool readData(std::ifstream& file);
        void copyOutputData(std::vector<double>& attenuation,
                            std::vector<double>& phaseShift, bool propMatrix,
                            std::vector<double>& timeSeries, std::vector<double>& reference,
                            bool& xyComponents, std::vector<double>& attenuationY,
                            std::vector<double>& phaseShiftY);
        void copyOutputData(fdtd::RecordedData& data);
        void transmittanceAt(size_t bin, double& x, double& y, bool reflected) const;
        void phaseShiftAt(size_t bin, double& x, double& y, bool reflected) const;
        void phaseAt(size_t bin, double& x, double& y, bool reflected);
        void amplitudeAt(size_t bin, double& x, double& y, bool reflected);
        void propMatTransmittanceAt(size_t bin, double& x) const;
        void propMatPhaseShiftAt(size_t bin, double& x) const;
        box::Vector<size_t> column() const;
        void setData(const box::Vector<double>& transmitted,
                const box::Vector<double>& transmittedRef,
                     const box::Vector<double>& reflected,
                     const box::Vector<double>& reflectedRef);

        // Getters
        const ReferencedSpectrum& transmitted() const { return transmitted_; }
        const ReferencedSpectrum& reflected() const { return reflected_; }
        const ReferencedComponent& transmittedPropMatrix() const { return transmittedPropMat_; }
        const ReferencedComponent& reflectedPropMatrix() const { return reflectedPropMat_; }
        const box::Rectangle<size_t>& rect() const { return rect_; }
        const SpectrumPoint& transmittedData() const { return transmittedData_; }
        const SpectrumPoint& reflectedData() const { return reflectedData_; }
        const box::Vector<double>& center() const { return center_; }

    protected:
        // Data
        ArraySensor* sensor_;
        box::Rectangle<size_t> rect_;
        SpectrumPoint transmittedRef_;
        SpectrumPoint transmittedData_;
        SpectrumPoint reflectedRef_;
        SpectrumPoint reflectedData_;
        ReferencedSpectrum transmitted_;
        ReferencedSpectrum reflected_;
        ReferencedComponent transmittedPropMat_;
        ReferencedComponent reflectedPropMat_;
        box::Vector<double> center_;

        // Helper methods
        void minMax();
    };

}

#endif //FDTDLIFE_ARRAYSENSORPOINT_H
