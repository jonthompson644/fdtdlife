/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ArraySensorPoint.h"
#include <Fdtd/Model.h>
#include <Source/Sources.h>
#include <Box/BinaryFileHeader.h>
#include <Box/Constants.h>
#include <Fdtd/RecordedData.h>
#include "ArraySensor.h"
#include <Source/PlaneWaveSource.h>
#include <fstream>
#include <algorithm>
#include <cmath>

#if defined(USE_OPENMP)

#include <omp.h>

#else
#define omp_get_thread_num() 0
#endif

// Constructor
sensor::ArraySensorPoint::ArraySensorPoint(sensor::ArraySensor* sensor,
                                           const box::Rectangle<size_t>& rect,
                                           const box::Vector<double>& center) :
        sensor_(sensor),
        rect_(rect),
        center_(center) {
    transmittedData_.initialise(sensor_->frequencies(),
                                sensor_->m()->p()->numThreadsUsed(),
                                sensor_->m()->p()->dt(),
                                sensor_->componentSel(),
                                sensor_->nSamples(),
                                sensor_->useWindow());
    transmittedRef_.initialise(sensor_->frequencies(),
                               sensor_->m()->p()->numThreadsUsed(),
                               sensor_->m()->p()->dt(),
                               sensor_->componentSel(),
                               sensor_->nSamples(),
                               sensor_->useWindow());
    reflectedRef_.initialise(sensor_->frequencies(),
                             sensor_->m()->p()->numThreadsUsed(),
                             sensor_->m()->p()->dt(),
                             sensor_->componentSel(),
                             sensor_->nSamples(),
                             sensor_->useWindow());
    reflectedData_.initialise(sensor_->frequencies(),
                              sensor_->m()->p()->numThreadsUsed(),
                              sensor_->m()->p()->dt(),
                              sensor_->componentSel(),
                              sensor_->nSamples(),
                              sensor_->useWindow());
    transmitted_.initialise(sensor_->frequencies().n(),
                            sensor_->componentSel());
    reflected_.initialise(sensor_->frequencies().n(),
                          sensor_->componentSel());
    transmittedPropMat_.initialise(sensor_->frequencies().n());
    reflectedPropMat_.initialise(sensor_->frequencies().n());
}

// Collect data for this point
void sensor::ArraySensorPoint::collect() {
    // Sum the data from the cells this sensorId point covers
    box::Vector<double> transmitted(0.0);
    box::Vector<double> reflected(0.0);
    size_t count = 0;
    for(size_t i = rect_.left(); i < rect_.right(); i++) {
        for(size_t j = rect_.top(); j < rect_.bottom(); j++) {
            transmitted += sensor_->transmittedValue(i, j);
            reflected += sensor_->reflectedValue(i, j);
            count++;
        }
    }
    // Do the average
    if(count > 0) {
        transmitted = transmitted / static_cast<double>(count);
        reflected = reflected / static_cast<double>(count);
    }
    // Get the reference data (all the +0.5s put the coordinate at the center
    // of the cells).
    box::Vector<double> transmittedRef = sensor_->m()->sources().eIncident(
            rect_.centerX() + 0.5, rect_.centerY() + 0.5,
            sensor_->transmittedZ() + 0.5);
    box::Vector<double> reflectedRef = sensor_->m()->sources().eIncident(
            rect_.centerX() + 0.5, rect_.centerY() + 0.5,
            sensor_->reflectedReferenceZ() + 0.5);
    // Sample the data
    transmittedData_.collect(transmitted.x(), transmitted.y(), transmitted.z());
    transmittedRef_.collect(transmittedRef.x(), transmittedRef.y(), transmitted.z());
    reflectedData_.collect(reflected.x(), reflected.y(), reflected.z());
    reflectedRef_.collect(reflectedRef.x(), reflectedRef.y(), reflected.z());
}

// Set the data for an array sensorId point
void sensor::ArraySensorPoint::setData(const box::Vector<double>& transmitted,
             const box::Vector<double>& transmittedRef,
             const box::Vector<double>& reflected,
             const box::Vector<double>& reflectedRef) {
    transmittedData_.collect(transmitted.x(), transmitted.y(), transmitted.z());
    transmittedRef_.collect(transmittedRef.x(), transmittedRef.y(), transmitted.z());
    reflectedData_.collect(reflected.x(), reflected.y(), reflected.z());
    reflectedRef_.collect(reflectedRef.x(), reflectedRef.y(), reflected.z());
}

// Calculate the ranges of the results
void sensor::ArraySensorPoint::minMax() {
    transmitted_.evaluateRanges();
    reflected_.evaluateRanges();
    transmittedPropMat_.evaluateRanges();
    reflectedPropMat_.evaluateRanges();
}

// Calculate the DFTs
void sensor::ArraySensorPoint::calculate(bool calculateAll, size_t calculateOnly) {
    if(calculateAll) {
        // Do the calculate on all channels
        transmittedRef_.calculate();
        transmittedData_.calculate();
        reflectedRef_.calculate();
        reflectedData_.calculate();
        transmitted_.calculate(transmittedData_, transmittedRef_);
        reflected_.calculate(reflectedData_, reflectedRef_);
        minMax();
    } else {
        // Do the calculation on only the channel specified
        transmittedRef_.calculate(calculateOnly);
        transmittedData_.calculate(calculateOnly);
        reflectedRef_.calculate(calculateOnly);
        reflectedData_.calculate(calculateOnly);
        transmitted_.calculate(calculateOnly, transmittedData_, transmittedRef_);
        reflected_.calculate(calculateOnly, reflectedData_, reflectedRef_);
    }
}

// Return true if this sensorId point occupies the given position.
// Note that the z coordinate has already been checked.
bool sensor::ArraySensorPoint::isAt(const box::Vector<size_t>& pos) {
    return rect_.isIn(pos.x(), pos.y());
}

// Return the transmittance at the specified frequency
double sensor::ArraySensorPoint::transmittanceX(size_t channel) {
    double result = 0.0;
    if(channel < transmitted_.x().transmittance().size()) {
        result = transmitted_.x().transmittance()[channel];
    }
    return result;
}

// Return the phase shift at the specified frequency
double sensor::ArraySensorPoint::phaseShiftX(size_t channel) {
    double result = 0.0;
    if(channel < transmitted_.x().phaseShift().size()) {
        result = transmitted_.x().phaseShift()[channel];
    }
    return result;
}

// Write a propagation matrix point
void sensor::ArraySensorPoint::writePropMatrix(size_t channel, double transTransmittance,
                                               double transPhaseShift,
                                               double reflTransmittance,
                                               double reflPhaseShift) {

    if(channel < sensor_->frequencies().n()) {
        transmittedPropMat_.set(channel, transTransmittance, transPhaseShift, 0);
        reflectedPropMat_.set(channel, reflTransmittance, reflPhaseShift, 0);
    }
    minMax();
}

// Fill the data specification information record for the transmitted attenuation traces
void sensor::ArraySensorPoint::fillTransAttenInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = std::min(transmitted_.x().transmittanceRange().min(),
                          transmitted_.y().transmittanceRange().min());
    info->max_ = std::max(transmitted_.x().transmittanceRange().max(),
                          transmitted_.y().transmittanceRange().max());
    info->xUnits_ = "GHz";
    info->yUnits_ = "dB";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Fill the data specification information record for the reflected attenuation traces
void sensor::ArraySensorPoint::fillReflAttenInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = std::min(reflected_.x().transmittanceRange().min(),
                          reflected_.y().transmittanceRange().min());
    info->max_ = std::min(reflected_.x().transmittanceRange().max(),
                          reflected_.y().transmittanceRange().max());
    info->xUnits_ = "GHz";
    info->yUnits_ = "dB";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Fill the data specification information record for the transmitted phase shift traces
void sensor::ArraySensorPoint::fillTransPhaseShInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = -180.0;
    info->max_ = +180.0;
    info->xUnits_ = "GHz";
    info->yUnits_ = "deg";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}
// Fill the data specification information record for the reflected phase shift traces
void sensor::ArraySensorPoint::fillReflPhaseShInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = -180.0;
    info->max_ = +180.0;
    info->xUnits_ = "GHz";
    info->yUnits_ = "deg";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Fill the data specification information record for the transmitted time series
void sensor::ArraySensorPoint::fillTransTimeSeriesInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->nSamples();
    box::Range<double> range;
    if(sensor_->componentSel().x()) {
        range |= transmittedData_.timeSeriesX().range() |
                 transmittedRef_.timeSeriesX().range();
    }
    if(sensor_->componentSel().y()) {
        range |= transmittedData_.timeSeriesY().range() |
                 transmittedRef_.timeSeriesY().range();
    }
    if(sensor_->componentSel().z()) {
        range |= transmittedData_.timeSeriesZ().range() |
                 transmittedRef_.timeSeriesZ().range();
    }
    info->min_ = range.min();
    info->max_ = range.max();
    info->yUnits_ = "V/m";
    info->xUnits_ = "ps";
    info->xLeft_ = -((sensor_->nSamples() - 1) * sensor_->m()->p()->dt())
                   / box::Constants::pico_;
    info->xRight_ = 0;
}

// Fill the data specification information record for the reflected time series
void sensor::ArraySensorPoint::fillReflTimeSeriesInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->nSamples();
    box::Range<double> range;
    if(sensor_->componentSel().x()) {
        range |= reflectedData_.timeSeriesX().range() |
                 reflectedRef_.timeSeriesX().range();
    }
    if(sensor_->componentSel().y()) {
        range |= reflectedData_.timeSeriesY().range() |
                 reflectedRef_.timeSeriesY().range();
    }
    if(sensor_->componentSel().z()) {
        range |= reflectedData_.timeSeriesZ().range() |
                 reflectedRef_.timeSeriesZ().range();
    }
    info->min_ = range.min();
    info->max_ = range.max();
    info->yUnits_ = "V/m";
    info->xUnits_ = "ps";
    info->xLeft_ = -((sensor_->nSamples() - 1) * sensor_->m()->p()->dt())
                   / box::Constants::pico_;
    info->xRight_ = 0;
}

// Get a transmitted attenuation data point
void sensor::ArraySensorPoint::getTransAttenData(size_t x, std::vector<double>& dy,
                                                 double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = transmitted_.x().transmittance()[x];
        }
        if(dy.size() > 1) {
            dy[1] = transmittedPropMat_.transmittance()[x];
        }
        if(dy.size() > 2) {
            dy[2] = transmitted_.y().transmittance()[x];
        }
        if(dy.size() > 3) {
            dy[3] = transmitted_.z().transmittance()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get a reflected attenuation data point
void sensor::ArraySensorPoint::getReflAttenData(size_t x, std::vector<double>& dy,
                                                double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = reflected_.x().transmittance()[x];
        }
        if(dy.size() > 1) {
            dy[1] = reflectedPropMat_.transmittance()[x];
        }
        if(dy.size() > 2) {
            dy[2] = reflected_.y().transmittance()[x];
        }
        if(dy.size() > 3) {
            dy[3] = reflected_.z().transmittance()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get a transmitted phase shift data point
void sensor::ArraySensorPoint::getTransPhaseShData(size_t x, std::vector<double>& dy,
                                                   double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = transmitted_.x().phaseShift()[x];
        }
        if(dy.size() > 1) {
            dy[1] = transmittedPropMat_.phaseShift()[x];
        }
        if(dy.size() > 2) {
            dy[2] = transmitted_.y().phaseShift()[x];
        }
        if(dy.size() > 3) {
            dy[3] = transmitted_.z().phaseShift()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get a reflected phase shift data point
void sensor::ArraySensorPoint::getReflPhaseShData(size_t x, std::vector<double>& dy,
                                                  double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = reflected_.x().phaseShift()[x];
        }
        if(dy.size() > 1) {
            dy[1] = reflectedPropMat_.phaseShift()[x];
        }
        if(dy.size() > 2) {
            dy[2] = reflected_.y().phaseShift()[x];
        }
        if(dy.size() > 3) {
            dy[3] = reflected_.z().phaseShift()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get a transmitted time series data point
void sensor::ArraySensorPoint::getTransTimeSeriesData(size_t x, std::vector<double>& dy,
                                                      double& dx) {
    if(x < sensor_->nSamples()) {
        size_t n = 0;
        if(sensor_->componentSel().x()) {
            if(n < dy.size()) {
                dy[n++] = transmittedData_.timeSeriesX()[x];
            }
            if(n < dy.size()) {
                dy[n++] = transmittedRef_.timeSeriesX()[x];
            }
        }
        if(sensor_->componentSel().y()) {
            if(n < dy.size()) {
                dy[n++] = transmittedData_.timeSeriesY()[x];
            }
            if(n < dy.size()) {
                dy[n++] = transmittedRef_.timeSeriesY()[x];
            }
        }
        if(sensor_->componentSel().z()) {
            if(n < dy.size()) {
                dy[n++] = transmittedData_.timeSeriesZ()[x];
            }
            if(n < dy.size()) {
                dy[n] = transmittedRef_.timeSeriesZ()[x];
            }
        }
        dx = sensor_->m()->p()->dt() * (sensor_->frequencies().n() - x);
    }
}

// Get a reflected time series data point
void sensor::ArraySensorPoint::getReflTimeSeriesData(size_t x, std::vector<double>& dy,
                                                     double& dx) {
    if(x < sensor_->nSamples()) {
        size_t n = 0;
        if(sensor_->componentSel().x()) {
            if(n < dy.size()) {
                dy[n++] = reflectedData_.timeSeriesX()[x];
            }
            if(n < dy.size()) {
                dy[n++] = reflectedRef_.timeSeriesX()[x];
            }
        }
        if(sensor_->componentSel().y()) {
            if(n < dy.size()) {
                dy[n++] = reflectedData_.timeSeriesY()[x];
            }
            if(n < dy.size()) {
                dy[n++] = reflectedRef_.timeSeriesY()[x];
            }
        }
        if(sensor_->componentSel().z()) {
            if(n < dy.size()) {
                dy[n++] = reflectedData_.timeSeriesZ()[x];
            }
            if(n < dy.size()) {
                dy[n] = reflectedRef_.timeSeriesZ()[x];
            }
        }
        dx = sensor_->m()->p()->dt() * (sensor_->frequencies().n() - x);
    }
}

// Write run state data to the binary file
bool sensor::ArraySensorPoint::writeData(std::ofstream& file) {
    box::BinaryFileHeader header("ARRAYSENSORPOINT", 1,
                                 sensor_->frequencies().n() * 4 * sizeof(double));
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    // TODO:  Implement read and write data
    return true;
}

// Read run state data from the binary file
bool sensor::ArraySensorPoint::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("ARRAYSENSORPOINT")) {
        switch(header.version()) {
            case 1:
                // TODO:  Implement read and write data
                break;
            default:
                break;
        }
    }
    return result;
}

// Copy the output data into the given vector objects
void sensor::ArraySensorPoint::copyOutputData(std::vector<double>& attenuation,
                                              std::vector<double>& phaseShift,
                                              bool propMatrix,
                                              std::vector<double>& timeSeries,
                                              std::vector<double>& reference,
                                              bool& /*xyComponents*/,
                                              std::vector<double>& attenuationY,
                                              std::vector<double>& phaseShiftY) {
    if(propMatrix) {
        phaseShift = transmittedPropMat_.phaseShift();
        attenuation = transmittedPropMat_.transmittance();
    } else {
        phaseShift = transmitted_.x().phaseShift();
        attenuation = transmitted_.x().transmittance();
        phaseShiftY = transmitted_.y().phaseShift();
        attenuationY = transmitted_.y().transmittance();
        transmittedData_.timeSeriesX().copyInto(timeSeries);
        transmittedRef_.timeSeriesX().copyInto(reference);
    }
    std::transform(attenuation.begin(), attenuation.end(), attenuation.begin(),
                   [](double a) { return std::sqrt(a); });
}

// Copy the output data into the given object
void sensor::ArraySensorPoint::copyOutputData(fdtd::RecordedData& data) {
    data.resize(sensor_->frequencies().n(), sensor_->componentSel().y(),
                sensor_->includeReflected());
    for(size_t i = 0; i < sensor_->frequencies().n(); i++) {
        if(sensor_->m()->p()->doFdtd()) {
            double transX = transmitted_.x().transmittance()[i];
            double phaseX = transmitted_.x().phaseShift()[i];
            double transY = transmitted_.y().transmittance()[i];
            double phaseY = transmitted_.y().phaseShift()[i];
            double reflX = reflected_.x().transmittance()[i];
            double reflY = reflected_.y().transmittance()[i];
            data.setData(i, transX, phaseX, transY, phaseY, reflX, reflY);
        } else {
            double transX = transmittedPropMat_.transmittance()[i];
            double phaseX = transmittedPropMat_.phaseShift()[i];
            double reflX = reflectedPropMat_.transmittance()[i];
            data.setData(i, transX, phaseX, 0.0, 0.0,
                         reflX, 0.0);
        }
    }
}

// Fill the data specification information with polarisation information
void sensor::ArraySensorPoint::fillPolarisationInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = -90.0;
    info->max_ = +90.0;
    info->xUnits_ = "GHz";
    info->yUnits_ = "deg";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Fill the data specification information with skew information
void sensor::ArraySensorPoint::fillSkewInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    info->min_ = 0.0;
    info->max_ = 1.0;
    info->xUnits_ = "GHz";
    info->yUnits_ = "";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Fill the data specification information with admittance information
void sensor::ArraySensorPoint::fillAdmittanceInfo(DataSpec::Info* info) const {
    info->xSize_ = sensor_->frequencies().n();
    box::Range<double> range = transmitted_.x().admittanceRange();
    info->min_ = range.min();
    info->max_ = range.max();
    info->xUnits_ = "GHz";
    info->yUnits_ = "";
    info->xLeft_ = sensor_->frequencies().first() / box::Constants::giga_;
    info->xRight_ = sensor_->frequencies().last() / box::Constants::giga_;
}

// Get polarisation data
void sensor::ArraySensorPoint::getPolarisationData(size_t x, std::vector<double>& dy,
                                                   double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = transmitted_.polarisation()[x];
        }
        if(dy.size() > 1) {
            dy[1] = reflected_.polarisation()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get skew data
void sensor::ArraySensorPoint::getSkewData(size_t x, std::vector<double>& dy, double& dx) {
    if(x < sensor_->frequencies().n()) {
        if(!dy.empty()) {
            dy[0] = transmitted_.skew()[x];
        }
        if(dy.size() > 1) {
            dy[1] = reflected_.skew()[x];
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Get admittance data
void sensor::ArraySensorPoint::getAdmittanceData(size_t x, std::vector<double>& dy,
                                                 double& dx) {
    if(x < sensor_->frequencies().n()) {
        std::complex<double> a = transmitted_.x().admittance()[x];
        if(!dy.empty()) {
            dy[0] = a.real();
        }
        if(dy.size() > 1) {
            dy[1] = a.imag();
        }
        dx = sensor_->frequencies().frequency(x) / box::Constants::giga_;
    }
}

// Return the transmittance from the specified frequency bin
void sensor::ArraySensorPoint::transmittanceAt(size_t bin, double& x, double& y,
                                               bool reflected) const {
    x = 0.0;
    y = 0.0;
    if(bin < sensor_->frequencies().n()) {
        if(reflected) {
            x = reflected_.x().transmittance().at(bin);
            y = reflected_.y().transmittance().at(bin);
        } else {
            x = transmitted_.x().transmittance().at(bin);
            y = transmitted_.y().transmittance().at(bin);
        }
    }
}

// Return the phase shift at the specified frequency bin
void sensor::ArraySensorPoint::phaseShiftAt(size_t bin, double& x, double& y,
                                            bool reflected) const {
    x = 0.0;
    y = 0.0;
    if(bin < sensor_->frequencies().n()) {
        if(reflected) {
            x = reflected_.x().phaseShift().at(bin);
            y = reflected_.y().phaseShift().at(bin);
        } else {
            x = transmitted_.x().phaseShift().at(bin);
            y = transmitted_.y().phaseShift().at(bin);
        }
    }
}

// Return the propagation matrix transmittance from the specified frequency bin
void sensor::ArraySensorPoint::propMatTransmittanceAt(size_t bin, double& x) const {
    x = 0.0;
    if(bin < sensor_->frequencies().n()) {
        x = transmittedPropMat_.transmittance().at(bin);
    }
}

// Return the propagation matrix phase shift at the specified frequency bin
void sensor::ArraySensorPoint::propMatPhaseShiftAt(size_t bin, double& x) const {
    x = 0.0;
    if(bin < sensor_->frequencies().n()) {
        x = transmittedPropMat_.phaseShift().at(bin);
    }
}

// Return a coordinate for the column the point is in
box::Vector<size_t> sensor::ArraySensorPoint::column() const {
    return box::Vector<double>(rect_.centerX(), rect_.centerY(), 0).floor<size_t>();
}

// Return the phase at the specified frequency bin
void sensor::ArraySensorPoint::phaseAt(size_t bin, double& x, double& y, bool reflected) {
    x = 0.0;
    y = 0.0;
    if(bin < sensor_->frequencies().n()) {
        if(reflected) {
            x = reflectedData_.spectrumX().phase()[bin];
            y = reflectedData_.spectrumY().phase()[bin];
        } else {
            x = transmittedData_.spectrumX().phase()[bin];
            y = transmittedData_.spectrumY().phase()[bin];
        }
    }
}

// Return the amplitude at the specified frequency bin
void sensor::ArraySensorPoint::amplitudeAt(size_t bin, double& x, double& y, bool reflected) {
    x = 0.0;
    y = 0.0;
    if(bin < sensor_->frequencies().n()) {
        if(reflected) {
            x = reflectedData_.spectrumX().amplitude()[bin];
            y = reflectedData_.spectrumY().amplitude()[bin];
        } else {
            x = transmittedData_.spectrumX().amplitude()[bin];
            y = transmittedData_.spectrumY().amplitude()[bin];
        }
    }
}

