/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Analyser.h"
#include <Fdtd/Model.h>
#include <Xml/DomObject.h>
#include <Sensor/ArraySensor.h>

// Second stage construction used by factories
void sensor::Analyser::construct(int mode, int id, fdtd::Model* m) {
    mode_ = mode;
    identifier_ = id;
    m_ = m;
}

// Get or create the sensorId for this fitness function
sensor::ArraySensor* sensor::Analyser::getSensor() {
    return dynamic_cast<ArraySensor*>(m_->sensors().find(sensorId_));
}

// Write the configuration to the DOM object
void sensor::Analyser::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("sensorid") << sensorId_;
    root << xml::Obj("location") << location_;
    root << xml::Obj("component") << component_;
    root << xml::Obj("name") << name_;
    root << xml::Close();
}

// Read the configuration from the DOM object
// Note: Don't read mode_ and id_ as they may be different and
// have already been set by the factory
void sensor::Analyser::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("sensorid") >> sensorId_;
    root >> xml::Obj("location") >> location_;
    root >> xml::Obj("component") >> component_;
    root >> xml::Obj("name") >> name_;
    root >> xml::Close();
}
