/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <iostream>
#include "TwoDSlice.h"
#include <Fdtd/Model.h>
#include <Fdtd/EField.h>
#include <Fdtd/HField.h>
#include <Domain/Material.h>
#include <Source/Sources.h>
#include <Source/Source.h>
#include <Box/BinaryFileHeader.h>
#include <Xml/DomObject.h>

// Constructor
sensor::TwoDSlice::TwoDSlice() :
        sliceSpec_(this, "twoDSlice",
                   DataSpec::modeTwoD, {""}) {
    specs_.push_back(&sliceSpec_);
    nullArrays();
}

// Destructor
sensor::TwoDSlice::~TwoDSlice() {
    clear();
}

// Clean up the slice
void sensor::TwoDSlice::clear() {
    delete[] data_;
    nullArrays();
}

// Null out the arrays
void sensor::TwoDSlice::nullArrays() {
    data_ = nullptr;
}

// Initialise the slice ready for a run
void sensor::TwoDSlice::initialise() {
    // Delete old data
    clear();
    // The size and start of the data
    switch(orientation_) {
        case box::Constants::orientationXPlane:
            nSliceX_ = m_->p()->geometry()->n().y();
            nSliceY_ = m_->p()->geometry()->n().z();
            // The rounding here must be the same as the rounding used when filling the domain
            sliceOffset_ = m_->p()->geometry()->cellFloor(
                    box::Vector<double>(offset_, 0.0, 0.0)).x();
            break;
        case box::Constants::orientationYPlane:
            nSliceX_ = m_->p()->geometry()->n().x();
            nSliceY_ = m_->p()->geometry()->n().z();
            // The rounding here must be the same as the rounding used when filling the domain
            sliceOffset_ = m_->p()->geometry()->cellFloor(
                    box::Vector<double>(0.0, offset_, 0.0)).y();
            break;
        case box::Constants::orientationZPlane:
            nSliceX_ = m_->p()->geometry()->n().x();
            nSliceY_ = m_->p()->geometry()->n().y();
            // The rounding here must be the same as the rounding used when filling the domain
            sliceOffset_ = m_->p()->geometry()->cellFloor(
                    box::Vector<double>(0.0, 0.0, offset_)).z();
            break;
    }
    // Allocate new data
    data_ = new double[nSliceX_ * nSliceY_];
    for(size_t i = 0; i < nSliceX_ * nSliceY_; i++) {
        data_[i] = 0.0;
    }
}

// Calculate the index into the slice matrix.
size_t sensor::TwoDSlice::index(size_t sx, size_t sy) const {
    size_t result = sy * nSliceX_ + sx;
    if(result >= nSliceX_ * nSliceY_) {
        std::cout << "TwoDSlice: index out of range " << nSliceX_
                  << "," << nSliceY_ << std::endl;
        result = 0;
    }
    return result;
}

// Collect a slice from the model.
void sensor::TwoDSlice::collect() {
    if(data_ != nullptr) {
        size_t i, j, k;
        // Extract the slice from the model
        switch(orientation_) {
            case box::Constants::orientationXPlane:
                i = sliceOffset_;
                for(j = 0; j < nSliceX_; j++) {
                    for(k = 0; k < nSliceY_; k++) {
                        data_[index(j, k)] = getValue({i, j, k});
                    }
                }
                break;
            case box::Constants::orientationYPlane:
                j = sliceOffset_;
                for(i = 0; i < nSliceX_; i++) {
                    for(k = 0; k < nSliceY_; k++) {
                        data_[index(i, k)] = getValue({i, j, k});
                    }
                }
                break;
            case box::Constants::orientationZPlane:
                k = sliceOffset_;
                for(i = 0; i < nSliceX_; i++) {
                    for(j = 0; j < nSliceY_; j++) {
                        data_[index(i, j)] = getValue({i, j, k});
                    }
                }
                break;
        }
        // Find the max and min values
        findMinMax();
    }
}

// Find the minium and maximum values
void sensor::TwoDSlice::findMinMax() {
    if(data_ != nullptr) {
        for(size_t i = 0; i < nSliceX_; i++) {
            for(size_t j = 0; j < nSliceY_; j++) {
                if(i == 0 && j == 0) {
                    min_ = data_[index(i, j)];
                    max_ = data_[index(i, j)];
                } else {
                    if(data_[index(i, j)] < min_) {
                        min_ = data_[index(i, j)];
                    }
                    if(data_[index(i, j)] > max_) {
                        max_ = data_[index(i, j)];
                    }
                }
            }
        }
    }
}

// Return the model value at the specified cell coordinates
double sensor::TwoDSlice::getValue(const box::Vector<size_t>& p) {
    double result = 0.0;
    domain::Material* mat;
    switch(dataSource_) {
        case eMagnitude:
            result = m_->e()->value(p).mag();
            break;
        case hMagnitude:
            result = m_->h()->value(p).mag();
            break;
        case ex:
            result = m_->e()->value(p).x();
            break;
        case ey:
            result = m_->e()->value(p).y();
            break;
        case ez:
            result = m_->e()->value(p).z();
            break;
        case hx:
            result = m_->h()->value(p).x();
            break;
        case hy:
            result = m_->h()->value(p).y();
            break;
        case hz:
            result = m_->h()->value(p).z();
            break;
        case material: {
            mat = m_->d()->getMaterialAt(p);
            if(mat != nullptr) {
                result = static_cast<double>(static_cast<int>(mat->color()));
            }
            source::Source* source = m_->sources().getAt(p);
            if(source != nullptr) {
                result = static_cast<double>(static_cast<int>(source->colour()));
            }
            Sensor* sensor = m_->sensors().getAt(p);
            if(sensor != nullptr) {
                result = static_cast<double>(static_cast<int>(sensor->colour()));
            }
            break;
        }
    }
    return result;
}

// Return the data stream information
void sensor::TwoDSlice::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == &sliceSpec_) {
        info->xSize_ = nSliceX_;
        info->ySize_ = nSliceY_;
        info->zPos_ = static_cast<int>(sliceOffset_);
        info->min_ = min_;
        info->max_ = max_;
        info->xUnits_ = "nm";
        info->yUnits_ = "nm";
    } else {
        return Sensor::getInfo(spec, info);
    }
}

// Return a data point from the stream
void sensor::TwoDSlice::getData(DataSpec* spec, size_t x, size_t y, double* d,
                                double* dx, double* dy) {
    if(spec == &sliceSpec_) {
        if(data_ != nullptr) {
            *d = data_[index(x, y)];
            switch(orientation_) {
                case box::Constants::orientationXPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().y().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().z().value() * y / box::Constants::giga_;
                    break;
                case box::Constants::orientationYPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().x().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().z().value() * y / box::Constants::giga_;
                    break;
                case box::Constants::orientationZPlane:
                    // Distances in nm
                    *dx = m_->p()->dr().x().value() * x / box::Constants::giga_;
                    *dy = m_->p()->dr().y().value() * y / box::Constants::giga_;
                    break;
            }
        }
    } else {
        Sensor::getData(spec, x, y, d, dx, dy);
    }
}

// Write state data to the binary file
bool sensor::TwoDSlice::writeData(std::ofstream& file) {
    box::BinaryFileHeader header("TWODSLICE", 1, nSliceX_ * nSliceY_ * sizeof(double));
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    file.write(reinterpret_cast<const char*>(data_),
               static_cast<std::streamsize>(nSliceX_ * nSliceY_ * sizeof(double)));
    return true;
}

// Read state data from the binary file
bool sensor::TwoDSlice::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("TWODSLICE")) {
        size_t arraySize = 0;
        switch(header.version()) {
            case 1:
                arraySize = header.recordSize() / sizeof(double);
                file.read(reinterpret_cast<char*>(data_),
                          static_cast<std::streamsize>(nSliceX_ * nSliceY_ * sizeof(double)));
                result = nSliceX_ * nSliceY_ == arraySize;
                break;
            default:
                break;
        }
    }
    return result;
}

// Write the configuration to the DOM object
void sensor::TwoDSlice::writeConfig(xml::DomObject& p) const {
    // Base class first
    Sensor::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("orientation") << orientation_;
    p << xml::Obj("offset") << offset_;
    p << xml::Obj("datasource") << dataSource_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::TwoDSlice::readConfig(xml::DomObject& p) {
    // Base class first
    Sensor::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("orientation") >> orientation_;
    p >> xml::Obj("offset") >> offset_;
    p >> xml::Obj("datasource") >> dataSource_;
    p >> xml::Close();
}
