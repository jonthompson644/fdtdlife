/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensPhaseAnalyser.h"
#include "ArraySensor.h"
#include <Fdtd/Model.h>

// Constructor
sensor::LensPhaseAnalyser::LensPhaseAnalyser() {
    dataType_ = DataType::phaseDistribution;
}

// Write the configuration to the XML DOM
void sensor::LensPhaseAnalyser::writeConfig(xml::DomObject& root) const {
    // Base class first
    Analyser::writeConfig(root);
    // Now me
    root << xml::Reopen();
    root << xml::Obj("frequency") << frequency_;
    root << xml::Close();
}

// Read the configuration from the XML DOM
void sensor::LensPhaseAnalyser::readConfig(xml::DomObject& root) {
    // Base class first
    Analyser::readConfig(root);
    // Now me
    root >> xml::Reopen();
    root >> xml::Obj("frequency") >> frequency_;
    root >> xml::Close();
}

// Evaluate any expressions
void sensor::LensPhaseAnalyser::evaluate(box::Expression::Context& c) {
    // Base class first
    Analyser::evaluate(c);
    // Now my stuff
    frequency_.evaluate(c);
}

// Collect data from the sensorId
void sensor::LensPhaseAnalyser::collect() {
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr && !sensor->points().empty()) {
        // Get the data from the sensorId
        size_t numPoints = sensor->numPointsX();
        data_.resize(numPoints);
        reference_.resize(numPoints);
        minX_ = sensor->points()[sensor->index(0, 0)].center().x();
        double maxX = minX_;
        if(numPoints > 0) {
            maxX = sensor->points()[sensor->index(numPoints - 1, 0)].center().x();
        }
        stepX_ = 0.0;
        if(numPoints > 1) {
            stepX_ = (maxX - minX_) / static_cast<double>(numPoints - 1);
        }
        // Record the data
        double ax = 0.0;
        double ay = 0.0;
        double offset = 0.0;
        double last = 0.0;
        for(size_t i = 0; i < numPoints; i++) {
            // Get the point
            sensor->phaseAt(i, 0, frequency_.value(), ax, ay,
                            location_ != SensorLocation::highestZ);
            double measured = component_ == SensorComponent::y ? ay : ax;
            if(i != 0) {
                if((measured - last) < -box::Constants::pi_) {
                    offset += 2.0 * box::Constants::pi_;
                } else if((measured - last) > box::Constants::pi_) {
                    offset -= 2.0 * box::Constants::pi_;
                }
            }
            last = measured;
            measured += offset;
            data_[i] = measured;
        }
    }
}

// Calculate the error using the current state of the sensorId
void sensor::LensPhaseAnalyser::calculate() {
    error_ = 0.0;
    size_t numPoints = data_.size();
    // Make the reference and calculate the error
    double reference = 0.0;
    if(numPoints > 0) {
        if((numPoints % 2U) == 0) {
            // Even number of points, reference is the average of the two in the middle
            reference = (data_[numPoints / 2U] + data_[numPoints / 2U - 1U]) / 2.0;
        } else {
            // Odd number of points, the single middle point is the reference
            reference = data_[numPoints / 2U];
        }
    }
    for(size_t i = 0; i < numPoints; i++) {
        // Fill the reference data
        reference_[i] = 0.0;
        // Record the data as an offset from the reference
        double diff = data_[i] - reference;
        data_[i] = diff;
        // Calculate the error from the reference
        error_ += (diff * diff);
        // Calculate the min/max
        if(i == 0) {
            minY_ = diff;
            maxY_ = diff;
        } else {
            minY_ = std::min(minY_, diff);
            maxY_ = std::max(maxY_, diff);
        }
    }
}

// Set the data as though it was collected from the sensorId.
// Used mainly by the test suite
void sensor::LensPhaseAnalyser::setData(const std::vector<double>& data) {
    // Calculate metadata
    size_t numPoints = data.size();
    data_.resize(numPoints);
    reference_.resize(numPoints);
    stepX_ = (m_->p()->p2().x().value() - m_->p()->p1().x().value()) / numPoints;
    minX_ = m_->p()->p1().x().value() + stepX_ / 2;
    // Record the data
    double offset = 0.0;
    double last = 0.0;
    for(size_t i = 0; i < numPoints; i++) {
        // Get the point
        double measured = data[i];
        if(i != 0) {
            if((measured - last) < -box::Constants::pi_) {
                offset += 2.0 * box::Constants::pi_;
            } else if((measured - last) > box::Constants::pi_) {
                offset -= 2.0 * box::Constants::pi_;
            }
        }
        last = measured;
        measured += offset;
        data_[i] = measured;
    }
    // Now calculate using this data
    calculate();
    m_->doNotification(fdtd::Model::notifyAnalysersCalculate);
}
