/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ANALYSERS_H
#define FDTDLIFE_ANALYSERS_H

#include <list>
#include <Box/Factory.h>
#include "Analyser.h"

namespace fdtd { class Model; }

namespace sensor {

    // A class that manages the analysers
    class Analysers {
    public:
        // Construction
        explicit Analysers(fdtd::Model* m);

        // Iterators
        typedef std::list<std::unique_ptr<Analyser>>::iterator iterator;
        iterator begin() { return analysers_.begin(); }
        iterator end() { return analysers_.end(); }

        // API
        enum {
            modeLensPhase, modeLensReflection, modeLensTransmission,
            modeTransmittance, modePhaseShift, modeFilter
        };
        Analyser* restore(xml::DomObject& o);
        Analyser* make(int mode);
        Analyser* find(int identifier);
        void remove(Analyser* analyser);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        void clear();
        void initialise();
        void calculate();
        void stepComplete();
        friend xml::DomObject& operator<<(xml::DomObject& o, const Analysers& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Analysers& s) {
            s.readConfig(o);
            return o;
        }
        box::Factory<Analyser>& factory() { return factory_; }

    protected:
        // Members
        std::list<std::unique_ptr<Analyser>> analysers_;  // The list of analysers
        box::Factory<Analyser> factory_;  // The factory that creates analysers
        fdtd::Model* m_;  // The model

    };
}


#endif //FDTDLIFE_ANALYSERS_H
