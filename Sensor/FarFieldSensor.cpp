/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FarFieldSensor.h"
#include <Fdtd/Model.h>
#include <Source/Sources.h>
#include <Source/PlaneWaveSource.h>
#include <Box/BinaryFileHeader.h>
#include <Xml/DomObject.h>

// Constructor
sensor::FarFieldSensor::FarFieldSensor() {
    attenuationFront_ = new DataSpec(this, "FrontAttenuation",
                                     DataSpec::modeAttenuation,
                                     {"X", "Y", "Z"});
    phaseShiftFront_ = new DataSpec(this, "FrontPhaseShift",
                                    DataSpec::modePhaseShift,
                                    {"X", "Y", "Z"});
    attenuationBack_ = new DataSpec(this, "BackAttenuation",
                                    DataSpec::modeAttenuation,
                                    {"X", "Y", "Z"});
    phaseShiftBack_ = new DataSpec(this, "BackPhaseShift",
                                   DataSpec::modePhaseShift,
                                   {"X", "Y", "Z"});
    timeSeriesFaceX0_ = new DataSpec(this, "TimeSeriesFaceX0",
                                     DataSpec::modeTwoD, {""});
    timeSeriesFaceX1_ = new DataSpec(this, "TimeSeriesFaceX1",
                                     DataSpec::modeTwoD, {""});
    timeSeriesFaceY0_ = new DataSpec(this, "TimeSeriesFaceY0",
                                     DataSpec::modeTwoD, {""});
    timeSeriesFaceY1_ = new DataSpec(this, "TimeSeriesFaceY1",
                                     DataSpec::modeTwoD, {""});
    timeSeriesFaceZ0_ = new DataSpec(this, "TimeSeriesFaceZ0",
                                     DataSpec::modeTwoD, {""});
    timeSeriesFaceZ1_ = new DataSpec(this, "TimeSeriesFaceZ1",
                                     DataSpec::modeTwoD, {""});
    specs_.push_back(attenuationFront_);
    specs_.push_back(phaseShiftFront_);
    specs_.push_back(attenuationBack_);
    specs_.push_back(phaseShiftBack_);
    specs_.push_back(timeSeriesFaceX0_);
    specs_.push_back(timeSeriesFaceX1_);
    specs_.push_back(timeSeriesFaceY0_);
    specs_.push_back(timeSeriesFaceY1_);
    specs_.push_back(timeSeriesFaceZ0_);
    specs_.push_back(timeSeriesFaceZ1_);
}

// Desructor
sensor::FarFieldSensor::~FarFieldSensor() {
    delete attenuationFront_;
    delete phaseShiftFront_;
    delete attenuationBack_;
    delete phaseShiftBack_;
    delete timeSeriesFaceX0_;
    delete timeSeriesFaceX1_;
    delete timeSeriesFaceY0_;
    delete timeSeriesFaceY1_;
    delete timeSeriesFaceZ0_;
    delete timeSeriesFaceZ1_;
}

// Return the source the array sensorId is linked to
source::PlaneWaveSource* sensor::FarFieldSensor::getSource() {
    source::PlaneWaveSource* source = nullptr;
    for(auto& s : m_->sources().sources()) {
        source = dynamic_cast<source::PlaneWaveSource*>(s);
        if(source != nullptr) {
            break;
        }
    }
    return source;
}

// Initialise the sensorId ready for a run
void sensor::FarFieldSensor::initialise() {
    std::cout << "FarFieldSensor::initialise" << std::endl;
    // Get the source frequency information (we only use the first source)
    freqStart_ = 0.0;
    freqSpacing_ = 0.0;
    numChannels_ = 0;
    source::PlaneWaveSource* source = getSource();
    if(source != nullptr) {
        freqStart_ = source->frequencies().first().value();
        freqSpacing_ = source->frequencies().spacing().value();
        numChannels_ = static_cast<size_t>(source->frequencies().n().value());
    }
    // Work out the length of the time series, we need space for one whole
    // cycle of the frequency spacing (not the start frequency otherwise
    // we may not have enough resolution for the frequency steps).
    nSamples_ = 0;
    if(freqSpacing_ > 0.0) {
        nSamples_ = static_cast<size_t>(std::floor(1.0 / freqSpacing_ / m_->p()->dt()));
    }
    // Work out the cell coordinates of the near field surface which we
    // will put half way into the scattered field zone
    box::Vector<size_t> p1 = m_->p()->geometry()->cellFloor(m_->p()->p1().value());
    box::Vector<size_t> p2 = m_->p()->geometry()->cellFloor(m_->p()->p2().value());
    size_t offset = m_->p()->scatteredFieldZoneSize() / 2U;
    x0_ = p1.x() - offset;
    x1_ = p2.x() + offset;
    y0_ = p1.y() - offset;
    y1_ = p2.y() + offset;
    z0_ = p1.z() - offset;
    z1_ = p2.z() + offset;
    // Initialise the faces
    faceX0_.initialise({x0_, y0_, z0_}, {x0_, y1_, z1_}, nSamples_);
    faceX1_.initialise({x1_, y0_, z0_}, {x1_, y1_, z1_}, nSamples_);
    faceY0_.initialise({x0_, y0_, z0_}, {x1_, y0_, z1_}, nSamples_);
    faceY1_.initialise({x0_, y1_, z0_}, {x1_, y1_, z1_}, nSamples_);
    faceZ0_.initialise({x0_, y0_, z0_}, {x1_, y1_, z0_}, nSamples_);
    faceZ1_.initialise({x0_, y0_, z1_}, {x1_, y1_, z1_}, nSamples_);
    std::cout << "FarFieldSensor::complete" << std::endl;
}

// Collect data for the sensorId
void sensor::FarFieldSensor::collect() {
    faceX0_.collect(m_);
    faceX1_.collect(m_);
    faceY0_.collect(m_);
    faceY1_.collect(m_);
    faceZ0_.collect(m_);
    faceZ1_.collect(m_);
}

// Make the sensorId calculations
void sensor::FarFieldSensor::calculate() {
    faceX0_.calculate();
    faceX1_.calculate();
    faceY0_.calculate();
    faceY1_.calculate();
    faceZ0_.calculate();
    faceZ1_.calculate();
}

// Return sensorId information
void sensor::FarFieldSensor::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == attenuationBack_ || spec == attenuationFront_ || spec == phaseShiftBack_
       || spec == phaseShiftFront_) {
        info->channelUsed_.resize(1);
        info->channelUsed_[0] = true;
    } else if(spec == timeSeriesFaceX0_) {
        info->xSize_ = faceX0_.width();
        info->ySize_ = faceX0_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else if(spec == timeSeriesFaceX1_) {
        info->xSize_ = faceX1_.width();
        info->ySize_ = faceX1_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else if(spec == timeSeriesFaceY0_) {
        info->xSize_ = faceY0_.width();
        info->ySize_ = faceY0_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else if(spec == timeSeriesFaceY1_) {
        info->xSize_ = faceY1_.width();
        info->ySize_ = faceY1_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else if(spec == timeSeriesFaceZ0_) {
        info->xSize_ = faceZ0_.width();
        info->ySize_ = faceZ0_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else if(spec == timeSeriesFaceZ1_) {
        info->xSize_ = faceZ1_.width();
        info->ySize_ = faceZ1_.height();
        info->min_ = -1.0;
        info->max_ = 1.0;
        info->xUnits_ = "";
        info->yUnits_ = "";
    } else {
        Sensor::getInfo(spec, info);
    }
}

// Return sensorId data for display
void sensor::FarFieldSensor::getData(DataSpec* spec, size_t x, std::vector<double>& dy,
                                     double& dx) {
    if(spec == attenuationBack_) {
        // TODO:
    } else if(spec == attenuationFront_) {
        // TODO:
    } else if(spec == phaseShiftBack_) {
        // TODO:
    } else if(spec == phaseShiftFront_) {
        // TODO:
    } else {
        Sensor::getData(spec, x, dy, dx);
    }
}

// Return sensorId data for display
void sensor::FarFieldSensor::getData(DataSpec* spec, size_t x, size_t y, double* d, double* dx,
                                     double* dy) {
    if(spec == timeSeriesFaceX0_) {
        *d = faceX0_.getEMag(x, y);
    } else if(spec == timeSeriesFaceX1_) {
        *d = faceX1_.getEMag(x, y);
    } else if(spec == timeSeriesFaceY0_) {
        *d = faceY0_.getEMag(x, y);
    } else if(spec == timeSeriesFaceY1_) {
        *d = faceY1_.getEMag(x, y);
    } else if(spec == timeSeriesFaceZ0_) {
        *d = faceZ0_.getEMag(x, y);
    } else if(spec == timeSeriesFaceZ1_) {
        *d = faceZ1_.getEMag(x, y);
    } else {
        Sensor::getData(spec, x, y, d, dx, dy);
    }
}

// Return sensorId meta-data for display
void sensor::FarFieldSensor::getMetaData(DataSpec* /*spec*/, int /*id*/, double* d,
                                         std::string& /*units*/) {
    *d = 0.0;
    // TODO:
}

// Set a meta data item
void sensor::FarFieldSensor::setMetaData(DataSpec* /*spec*/, int /*id*/,
                                         double /*d1*/, double /*d2*/) {
    // TODO:
}

// Is the sensorId at the specified location
bool sensor::FarFieldSensor::isAt(const box::Vector<size_t>& pos) {
    // Indicate the position of the near field surface
    bool result = false;
    result = result || faceX0_.isAt(pos);
    result = result || faceX1_.isAt(pos);
    result = result || faceY0_.isAt(pos);
    result = result || faceY1_.isAt(pos);
    result = result || faceZ0_.isAt(pos);
    result = result || faceZ1_.isAt(pos);
    return result;
}

// Write the sensorId state information to the binary file
bool sensor::FarFieldSensor::writeData(std::ofstream& file) {
    bool result = true;
    box::BinaryFileHeader header("FARFIELDSENSOR", 1, 0);
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    // TODO:
    return result;
}

// Read the sensorId state information from the binary file
bool sensor::FarFieldSensor::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("FARFIELDSENSOR")) {
        result = true;
        switch(header.version()) {
            case 1:
                // TODO:
                // Deliberate fall through
            default:
                break;
        }
    }
    return result;
}

// Write the configuration to the DOM object
void sensor::FarFieldSensor::writeConfig(xml::DomObject& p) const {
    // Base class first
    Sensor::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("distance") << distance_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::FarFieldSensor::readConfig(xml::DomObject& p) {
    // Base class first
    Sensor::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("distance") >> distance_;
    p >> xml::Close();
}
