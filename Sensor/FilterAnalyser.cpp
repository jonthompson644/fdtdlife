/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FilterAnalyser.h"
#include <Sensor/ArraySensor.h>

// Write the object to the XML DOM
void sensor::FilterAnalyser::writeConfig(xml::DomObject& p) const {
    // Base class first
    Analyser::writeConfig(p);
    // Now my stuff
    p << xml::Reopen();
    p << xml::Obj("filtertype") << filterType_;
    p << xml::Obj("filtermode") << filterMode_;
    p << xml::Obj("order") << order_;
    p << xml::Obj("cutofffrequency") << cutOffFrequency_;
    p << xml::Obj("maxfrequency") << maxFrequency_;
    p << xml::Obj("passbandripple") << passbandRipple_;
    p << xml::Close();
}

// Read the object from the XML DOM
void sensor::FilterAnalyser::readConfig(xml::DomObject& p) {
    // Base class first
    Analyser::readConfig(p);
    // Now my stuff
    p >> xml::Reopen();
    p >> xml::Obj("filtertype") >> filterType_;
    p >> xml::Obj("filtermode") >> filterMode_;
    p >> xml::Obj("order") >> order_;
    p >> xml::Obj("cutofffrequency") >> cutOffFrequency_;
    p >> xml::Obj("maxfrequency") >> maxFrequency_;
    p >> xml::Obj("passbandripple") >> passbandRipple_;
    p >> xml::Close();
}

// Initialise the analyser
// Create the table of values that represent the filter
void sensor::FilterAnalyser::initialise() {
    // Base class first
    Analyser::initialise();
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        sensor->initialise();
        size_t numSamples = sensor->frequencies().n();
        // Generate the reference array using the appropriate filter equation
        reference_.resize(numSamples);
        delta_.resize(numSamples);
        for(size_t i = 0; i < numSamples; i++) {
            double frequency = sensor->frequencies().first() +
                               i * sensor->frequencies().spacing();
            std::complex<double> value;
            std::complex<double> a;
            switch(filterMode_) {
                case FilterMode::lowPass:
                    a = {0.0, frequency / cutOffFrequency_.value()};
                    break;
                case FilterMode::highPass:
                    a = {0.0, cutOffFrequency_.value() / frequency};
                    break;
            }
            switch(filterType_) {
                case FilterType::butterworth: {
                    size_t limit;
                    if(order_ % 2) {
                        // Odd numbered order
                        value = a + 1.0;
                        limit = (order_ - 1U) / 2U;
                    } else {
                        // Even numbered order
                        value = 1.0;
                        limit = order_ / 2U;
                    }
                    for(size_t k = 1; k <= limit; k++) {
                        value *= (a * a - a * 2.0 *
                                          cos((2.0 * k + order_ - 1.0) /
                                              2.0 / order_ * box::Constants::pi_) + 1.0);
                    }
                    break;
                }
                case FilterType::chebyshevI: {
                    std::complex<double> epsilon = std::sqrt(
                            std::pow(10.0, 0.1 * passbandRipple_.value()) -
                            std::complex<double>(1.0, 0.0));
                    value = std::pow(2.0, order_ - 1.0) * epsilon;
                    std::complex<double> h = 1.0 / order_ * std::asinh(1.0 / epsilon);
                    for(size_t m = 1; m <= order_; m++) {
                        double theta =
                                (2.0 * m - 1.0) * box::Constants::pi_ / order_ / 2.0;
                        std::complex<double> term = a;
                        term += std::sinh(h) * sin(theta);
                        term -= std::cosh(h) * cos(theta) *
                                std::complex<double>(0.0, 1.0);
                        value *= term;
                    }
                    break;
                }
                default:
                    break;
            }
            value = 1.0 / value;
            double reference;
            double delta;
            switch(property_) {
                case Property::transmittance:
                    if(frequency > maxFrequency_.value()) {
                        reference = 0.5;
                        delta = 0.5;
                    } else {
                        reference = std::abs(value) * std::abs(value);
                        delta = 0.0;
                    }
                    break;
                case Property::phaseShift:
                    if(frequency > maxFrequency_.value()) {
                        reference = 0.0;
                        delta = box::Constants::pi_ * box::Constants::half_;
                    } else {
                        reference = std::arg(value);
                        delta = 0.0;
                    }
                    break;
            }
            reference_[i] = reference;
            delta_[i] = delta;
        }
    }
}

// Calculate the error using the current state of the sensorId
void sensor::FilterAnalyser::calculate() {
    error_ = 0.0;
    // Get the sensorId
    auto sensor = getSensor();
    if(sensor != nullptr) {
        // Get the information from the sensorId
        size_t numSamples = sensor->frequencies().n();
        data_.resize(numSamples);
        dataType_ = DataType::transmittance;
        minX_ = sensor->frequencies().first();
        stepX_ = sensor->frequencies().spacing();
        // Record the data and calculate the error
        double ax = 0.0;
        double ay = 0.0;
        for(size_t i = 0; i < numSamples; i++) {
            double frequency = i * stepX_ + minX_;
            // Get the point
            switch(property_) {
                case Property::transmittance:
                    sensor->transmittanceAt(0, 0, frequency, ax, ay,
                                            location_ != SensorLocation::highestZ);
                    break;
                case Property::phaseShift:
                    sensor->phaseShiftAt(0, 0, frequency, ax, ay,
                                            location_ != SensorLocation::highestZ);
                    break;
            }
            double measured = component_ == SensorComponent::y ? ay : ax;
            data_[i] = measured;
            // The error
            if(reference_.size() > i) {
                double reference = reference_[i];
                double delta = delta_[i];
                double diff = 0.0;
                if(measured > (reference + delta)) {
                    diff = measured - reference - delta;
                } else if(measured < (reference - delta)) {
                    diff = reference - delta - measured;
                }
                error_ += diff * diff;
            }
            // Calculate the min/max
            if(i == 0) {
                minY_ = measured;
                maxY_ = measured;
            } else {
                minY_ = std::min(minY_, measured);
                maxY_ = std::max(maxY_, measured);
            }
        }
    }
}

// Evaluate any expressions
void sensor::FilterAnalyser::evaluate(box::Expression::Context& c) {
    // Base class first
    Analyser::evaluate(c);
    // Now my stuff
    cutOffFrequency_.evaluate(c);
    maxFrequency_.evaluate(c);
    passbandRipple_.evaluate(c);
}
