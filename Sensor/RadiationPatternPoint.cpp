/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RadiationPatternPoint.h"
#include <Fdtd/Model.h>
#include <Fdtd/EField.h>
#include <Box/Constants.h>
#include <algorithm>

// Prepare the sensorId point for collection
void sensor::RadiationPatternPoint::initialise(const box::Vector<size_t>& cellPosition,
                                               const box::FrequencySeq& frequencies,
                                               size_t numThreadsToUse,
                                               double samplePeriod, size_t numSamples) {
    point_.initialise(frequencies, numThreadsToUse, samplePeriod, {true, true, true},
            numSamples, false);
    cellPosition_ = cellPosition;
}

// Return true if this point is on the given coordinate
bool sensor::RadiationPatternPoint::isAt(const box::Vector<size_t>& pos) {
    return pos == cellPosition_;
}

// Collect a data point
void sensor::RadiationPatternPoint::collect(fdtd::Model* m) {
    box::Vector<double> e = m->e()->value(cellPosition_);
    point_.collect(e.x(), e.y(), e.z());
}

// Return the range of the data stored in the time series
void sensor::RadiationPatternPoint::getMinMax(double& min, double& max) {
    min = 0.0;
    max = 0.0;
    if(!point_.timeSeriesX().empty()) {
        min = point_.timeSeriesX()[0];
        max = min;
    }
    for(auto s : point_.timeSeriesX()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
    for(auto s : point_.timeSeriesY()) {
        min = std::min(s, min);
        max = std::max(s, max);
    }
}

// Calculate the DFTs
void sensor::RadiationPatternPoint::calculate() {
    point_.calculate();
}

// Return the power power from a spectrum bin with the given polarisation
void sensor::RadiationPatternPoint::powerAt(size_t spectrumBin, double polarisation,
        double& power, double& crossPower) {
    double x = std::abs(point_.spectrumX()[spectrumBin]);
    double y = std::abs(point_.spectrumY()[spectrumBin]);
    double cosPhi = std::cos(polarisation * box::Constants::deg2rad_);
    double sinPhi = std::sin(polarisation * box::Constants::deg2rad_);
    double den = cosPhi * cosPhi + sinPhi * sinPhi;
    // The power at the cross polarisation
    crossPower = (y * cosPhi - x * sinPhi) / den;
    // The power at the polarisation
    power = (y * sinPhi + x * cosPhi) / den;
}

