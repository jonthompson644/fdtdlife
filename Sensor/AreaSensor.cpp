/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AreaSensor.h"
#include <Fdtd/Model.h>
#include <Fdtd/Configuration.h>
#include <Xml/DomObject.h>
#include <Fdtd/EField.h>

// Constructor
sensor::AreaSensor::AreaSensor() :
        frequencies_(1 * box::Constants::giga_,
                     1 * box::Constants::giga_, 1) {
    timeSeriesSpec_ = new DataSpec(this, "TimeSeries",
                                   DataSpec::modeTimeSeries, {"X", "Y"});
    attenuationSpec_ = new DataSpec(this, "Power",
                                    DataSpec::modePower, {"X", "Y"});
    phaseShiftSpec_ = new DataSpec(this, "Phase",
                                   DataSpec::modePhaseShift, {"X", "Y"});
    specs_.push_back(timeSeriesSpec_);
    specs_.push_back(attenuationSpec_);
    specs_.push_back(phaseShiftSpec_);
}

// Prepare the sensorId for a run
void sensor::AreaSensor::initialise() {
    // The cell coordinate of the area
    box::Vector<double> offset;
    switch(orientation_) {
        case box::Constants::orientationXPlane:
            offset = box::Vector<double>(
                    m_->p()->dr().x().value() / 2.0, width_ / 2.0, height_ / 2.0);
            break;
        case box::Constants::orientationYPlane:
            offset = box::Vector<double>(
                    height_ / 2.0, m_->p()->dr().y().value() / 2.0, width_ / 2.0);
            break;
        case box::Constants::orientationZPlane:
            offset = box::Vector<double>(
                    width_ / 2.0, height_ / 2.0, m_->p()->dr().z().value() / 2.0);
            break;
    }
    topLeftCell_ = m_->p()->geometry()->cellRound(center_ - offset);
    bottomRightCell_ = m_->p()->geometry()->cellRound(center_ + offset);
    // Initialise the point
    frequencies_ = m_->sources().frequencyInfo();
    point_.initialise(frequencies_,
                      (size_t) m_->p()->numThreadsUsed(),
                      m_->p()->dt(),
                      {true, true, true},
                      frequencies_.requiredSamples(
                              m_->p()->dt()), false);
}

// Write the configuration to the DOM object
void sensor::AreaSensor::writeConfig(xml::DomObject& p) const {
    // Base class first
    Sensor::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("centerx") << center_.x();
    p << xml::Obj("centery") << center_.y();
    p << xml::Obj("centerz") << center_.z();
    p << xml::Obj("orientation") << orientation_;
    p << xml::Obj("width") << width_;
    p << xml::Obj("height") << height_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void sensor::AreaSensor::readConfig(xml::DomObject& p) {
    // Base class first
    Sensor::readConfig(p);
    // Now my things
    double v;
    p >> xml::Reopen();
    p >> xml::Obj("centerx") >> v;
    center_.x(v);
    p >> xml::Obj("centery") >> v;
    center_.y(v);
    p >> xml::Obj("centerz") >> v;
    center_.z(v);
    p >> xml::Obj("orientation") >> orientation_;
    p >> xml::Obj("width") >> width_;
    p >> xml::Obj("height") >> height_;
    p >> xml::Close();
}

// Is the sensorId at the specified location
bool sensor::AreaSensor::isAt(const box::Vector<size_t>& pos) {
    bool result = true;
    result = result && pos.x() >= topLeftCell_.x() && pos.x() < bottomRightCell_.x();
    result = result && pos.y() >= topLeftCell_.y() && pos.y() < bottomRightCell_.y();
    result = result && pos.z() >= topLeftCell_.z() && pos.z() < bottomRightCell_.z();
    return result;
}

// Collect data for the sensorId
void sensor::AreaSensor::collect() {
    box::Vector<double> v(0.0);
    double numSamples = 0.0;
    for(size_t i = topLeftCell_.x(); i < bottomRightCell_.x(); i++) {
        for(size_t j = topLeftCell_.y(); j < bottomRightCell_.y(); j++) {
            for(size_t k = topLeftCell_.z(); k < bottomRightCell_.z(); k++) {
                v += m_->e()->value({i, j, k});
                numSamples += 1.0;
            }
        }
    }
    if(numSamples > 0.0) {
        v = v / numSamples;
    }
    switch(orientation_) {
        case box::Constants::orientationXPlane:
            point_.collect(v.y(), v.z(), v.x());
            break;
        case box::Constants::orientationYPlane:
            point_.collect(v.z(), v.x(), v.y());
            break;
        case box::Constants::orientationZPlane:
            point_.collect(v.x(), v.y(), v.z());
            break;
    }
}

// Perform calculations
void sensor::AreaSensor::calculate() {
    point_.calculate();
}

// Return sensorId information
void sensor::AreaSensor::getInfo(DataSpec* spec, DataSpec::Info* info) {
    if(spec == timeSeriesSpec_) {
        point_.timeSeriesMinMax(info->min_, info->max_);
        info->xSize_ = (int) point_.numSamples();
        info->yUnits_ = "V/m";
        info->xUnits_ = "ps";
        info->xLeft_ = -(point_.numSamples() * m_->p()->dt()) / box::Constants::pico_;
        info->xRight_ = 0;
    } else if(spec == attenuationSpec_) {
        info->xSize_ = (int) frequencies_.n();
        point_.powerMinMax(info->min_, info->max_);
        info->xUnits_ = "GHz";
        info->yUnits_ = "V/m";
        info->xLeft_ = frequencies_.first() / box::Constants::giga_;
        info->xRight_ = frequencies_.last() / box::Constants::giga_;
    } else if(spec == phaseShiftSpec_) {
        info->xSize_ = (int) frequencies_.n();
        info->min_ = -180.0;
        info->max_ = +180.0;
        info->xUnits_ = "GHz";
        info->yUnits_ = "deg";
        info->xLeft_ = frequencies_.first() / box::Constants::giga_;
        info->xRight_ = frequencies_.last() / box::Constants::giga_;
    } else {
        Sensor::getInfo(spec, info);
    }
}

// Return 1D sensorId data for display
void
sensor::AreaSensor::getData(DataSpec* spec, size_t x, std::vector<double>& dy, double& dx) {
    if(spec == timeSeriesSpec_) {
        if(x < point_.numSamples()) {
            if(!dy.empty()) {
                dy[0] = point_.timeSeriesX()[x];
            }
            if(dy.size() > 1) {
                dy[1] = point_.timeSeriesY()[x];
            }
            dx = m_->p()->dt() * (double) (point_.numSamples() - x);
        }
    } else if(spec == attenuationSpec_) {
        if(x < frequencies_.n()) {
            if(!dy.empty()) {
                dy[0] = point_.spectrumX().amplitude()[x];
            }
            if(dy.size() > 1) {
                dy[1] = point_.spectrumY().amplitude()[x];
            }
            dx = frequencies_.frequency((size_t) x);
        }
    } else if(spec == phaseShiftSpec_) {
        if(x < frequencies_.n()) {
            if(!dy.empty()) {
                dy[0] = point_.spectrumX().phase()[x];
            }
            if(dy.size() > 1) {
                dy[1] = point_.spectrumY().phase()[x];
            }
            dx = frequencies_.frequency((size_t) x);
        }
    } else {
        Sensor::getData(spec, x, dy, dx);
    }
}

// Find the signal components in the bin nearest the specified frequency
void sensor::AreaSensor::at(double frequency, double& x, double& y) {
    // Which bin?
    size_t bin = frequencies_.channel(frequency);
    // The magnitude at this bin
    x = point_.spectrumX().amplitude()[bin];
    y = point_.spectrumY().amplitude()[bin];
}

