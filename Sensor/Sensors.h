/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SENSORS_H
#define FDTDLIFE_SENSORS_H

#include <list>
#include <Box/Vector.h>
#include <Xml/DomObject.h>
#include <Box/Factory.h>

namespace fdtd { class Model; }

namespace sensor {

    class Sensor;

    class DataSpec;

    class ArraySensor;

    // A class that manages a model's sensors
    class Sensors {
    public:
        // Constants
        enum {modeNone=-1, modeTimeSeries=0, modeCrossCorrelation,
            modeFourierTransform, modeTwoD, modeGeortzelDft,
            modeArraySensor, modeMaterial, modeFarFieldSensor,
            modeRadiationPattern, modeArea};

        // Construction
        explicit Sensors(fdtd::Model* m);

        // API used by the sensorId base class
        void remove(Sensor* sensor);

        // General API
        Sensor* find(int identifier);
        Sensor* find(const std::string& name);
        Sensor* getAt(const box::Vector<size_t>& p);
        DataSpec* getDataSpec(int identifier, const std::string& data);
        void getArraySensors(std::list<ArraySensor*>& arraySensors);  // TODO: Remove this!
        void clear();
        void initialise();
        void collect();
        void initialCollect();
        void calculate();
        void finalCalculate();
        void animate();
        bool writeData(std::ofstream& file);
        bool readData(std::ifstream& file);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Sensors& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Sensors& s) {
            s.readConfig(o);
            return o;
        }
        bool wantsCsv();
        void writeCsv(std::ofstream* file);
        void clearSeqGenerated();
        Sensor* makeSensor(int mode);
        Sensor* makeSensor(int id, int mode);
        Sensor* makeSensor(xml::DomObject& o);

        // Iterators
        typedef std::list<std::unique_ptr<Sensor>>::iterator iterator;
        iterator begin() { return sensors_.begin(); }
        iterator end() { return sensors_.end(); }

        // Getters
        box::Factory<Sensor>& factory() { return factory_; }
        [[nodiscard]] const std::list<std::unique_ptr<Sensor>>& sensors() const {
            return sensors_;
        }
        fdtd::Model* m() { return m_; }

        // Setters

    protected:
        // Members
        std::list<std::unique_ptr<Sensor>> sensors_;  // The list of sensors
        box::Factory<Sensor> factory_{0, "mode2"};
        fdtd::Model* m_;  // The model
    };
}


#endif //FDTDLIFE_SENSORS_H
