/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSREFLECTIONANALYSER_H
#define FDTDLIFE_LENSREFLECTIONANALYSER_H

#include "Analyser.h"

namespace sensor {

    // Analyses reflections from a lens by taking a reference early in a
    // run, before reflections have had a chance to return from the lens
    // structure, and then comparing with the final data.
    class LensReflectionAnalyser : public Analyser {
    public:
        // Construction
        LensReflectionAnalyser();

        // Overrides of Analyser
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void evaluate(box::Expression::Context& c) override;
        void collect() override;
        void calculate() override;
        void stepComplete() override;

        // API
        void captureReferenceNow();
        void setData(const std::vector<double>& data);
        void setReference(const std::vector<double>& data);

        // Getters
        [[nodiscard]] const box::Expression& frequency() const { return frequency_; }
        [[nodiscard]] const box::Expression& referenceTime() const { return referenceTime_; }

        // Setters
        void frequency(const box::Expression& v) { frequency_ = v; }
        void referenceTime(const box::Expression& v) { referenceTime_ = v; }

    protected:
        // Parameters
        box::Expression frequency_{1e9};   // The frequency at which to do the test
        box::Expression referenceTime_{0.0};   // The time at which to take the reference

        // Helpers
        void captureReference();
    };
}


#endif //FDTDLIFE_LENSREFLECTIONANALYSER_H
