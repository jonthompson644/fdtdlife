/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FARFIELDSENSOR_H
#define FDTDLIFE_FARFIELDSENSOR_H

#include "Sensor.h"
#include "NearFieldFace.h"
#include <Box/Constants.h>

namespace source { class PlaneWaveSource; }

namespace sensor {

    class NearFieldCell;

    class FarFieldSensor : public Sensor {
    public:
        // Construction
        FarFieldSensor();
        ~FarFieldSensor() override;
        // Overrides
        void initialise() override;
        void collect() override;
        void calculate() override;
        void getInfo(DataSpec* spec, DataSpec::Info* info) override;
        void getData(DataSpec* spec, size_t x, std::vector<double>& dy, double& dx) override;
        void getData(DataSpec* spec, size_t x, size_t y, double* d, double* dx,
                     double* dy) override;
        void getMetaData(DataSpec* spec, int id, double* d, std::string& units) override;
        void setMetaData(DataSpec* spec, int id, double d1, double d2) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        bool writeData(std::ofstream& file) override;
        bool readData(std::ifstream& file) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;

        // Setters
        void distance(double distance) { distance_ = distance; }

        // Getters
        double distance() const { return distance_; }

        // API for parts
        source::PlaneWaveSource* getSource();

    protected:
        // Data specifications
        DataSpec* attenuationFront_{};
        DataSpec* phaseShiftFront_{};
        DataSpec* attenuationBack_{};
        DataSpec* phaseShiftBack_{};
        DataSpec* timeSeriesFaceX0_{};
        DataSpec* timeSeriesFaceX1_{};
        DataSpec* timeSeriesFaceY0_{};
        DataSpec* timeSeriesFaceY1_{};
        DataSpec* timeSeriesFaceZ0_{};
        DataSpec* timeSeriesFaceZ1_{};
        // Configuration
        double distance_{1.0};
        // Operating parameters
        double freqStart_{};
        double freqSpacing_{};
        size_t numChannels_{};
        size_t nSamples_{};
        size_t x0_{};
        size_t x1_{};
        size_t y0_{};
        size_t y1_{};
        size_t z0_{};
        size_t z1_{};
        NearFieldFace faceX0_;
        NearFieldFace faceX1_;
        NearFieldFace faceY0_;
        NearFieldFace faceY1_;
        NearFieldFace faceZ0_;
        NearFieldFace faceZ1_;
    };
}


#endif //FDTDLIFE_FARFIELDSENSOR_H
