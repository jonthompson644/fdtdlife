/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_NEARFIELDCELL_H
#define FDTDLIFE_NEARFIELDCELL_H

#include <vector>
#include <Box/Vector.h>

namespace fdtd {class Model;}

namespace sensor {

    class NearFieldCell {
    public:
        // Construction
        NearFieldCell();
        ~NearFieldCell();
        NearFieldCell(const NearFieldCell& other);
        // API
        void initialise(const box::Vector<size_t>& pos, size_t nSamples);
        NearFieldCell& operator=(const NearFieldCell& other);
        void collect(fdtd::Model* m);
        double getEMag();
    protected:
        // Helpers
        void clearMemory();
        // Data
        size_t nSamples_ {};
        size_t nextSample_ {};
        size_t prevSample_ {};
        box::Vector<size_t> pos_;
        bool needsClear_;
        double* timeSeriesEPrevX_ {};
        double* timeSeriesEX_ {};
        double* timeSeriesHX_ {};
        double* timeSeriesEPrevY_ {};
        double* timeSeriesEY_ {};
        double* timeSeriesHY_ {};
        double* timeSeriesEPrevZ_ {};
        double* timeSeriesEZ_ {};
        double* timeSeriesHZ_ {};
    };

}


#endif //FDTDLIFE_NEARFIELDCELL_H
