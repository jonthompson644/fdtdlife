/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_REFERENCEDCOMPONENT_H
#define FDTDLIFE_REFERENCEDCOMPONENT_H

#include <vector>
#include <complex>
#include <Box/Range.h>

namespace box {class GeortzelSpectrum;}

namespace sensor {

    // A class that contains spectrum data that has been processed
    // against a reference for one component.
    class ReferencedComponent {
    public:
        // Construction
        ReferencedComponent();

        // API
        void initialise(size_t numChannels);
        void calculate(const box::GeortzelSpectrum& data, const box::GeortzelSpectrum& reference);
        void calculate(size_t channel, std::complex<double> data, std::complex<double> reference);
        void set(size_t channel, double transmittance, double phaseShift, const std::complex<double>& admittance);
        void evaluateRanges();

        // Getters
        const std::vector<std::complex<double>>& admittance() const {return admittance_;}
        const std::vector<double>& transmittance() const {return transmittance_;}
        const std::vector<double>& phaseShift() const {return phaseShift_;}
        const box::Range<double>& admittanceRange() const {return admittanceRange_;}
        const box::Range<double>& transmittanceRange() const {return transmittanceRange_;}
        const box::Range<double>& phaseShiftRange() const {return phaseShiftRange_;}

    protected:
        // Members
        std::vector<std::complex<double>> admittance_;  // The admittance data
        std::vector<double> transmittance_;  // The transmittance spectrum
        std::vector<double> phaseShift_;  // The phase shift spectrum (in degrees)
        box::Range<double> admittanceRange_;  // The range of the admittance data
        box::Range<double> transmittanceRange_;  // The range of the transmittance data
        box::Range<double> phaseShiftRange_;  // The range of the phase shift data
    };

}

#endif //FDTDLIFE_REFERENCEDCOMPONENT_H
