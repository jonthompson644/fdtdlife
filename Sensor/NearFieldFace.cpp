/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "NearFieldFace.h"
#include <cassert>

// Initialise ready for a run
void sensor::NearFieldFace::initialise(const box::Vector<size_t>& p1, const box::Vector<size_t>& p2, size_t nSamples) {
    // Work out which face and the face size
    p1_ = p1;
    p2_ = p2;
    if(p2_.x() == p1_.x()) {
        w_ = p2_.y() - p1_.y();
        h_ = p2_.z() - p1_.z();
        face_ = xFace;
    } else if(p2_.y() == p1_.y()) {
        w_ = p2_.z() - p1_.z();
        h_ = p2_.x() - p1_.x();
        face_ = yFace;
    } else if(p2_.z() == p1_.z()) {
        w_ = p2_.x() - p1_.x();
        h_ = p2_.y() - p1_.y();
        face_ = zFace;
    } else {
        // Shouldn't get here.
    }
    // Set the cell array size
    cells_.resize(h_);
    for(auto& row : cells_) {
        row.resize(w_);
    }
    // Initialise the cells
    for(size_t row=0; row<cells_.size(); row++) {
        for(size_t col=0; col<cells_[row].size(); col++) {
            box::Vector<size_t> pos;
            switch(face_) {
                case xFace:
                    pos.x(p1_.x());
                    pos.y(p1_.y()+col);
                    pos.z(p1_.z()+row);
                    break;
                case yFace:
                    pos.x(p1_.x()+row);
                    pos.y(p1_.y());
                    pos.z(p1_.z()+col);
                    break;
                case zFace:
                    pos.x(p1_.x()+col);
                    pos.y(p1_.y()+row);
                    pos.z(p1_.z());
                    break;
            }
            cells_[row][col].initialise(pos, nSamples);
        }
    }
}

// Collect data for this face
void sensor::NearFieldFace::collect(fdtd::Model* m) {
    for(auto& row : cells_) {
        for(auto& cell : row) {
            cell.collect(m);
        }
    }
}

// Calculate the DFTs
void sensor::NearFieldFace::calculate() {
    // TODO:
}

// Return data at a point
double sensor::NearFieldFace::getEMag(size_t x, size_t y) {
    return cells_[y][x].getEMag();
}

// Return true if the given position is on the near field face
bool sensor::NearFieldFace::isAt(const box::Vector<size_t>& pos) {
    bool result = false;
    switch(face_) {
        case xFace:
            if(pos.x() == p1_.x()) {
                result = pos.y() >= p1_.y() && pos.y() <= p2_.y() &&
                        pos.z() >= p1_.z() && pos.z() <= p2_.z();
            }
            break;
        case yFace:
            if(pos.y() == p1_.y()) {
                result = pos.x() >= p1_.x() && pos.x() <= p2_.x() &&
                         pos.z() >= p1_.z() && pos.z() <= p2_.z();
            }
            break;
        case zFace:
            if(pos.z() == p1_.z()) {
                result = pos.x() >= p1_.x() && pos.x() <= p2_.x() &&
                         pos.y() >= p1_.y() && pos.y() <= p2_.y();
            }
            break;
    }
    return result;
}
