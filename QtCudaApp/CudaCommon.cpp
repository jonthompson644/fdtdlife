/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaCommon.h"
#include "CudaIndexing.h"
#include <Model.h>
#include <Domain.h>
#include <Material.h>
#include <Parameters.h>

// Constructor
CudaCommon::CudaCommon(fdtd::Model* model) :
        m_(model),
        material_(&cudaProxy_, "material_"),
        boundaryDepthX_(&cudaProxy_, "boundaryDepthX_"),
        boundaryDepthY_(&cudaProxy_, "boundaryDepthY_"),
        boundaryDepthZ_(&cudaProxy_, "boundaryDepthZ_"),
        drX_(&cudaProxy_, "drX_"),
        drY_(&cudaProxy_, "drY_"),
        drZ_(&cudaProxy_, "drZ_"),
        info_(&cudaProxy_, "info_") {
}

// Initialise the CUDA memory ready for a run
void CudaCommon::initialise() {
    arraySize_ = (size_t) m_->p()->arraySize();
    matDepthArraySize_ = fdtd::Parameters::maxNumMaterials * ((size_t)m_->p()->boundarySize() + 2);
    material_.alloc(arraySize_);
    boundaryDepthX_.alloc(arraySize_);
    boundaryDepthY_.alloc(arraySize_);
    boundaryDepthZ_.alloc(arraySize_);
    drX_.alloc(matDepthArraySize_);
    drY_.alloc(matDepthArraySize_);
    drZ_.alloc(matDepthArraySize_);
    initialiseMaterial();
    initialiseBoundaryDepth();
    initialiseCellSize();
    initialiseInfo();
}

// Initialise the CUDA material array
void CudaCommon::initialiseMaterial() {
    for(int k = 0; k < m_->p()->n().z(); k++) {
        for(int j = 0; j < m_->p()->n().y(); j++) {
            for(int i = 0; i < m_->p()->n().x(); i++) {
                int idx = m_->p()->index(i, j, k);
                material_[idx] = (char) m_->d()->getMaterialAt(i, j, k)->index();
            }
        }
    }
}

// Initialise the boundary depth arrays
void CudaCommon::initialiseBoundaryDepth() {
    int i, j, k;
    fdtd::Parameters* p = m_->p();
    box::Vector<int> boundaryDepth;
    for(k = 0; k < p->n().z(); k++) {
        // Z boundary depth if this is an absorbing boundary
        boundaryDepth.z(-1);
        if(p->boundaryZ() == fdtd::Parameters::Boundary::absorbing) {
            if(k < p->boundarySize()) {
                boundaryDepth.z(p->boundarySize() - k - 1);
            } else if(k >= (p->n().z() - p->boundarySize())) {
                boundaryDepth.z(k - (p->n().z() - p->boundarySize()));
            }
        }
        for(j = 0; j < p->n().y(); j++) {
            // Y boundary depth if this is an absorbing boundary
            boundaryDepth.y(-1);
            if(p->boundaryY() == fdtd::Parameters::Boundary::absorbing) {
                if(j < p->boundarySize()) {
                    boundaryDepth.y(p->boundarySize() - j - 1);
                } else if(j >= (p->n().y() - p->boundarySize())) {
                    boundaryDepth.y(j - (p->n().y() - p->boundarySize()));
                }
            }
            for(i = 0; i < p->n().x(); i++) {
                // X boundary depth if this is an absorbing boundary
                boundaryDepth.x(-1);
                if(p->boundaryX() == fdtd::Parameters::Boundary::absorbing) {
                    if(i < p->boundarySize()) {
                        boundaryDepth.x(p->boundarySize() - i - 1);
                    } else if(i >= (p->n().x() - p->boundarySize())) {
                        boundaryDepth.x(i - (p->n().x() - p->boundarySize()));
                    }
                }
                int idx = p->index(i, j, k);
                boundaryDepthX_[idx] = (char) boundaryDepth.x();
                boundaryDepthY_[idx] = (char) boundaryDepth.y();
                boundaryDepthZ_[idx] = (char) boundaryDepth.z();
            }
        }
    }
}

// Return an index into a material constant array
// Boundary depth ranges from -1 to p_->boundarySize() inclusive
int CudaCommon::matDepthIndex(int materialId, int boundaryDepth) {
    int result = CUDAINDEXMATDEPTH(materialId, boundaryDepth, m_->p()->boundarySize());
    if(result >= matDepthArraySize_) {
        result = 0;
    }
    return result;
}

// Initialise the arrays that return the cell size from the material and boundary depth
// This performs the coordinate stretching required in the CPML material
void CudaCommon::initialiseCellSize() {
    for(auto pos : m_->d()->materialLookup()) {
        fdtd::Material* mat = pos.second;
        for(int boundaryDepth = -1; boundaryDepth <= m_->p()->boundarySize(); boundaryDepth++) {
            int idx = matDepthIndex(mat->index(), boundaryDepth);
            box::Vector<double> dr = mat->dr(box::Vector<int>(boundaryDepth));
            drX_[idx] = (float)dr.x();
            drY_[idx] = (float)dr.y();
            drZ_[idx] = (float)dr.z();
        }
    }
}

// Initialise the information structure
void CudaCommon::initialiseInfo() {
    fdtd::Parameters* p = m_->p();
    info_.alloc();
    CudaInfo* i = info_.host();
    i->arraySizeX_ = p->nx();
    i->arraySizeY_ = p->ny();
    i->arraySizeZ_ = p->nz();
    i->dt_ = p->dt();
    i->dx_ = p->dr().x();
    i->dy_ = p->dr().y();
    i->dz_ = p->dr().z();
    i->epsilon0_ = fdtd::Parameters::epsilon0;
    i->mu0_ = fdtd::Parameters::mu0;
    i->periodicX_ = p->boundaryX() == fdtd::Parameters::Boundary::periodic;
    i->periodicY_ = p->boundaryY() == fdtd::Parameters::Boundary::periodic;
    i->periodicZ_ = p->boundaryZ() == fdtd::Parameters::Boundary::periodic;
    i->scatteredFieldZoneSize_ = p->scatteredFieldZoneSize();
    i->tfEndX_ = p->tfEnd().x();
    i->tfEndY_ = p->tfEnd().y();
    i->tfEndZ_ = p->tfEnd().z();
    i->tfStartX_ = p->tfStart().x();
    i->tfStartY_ = p->tfStart().y();
    i->tfStartZ_ = p->tfStart().z();
    i->nx_ = p->n().x();
    i->ny_ = p->n().y();
    i->nz_ = p->n().z();
}

// Return a pointer to the proxy object
CudaProxy* CudaCommon::proxy() {
    CudaProxy* result = nullptr;
    if(this != nullptr) {
        result = &cudaProxy_;
    }
    return result;
}
