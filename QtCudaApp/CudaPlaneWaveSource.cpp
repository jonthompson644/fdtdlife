/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaPlaneWaveSource.h"
#include "CudaCommon.h"
#include "FdTdCuda.h"
#include "ECudaCpml.h"
#include "HCudaCpml.h"

// Constructor
CudaPlaneWaveSource::CudaPlaneWaveSource(int id, const std::string& name, double frequency, double amplitude,
                                         double polarisation, double pmlSigma, double spacing,
                                         int n, bool continuous, double time, double pmlSigmaFactor,
                                         double azimuth, double initialTime, FdTdCuda* m) :
        PlaneWaveSource(id, name, frequency, amplitude, polarisation, pmlSigma, spacing,
                        n, continuous, time, pmlSigmaFactor, azimuth, initialTime, m),
        exShared_(m->common()->proxy(), "exShared_"),
        hyShared_(m->common()->proxy(), "hyShared_"),
        common_(m->common()),
        m_(m) {
}

// Constructor
CudaPlaneWaveSource::CudaPlaneWaveSource(int id, fdtd::Model* m) :
        PlaneWaveSource(id, m),
        exShared_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "exShared_"),
        hyShared_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "hyShared_"),
        common_(dynamic_cast<FdTdCuda*>(m)->common()),
        m_(dynamic_cast<FdTdCuda*>(m)) {
}

// Destructor
CudaPlaneWaveSource::~CudaPlaneWaveSource() {
}

// Initialise ready for the next run
void CudaPlaneWaveSource::initialise() {
    // Let the base class do most of the work
    PlaneWaveSource::initialise();
    // Allocate the shared memory
    exShared_.alloc(incidentN_);
    hyShared_.alloc(incidentN_);
    exShared_.set(0.0);
    hyShared_.set(0.0);
}

// Update the information record with data from this source
void CudaPlaneWaveSource::updateInfo() {
    CudaInfo* i = common_->info_.host();
    i->cosAzimuth_ = cosAzimuth_;
    i->cosPolarisation_ = cosPolarisation_;
    i->sinAzimuth_ = sinAzimuth_;
    i->sinPolarisation_ = sinPolarisation_;
    i->incidentCornerX_ = incidentCorner_.x();
    i->incidentCornerY_ = incidentCorner_.y();
    i->incidentCornerZ_ = incidentCorner_.z();
}

// Perform the electric total field/scattered field corrections for the plane wave
void CudaPlaneWaveSource::stepE(const box::ThreadInfo* info,
                                double* x, double* y, double* z) {
    // Information
    updateInfo();
    // Copy the auxiliary H field
    hyShared_.ignoreGpuChanges();
    float* shared = hyShared_.host();
    for(int i=0; i<incidentN_; i++) {
        shared[i] = (float)hy_[i];
    }
    // Do the corrections
    common_->cudaProxy_.tfsfE(m_->cudaE_->x_, m_->cudaE_->y_, m_->cudaE_->z_, 
                              hyShared_, common_->info_);
}

// Perform the magnetic total field/scattered field corrections for the plane wave
void CudaPlaneWaveSource::stepH(const box::ThreadInfo* info,
                                double* x, double* y, double* z) {
    // Information
    updateInfo();
    // Copy the auxiliary E field
    exShared_.ignoreGpuChanges();
    float* shared = exShared_.host();
    for(int i=0; i<incidentN_; i++) {
        shared[i] = (float)ex_[i];
    }
    common_->cudaProxy_.tfsfH(m_->cudaH_->x_, m_->cudaH_->y_, m_->cudaH_->z_, 
                              exShared_, common_->info_);
}

// Create a new source of this kind
fdtd::Source* CudaPlaneWaveSource::create(int identifier, fdtd::Model* m) {
    return new CudaPlaneWaveSource(identifier, m);
}
