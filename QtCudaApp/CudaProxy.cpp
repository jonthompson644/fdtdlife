/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaProxy.h"
#include "CudaException.h"
#include "CudaIndexing.h"
#include <sstream>
#include <iostream>

#if defined(USE_CUDA)
#include "Kernel.cuh"
// A macro that makes error checking easier
#define ERRCHK(code) cudaAssert((code), __FILE__, __LINE__, #code)


// A function that checks a CUDA return code and throws an exception
// when an error occurs
inline void cudaAssert(cudaError_t code, const char* file, int line, const char* callText) {
    //std::cout << callText << "| " << file << ":" << line << " Cuda result=" << cudaGetErrorString(code) << std::endl;
    if (code != cudaSuccess) {
        std::stringstream text;
        text << cudaGetErrorString(code) << " at " << file << ":" << line;
        throw CudaException(text.str());
    }
}   
#endif

// Constructor
CudaProxy::CudaProxy() :
        numGpus_(0) {
#if defined(USE_CUDA)
    // What CUDA software do we have?
    ERRCHK(cudaDriverGetVersion(&driverVersion_));
    ERRCHK(cudaRuntimeGetVersion(&runtimeVersion_));
    std::cout << "CUDA version=" << (float)runtimeVersion_ / 1000.0
              << ", driver version=" << (float)driverVersion_ / 1000.0 << std::endl;
    // Discover what kinds of GPUs we have
    ERRCHK(cudaGetDeviceCount(&numGpus_));
    gpuProps_.resize((size_t)numGpus_);
    for(int device=0; device<numGpus_; device++) {
        // Device properties
        struct cudaDeviceProp props;
        ERRCHK(cudaGetDeviceProperties(&props, device));
        gpuProps_[device] = {
            props.clockRate, (ComputeMode)props.computeMode,
            props.major, props.minor, 
            props.name, 0, 0, 
            props.multiProcessorCount, props.maxThreadsPerBlock,
            props.maxThreadsPerMultiProcessor
        };
        // The memory each device has
        ERRCHK(cudaSetDevice(device));
        ERRCHK(cudaMemGetInfo(&gpuProps_[device].freeMemory_, 
                              &gpuProps_[device].totalMemory_));
        std::cout << "[" << device << "]" << props.name 
                  << ": clock=" << (float)props.clockRate/1000000.0 
                  << "GHz, computeMode=" << props.computeMode 
                  << ", capability=" << props.major << "." << props.minor 
                  << ", totalMemory=" << (float)gpuProps_[device].totalMemory_ / 1000000.0 
                  << "M, freeMemory=" << (float)gpuProps_[device].freeMemory_ / 1000000.0
                  << "M, multiProcessors=" << props.multiProcessorCount << std::endl;
    }
    // Select a GPU to use
    ERRCHK(cudaSetDevice(0));
#endif
    std::cout << "CUDA proxy initialised" << std::endl;
}

// Desructor
CudaProxy::~CudaProxy() {

}

// Allocate CUDA memory
void CudaProxy::malloc(void** memory, size_t size) {
#if defined(USE_CUDA)
    ERRCHK(cudaMalloc(memory, size));
#else
    *memory = ::malloc(size);
#endif
}

// Free cuda memory
void CudaProxy::free(void* memory) {
#if defined(USE_CUDA)
    ERRCHK(cudaFree(memory));
#else
    ::free(memory);
#endif
}

// Copy data between host and GPU
void CudaProxy::memcpy(void* destination, void* source, size_t count, bool toHost) {
#if defined(USE_CUDA)
    ERRCHK(cudaMemcpy(destination, source, count, toHost ? cudaMemcpyDeviceToHost : cudaMemcpyHostToDevice));
#else
    ::memcpy(destination, source, count);
#endif
}

// Call the FDTD electric step function
void CudaProxy::fdtdEFieldStep(int startX, int startY, int startZ,
                               int endX, int endY, int endZ,
                               int nX, int nY, int nZ,
                               CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                               CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                               CudaMemory<char>& material, int boundarySize,
                               CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                               CudaMemory<char>& boundaryDepthZ,
                               CudaMemory<float>& drX, CudaMemory<float>& drY, CudaMemory<float>& drZ,
                               CudaMemory<float>& caX, CudaMemory<float>& cbX,
                               CudaMemory<float>& caY, CudaMemory<float>& cbY,
                               CudaMemory<float>& caZ, CudaMemory<float>& cbZ,
                               CudaMemory<float>& thinExtraZ, CudaMemory<float>& caThin, CudaMemory<float>& cbThin) {
    ERRCHK(kernelFdtdEStep(startX, startY, startZ,
                               endX, endY, endZ,
                               nX, nY, nZ,
                               ex.gpu(), ey.gpu(), ez.gpu(),
                               hx.gpu(), hy.gpu(), hz.gpu(),
                               material.gpu(), boundarySize,
                               boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                               boundaryDepthZ.gpu(),
                               drX.gpu(), drY.gpu(), drZ.gpu(),
                               caX.gpu(), cbX.gpu(),
                               caY.gpu(), cbY.gpu(),
                               caZ.gpu(), cbZ.gpu(),
                               thinExtraZ.gpu(), caThin.gpu(), cbThin.gpu()));
}

// Call the FDTD magnetic step function
void CudaProxy::fdtdHFieldStep(int startX, int startY, int startZ,
                               int endX, int endY, int endZ,
                               int nX, int nY, int nZ,
                               CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                               CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                               CudaMemory<char>& material, int boundarySize,
                               CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                               CudaMemory<char>& boundaryDepthZ,
                               CudaMemory<float>& drX, CudaMemory<float>& drY, CudaMemory<float>& drZ,
                               CudaMemory<float>& caX, CudaMemory<float>& cbX,
                               CudaMemory<float>& caY, CudaMemory<float>& cbY,
                               CudaMemory<float>& caZ, CudaMemory<float>& cbZ,
                               CudaMemory<float>& thinExtraZ, CudaMemory<float>& thinInRatio, CudaMemory<float>& thinOutRatio) {
    ERRCHK(kernelFdtdHStep(startX, startY, startZ,
                               endX, endY, endZ,
                               nX, nY, nZ,
                               ex.gpu(), ey.gpu(), ez.gpu(),
                               hx.gpu(), hy.gpu(), hz.gpu(),
                               material.gpu(), boundarySize,
                               boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                               boundaryDepthZ.gpu(),
                               drX.gpu(), drY.gpu(), drZ.gpu(),
                               caX.gpu(), cbX.gpu(),
                               caY.gpu(), cbY.gpu(),
                               caZ.gpu(), cbZ.gpu(),
                               thinExtraZ.gpu(), thinInRatio.gpu(), thinOutRatio.gpu()));
}

// Call the E Field CPML correction for the X left boundary
void CudaProxy::cpmlEFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& yxPsiL, CudaMemory<float>& zxPsiL,
                                CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                                CudaMemory<float>& caY, CudaMemory<float>& caZ,
                                int psiWidth, float dx) {
    ERRCHK(kernelCpmlEFieldXLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiL.gpu(), zxPsiL.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx));
}

// Call the E Field CPML correction for the X right boundary
void CudaProxy::cpmlEFieldXRight(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& yxPsiR, CudaMemory<float>& zxPsiR,
                                CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                                CudaMemory<float>& caY, CudaMemory<float>& caZ,
                                int psiWidth, float dx, int nx) {
    ERRCHK(kernelCpmlEFieldXRight(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiR.gpu(), zxPsiR.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx, nx));
}

// Call the H Field CPML correction for the X left boundary
void CudaProxy::cpmlHFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& yxPsiL, CudaMemory<float>& zxPsiL,
                                CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                                CudaMemory<float>& caY, CudaMemory<float>& caZ,
                                int psiWidth, float dx) {
    ERRCHK(kernelCpmlHFieldXLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiL.gpu(), zxPsiL.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx));
}

// Call the H Field CPML correction for the X right boundary
void CudaProxy::cpmlHFieldXRight(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& yxPsiR, CudaMemory<float>& zxPsiR,
                                CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                                CudaMemory<float>& caY, CudaMemory<float>& caZ,
                                int psiWidth, float dx, int nx) {
    ERRCHK(kernelCpmlHFieldXRight(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiR.gpu(), zxPsiR.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx, nx));
}

// Call the E Field CPML correction for the Y left boundary
void CudaProxy::cpmlEFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& xyPsiL, CudaMemory<float>& zyPsiL,
                                CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                                CudaMemory<float>& caX, CudaMemory<float>& caZ,
                                int psiWidth, float dy) {
    ERRCHK(kernelCpmlEFieldYLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xyPsiL.gpu(), zyPsiL.gpu(),
                                cpmlBY.gpu(), cpmlCY.gpu(),
                                caX.gpu(), caZ.gpu(),
                                psiWidth, dy));
}

// Call the E Field CPML correction for the Y right boundary
void CudaProxy::cpmlEFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                 CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<float>& xyPsiR, CudaMemory<float>& zyPsiR,
                                 CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                                 CudaMemory<float>& caX, CudaMemory<float>& caZ,
                                 int psiWidth, float dy, int ny) {
    ERRCHK(kernelCpmlEFieldYRight(startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xyPsiR.gpu(), zyPsiR.gpu(),
                                 cpmlBY.gpu(), cpmlCY.gpu(),
                                 caX.gpu(), caZ.gpu(),
                                 psiWidth, dy, ny));
}

// Call the H Field CPML correction for the Y left boundary
void CudaProxy::cpmlHFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& xyPsiL, CudaMemory<float>& zyPsiL,
                                CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                                CudaMemory<float>& caX, CudaMemory<float>& caZ,
                                int psiWidth, float dy) {
    ERRCHK(kernelCpmlHFieldYLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xyPsiL.gpu(), zyPsiL.gpu(),
                                cpmlBY.gpu(), cpmlCY.gpu(),
                                caX.gpu(), caZ.gpu(),
                                psiWidth, dy));
}

// Call the H Field CPML correction for the Y right boundary
void CudaProxy::cpmlHFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                 CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<float>& xyPsiR, CudaMemory<float>& zyPsiR,
                                 CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                                 CudaMemory<float>& caX, CudaMemory<float>& caZ,
                                 int psiWidth, float dy, int ny) {
    ERRCHK(kernelCpmlHFieldYRight(startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xyPsiR.gpu(), zyPsiR.gpu(),
                                 cpmlBY.gpu(), cpmlCY.gpu(),
                                 caX.gpu(), caZ.gpu(),
                                 psiWidth, dy, ny));
}

// Call the E Field CPML correction for the Z left boundary
void CudaProxy::cpmlEFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& xzPsiL, CudaMemory<float>& yzPsiL,
                                CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                                CudaMemory<float>& caX, CudaMemory<float>& caY,
                                int psiWidth, float dz) {
    ERRCHK(kernelCpmlEFieldZLeft(
            startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xzPsiL.gpu(), yzPsiL.gpu(),
                                cpmlBZ.gpu(), cpmlCZ.gpu(),
                                caX.gpu(), caY.gpu(),
                                psiWidth, dz));
}

// Call the E Field CPML correction for the Z right boundary
void CudaProxy::cpmlEFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                 CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<float>& xzPsiR, CudaMemory<float>& yzPsiR,
                                 CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                                 CudaMemory<float>& caX, CudaMemory<float>& caY,
                                 int psiWidth, float dz, int nz) {
    ERRCHK(kernelCpmlEFieldZRight(
            startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xzPsiR.gpu(), yzPsiR.gpu(),
                                 cpmlBZ.gpu(), cpmlCZ.gpu(),
                                 caX.gpu(), caY.gpu(),
                                 psiWidth, dz, nz));
}

// Call the H Field CPML correction for the Z left boundary
void CudaProxy::cpmlHFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<float>& xzPsiL, CudaMemory<float>& yzPsiL,
                                CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                                CudaMemory<float>& caX, CudaMemory<float>& caY,
                                int psiWidth, float dz) {
    ERRCHK(kernelCpmlHFieldZLeft(
            startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xzPsiL.gpu(), yzPsiL.gpu(),
                                cpmlBZ.gpu(), cpmlCZ.gpu(),
                                caX.gpu(), caY.gpu(),
                                psiWidth, dz));
}

// Call the H Field CPML correction for the Z right boundary
void CudaProxy::cpmlHFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                                 CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<float>& xzPsiR, CudaMemory<float>& yzPsiR,
                                 CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                                 CudaMemory<float>& caX, CudaMemory<float>& caY,
                                 int psiWidth, float dz, int nz) {
    ERRCHK(kernelCpmlHFieldZRight(
            startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xzPsiR.gpu(), yzPsiR.gpu(),
                                 cpmlBZ.gpu(), cpmlCZ.gpu(),
                                 caX.gpu(), caY.gpu(),
                                 psiWidth, dz, nz));
}

// Call the total field/scattered E field adjustments for a plane wave source
void CudaProxy::tfsfE(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
            CudaMemory<float>& auxHy, CudaMemory<CudaInfo>& info) {
    auxHy.copyToGpu();
    ERRCHK(kernelTfsfE(x.gpu(), y.gpu(), z.gpu(), 
                        auxHy.constGpu(), info.constHost(), info.constGpu()));
}

// Call the total field/scattered H field adjustments for a plane wave source
void CudaProxy::tfsfH(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
            CudaMemory<float>& auxEx, CudaMemory<CudaInfo>& info) {
    auxEx.copyToGpu();
    ERRCHK(kernelTfsfH(x.gpu(), y.gpu(), z.gpu(), 
                        auxEx.constGpu(), info.constHost(), info.constGpu()));
}

// Fix the E field periodic boundaries
void CudaProxy::periodicBoundariesE(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
                                    CudaMemory<CudaInfo>& info) {
    ERRCHK(kernelPeriodicBoundariesE(x.gpu(), y.gpu(), z.gpu(), 
                                     info.constHost(), info.constGpu()));
}

// Fix the H field periodic boundaries
void CudaProxy::periodicBoundariesH(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
                                    CudaMemory<CudaInfo>& info) {
    ERRCHK(kernelPeriodicBoundariesH(x.gpu(), y.gpu(), z.gpu(), 
                                     info.constHost(), info.constGpu()));
}

// Get 2D sensor data 
void CudaProxy::getXPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetXPlaneData(field.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}

// Get 2D sensor data 
void CudaProxy::getYPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetYPlaneData(field.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}

// Get 2D sensor data 
void CudaProxy::getZPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetZPlaneData(field.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}

// Get 2D sensor data 
void CudaProxy::getXPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, 
                                 CudaMemory<float>& z, CudaMemory<float>& capture, 
                                 CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetXPlaneMagData(x.gpu(), y.gpu(), z.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}

// Get 2D sensor data 
void CudaProxy::getYPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, 
                                 CudaMemory<float>& z, CudaMemory<float>& capture, 
                                 CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetYPlaneMagData(x.gpu(), y.gpu(), z.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}

// Get 2D sensor data 
void CudaProxy::getZPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, 
                                 CudaMemory<float>& z, CudaMemory<float>& capture, 
                                 CudaMemory<CudaInfo>& info) {
    capture.ignoreHostChanges();
    ERRCHK(kernelGetZPlaneMagData(x.gpu(), y.gpu(), z.gpu(), capture.gpu(), 
                                     info.constHost(), info.constGpu()));
    capture.copyToHost();
}
