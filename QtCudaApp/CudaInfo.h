/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAINFO_H
#define FDTDLIFE_CUDAINFO_H

// A structure used to pass configuration and status information between
// the host and the gpu.
struct CudaInfo {
    double incidentCornerX_;
    double incidentCornerY_;
    double incidentCornerZ_;
    double sinAzimuth_;
    double cosAzimuth_;
    double sinPolarisation_;
    double cosPolarisation_;
    double scatteredFieldZoneSize_;
    int tfStartX_;
    int tfStartY_; 
    int tfStartZ_;
    int tfEndX_;
    int tfEndY_;
    int tfEndZ_;
    int arraySizeX_;
    int arraySizeY_;
    int arraySizeZ_;
    bool periodicX_;
    bool periodicY_; 
    bool periodicZ_;
    double dx_;
    double dy_;
    double dz_;
    double epsilon0_;
    double mu0_;
    double dt_;
    int nx_;
    int ny_;
    int nz_;
    int sliceOffset_;
};

#endif //FDTDLIFE_CUDAINFO_H
