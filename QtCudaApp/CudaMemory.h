/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAMEMORY_H
#define FDTDLIFE_CUDAMEMORY_H

#include <cstddef>
#include <string>
class CudaProxy;

// A template class that manages CUDA and host memory
template <class T>
class CudaMemory {
protected:
    CudaProxy* proxy_;
    T* host_;
    T* gpu_;
    size_t size_;
    bool needsCopyToGpu_;
    bool needsCopyToHost_;
    std::string name_;
public:
    CudaMemory(CudaProxy* proxy, const std::string& name);
    ~CudaMemory();
    void alloc(size_t size=1);
    void set(T v);
    T* host();
    const T* constHost();
    T* gpu();
    const T* constGpu();
    T& operator[] (size_t i) &;
    T operator[] (size_t i) &&;
    T get(size_t i); 
    void copyToHost();
    void copyToGpu();
    void ignoreHostChanges() {needsCopyToGpu_=false;}
    void ignoreGpuChanges() {needsCopyToHost_=false;}
protected:
    void free();
};


#endif //FDTDLIFE_CUDAMEMORY_H
