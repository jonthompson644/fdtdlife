/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Kernel.cuh"
#include "CudaIndexing.h"
#include <device_launch_parameters.h>
#include <math.h>
#include <iostream>

// Globals
static const int threadsPerBlock = 256;

// The GPU kernel function that will be executed in parallel to calculate the E field step
__global__ void kernelDoFdtdEStep(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int nX, int nY, int nZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* drX, float* drY, float* drZ,
                                 float* caX, float* cbX,
                                 float* caY, float* cbY,
                                 float* caZ, float* cbZ,
                                 float* thinExtraZ, float* caThin, float* cbThin) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + startZ;
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < endZ) {
        // Do the slice
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, nX, nY);
            int idxjp1 = CUDAINDEX(i, j + 1, k, nX, nY);
            int idxkp1 = CUDAINDEX(i, j, k + 1, nX, nY);
            int udxip1 = CUDAINDEX(i + 1, j, k, nX, nY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx], boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx], boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx], boundaryDepthZ[idx], boundarySize);
            float drx = drX[matIdxX];
            float dry = drY[matIdxY];
            float drz = drZ[matIdxZ];
            float dhxj = hx[idxjp1] - hx[idx];
            float dhxk = hx[idxkp1] - hx[idx];
            float dhyi = hy[udxip1] - hy[idx];
            float dhyk = hy[idxkp1] - hy[idx];
            float dhzj = hz[idxjp1] - hz[idx];
            float dhzi = hz[udxip1] - hz[idx];
            ex[idx] =
                    (dhzj / dry - dhyk / drz) * caX[matIdxX] +
                    ex[idx] * cbX[matIdxX];
            ey[idx] =
                    (dhxk / drz - dhzi / drx) * caY[matIdxY] +
                    ey[idx] * cbY[matIdxY];
            ez[idx] =
                    (dhyi / drx - dhxj / dry) * caZ[matIdxZ] +
                    ez[idx] * cbZ[matIdxZ];
            thinExtraZ[idx] =
                    (dhyi / drx - dhxj / dry) * caThin[matIdxZ] +
                    thinExtraZ[idx] * cbThin[matIdxZ];
        }
    }
}

// A function that invokes the E field step kernel function
cudaError_t kernelFdtdEStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           float* ex, float* ey, float* ez,
                           float* hx, float* hy, float* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           float* drX, float* drY, float* drZ,
                           float* caX, float* cbX,
                           float* caY, float* cbY,
                           float* caZ, float* cbZ,
                           float* thinExtraZ, float* caThin, float* cbThin) {
    int yDim = endY - startY;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoFdtdEStep <<< requiredBlocks, threadsPerBlock >>> (startX, startY, startZ,
            endX, endY, endZ,
            nX, nY, nZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            drX, drY, drZ,
            caX, cbX,
            caY, cbY,
            caZ, cbZ,
            thinExtraZ, caThin, cbThin);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// The GPU kernel function that will be executed in parallel to calculate the E field step
__global__ void kernelDoFdtdHStep(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int nX, int nY, int nZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* drX, float* drY, float* drZ,
                                 float* caX, float* cbX,
                                 float* caY, float* cbY,
                                 float* caZ, float* cbZ,
                                 float* thinExtraZ, float* thinInRatio, float* thinOutRatio) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + startZ;
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < endZ) {
        // Do the slice
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, nX, nY);
            int idxjm1 = CUDAINDEX(i, j - 1, k, nX, nY);
            int idxkm1 = CUDAINDEX(i, j, k - 1, nX, nY);
            int idxim1 = CUDAINDEX(i - 1, j, k, nX, nY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx], boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx], boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx], boundaryDepthZ[idx], boundarySize);
            float drx = drX[matIdxX];
            float dry = drY[matIdxY];
            float drz = drZ[matIdxZ];
            float dexj = ex[idx] - ex[idxjm1];
            float dexk = ex[idx] - ex[idxkm1];
            float deyi = ey[idx] - ey[idxim1];
            float deyk = ey[idx] - ey[idxkm1];
            float dezj = ez[idx] - ez[idxjm1];
            float dezi = ez[idx] - ez[idxim1];
            float dezjin = thinExtraZ[idx] - thinExtraZ[CUDAINDEX(i, j - 1, k, nX, nY)];
            float deziin = thinExtraZ[idx] - thinExtraZ[CUDAINDEX(i - 1, j, k, nX, nY)];
            hx[idx] =
                    (-(dezj * thinOutRatio[matIdxX] + dezjin * thinInRatio[matIdxX]) /
                    dry + deyk / drz) * caX[matIdxX] + hx[idx] * cbX[matIdxX];
            hy[idx] =
                    (-dexk / drz + (dezi * thinOutRatio[matIdxY] + deziin * thinInRatio[matIdxY]) /
                    drx) * caY[matIdxY] + hy[idx] * cbY[matIdxY];
            hz[idx] =
                    (-deyi / drx + dexj / dry) * caZ[matIdxZ] +
                    hz[idx] * cbZ[matIdxZ];
        }
    }
}

// A function that invokes the H field step kernel function
cudaError_t kernelFdtdHStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           float* ex, float* ey, float* ez,
                           float* hx, float* hy, float* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           float* drX, float* drY, float* drZ,
                           float* caX, float* cbX,
                           float* caY, float* cbY,
                           float* caZ, float* cbZ,
                           float* thinExtraZ, float* thinInRatio, float* thinOutRatio) {
    int yDim = endY - startY;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoFdtdHStep <<< requiredBlocks, threadsPerBlock >>> (startX, startY, startZ,
            endX, endY, endZ,
            nX, nY, nZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            drX, drY, drZ,
            caX, cbX,
            caY, cbY,
            caZ, cbZ,
            thinExtraZ, thinInRatio, thinOutRatio);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the CPML x left correction
__global__ void kernelDoCpmlEFieldXLeft(int startX, int startY, int startZ,
                             int endX, int endY, int endZ,
                             int arraySizeX, int arraySizeY, int arraySizeZ,
                             float* ex, float* ey, float* ez,
                             float* hx, float* hy, float* hz,
                             char* material, int boundarySize,
                             char* boundaryDepthX, char* boundaryDepthY,
                             char* boundaryDepthZ,
                             float* yxPsiL, float* zxPsiL,
                             float* cpmlBX, float* cpmlCX,
                             float* caY, float* caZ,
                             int psiWidth, float dx) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx / zDim + startX;
    int k = tidx % zDim + startZ;
    // If this is in range
    if(i < (startX+boundarySize) && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float dhyi = hy[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy[idx];
            float dhzi = hz[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI(i-startX, j, k, psiWidth);
            yxPsiL[psiIndex] = cpmlBX[matIdxX] * yxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * dhzi / dx;
            ey[idx] -= yxPsiL[psiIndex] * caY[matIdxY];
            zxPsiL[psiIndex] = cpmlBX[matIdxX] * zxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * dhyi / dx;
            ez[idx] += zxPsiL[psiIndex] * caZ[matIdxZ];
        }
    }
}

// A function that invokes the kernel function
cudaError_t kernelCpmlEFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* yxPsiL, float* zxPsiL,
                                float* cpmlBX, float* cpmlCX,
                                float* caY, float* caZ,
                                int psiWidth, float dx) {
    int xDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldXLeft<<<requiredBlocks, threadsPerBlock>>>(
            startX, startY, startZ,
            endX, endY, endZ,
            arraySizeX, arraySizeY, arraySizeZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            yxPsiL, zxPsiL,
            cpmlBX, cpmlCX,
            caY, caZ,
            psiWidth, dx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the CPML x left correction
__global__ void kernelDoCpmlHFieldXLeft(int startX, int startY, int startZ,
                             int endX, int endY, int endZ,
                             int arraySizeX, int arraySizeY, int arraySizeZ,
                             float* ex, float* ey, float* ez,
                             float* hx, float* hy, float* hz,
                             char* material, int boundarySize,
                             char* boundaryDepthX, char* boundaryDepthY,
                             char* boundaryDepthZ,
                             float* yxPsiL, float* zxPsiL,
                             float* cpmlBX, float* cpmlCX,
                             float* caY, float* caZ,
                             int psiWidth, float dx) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx / zDim + startX;
    int k = tidx % zDim + startZ;
    // If this is in range
    if(i < (startX+boundarySize) && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float deyi = ey[idx] - ey[CUDAINDEX(i - 1, j, k, arraySizeX, arraySizeY)];
            float dezi = ez[idx] - ez[CUDAINDEX(i - 1, j, k, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI(i-startX, j, k, psiWidth);
            yxPsiL[psiIndex] = cpmlBX[matIdxX] * yxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * dezi / dx;
            hy[idx] += yxPsiL[psiIndex] * caY[matIdxY];
            zxPsiL[psiIndex] = cpmlBX[matIdxX] * zxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * deyi / dx;
            hz[idx] -= zxPsiL[psiIndex] * caZ[matIdxZ];
        }
    }
}

// A function that invokes the kernel function
cudaError_t kernelCpmlHFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* yxPsiL, float* zxPsiL,
                                float* cpmlBX, float* cpmlCX,
                                float* caY, float* caZ,
                                int psiWidth, float dx) {
    int xDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldXLeft<<<requiredBlocks, threadsPerBlock>>>(
            startX, startY, startZ,
            endX, endY, endZ,
            arraySizeX, arraySizeY, arraySizeZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            yxPsiL, zxPsiL,
            cpmlBX, cpmlCX,
            caY, caZ,
            psiWidth, dx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the X right CPML correction
__global__ void kernelDoCpmlEFieldXRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* yxPsiR, float* zxPsiR,
                                 float* cpmlBX, float* cpmlCX,
                                 float* caY, float* caZ,
                                 int psiWidth, float dx, int nx) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx / zDim + (endX - boundarySize);
    int k = tidx % zDim + startZ;
    // If this is in range
    if(i < endX && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float dhyi = hy[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy[idx];
            float dhzi = hz[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI((endX - i - 1), j, k, psiWidth);
            yxPsiR[psiIndex] = cpmlBX[matIdxX] * yxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * dhzi / dx;
            ey[idx] -= yxPsiR[psiIndex] * caY[matIdxY];
            zxPsiR[psiIndex] = cpmlBX[matIdxX] * zxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * dhyi / dx;
            ez[idx] += zxPsiR[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the E Field CPML correction for the X right boundary
cudaError_t kernelCpmlEFieldXRight(int startX, int startY, int startZ,
                     int endX, int endY, int endZ,
                     int arraySizeX, int arraySizeY, int arraySizeZ,
                     float* ex, float* ey, float* ez,
                     float* hx, float* hy, float* hz,
                     char* material, int boundarySize,
                     char* boundaryDepthX, char* boundaryDepthY,
                     char* boundaryDepthZ,
                     float* yxPsiR, float* zxPsiR,
                     float* cpmlBX, float* cpmlCX,
                     float* caY, float* caZ,
                     int psiWidth, float dx, int nx) {
    int xDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldXRight<<<requiredBlocks, threadsPerBlock>>>(
        startX, startY, startZ,
        endX, endY, endZ,
        arraySizeX, arraySizeY, arraySizeZ,
        ex, ey, ez,
        hx, hy, hz,
        material, boundarySize,
        boundaryDepthX, boundaryDepthY,
        boundaryDepthZ,
        yxPsiR, zxPsiR,
        cpmlBX, cpmlCX,
        caY, caZ,
        psiWidth, dx, nx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the X right CPML correction
__global__ void kernelDoCpmlHFieldXRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* yxPsiR, float* zxPsiR,
                                 float* cpmlBX, float* cpmlCX,
                                 float* caY, float* caZ,
                                 int psiWidth, float dx, int nx) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx / zDim + (endX - boundarySize);
    int k = tidx % zDim + startZ;
    // If this is in range
    if(i < endX && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float deyi = ey[idx] - ey[CUDAINDEX(i - 1, j, k, arraySizeX, arraySizeY)];
            float dezi = ez[idx] - ez[CUDAINDEX(i - 1, j, k, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI((endX - i - 1), j, k, psiWidth);
            yxPsiR[psiIndex] = cpmlBX[matIdxX] * yxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * dezi / dx;
            hy[idx] += yxPsiR[psiIndex] * caY[matIdxY];
            zxPsiR[psiIndex] = cpmlBX[matIdxX] * zxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * deyi / dx;
            hz[idx] -= zxPsiR[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the E Field CPML correction for the X right boundary
cudaError_t kernelCpmlHFieldXRight(int startX, int startY, int startZ,
                     int endX, int endY, int endZ,
                     int arraySizeX, int arraySizeY, int arraySizeZ,
                     float* ex, float* ey, float* ez,
                     float* hx, float* hy, float* hz,
                     char* material, int boundarySize,
                     char* boundaryDepthX, char* boundaryDepthY,
                     char* boundaryDepthZ,
                     float* yxPsiR, float* zxPsiR,
                     float* cpmlBX, float* cpmlCX,
                     float* caY, float* caZ,
                     int psiWidth, float dx, int nx) {
    int xDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldXRight<<<requiredBlocks, threadsPerBlock>>>(
        startX, startY, startZ,
        endX, endY, endZ,
        arraySizeX, arraySizeY, arraySizeZ,
        ex, ey, ez,
        hx, hy, hz,
        material, boundarySize,
        boundaryDepthX, boundaryDepthY,
        boundaryDepthZ,
        yxPsiR, zxPsiR,
        cpmlBX, cpmlCX,
        caY, caZ,
        psiWidth, dx, nx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the Y left CPML correction
__global__ void kernelDoCpmlEFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx / zDim + startY;
    int k = tidx % zDim + startZ;
    // If this is in range
    if(j < (startY + boundarySize) && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dhxj = hx[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hx[idx];
            float dhzj = hz[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI(i, j-startY, k, psiWidth);
            xyPsiL[psiIndex] = cpmlBY[matIdxY] * xyPsiL[psiIndex] +
                                        cpmlCY[matIdxY] * dhzj / dy;
            ex[idx] += xyPsiL[psiIndex] * caX[matIdxX];
            zyPsiL[psiIndex] = cpmlBY[matIdxY] * zyPsiL[psiIndex] +
                                        cpmlCY[matIdxY] * dhxj / dy;
            ez[idx] -= zyPsiL[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the E Field CPML correction for the Y left boundary
cudaError_t kernelCpmlEFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy) {
    int yDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldYLeft<<<requiredBlocks, threadsPerBlock>>>(
                                startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex, ey, ez,
                                hx, hy, hz,
                                material, boundarySize,
                                boundaryDepthX, boundaryDepthY,
                                boundaryDepthZ,
                                xyPsiL, zyPsiL,
                                cpmlBY, cpmlCY,
                                caX, caZ,
                                psiWidth, dy);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the Y left CPML correction
__global__ void kernelDoCpmlHFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx / zDim + startY;
    int k = tidx % zDim + startZ;
    // If this is in range
    if(j < (startY + boundarySize) && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dexj = ex[idx] - ex[CUDAINDEX(i, j - 1, k, arraySizeX, arraySizeY)];
            float dezj = ez[idx] - ez[CUDAINDEX(i, j - 1, k, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI(i, j-startY, k, psiWidth);
            xyPsiL[psiIndex] = cpmlBY[matIdxY] * xyPsiL[psiIndex] +
                                        cpmlCY[matIdxY] * dezj / dy;
            hx[idx] -= xyPsiL[psiIndex] * caX[matIdxX];
            zyPsiL[psiIndex] = cpmlBY[matIdxY] * zyPsiL[psiIndex] +
                                        cpmlCY[matIdxY] * dexj / dy;
            hz[idx] += zyPsiL[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the H Field CPML correction for the Y left boundary
cudaError_t kernelCpmlHFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy) {
    int yDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldYLeft<<<requiredBlocks, threadsPerBlock>>>(
                                startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex, ey, ez,
                                hx, hy, hz,
                                material, boundarySize,
                                boundaryDepthX, boundaryDepthY,
                                boundaryDepthZ,
                                xyPsiL, zyPsiL,
                                cpmlBY, cpmlCY,
                                caX, caZ,
                                psiWidth, dy);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function for the E Field CPML correction for the Y right boundary
__global__ void kernelDoCpmlEFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx / zDim + (endY - boundarySize);
    int k = tidx % zDim + startZ;
    // If this is in range
    if(j < endY && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dhxj = hx[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hx[idx];
            float dhzj = hz[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI(i, (endY - j - 1), k, psiWidth);
            xyPsiR[psiIndex] = cpmlBY[matIdxY] * xyPsiR[psiIndex] +
                                        cpmlCY[matIdxY] * dhzj / dy;
            ex[idx] += xyPsiR[psiIndex] * caX[matIdxX];
            zyPsiR[psiIndex] = cpmlBY[matIdxY] * zyPsiR[psiIndex] +
                                        cpmlCY[matIdxY] * dhxj / dy;
            ez[idx] -= zyPsiR[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the E Field CPML correction for the Y right boundary
cudaError_t kernelCpmlEFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny) {
    int yDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldYRight<<<requiredBlocks, threadsPerBlock>>>(
                                 startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex, ey, ez,
                                 hx, hy, hz,
                                 material, boundarySize,
                                 boundaryDepthX, boundaryDepthY,
                                 boundaryDepthZ,
                                 xyPsiR, zyPsiR,
                                 cpmlBY, cpmlCY,
                                 caX, caZ,
                                 psiWidth, dy, ny);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function for the H Field CPML correction for the Y right boundary
__global__ void kernelDoCpmlHFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny) {
    int zDim = endZ - startZ;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx / zDim + endY - boundarySize;
    int k = tidx % zDim + startZ;
    // If this is in range
    if(j < endY && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dexj = ex[idx] - ex[CUDAINDEX(i, j - 1, k, arraySizeX, arraySizeY)];
            float dezj = ez[idx] - ez[CUDAINDEX(i, j - 1, k, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI(i, (endY - j - 1), k, psiWidth);
            xyPsiR[psiIndex] = cpmlBY[matIdxY] * xyPsiR[psiIndex] +
                                        cpmlCY[matIdxY] * dezj / dy;
            hx[idx] -= xyPsiR[psiIndex] * caX[matIdxX];
            zyPsiR[psiIndex] = cpmlBY[matIdxY] * zyPsiR[psiIndex] +
                                        cpmlCY[matIdxY] * dexj / dy;
            hz[idx] += zyPsiR[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the H Field CPML correction for the Y right boundary
cudaError_t kernelCpmlHFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny) {
    int yDim = boundarySize;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldYRight<<<requiredBlocks, threadsPerBlock>>>(
                                 startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex, ey, ez,
                                 hx, hy, hz,
                                 material, boundarySize,
                                 boundaryDepthX, boundaryDepthY,
                                 boundaryDepthZ,
                                 xyPsiR, zyPsiR,
                                 cpmlBY, cpmlCY,
                                 caX, caZ,
                                 psiWidth, dy, ny);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel that calculates the E Field CPML correction for the Z left boundary
__global__ void kernelDoCpmlEFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + startZ;
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < (boundarySize+startZ)) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float dhxk = hx[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hx[idx];
            float dhyk = hy[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hy[idx];
            int psiIndex = CUDAINDEXPSI(i, j, k-startZ, psiWidth);
            xzPsiL[psiIndex] = cpmlBZ[matIdxZ] * xzPsiL[psiIndex] +
                                        cpmlCZ[matIdxZ] * dhyk / dz;
            ex[idx] -= xzPsiL[psiIndex] * caX[matIdxX];
            yzPsiL[psiIndex] = cpmlBZ[matIdxZ] * yzPsiL[psiIndex] +
                                        cpmlCZ[matIdxZ] * dhxk / dz;
            ey[idx] += yzPsiL[psiIndex] * caY[matIdxY];
        }
    }
}

// Call the E Field CPML correction for the Z left boundary
cudaError_t kernelCpmlEFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz) {
    int yDim = endY - startY;
    int zDim = boundarySize;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldZLeft<<<requiredBlocks, threadsPerBlock>>>(
                                startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex, ey, ez,
                                hx, hy, hz,
                                material, boundarySize,
                                boundaryDepthX, boundaryDepthY,
                                boundaryDepthZ,
                                xzPsiL, yzPsiL,
                                cpmlBZ, cpmlCZ,
                                caX, caY,
                                psiWidth, dz);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel that calculates the H Field CPML correction for the Z left boundary
__global__ void kernelDoCpmlHFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + startZ;
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < (boundarySize+startZ)) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            float dexk = ex[idx] - ex[CUDAINDEX(i, j, k - 1, arraySizeX, arraySizeY)];
            float deyk = ey[idx] - ey[CUDAINDEX(i, j, k - 1, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI(i, j, k-startZ, psiWidth);
            xzPsiL[psiIndex] = cpmlBZ[matIdxZ] * xzPsiL[psiIndex] +
                                        cpmlCZ[matIdxZ] * deyk / dz;
            hx[idx] += xzPsiL[psiIndex] * caX[matIdxX];
            yzPsiL[psiIndex] = cpmlBZ[matIdxZ] * yzPsiL[psiIndex] +
                                        cpmlCZ[matIdxZ] * dexk / dz;
            hy[idx] -= yzPsiL[psiIndex] * caY[matIdxY];
        }
    }
}

// Call the H Field CPML correction for the Z left boundary
cudaError_t kernelCpmlHFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz) {
    int yDim = endY - startY;
    int zDim = boundarySize;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldZLeft<<<requiredBlocks, threadsPerBlock>>>(
                                startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex, ey, ez,
                                hx, hy, hz,
                                material, boundarySize,
                                boundaryDepthX, boundaryDepthY,
                                boundaryDepthZ,
                                xzPsiL, yzPsiL,
                                cpmlBZ, cpmlCZ,
                                caX, caY,
                                psiWidth, dz);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel that calculates the E Field CPML correction for the Z right boundary
__global__ void kernelDoCpmlEFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + (endZ - boundarySize);
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dhxk = hx[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hx[idx];
            float dhyk = hy[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hy[idx];
            int psiIndex = CUDAINDEXPSI(i, j, (endZ - k - 1), psiWidth);
            xzPsiR[psiIndex] = cpmlBZ[matIdxZ] * xzPsiR[psiIndex] +
                                        cpmlCZ[matIdxZ] * dhyk / dz;
            ex[idx] -= xzPsiR[psiIndex] * caX[matIdxX];
            yzPsiR[psiIndex] = cpmlBZ[matIdxZ] * yzPsiR[psiIndex] +
                                        cpmlCZ[matIdxZ] * dhxk / dz;
            ey[idx] += yzPsiR[psiIndex] * caY[matIdxY];
        }
    }
}

// Call the E Field CPML correction for the Z right boundary
cudaError_t kernelCpmlEFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz) {
    int yDim = endY - startY;
    int zDim = boundarySize;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldZRight<<<requiredBlocks, threadsPerBlock>>>(
                                 startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex, ey, ez,
                                 hx, hy, hz,
                                 material, boundarySize,
                                 boundaryDepthX, boundaryDepthY,
                                 boundaryDepthZ,
                                 xzPsiR, yzPsiR,
                                 cpmlBZ, cpmlCZ,
                                 caX, caY,
                                 psiWidth, dz, nz);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel that calculates the H Field CPML correction for the Z right boundary
__global__ void kernelDoCpmlHFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + (endZ - boundarySize);
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < endZ) {
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                                    boundaryDepthZ[idx], boundarySize);
            float dexk = ex[idx] - ex[CUDAINDEX(i, j, k - 1, arraySizeX, arraySizeY)];
            float deyk = ey[idx] - ey[CUDAINDEX(i, j, k - 1, arraySizeX, arraySizeY)];
            int psiIndex = CUDAINDEXPSI(i, j, (endZ - k - 1), psiWidth);
            xzPsiR[psiIndex] = cpmlBZ[matIdxZ] * xzPsiR[psiIndex] +
                                        cpmlCZ[matIdxZ] * deyk / dz;
            hx[idx] += xzPsiR[psiIndex] * caX[matIdxX];
            yzPsiR[psiIndex] = cpmlBZ[matIdxZ] * yzPsiR[psiIndex] +
                                        cpmlCZ[matIdxZ] * dexk / dz;
            hy[idx] -= yzPsiR[psiIndex] * caY[matIdxY];
        }
    }
}

// Call the H Field CPML correction for the Z right boundary
cudaError_t kernelCpmlHFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz) {
    int yDim = endY - startY;
    int zDim = boundarySize;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlHFieldZRight<<<requiredBlocks, threadsPerBlock>>>(
                                 startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex, ey, ez,
                                 hx, hy, hz,
                                 material, boundarySize,
                                 boundaryDepthX, boundaryDepthY,
                                 boundaryDepthZ,
                                 xzPsiR, yzPsiR,
                                 cpmlBZ, cpmlCZ,
                                 caX, caY,
                                 psiWidth, dz, nz);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Calculate the distance from the point projected onto the incident wave to
// the corner the wave first encounters.
__device__ float distanceToIncidentCorner(float /*i*/, float j, float k, const CudaInfo* info) {
    // Calculate the distance of the point projected onto the wave
    // from the incident corner
    float d = (j - info->incidentCornerY_) * info->sinAzimuth_ +
               (k - info->incidentCornerZ_) * info->cosAzimuth_;
    // Now add in the scattered field zone size
    d += info->scatteredFieldZoneSize_;
    return d;
}

// Interpolate into an array of data
__device__ float interpolate(float d, const float* data) {
    int n = (int) floor(d);
    float frac = d - n;
    return data[n] * (1 - frac) + data[n + 1] * frac;
}

// Return the incident wave X component
__device__ float eIncidentX(float i, float j, float k, const float* ex, const CudaInfo* info) {
    // Get the distance into the wave
    float d = distanceToIncidentCorner(i, j, k, info);
    // Interpolate from the array
    float value = interpolate(d, ex);
    return value * info->cosPolarisation_;
}

// Return the incident wave Y component
__device__ float eIncidentY(float i, float j, float k, const float* ex, const CudaInfo* info) {
    // Get the distance into the wave
    float d = distanceToIncidentCorner(i, j, k, info);
    // Interpolate from the array
    float value = interpolate(d, ex);
    return value * info->sinPolarisation_ * info->cosAzimuth_;
}

// Return the incident wave Z component
__device__ float eIncidentZ(float i, float j, float k, const float* ex, const CudaInfo* info) {
    // Get the distance into the wave
    float d = distanceToIncidentCorner(i, j, k, info);
    // Interpolate from the array
    float value = interpolate(d, ex);
    return -value * info->sinPolarisation_ * info->sinAzimuth_;
}

// Return the incident wave X component
// Note the distance calculation accounting for the fact that
// the magnetic field array is half a step advanced.
__device__ float hIncidentX(float i, float j, float k, const float* hy, const CudaInfo* info) {
    // Get the distance into the wave
    // Account for the magnetic field being a half step advanced
    float d = distanceToIncidentCorner(i, j, k, info) - 0.5;
    if(d < 0.0) {
        d = 0.0;
    }
    // Interpolate from the array
    float value = interpolate(d, hy);
    return -value * info->sinPolarisation_;
}

// Return the incident wave Y component
// Note the distance calculation accounting for the fact that
// the magnetic field array is half a step advanced.
__device__ float hIncidentY(float i, float j, float k, const float* hy, const CudaInfo* info) {
    // Get the distance into the wave
    // Account for the magnetic field being a half step advanced
    float d = distanceToIncidentCorner(i, j, k, info) - 0.5;
    if(d < 0.0) {
        d = 0.0;
    }
    // Interpolate from the array
    float value = interpolate(d, hy);
    return value * info->cosPolarisation_ * info->cosAzimuth_;
}

// Return the incident wave Z component
// Note the distance calculation accounting for the fact that
// the magnetic field array is half a step advanced.
__device__ float hIncidentZ(float i, float j, float k, const float* hy, const CudaInfo* info) {
    // Get the distance into the wave
    // Account for the magnetic field being a half step advanced
    float d = distanceToIncidentCorner(i, j, k, info) - 0.5;
    if(d < 0.0) {
        d = 0.0;
    }
    // Interpolate from the array
    float value = interpolate(d, hy);
    return -value * info->cosPolarisation_ * info->sinAzimuth_;
}

// The GPU code for the TFSF E field X boundaries
__global__ void kernelDoTfsfEX(float* x, float* y, float* z,
                                const float* auxHy, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i;
    int j = tidx;
    int k;
    if(j <= info->tfEndY_) {
        i = info->tfStartX_ - 1;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hy = hIncidentY(i + 1, j + 0.5, k, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] -= info->dt_ / info->epsilon0_ / info->dy_ * hy;
        }
        i = info->tfStartX_ - 1;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hz = hIncidentZ(i + 1, j, k + 0.5, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] += info->dt_ / info->epsilon0_ / info->dy_ * hz;
        }
        i = info->tfEndX_;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hy = hIncidentY(i + 1, j + 0.5, k, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] += info->dt_ / info->epsilon0_ / info->dy_ * hy;
        }
        i = info->tfEndX_;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hz = hIncidentZ(i + 1, j, k + 0.5, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] -= info->dt_ / info->epsilon0_ / info->dy_ * hz;
        }
    }
}

// The GPU code for the TFSF E field Y boundaries
__global__ void kernelDoTfsfEY(float* x, float* y, float* z,
                                const float* auxHy, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx;
    int j;
    int k;
    if(i <= info->tfEndX_) {
        j = info->tfStartY_ - 1;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hz = hIncidentZ(i, j + 1, k + 0.5, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] -= info->dt_ / info->epsilon0_ / info->dx_ * hz;
        }
        j = info->tfStartY_ - 1;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hx = hIncidentX(i + 0.5, j + 1, k, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] += info->dt_ / info->epsilon0_ / info->dx_ * hx;
        }
        j = info->tfEndY_;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hz = hIncidentZ(i, j + 1, k + 0.5, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] += info->dt_ / info->epsilon0_ / info->dx_ * hz;
        }
        j = info->tfEndY_;
        for(k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float hx = hIncidentX(i + 0.5, j + 1, k, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] += info->dt_ / info->epsilon0_ / info->dx_ * hx;
        }
    }
}

// The GPU code for the TFSF E field Z boundaries
__global__ void kernelDoTfsfEZ(float* x, float* y, float* z,
                                const float* auxHy, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx;
    int j;
    int k;
    if(i <= info->tfEndX_) {
        for(j = info->tfStartY_; j <= info->tfEndY_; j++) {
            k = info->tfStartZ_ - 1;
            float hy = hIncidentY(i, j + 0.5, k + 1, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] += info->dt_ / info->epsilon0_ / info->dz_ * hy;
        }
        for(j = info->tfStartY_; j <= info->tfEndY_; j++) {
            k = info->tfStartZ_ - 1;
            float hx = hIncidentX(i + 0.5, j, k + 1, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] -= info->dt_ / info->epsilon0_ / info->dz_ * hx;
        }
        for(j = info->tfStartY_; j <= info->tfEndY_; j++) {
            k = info->tfEndZ_;
            float hy = hIncidentY(i, j + 0.5, k + 1, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] -= info->dt_ / info->epsilon0_ / info->dz_ * hy;
        }
        for(j = info->tfStartY_; j <= info->tfEndY_; j++) {
            k = info->tfEndZ_;
            float hx = hIncidentX(i + 0.5, j, k + 1, auxHy, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] += info->dt_ / info->epsilon0_ / info->dz_ * hx;
        }
    }
}

// The GPU code for the TFSF H field X boundaries
__global__ void kernelDoTfsfHX(float* x, float* y, float* z,
                                const float* auxEx, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i;
    int j = tidx;
    int k;
    if(j <= info->tfEndY_) {
        i = info->tfStartX_;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ez = eIncidentZ(i - 0.5, j + 0.5, k, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] -= info->dt_ / info->mu0_ / info->dy_ * ez;
        }
        i = info->tfStartX_;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ey = eIncidentY(i - 0.5, j, k + 0.5, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] += info->dt_ / info->mu0_ / info->dy_ * ey;
        }
        i = info->tfEndX_ + 1;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ez = eIncidentZ(i - 0.5, j + 0.5, k, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] += info->dt_ / info->mu0_ / info->dy_ * ez;
        }
        i = info->tfEndX_ + 1;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ey = eIncidentY(i - 0.5, j, k + 0.5, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] -= info->dt_ / info->mu0_ / info->dy_ * ey;
        }
    }
}

// The GPU code for the TFSF H field Y boundaries
__global__ void kernelDoTfsfHY(float* x, float* y, float* z,
                                const float* auxEx, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx;
    int j;
    int k;
    if(i <= info->tfEndX_) {
        j = info->tfStartY_;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ex = eIncidentX(i, j - 0.5, k + 0.5, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] -= info->dt_ / info->mu0_ / info->dx_ * ex;
        }
        j = info->tfStartY_;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ez = eIncidentZ(i + 0.5, j - 0.5, k, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] += info->dt_ / info->mu0_ / info->dx_ * ez;
        }
        j = info->tfEndY_ + 1;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ex = eIncidentX(i, j - 0.5, k + 0.5, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            z[idx] += info->dt_ / info->mu0_ / info->dx_ * ex;
        }
        j = info->tfEndY_ + 1;
        for (k = info->tfStartZ_; k <= info->tfEndZ_; k++) {
            float ez = eIncidentZ(i + 0.5, j - 0.5, k, auxEx, info);
            int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            x[idx] -= info->dt_ / info->mu0_ / info->dx_ * ez;
        }
    }
}

// The GPU code for the TFSF H field Z boundaries
__global__ void kernelDoTfsfHZ(float* x, float* y, float* z,
                                const float* auxEx, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx + info->tfStartX_;
    int j;
    int k;
    float ex;
    float ey;
    int idx;
    if(i <= info->tfEndX_) {
        for (j = info->tfStartY_; j <= info->tfEndY_; j++) {
            k = info->tfStartZ_;
            ex = eIncidentX(i, j + 0.5, k - 0.5, auxEx, info);
            idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] += info->dt_ / info->mu0_ / info->dz_ * ex;
            ey = eIncidentY(i + 0.5, j, k - 0.5, auxEx, info);
            x[idx] -= info->dt_ / info->mu0_ / info->dz_ * ey;
            k = info->tfEndZ_ + 1;
            ex = eIncidentX(i, j + 0.5, k - 0.5, auxEx, info);
            idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
            y[idx] -= info->dt_ / info->mu0_ / info->dz_ * ex;
            ey = eIncidentY(i + 0.5, j, k - 0.5, auxEx, info);
            x[idx] += info->dt_ / info->mu0_ / info->dz_ * ey;
        }
    }
}

// Perform the electric total field/scattered field corrections for the plane wave
cudaError_t kernelTfsfE(float* x, float* y, float* z, const float* auxHy, 
                        const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    if(error == cudaSuccess && !hostInfo->periodicX_) {
        int totalThreads = hostInfo->tfEndY_ - hostInfo->tfStartY_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfEX<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxHy, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && !hostInfo->periodicY_) {
        int totalThreads = hostInfo->tfEndX_ - hostInfo->tfStartX_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfEY<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxHy, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && !hostInfo->periodicZ_) {
        int totalThreads = hostInfo->tfEndX_ - hostInfo->tfStartX_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfEZ<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxHy, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    return error;
}

// Perform the magnetic total field/scattered field corrections for the plane wave
cudaError_t kernelTfsfH(float* x, float* y, float* z, const float* auxEx, 
                        const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    if(error == cudaSuccess && !hostInfo->periodicX_) {
        int totalThreads = hostInfo->tfEndY_ - hostInfo->tfStartY_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfHX<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxEx, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && !hostInfo->periodicY_) {
        int totalThreads = hostInfo->tfEndX_ - hostInfo->tfStartX_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfHY<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxEx, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && !hostInfo->periodicZ_) {
        int totalThreads = hostInfo->tfEndX_ - hostInfo->tfStartX_ + 1;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoTfsfHZ<<<requiredBlocks, threadsPerBlock>>>(x, y, z, auxEx, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    return error;
}

// The GPU code for the E field X periodic boundary
__global__ void kernelDoPeriodicBoundariesEX(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx % info->ny_;
    int k = tidx / info->ny_;
    if(k<info->nz_) {
        int idxFrom = CUDAINDEX(info->nx_-1,j,k, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(-1,j,k, info->arraySizeX_, info->arraySizeY_);
        y[idxTo] = y[idxFrom];
        z[idxTo] = z[idxFrom];
    }
}

// The GPU code for the E field Y periodic boundary
__global__ void kernelDoPeriodicBoundariesEY(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int k = tidx / info->nx_;
    if(k<info->nz_) {
        int idxFrom = CUDAINDEX(i,info->ny_-1,k, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(i,-1,k, info->arraySizeX_, info->arraySizeY_);
        z[idxTo] = z[idxFrom];
        x[idxTo] = x[idxFrom];
    }
}

// The GPU code for the E field Z periodic boundary
__global__ void kernelDoPeriodicBoundariesEZ(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = tidx / info->nx_;
    if(j<info->ny_) {
        int idxFrom = CUDAINDEX(i,j,info->nz_-1, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(i,j,-1, info->arraySizeX_, info->arraySizeY_);
        x[idxTo] = x[idxFrom];
        y[idxTo] = y[idxFrom];
    }
}

// Handle the periodic boundaries for the E field
cudaError_t kernelPeriodicBoundariesE(float* x, float* y, float* z, 
                                      const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    if(error == cudaSuccess && hostInfo->periodicX_) {
        int totalThreads = hostInfo->ny_ * hostInfo->nz_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesEX<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && hostInfo->periodicY_) {
        int totalThreads = hostInfo->nx_ * hostInfo->nz_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesEY<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && hostInfo->periodicZ_) {
        int totalThreads = hostInfo->nx_ * hostInfo->ny_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesEZ<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    return cudaSuccess;
}

// The GPU code for the H field X periodic boundary
__global__ void kernelDoPeriodicBoundariesHX(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx % info->ny_;
    int k = tidx / info->ny_;
    if(k<info->nz_) {
        int idxFrom = CUDAINDEX(0,j,k, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(info->nx_,j,k, info->arraySizeX_, info->arraySizeY_);
        y[idxTo] = y[idxFrom];
        z[idxTo] = z[idxFrom];
    }
}

// The GPU code for the H field Y periodic boundary
__global__ void kernelDoPeriodicBoundariesHY(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int k = tidx / info->nx_;
    if(k<info->nz_) {
        int idxFrom = CUDAINDEX(i,0,k, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(i,info->ny_,k, info->arraySizeX_, info->arraySizeY_);
        z[idxTo] = z[idxFrom];
        x[idxTo] = x[idxFrom];
    }
}

// The GPU code for the H field Z periodic boundary
__global__ void kernelDoPeriodicBoundariesHZ(float* x, float* y, float* z,
                                const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = tidx / info->nx_;
    if(j<info->ny_) {
        int idxFrom = CUDAINDEX(i,j,0, info->arraySizeX_, info->arraySizeY_);
        int idxTo = CUDAINDEX(i,j,info->nz_, info->arraySizeX_, info->arraySizeY_);
        x[idxTo] = x[idxFrom];
        y[idxTo] = y[idxFrom];
    }
}

// Handle the periodic boundaries for the H field
cudaError_t kernelPeriodicBoundariesH(float* x, float* y, float* z, 
                                      const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    if(error == cudaSuccess && hostInfo->periodicX_) {
        int totalThreads = hostInfo->ny_ * hostInfo->nz_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesHX<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && hostInfo->periodicY_) {
        int totalThreads = hostInfo->nx_ * hostInfo->nz_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesHY<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    if(error == cudaSuccess && hostInfo->periodicZ_) {
        int totalThreads = hostInfo->nx_ * hostInfo->ny_;
        int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
        kernelDoPeriodicBoundariesHZ<<<requiredBlocks, threadsPerBlock>>>(x, y, z, gpuInfo);
        error = cudaGetLastError();
        if(error == cudaSuccess) {
            error = cudaDeviceSynchronize();
        }
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetXPlaneData(float* field, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = info->sliceOffset_;
    int j = tidx % info->ny_;
    int k = tidx / info->nz_;
    if(k < info->nz_) {
        int sliceIdx = CUDAINDEX2DSENSOR(j, k, info->ny_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        capture[sliceIdx] = field[idx];
    }
}

// Get 2D sensor data
cudaError_t kernelGetXPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->ny_ * hostInfo->nz_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetXPlaneData<<<requiredBlocks, threadsPerBlock>>>(field, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetYPlaneData(float* field, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = info->sliceOffset_;
    int k = tidx / info->nx_;
    if(k < info->nz_) {
        int sliceIdx = CUDAINDEX2DSENSOR(i, k, info->nx_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        capture[sliceIdx] = field[idx];
    }
}

// Get 2D sensor data
cudaError_t kernelGetYPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->nx_ * hostInfo->nz_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetYPlaneData<<<requiredBlocks, threadsPerBlock>>>(field, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetZPlaneData(float* field, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = tidx / info->nx_;
    int k = info->sliceOffset_;
    if(j < info->ny_) {
        int sliceIdx = CUDAINDEX2DSENSOR(i, j, info->nx_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        capture[sliceIdx] = field[idx];
    }
}

// Get 2D sensor data
cudaError_t kernelGetZPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->nx_ * hostInfo->ny_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetZPlaneData<<<requiredBlocks, threadsPerBlock>>>(field, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetXPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int j = tidx % info->ny_;
    int i = info->sliceOffset_;
    int k = tidx / info->ny_;
    if(k < info->nz_) {
        int sliceIdx = CUDAINDEX2DSENSOR(j, k, info->ny_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        float xv = x[idx];
        float yv = y[idx];
        float zv = z[idx];
        float mag = sqrt(xv*xv + yv*yv + zv*zv);
        capture[sliceIdx] = mag;
    }
}

// Get 2D sensor data
cudaError_t kernelGetXPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->ny_ * hostInfo->nz_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetXPlaneMagData<<<requiredBlocks, threadsPerBlock>>>(x, y, z, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetYPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = info->sliceOffset_;
    int k = tidx / info->nx_;
    if(k < info->nz_) {
        int sliceIdx = CUDAINDEX2DSENSOR(i, k, info->nx_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        float xv = x[idx];
        float yv = y[idx];
        float zv = z[idx];
        float mag = sqrt(xv*xv + yv*yv + zv*zv);
        capture[sliceIdx] = mag;
    }
}

// Get 2D sensor data
cudaError_t kernelGetYPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->nx_ * hostInfo->nz_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetYPlaneMagData<<<requiredBlocks, threadsPerBlock>>>(x, y, z, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// Kernel part of get 2D sensor data
__global__ void kernelDoGetZPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* info) {
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int i = tidx % info->nx_;
    int j = tidx / info->nx_;
    int k = info->sliceOffset_;
    if(j < info->ny_) {
        int sliceIdx = CUDAINDEX2DSENSOR(i, j, info->nx_);
        int idx = CUDAINDEX(i, j, k, info->arraySizeX_, info->arraySizeY_);
        float xv = x[idx];
        float yv = y[idx];
        float zv = z[idx];
        float mag = sqrt(xv*xv + yv*yv + zv*zv);
        capture[sliceIdx] = mag;
    }
}

// Get 2D sensor data
cudaError_t kernelGetZPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo) {
    cudaError_t error = cudaSuccess;
    int totalThreads = hostInfo->nx_ * hostInfo->ny_;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoGetZPlaneMagData<<<requiredBlocks, threadsPerBlock>>>(x, y, z, capture, gpuInfo);
    error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}
