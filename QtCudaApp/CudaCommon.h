/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDACOMMON_H
#define FDTDLIFE_CUDACOMMON_H

#include "CudaProxy.h"
#include "CudaMemory.h"
#include "CudaInfo.h"

namespace fdtd {class Model;}

// Algorithm details that are common to both the E and H fields
class CudaCommon {
public:
    // Construction
    explicit CudaCommon(fdtd::Model* model);

    // API
    void initialise();
    int matDepthIndex(int materialId, int boundaryDepth);
    CudaProxy* proxy();

protected:
    // Helper functions
    void initialiseMaterial();
    void initialiseBoundaryDepth();
    void initialiseCellSize();
    void initialiseInfo();

    // Variables
    fdtd::Model* m_;
    size_t arraySize_;

public:
    // Memory indexed by x,y,z
    CudaMemory<char> material_;   // The material at each grid cell
    CudaMemory<char> boundaryDepthX_;  // The X boundary depth at each location
    CudaMemory<char> boundaryDepthY_;  // The Y boundary depth at each location
    CudaMemory<char> boundaryDepthZ_;  // The Z boundary depth at each location
    // Memory containing (stretched) dr, indexed by material id and boundary depth
    CudaMemory<float> drX_;   // The X cell size
    CudaMemory<float> drY_;   // The Y cell size
    CudaMemory<float> drZ_;   // The Z cell size
    // Configuration and status
    CudaMemory<CudaInfo> info_;
    // The CUDA proxy
    CudaProxy cudaProxy_;
    // Sizes
    size_t matDepthArraySize_;  // The size of arrays indexed by material identifier and boundary depth
};


#endif //FDTDLIFE_CUDACOMMON_H
