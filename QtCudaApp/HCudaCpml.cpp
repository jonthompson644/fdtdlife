/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "HCudaCpml.h"
#include "ECudaCpml.h"
#include "CudaIncludes.h"
#include "CudaCommon.h"
#include "CudaIndexing.h"
#include "CudaPlaneWaveSource.h"
#include <Model.h>
#include <Domain.h>
#include <Material.h>
#include <CpmlMaterial.h>
#include <ThinSheetMaterial.h>
#include <Sources.h>
#include <algorithm>

// Constructor
HCudaCpml::HCudaCpml(fdtd::Model* m, CudaCommon* common) :
        HField(m),
        CudaCpmlField(m->p(), common, "H"),
        thinInRatio_(&common->cudaProxy_, "thinInRatio_"),
        thinOutRatio_(&common->cudaProxy_, "thinOutRatio_") {
}

// Initialise the model
void HCudaCpml::initialise() {
    HField::initialise();
    CudaCpmlField::initialise();
    // CUDA unified memory for the thin layer constants
    thinInRatio_.alloc(common_->matDepthArraySize_);
    thinOutRatio_.alloc(common_->matDepthArraySize_);
    // Now generate the constants
    initialiseConstantArrays();
    // And the cell ranges
    initialiseStartEnd();
}

// Print the data
void HCudaCpml::print() {
    CudaCpmlField::print("H Field");
}

// Return the amount of memory used
size_t HCudaCpml::memoryUse() {
    return CudaCpmlField::memoryUse();
}

// Return a value from the model
box::Vector<double> HCudaCpml::value(const box::Vector<int>& cell) {
    return CudaCpmlField::value(cell);
}

// Calculate the main model update
void HCudaCpml::stepField(const box::ThreadInfo* /*info*/) {
    int i, j, k;
    int psiIndex;
    auto* e = dynamic_cast<ECudaCpml*>(m_->e());
    if(e != nullptr) {
        // Step the grid
        common_->cudaProxy_.fdtdHFieldStep(
                start_.x(), start_.y(), start_.z(),
                end_.x(), end_.y(), end_.z(),
                p_->nx(), p_->ny(), p_->nz(),
                e->x_, e->y_, e->z_,
                x_, y_, z_,
                common_->material_, p_->boundarySize(),
                common_->boundaryDepthX_, common_->boundaryDepthY_, common_->boundaryDepthZ_,
                common_->drX_, common_->drY_, common_->drZ_,
                caX_, cbX_,
                caY_, cbY_,
                caZ_, cbZ_,
                e->thinExtraZ_, thinInRatio_, thinOutRatio_);
        // Now the CPML boundary modifications (note uses non-stretched coordinate values)
        // The X CPML modifications
        if(p_->boundaryX() == fdtd::Parameters::Boundary::absorbing) {
            common_->cudaProxy_.cpmlHFieldXLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    yxPsiL_, zxPsiL_,
                    cpmlBX_, cpmlCX_,
                    caY_, caZ_,
                    psiWidth_, p_->dr().x());
            common_->cudaProxy_.cpmlHFieldXRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    yxPsiR_, zxPsiR_,
                    cpmlBX_, cpmlCX_,
                    caY_, caZ_,
                    psiWidth_, p_->dr().x(), p_->n().x());
        }
        // The Y CPML modifications
        if(p_->boundaryY() == fdtd::Parameters::Boundary::absorbing) {
            common_->cudaProxy_.cpmlHFieldYLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xyPsiL_, zyPsiL_,
                    cpmlBY_, cpmlCY_,
                    caX_, caZ_,
                    psiWidth_, p_->dr().y());
            common_->cudaProxy_.cpmlHFieldYRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xyPsiR_, zyPsiR_,
                    cpmlBY_, cpmlCY_,
                    caX_, caZ_,
                    psiWidth_, p_->dr().y(), p_->n().y());
        }
        // The Z CPML modifications
        if(p_->boundaryZ() == fdtd::Parameters::Boundary::absorbing) {
            common_->cudaProxy_.cpmlHFieldZLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xzPsiL_, yzPsiL_,
                    cpmlBZ_, cpmlCZ_,
                    caX_, caY_,
                    psiWidth_, p_->dr().z());
            common_->cudaProxy_.cpmlHFieldZRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    e->x_, e->y_, e->z_,
                    x_, y_, z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xzPsiR_, yzPsiR_,
                    cpmlBZ_, cpmlCZ_,
                    caX_, caY_,
                    psiWidth_, p_->dr().z(), p_->n().z());
        }
    }
}

// Perform a single step on part of the model
// For shared memory multiprocessing we partition the model
// along the Z axis.
void HCudaCpml::step(const box::ThreadInfo* info) {
    // Step the grid
    stepField(info);
    // Now correct for any incident waves at the points on the
    // boundary between the total field and scattered field zones
    for(auto& source : m_->s()->sources()) {
        if(dynamic_cast<CudaPlaneWaveSource*>(source) != nullptr) {
            source->stepH(info, nullptr, nullptr, nullptr);
        } else {
            //source->stepH(info, x_.host(), y_.host(), z_.host());
        }
    }
    // Handle any periodic boundary conditions
    common_->cudaProxy_.periodicBoundariesH(x_, y_, z_, common_->info_);
}

// Initialise the arrays of constants
void HCudaCpml::initialiseConstantArrays() {
    fdtd::Parameters* p = m_->p();
    fdtd::Domain* d = m_->d();
    // Calculate the constants for each material
    for(auto pos : d->materialLookup()) {
        fdtd::Material* mat = pos.second;
        for(int boundaryDepth = -1; boundaryDepth <= p->boundarySize(); boundaryDepth++) {
            int idx = common_->matDepthIndex(mat->index(), boundaryDepth);
            caX_.host()[idx] = (float)mat->cah(fdtd::Material::directionX, boundaryDepth);
            caY_.host()[idx] = (float)mat->cah(fdtd::Material::directionY, boundaryDepth);
            caZ_.host()[idx] = (float)mat->cah(fdtd::Material::directionZ, boundaryDepth);
            cbX_.host()[idx] = (float)mat->cbh(fdtd::Material::directionX, boundaryDepth);
            cbY_.host()[idx] = (float)mat->cbh(fdtd::Material::directionY, boundaryDepth);
            cbZ_.host()[idx] = (float)mat->cbh(fdtd::Material::directionZ, boundaryDepth);
            auto thinMat = dynamic_cast<fdtd::ThinSheetMaterial*>(mat);
            if(m_->p()->useThinSheetSubcells() && thinMat != nullptr) {
                thinInRatio_.host()[idx] = (float)thinMat->inRatio();
                thinOutRatio_.host()[idx] = (float)thinMat->outRatio();
            } else {
                thinInRatio_.host()[idx] = (float)0.0;
                thinOutRatio_.host()[idx] = (float)1.0;
            }
            auto boundaryMat = dynamic_cast<fdtd::CpmlMaterial*>(mat);
            if(boundaryMat != nullptr) {
                cpmlBX_.host()[idx] = (float)boundaryMat->b(fdtd::Material::directionX, boundaryDepth);
                cpmlCX_.host()[idx] = (float)boundaryMat->c(fdtd::Material::directionX, boundaryDepth);
                cpmlBY_.host()[idx] = (float)boundaryMat->b(fdtd::Material::directionY, boundaryDepth);
                cpmlCY_.host()[idx] = (float)boundaryMat->c(fdtd::Material::directionY, boundaryDepth);
                cpmlBZ_.host()[idx] = (float)boundaryMat->b(fdtd::Material::directionZ, boundaryDepth);
                cpmlCZ_.host()[idx] = (float)boundaryMat->c(fdtd::Material::directionZ, boundaryDepth);
            } else {
                cpmlBX_.host()[idx] = (float)0.0;
                cpmlCX_.host()[idx] = (float)0.0;
                cpmlBY_.host()[idx] = (float)0.0;
                cpmlCY_.host()[idx] = (float)0.0;
                cpmlBZ_.host()[idx] = (float)0.0;
                cpmlCZ_.host()[idx] = (float)0.0;
            }
        }
    }
}

// Initialise the start and end cell coordinates
void HCudaCpml::initialiseStartEnd() {
    end_.x(m_->p()->n().x());
    end_.y(m_->p()->n().y());
    end_.z(m_->p()->n().z());
    // Only calculate the 0th entries on periodic axes
    start_.x(1);
    start_.y(1);
    start_.z(1);
    if(m_->p()->boundaryX() == fdtd::Parameters::Boundary::periodic) {
        start_.x(0);
    }
    if(m_->p()->boundaryY() == fdtd::Parameters::Boundary::periodic) {
        start_.y(0);
    }
    if(m_->p()->boundaryZ() == fdtd::Parameters::Boundary::periodic) {
        start_.z(0);
    }
}

