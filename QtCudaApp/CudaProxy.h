/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAPROXY_H
#define FDTDLIFE_CUDAPROXY_H

#include <stddef.h>
#include <string>
#include <vector>
#include "CudaMemory.h"
#include "CudaInfo.h"

// This class acts as the interface to the CUDA engine
class CudaProxy {
public:
    // Construction
    CudaProxy();
    ~CudaProxy();

    // Capabilities information
    int numGpus_;
    enum ComputeMode {computeModeDefault, computeModeExclusive, computeModeProhibited};
    struct GpuProps {
        int clockRateKhz_;
        ComputeMode computeMode_;
        int majorCapability_;
        int minorCapability_;
        std::string name_;
        size_t totalMemory_;
        size_t freeMemory_;
        int numMultiprocessors_;
        int maxThreadsPerBlock_;
        int maxThreadsPerMultiprocessor_;
    };
    std::vector<GpuProps> gpuProps_;
    int driverVersion_;
    int runtimeVersion_;

    // API to the wrapped CUDA library
    void malloc(void** memory, size_t size);
    void free(void* memory);
    void memcpy(void* destination, void* source, size_t count, bool toHost);

    // API to the kernels
    void fdtdEFieldStep(int startX, int startY, int startZ,
                        int endX, int endY, int endZ,
                        int nX, int nY, int nZ,
                        CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                        CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                        CudaMemory<char>& material, int boundarySize,
                        CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                        CudaMemory<char>& boundaryDepthZ,
                        CudaMemory<float>& drX, CudaMemory<float>& drY, CudaMemory<float>& drZ,
                        CudaMemory<float>& caX, CudaMemory<float>& cbX,
                        CudaMemory<float>& caY, CudaMemory<float>& cbY,
                        CudaMemory<float>& caZ, CudaMemory<float>& cbZ,
                        CudaMemory<float>& thinExtraZ, CudaMemory<float>& caThin, CudaMemory<float>& cbThin);
    void fdtdHFieldStep(int startX, int startY, int startZ,
                        int endX, int endY, int endZ,
                        int nX, int nY, int nZ,
                        CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                        CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                        CudaMemory<char>& material, int boundarySize,
                        CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                        CudaMemory<char>& boundaryDepthZ,
                        CudaMemory<float>& drX, CudaMemory<float>& drY, CudaMemory<float>& drZ,
                        CudaMemory<float>& caX, CudaMemory<float>& cbX,
                        CudaMemory<float>& caY, CudaMemory<float>& cbY,
                        CudaMemory<float>& caZ, CudaMemory<float>& cbZ,
                        CudaMemory<float>& thinExtraZ, CudaMemory<float>& thinInRatio, CudaMemory<float>& thinOutRatio);
    void cpmlEFieldXLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& yxPsiL, CudaMemory<float>& zxPsiL,
                         CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                         CudaMemory<float>& caY, CudaMemory<float>& caZ,
                         int psiWidth, float dx);
    void cpmlEFieldXRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& yxPsiR, CudaMemory<float>& zxPsiR,
                          CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                          CudaMemory<float>& caY, CudaMemory<float>& caZ,
                          int psiWidth, float dx, int nx);
    void cpmlHFieldXLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& yxPsiL, CudaMemory<float>& zxPsiL,
                         CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                         CudaMemory<float>& caY, CudaMemory<float>& caZ,
                         int psiWidth, float dx);
    void cpmlHFieldXRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& yxPsiR, CudaMemory<float>& zxPsiR,
                          CudaMemory<float>& cpmlBX, CudaMemory<float>& cpmlCX,
                          CudaMemory<float>& caY, CudaMemory<float>& caZ,
                          int psiWidth, float dx, int nx);
    void cpmlEFieldYLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& xyPsiL, CudaMemory<float>& zyPsiL,
                         CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                         CudaMemory<float>& caX, CudaMemory<float>& caZ,
                         int psiWidth, float dy);
    void cpmlEFieldYRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& xyPsiR, CudaMemory<float>& zyPsiR,
                          CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                          CudaMemory<float>& caX, CudaMemory<float>& caZ,
                          int psiWidth, float dy, int ny);
    void cpmlHFieldYLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& xyPsiL, CudaMemory<float>& zyPsiL,
                         CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                         CudaMemory<float>& caX, CudaMemory<float>& caZ,
                         int psiWidth, float dy);
    void cpmlHFieldYRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& xyPsiR, CudaMemory<float>& zyPsiR,
                          CudaMemory<float>& cpmlBY, CudaMemory<float>& cpmlCY,
                          CudaMemory<float>& caX, CudaMemory<float>& caZ,
                          int psiWidth, float dy, int ny);
    void cpmlEFieldZLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& xzPsiL, CudaMemory<float>& yzPsiL,
                         CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                         CudaMemory<float>& caX, CudaMemory<float>& caY,
                         int psiWidth, float dz);
    void cpmlEFieldZRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& xzPsiR, CudaMemory<float>& yzPsiR,
                          CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                          CudaMemory<float>& caX, CudaMemory<float>& caY,
                          int psiWidth, float dz, int nz);
    void cpmlHFieldZLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                         CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<float>& xzPsiL, CudaMemory<float>& yzPsiL,
                         CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                         CudaMemory<float>& caX, CudaMemory<float>& caY,
                         int psiWidth, float dz);
    void cpmlHFieldZRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<float>& ex, CudaMemory<float>& ey, CudaMemory<float>& ez,
                          CudaMemory<float>& hx, CudaMemory<float>& hy, CudaMemory<float>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<float>& xzPsiR, CudaMemory<float>& yzPsiR,
                          CudaMemory<float>& cpmlBZ, CudaMemory<float>& cpmlCZ,
                          CudaMemory<float>& caX, CudaMemory<float>& caY,
                          int psiWidth, float dz, int nz);
    void tfsfE(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
               CudaMemory<float>& auxHy, CudaMemory<CudaInfo>& info);
    void tfsfH(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
               CudaMemory<float>& auxEx, CudaMemory<CudaInfo>& info);
    void periodicBoundariesE(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
                                      CudaMemory<CudaInfo>& info);
    void periodicBoundariesH(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, 
                                      CudaMemory<CudaInfo>& info);
    void getXPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
    void getYPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
    void getZPlaneData(CudaMemory<float>& field, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
    void getXPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
    void getYPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
    void getZPlaneMagData(CudaMemory<float>& x, CudaMemory<float>& y, CudaMemory<float>& z, CudaMemory<float>& capture, CudaMemory<CudaInfo>& info);
};


#endif //FDTDLIFE_CUDAPROXY_H
