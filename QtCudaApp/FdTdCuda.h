/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FDTDCUDA_H
#define FDTDLIFE_FDTDCUDA_H


#include <FdTdLife.h>
#include <chrono>
namespace fdtd {class GeneticSearch;}
class CudaCommon;
class ECudaCpml;
class HCudaCpml;

class FdTdCuda : public FdTdLife {
public:
    // Construction
    FdTdCuda();
    ~FdTdCuda() override;

    // Overrides of fdtd::FdTdLife
    void createFields() override;

    // Getters
    CudaCommon* common();

public:
    // Variables
    CudaCommon* common_;
    ECudaCpml* cudaE_;
    HCudaCpml* cudaH_;

protected:
    // Helpers
    //void geneticIndividualComplete(fdtd::GeneticSearch* geneticSearch);
    //void geneticGenerationComplete(fdtd::GeneticSearch* geneticSearch);

};

#endif //FDTDLIFE_FDTDCUDA_H
