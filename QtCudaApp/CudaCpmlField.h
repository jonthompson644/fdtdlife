/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDACPMLFIELD_H
#define FDTDLIFE_CUDACPMLFIELD_H

#include <string>
#include <CpmlField.h>
#include <Box/Vector.h>
#include "CudaMemory.h"
class CudaCommon;
namespace fdtd {class Parameters;}

class CudaCpmlField {
public:
    // Construction
    CudaCpmlField(fdtd::Parameters* p, CudaCommon* common, const std::string& name);

    // API
    void initialise();
    size_t memoryUse();
    box::Vector<double> value(const box::Vector<int>& cell);
    bool writeData(std::ofstream& file);
    bool readData(std::ifstream& file);
    int wxPsiIndex(int x, int y, int z) const;
    int wyPsiIndex(int x, int y, int z) const;
    int wzPsiIndex(int x, int y, int z) const;
    int index(int i, int j, int k) const;
    void print(const char* title);

public:
    // The FDTD cell memory, indexed by x,y,z
    CudaMemory<float> x_;
    CudaMemory<float> y_;
    CudaMemory<float> z_;

    // Memory indexed by the special CPML indices
    CudaMemory<float> xyPsi_;
    CudaMemory<float> xzPsi_;
    CudaMemory<float> yzPsi_;
    CudaMemory<float> yxPsi_;
    CudaMemory<float> zxPsi_;
    CudaMemory<float> zyPsi_;
    CudaMemory<float> yxPsiL_;
    CudaMemory<float> yxPsiR_;
    CudaMemory<float> zxPsiL_;
    CudaMemory<float> zxPsiR_;
    CudaMemory<float> xyPsiL_;
    CudaMemory<float> xyPsiR_;
    CudaMemory<float> zyPsiL_;
    CudaMemory<float> zyPsiR_;
    CudaMemory<float> xzPsiL_;
    CudaMemory<float> xzPsiR_;
    CudaMemory<float> yzPsiL_;
    CudaMemory<float> yzPsiR_;

    // Memory containing material constants, indexed by material id and boundary depth
    CudaMemory<float> caX_;   // The A constant for x (The average when containing a thin material)
    CudaMemory<float> caY_;   // The A constant for y (The average when containing a thin material)
    CudaMemory<float> caZ_;   // The A constant for z
    CudaMemory<float> cbX_;   // The B constant for x (The average when containing a thin material)
    CudaMemory<float> cbY_;   // The B constant for y (The average when containing a thin material)
    CudaMemory<float> cbZ_;   // The B constant for z
    CudaMemory<float> cpmlBX_;  // The CPML boundary B constant
    CudaMemory<float> cpmlBY_;  // The CPML boundary B constant
    CudaMemory<float> cpmlBZ_;  // The CPML boundary B constant
    CudaMemory<float> cpmlCX_;  // The CPML boundary C constant
    CudaMemory<float> cpmlCY_;  // The CPML boundary C constant
    CudaMemory<float> cpmlCZ_;  // The CPML boundary C constant

    // Other members
    fdtd::Parameters* p_;
    size_t arraySize_;
    size_t arraySizePsi_;
    size_t arraySizePsiLR_;
    int psiWidth_;
    box::Vector<int> start_;
    box::Vector<int> end_;
    CudaCommon* common_;   // Things common to both fields
};


#endif //FDTDLIFE_CUDACPMLFIELD_H
