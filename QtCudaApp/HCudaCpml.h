/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_HCUDACPML_H
#define FDTDLIFE_HCUDACPML_H

#include <HField.h>
#include "CudaCpmlField.h"

namespace fdtd { class Model; }

// A class that calculates the electric field using a total
// field representation and a simple normally absorbing boundary
class HCudaCpml : public fdtd::HField, public CudaCpmlField {
public:
    // Construction
    HCudaCpml(fdtd::Model* m, CudaCommon* common);

    // Overrides of fdtd::HField
    void initialise() override;
    box::Vector<double> value(const box::Vector<int>& cell) override;
    void print() override;
    size_t memoryUse() override;
    bool writeData(std::ofstream& file) override { return CudaCpmlField::writeData(file); }
    bool readData(std::ifstream& file) override { return CudaCpmlField::readData(file); }
    void step(const box::ThreadInfo* info) override;

protected:
    // Helper functions
    void stepField(const box::ThreadInfo* info);
    void initialiseConstantArrays();
    void initialiseStartEnd();

    // Thin layer constants indexed by material identifier
    CudaMemory<float> thinInRatio_;  // The ratio of thin layer in to total
    CudaMemory<float> thinOutRatio_;  // The ratio of thin layer out to total
};


#endif //FDTDLIFE_HCUDACPML_H
