/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAARRAYSENSOR_H
#define FDTDLIFE_CUDAARRAYSENSOR_H

#include <ArraySensor.h>
#include "CudaMemory.h"
class FdTdCuda;

// A specialisation of the Array Sensor that works with the GPU
class CudaArraySensor : public fdtd::ArraySensor {
public:
    // Construction
    CudaArraySensor(int identifier, const char* name, fdtd::Parameters::Color color,
                bool saveCsvData, int numPointsX, int numPointsY, double frequency,
                bool animateFrequency, double frequencyStep, bool averageOverCell,
                fdtd::Model* m);
    CudaArraySensor(int identifier, fdtd::Model* m);

    // Overrides of ArraySensor
    void collect() override;
    void initialise() override;
    sensor::Sensor* create(int identifier, fdtd::Model* m) override;
    box::Vector<double> transmittedValue(int x, int y) override;
    box::Vector<double> reflectedValue(int x, int y) override;

protected:
    // Members
    CudaMemory<float> transEx_;  // Memory shared with GPU
    CudaMemory<float> transEy_;  // Memory shared with GPU
    CudaMemory<float> transEz_;  // Memory shared with GPU
    CudaMemory<float> reflEx_;  // Memory shared with GPU
    CudaMemory<float> reflEy_;  // Memory shared with GPU
    CudaMemory<float> reflEz_;  // Memory shared with GPU
    FdTdCuda* m_;  // The CUDA main object
};

#endif //FDTDLIFE_CUDAARRAYSENSOR_H
