/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaArraySensor.h"
#include "FdTdCuda.h"
#include "CudaCommon.h"
#include "ECudaCpml.h"
#include "HCudaCpml.h"
#include "CudaIndexing.h"

// Constructor
CudaArraySensor::CudaArraySensor(int identifier, const char* name, fdtd::Parameters::Color color,
                bool saveCsvData, int numPointsX, int numPointsY, double frequency,
                bool animateFrequency, double frequencyStep, bool averageOverCell,
                fdtd::Model* m) :
        ArraySensor(identifier, name, color, saveCsvData, numPointsX, numPointsY, frequency,
                    animateFrequency, frequencyStep, averageOverCell, m),
        transEx_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEx"),
        transEy_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEy"),
        transEz_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEz"),
        reflEx_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEx"),
        reflEy_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEy"),
        reflEz_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEz"),
        m_(dynamic_cast<FdTdCuda*>(m)) {
}

//Constructor
CudaArraySensor::CudaArraySensor(int identifier, fdtd::Model* m) :
        ArraySensor(identifier, m),
        transEx_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEx"),
        transEy_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEy"),
        transEz_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "transEz"),
        reflEx_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEx"),
        reflEy_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEy"),
        reflEz_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "reflEz"),
        m_(dynamic_cast<FdTdCuda*>(m)) {
}

// Prepare the sensor for a run
void CudaArraySensor::initialise() {
    // Base class does most of the work
    ArraySensor::initialise();
    // Size the GPU capture memory
    transEx_.alloc(m_->p()->n().x() * m_->p()->n().y());
    transEy_.alloc(m_->p()->n().x() * m_->p()->n().y());
    transEz_.alloc(m_->p()->n().x() * m_->p()->n().y());
    reflEx_.alloc(m_->p()->n().x() * m_->p()->n().y());
    reflEy_.alloc(m_->p()->n().x() * m_->p()->n().y());
    reflEz_.alloc(m_->p()->n().x() * m_->p()->n().y());
}

// Collect data for the sensor
void CudaArraySensor::collect() {
    // Fill the capture memory
    m_->common()->info_.host()->sliceOffset_ = transmittedZ_;
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->x_, transEx_, m_->common()->info_);
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->y_, transEy_, m_->common()->info_);
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->z_, transEz_, m_->common()->info_);
    m_->common()->info_.host()->sliceOffset_ = reflectedZ_;
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->x_, reflEx_, m_->common()->info_);
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->y_, reflEy_, m_->common()->info_);
    m_->common()->proxy()->getZPlaneData(m_->cudaE_->z_, reflEz_, m_->common()->info_);
    // Now call the standard collect
    ArraySensor::collect();
}

// Return a transmitted electric field value
box::Vector<double> CudaArraySensor::transmittedValue(int x, int y) {
    int idx = CUDAINDEX2DSENSOR(x, y, m_->p()->n().x());
    return {transEx_.host()[idx], transEy_.host()[idx], transEz_.host()[idx]};
}

// Return a reflected electric field value
box::Vector<double> CudaArraySensor::reflectedValue(int x, int y) {
    int idx = CUDAINDEX2DSENSOR(x, y, m_->p()->n().x());
    return {reflEx_.host()[idx], reflEy_.host()[idx], reflEz_.host()[idx]};
}

// Create a new sensor of this type
sensor::Sensor* CudaArraySensor::create(int identifier, fdtd::Model* m) {
    return new CudaArraySensor(identifier, m);
}
