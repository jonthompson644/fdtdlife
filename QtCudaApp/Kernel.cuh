/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "cuda_runtime.h"
#include "CudaInfo.h"

cudaError_t kernelFdtdEStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           float* ex, float* ey, float* ez,
                           float* hx, float* hy, float* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           float* drX, float* drY, float* drZ,
                           float* caX, float* cbX,
                           float* caY, float* cbY,
                           float* caZ, float* cbZ,
                           float* thinExtraZ, float* caThin, float* cbThin);
cudaError_t kernelFdtdHStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           float* ex, float* ey, float* ez,
                           float* hx, float* hy, float* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           float* drX, float* drY, float* drZ,
                           float* caX, float* cbX,
                           float* caY, float* cbY,
                           float* caZ, float* cbZ,
                           float* thinExtraZ, float* thinInRatio, float* thinOutRatio);
cudaError_t kernelCpmlEFieldXLeft(int startX, int startY, int startZ,
                                  int endX, int endY, int endZ,
                                  int arraySizeX, int arraySizeY, int arraySizeZ,
                                  float* ex, float* ey, float* ez,
                                  float* hx, float* hy, float* hz,
                                  char* material, int boundarySize,
                                  char* boundaryDepthX, char* boundaryDepthY,
                                  char* boundaryDepthZ,
                                  float* yxPsiL, float* zxPsiL,
                                  float* cpmlBX, float* cpmlCX,
                                  float* caY, float* caZ,
                                  int psiWidth, float dx);
cudaError_t kernelCpmlEFieldXRight(int startX, int startY, int startZ,
                                   int endX, int endY, int endZ,
                                   int arraySizeX, int arraySizeY, int arraySizeZ,
                                   float* ex, float* ey, float* ez,
                                   float* hx, float* hy, float* hz,
                                   char* material, int boundarySize,
                                   char* boundaryDepthX, char* boundaryDepthY,
                                   char* boundaryDepthZ,
                                   float* yxPsiR, float* zxPsiR,
                                   float* cpmlBX, float* cpmlCX,
                                   float* caY, float* caZ,
                                   int psiWidth, float dx, int nx);
cudaError_t kernelCpmlHFieldXLeft(int startX, int startY, int startZ,
                                  int endX, int endY, int endZ,
                                  int arraySizeX, int arraySizeY, int arraySizeZ,
                                  float* ex, float* ey, float* ez,
                                  float* hx, float* hy, float* hz,
                                  char* material, int boundarySize,
                                  char* boundaryDepthX, char* boundaryDepthY,
                                  char* boundaryDepthZ,
                                  float* yxPsiL, float* zxPsiL,
                                  float* cpmlBX, float* cpmlCX,
                                  float* caY, float* caZ,
                                  int psiWidth, float dx);
cudaError_t kernelCpmlHFieldXRight(int startX, int startY, int startZ,
                                   int endX, int endY, int endZ,
                                   int arraySizeX, int arraySizeY, int arraySizeZ,
                                   float* ex, float* ey, float* ez,
                                   float* hx, float* hy, float* hz,
                                   char* material, int boundarySize,
                                   char* boundaryDepthX, char* boundaryDepthY,
                                   char* boundaryDepthZ,
                                   float* yxPsiR, float* zxPsiR,
                                   float* cpmlBX, float* cpmlCX,
                                   float* caY, float* caZ,
                                   int psiWidth, float dx, int nx);
cudaError_t kernelCpmlEFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy);
cudaError_t kernelCpmlEFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny);
cudaError_t kernelCpmlHFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xyPsiL, float* zyPsiL,
                                float* cpmlBY, float* cpmlCY,
                                float* caX, float* caZ,
                                int psiWidth, float dy);
cudaError_t kernelCpmlHFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xyPsiR, float* zyPsiR,
                                 float* cpmlBY, float* cpmlCY,
                                 float* caX, float* caZ,
                                 int psiWidth, float dy, int ny);
cudaError_t kernelCpmlEFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz);
cudaError_t kernelCpmlEFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz);
cudaError_t kernelCpmlHFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                float* ex, float* ey, float* ez,
                                float* hx, float* hy, float* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                float* xzPsiL, float* yzPsiL,
                                float* cpmlBZ, float* cpmlCZ,
                                float* caX, float* caY,
                                int psiWidth, float dz);
cudaError_t kernelCpmlHFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 float* ex, float* ey, float* ez,
                                 float* hx, float* hy, float* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 float* xzPsiR, float* yzPsiR,
                                 float* cpmlBZ, float* cpmlCZ,
                                 float* caX, float* caY,
                                 int psiWidth, float dz, int nz);
cudaError_t kernelTfsfE(float* x, float* y, float* z, const float* auxHy, 
                        const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelTfsfH(float* x, float* y, float* z, const float* auxEx, 
                        const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelPeriodicBoundariesE(float* x, float* y, float* z, 
                                      const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelPeriodicBoundariesH(float* x, float* y, float* z, 
                                      const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetXPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetYPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetZPlaneData(float* field, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetXPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetYPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
cudaError_t kernelGetZPlaneMagData(float* x, float* y, float* z, float* capture, const CudaInfo* hostInfo, const CudaInfo* gpuInfo);
