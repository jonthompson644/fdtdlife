/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAPLANEWAVESOURCE_H
#define FDTDLIFE_CUDAPLANEWAVESOURCE_H

#include <PlaneWaveSource.h>
#include "CudaMemory.h"
class CudaCommon;
class FdTdCuda;

// The CUDA GPU version of the plane wave source
class CudaPlaneWaveSource : public fdtd::PlaneWaveSource {
public:
    // Construction
    CudaPlaneWaveSource(int id, const std::string& name, double frequency, double amplitude,
                        double polarisation, double pmlSigma,  double spacing,
                        int n, bool continuous, double time, double pmlSigmaFactor,
                        double azimuth, double initialTime, FdTdCuda* m);
    CudaPlaneWaveSource(int id, fdtd::Model* m);
    ~CudaPlaneWaveSource() override;

    // Methods
    void initialise() override;
    void stepE(const box::ThreadInfo* info, double* x, double* y, double* z) override;
    void stepH(const box::ThreadInfo* info, double* x, double* y, double* z) override;
    fdtd::Source* create(int identifier, fdtd::Model* m);

protected:
    // Helper functions
    void updateInfo();

    // Shared memory
    CudaMemory<float> exShared_;
    CudaMemory<float> hyShared_;
    // Information
    CudaCommon* common_;
    FdTdCuda* m_;
};

#endif //FDTDLIFE_CUDAPLANEWAVESOURCE_H
