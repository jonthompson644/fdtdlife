/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDATWODSLICE_H
#define FDTDLIFE_CUDATWODSLICE_H

#include <TwoDSlice.h>
#include "CudaMemory.h"
class CudaCommon;
class FdTdCuda;

class CudaTwoDSlice : public fdtd::TwoDSlice {
public:
    // Construction
    CudaTwoDSlice(int identifier, const char* name, fdtd::Parameters::Color color,
                bool saveCsvData, fdtd::Model* m, DataSource dataSource,
                  box::Constants::Orientation orientation, double offset);
    CudaTwoDSlice(int identifier, fdtd::Model* m);
    ~CudaTwoDSlice() override;

    // Overrides of TwoDSlice
    void collect() override;
    void initialise() override;
    sensor::Sensor* create(int identifier, fdtd::Model* m) override;

protected:
    // Members
    CudaMemory<float> capture_;  // Memory shared with GPU
    FdTdCuda* m_;  // The CUDA main object
};

#endif //FDTDLIFE_CUDATWODSLICE_H
