/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaTwoDSlice.h"
#include "FdTdCuda.h"
#include "CudaCommon.h"
#include "ECudaCpml.h"
#include "HCudaCpml.h"
#include "CudaIndexing.h"
#include <Box/Vector.h>

// Constructor
CudaTwoDSlice::CudaTwoDSlice(int identifier, const char* name, 
                             fdtd::Parameters::Color color,
                             bool saveCsvData, fdtd::Model* m, DataSource dataSource,
                             box::Constants::Orientation orientation, double offset) :
        TwoDSlice(identifier, name, color, saveCsvData, m, dataSource, orientation, offset),
        capture_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "TwoDCap"),
        m_(dynamic_cast<FdTdCuda*>(m)) {
}

// Constructor
CudaTwoDSlice::CudaTwoDSlice(int identifier, fdtd::Model* m) :
        TwoDSlice(identifier, m),
        capture_(dynamic_cast<FdTdCuda*>(m)->common()->proxy(), "TwoDCap"),
        m_(dynamic_cast<FdTdCuda*>(m)) {
}

// Destructor
CudaTwoDSlice::~CudaTwoDSlice() {
}

// Initialise the sensor
// TODO: We should be able to call the base class for some of this rather
// than having copied it into here.  The problem is the call to collect.
void CudaTwoDSlice::initialise() {
    // The size and start of the data
    switch(orientation_) {
        case box::Constants::orientationXPlane:
            nSliceX_ = m_->p()->n().y();
            nSliceY_ = m_->p()->n().z();
            sliceOffset_ = m_->p()->cellRound(box::Vector<double>(offset_, 0.0, 0.0)).x();
            break;
        case box::Constants::orientationYPlane:
            nSliceX_ = m_->p()->n().x();
            nSliceY_ = m_->p()->n().z();
            sliceOffset_ = m_->p()->cellRound(box::Vector<double>(0.0, offset_, 0.0)).y();
            break;
        case box::Constants::orientationZPlane:
            nSliceX_ = m_->p()->n().x();
            nSliceY_ = m_->p()->n().y();
            sliceOffset_ = m_->p()->cellRound(box::Vector<double>(0.0, 0.0, offset_)).z();
            break;
    }
    // Allocate new data
    data_ = new double[nSliceX_ * nSliceY_];
    for(int i=0; i<nSliceX_ * nSliceY_; i++) {
        data_[i] = 0.0;
    }
    // Allocate shared data
    capture_.alloc(nSliceX_ * nSliceY_);
    capture_.set(0.0);
    // As this sensor does not store historical data we should collect now
    collect();
}

// Collect a slice from the model.
void CudaTwoDSlice::collect() {
    if(dataSource_ == material) {
        // Material view is unchanged
        TwoDSlice::collect();
    } else {
        if(data_ != nullptr) {
            // Set up the CUDA information
            m_->common()->info_.host()->sliceOffset_ = sliceOffset_;
            // Get the data
            switch(dataSource_) {
            default:
            case eMagnitude:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneMagData(m_->cudaE_->x_, m_->cudaE_->y_, m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneMagData(m_->cudaE_->x_, m_->cudaE_->y_, m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneMagData(m_->cudaE_->x_, m_->cudaE_->y_, m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case hMagnitude:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneMagData(m_->cudaH_->x_, m_->cudaH_->y_, m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneMagData(m_->cudaH_->x_, m_->cudaH_->y_, m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneMagData(m_->cudaH_->x_, m_->cudaH_->y_, m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case ex:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaE_->x_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaE_->x_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaE_->x_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case ey:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaE_->y_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaE_->y_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaE_->y_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case ez:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaE_->z_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case hx:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaH_->x_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaH_->x_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaH_->x_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case hy:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaH_->y_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaH_->y_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaH_->y_, capture_, m_->common()->info_);
                    break;
                }
                break;
            case hz:
                switch (orientation_) {
                default:
                case box::Constants::orientationXPlane:
                    m_->common()->proxy()->getXPlaneData(m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationYPlane:
                    m_->common()->proxy()->getYPlaneData(m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                case box::Constants::orientationZPlane:
                    m_->common()->proxy()->getZPlaneData(m_->cudaH_->z_, capture_, m_->common()->info_);
                    break;
                }
                break;
            }
            // Copy the data
            float* shared = capture_.host();
            for(int i=0; i<nSliceX_ * nSliceY_; i++) {
                data_[i] = shared[i];
            }
            // Find the max and min values
            findMinMax();
        }
    }
}

// Create a new sensor of this type
sensor::Sensor* CudaTwoDSlice::create(int identifier, fdtd::Model* m) {
    return new CudaTwoDSlice(identifier, m);
}
