/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaMemory.h"
#include "CudaProxy.h"
#include "CudaException.h"
#include "CudaInfo.h"
#include <iostream>

// Constructor
template<class T>
CudaMemory<T>::CudaMemory(CudaProxy* proxy, const std::string& name) :
        proxy_(proxy),
        host_(nullptr),
        gpu_(nullptr), 
        needsCopyToGpu_(false),
        needsCopyToHost_(false),
        name_(name) {
}

// Destructor
template<class T>
CudaMemory<T>::~CudaMemory() {
    free();
}

// Allocate host and GPU memory
template<class T>
void CudaMemory<T>::alloc(size_t size) {
    free();
    size_ = size;
    host_ = new T[size_];
    proxy_->malloc((void**) &gpu_, size_ * sizeof(T));
}

// Free host and GPU memory
template<class T>
void CudaMemory<T>::free() {
    delete[] host_;
    if(gpu_ != nullptr) {
        proxy_->free(gpu_);
    }
    host_ = nullptr;
    gpu_ = nullptr;
}

// Set host memory to a particular value
template<class T>
void CudaMemory<T>::set(T v) {
    for(size_t i = 0; i < size_; i++) {
        host_[i] = v;
    }
    needsCopyToGpu_ = true;
}

// Return a pointer to the host memory
template<class T>
const T* CudaMemory<T>::constHost() {
    // Does the memory need copying?
    if(needsCopyToHost_) {
        copyToHost();
    }
    // The pointer
    return host_;
}

// Return a pointer to the host memory
template<class T>
T* CudaMemory<T>::host() {
    // Does the memory need copying?
    if(needsCopyToHost_) {
        copyToHost();
    }
    // The pointer
    needsCopyToGpu_ = true;
    if(name_ == "info_") {
        int a = 1;
    }
    return host_;
}

// Return a pointer to the GPU memory
template<class T>
T* CudaMemory<T>::gpu() {
    // Does the memory need copying?
    if(needsCopyToGpu_) {
        copyToGpu();
    }
    // The pointer
    needsCopyToHost_ = true;
    return gpu_;
}

// Return a pointer to the GPU memory
template<class T>
const T* CudaMemory<T>::constGpu() {
    // Does the memory need copying?
    if(needsCopyToGpu_) {
        copyToGpu();
    }
    // The pointer
    return gpu_;
}

// Access to the host array with bounds checking
template<class T>
T& CudaMemory<T>::operator[] (size_t i) & {
    // Array bounds check
    if(i >= size_) {
        throw CudaException("Array bounds exceeded");
    }
    // Get data from GPU if necessary
    if(needsCopyToHost_) {
        copyToHost();
    }
    // Return the location for writing
    needsCopyToGpu_ = true;
    if(name_ == "info_") {
        int a = 1;
    }
    return host_[i];
}

// Access to the host array with bounds checking
template<class T>
T CudaMemory<T>::operator[] (size_t i) && {
    // Array bounds check
    if(i >= size_) {
        throw CudaException("Array bounds exceeded");
    }
    // Get data from GPU if necessary
    if(needsCopyToHost_) {
        copyToHost();
    }
    // Return the value read
    return host_[i];
}

// Access to the host array with bounds checking
template<class T>
T CudaMemory<T>::get(size_t i) {
    // Array bounds check
    if(i >= size_) {
        throw CudaException("Array bounds exceeded");
    }
    // Get data from GPU if necessary
    if(needsCopyToHost_) {
        copyToHost();
    }
    // Return the value read
    return host_[i];
}

// Copy the contents from the GPU to the host
template<class T>
void CudaMemory<T>::copyToHost() {
    proxy_->memcpy(host_, gpu_, size_ * sizeof(T), true);
    needsCopyToHost_ = false;
    if(name_ == "Ex_") {
        int a = 1;
    }
#ifdef _DEBUG
    std::cout << "CUDA memory " << name_ << ", copy to host" << std::endl;
#endif
}

// Copy the contents from the host to the GPU
template<class T>
void CudaMemory<T>::copyToGpu() {
    proxy_->memcpy(gpu_, host_, size_ * sizeof(T), false);
    needsCopyToGpu_ = false;
    if(name_ == "info_") {
        int a = 1;
    }
#ifdef _DEBUG
    std::cout << "CUDA memory " << name_ << ", copy to GPU" << std::endl;
#endif
}

// Instantiate particular types to make linking work
template class CudaMemory<float>;
template class CudaMemory<char>;
template class CudaMemory<CudaInfo>;


