/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/SignalGenerator.h>
#include <Sensor/SpectrumPoint.h>
#include <complex>

using namespace Catch::literals;

// Operation of a spectrum point X component
TEST_CASE("SpectrumPoint_xComponent") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator gen;
    gen.initialise(0.01, {10, 10, 3}, 0,
                   1.0);
    // The spectrum point to test
    sensor::SpectrumPoint spectrumPoint;
    spectrumPoint.initialise({5, 5, 10}, 1,
                             0.01, {true, false, false},
                             100,false);
    // Collect the time series for 1s
    for(int i = 0; i < 100; i++) {
        spectrumPoint.collect(gen.sample(i, 0.0), 0.0, 0.0);
    }
    // Convert it to a spectrum
    spectrumPoint.calculate();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        if(i == 1) {
            REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 3) {
            REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 5) {
            REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else {
            REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) == Approx(0.0).margin(0.00000001));
        }
        REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) == 0.0_a);
        REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) == 0.0_a);
    }
}

// Operation of a spectrum point Y component
TEST_CASE("SpectrumPoint_yComponent") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator gen;
    gen.initialise(0.01, {10, 10, 3}, 0,
                   1.0);
    // The spectrum point to test
    sensor::SpectrumPoint spectrumPoint;
    spectrumPoint.initialise({5, 5, 10}, 1,
                             0.01, {false, true, false},
                             100,false);
    // Collect the time series for 1s
    for(int i = 0; i < 100; i++) {
        spectrumPoint.collect(0.0, gen.sample(i, 0.0), 0.0);
    }
    // Convert it to a spectrum
    spectrumPoint.calculate();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        if(i == 1) {
            REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 3) {
            REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 5) {
            REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else {
            REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) == Approx(0.0).margin(0.00000001));
        }
        REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) == 0.0_a);
        REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) == 0.0_a);
    }
}

// Operation of a spectrum point Z component
TEST_CASE("SpectrumPoint_zComponent") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator gen;
    gen.initialise(0.01, {10, 10, 3}, 0,
                   1.0);
    // The spectrum point to test
    sensor::SpectrumPoint spectrumPoint;
    spectrumPoint.initialise({5, 5, 10}, 1,
                             0.01, {false, false, true},
                             100,false);
    // Collect the time series for 1s
    for(int i = 0; i < 100; i++) {
        spectrumPoint.collect(0.0, 0.0, gen.sample(i, 0.0));
    }
    // Convert it to a spectrum
    spectrumPoint.calculate();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        if(i == 1) {
            REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 3) {
            REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 5) {
            REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) ==
                    Approx(1.0 / 3.0).margin(0.00000001));
        } else {
            REQUIRE(std::abs(spectrumPoint.spectrumZ()[i]) ==
                    Approx(0.0).margin(0.00000001));
        }
        REQUIRE(std::abs(spectrumPoint.spectrumX()[i]) == 0.0_a);
        REQUIRE(std::abs(spectrumPoint.spectrumY()[i]) == 0.0_a);
    }
}
