/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// The operation of the array detector using a single cell
TEST_CASE("ArraySensor_SingleCell") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(
            -0.0001, -0.0001, -0.0008,
            +0.0001, +0.0001, +0.0008,
            5e-5, 5e-5, 5e-5,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::absorbing,
            false);
    model.p()->boundarySize(0);
    model.p()->tSize(box::Expression(1.04 / 10e9));
    auto* source = model.addPlaneWaveSource();
    source->frequencies().first(box::Expression(10e9));
    source->frequencies().spacing(box::Expression(10e9));
    source->frequencies().n(box::Expression(10));
    auto* arraySensor = model.addArraySensor();
    arraySensor->colour(box::Constants::colourWhite);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(4, 4, 36));
    // Does the sensorId initialise correctly
    REQUIRE(arraySensor->nSamples() == 1199);
    REQUIRE(arraySensor->reflectedZ() == 0);
    REQUIRE(arraySensor->reflectedReferenceZ() == 36);
    REQUIRE(arraySensor->transmittedZ() == 33);
    // Does the sensor appear in the material sensorId?
    REQUIRE(model.materialSensorRow(materialSensor, 0) ==
            std::vector<int>({0, 0, 7, 0}));
    REQUIRE(model.materialSensorRow(materialSensor, 33) ==
            std::vector<int>({0, 0, 7, 0}));
    // Run the model for 1199 steps
    model.start();
    size_t stepNum = 0;
    while(model.p()->timeStep() < 1199) {
        REQUIRE(model.p()->timeStep() == stepNum);
        stepNum++;
        model.step();
        model.step();
    }
    // Does the spectrum data look right?
    arraySensor->calculate();
    arraySensor->finalCalculate();
    size_t i = arraySensor->index(0, 0);
    REQUIRE(arraySensor->points()[i].transmitted().x().transmittance().size() == 10);
    for(size_t bin = 0;
        bin < arraySensor->points()[i].transmitted().x().transmittance().size();
        bin++) {
        INFO("Spectrum bin = " << bin);
        REQUIRE(arraySensor->points()[i].transmitted().x().transmittance()[bin] ==
                Approx(1.0).margin(0.0001));
        REQUIRE(arraySensor->points()[i].transmitted().y().transmittance()[bin] ==
                0.0_a);
        REQUIRE(arraySensor->points()[i].transmitted().z().transmittance()[bin] ==
                0.0_a);
    }
}

// The operation of the array detector using signal magnitude at the center of a cell
TEST_CASE("ArraySensor_MagnitudeAndPoint") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(
            -0.0001, -0.0001, -0.0008,
            +0.0001, +0.0001, +0.0008,
            5e-5, 5e-5, 5e-5,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::absorbing,
            false);
    model.p()->boundarySize(0);
    model.p()->tSize(box::Expression(1.04 / 10e9));
    auto* source = model.addPlaneWaveSource();
    source->frequencies().first(box::Expression(10e9));
    source->frequencies().spacing(box::Expression(10e9));
    source->frequencies().n(box::Expression(10));
    auto* arraySensor = model.addArraySensor();
    arraySensor->colour(box::Constants::colourWhite);
    arraySensor->numPointsY(8);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(4, 4, 36));
    // Does the sensorId initialise correctly
    REQUIRE(arraySensor->nSamples() == 1199);
    REQUIRE(arraySensor->reflectedZ() == 0);
    REQUIRE(arraySensor->reflectedReferenceZ() == 36);
    REQUIRE(arraySensor->transmittedZ() == 33);
    REQUIRE(arraySensor->points().size() == 8);
    // Run the model for 1199 steps
    model.start();
    size_t stepNum = 0;
    while(model.p()->timeStep() < 1199) {
        REQUIRE(model.p()->timeStep() == stepNum);
        stepNum++;
        model.step();
        model.step();
    }
    // Does the spectrum data look right?
    arraySensor->calculate();
    arraySensor->finalCalculate();
    for(size_t x = 0; x < 2; x++) {
        for(size_t y = 0; y < 4; y++) {
            size_t i = arraySensor->index((int) x, (int) y);
            INFO("x = " << x << ", y = " << y);
            REQUIRE(arraySensor->points()[i].transmitted().x().transmittance().size()
                    == 10);
            for(size_t bin = 0;
                bin < arraySensor->points()[i].transmitted().x().transmittance().size();
                bin++) {
                INFO("bin = " << bin);
                double v = arraySensor->points()[i].
                        transmitted().x().transmittance()[bin];
                if((x == 0 && y == 0) || bin == 0) {
                    REQUIRE(v == Approx(1.0).margin(0.05));
                } else {
                    REQUIRE(v == 0.0_a);
                }
            }
        }
    }
}

// The operation of the array detector using signal components at the center of a cell
TEST_CASE("ArraySensor_ComponentsAndPoint") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(
            -0.0001, -0.0001, -0.0008,
            +0.0001, +0.0001, +0.0008,
            5e-5, 5e-5, 5e-5,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::absorbing,
            false);
    model.p()->boundarySize(0);
    model.p()->tSize(box::Expression(1.04 / 10e9));
    auto* source = model.addPlaneWaveSource();
    source->frequencies().first(box::Expression(10e9));
    source->frequencies().spacing(box::Expression(10e9));
    source->frequencies().n(box::Expression(10));
    source->polarisation(box::Expression(22.5));
    auto* arraySensor = model.addArraySensor();
    arraySensor->colour(box::Constants::colourWhite);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(4, 4, 36));
    // Does the sensorId initialise correctly
    REQUIRE(arraySensor->nSamples() == 1199);
    REQUIRE(arraySensor->reflectedZ() == 0);
    REQUIRE(arraySensor->reflectedReferenceZ() == 36);
    REQUIRE(arraySensor->transmittedZ() == 33);
    // Run the model for 1199 steps
    model.start();
    size_t stepNum = 0;
    while(model.p()->timeStep() < 1199) {
        REQUIRE(model.p()->timeStep() == stepNum);
        stepNum++;
        model.step();
        model.step();
    }
    // Does the spectrum data look right?
    arraySensor->calculate();
    arraySensor->finalCalculate();
    size_t i = arraySensor->index(0, 0);
    REQUIRE(arraySensor->points()[i].transmitted().x().transmittance().size() == 10);
    for(size_t bin = 0;
        bin < arraySensor->points()[i].transmitted().x().transmittance().size();
        bin++) {
        INFO("Spectrum bin = " << bin);
        REQUIRE(arraySensor->points()[i].transmitted().x().transmittance()[bin] ==
                Approx(1.0).margin(0.05));
        REQUIRE(arraySensor->points()[i].transmitted().y().transmittance()[bin] ==
                Approx(1.0).margin(0.05));
    }
}

// Various array sizes
TEST_CASE("ArraySensor_ArraySizes") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(
            -0.0001, -0.0001, -0.0008,
            +0.0001, +0.0001, +0.0008,
            5e-5, 5e-5, 5e-5,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::absorbing,
            false);
    model.p()->boundarySize(0);
    model.p()->tSize(box::Expression(1.04 / 10e9));
    auto* source = model.addPlaneWaveSource();
    source->frequencies().first(box::Expression(10e9));
    source->frequencies().spacing(box::Expression(10e9));
    source->frequencies().n(box::Expression(10));
    auto* arraySensor = model.addArraySensor();
    arraySensor->colour(box::Constants::colourWhite);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(4, 4, 36));
    // Try a 4 x 1 (should see four points on the material view)
    arraySensor->numPointsX(4);
    arraySensor->numPointsY(1);
    model.initialise();
    REQUIRE(model.materialSensorRow(materialSensor, 0) ==
            std::vector<int>({7, 7, 7, 7}));
    REQUIRE(model.materialSensorRow(materialSensor, 33) ==
            std::vector<int>({7, 7, 7, 7}));
    // Try a 1 x 2 (should see no points on the material view)
    arraySensor->numPointsX(1);
    arraySensor->numPointsY(2);
    model.initialise();
    REQUIRE(model.materialSensorRow(materialSensor, 0) ==
            std::vector<int>({0, 0, 0, 0}));
    REQUIRE(model.materialSensorRow(materialSensor, 33) ==
            std::vector<int>({0, 0, 0, 0}));
}

// Conversion of configuration to and from XML
TEST_CASE("ArraySensor_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(
            -0.0004, -0.0004, -0.0002,
            +0.0004, +0.0004, +0.002,
            0.00005, 0.00005, 0.00005,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::periodic,
            fdtd::Configuration::Boundary::absorbing,
            false);
    auto* arraySensor = model.addArraySensor();
    // A test configuration
    arraySensor->nSamples(1);
    arraySensor->reflectedZPos(2.0);
    arraySensor->transmittedZPos(2.1);
    arraySensor->manualZPos(true);
    arraySensor->manualSamples(true);
    arraySensor->averageOverCell(true);
    arraySensor->numPointsX(3);
    arraySensor->numPointsY(4);
    arraySensor->frequency(7.0);
    arraySensor->animateFrequency(true);
    arraySensor->frequencyStep(8.0);
    arraySensor->componentSel({true, true, true});
    arraySensor->name("testing");
    arraySensor->colour(box::Constants::colourCyan);
    arraySensor->saveCsvData(true);
    // Write the XML
    auto* root = new xml::DomObject("sensorId");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<sensor::Sensor*>(arraySensor));
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Clear the configuration
    arraySensor->nSamples(0);
    arraySensor->reflectedZPos(0);
    arraySensor->transmittedZPos(0);
    arraySensor->manualZPos(false);
    arraySensor->manualSamples(false);
    arraySensor->averageOverCell(false);
    arraySensor->numPointsX(0);
    arraySensor->numPointsY(0);
    arraySensor->frequency(0.0);
    arraySensor->animateFrequency(false);
    arraySensor->frequencyStep(0.0);
    arraySensor->componentSel({false, false, false});
    arraySensor->name("");
    arraySensor->colour(box::Constants::colourBlack);
    arraySensor->saveCsvData(false);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("sensorId");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<sensor::Sensor*>(arraySensor));
    // Check the configuration
    REQUIRE(arraySensor->nSamples() == 1);
    REQUIRE(arraySensor->reflectedZPos() == 2.0_a);
    REQUIRE(arraySensor->transmittedZPos() == 2.1_a);
    REQUIRE(arraySensor->manualZPos());
    REQUIRE(arraySensor->manualSamples());
    REQUIRE(arraySensor->averageOverCell());
    REQUIRE(arraySensor->numPointsX() == 3);
    REQUIRE(arraySensor->numPointsY() == 4);
    REQUIRE(arraySensor->frequency() == 7.0_a);
    REQUIRE(arraySensor->animateFrequency());
    REQUIRE(arraySensor->frequencyStep() == 8.0_a);
    REQUIRE(arraySensor->componentSel().y());
    REQUIRE(arraySensor->componentSel().x());
    REQUIRE(arraySensor->componentSel().z());
    REQUIRE(arraySensor->name() == "testing");
    REQUIRE(arraySensor->colour() == box::Constants::colourCyan);
    REQUIRE(arraySensor->saveCsvData());
}

