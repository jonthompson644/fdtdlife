/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/SignalGenerator.h>
#include <Sensor/SpectrumPoint.h>
#include <Sensor/ReferencedComponent.h>
#include <complex>

using namespace Catch::literals;

// Operation of a referenced component calculation
TEST_CASE("ReferencedComponent_calculate") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator dataGen;
    box::SignalGenerator refGen;
    dataGen.initialise(0.01, {10, 10, 2},
                       0, 0.666666);
    refGen.initialise(0.01, {10, 10, 3},
                      0, 1.0);
    // Create spectra of data and a reference
    sensor::SpectrumPoint data;
    sensor::SpectrumPoint ref;
    data.initialise({5, 5, 10}, 1,
                    0.01, {true, true, true},
                    100, false);
    ref.initialise({5, 5, 10}, 1,
                   0.01, {true, true, true},
                   100, false);
    for(int i = 0; i < 100; i++) {
        data.collect(dataGen.sample(i, 0.0) / 2.0, 0.0, 0.0);
        ref.collect(refGen.sample(i, 0.0), 0.0, 0.0);
    }
    data.calculate();
    ref.calculate();
    // The referenced component to test
    sensor::ReferencedComponent component;
    component.initialise(10);
    component.calculate(data.spectrumX(), ref.spectrumX());
    component.evaluateRanges();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        INFO("i = " << i);
        if(i == 1 || i == 3) {
            REQUIRE(component.transmittance()[i] == Approx(0.25).margin(0.000001));
            REQUIRE(component.phaseShift()[i] == Approx(0.0).margin(0.000001));
        } else if(i == 5) {
            REQUIRE(component.transmittance()[i] == Approx(0.0).margin(0.000001));
            REQUIRE(component.phaseShift()[i] == Approx(127.0).margin(1.0));
        } else {
            REQUIRE(component.transmittance()[i] == Approx(0.0).margin(0.1));
            REQUIRE(component.phaseShift()[i] == Approx(0.0).margin(5.0));
        }
    }
    REQUIRE(component.transmittanceRange().min() == Approx(0.0).margin(0.000001));
    REQUIRE(component.transmittanceRange().max() == Approx(0.25).margin(0.000001));
    REQUIRE(component.phaseShiftRange().min() == Approx(-3.0).margin(1.0));
    REQUIRE(component.phaseShiftRange().max() == Approx(127.0).margin(1.0));
}

// Operation of a referenced component set
TEST_CASE("ReferencedComponent_set") {
    // The referenced component to test
    sensor::ReferencedComponent component;
    component.initialise(10);
    component.set(0, 0.1, 0.2, {0.3, 0.4});
    component.set(1, 0.5, 0.6, {0.7, 0.8});
    // Validate the results
    REQUIRE(component.transmittance()[0] == 0.1_a);
    REQUIRE(component.transmittance()[1] == 0.5_a);
    REQUIRE(component.phaseShift()[0] == 0.2_a);
    REQUIRE(component.phaseShift()[1] == 0.6_a);
    REQUIRE(component.admittance()[0].real() == 0.3_a);
    REQUIRE(component.admittance()[0].imag() == 0.4_a);
    REQUIRE(component.admittance()[1].real() == 0.7_a);
    REQUIRE(component.admittance()[1].imag() == 0.8_a);
}


