/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// A simple document with an empty root object
TEST_CASE("RadiationPattern_BasicOperation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0008, -5e-5, -0.0008,
                             +0.0008, +5e-5, +0.0008,
                             2.5e-5, 2.5e-5, 2.5e-5,
                             fdtd::Configuration::Boundary::absorbing,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    model.addZoneSource(0.0, 0.0, 0.0, 5e-5, 5e-5,
                        5e-5, 1 * box::Constants::tera_);
    model.addRadiationPatternSensor();
    model.radiationPatternSensor_->positiveSemicircle(true);
    model.radiationPatternSensor_->center(box::Vector<double>(0.0, 0.0, 0.0));
    model.radiationPatternSensor_->distance(0.0005);
    model.radiationPatternSensor_->numPoints(3);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(148, 4, 148));
    // Does the sensorId initialise correctly
    REQUIRE(model.radiationPatternSensor_->centerCell() == box::Vector<size_t>(74, 2, 74));
    REQUIRE(model.radiationPatternSensor_->points().size() == 3);
    REQUIRE(model.radiationPatternSensor_->points()[0].cellPosition() ==
            box::Vector<size_t>(94, 2, 74));
    REQUIRE(model.radiationPatternSensor_->points()[1].cellPosition() ==
            box::Vector<size_t>(74, 2, 94));
    REQUIRE(model.radiationPatternSensor_->points()[2].cellPosition() ==
            box::Vector<size_t>(54, 2, 74));
    // Does the sensor appear in the material sensorId?
    REQUIRE(materialSensor->data()[materialSensor->index(94, 74)] ==
            box::Constants::colourWhite);
    REQUIRE(materialSensor->data()[materialSensor->index(74, 94)] ==
            box::Constants::colourWhite);
    REQUIRE(materialSensor->data()[materialSensor->index(54, 74)] ==
            box::Constants::colourWhite);
    REQUIRE(materialSensor->data()[materialSensor->index(74, 74)] ==
            box::Constants::colourWhite);
    // Run the model for 130 steps
    model.start();
    while(model.p()->timeStep() < 130) {
        model.step();
    }
    // Check the radiation pattern sensorId results
    REQUIRE(model.radiationPatternSensor_->coPattern()[0] == Approx(0.02538).margin(0.00001));
    REQUIRE(model.radiationPatternSensor_->coPattern()[1] == Approx(0.26022).margin(0.00001));
    REQUIRE(model.radiationPatternSensor_->coPattern()[2] == Approx(0.02662).margin(0.00001));
}

// Conversion of configuration to and from XML
TEST_CASE("RadiationPattern_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    model.addRadiationPatternSensor();
    // A test configuration
    model.radiationPatternSensor_->positiveSemicircle(true);
    model.radiationPatternSensor_->center({1.0, 1.1, 1.2});
    model.radiationPatternSensor_->distance(1.3);
    model.radiationPatternSensor_->numPoints(2);
    model.radiationPatternSensor_->patternFrequency(2.1);
    model.radiationPatternSensor_->patternPolarisation(2.2);
    model.radiationPatternSensor_->name("testing");
    model.radiationPatternSensor_->colour(box::Constants::colourCyan);
    model.radiationPatternSensor_->saveCsvData(true);
    // Write the XML
    auto* root = new xml::DomObject("sensorId");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<sensor::Sensor*>(model.radiationPatternSensor_));
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Clear the configuration
    model.radiationPatternSensor_->positiveSemicircle(false);
    model.radiationPatternSensor_->center({0.0, 0.0, 0.0});
    model.radiationPatternSensor_->distance(0.0);
    model.radiationPatternSensor_->numPoints(0);
    model.radiationPatternSensor_->patternFrequency(0.0);
    model.radiationPatternSensor_->patternPolarisation(0.0);
    model.radiationPatternSensor_->name("");
    model.radiationPatternSensor_->colour(box::Constants::colourBlack);
    model.radiationPatternSensor_->saveCsvData(false);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("sensorId");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject()
                            >> *dynamic_cast<sensor::Sensor*>(model.radiationPatternSensor_));
    // Check the configuration
    REQUIRE(model.radiationPatternSensor_->positiveSemicircle());
    REQUIRE(model.radiationPatternSensor_->center().x() == 1.0_a);
    REQUIRE(model.radiationPatternSensor_->center().y() == 1.1_a);
    REQUIRE(model.radiationPatternSensor_->center().z() == 1.2_a);
    REQUIRE(model.radiationPatternSensor_->distance() == 1.3);
    REQUIRE(model.radiationPatternSensor_->numPoints() == 2);
    REQUIRE(model.radiationPatternSensor_->patternFrequency() == 2.1_a);
    REQUIRE(model.radiationPatternSensor_->patternPolarisation() == 2.2_a);
    REQUIRE(model.radiationPatternSensor_->name() == "testing");
    REQUIRE(model.radiationPatternSensor_->colour() == box::Constants::colourCyan);
    REQUIRE(model.radiationPatternSensor_->saveCsvData());
}

