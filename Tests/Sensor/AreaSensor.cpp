/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Box/Constants.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// Basic operation of an area sensorId
TEST_CASE("AreaSensor_BasicOperation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0001, -0.0001, -0.0008,
                             +0.0001, +0.0001, +0.0008,
                             2.5e-5, 2.5e-5, 2.5e-5,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    model.addPlaneWaveSource();
    model.addAreaSensor(0.0, 0.0, 0.0, 2.5e-5,
                        2.5e-5, box::Constants::orientationZPlane);
    auto* materialSensor = model.addMaterialSensor();
    materialSensor->orientation(box::Constants::orientationYPlane);
    materialSensor->offset(0.0);
    model.addSingleSequencer();
    model.initialise();
    REQUIRE(model.p()->geometry()->n() == box::Vector<size_t>(8, 8, 148));
    // Does the sensorId initialise correctly
    REQUIRE(model.areaSensor_->topLeftCell() == box::Vector<size_t>(4, 4, 74));
    REQUIRE(model.areaSensor_->bottomRightCell() == box::Vector<size_t>(5, 5, 75));
    REQUIRE(model.areaSensor_->frequencies().first() == 1e11_a);
    REQUIRE(model.areaSensor_->frequencies().spacing() == 1e11_a);
    REQUIRE(model.areaSensor_->frequencies().n() == 10);
    // Does the sensor appear in the material sensorId?
    REQUIRE(materialSensor->data()[materialSensor->index(4, 74)] ==
            box::Constants::colourWhite);
    REQUIRE(materialSensor->data()[materialSensor->index(5, 74)] ==
            box::Constants::colourBlack);
    REQUIRE(materialSensor->data()[materialSensor->index(3, 74)] ==
            box::Constants::colourBlack);
    REQUIRE(materialSensor->data()[materialSensor->index(4, 75)] ==
            box::Constants::colourBlack);
    // Run the model for 428 steps
    model.start();
    while(model.p()->timeStep() < 428) {
        model.step();
    }
    // Does the spectrum data look right?
    REQUIRE(model.areaSensor_->point().spectrumX().amplitude().size() == 10);
    REQUIRE(model.areaSensor_->point().spectrumY().amplitude().size() == 10);
    for(size_t bin = 0;
        bin < model.areaSensor_->point().spectrumX().amplitude().size();
        bin++) {
        INFO("Spectrum bin = " << bin);
        REQUIRE(model.areaSensor_->point().spectrumX().amplitude()[bin] ==
                Approx(0.1).margin(0.01));
        REQUIRE(model.areaSensor_->point().spectrumY().amplitude()[bin] ==
                Approx(0.0).margin(0.01));
    }
}

// Conversion of configuration to and from XML
TEST_CASE("AreaSensor_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    model.addAreaSensor(0.0, 0.0, 0.0, 2.5e-5, 2.5e-5,
                        box::Constants::orientationZPlane);
    // A test configuration
    model.areaSensor_->center({1.0, 1.1, 1.2});
    model.areaSensor_->orientation(box::Constants::orientationZPlane);
    model.areaSensor_->width(1.3);
    model.areaSensor_->height(1.4);
    model.areaSensor_->name("testing");
    model.areaSensor_->colour(box::Constants::colourCyan);
    model.areaSensor_->saveCsvData(true);
    // Write the XML
    auto* root = new xml::DomObject("sensorId");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<sensor::Sensor*>(model.areaSensor_));
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Clear the configuration
    model.areaSensor_->center({0.0, 0.0, 0.0});
    model.areaSensor_->orientation(box::Constants::orientationXPlane);
    model.areaSensor_->width(0.0);
    model.areaSensor_->height(0.0);
    model.areaSensor_->name("");
    model.areaSensor_->colour(box::Constants::colourBlack);
    model.areaSensor_->saveCsvData(false);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("sensorId");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<sensor::Sensor*>(model.areaSensor_));
    // Check the configuration
    REQUIRE(model.areaSensor_->center().x() == 1.0_a);
    REQUIRE(model.areaSensor_->center().y() == 1.1_a);
    REQUIRE(model.areaSensor_->center().z() == 1.2_a);
    REQUIRE(model.areaSensor_->orientation() == box::Constants::orientationZPlane);
    REQUIRE(model.areaSensor_->width() == 1.3_a);
    REQUIRE(model.areaSensor_->height() == 1.4_a);
    REQUIRE(model.areaSensor_->name() == "testing");
    REQUIRE(model.areaSensor_->colour() == box::Constants::colourCyan);
    REQUIRE(model.areaSensor_->saveCsvData());
}

