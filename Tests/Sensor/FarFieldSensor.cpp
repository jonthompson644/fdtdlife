/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Box/Constants.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// Conversion of configuration to and from XML
TEST_CASE("FarFieldSensor_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    model.addFarFieldSensor();
    // A test configuration
    model.farFieldSensor_->distance(1.3);
    model.farFieldSensor_->name("testing");
    model.farFieldSensor_->colour(box::Constants::colourCyan);
    model.farFieldSensor_->saveCsvData(true);
    // Write the XML
    auto* root = new xml::DomObject("sensorId");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<sensor::Sensor*>(model.farFieldSensor_));
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Clear the configuration
    model.farFieldSensor_->distance(0.0);
    model.farFieldSensor_->name("");
    model.farFieldSensor_->colour(box::Constants::colourBlack);
    model.farFieldSensor_->saveCsvData(false);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("sensorId");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >>
                                      *dynamic_cast<sensor::Sensor*>(model.farFieldSensor_));
    // Check the configuration
    REQUIRE(model.farFieldSensor_->distance() == 1.3_a);
    REQUIRE(model.farFieldSensor_->name() == "testing");
    REQUIRE(model.farFieldSensor_->colour() == box::Constants::colourCyan);
    REQUIRE(model.farFieldSensor_->saveCsvData());
}

