/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/SignalGenerator.h>
#include <Sensor/SpectrumPoint.h>
#include <Sensor/ReferencedSpectrum.h>
#include <Box/Constants.h>
#include <complex>

using namespace Catch::literals;

// Operation of a referenced spectra X component calculation
TEST_CASE("ReferencedSpectra_calculateX") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator dataGen;
    box::SignalGenerator refGen;
    dataGen.initialise(0.01, {10, 10, 2},
                       0, 0.666666);
    refGen.initialise(0.01, {10, 10, 3},
                      0, 1.0);
    // Create spectra of data and a reference
    sensor::SpectrumPoint data;
    sensor::SpectrumPoint ref;
    data.initialise({5, 5, 10}, 1,
                    0.01, {true, true, true},
                    100, false);
    ref.initialise({5, 5, 10}, 1,
                   0.01, {true, true, true},
                   100, false);
    for(int i = 0; i < 100; i++) {
        data.collect(dataGen.sample(i, 0.0) / 2.0, 0.0, 0.0);
        ref.collect(refGen.sample(i, 0.0), 0.0, 0.0);
    }
    data.calculate();
    ref.calculate();
    // The referenced spectrum to test
    sensor::ReferencedSpectrum spectrum;
    spectrum.initialise(10, {true, true, true});
    spectrum.calculate(data, ref);
    spectrum.evaluateRanges();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        INFO("i = " << i);
        if(i == 1 || i == 3) {
            REQUIRE(spectrum.x().transmittance()[i] == Approx(0.25).margin(0.000001));
            REQUIRE(spectrum.x().phaseShift()[i] == Approx(0.0).margin(0.000001));
        } else if(i == 5) {
            REQUIRE(spectrum.x().transmittance()[i] == Approx(0.0).margin(0.000001));
            REQUIRE(spectrum.x().phaseShift()[i] == Approx(127.0).margin(1.0));
        } else {
            REQUIRE(spectrum.x().transmittance()[i] == Approx(0.0).margin(0.1));
            REQUIRE(spectrum.x().phaseShift()[i] == Approx(0.0).margin(5.0));
        }
        REQUIRE(spectrum.y().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.y().phaseShift()[i] == 0.0_a);
        REQUIRE(spectrum.z().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.z().phaseShift()[i] == 0.0_a);
    }
    REQUIRE(spectrum.x().transmittanceRange().min() == Approx(0.0).margin(0.000001));
    REQUIRE(spectrum.x().transmittanceRange().max() == Approx(0.25).margin(0.000001));
    REQUIRE(spectrum.x().phaseShiftRange().min() == Approx(-3.0).margin(1.0));
    REQUIRE(spectrum.x().phaseShiftRange().max() == Approx(127.0).margin(1.0));
    REQUIRE(spectrum.y().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.y().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.y().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.y().phaseShiftRange().max() == 0.0_a);
    REQUIRE(spectrum.z().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.z().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.z().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.z().phaseShiftRange().max() == 0.0_a);
}

// Operation of a referenced spectra Y component calculation
TEST_CASE("ReferencedSpectra_calculateY") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator dataGen;
    box::SignalGenerator refGen;
    dataGen.initialise(0.01, {10, 10, 2},
                       0, 0.666666);
    refGen.initialise(0.01, {10, 10, 3},
                      0, 1.0);
    // Create spectra of data and a reference
    sensor::SpectrumPoint data;
    sensor::SpectrumPoint ref;
    data.initialise({5, 5, 10}, 1,
                    0.01, {true, true, true},
                    100, false);
    ref.initialise({5, 5, 10}, 1,
                   0.01, {true, true, true},
                   100, false);
    for(int i = 0; i < 100; i++) {
        data.collect(0.0, dataGen.sample(i, 0.0) / 2.0, 0.0);
        ref.collect(0.0, refGen.sample(i, 0.0), 0.0);
    }
    data.calculate();
    ref.calculate();
    // The referenced spectrum to test
    sensor::ReferencedSpectrum spectrum;
    spectrum.initialise(10, {true, true, true});
    spectrum.calculate(data, ref);
    spectrum.evaluateRanges();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        INFO("i = " << i);
        if(i == 1 || i == 3) {
            REQUIRE(spectrum.y().transmittance()[i] == Approx(0.25).margin(0.000001));
            REQUIRE(spectrum.y().phaseShift()[i] == Approx(0.0).margin(0.000001));
        } else if(i == 5) {
            REQUIRE(spectrum.y().transmittance()[i] == Approx(0.0).margin(0.000001));
            REQUIRE(spectrum.y().phaseShift()[i] == Approx(127.0).margin(1.0));
        } else {
            REQUIRE(spectrum.y().transmittance()[i] == Approx(0.0).margin(0.1));
            REQUIRE(spectrum.y().phaseShift()[i] == Approx(0.0).margin(5.0));
        }
        REQUIRE(spectrum.x().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.x().phaseShift()[i] == 0.0_a);
        REQUIRE(spectrum.z().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.z().phaseShift()[i] == 0.0_a);
    }
    REQUIRE(spectrum.y().transmittanceRange().min() == Approx(0.0).margin(0.000001));
    REQUIRE(spectrum.y().transmittanceRange().max() == Approx(0.25).margin(0.000001));
    REQUIRE(spectrum.y().phaseShiftRange().min() == Approx(-3.0).margin(1.0));
    REQUIRE(spectrum.y().phaseShiftRange().max() == Approx(127.0).margin(1.0));
    REQUIRE(spectrum.x().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.x().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.x().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.x().phaseShiftRange().max() == 0.0_a);
    REQUIRE(spectrum.z().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.z().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.z().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.z().phaseShiftRange().max() == 0.0_a);
}

// Operation of a referenced spectra Z component calculation
TEST_CASE("ReferencedSpectra_calculateZ") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator dataGen;
    box::SignalGenerator refGen;
    dataGen.initialise(0.01, {10, 10, 2},
                       0, 0.666666);
    refGen.initialise(0.01, {10, 10, 3},
                      0, 1.0);
    // Create spectra of data and a reference
    sensor::SpectrumPoint data;
    sensor::SpectrumPoint ref;
    data.initialise({5, 5, 10}, 1,
                    0.01, {true, true, true},
                    100, false);
    ref.initialise({5, 5, 10}, 1,
                   0.01, {true, true, true},
                   100, false);
    for(int i = 0; i < 100; i++) {
        data.collect(0.0, 0.0, dataGen.sample(i, 0.0) / 2.0);
        ref.collect(0.0, 0.0, refGen.sample(i, 0.0));
    }
    data.calculate();
    ref.calculate();
    // The referenced spectrum to test
    sensor::ReferencedSpectrum spectrum;
    spectrum.initialise(10, {true, true, true});
    spectrum.calculate(data, ref);
    spectrum.evaluateRanges();
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        INFO("i = " << i);
        if(i == 1 || i == 3) {
            REQUIRE(spectrum.z().transmittance()[i] == Approx(0.25).margin(0.000001));
            REQUIRE(spectrum.z().phaseShift()[i] == Approx(0.0).margin(0.000001));
        } else if(i == 5) {
            REQUIRE(spectrum.z().transmittance()[i] == Approx(0.0).margin(0.000001));
            REQUIRE(spectrum.z().phaseShift()[i] == Approx(127.0).margin(1.0));
        } else {
            REQUIRE(spectrum.z().transmittance()[i] == Approx(0.0).margin(0.1));
            REQUIRE(spectrum.z().phaseShift()[i] == Approx(0.0).margin(5.0));
        }
        REQUIRE(spectrum.y().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.y().phaseShift()[i] == 0.0_a);
        REQUIRE(spectrum.x().transmittance()[i] == 0.0_a);
        REQUIRE(spectrum.x().phaseShift()[i] == 0.0_a);
    }
    REQUIRE(spectrum.z().transmittanceRange().min() == Approx(0.0).margin(0.000001));
    REQUIRE(spectrum.z().transmittanceRange().max() == Approx(0.25).margin(0.000001));
    REQUIRE(spectrum.z().phaseShiftRange().min() == Approx(-3.0).margin(1.0));
    REQUIRE(spectrum.z().phaseShiftRange().max() == Approx(127.0).margin(1.0));
    REQUIRE(spectrum.y().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.y().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.y().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.y().phaseShiftRange().max() == 0.0_a);
    REQUIRE(spectrum.x().transmittanceRange().min() == 0.0_a);
    REQUIRE(spectrum.x().transmittanceRange().max() == 0.0_a);
    REQUIRE(spectrum.x().phaseShiftRange().min() == 0.0_a);
    REQUIRE(spectrum.x().phaseShiftRange().max() == 0.0_a);
}

