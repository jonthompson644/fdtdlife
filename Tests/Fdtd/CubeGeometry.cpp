/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Fdtd/Model.h>
#include <Fdtd/CubeGeometry.h>
#include "catch.hpp"

using namespace Catch::literals;

// Test the initialisation of the cubic geometry
TEST_CASE("CubeGeometry_Initialise") {
    fdtd::Model model;
    // Configure the model for hexagons
    model.p()->p1({-0.001, -0.001, -0.001});
    model.p()->p2({0.001, 0.001, 0.001});
    model.p()->boundarySize(4);
    model.p()->scatteredFieldZoneSize(2);
    const double dx = 0.0001;
    const double dy = 0.0001;
    const double dz = 0.0001;
    model.p()->dr({dx, dy, dz});
    // Run the initialisation
    model.p()->initialise();
    // Verify the geometry
    auto* g = dynamic_cast<fdtd::CubeGeometry*>(model.p()->geometry());
    REQUIRE(g != nullptr);
    REQUIRE(g->n() == box::Vector<size_t>(20, 20, 32));
    // Convert a point in the main zone to a cube coordinate
    REQUIRE(g->cellFloor({0.00019, 0.00011, 0.00009}) == box::Vector<size_t>(11, 11, 16));
    REQUIRE(g->cellFloor({-0.00013, -0.00007, -0.00005}) == box::Vector<size_t>(8, 9, 15));
    // Get the center coordinate of some cells
    box::Vector<double> c;
    c = g->cellCenter({0, 0, 6});
    REQUIRE(c.x() == -0.00095_a);
    REQUIRE(c.y() == -0.00095_a);
    REQUIRE(c.z() == -0.00095_a);
    c = g->cellCenter({1, 1, 7});
    REQUIRE(c.x() == -0.00085_a);
    REQUIRE(c.y() == -0.00085_a);
    REQUIRE(c.z() == -0.00085_a);
}
