/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Fdtd/Model.h>
#include <Fdtd/CubeGeometry.h>
#include <Box/Random.h>
#include <Fdtd/EquivCircuit.h>
#include "catch.hpp"

using namespace Catch::literals;

// Encoding and decoding of a chromosome
TEST_CASE("EquivCircuit_ChromosomeEncoding") {
    box::Random random;
    fdtd::EquivCircuit equiv(random, 2U);
    equiv.front().r_ = 1U;
    equiv.front().l_ = 2U;
    equiv.front().c_ = 4U;
    equiv.back().r_ = 8U;
    equiv.back().l_ = 16U;
    equiv.back().c_ = 32U;
    equiv.encode();
    REQUIRE(equiv.chromosomeBits() == 40);
    REQUIRE(equiv.chromosomeBitsUsed() == 4U);
    REQUIRE(equiv.chromosome().size() == 5U);
    REQUIRE(equiv.chromosome()[0] == 0x40U);
    REQUIRE(equiv.chromosome()[1] == 0x20U);
    REQUIRE(equiv.chromosome()[2] == 0x0CU);
    REQUIRE(equiv.chromosome()[3] == 0x44U);
    REQUIRE(equiv.chromosome()[4] == 0x01U);
    equiv.decode();
    REQUIRE(equiv.front().r_ == 1U);
    REQUIRE(equiv.front().l_ == 2U);
    REQUIRE(equiv.front().c_ == 4U);
    REQUIRE(equiv.back().r_ == 8U);
    REQUIRE(equiv.back().l_ == 16U);
    REQUIRE(equiv.back().c_ == 32U);
}

