/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>

using namespace Catch::literals;

// Conversion of configuration to and from XML
TEST_CASE("PlaneWaveSource_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic, fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing, fdtd::Configuration::absorbingCpml,
                             false);
    model.addPlaneWaveSource(0.0,0.0,0);
    // A test configuration
    model.planeWaveSource_->amplitude(1.1);
    model.planeWaveSource_->polarisation(1.2);
    model.planeWaveSource_->pmlSigma(1.3);
    model.planeWaveSource_->pmlSigmaFactor(1.4);
    model.planeWaveSource_->continuous(true);
    model.planeWaveSource_->time(1.5);
    model.planeWaveSource_->azimuth(1.6);
    model.planeWaveSource_->initialTime(1.7);
    model.planeWaveSource_->name("testing");
    model.planeWaveSource_->frequencies({2.3, 2.4, 3});
    model.planeWaveSource_->colour(box::Constants::colourCyan);
    // Write the XML
    xml::DomObject* root = new xml::DomObject("source");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<source::Source*>(model.planeWaveSource_));
    xml::Writer writer;
    std::string text = writer.write(&dom1);
    // Clear the configuration
    model.planeWaveSource_->amplitude(0.0);
    model.planeWaveSource_->polarisation(0.0);
    model.planeWaveSource_->pmlSigma(0.0);
    model.planeWaveSource_->pmlSigmaFactor(0.0);
    model.planeWaveSource_->continuous(false);
    model.planeWaveSource_->time(0.0);
    model.planeWaveSource_->azimuth(0.0);
    model.planeWaveSource_->initialTime(0.0);
    model.planeWaveSource_->name("");
    model.planeWaveSource_->frequencies({0.0, 0.0, 0});
    model.planeWaveSource_->colour(box::Constants::colourBlack);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("source");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<source::Source*>(model.planeWaveSource_));
    // Check the configuration
    REQUIRE(model.planeWaveSource_->amplitude() == 1.1_a);
    REQUIRE(model.planeWaveSource_->polarisation() == 1.2_a);
    REQUIRE(model.planeWaveSource_->pmlSigma() == 1.3_a);
    REQUIRE(model.planeWaveSource_->pmlSigmaFactor() == 1.4_a);
    REQUIRE(model.planeWaveSource_->continuous());
    REQUIRE(model.planeWaveSource_->time() == 1.5_a);
    REQUIRE(model.planeWaveSource_->azimuth() == 1.6_a);
    REQUIRE(model.planeWaveSource_->initialTime() == 1.7_a);
    REQUIRE(model.planeWaveSource_->name() == "testing");
    REQUIRE(model.planeWaveSource_->frequencies().first() == 2.3_a);
    REQUIRE(model.planeWaveSource_->frequencies().spacing() == 2.4_a);
    REQUIRE(model.planeWaveSource_->frequencies().n() == 3);
    REQUIRE(model.planeWaveSource_->colour() == box::Constants::colourCyan);
}

