/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>

using namespace Catch::literals;

// A simple document with an empty root object
TEST_CASE("ZoneSource_BasicOperation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic, fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing, fdtd::Configuration::absorbingCpml,
                             false);
    model.addZoneSource(0.0,0.0,0.0, 0.0002,0.0002,0.00005, 1 * box::Constants::tera_);
    model.addSingleSequencer();
    // Start the model
    REQUIRE(model.start());
    // Make an E step
    model.step();
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {
            INFO("Offset = " << i << "," << j << ",0");
            REQUIRE(model.e()->value(box::Vector<int>(6+i,6+j,45)).x() == 2.356042578_a);
        }
    }
}

// Conversion of configuration to and from XML
TEST_CASE("ZoneSource_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic, fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing, fdtd::Configuration::absorbingCpml,
                             false);
    model.addZoneSource(0.0,0.0,0.0, 0.0002,0.0002,0.00005, 1 * box::Constants::tera_);
    // A test configuration
    model.zoneSource_->amplitude(1.1);
    model.zoneSource_->azimuth(1.2);
    model.zoneSource_->elevation(1.3);
    model.zoneSource_->polarisation(1.4);
    model.zoneSource_->continuous(true);
    model.zoneSource_->time(1.5);
    model.zoneSource_->initialTime(1.6);
    model.zoneSource_->zoneCenter({1.7, 1.8, 1.9});
    model.zoneSource_->zoneSize({2.0, 2.1, 2.2});
    model.zoneSource_->name("testing");
    model.zoneSource_->frequencies({2.3, 2.4, 3});
    model.zoneSource_->colour(box::Constants::colourCyan);
    // Write the XML
    xml::DomObject* root = new xml::DomObject("source");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<source::Source*>(model.zoneSource_));
    xml::Writer writer;
    std::string text = writer.write(&dom1);
    // Clear the configuration
    model.zoneSource_->amplitude(0.0);
    model.zoneSource_->azimuth(0.0);
    model.zoneSource_->elevation(0.0);
    model.zoneSource_->polarisation(0.0);
    model.zoneSource_->continuous(false);
    model.zoneSource_->time(0.0);
    model.zoneSource_->initialTime(0.0);
    model.zoneSource_->zoneCenter({0.0, 0.0, 0.0});
    model.zoneSource_->zoneSize({0.0, 0.0, 0.0});
    model.zoneSource_->name("");
    model.zoneSource_->frequencies({0.0, 0.0, 0});
    model.zoneSource_->colour(box::Constants::colourBlack);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("source");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<source::Source*>(model.zoneSource_));
    // Check the configuration
    REQUIRE(model.zoneSource_->amplitude() == 1.1_a);
    REQUIRE(model.zoneSource_->azimuth() == 1.2_a);
    REQUIRE(model.zoneSource_->elevation() == 1.3_a);
    REQUIRE(model.zoneSource_->polarisation() == 1.4_a);
    REQUIRE(model.zoneSource_->continuous());
    REQUIRE(model.zoneSource_->time() == 1.5_a);
    REQUIRE(model.zoneSource_->initialTime() == 1.6_a);
    REQUIRE(model.zoneSource_->zoneCenter().x() == 1.7_a);
    REQUIRE(model.zoneSource_->zoneCenter().y() == 1.8_a);
    REQUIRE(model.zoneSource_->zoneCenter().z() == 1.9_a);
    REQUIRE(model.zoneSource_->zoneSize().x() == 2.0_a);
    REQUIRE(model.zoneSource_->zoneSize().y() == 2.1_a);
    REQUIRE(model.zoneSource_->zoneSize().z() == 2.2_a);
    REQUIRE(model.zoneSource_->name() == "testing");
    REQUIRE(model.zoneSource_->frequencies().first() == 2.3_a);
    REQUIRE(model.zoneSource_->frequencies().spacing() == 2.4_a);
    REQUIRE(model.zoneSource_->frequencies().n() == 3);
    REQUIRE(model.zoneSource_->colour() == box::Constants::colourCyan);
}

