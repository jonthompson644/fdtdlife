/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>

using namespace Catch::literals;

// Conversion of configuration to and from XML
TEST_CASE("GravyWaveSource_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.0004, -0.0004, -0.0002,
                             +0.0004, +0.0004, +0.002,
                             0.00005, 0.00005, 0.00005,
                             fdtd::Configuration::Boundary::periodic, fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing, fdtd::Configuration::absorbingCpml,
                             false);
    model.addGravyWaveSource();
    // A test configuration
    model.gravyWaveSource_->peakAmplitude(1.1);
    model.gravyWaveSource_->timeOfMaximum(1.2);
    model.gravyWaveSource_->widthAtHalfHeight(1.3);
    model.gravyWaveSource_->azimuth(1.4);
    model.gravyWaveSource_->elevation(1.5);
    model.gravyWaveSource_->polarisation(1.6);
    model.gravyWaveSource_->magneticField(1.7);
    model.gravyWaveSource_->zoneCenter({1.8, 1.9, 2.0});
    model.gravyWaveSource_->zoneSize({2.1, 2.2, 2.3});
    model.gravyWaveSource_->name("testing");
    model.gravyWaveSource_->frequencies({2.4, 2.5, 3});
    model.gravyWaveSource_->colour(box::Constants::colourCyan);
    // Write the XML
    xml::DomObject* root = new xml::DomObject("source");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<source::Source*>(model.gravyWaveSource_));
    xml::Writer writer;
    std::string text = writer.write(&dom1);
    // Clear the configuration
    model.gravyWaveSource_->peakAmplitude(0.0);
    model.gravyWaveSource_->timeOfMaximum(0.0);
    model.gravyWaveSource_->widthAtHalfHeight(0.0);
    model.gravyWaveSource_->azimuth(0.0);
    model.gravyWaveSource_->elevation(0.0);
    model.gravyWaveSource_->polarisation(0.0);
    model.gravyWaveSource_->magneticField(0.0);
    model.gravyWaveSource_->zoneCenter({0.0, 0.0, 0.0});
    model.gravyWaveSource_->zoneSize({0.0, 0.0, 0.0});
    model.gravyWaveSource_->name("");
    model.gravyWaveSource_->frequencies({0.0, 0.0, 0});
    model.gravyWaveSource_->colour(box::Constants::colourBlack);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("source");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<source::Source*>(model.gravyWaveSource_));
    // Check the configuration
    REQUIRE(model.gravyWaveSource_->peakAmplitude() == 1.1_a);
    REQUIRE(model.gravyWaveSource_->timeOfMaximum() == 1.2_a);
    REQUIRE(model.gravyWaveSource_->widthAtHalfHeight() == 1.3_a);
    REQUIRE(model.gravyWaveSource_->azimuth() == 1.4_a);
    REQUIRE(model.gravyWaveSource_->elevation() == 1.5_a);
    REQUIRE(model.gravyWaveSource_->polarisation() == 1.6_a);
    REQUIRE(model.gravyWaveSource_->magneticField() == 1.7_a);
    REQUIRE(model.gravyWaveSource_->zoneCenter().x() == 1.8_a);
    REQUIRE(model.gravyWaveSource_->zoneCenter().y() == 1.9_a);
    REQUIRE(model.gravyWaveSource_->zoneCenter().z() == 2.0_a);
    REQUIRE(model.gravyWaveSource_->zoneSize().x() == 2.1_a);
    REQUIRE(model.gravyWaveSource_->zoneSize().y() == 2.2_a);
    REQUIRE(model.gravyWaveSource_->zoneSize().z() == 2.3_a);
    REQUIRE(model.gravyWaveSource_->name() == "testing");
    REQUIRE(model.gravyWaveSource_->frequencies().first() == 2.4_a);
    REQUIRE(model.gravyWaveSource_->frequencies().spacing() == 2.5_a);
    REQUIRE(model.gravyWaveSource_->frequencies().n() == 3);
    REQUIRE(model.gravyWaveSource_->colour() == box::Constants::colourCyan);
}

