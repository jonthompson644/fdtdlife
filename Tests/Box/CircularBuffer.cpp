/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/CircularBuffer.h>

// Operation of the box::CircularBuffer template
TEST_CASE("CircularBuffer_BasicOperation") {
    int value, total;
    // The buffer
    box::CircularBuffer<int> buffer;
    buffer.resize(4);
    // Is it empty?
    REQUIRE(buffer.empty());
    // Can we iterate?
    value = 0;
    total = 0;
    for(auto& d : buffer) {
        value++;
        total += d;
    }
    REQUIRE(value == 0);
    // Put something in the buffer
    buffer.push(0);
    REQUIRE_FALSE(buffer.empty());
    REQUIRE(buffer.size() == 1);
    // Can we iterate?
    value = 0;
    for(auto& d : buffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 1);
    // Put something else in the buffer
    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    REQUIRE(buffer.size() == 4);
    value = 0;
    for(auto& d : buffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 4);
    // Another thing in the buffer should cause the first one to drop
    buffer.push(4);
    REQUIRE(buffer.size() == 4);
    value = 1;
    for(auto& d : buffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 5);
    // Another thing in the buffer should cause the first one to drop
    buffer.push(5);
    REQUIRE(buffer.size() == 4);
    value = 2;
    for(auto& d : buffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 6);
    // Change an entry
    buffer[1] = 6;
    REQUIRE(buffer[0] == 2);
    REQUIRE(buffer[1] == 6);
    REQUIRE(buffer[2] == 4);
    REQUIRE(buffer[3] == 5);
    // Pop two from the buffer
    REQUIRE(buffer.pop() == 2);
    REQUIRE(buffer.pop() == 6);
    REQUIRE(buffer[0] == 4);
    REQUIRE(buffer[1] == 5);
    value = 4;
    for(auto& d : buffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 6);
}

// Operation of a constant box::CircularBuffer template
TEST_CASE("CircularBuffer_ConstOperation") {
    // The buffer
    box::CircularBuffer<int> buffer;
    buffer.resize(4);
    buffer.push(0);
    buffer.push(1);
    buffer.push(2);
    buffer.push(3);
    REQUIRE(buffer.size() == 4);
    // Make a constant reference to it
    const box::CircularBuffer<int>& constBuffer = buffer;
    // Contents
    REQUIRE(constBuffer[0] == 0);
    REQUIRE(constBuffer[1] == 1);
    REQUIRE(constBuffer[2] == 2);
    REQUIRE(constBuffer[3] == 3);
    // Can we iterate over this?
    int value = 0;
    for(auto d : constBuffer) {
        REQUIRE(d == value);
        value++;
    }
    REQUIRE(value == 4);
}
