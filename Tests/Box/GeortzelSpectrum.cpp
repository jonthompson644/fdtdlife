/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/SignalGenerator.h>
#include <Box/GeortzelSpectrum.h>
#include <Box/CircularBuffer.h>
#include <complex>

using namespace Catch::literals;

// Operation of the geortzel algorithm without the window
TEST_CASE("GeortzelSpectrum_NoWindow") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator gen;
    gen.initialise(0.01, {10, 10, 3},
                   0, 1.0);
    // Make a time series for 1s
    box::CircularBuffer<double> timeSeries;
    timeSeries.resize(100);
    for(int i = 0; i < 100; i++) {
        timeSeries.push(gen.sample(i, 0.0));
    }
    // Convert it to a spectrum
    box::GeortzelSpectrum spectrum;
    spectrum.initialise({5, 5, 10},
                        1, 0.01, false,
                        100);
    spectrum.calculate(timeSeries);
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        if(i == 1) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 3) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 3.0).margin(0.00000001));
        } else if(i == 5) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 3.0).margin(0.00000001));
        } else {
            REQUIRE(std::abs(spectrum[i]) == Approx(0.0).margin(0.00000001));
        }
    }
    REQUIRE_FALSE(spectrum.empty());
    REQUIRE(spectrum.frequencies() == box::FrequencySeq(5, 5, 10));
    REQUIRE(spectrum.samplePeriod() == 0.01_a);
    REQUIRE(spectrum.begin() == spectrum.spectrum().begin());
    REQUIRE(spectrum.end() == spectrum.spectrum().end());
    // Validate the ranges
    box::Range<double> spectrumRange;
    spectrumRange.evaluate(spectrum.spectrum());
    box::Range<double> amplitudeRange;
    amplitudeRange.evaluate(spectrum.amplitude());
    box::Range<double> phaseRange;
    phaseRange.evaluate(spectrum.phase());
    REQUIRE(spectrumRange.min() == Approx(-0.317018).margin(0.000001));
    REQUIRE(spectrumRange.max() == Approx(0.103005).margin(0.000001));
    REQUIRE(amplitudeRange.min() == Approx(0.000000).margin(0.000001));
    REQUIRE(amplitudeRange.max() == Approx(0.333333).margin(0.000001));
    REQUIRE(phaseRange.min() == Approx(-2.827433).margin(0.000001));
    REQUIRE(phaseRange.max() == Approx(2.827433).margin(0.000001));
}

// Operation of the geortzel algorithm with the window
TEST_CASE("GeortzelSpectrum_WithWindow") {
    // Create a signal generator with 10, 20 and 30Hz signals.
    box::SignalGenerator gen;
    gen.initialise(0.01, {10, 10, 3},
                   0, 1.0);
    // Make a time series for 1s
    box::CircularBuffer<double> timeSeries;
    timeSeries.resize(100);
    for(int i = 0; i < 100; i++) {
        timeSeries.push(gen.sample(i, 0.0));
    }
    // Convert it to a spectrum
    box::GeortzelSpectrum spectrum;
    spectrum.initialise({5, 5, 10},
                        1, 0.01, true,
                        100);
    spectrum.calculate(timeSeries);
    // Validate the results
    for(size_t i = 0; i < 10; i++) {
        if(i == 1) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 6.0).margin(0.00000001));
        } else if(i == 3) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 6.0).margin(0.00000001));
        } else if(i == 5) {
            REQUIRE(std::abs(spectrum[i]) == Approx(1.0 / 6.0).margin(0.00000001));
        } else {
            REQUIRE(std::abs(spectrum[i]) == Approx(0.0).margin(0.00000001));
        }
    }
}
