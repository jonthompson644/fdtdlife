/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/SignalGenerator.h>
#include <Box/Constants.h>

using namespace Catch::literals;

// Operation of the signal generator with one frequency
TEST_CASE("SignalGenerator_BasicOperation") {
    double val;
    // The generator
    box::SignalGenerator gen;
    // Set it up
    gen.initialise(0.01, {10, 1, 1},
            0, 1.0);
    REQUIRE(gen.samplePeriod() == 0.01_a);
    REQUIRE(gen.frequencies().first() == 10.0_a);
    REQUIRE(gen.frequencies().spacing() == 1.0_a);
    REQUIRE(gen.frequencies().n() == 1);
    REQUIRE(gen.initialTime() == 0.0_a);
    REQUIRE(gen.amplitude() == 1.0_a);
    // Take some samples
    for(int i = 0; i < 10; i++) {
        val = std::sin(2.0 * box::Constants::pi_ * 10 * 0.01 * i);
        INFO("i=" << i);
        REQUIRE(gen.sample(i, 0.0) == Approx(val));
    }
}

// Operation of the signal generator with 2 frequencies
TEST_CASE("SignalGenerator_TwoFrequencies") {
    double val;
    // The generator
    box::SignalGenerator gen;
    // Set it up
    gen.initialise(0.01, {10, 10, 2},
            0, 1.0);
    REQUIRE(gen.samplePeriod() == 0.01_a);
    REQUIRE(gen.frequencies().first() == 10.0_a);
    REQUIRE(gen.frequencies().spacing() == 10.0_a);
    REQUIRE(gen.frequencies().n() == 2);
    REQUIRE(gen.initialTime() == 0.0_a);
    REQUIRE(gen.amplitude() == 1.0_a);
    // Take some samples
    for(int i = 0; i < 20; i++) {
        val = std::sin(2.0 * box::Constants::pi_ * 10 * 0.01 * i) +
              std::sin(2.0 * box::Constants::pi_ * 20 * 0.01 * i);
        INFO("i=" << i);
        REQUIRE(gen.sample(i, 0.0) == Approx(val / 2.0));
    }
}
