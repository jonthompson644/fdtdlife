/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/Expression.h>
#include <sstream>
using namespace Catch::literals;

// Parsing of constants
TEST_CASE("Expression_Constants") {
    box::Expression e;
    box::Expression::Context c;
    // Simple integer
    e.text("1234");
    REQUIRE(e.evaluate(c) == 1234.0_a);
    // Ending with decimal point
    e.text("1234.");
    REQUIRE(e.evaluate(c) == 1234._a);
    // Fractional number
    e.text("1234.567");
    REQUIRE(e.evaluate(c) == 1234.567_a);
    // Exponent
    e.text("1234.567e");
    REQUIRE(e.evaluate(c) == 1234.567_a);
    e.text("1234.567e1");
    REQUIRE(e.evaluate(c) == 12345.67_a);
    e.text("1234.567e+1");
    REQUIRE(e.evaluate(c) == 12345.67_a);
    e.text("1234.567e-1");
    REQUIRE(e.evaluate(c) == 123.4567_a);
}

// Monadic operators
TEST_CASE("Expression_MonadicOperators") {
    box::Expression e;
    box::Expression::Context c;
    e.text("+ 1234");
    REQUIRE(e.evaluate(c) == 1234.0_a);
    e.text("- 1234");
    REQUIRE(e.evaluate(c) == -1234.0_a);
}

// Global parameters
TEST_CASE("Expression_Parameters") {
    box::Expression e;
    box::Expression::Context c;
    // Parameter
    e.text("x");
    c.add({box::Expression::Parameter("x", 1234.0)});
    REQUIRE(e.evaluate(c) == 1234.0_a);
    c.clear();
    // Redefined parameter
    e.text("x");
    c.add({box::Expression::Parameter("x", 4567.0)});
    c.add({box::Expression::Parameter("x", 1234.0)});
    REQUIRE(e.evaluate(c) == 4567.0_a);
    c.clear();
}

// Functions
TEST_CASE("Expression_Functions") {
    box::Expression e;
    box::Expression::Context c;
    // Cosine
    e.text("cos(1.04719755)");
    REQUIRE(e.evaluate(c) == 0.5_a);
    // Sine
    e.text("sin(0.52359878)");
    REQUIRE(e.evaluate(c) == 0.5_a);
    // Tan
    e.text("tan(0.46364761)");
    REQUIRE(e.evaluate(c) == 0.5_a);
    // Square root
    e.text("sqrt(4.0)");
    REQUIRE(e.evaluate(c) == 2.0_a);
    // Min
    e.text("min(7.0, 4.0)");
    REQUIRE(e.evaluate(c) == 4.0_a);
    // Max
    e.text("max(7.0, 4.0)");
    REQUIRE(e.evaluate(c) == 7.0_a);
    // Exponential
    e.text("exp(2)");
    REQUIRE(e.evaluate(c) == Approx(exp(2)));
    // Rounding
    e.text("round(2.3)");
    REQUIRE(e.evaluate(c) == 2.0_a);
}

// Power operator
TEST_CASE("Expression_Power") {
    box::Expression e;
    box::Expression::Context c;
    // Squaring
    e.text("4^2");
    REQUIRE(e.evaluate(c) == 16.0_a);
    // Negative power
    e.text("4^-2");
    REQUIRE(e.evaluate(c) == 0.0625_a);
    // Squaring with a monadic operator
    e.text("-4^2");
    REQUIRE(e.evaluate(c) == -16.0_a);
}

// Swap operation
TEST_CASE("Expression_Swap") {
    box::Expression e1("42.3");
    box::Expression e2("59.6");
    box::Expression::Context c;
    e1.evaluate(c);
    e2.evaluate(c);
    box::Expression::swap(e1, e2);
    REQUIRE(e1.text() == "59.6");
    REQUIRE(e1.value() == 59.6_a);
    REQUIRE(e2.text() == "42.3");
    REQUIRE(e2.value() == 42.3_a);
}

// Multiply and divide operators
TEST_CASE("Expression_MultiplyDivide") {
    box::Expression e;
    box::Expression::Context c;
    // Multiply
    e.text("2 * 4");
    REQUIRE(e.evaluate(c) == 8.0_a);
    // Divide
    e.text("2 / 4");
    REQUIRE(e.evaluate(c) == 0.5_a);
}

// Add and subtract operators
TEST_CASE("Expression_AddSubtract") {
    box::Expression e;
    box::Expression::Context c;
    // Add
    e.text("2 + 4");
    REQUIRE(e.evaluate(c) == 6.0_a);
    // Subtract
    e.text("2 - 4");
    REQUIRE(e.evaluate(c) == -2.0_a);
    // Three terms
    e.text("4 - 2 + 3");
    REQUIRE(e.evaluate(c) == 5.0_a);
}

// Operator priority
TEST_CASE("Expression_OperatorPriority") {
    box::Expression e;
    box::Expression::Context c;
    e.text("2 + 4 * 2");
    REQUIRE(e.evaluate(c) == 10.0_a);
    e.text("2 * 3^2");
    REQUIRE(e.evaluate(c) == 18.0_a);
    e.text("(2 + 4) * 2");
    REQUIRE(e.evaluate(c) == 12.0_a);
}

// Constructors
TEST_CASE("Expression_Constructors") {
    box::Expression e1("31.0");
    box::Expression e2(31.0);
    box::Expression::Context c;
    e1.evaluate(c);
    e1.evaluate(c);
    REQUIRE(e1.value() == 31.0_a);
    REQUIRE(e1.text() == "31.0");
    REQUIRE(e2.value() == 31.0_a);
    REQUIRE(e2.text() == "31");
}

// Context parameters
TEST_CASE("Expression_ContextParameters") {
    box::Expression e;
    box::Expression::Context c;
    c.add({box::Expression::Parameter("two", 2),
           box::Expression::Parameter("three", 3)});
    e.text("e * two");
    e.evaluate(c);
    REQUIRE(e.value() == Approx(2.0 * 2.71828183));
    e.text("pi * two");
    e.evaluate(c);
    REQUIRE(e.value() == Approx(2.0 * 3.14159265));
    e.text("epsilon0 * two");
    e.evaluate(c);
    REQUIRE(e.value() == Approx(2.0 * 8.85418782e-12));
    e.text("mu0 * two");
    e.evaluate(c);
    REQUIRE(e.value() == Approx(2.0 * 1.25663706e-6));
    e.text("c * three");
    e.evaluate(c);
    REQUIRE(e.value() == Approx(3.0 * 299792458.0));
}

// Exceptions
TEST_CASE("Expression_Exceptions") {
    box::Expression e;
    box::Expression::Context c;
    e.text("1 + 2 five");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("sqrt(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("sin(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("cos(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("tan(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("exp(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("round(4,5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("min(5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("max(5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("unknown(5)");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("one");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("max(5,3");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text("(1");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
    e.text(")");
    REQUIRE_THROWS_AS(e.evaluate(c), box::Expression::Exception);
}


