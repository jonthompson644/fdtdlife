/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/Line2D.h>
#include <sstream>
#include <cmath>

using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("Line2D_Constructors") {
    // Default constructor
    box::Line2D<double> p1;
    REQUIRE(p1.a().x() == 0.0_a);
    REQUIRE(p1.a().y() == 0.0_a);
    REQUIRE(p1.b().x() == 0.0_a);
    REQUIRE(p1.b().y() == 0.0_a);
    // Initialising constructor
    box::Line2D<double> p2({2,1}, {3,2});
    REQUIRE(p2.a().x() == 2.0_a);
    REQUIRE(p2.a().y() == 1.0_a);
    REQUIRE(p2.b().x() == 3.0_a);
    REQUIRE(p2.b().y() == 2.0_a);
    // Copy constructor
    box::Line2D<double> p3(p2);
    REQUIRE(p3.a().x() == 2.0_a);
    REQUIRE(p3.a().y() == 1.0_a);
    REQUIRE(p3.b().x() == 3.0_a);
    REQUIRE(p3.b().y() == 2.0_a);
}

// Operation of the operators
TEST_CASE("Line2D_Operators") {
    // Assignment
    box::Line2D<double> p1({2,1}, {3,2});
    box::Line2D<double> p2({5,4}, {7,6});
    REQUIRE(p1.a().x() == 2.0_a);
    REQUIRE(p1.a().y() == 1.0_a);
    REQUIRE(p1.b().x() == 3.0_a);
    REQUIRE(p1.b().y() == 2.0_a);
    REQUIRE(p2.a().x() == 5.0_a);
    REQUIRE(p2.a().y() == 4.0_a);
    REQUIRE(p2.b().x() == 7.0_a);
    REQUIRE(p2.b().y() == 6.0_a);
    p1 = p2;
    REQUIRE(p1.a().x() == 5.0_a);
    REQUIRE(p1.a().y() == 4.0_a);
    REQUIRE(p1.b().x() == 7.0_a);
    REQUIRE(p1.b().y() == 6.0_a);
}

// Operation of setters
TEST_CASE("Line2D_Setters") {
    box::Line2D<double> p1({2,1}, {3,2});
    REQUIRE(p1.a().x() == 2.0_a);
    REQUIRE(p1.a().y() == 1.0_a);
    REQUIRE(p1.b().x() == 3.0_a);
    REQUIRE(p1.b().y() == 2.0_a);
    p1.a({6,5});
    REQUIRE(p1.a().x() == 6.0_a);
    REQUIRE(p1.a().y() == 5.0_a);
    REQUIRE(p1.b().x() == 3.0_a);
    REQUIRE(p1.b().y() == 2.0_a);
    p1.b({8,7});
    REQUIRE(p1.a().x() == 6.0_a);
    REQUIRE(p1.a().y() == 5.0_a);
    REQUIRE(p1.b().x() == 8.0_a);
    REQUIRE(p1.b().y() == 7.0_a);
}

// Equation of the line calculations
TEST_CASE("Line2D_EquationOfLine") {
    // Normal case
    box::Line2D<double> l1({2,2}, {4,3});
    REQUIRE_FALSE(l1.isVertical());
    REQUIRE(l1.m() == 0.5_a);
    REQUIRE(l1.c() == 1.0_a);
    // Horizontal line
    box::Line2D<double> l2({2,2}, {4,2});
    REQUIRE_FALSE(l2.isVertical());
    REQUIRE(l2.m() == 0.0_a);
    REQUIRE(l2.c() == 2.0_a);
    // Vertical line
    box::Line2D<double> l3({1,1}, {1,3});
    REQUIRE(l3.isVertical());
    REQUIRE(isinf(l3.m()));
    REQUIRE(isinf(l3.c()));
}

// Point inside rectangle calculation
TEST_CASE("Line2D_isIn") {
    box::Line2D<double> l1({1,0}, {4,2});
    REQUIRE(l1.isIn({1,0}));
    REQUIRE(l1.isIn({1.1,0}));
    REQUIRE_FALSE(l1.isIn({0.9,0}));
    REQUIRE(l1.isIn({4,0}));
    REQUIRE(l1.isIn({3.9,0}));
    REQUIRE_FALSE(l1.isIn({4.1,0}));
    REQUIRE_FALSE(l1.isIn({1,-0.1}));
    REQUIRE_FALSE(l1.isIn({1,2.1}));
    REQUIRE(l1.isIn({1,2}));
    REQUIRE(l1.isIn({1,1.9}));
}

// Intersection calculations
TEST_CASE("Line2D_Intersection") {
    // Easy case, intersection in the middle
    box::Line2D<double> l1({0,2}, {4,4});
    box::Line2D<double> l2({0,4}, {4,2});
    box::Point2D<double> p1;
    REQUIRE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Intersection beyond the first line
    l1 = box::Line2D<double>({0,2}, {1,2.5});
    l2 = box::Line2D<double>({0,4}, {4,2});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Intersection beyond the second line
    l1 = box::Line2D<double>({0,2}, {4,4});
    l2 = box::Line2D<double>({0,4}, {1,3.5});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Intersection before the first line
    l1 = box::Line2D<double>({3,3.5}, {4,4});
    l2 = box::Line2D<double>({0,4}, {4,2});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Intersection before the second line
    l1 = box::Line2D<double>({0,2}, {4,4});
    l2 = box::Line2D<double>({3,2.5}, {4,2});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // First line vertical
    l1 = box::Line2D<double>({2,0}, {2,4});
    l2 = box::Line2D<double>({0,4}, {4,2});
    REQUIRE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Second line vertical
    l1 = box::Line2D<double>({0,2}, {4,4});
    l2 = box::Line2D<double>({2,4}, {2,2});
    REQUIRE(l1.intersection(l2, p1));
    REQUIRE(p1.x() == 2.0_a);
    REQUIRE(p1.y() == 3.0_a);
    // Both lines vertical
    l1 = box::Line2D<double>({2.1,0}, {2.1,4});
    l2 = box::Line2D<double>({2,4}, {2,2});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    // Both lines with the same gradient
    l1 = box::Line2D<double>({0,1}, {4,3});
    l2 = box::Line2D<double>({0,2}, {4,4});
    REQUIRE_FALSE(l1.intersection(l2, p1));
    // List of points
    std::vector<box::Point2D<double>> points;
    l1 = {{0,2}, {4,4}};
    std::vector<box::Line2D<double>> lines = {{{0,4}, {4,2}}};
    l1.intersections(lines, points);
    REQUIRE(points.size() == 1);
    REQUIRE(points.front().x() == 2.0_a);
    REQUIRE(points.front().y() == 3.0_a);
}
