/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/BinaryPattern.h>
#include <sstream>

using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("BinaryPattern_TheConstructors") {
    // Default constructor
    box::BinaryPattern p1;
    REQUIRE(p1.n() == 0);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({}));
    // Initialising constructor
    box::BinaryPattern p2(5);
    REQUIRE(p2.n() == 5);
    REQUIRE(p2.pattern() == std::vector<uint64_t>({0ULL}));
    // Initialising constructor
    box::BinaryPattern p3(5, 42);
    REQUIRE(p3.n() == 5);
    REQUIRE(p3.pattern() == std::vector<uint64_t>({42ULL}));
    // Copy constructor
    box::BinaryPattern p4(p3);
    REQUIRE(p4.n() == 5);
    REQUIRE(p4.pattern() == std::vector<uint64_t>({42ULL}));
    // Initialising constructor
    box::BinaryPattern p5(5, std::vector<uint64_t>({42ULL}));
    REQUIRE(p5.n() == 5);
    REQUIRE(p5.pattern() == std::vector<uint64_t>({42ULL}));
}

// Operation of the API
TEST_CASE("BinaryPattern_Api") {
    std::stringstream s1;
    // One bit
    box::BinaryPattern p1(5);
    p1.set(0,0,true);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({1ULL}));
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == "*....\n.....\n.....\n.....\n.....\n");
    REQUIRE(p1.get(0,0));
    REQUIRE_FALSE(p1.get(1,0));
    REQUIRE_FALSE(p1.get(1,2));
    // A second bit
    p1.set(1,0,true);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({3ULL}));
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == "**...\n.....\n.....\n.....\n.....\n");
    REQUIRE(p1.get(0,0));
    REQUIRE(p1.get(1,0));
    REQUIRE_FALSE(p1.get(1,2));
    // A third bit
    p1.set(1,2,true);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({0x803ULL}));
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == "**...\n.....\n.*...\n.....\n.....\n");
    REQUIRE(p1.get(0,0));
    REQUIRE(p1.get(1,0));
    REQUIRE(p1.get(1,2));
    // Clear the first bit
    p1.set(0,0,false);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({0x802ULL}));
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".*...\n.....\n.*...\n.....\n.....\n");
    REQUIRE_FALSE(p1.get(0,0));
    REQUIRE(p1.get(1,0));
    REQUIRE(p1.get(1,2));
    // Initialise the pattern
    p1.initialise(5, {42});
    REQUIRE(p1.n() == 5);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({42ULL}));
    // Corner to corner
    p1.initialise(5, {0x41ULL});
    REQUIRE(p1.hasCornerToCorner());
    p1.initialise(5, {0x11ULL});
    REQUIRE_FALSE(p1.hasCornerToCorner());
    // Hexadecimal
    p1.initialise(18, {0x41ULL, 0x32ULL});
    REQUIRE(p1.hexadecimal() == "41 32");
}

// Operators
TEST_CASE("BinaryPattern_Operators") {
    // Assignment
    box::BinaryPattern p1(5, 0x803);
    box::BinaryPattern p2;
    REQUIRE(p1 != p2);
    REQUIRE(p1.n() == 5);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({0x803ULL}));
    REQUIRE(p2.n() == 0);
    REQUIRE(p2.pattern() == std::vector<uint64_t>({}));
    p2 = p1;
    REQUIRE(p1.n() == 5);
    REQUIRE(p1.pattern() == std::vector<uint64_t>({0x803ULL}));
    REQUIRE(p2.n() == 5);
    REQUIRE(p2.pattern() == std::vector<uint64_t>({0x803ULL}));
    REQUIRE(p1 == p2);
}

// Translations
TEST_CASE("BinaryPattern_Translations") {
    std::stringstream s1;
    box::BinaryPattern p1(5);
    // Translate X
    p1.clear();
    p1.set(1, 4, true);
    p1.set(2, 4, true);
    p1.set(3, 4, true);
    p1.set(4, 4, true);
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".....\n.....\n.....\n.....\n.****\n");
    p1.translateX();
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".....\n.....\n.....\n.....\n****.\n");
    // Translate Y
    p1.clear();
    p1.set(4, 1, true);
    p1.set(4, 2, true);
    p1.set(4, 3, true);
    p1.set(4, 4, true);
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".....\n....*\n....*\n....*\n....*\n");
    p1.translateY();
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == "....*\n....*\n....*\n....*\n.....\n");
    // Rotate 90
    p1.clear();
    p1.set(4, 1, true);
    p1.set(4, 2, true);
    p1.set(4, 3, true);
    p1.set(4, 4, true);
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".....\n....*\n....*\n....*\n....*\n");
    p1.rotate90();
    s1.str(""); s1.clear(); s1 << p1;
    REQUIRE(s1.str() == ".....\n.....\n.....\n.....\n.****\n");
}

