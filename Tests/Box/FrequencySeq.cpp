/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/FrequencySeq.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>

using namespace Catch::literals;

// Operation of the frequency sequence object
TEST_CASE("FrequencySeq_BasicOperation") {
    // Default constructor
    box::FrequencySeq seq1;
    REQUIRE(seq1.first() == 0.0_a);
    REQUIRE(seq1.spacing() == 0.0_a);
    REQUIRE(seq1.n() == 0);
    // Initialising constructor
    box::FrequencySeq seq2(1.0, 2.0, 3);
    REQUIRE(seq2.first() == 1.0_a);
    REQUIRE(seq2.spacing() == 2.0_a);
    REQUIRE(seq2.n() == 3);
    // Copy constructor
    box::FrequencySeq seq3(seq2);
    REQUIRE(seq3.first() == 1.0_a);
    REQUIRE(seq3.spacing() == 2.0_a);
    REQUIRE(seq3.n() == 3);
    // Assignment
    seq1 = seq3;
    REQUIRE(seq1.first() == 1.0_a);
    REQUIRE(seq1.spacing() == 2.0_a);
    REQUIRE(seq1.n() == 3);
    // Setters
    seq1.first(4.6);
    seq1.spacing(5.1);
    seq1.n(6);
    REQUIRE(seq1.first() == 4.6_a);
    REQUIRE(seq1.spacing() == 5.1_a);
    REQUIRE(seq1.n() == 6);
    // Iterator
    size_t i = 0;
    for(double f : seq1) {
        INFO("i = " << i);
        REQUIRE(f == Approx(5.1*i + 4.6));
        i++;
    }
    REQUIRE(i == 6);
    // Channel calculation
    REQUIRE(seq1.channel(4.1) == 0);
    REQUIRE(seq1.channel(4.7) == 0);
    REQUIRE(seq1.channel(11.8) == 1);
    REQUIRE(seq1.channel(30.1) == 5);
    REQUIRE(seq1.channel(35.3) == 5);
    REQUIRE(seq1.channel(100.0) == 5);
    seq1.n(0);
    REQUIRE(seq1.channel(4.1) == 0);
    REQUIRE(seq1.channel(4.7) == 0);
    REQUIRE(seq1.channel(11.8) == 0);
    REQUIRE(seq1.channel(30.1) == 0);
    REQUIRE(seq1.channel(35.3) == 0);
    REQUIRE(seq1.channel(100.0) == 0);
    // Last channel
    seq1.n(4);
    REQUIRE(seq1.last() == 19.9_a);
    // Frequency calculation
    seq1 = {4.6, 5.1, 4};
    REQUIRE(seq1.frequency(0) == 4.6_a);
    REQUIRE(seq1.frequency(1) == 9.7_a);
    REQUIRE(seq1.frequency(2) == 14.8_a);
    // Required samples calculation
    seq1 = {2.0, 1.0, 5};
    REQUIRE(seq1.requiredSamples(0.1) == 10);
    seq1 = {2.0, 0.0, 5};
    REQUIRE(seq1.requiredSamples(0.1) == 5);
    seq1 = {0.0, 0.0, 5};
    REQUIRE(seq1.requiredSamples(0.1) == 0);
    seq1 = {2.0, 1.0, 5};
    REQUIRE(seq1.requiredSamples(0.0) == 0);
    seq1 = {2.0, 1.0, 5};
    REQUIRE(seq1.requiredSamples(0.0000001) == 1e6);
    // Validity
    seq1 = {0.0, 0.0, 1};
    REQUIRE_FALSE(seq1.valid());
    seq1 = {1.0, 0.0, 0};
    REQUIRE_FALSE(seq1.valid());
    seq1 = {1.0, 0.0, 1};
    REQUIRE(seq1.valid());
    REQUIRE_FALSE(seq1.empty());
    // Merge operator
    seq1 = {0.0, 0.0, 0};
    seq2 = {1.0, 2.0, 3};
    seq1 |= seq2;
    REQUIRE(seq1.first() == 1.0);
    REQUIRE(seq1.spacing() == 2.0);
    REQUIRE(seq1.n() == 3);
    seq2 = {2.0, 4.0, 3};
    seq1 |= seq2;
    REQUIRE(seq1.first() == 1.0);
    REQUIRE(seq1.spacing() == 2.0);
    REQUIRE(seq1.n() == 6);
    // Clear
    seq1.clear();
    REQUIRE(seq1.first() == 0.0_a);
    REQUIRE(seq1.spacing() == 0.0_a);
    REQUIRE(seq1.n() == 0);
    REQUIRE(seq1.empty());
    // Equality operator
    seq1.clear();
    seq2.clear();
    REQUIRE(seq1 == seq2);
    REQUIRE_FALSE(seq1 != seq2);
    // Read and write XML
    seq1 = {2.0, 4.0, 3};
    seq2.clear();
    // Write the XML
    auto* root = new xml::DomObject("root");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << seq1);
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("root");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> seq2);
    REQUIRE(seq1 == seq2);
}
