/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/Polynomial.h>
#include <sstream>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>

using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("Polynomial_Constructors") {
    // Default constructor
    box::Polynomial p1;
    REQUIRE(p1.a() == 0.0_a);
    REQUIRE(p1.b() == 0.0_a);
    REQUIRE(p1.c() == 0.0_a);
    REQUIRE(p1.d() == 0.0_a);
    REQUIRE(p1.e() == 0.0_a);
    REQUIRE(p1.f() == 0.0_a);
    // Initialising constructor
    box::Polynomial p2(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
    REQUIRE(p2.a() == 1.0_a);
    REQUIRE(p2.b() == 2.0_a);
    REQUIRE(p2.c() == 3.0_a);
    REQUIRE(p2.d() == 4.0_a);
    REQUIRE(p2.e() == 5.0_a);
    REQUIRE(p2.f() == 6.0_a);
    // Copy constructor
    box::Polynomial p3(p2);
    REQUIRE(p3.a() == 1.0_a);
    REQUIRE(p3.b() == 2.0_a);
    REQUIRE(p3.c() == 3.0_a);
    REQUIRE(p3.d() == 4.0_a);
    REQUIRE(p3.e() == 5.0_a);
    REQUIRE(p3.f() == 6.0_a);
}

// Operation of the operators
TEST_CASE("Polynomial_Operators") {
    box::Polynomial p1;
    box::Polynomial p2(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
    // Inequality
    REQUIRE(p1 != p2);
    REQUIRE_FALSE(p1 == p2);
    // Assignment
    p1 = p2;
    REQUIRE(p1.a() == 1.0_a);
    REQUIRE(p1.b() == 2.0_a);
    REQUIRE(p1.c() == 3.0_a);
    REQUIRE(p1.d() == 4.0_a);
    REQUIRE(p1.e() == 5.0_a);
    REQUIRE(p1.f() == 6.0_a);
    // Equality
    REQUIRE_FALSE(p1 != p2);
    REQUIRE(p1 == p2);
}

// Polynomial value
TEST_CASE("Polynomial_Value") {
    box::Polynomial p1(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
    REQUIRE(p1.value(1.0) == 21.0_a);
    REQUIRE(p1.value(2.0) == 321.0_a);
    REQUIRE(p1.value(0.2) == 1.56192_a);
}

// XML reading
TEST_CASE("Polynomial_XmlRead") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "    <poly_1>\n"
            "        <a>1</a>\n"
            "        <b>2</b>\n"
            "        <c>3</c>\n"
            "        <d>4</d>\n"
            "        <e>5</e>\n"
            "        <f>6</f>\n"
            "    </poly_1>\n"
            "</root>\n"));
    box::Polynomial v1;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Obj("poly_1") >> v1);
    REQUIRE(v1 == box::Polynomial(1, 2, 3, 4, 5, 6));
}

// XML Writing
TEST_CASE("Polynomial_XmlWrite") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_NOTHROW(*root << xml::Obj("poly_1")
                          << box::Polynomial(1, 2, 3, 4, 5, 6));
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root>\n"
                    "    <poly_1>\n"
                    "        <a>1</a>\n"
                    "        <b>2</b>\n"
                    "        <c>3</c>\n"
                    "        <d>4</d>\n"
                    "        <e>5</e>\n"
                    "        <f>6</f>\n"
                    "    </poly_1>\n"
                    "</root>\n");
}

// Polynomial regression
TEST_CASE("Polynomial_Fit") {
    std::vector<double> x = {0.1, 0.2, 0.3, 0.4, 0.5,
                             0.6, 0.7, 0.8, 0.9, 1.0,
                             1.1, 1.2, 1.3, 1.4, 1.5,
                             1.6, 1.7, 1.8, 1.9, 2.0,
                             2.1, 2.2, 2.3, 2.4, 2.5,
                             2.6, 2.7, 2.8, 2.9, 3.0};
    std::vector<double> y = {1.23456, 1.56192, 2.03308, 2.72544, 3.75000,
                             5.25856, 7.45092, 10.58208, 14.96944, 21.00000,
                             29.13756, 39.92992, 54.01608, 72.13344, 95.12500,
                             123.94656, 159.67392, 203.51008, 256.79244, 321.00000,
                             397.76056, 488.85792, 596.23908, 722.02144, 868.50000,
                             1038.15456, 1233.65692, 1457.87808, 1713.89544, 2005.00000};
    box::Polynomial p1;
    REQUIRE(p1.fit(x,y,5));
    REQUIRE(p1.a() == 1.0_a);
    REQUIRE(p1.b() == 2.0_a);
    REQUIRE(p1.c() == 3.0_a);
    REQUIRE(p1.d() == 4.0_a);
    REQUIRE(p1.e() == 5.0_a);
    REQUIRE(p1.f() == 6.0_a);
    REQUIRE(p1.fit(x,y,4));
    REQUIRE(p1.a() == 10.2752_a);
    REQUIRE(p1.b() == -70.99773333_a);
    REQUIRE(p1.c() == 157.225_a);
    REQUIRE(p1.d() == -125.26666666_a);
    REQUIRE(p1.e() == 51.5_a);
    REQUIRE(p1.f() == 0.0_a);
}

