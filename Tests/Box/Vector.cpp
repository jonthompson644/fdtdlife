/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/Vector.h>
#include <sstream>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>

using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("Vector_Constructors") {
    // Default constructor
    box::Vector<int> v1;
    REQUIRE(v1.x() == 0);
    REQUIRE(v1.y() == 0);
    REQUIRE(v1.z() == 0);
    // Initialising constuctors
    box::Vector<int> v2(1, 2, 3);
    REQUIRE(v2.x() == 1);
    REQUIRE(v2.y() == 2);
    REQUIRE(v2.z() == 3);
    box::Vector<int> v3(4);
    REQUIRE(v3.x() == 4);
    REQUIRE(v3.y() == 4);
    REQUIRE(v3.z() == 4);
    // Copy constructor
    box::Vector<int> v4(v2);
    REQUIRE(v4.x() == 1);
    REQUIRE(v4.y() == 2);
    REQUIRE(v4.z() == 3);
    // Conversion constructor
    box::Vector<double> v5(5.1, 6.1, 7.1);
    box::Vector<int> v6(v5);
    REQUIRE(v6.x() == 5);
    REQUIRE(v6.y() == 6);
    REQUIRE(v6.z() == 7);
}

// Operation of assignment operators
TEST_CASE("Vector_Assignment") {
    box::Vector<int> v1(1, 2, 3);
    // Same type assignment
    box::Vector<int> v2;
    v2 = v1;
    REQUIRE(v2.x() == 1);
    REQUIRE(v2.y() == 2);
    REQUIRE(v2.z() == 3);
    // Member assignment
    box::Vector<double> v4;
    v4.x(4);
    v4.y(5);
    v4.z(6);
    REQUIRE(v4.x() == 4);
    REQUIRE(v4.y() == 5);
    REQUIRE(v4.z() == 6);
    // Constant assignment
    v2 = 9;
    REQUIRE(v2.x() == 9);
    REQUIRE(v2.y() == 9);
    REQUIRE(v2.z() == 9);
}

// Operation of addition operators
TEST_CASE("Vector_AdditionOperators") {
    box::Vector<int> v1(1, 2, 3);
    box::Vector<int> v2(4, 5, 6);
    box::Vector<double> v3(7.1, 8.1, 9.1);
    // Addition of same type
    box::Vector<int> v4 = v1 + v2;
    REQUIRE(v4.x() == 5);
    REQUIRE(v4.y() == 7);
    REQUIRE(v4.z() == 9);
    // Addition of a constant
    box::Vector<int> v7 = v1 + 1;
    REQUIRE(v7.x() == 2);
    REQUIRE(v7.y() == 3);
    REQUIRE(v7.z() == 4);
    box::Vector<int> v8 = 1 + v1;
    REQUIRE(v8.x() == 2);
    REQUIRE(v8.y() == 3);
    REQUIRE(v8.z() == 4);
    // Assignment with addition
    box::Vector<int> v9 = v1;
    v9 += v2;
    REQUIRE(v9.x() == 5);
    REQUIRE(v9.y() == 7);
    REQUIRE(v9.z() == 9);
}

// Operation of subtraction operators
TEST_CASE("Vector_SubtractionOperators") {
    box::Vector<int> v1(1, 2, 3);
    box::Vector<int> v2(4, 6, 8);
    box::Vector<double> v3(7.1, 9.1, 11.1);
    // Subtraction of same type
    box::Vector<int> v4 = v1 - v2;
    REQUIRE(v4.x() == -3);
    REQUIRE(v4.y() == -4);
    REQUIRE(v4.z() == -5);
    // Subtraction of a constant
    box::Vector<int> v7 = v1 - 1;
    REQUIRE(v7.x() == 0);
    REQUIRE(v7.y() == 1);
    REQUIRE(v7.z() == 2);
    box::Vector<int> v8 = 1 - v1;
    REQUIRE(v8.x() == 0);
    REQUIRE(v8.y() == -1);
    REQUIRE(v8.z() == -2);
    // Unary minus
    box::Vector<int> v9 = -v1;
    REQUIRE(v9.x() == -1);
    REQUIRE(v9.y() == -2);
    REQUIRE(v9.z() == -3);
}

// Operation of multiple and divide operators
TEST_CASE("Vector_MultipleAndDivideOperators") {
    box::Vector<double> v1(2.0, 3.0, 4.0);
    box::Vector<double> v2(5.0, 6.0, 7.0);
    // Multiply
    box::Vector<double> v3 = v1 * v2;
    REQUIRE(v3.x() == 10.0_a);
    REQUIRE(v3.y() == 18.0_a);
    REQUIRE(v3.z() == 28.0_a);
    // Multiply by constant
    box::Vector<double> v4 = v1 * 2;
    REQUIRE(v4.x() == 4.0_a);
    REQUIRE(v4.y() == 6.0_a);
    REQUIRE(v4.z() == 8.0_a);
    box::Vector<double> v5 = 2 * v1;
    REQUIRE(v5.x() == 4.0_a);
    REQUIRE(v5.y() == 6.0_a);
    REQUIRE(v5.z() == 8.0_a);
    // Divide
    box::Vector<double> v6 = v1 / v2;
    REQUIRE(v6.x() == Approx(2.0 / 5.0));
    REQUIRE(v6.y() == Approx(3.0 / 6.0));
    REQUIRE(v6.z() == Approx(4.0 / 7.0));
    // Divide by a constant
    box::Vector<double> v7 = v1 / 2.0;
    REQUIRE(v7.x() == Approx(2.0 / 2.0));
    REQUIRE(v7.y() == Approx(3.0 / 2.0));
    REQUIRE(v7.z() == Approx(4.0 / 2.0));
    box::Vector<double> v8 = 2.0 / v1;
    REQUIRE(v8.x() == Approx(2.0 / 2.0));
    REQUIRE(v8.y() == Approx(2.0 / 3.0));
    REQUIRE(v8.z() == Approx(2.0 / 4.0));
}

// Operation of comparison operators
TEST_CASE("Vector_ComparisonOperators") {
    box::Vector<int> v1(1, 2, 3);
    // Equality
    REQUIRE(v1 == box::Vector<int>(1, 2, 3));
    REQUIRE_FALSE(v1 == box::Vector<int>(1, 2, 4));
    REQUIRE_FALSE(v1 == box::Vector<int>(1, 3, 3));
    REQUIRE_FALSE(v1 == box::Vector<int>(2, 2, 3));
    // Inequality
    REQUIRE_FALSE(v1 != box::Vector<int>(1, 2, 3));
    REQUIRE(v1 != box::Vector<int>(1, 2, 4));
    REQUIRE(v1 != box::Vector<int>(1, 3, 3));
    REQUIRE(v1 != box::Vector<int>(2, 2, 3));
    // Greater than or equal (not sure about this one)
    REQUIRE(v1 >= box::Vector<int>(0, 1, 2));
    REQUIRE(v1 >= box::Vector<int>(1, 2, 3));
    REQUIRE_FALSE(v1 >= box::Vector<int>(1, 2, 4));
    REQUIRE_FALSE(v1 >= box::Vector<int>(1, 3, 3));
    REQUIRE_FALSE(v1 >= box::Vector<int>(2, 2, 3));
    // Less than or equal (not sure about this one)
    REQUIRE(v1 <= box::Vector<int>(2, 3, 4));
    REQUIRE(v1 <= box::Vector<int>(1, 2, 3));
    REQUIRE_FALSE(v1 <= box::Vector<int>(1, 2, 2));
    REQUIRE_FALSE(v1 <= box::Vector<int>(1, 1, 3));
    REQUIRE_FALSE(v1 <= box::Vector<int>(0, 2, 3));
}

// Operation of math functions
TEST_CASE("Vector_Functions") {
    // Ceiling
    box::Vector<double> v1 = box::Vector<double>(1.3, 2.3, 3.3).ceil<double>();
    REQUIRE(v1.x() == 2.0_a);
    REQUIRE(v1.y() == 3.0_a);
    REQUIRE(v1.z() == 4.0_a);
    // Floor
    box::Vector<double> v2 = box::Vector<double>(1.8, 2.8, 3.8).floor<double>();
    REQUIRE(v2.x() == 1.0_a);
    REQUIRE(v2.y() == 2.0_a);
    REQUIRE(v2.z() == 3.0_a);
    // Round
    box::Vector<double> v3 = box::Vector<double>(
            1.888888888, 3.33333333, 3.555555555).round<double>();
    REQUIRE(v3.x() == 2.0_a);
    REQUIRE(v3.y() == 3.0_a);
    REQUIRE(v3.z() == 4.0_a);
    // Modulo
    box::Vector<double> v4 = box::Vector<double>(8.0, 7.0, 6.0).
            modulo(box::Vector<double>(2.0, 3.0, 4.0));
    REQUIRE(v4.x() == 0.0_a);
    REQUIRE(v4.y() == 1.0_a);
    REQUIRE(v4.z() == 2.0_a);
    // Magnitude
    double v5 = box::Vector<double>(8.0, 7.0, 6.0).mag();
    REQUIRE(v5 == Approx(sqrt(8.0 * 8.0 + 7.0 * 7.0 + 6.0 * 6.0)));
    // Exponent
    box::Vector<double> v6 = box::Vector<double>(1.1, 1.2, 1.3).exp();
    REQUIRE(v6.x() == std::exp(1.1));
    REQUIRE(v6.y() == std::exp(1.2));
    REQUIRE(v6.z() == std::exp(1.3));
    // Signed magnitude
    double v7 = box::Vector<double>(8.0, 7.0, 6.0).signedMag();
    REQUIRE(v7 == Approx(sqrt(8.0 * 8.0 + 7.0 * 7.0 + 6.0 * 6.0)));
    v7 = box::Vector<double>(-8.0, 7.0, 6.0).signedMag();
    REQUIRE(v7 == -Approx(sqrt(8.0 * 8.0 + 7.0 * 7.0 + 6.0 * 6.0)));
    // Polarisation
    double v8 = box::Vector<double>(8.0, 7.0, 6.0).polarisation();
    REQUIRE(v8 == Approx(atan2(7.0, 8.0)));
    // Abs
    box::Vector<double> v10(-1, -2, -3);
    v10 = v10.abs();
    REQUIRE(v10.x() == 1.0_a);
    REQUIRE(v10.y() == 2.0_a);
    REQUIRE(v10.z() == 3.0_a);
    // Min, max
    box::Vector<double> v11(4, 5, 6);
    box::Vector<double> v12(3, 4, 7);
    REQUIRE(v11.min(v12) == box::Vector<double>(3, 4, 6));
    REQUIRE(v11.max(v12) == box::Vector<double>(4, 5, 7));
    // Casting
    box::Vector<double> v9;
    v9 = static_cast<box::Vector<double>>(box::Vector<size_t>(1, 2, 3));
    REQUIRE(v9.x() == 1.0_a);
    REQUIRE(v9.y() == 2.0_a);
    REQUIRE(v9.z() == 3.0_a);
    // Stream insertion
    std::stringstream s;
    s << box::Vector<int>(1, 2, 3);
    REQUIRE(s.str() == "1,2,3");
}

// Enumeration specialisation
TEST_CASE("Vector_Enumerations") {
    enum class Items {
        one, two, three
    };
    // Constructors
    box::Vector<Items> v1(Items::one, Items::two, Items::three);
    REQUIRE(v1.x() == Items::one);
    REQUIRE(v1.y() == Items::two);
    REQUIRE(v1.z() == Items::three);
    box::Vector<Items> v2;
    REQUIRE(v2.x() == Items::one);
    REQUIRE(v2.y() == Items::one);
    REQUIRE(v2.z() == Items::one);
    box::Vector<Items> v3(Items::two);
    REQUIRE(v3.x() == Items::two);
    REQUIRE(v3.y() == Items::two);
    REQUIRE(v3.z() == Items::two);
    box::Vector<Items> v4(v1);
    REQUIRE(v4.x() == Items::one);
    REQUIRE(v4.y() == Items::two);
    REQUIRE(v4.z() == Items::three);
    // Assignment operators
    v2 = Items::three;
    REQUIRE(v2.x() == Items::three);
    REQUIRE(v2.y() == Items::three);
    REQUIRE(v2.z() == Items::three);
    v2 = v1;
    REQUIRE(v2.x() == Items::one);
    REQUIRE(v2.y() == Items::two);
    REQUIRE(v2.z() == Items::three);
    // Comparison operators
    REQUIRE(v2 == v1);
    REQUIRE(v2 != v3);
    // Setters
    v2.x(Items::three);
    v2.y(Items::one);
    v2.z(Items::two);
    REQUIRE(v2.x() == Items::three);
    REQUIRE(v2.y() == Items::one);
    REQUIRE(v2.z() == Items::two);
    // Stream output
    std::stringstream s;
    s << v1;
    REQUIRE(s.str() == "0,1,2");
}

// String specialisation
TEST_CASE("Vector_Strings") {
    // Constructors
    box::Vector<std::string> v1("one", "two", "three");
    REQUIRE(v1.x() == "one");
    REQUIRE(v1.y() == "two");
    REQUIRE(v1.z() == "three");
    box::Vector<std::string> v2;
    REQUIRE(v2.x().empty());
    REQUIRE(v2.y().empty());
    REQUIRE(v2.z().empty());
    box::Vector<std::string> v3("two");
    REQUIRE(v3.x() == "two");
    REQUIRE(v3.y() == "two");
    REQUIRE(v3.z() == "two");
    box::Vector<std::string> v4(v1);
    REQUIRE(v4.x() == "one");
    REQUIRE(v4.y() == "two");
    REQUIRE(v4.z() == "three");
    // Assignment operators
    v2 = "three";
    REQUIRE(v2.x() == "three");
    REQUIRE(v2.y() == "three");
    REQUIRE(v2.z() == "three");
    v2 = v1;
    REQUIRE(v2.x() == "one");
    REQUIRE(v2.y() == "two");
    REQUIRE(v2.z() == "three");
    // Comparison operators
    REQUIRE(v2 == v1);
    REQUIRE(v2 != v3);
    // Setters
    v2.x("three");
    v2.y("one");
    v2.z("two");
    REQUIRE(v2.x() == "three");
    REQUIRE(v2.y() == "one");
    REQUIRE(v2.z() == "two");
    // Stream output
    std::stringstream s;
    s << v1;
    REQUIRE(s.str() == "one,two,three");
}

// XML reading
TEST_CASE("Vector_XmlRead") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "    <vector_1>\n"
            "        <x>-2</x>\n"
            "        <y>-3</y>\n"
            "        <z>-4</z>\n"
            "    </vector_1>\n"
            "    <vector_2>\n"
            "        <x>one</x>\n"
            "        <y>two</y>\n"
            "        <z>three</z>\n"
            "    </vector_2>\n"
            "    <vector_3>\n"
            "        <x>0</x>\n"
            "        <y>1</y>\n"
            "        <z>2</z>\n"
            "    </vector_3>\n"
            "</root>\n"));
    box::Vector<int> v1;
    box::Vector<std::string> v2;
    enum class Items {
        one, two, three
    };
    box::Vector<Items> v3;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Obj("vector_1") >> v1);
    REQUIRE(v1 == box::Vector<int>(-2, -3, -4));
    REQUIRE_NOTHROW(*root >> xml::Obj("vector_2") >> v2);
    REQUIRE(v2 == box::Vector<std::string>("one", "two", "three"));
    REQUIRE_NOTHROW(*root >> xml::Obj("vector_3") >> v3);
    REQUIRE(v3 == box::Vector<Items>(Items::one, Items::two, Items::three));
}

// XML Writing
TEST_CASE("Vector_XmlWrite") {
    enum class Items {
        one, two, three
    };
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_NOTHROW(*root << xml::Obj("vector_1")
                          << box::Vector<int>(-2, -3, -4));
    REQUIRE_NOTHROW(*root << xml::Obj("vector_2")
                          << box::Vector<std::string>("one", "two", "three"));
    REQUIRE_NOTHROW(*root << xml::Obj("vector_3")
                          << box::Vector<Items>(Items::one, Items::two, Items::three));
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root>\n"
                    "    <vector_1>\n"
                    "        <x>-2</x>\n"
                    "        <y>-3</y>\n"
                    "        <z>-4</z>\n"
                    "    </vector_1>\n"
                    "    <vector_2>\n"
                    "        <x>one</x>\n"
                    "        <y>two</y>\n"
                    "        <z>three</z>\n"
                    "    </vector_2>\n"
                    "    <vector_3>\n"
                    "        <x>0</x>\n"
                    "        <y>1</y>\n"
                    "        <z>2</z>\n"
                    "    </vector_3>\n"
                    "</root>\n");
}

