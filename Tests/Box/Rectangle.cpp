/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/Rectangle.h>
#include <sstream>
using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("Rectangle_Constructors") {
    // Default constructor
    box::Rectangle<int> r1;
    REQUIRE(r1.top() == 0);
    REQUIRE(r1.left() == 0);
    REQUIRE(r1.bottom() == 0);
    REQUIRE(r1.right() == 0);
    // Initialising constructor
    box::Rectangle<int> r2(1,2,3,4);
    REQUIRE(r2.top() == 2);
    REQUIRE(r2.left() == 1);
    REQUIRE(r2.bottom() == 4);
    REQUIRE(r2.right() == 3);
    // Copy constructor
    box::Rectangle<int> r3(r2);
    REQUIRE(r3.top() == 2);
    REQUIRE(r3.left() == 1);
    REQUIRE(r3.bottom() == 4);
    REQUIRE(r3.right() == 3);
}

// Rectangle operations
TEST_CASE("Rectangle_Operations") {
    // Assignment
    box::Rectangle<int> r1;
    r1 = {1, 2, 4, 5};
    REQUIRE(r1.top() == 2);
    REQUIRE(r1.left() == 1);
    REQUIRE(r1.bottom() == 5);
    REQUIRE(r1.right() == 4);
    // Center
    REQUIRE(r1.centerX() == 2.0_a);
    REQUIRE(r1.centerY() == 3.0_a);
    // Point is inside the rectangle
    REQUIRE(r1.isIn(2, 3));
    REQUIRE(r1.isIn(1, 3));
    REQUIRE(r1.isIn(2, 2));
    REQUIRE_FALSE(r1.isIn(4, 3));
    REQUIRE_FALSE(r1.isIn(2, 5));
    REQUIRE_FALSE(r1.isIn(0, 0));
    // stream output
    std::stringstream text;
    text << r1;
    REQUIRE(text.str() == "1,2...4,5");
    // Setters
    r1.left(5);
    r1.top(6);
    r1.right(7);
    r1.bottom(8);
    REQUIRE(r1.top() == 6);
    REQUIRE(r1.left() == 5);
    REQUIRE(r1.bottom() == 8);
    REQUIRE(r1.right() == 7);
    // Square
    r1 = box::Rectangle<int>::square(2,2,6);
    REQUIRE(r1.top() == -1);
    REQUIRE(r1.left() == -1);
    REQUIRE(r1.bottom() == 5);
    REQUIRE(r1.right() == 5);
    // Is a point in any rectangles
    REQUIRE(box::Rectangle<int>::isInAny({
        box::Rectangle<int>(1,1,5,5), box::Rectangle<int>(1,1,7,7)}, 4, 6));
    // Division
    r1 = {2, 4, 6, 8};
    r1 = r1 / 2;
    REQUIRE(r1.top() == 2);
    REQUIRE(r1.left() == 1);
    REQUIRE(r1.bottom() == 4);
    REQUIRE(r1.right() == 3);
    // Validity
    r1 = {2, 4, 6, 8};
    REQUIRE(r1.isValid());
    r1 = {6, 8, 2, 4};
    REQUIRE_FALSE(r1.isValid());
    // Clip
    r1 = {1, 1, 8, 8};
    r1 = r1.clip({2, 2, 6, 6});
    REQUIRE(r1.top() == 2);
    REQUIRE(r1.left() == 2);
    REQUIRE(r1.bottom() == 6);
    REQUIRE(r1.right() == 6);
}
