/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Box/Point2D.h>
#include <sstream>

using namespace Catch::literals;

// Operation of the constructors
TEST_CASE("Point2D_Constructors") {
    // Default constructor
    box::Point2D<double> p1;
    REQUIRE(p1.x() == 0.0_a);
    REQUIRE(p1.y() == 0.0_a);
    // Initialising constructor
    box::Point2D<double> p2(1.0, 2.0);
    REQUIRE(p2.x() == 1.0_a);
    REQUIRE(p2.y() == 2.0_a);
    // Copy constructor
    box::Point2D<double> p3(p2);
    REQUIRE(p3.x() == 1.0_a);
    REQUIRE(p3.y() == 2.0_a);
}

// Operation of the operators
TEST_CASE("Point2D_Operators") {
    // Assignment
    box::Point2D<double> p1(1.0, 2.0);
    box::Point2D<double> p2(3.0, 4.0);
    REQUIRE(p1.x() == 1.0_a);
    REQUIRE(p1.y() == 2.0_a);
    REQUIRE(p2.x() == 3.0_a);
    REQUIRE(p2.y() == 4.0_a);
    p1 = p2;
    REQUIRE(p1.x() == 3.0_a);
    REQUIRE(p1.y() == 4.0_a);
    // Addition
    box::Point2D<double> p3 = p1 + p2;
    REQUIRE(p3.x() == 6.0_a);
    REQUIRE(p3.y() == 8.0_a);
}

// Operation of setters
TEST_CASE("Point2D_Setters") {
    box::Point2D<double> p1(1.0, 2.0);
    REQUIRE(p1.x() == 1.0_a);
    REQUIRE(p1.y() == 2.0_a);
    p1.x(3.0);
    REQUIRE(p1.x() == 3.0_a);
    REQUIRE(p1.y() == 2.0_a);
    p1.y(4.0);
    REQUIRE(p1.x() == 3.0_a);
    REQUIRE(p1.y() == 4.0_a);
}
