/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Box/Utility.h>
#include <sstream>
using namespace Catch::literals;

// String split function
TEST_CASE("Utility_Split") {
    std::vector<std::string> s;
    // Empty string
    s = box::Utility::split("", "\n");
    REQUIRE(s.empty());
    // No splits
    s = box::Utility::split("aaaaa", "\n");
    REQUIRE(s.size() == 1);
    REQUIRE(s[0] == "aaaaa");
    // One split
    s = box::Utility::split("aaaaa\nbbbbb", "\n");
    REQUIRE(s.size() == 2);
    REQUIRE(s[0] == "aaaaa");
    REQUIRE(s[1] == "bbbbb");
    // Two splits
    s = box::Utility::split("aaaaa\nbbbbb\nccccc", "\n");
    REQUIRE(s.size() == 3);
    REQUIRE(s[0] == "aaaaa");
    REQUIRE(s[1] == "bbbbb");
    REQUIRE(s[2] == "ccccc");
    // Multi-character splits
    s = box::Utility::split("aaaaa\n\rbbbbb\nccccc", "\n\r");
    REQUIRE(s.size() == 2);
    REQUIRE(s[0] == "aaaaa");
    REQUIRE(s[1] == "bbbbb\nccccc");
}

// String stripFront function
TEST_CASE("Utility_StripFront") {
    std::string s;
    // Empty string
    s = box::Utility::stripFront("", "ab");
    REQUIRE(s.empty());
    // No character at front
    s = box::Utility::stripFront("cdef", "ab");
    REQUIRE(s == "cdef");
    // One character at front
    s = box::Utility::stripFront("acdef", "ab");
    REQUIRE(s == "cdef");
    // Two characters at front
    s = box::Utility::stripFront("abcdef", "ab");
    REQUIRE(s == "cdef");
    // Character in the middle
    s = box::Utility::stripFront("cdabef", "ab");
    REQUIRE(s == "cdabef");
}

// String stripBack function
TEST_CASE("Utility_StripBack") {
    std::string s;
    // Empty string
    s = box::Utility::stripBack("", "ab");
    REQUIRE(s.empty());
    // No character at back
    s = box::Utility::stripBack("cdef", "gh");
    REQUIRE(s == "cdef");
    // One character at back
    s = box::Utility::stripBack("cdefg", "gh");
    REQUIRE(s == "cdef");
    // Two characters at back
    s = box::Utility::stripBack("cdefgh", "gh");
    REQUIRE(s == "cdef");
    // Character in the middle
    s = box::Utility::stripBack("cdghef", "gh");
    REQUIRE(s == "cdghef");
}

// String strip function
TEST_CASE("Utility_Strip") {
    std::string s;
    // Empty string
    s = box::Utility::strip("", "ab");
    REQUIRE(s.empty());
    // No characters
    s = box::Utility::strip("cdef", "ab");
    REQUIRE(s == "cdef");
    // One character at each end
    s = box::Utility::strip("acdefa", "ab");
    REQUIRE(s == "cdef");
    // Two characters at each end
    s = box::Utility::strip("abcdefab", "ab");
    REQUIRE(s == "cdef");
    // Characters in the middle
    s = box::Utility::strip("cdabef", "ab");
    REQUIRE(s == "cdabef");
}

// String to double function
TEST_CASE("Utility_toDouble") {
    REQUIRE(box::Utility::toDouble("0.4") == 0.4_a);
    REQUIRE(box::Utility::toDouble("") == 0.0_a);
    REQUIRE(box::Utility::toDouble("54e3k") == 54e3_a);
    REQUIRE(box::Utility::toDouble("f6") == 0.0_a);
}

// String to complex function
TEST_CASE("Utility_toComplex") {
    std::complex<double> v;
    v = box::Utility::toComplex("0.4");
    REQUIRE(v.real() == 0.4_a);
    REQUIRE(v.imag() == 0.0_a);
    v = box::Utility::toComplex("0.4+2.0i");
    REQUIRE(v.real() == 0.4_a);
    REQUIRE(v.imag() == 2.0_a);
    v = box::Utility::toComplex("+0.4+2.0i");
    REQUIRE(v.real() == 0.4_a);
    REQUIRE(v.imag() == 2.0_a);
    v = box::Utility::toComplex("-0.4+2.0i");
    REQUIRE(v.real() == -0.4_a);
    REQUIRE(v.imag() == 2.0_a);
    v = box::Utility::toComplex("-0.4-2.0i");
    REQUIRE(v.real() == -0.4_a);
    REQUIRE(v.imag() == -2.0_a);
    v = box::Utility::toComplex("-4e-1-2e-1i");
    REQUIRE(v.real() == -0.4_a);
    REQUIRE(v.imag() == -0.2_a);
    v = box::Utility::toComplex("-0.4-2.0j");
    REQUIRE(v.real() == -0.4_a);
    REQUIRE(v.imag() == -2.0_a);
}
