/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Fdtd/SquarePlate.h>
#include <Fdtd/NodeManager.h>
#include <Seq/CharacterizerJob.h>
#include <Seq/CharacterizerStep.h>

using namespace Catch::literals;

// A simple document with an empty root object
TEST_CASE("Characterizer_NumberOfIdenticalLayers") {
    // The model
    TestModel model;
    model.clear();
    model.addCharacterizer();
    REQUIRE(model.characterizer_->mode() == seq::Sequencer::modeCharacterizer);
    REQUIRE(model.characterizer_->identifier() == 0);
    // Set up the model ready for the testing of a layer
    model.p()->p1({-0.0004, -0.0004, -0.0002});
    model.p()->p2({+0.0004, +0.0004, +0.002});
    model.p()->dr({0.00005, 0.00005, 0.00005});
    model.p()->boundaryX(fdtd::Configuration::Boundary::periodic);
    model.p()->boundaryY(fdtd::Configuration::Boundary::periodic);
    model.p()->boundaryZ(fdtd::Configuration::Boundary::absorbing);
    model.p()->absorbing(fdtd::Configuration::absorbingCpml);
    model.p()->boundarySize(20);
    model.p()->useThinSheetSubcells(false);
    model.p()->scatteredFieldZoneSize(2);
    model.p()->tSize(5e-12);
    // The source
    model.addPlaneWaveSource();
    model.planeWaveSource_->frequencies({10e9, 10e9, 10});
    model.planeWaveSource_->amplitude(1.0);
    model.planeWaveSource_->azimuth(0.0);
    model.planeWaveSource_->polarisation(0.0);
    model.planeWaveSource_->initialTime(0.0);
    model.planeWaveSource_->continuous(true);
    // Material
    auto* metal = model.addMaterial();
    metal->name("Metal");
    metal->epsilon(-10e12);
    metal->mu(box::Constants::mu0_);
    metal->sigma(1.0);
    metal->sigmastar(0.0);
    // Sensor
    model.addArraySensor();
    model.arraySensor_->numPointsX(1);
    model.arraySensor_->numPointsY(1);
    model.arraySensor_->averageOverCell(true);
    model.arraySensor_->componentSel({true, false, false});
    // The prototype layer
    auto* protoLayer = dynamic_cast<fdtd::SquarePlate*>(fdtd::Shape::factory(
            fdtd::Shape::squarePlate, model.d()->allocShapeIdentifier(), &model));
    REQUIRE(protoLayer != nullptr);
    protoLayer->plateSize(90.0);
    protoLayer->plateOffsetX(0.0);
    protoLayer->plateOffsetY(0.0);
    protoLayer->layerThickness(1e-6);
    protoLayer->fillMaterial(fdtd::Configuration::defaultMaterial);
    protoLayer->cellSize(0.0008);
    protoLayer->repeatX(1);
    protoLayer->repeatY(1);
    protoLayer->p({0, 0, 0});
    protoLayer->material(metal->index());
    // Create the number of layers scanner
    auto* scanner = dynamic_cast<seq::IdenticalLayersScanner*>(
            model.characterizer_->makeScanner(seq::ScannerFactory::modeIdenticalLayers));
    REQUIRE(scanner != nullptr);
    REQUIRE(model.characterizer_->scanners().size() == 1);
    scanner->increment({4, 5, 6});
    REQUIRE(scanner->increment() == box::Vector<double>(4, 5, 6));
    scanner->increment({0, 0, 0.0002});
    scanner->startStep(3);
    REQUIRE(scanner->startStep() == 3);
    scanner->numSteps(4);
    REQUIRE(scanner->numSteps() == 4);
    scanner->prototypeLayer(protoLayer->identifier());
    REQUIRE(scanner->prototypeLayer() == protoLayer->identifier());
    // Save the configuration
    model.writeConfigFile("", "characterizerTest");
    // Create the jobs
    model.start();
    REQUIRE(model.processingMgr()->waitingJobs().size() == 3);
    REQUIRE(model.processingMgr()->runningJobs().size() == 1);
    auto* job = dynamic_cast<seq::CharacterizerJob*>(
            model.processingMgr()->runningJobs().front().get());
    REQUIRE(job != nullptr);
    size_t pos = 3;
    REQUIRE(job->step()->step() == std::vector<size_t>({pos++}));
    for(auto& j : model.processingMgr()->waitingJobs()) {
        job = dynamic_cast<seq::CharacterizerJob*>(j.get());
        REQUIRE(job->step()->step() == std::vector<size_t>({pos++}));
    }
    // Has the first job populated the model properly
    REQUIRE(model.d()->shapes().size() == 3);
    box::Vector<double> center = protoLayer->p();
    for(auto& s : model.d()->shapes()) {
        auto* layer = dynamic_cast<fdtd::UnitCellShape*>(s);
        REQUIRE(layer != nullptr);
        REQUIRE(layer->p().x() == Approx(center.x()));
        REQUIRE(layer->p().y() == Approx(center.y()));
        REQUIRE(layer->p().z() == Approx(center.z()));
        center += box::Vector<double>(0, 0, 0.0002);
    }
    // Are all the steps present in the characterizer with no data
    REQUIRE(model.characterizer_->steps().size() == 4);
    for(auto& step : model.characterizer_->steps()) {
        REQUIRE(step->empty());
    }
    // Now step the model until the first job completes
    while(model.processingMgr()->waitingJobs().size() == 3) {
        model.step();
    }
    // Does the first step now have data (and no others)
    REQUIRE(model.characterizer_->steps().size() == 4);
    for(auto& step : model.characterizer_->steps()) {
        if(step->step() == std::vector<size_t>({3})) {
            REQUIRE_FALSE(step->empty());
        } else {
            REQUIRE(step->empty());
        }
    }
    // Now step the model until the second job completes
    while(model.processingMgr()->waitingJobs().size() == 2) {
        model.step();
    }
    // Does the first step now have data (and no others)
    REQUIRE(model.characterizer_->steps().size() == 4);
    for(auto& step : model.characterizer_->steps()) {
        if(step->step() == std::vector<size_t>({3})) {
            REQUIRE_FALSE(step->empty());
        } else if(step->step() == std::vector<size_t>({4})) {
            REQUIRE_FALSE(step->empty());
        } else {
            REQUIRE(step->empty());
        }
    }
}
