/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_TESTMODEL_H
#define FDTDLIFE_TESTMODEL_H

#include <Fdtd/Model.h>
#include <Domain/Domain.h>
#include <Domain/Material.h>
#include <Domain/FingerPlate.h>
#include <Source/Sources.h>
#include <Source/PlaneWaveSource.h>
#include <Source/ZoneSource.h>
#include <Source/GravyWaveSource.h>
#include <Sensor/RadiationPattern.h>
#include <Sensor/AreaSensor.h>
#include <Sensor/ArraySensor.h>
#include <Sensor/MaterialSensor.h>
#include <Sensor/TwoDSlice.h>
#include <Sensor/FarFieldSensor.h>
#include <Seq/SingleSeq.h>
#include <Seq/Characterizer.h>
#include <Seq/IdenticalLayersScanner.h>
#include <Fdtd/EField.h>
#include <Fdtd/HField.h>
#include <Box/Constants.h>

// A derivation of the model class for use by the test suite.
class TestModel : public fdtd::Model {
public:
    sensor::RadiationPattern* radiationPatternSensor_{};
    sensor::AreaSensor* areaSensor_{};
    sensor::FarFieldSensor* farFieldSensor_{};
    source::ZoneSource* zoneSource_{};
    source::GravyWaveSource* gravyWaveSource_{};
    seq::Characterizer* characterizer_{};
    TestModel() = default;
    void basicConfiguration(double topLeftX, double topLeftY, double topLeftZ,
                            double bottomRightX, double bottomRightY, double bottomRightZ,
                            double dx, double dy, double dz,
                            fdtd::Configuration::Boundary boundaryX,
                            fdtd::Configuration::Boundary boundaryY,
                            fdtd::Configuration::Boundary boundaryZ,
                            bool useThinSheets);
    void addZoneSource(double centerX, double centerY, double centerZ,
                       double sizeX, double sizeY, double sizeZ,
                       double frequency);
    source::PlaneWaveSource* addPlaneWaveSource();
    void addGravyWaveSource();
    seq::SingleSeq* addSingleSequencer();
    void addCharacterizer();
    void addRadiationPatternSensor();
    sensor::MaterialSensor* addMaterialSensor();
    sensor::TwoDSlice* addTwoDSliceSensor();
    void addAreaSensor(double centerX, double centerY, double centerZ,
                       double width, double height, box::Constants::Orientation orientation);
    sensor::ArraySensor* addArraySensor();
    void addFarFieldSensor();
    [[nodiscard]] std::vector<int> materialSensorRow(sensor::MaterialSensor* sensor,
                                                     int y) const;
    domain::Material* addMaterial();
    domain::FingerPlate* addFingerPlate();
    std::string getDomainZ(size_t z, const std::map<int, char>& materials);
};


#endif //FDTDLIFE_TESTMODEL_H
