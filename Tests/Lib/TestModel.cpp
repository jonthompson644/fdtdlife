/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "TestModel.h"

// Set the basic model configuration
void TestModel::basicConfiguration(
        double topLeftX, double topLeftY, double topLeftZ,
        double bottomRightX, double bottomRightY, double bottomRightZ,
        double dx, double dy, double dz,
        fdtd::Configuration::Boundary boundaryX,
        fdtd::Configuration::Boundary boundaryY,
        fdtd::Configuration::Boundary boundaryZ,
        bool useThinSheets) {
    // The main configuration
    p_.doPropagationMatrices(false);
    p_.doFdtd(true);
    p_.s(0.5);
    p_.scatteredFieldZoneSize(2);
    p_.p1(box::VectorExpression<double>(topLeftX, topLeftY, topLeftZ));
    p_.p2(box::VectorExpression<double>(bottomRightX, bottomRightY, bottomRightZ));
    p_.dr(box::VectorExpression<double>(dx, dy, dz));
    p_.boundary({boundaryX, boundaryY, boundaryZ});
    p_.boundarySize(40);
    p_.tSize(box::Expression(5e-11));
    p_.numThreadsCfg(0);
    p_.useThinSheetSubcells(useThinSheets);
}

// Add a zone source
void TestModel::addZoneSource(double centerX, double centerY, double centerZ, double sizeX,
                              double sizeY, double sizeZ,
                              double frequency) {

    zoneSource_ = dynamic_cast<source::ZoneSource*>(
            sources_.factory()->make(source::SourceFactory::modeZone, this));
    zoneSource_->frequencies().first(box::Expression(frequency));
    zoneSource_->frequencies().spacing(box::Expression(frequency));
    zoneSource_->frequencies().n(box::Expression(1));
    zoneSource_->amplitude(500.0);
    zoneSource_->azimuth(0.0);
    zoneSource_->elevation(0.0);
    zoneSource_->polarisation(0.0);
    zoneSource_->initialTime(0.0);
    zoneSource_->continuous(true);
    zoneSource_->zoneCenter(box::Vector<double>(centerX, centerY, centerZ));
    zoneSource_->zoneSize(box::Vector<double>(sizeX, sizeY, sizeZ));
}

// Add a gravitational wave source
void TestModel::addGravyWaveSource() {
    gravyWaveSource_ = dynamic_cast<source::GravyWaveSource*>(
            sources_.factory()->make(source::SourceFactory::modeGravyWave, this));
}

// Add a plane wave source
source::PlaneWaveSource* TestModel::addPlaneWaveSource() {
    return dynamic_cast<source::PlaneWaveSource*>(
            sources_.factory()->make(source::SourceFactory::modePlaneWave, this));
}

// Add a single sequencer
seq::SingleSeq* TestModel::addSingleSequencer() {
    auto* result = dynamic_cast<seq::SingleSeq*>(
            sequencers().makeSequencer(seq::Sequencers::Mode::modeSingle).get());
    return result;
}

// Add a characterizer sequencer
void TestModel::addCharacterizer() {
    characterizer_ = dynamic_cast<seq::Characterizer*>(
            sequencers().makeSequencer(seq::Sequencers::Mode::modeCharacterizer).get());
}

// Add a radiation pattern sensorId
void TestModel::addRadiationPatternSensor() {
    radiationPatternSensor_ = dynamic_cast<sensor::RadiationPattern*>(
            sensors_.factory().make(sensor::Sensors::modeRadiationPattern, this));
}

// Add a far field sensorId
void TestModel::addFarFieldSensor() {
    farFieldSensor_ = dynamic_cast<sensor::FarFieldSensor*>(
            sensors_.factory().make(sensor::Sensors::modeFarFieldSensor, this));
}

// Add a material sensorId
sensor::MaterialSensor* TestModel::addMaterialSensor() {
    return dynamic_cast<sensor::MaterialSensor*>(
            sensors_.makeSensor(sensor::Sensors::modeMaterial));
}

// Add a 2D sensor return a pointer to it
sensor::TwoDSlice* TestModel::addTwoDSliceSensor() {
    auto* result = dynamic_cast<sensor::TwoDSlice*>(
            sensors_.makeSensor(sensor::Sensors::modeTwoD));
    return result;
}

// Add a finger plate shape and return a pointer to it
domain::FingerPlate* TestModel::addFingerPlate() {
    auto* result = dynamic_cast<domain::FingerPlate*>(
            d_.makeShape(domain::Domain::shapeModeFingerPlate));
    return result;
}

// Add an area sensorId
void TestModel::addAreaSensor(double centerX, double centerY, double centerZ, double width,
                              double height,
                              box::Constants::Orientation orientation) {
    areaSensor_ = dynamic_cast<sensor::AreaSensor*>(
            sensors_.factory().make(sensor::Sensors::modeArea, this));
    areaSensor_->center(box::Vector<double>(centerX, centerY, centerZ));
    areaSensor_->width(width);
    areaSensor_->height(height);
    areaSensor_->orientation(orientation);
}

// Add an array sensorId
sensor::ArraySensor* TestModel::addArraySensor() {
    return dynamic_cast<sensor::ArraySensor*>(
            sensors_.makeSensor(sensor::Sensors::modeArraySensor));
}

// Return a vector containing the data from a row of the material sensorId
std::vector<int> TestModel::materialSensorRow(sensor::MaterialSensor* sensor,
                                              int y) const {
    std::vector<int> result;
    result.resize((size_t) sensor->nSliceX());
    for(size_t x = 0; x < sensor->nSliceX(); x++) {
        result[x] = (int) sensor->data()[sensor->index(x, y)];
    }
    return result;
}

// Add a material to the model, returning a pointer to it
domain::Material* TestModel::addMaterial() {
    return d()->makeMaterial(domain::Domain::materialModeNormal).get();
}

// Converts a slice of the domain into a string.
// Rows are seperated by new lines.
// The characters represent the material at each x location in the row.
// A '.' always means the default material.
// Other characters are defined by the materials map parameter.
std::string TestModel::getDomainZ(size_t z, const std::map<int, char>& materials) {
    std::string result;
    for(size_t y = 0; y < p_.geometry()->n().y(); y++) {
        for(size_t x = 0; x < p_.geometry()->n().x(); x++) {
            auto* mat = d()->getMaterialAt(x, y, z);
            auto pos = materials.find(mat->index());
            if(pos == materials.end()) {
                result.push_back('.');
            } else {
                result.push_back(pos->second);
            }
        }
        result.push_back('\n');
    }
    return result;
}
