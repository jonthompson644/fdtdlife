/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <TestModel.h>
#include <Box/Constants.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// Conversion of configuration to and from XML
TEST_CASE("FingerPlate_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    auto* metal = model.addMaterial();
    metal->epsilon(box::Expression(8e12));
    // Create a shape with non-default values everywhere
    auto* shape = model.addFingerPlate();
    shape->material(metal->index());
    shape->ignore(true);
    shape->center(box::VectorExpression<double>(1.0, 2.0, 3.0));
    shape->cellSizeX(box::Expression(4.0));
    shape->layerThickness(box::Expression(5.0));
    shape->increment(box::VectorExpression<double>(6.0, 7.0, 8.0));
    shape->duplicate(box::VectorExpression<size_t>(9, 10, 11));
    shape->plateSize(box::Expression(12.0));
    shape->fillMaterial(metal->index());
    shape->fingersLeft(false);
    shape->fingersRight(true);
    shape->fingersTop(false);
    shape->fingersBottom(true);
    shape->minFeatureSize(box::Expression(13.0));
    shape->fingerOffset(box::Expression(14.0));
    // Write the XML
    auto* root = new xml::DomObject("root");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *dynamic_cast<domain::Shape*>(shape));
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Read the XML into a new shape
    auto* check = model.addFingerPlate();
    xml::Reader reader;
    xml::DomDocument dom2("root");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *dynamic_cast<domain::Shape*>(check));
    // Check the configuration
    REQUIRE(check->material() == metal->index());
    REQUIRE(check->ignore());
    REQUIRE(check->center() == box::VectorExpression<double>(1.0, 2.0, 3.0));
    REQUIRE(check->cellSizeX() == box::Expression(4.0));
    REQUIRE(check->layerThickness() == box::Expression(5.0));
    REQUIRE(check->increment() == box::VectorExpression<double>(6.0, 7.0, 8.0));
    REQUIRE(check->duplicate() == box::VectorExpression<size_t>(9, 10, 11));
    REQUIRE(check->plateSize() == box::Expression(12.0));
    REQUIRE(check->fillMaterial() == metal->index());
    REQUIRE(!check->fingersLeft());
    REQUIRE(check->fingersRight());
    REQUIRE(!check->fingersTop());
    REQUIRE(check->fingersBottom());
    REQUIRE(check->minFeatureSize() == box::Expression(13.0));
    REQUIRE(check->fingerOffset() == box::Expression(14.0));
}

// The filling of the domain from the shape
// with a zero offset
TEST_CASE("FingerPlate_DomainFilling") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, 0,
                             +0.00008, +0.00008, +0.002,
                             0.00001, 0.00001, 0.00001,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::reflecting,
                             false);
    model.p()->scatteredFieldZoneSize(0);
    model.addSingleSequencer();
    auto* metal = model.addMaterial();
    metal->epsilon(box::Expression(8e12));
    auto* shape = model.addFingerPlate();
    shape->material(metal->index());
    shape->cellSizeX(box::Expression(160e-6));
    shape->increment(box::VectorExpression<double>(160e-6, 160e-6, 0.0));
    shape->minFeatureSize(box::Expression(12.5));
    // 87.5% plate
    shape->plateSize(box::Expression(87.5));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            "................\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            "................\n");
    // 75.00% plate
    shape->plateSize(box::Expression(75.00));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            "................\n"
            "................\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "..************..\n"
            "................\n"
            "................\n");
    // 12.50% plate
    shape->plateSize(box::Expression(12.50));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            ".......**.......\n"
            ".......**.......\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n"
            "................\n");
    // 100.0% plate
    shape->plateSize(box::Expression(100.0));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            ".....**.........\n"
            ".**************.\n"
            ".**************.\n"
            ".************...\n"
            ".************...\n"
            "*************..*\n"
            "*************..*\n"
            ".************...\n"
            ".************...\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**......******.\n"
            ".**......******.\n"
            ".....**.........\n");
}

// The filling of the domain from the shape
// with a 50% offset
TEST_CASE("FingerPlate_DomainFillingOffset") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, 0,
                             +0.00008, +0.00008, +0.002,
                             0.00001, 0.00001, 0.00001,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::reflecting,
                             false);
    model.p()->scatteredFieldZoneSize(0);
    model.addSingleSequencer();
    auto* metal = model.addMaterial();
    metal->epsilon(box::Expression(8e12));
    auto* shape = model.addFingerPlate();
    shape->material(metal->index());
    shape->cellSizeX(box::Expression(160e-6));
    shape->increment(box::VectorExpression<double>(160e-6, 160e-6, 0.0));
    shape->minFeatureSize(box::Expression(12.5));
    shape->fingerOffset(box::Expression(50.0));
    // 87.5% plate
    shape->plateSize(box::Expression(87.5));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            "................\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            "................\n");
    // 100.0% plate
    shape->plateSize(box::Expression(100.0));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            ".......**.......\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".************...\n"
            ".************...\n"
            "*************..*\n"
            "*************..*\n"
            ".************...\n"
            ".************...\n"
            ".**************.\n"
            ".**************.\n"
            ".****......****.\n"
            ".****......****.\n"
            ".......**.......\n");
}

// The filling of the domain from the shape, right and bottom
// with a 50% offset
TEST_CASE("FingerPlate_DomainFillingOffsetRightBottom") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, 0,
                             +0.00008, +0.00008, +0.002,
                             0.00001, 0.00001, 0.00001,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::reflecting,
                             false);
    model.p()->scatteredFieldZoneSize(0);
    model.addSingleSequencer();
    auto* metal = model.addMaterial();
    metal->epsilon(box::Expression(8e12));
    auto* shape = model.addFingerPlate();
    shape->material(metal->index());
    shape->cellSizeX(box::Expression(160e-6));
    shape->increment(box::VectorExpression<double>(160e-6, 160e-6, 0.0));
    shape->minFeatureSize(box::Expression(12.5));
    shape->fingerOffset(box::Expression(50.0));
    shape->fingersBottom(true);
    shape->fingersTop(false);
    shape->fingersRight(true);
    shape->fingersLeft(false);
    // 87.5% plate
    shape->plateSize(box::Expression(87.5));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            "................\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            "................\n");
    // 100.0% plate
    shape->plateSize(box::Expression(100.0));
    model.initialise();
    REQUIRE(model.getDomainZ(0, {{metal->index(), '*'}}) ==
            ".......**.......\n"
            ".****......****.\n"
            ".****......****.\n"
            ".**************.\n"
            ".**************.\n"
            "...************.\n"
            "...************.\n"
            "*..*************\n"
            "*..*************\n"
            "...************.\n"
            "...************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".**************.\n"
            ".......**.......\n");
}
