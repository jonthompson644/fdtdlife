/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <TestModel.h>
#include <Xml/DomDocument.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// Adding and removing tables
TEST_CASE("ParameterTables_AddRemoveTable") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    REQUIRE(model.d()->parameterTables().size() == 0);
    auto* table = model.d()->parameterTables().make();
    REQUIRE(model.d()->parameterTables().size() == 1);
    REQUIRE(table != nullptr);
    // Check parameter table contents
    table->name("One");
    table->unitCell(2.0);
    table->layerSpacing(3.0);
    table->incidenceAngle(4.0);
    table->patchRatio(5.0);
    REQUIRE(table->identifier() == 0);
    REQUIRE(table->mode() == domain::ParameterTables::modeParameterTable);
    REQUIRE(table->name() == "One");
    REQUIRE(table->unitCell() == 2.0_a);
    REQUIRE(table->layerSpacing() == 3.0_a);
    REQUIRE(table->incidenceAngle() == 4.0_a);
    REQUIRE(table->patchRatio() == 5.0_a);
    // Check iterators
    auto pos = model.d()->parameterTables().begin();
    REQUIRE(pos->get()->identifier() == 0);
    pos++;
    REQUIRE(pos == model.d()->parameterTables().end());
    // Remove the table
    model.d()->parameterTables().remove(table);
    REQUIRE(model.d()->parameterTables().size() == 0);
    REQUIRE(model.d()->parameterTables().begin() == model.d()->parameterTables().end());
}

// Adding and removing curves from a table
TEST_CASE("ParameterTables_AddRemoveCurve") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    auto* table = model.d()->parameterTables().make();
    REQUIRE(table != nullptr);
    // Add a curve
    REQUIRE(table->size() == 0);
    auto* curve = table->make();
    REQUIRE(curve != nullptr);
    REQUIRE(table->size() == 1);
    // Check curve contents
    curve->which(domain::ParameterCurve::Which::permeability);
    REQUIRE(curve->identifier() == 0);
    REQUIRE(curve->mode() == domain::ParameterTable::modeParameterCurve);
    REQUIRE(curve->which() == domain::ParameterCurve::Which::permeability);
    // Check iterators
    auto pos = table->begin();
    REQUIRE(pos->get()->identifier() == 0);
    pos++;
    REQUIRE(pos == table->end());
    // Remove the curve
    table->remove(curve);
    REQUIRE(table->size() == 0);
    REQUIRE(table->begin() == table->end());
}

// Adding and removing points from a curve
TEST_CASE("ParameterTables_AddRemovePoint") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    auto* table = model.d()->parameterTables().make();
    REQUIRE(table != nullptr);
    // Add a curve
    auto* curve = table->make();
    REQUIRE(curve != nullptr);
    // Add a point
    REQUIRE(curve->size() == 0);
    auto* point = curve->make();
    REQUIRE(point != nullptr);
    REQUIRE(curve->size() == 1);
    // Check point contents
    point->frequency(1.0);
    point->value({2.0,3.0});
    REQUIRE(point->identifier() == 0);
    REQUIRE(point->mode() == domain::ParameterCurve::modeParameterPoint);
    REQUIRE(point->frequency() == 1.0_a);
    REQUIRE(point->value().real() == 2.0_a);
    REQUIRE(point->value().imag() == 3.0_a);
    // Check iterators
    auto pos = curve->begin();
    REQUIRE(pos->get()->identifier() == 0);
    pos++;
    REQUIRE(pos == curve->end());
    // Remove the point
    curve->remove(point);
    REQUIRE(curve->size() == 0);
    REQUIRE(curve->begin() == curve->end());
}

// Read and writing XML
TEST_CASE("ParameterTables_XmlSerialisation") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    auto* table = model.d()->parameterTables().make();
    REQUIRE(table != nullptr);
    table->name("One");
    table->unitCell(2.0);
    table->layerSpacing(3.0);
    table->incidenceAngle(4.0);
    table->patchRatio(5.0);
    // Add a curve
    auto* curve = table->make();
    REQUIRE(curve != nullptr);
    curve->which(domain::ParameterCurve::Which::permeability);
    // Add a point
    auto* point = curve->make();
    REQUIRE(point != nullptr);
    point->frequency(1.0);
    point->value({2.0,3.0});
    // Write the XML
    auto* root = new xml::DomObject("root");
    xml::DomDocument dom1(root);
    REQUIRE_NOTHROW(*root << *model.d());
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom1, text);
    // Clear the parameter tables
    model.d()->clear();
    // Read the XML
    xml::Reader reader;
    xml::DomDocument dom2("root");
    reader.readString(&dom2, text);
    REQUIRE_NOTHROW(*dom2.getObject() >> *model.d());
    // Check the table
    REQUIRE(model.d()->parameterTables().size() == 1);
    table = model.d()->parameterTables().find(0);
    REQUIRE(table != nullptr);
    REQUIRE(table->identifier() == 0);
    REQUIRE(table->mode() == domain::ParameterTables::modeParameterTable);
    REQUIRE(table->name() == "One");
    REQUIRE(table->unitCell() == 2.0_a);
    REQUIRE(table->layerSpacing() == 3.0_a);
    REQUIRE(table->incidenceAngle() == 4.0_a);
    REQUIRE(table->patchRatio() == 5.0_a);
    // Check the curve
    REQUIRE(table->size() == 1);
    curve = table->find(0);
    REQUIRE(curve != nullptr);
    REQUIRE(curve->identifier() == 0);
    REQUIRE(curve->mode() == domain::ParameterTable::modeParameterCurve);
    REQUIRE(curve->which() == domain::ParameterCurve::Which::permeability);
    // Check the point
    REQUIRE(curve->size() == 1);
    point = curve->find(0);
    REQUIRE(point != nullptr);
    REQUIRE(point->identifier() == 0);
    REQUIRE(point->mode() == domain::ParameterCurve::modeParameterPoint);
    REQUIRE(point->frequency() == 1.0_a);
    REQUIRE(point->value().real() == 2.0_a);
    REQUIRE(point->value().imag() == 3.0_a);
}

// Sorting the points into frequency order
TEST_CASE("ParameterTables_SortPoints") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    auto* table = model.d()->parameterTables().make();
    REQUIRE(table != nullptr);
    // Add a curve
    auto* curve = table->make();
    REQUIRE(curve != nullptr);
    // Add some points
    REQUIRE(curve->size() == 0);
    auto* point1 = curve->make();
    auto* point2 = curve->make();
    auto* point3 = curve->make();
    auto* point4 = curve->make();
    REQUIRE(curve->size() == 4);
    // Set point frequencies
    point1->frequency(25);
    point2->frequency(12);
    point3->frequency(37);
    point4->frequency(6);
    // Sort and check order
    curve->sort();
    auto pos = curve->begin();
    REQUIRE(pos->get() == point4);
    pos++;
    REQUIRE(pos->get() == point2);
    pos++;
    REQUIRE(pos->get() == point1);
    pos++;
    REQUIRE(pos->get() == point3);
    pos++;
    REQUIRE(pos == curve->end());
}

// Clearing the various lists
TEST_CASE("ParameterTables_Clear") {
    // The model
    TestModel model;
    model.clear();
    model.basicConfiguration(-0.00008, -0.00008, -0.00002,
                             +0.00008, +0.00008, +0.002,
                             0.000005, 0.000005, 0.000005,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing,
                             false);
    // Create a parameter table
    auto* table = model.d()->parameterTables().make();
    REQUIRE(table != nullptr);
    // Add a curve
    auto* curve = table->make();
    REQUIRE(curve != nullptr);
    // Add a point
    auto* point = curve->make();
    REQUIRE(point != nullptr);
    // Clear them
    curve->clear();
    REQUIRE(curve->size() == 0);
    table->clear();
    REQUIRE(table->size() == 0);
    model.d()->parameterTables().clear();
    REQUIRE(model.d()->parameterTables().size() == 0);
}
