/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>

using namespace Catch::literals;

// Handle the toolbar new button
TEST_CASE("Application_ToolbarNewNoSave") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the new button
    appRequests.expectModalDialog("File New", Gtk::RESPONSE_NO);
    clickToolButton("New", app->mainWindow());
    appRequests.checkModalDialog();
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
}

// Handle the toolbar new button
TEST_CASE("Application_ToolbarNewSave") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the new button
    appRequests.expectModalDialog("File New", Gtk::RESPONSE_YES);
    appRequests.expectFileModalDialog("File New", "./aa0.fdtd",
            Gtk::RESPONSE_ACCEPT);
    clickToolButton("New", app->mainWindow());
    appRequests.checkModalDialog();
    appRequests.checkModalDialog();
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
}

// Handle the toolbar new button cancel
TEST_CASE("Application_ToolbarNewCancel") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the new button
    appRequests.expectModalDialog("File New", Gtk::RESPONSE_CANCEL);
    clickToolButton("New", app->mainWindow());
    appRequests.checkModalDialog();
    // Check that the panels are unchanged
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
}

// Handle the toolbar new button, cancel on the save file dialog
TEST_CASE("Application_ToolbarNewSaveCancel") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the new button
    appRequests.expectModalDialog("File New", Gtk::RESPONSE_YES);
    appRequests.expectFileModalDialog("File New", "./aa0.fdtd",
                                      Gtk::RESPONSE_CANCEL);
    clickToolButton("New", app->mainWindow());
    appRequests.checkModalDialog();
    appRequests.checkModalDialog();
    // Check that the panels are unchanged
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
}

// Handle the toolbar save button
TEST_CASE("Application_ToolbarSave") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the save button
    appRequests.expectFileModalDialog("Save File As", "./aa0.fdtd",
                                      Gtk::RESPONSE_ACCEPT);
    clickToolButton("Save", app->mainWindow());
    appRequests.checkModalDialog();
    // Check that the panels are unchanged
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
    // We should be able to do a new now without being asked to save
    clickToolButton("New", app->mainWindow());
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
}

// Handle the toolbar save button cancel
TEST_CASE("Application_ToolbarSaveCancel") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the save button
    appRequests.expectFileModalDialog("Save File As", "./aa0.fdtd",
                                      Gtk::RESPONSE_CANCEL);
    clickToolButton("Save", app->mainWindow());
    appRequests.checkModalDialog();
    // Check that the panels are unchanged
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
    // We should not be able to do a new now without being asked to save
    appRequests.expectModalDialog("File New", Gtk::RESPONSE_NO);
    clickToolButton("New", app->mainWindow());
    appRequests.checkModalDialog();
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
}

// Handle the toolbar open button
TEST_CASE("Application_ToolbarOpen") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the save button
    appRequests.expectFileModalDialog("Save File As", "./aa0.fdtd",
                                      Gtk::RESPONSE_ACCEPT);
    clickToolButton("Save", app->mainWindow());
    appRequests.checkModalDialog();
    // Clear out the model
    clickToolButton("New", app->mainWindow());
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
    // Now read the file back in
    appRequests.expectFileModalDialog("Open File", "./aa0.fdtd",
                                      Gtk::RESPONSE_ACCEPT);
    clickToolMenuButton("Open", app->mainWindow());
    appRequests.checkModalDialog();
    // Check the panels are back to before the new
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
}

// Handle the toolbar open button cancel
TEST_CASE("Application_ToolbarOpenCancel") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the save button
    appRequests.expectFileModalDialog("Save File As", "./aa0.fdtd",
                                      Gtk::RESPONSE_ACCEPT);
    clickToolButton("Save", app->mainWindow());
    appRequests.checkModalDialog();
    // Clear out the model
    clickToolButton("New", app->mainWindow());
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
    // Now read the file back in
    appRequests.expectFileModalDialog("Open File", "./aa0.fdtd",
                                      Gtk::RESPONSE_CANCEL);
    clickToolMenuButton("Open", app->mainWindow());
    appRequests.checkModalDialog();
    // Check the panels are still as after new
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
}

// Handle the toolbar open recent file button
TEST_CASE("Application_ToolbarOpenRecent") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Add some stuff in the model
    clickMenuButton("New Source", 0, app->mainWindow().sourcesPage());
    clickMenuButton("New Material", 0, app->mainWindow().materialsPage());
    clickMenuButton("New Shape", 0, app->mainWindow().shapesPage());
    clickMenuButton("New Sensor", 1, app->mainWindow().sensorsPage());
    clickMenuButton("New Analyser", 0, app->mainWindow().analysersPage());
    testButton("New View", app->mainWindow().viewsPage());
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    testButton("New Variable", app->mainWindow().variablesPage());
    // Press the save button
    appRequests.expectFileModalDialog("Save File As", "./test001.fdtd",
                                      Gtk::RESPONSE_ACCEPT);
    clickToolButton("Save", app->mainWindow());
    appRequests.checkModalDialog();
    // Clear out the model
    clickToolButton("New", app->mainWindow());
    // Check that the panels are initialised
    testListWidgetEmpty("Sources", app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 2U, app->mainWindow().materialsPage());
    testListWidgetEmpty("Shapes", app->mainWindow().shapesPage());
    testListWidgetEmpty("Sensors", app->mainWindow().sensorsPage());
    testListWidgetEmpty("Analysers", app->mainWindow().analysersPage());
    testListWidgetEmpty("Views", app->mainWindow().viewsPage());
    testListWidgetEmpty("Sequencers", app->mainWindow().sequencersPage());
    testListWidgetEmpty("listview", app->mainWindow().variablesPage());
    // Now read the file back in using the recent file list
    clickToolMenuButton("Open", "test001", app->mainWindow());
    // Check the panels are back to before the new
    processEvents(UNTIL_CONTINUE);
    testListWidgetNumRows("Sources", 1U, app->mainWindow().sourcesPage());
    testListWidgetNumRows("Materials", 3U, app->mainWindow().materialsPage());
    testListWidgetNumRows("Shapes", 1U, app->mainWindow().shapesPage());
    testListWidgetNumRows("Sensors", 1U, app->mainWindow().sensorsPage());
    testListWidgetNumRows("Analysers", 1U, app->mainWindow().analysersPage());
    testListWidgetNumRows("Views", 1U, app->mainWindow().viewsPage());
    testListWidgetNumRows("Sequencers", 1U, app->mainWindow().sequencersPage());
    testListWidgetNumRows("listview", 1U, app->mainWindow().variablesPage());
}

// Handle the toolbar run/stop commands
TEST_CASE("Application_ToolbarRunStop") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // We need a single job sequencer in the model
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    // Press the run button
    clickToolButton("Run", app->mainWindow());
    // Wait for three seconds to see some steps happen
    processEvents(3000);
    clickToolButton("Stop", app->mainWindow());
    // Check the number of steps is correct
    int numSteps = app->model().p()->timeStep();
    REQUIRE(numSteps > 0);
    testEntry("Cur Time Step", app->model().p(), &fdtd::Configuration::timeStep,
              app->mainWindow().configurationPage(),
              box::Utility::fromInt(numSteps));
    // Run to continue
    clickToolButton("Run", app->mainWindow());
    // Wait for three seconds to see some steps happen
    processEvents(3000);
    clickToolButton("Stop", app->mainWindow());
    // Check the number of steps is correct
    REQUIRE(app->model().p()->timeStep() > numSteps);
    numSteps = app->model().p()->timeStep();
    testEntry("Cur Time Step", app->model().p(), &fdtd::Configuration::timeStep,
              app->mainWindow().configurationPage(),
              box::Utility::fromInt(numSteps));
    // Reinitialise
    clickToolButton("Initialise", app->mainWindow());
    testEntry("Cur Time Step", app->model().p(), &fdtd::Configuration::timeStep,
              app->mainWindow().configurationPage(),
              "0");
    // And run again
    clickToolButton("Run", app->mainWindow());
    // Wait for three seconds to see some steps happen
    processEvents(3000);
    clickToolButton("Stop", app->mainWindow());
    // Check the number of steps is correct
    numSteps = app->model().p()->timeStep();
    REQUIRE(numSteps > 0);
    testEntry("Cur Time Step", app->model().p(), &fdtd::Configuration::timeStep,
              app->mainWindow().configurationPage(),
              box::Utility::fromInt(numSteps));
}

// Handle the application quit request
TEST_CASE("Application_Quit") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    // Make a change to the model
    clickMenuButton("New Sequencer", 0, app->mainWindow().sequencersPage());
    // Now press the quit button and cancel
    appRequests.expectModalDialog("Quit", Gtk::RESPONSE_CANCEL);
    app->mainWindow().close();
    processEvents(0);
    appRequests.checkModalDialog();
    // Now press the quit button and no
    appRequests.expectModalDialog("Quit", Gtk::RESPONSE_NO);
    app->mainWindow().close();
    processEvents(0);
    appRequests.checkModalDialog();
}

