/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <GtkGui/TwoDSliceViewItem.h>
#include <GtkGui/MaterialViewItem.h>
#include <GtkGui/DataSeriesViewItem.h>
#include <GtkGui/PhaseShiftViewItem.h>
#include <GtkGui/AttenuationViewItem.h>
#include <Source/PlaneWaveSource.h>
#include <Domain/FingerPlate.h>
#include <Sensor/ArraySensor.h>

using namespace Catch::literals;

// Creating and destroying views
TEST_CASE("ViewsDlg_NewAndDeleteView") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& vwsPage = app->mainWindow().viewsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add three views
    testButton("New View", vwsPage);
    testButton("New View", vwsPage);
    testButton("New View", vwsPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      vwsPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      vwsPage);
    testListWidgetRow("Views", 2, {"2", "View 2", "View"},
                      vwsPage);
    testListWidgetRowSelected("Views", 2, true, vwsPage);
    // Edit the name of the last view
    int id = getListWidgetCell<int>("Views", 2, 0, vwsPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    REQUIRE(view->get_title() == "View 2");
    selectListWidgetRow("Views", 2, true, vwsPage);
    setListWidgetCell<const std::string&>("Views", 2, 1,
                                          "Changed Name", vwsPage);
    REQUIRE(view->name() == "Changed Name");
    REQUIRE(view->get_title() == "Changed Name");
    setListWidgetCell<const std::string&>("Views", 2, 1,
                                          "View 2", vwsPage);
    REQUIRE(view->name() == "View 2");
    REQUIRE(view->get_title() == "View 2");
    // Cancel deletion of the view
    selectListWidgetRow("Views", 2, true, vwsPage);
    appRequests.expectModalDialog("Delete View", Gtk::RESPONSE_CANCEL);
    clickButton("Delete View", vwsPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      vwsPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      vwsPage);
    testListWidgetRow("Views", 2, {"2", "View 2", "View"},
                      vwsPage);
    testListWidgetRowSelected("Views", 2, true, vwsPage);
    // Delete the view
    selectListWidgetRow("Views", 1, true, vwsPage);
    appRequests.expectModalDialog("Delete View", Gtk::RESPONSE_OK);
    clickButton("Delete View", vwsPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      vwsPage);
    testListWidgetRow("Views", 1, {"2", "View 2", "View"},
                      vwsPage);
    testListWidgetRowSelected("Views", 1, true, vwsPage);
}

// View configuration
TEST_CASE("ViewsDlg_ViewParameters") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& vwsPage = app->mainWindow().viewsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", vwsPage);
    testButton("New View", vwsPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      vwsPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      vwsPage);
    // Select the first
    selectListWidgetRow("Views", 0, true, vwsPage);
    int id = getListWidgetCell<int>("Views", 0, 0, vwsPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Check the sub dialog
    testEntry("Min Display Rate (ms)", view,
              &ViewFrame::frameDisplayPeriod, vwsPage,
              "2000", "5");
    testCheckButton("Display All Frames", view,
                    &ViewFrame::displayAllFrames, vwsPage,
                    false, true);
    // Switch to another view and back
    selectListWidgetRow("Views", 1, true, vwsPage);
    selectListWidgetRow("Views", 0, true, vwsPage);
    // Is the page still ok
    testEntry("Min Display Rate (ms)", view,
              &ViewFrame::frameDisplayPeriod, vwsPage,
              "5");
    testCheckButton("Display All Frames", view,
                    &ViewFrame::displayAllFrames, vwsPage,
                    true);
}

// View item selection
TEST_CASE("ViewsDlg_ViewItems") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add two 2D slice sensors
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 0, sensorPage);
    clickMenuButton("New Sensor", 0, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "2D Slice"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "2D Slice"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view0 = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view0 != nullptr);
    id = getListWidgetCell<int>("Views", 1, 0, viewPage);
    auto* view1 = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view1 != nullptr);
    // Are the sensorId data sources listed?
    testListWidgetRow("Sensor Data Sources", 0, {"0", "Sensor 0:twoDSlice"},
                      viewPage);
    testListWidgetRow("Sensor Data Sources", 1, {"0", "Sensor 1:twoDSlice"},
                      viewPage);
    // Enable a sensorId data item
    REQUIRE(view0->viewForSpec(1, "twoDSlice") == nullptr);
    setListWidgetCell("Sensor Data Sources", 1, 0, true, viewPage);
    REQUIRE(view0->viewForSpec(1, "twoDSlice") != nullptr);
    // Switch to another view and enable a sensorId data item there
    selectListWidgetRow("Views", 1, true, viewPage);
    REQUIRE(view1->viewForSpec(0, "twoDSlice") == nullptr);
    setListWidgetCell("Sensor Data Sources", 0, 0, true, viewPage);
    REQUIRE(view1->viewForSpec(0, "twoDSlice") != nullptr);
    // Switch back, is the list still correct?
    selectListWidgetRow("Views", 0, true, viewPage);
    testListWidgetRow("Sensor Data Sources", 0, {"0", "Sensor 0:twoDSlice"},
                      viewPage);
    testListWidgetRow("Sensor Data Sources", 1, {"1", "Sensor 1:twoDSlice"},
                      viewPage);
    // Now remove the item
    REQUIRE(view0->viewForSpec(1, "twoDSlice") != nullptr);
    setListWidgetCell("Sensor Data Sources", 1, 0, false, viewPage);
    REQUIRE(view0->viewForSpec(1, "twoDSlice") == nullptr);
}

// Two D slice sub-dialog
TEST_CASE("ViewsDlg_TwoDSliceItemDlg") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add a 2D slice sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 0, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "2D Slice"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first view
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Enable a sensorId data item
    setListWidgetCell("Sensor Data Sources", 0, 0, true, viewPage);
    auto viewItem = dynamic_cast<TwoDSliceViewItem*>(
            view->viewForSpec(0, "twoDSlice"));
    REQUIRE(viewItem != nullptr);
    // Test the configuration data
    testComboBoxText("Colour Map", viewItem, &TwoDSliceViewItem::colourMap,
                     viewPage,
                     TwoDSliceWidget::ColourMap::extKindlemann,
                     TwoDSliceWidget::ColourMap::divergingBY);
    testEntry("Display Max", viewItem, &TwoDSliceViewItem::displayMax, viewPage,
              "1", "2");
    // Switch to another view and back, is the configuration unchanged?
    selectListWidgetRow("Views", 1, true, viewPage);
    selectListWidgetRow("Views", 0, true, viewPage);
    selectListWidgetRow("Sensor Data Sources", 0, true, viewPage);
    testComboBoxText("Colour Map", viewItem, &TwoDSliceViewItem::colourMap,
                     viewPage,
                     TwoDSliceWidget::ColourMap::divergingBY);
    testEntry("Display Max", viewItem, &TwoDSliceViewItem::displayMax, viewPage,
              "2");
}

// Data series sub-dialog
TEST_CASE("ViewsDlg_DataSeriesItemDlg") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add an array sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first view
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Enable a sensorId data item
    setListWidgetCell("Sensor Data Sources", 6, 0, true, viewPage);
    auto viewItem = dynamic_cast<DataSeriesViewItem*>(
            view->viewForSpec(0, "FrontTimeSeries"));
    REQUIRE(viewItem != nullptr);
    // Test the configuration data
    testEntry("X Max", viewItem, &DataSeriesViewItem::xMax, viewPage,
              "0", "1");
    testEntry("X Min", viewItem, &DataSeriesViewItem::xMin, viewPage,
              "0", "2");
    testCheckButton("X Automatic", viewItem, &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    true, false);
    testEntry("Y Max", viewItem, &DataSeriesViewItem::yMax, viewPage,
              "0", "3");
    testEntry("Y Min", viewItem, &DataSeriesViewItem::yMin, viewPage,
              "0", "4");
    testCheckButton("Y Automatic", viewItem, &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    true, false);
    // Switch to another view and back, is the configuration unchanged?
    selectListWidgetRow("Views", 1, true, viewPage);
    selectListWidgetRow("Views", 0, true, viewPage);
    selectListWidgetRow("Sensor Data Sources", 6, true, viewPage);
    testEntry("X Max", viewItem, &DataSeriesViewItem::xMax, viewPage,
              "1");
    testEntry("X Min", viewItem, &DataSeriesViewItem::xMin, viewPage,
              "2");
    testCheckButton("X Automatic", viewItem, &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    false);
    testEntry("Y Max", viewItem, &DataSeriesViewItem::yMax, viewPage,
              "3");
    testEntry("Y Min", viewItem, &DataSeriesViewItem::yMin, viewPage,
              "4");
    testCheckButton("Y Automatic", viewItem, &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    false);
}

// Attenuation sub-dialog
TEST_CASE("ViewsDlg_AttenuationItemDlg") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add an array sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first view
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Enable a sensorId data item
    setListWidgetCell("Sensor Data Sources", 2, 0, true, viewPage);
    auto viewItem = dynamic_cast<AttenuationViewItem*>(
            view->viewForSpec(0, "FrontAttenuation"));
    REQUIRE(viewItem != nullptr);
    // Test the configuration data
    testEntry("X Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMax, viewPage,
              "0", "1");
    testEntry("X Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMin, viewPage,
              "0", "2");
    testCheckButton("X Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    true, false);
    testComboBoxText("X Scale Type", dynamic_cast<PhaseShiftViewItem*>(viewItem),
                     &PhaseShiftViewItem::xScaleType,
                     viewPage,
                     PhaseShiftViewItem::XScaleType::frequency,
                     PhaseShiftViewItem::XScaleType::waveNumber);
    testEntry("Y Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMax, viewPage,
              "0", "3");
    testEntry("Y Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMin, viewPage,
              "0", "4");
    testCheckButton("Y Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    true, false);
    testComboBoxText("Y Scale Type", viewItem, &AttenuationViewItem::yScaleType,
                     viewPage,
                     AttenuationViewItem::YScaleType::power,
                     AttenuationViewItem::YScaleType::decibels);
    // Switch to another view and back, is the configuration unchanged?
    selectListWidgetRow("Views", 1, true, viewPage);
    selectListWidgetRow("Views", 0, true, viewPage);
    selectListWidgetRow("Sensor Data Sources", 2, true, viewPage);
    testEntry("X Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMax, viewPage,
              "1");
    testEntry("X Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMin, viewPage,
              "2");
    testCheckButton("X Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    false);
    testComboBoxText("X Scale Type", dynamic_cast<PhaseShiftViewItem*>(viewItem),
                     &PhaseShiftViewItem::xScaleType,
                     viewPage,
                     PhaseShiftViewItem::XScaleType::waveNumber);
    testEntry("Y Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMax, viewPage,
              "3");
    testEntry("Y Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMin, viewPage,
              "4");
    testCheckButton("Y Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    false);
    testComboBoxText("Y Scale Type", viewItem, &AttenuationViewItem::yScaleType,
                     viewPage,
                     AttenuationViewItem::YScaleType::decibels);
}

// Phase Shift sub-dialog
TEST_CASE("ViewsDlg_PhaseShiftItemDlg") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add an array sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first view
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Enable a sensorId data item
    setListWidgetCell("Sensor Data Sources", 3, 0, true, viewPage);
    auto viewItem = dynamic_cast<PhaseShiftViewItem*>(
            view->viewForSpec(0, "FrontPhaseShift"));
    REQUIRE(viewItem != nullptr);
    // Test the configuration data
    testEntry("X Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMax, viewPage,
              "0", "1");
    testEntry("X Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMin, viewPage,
              "0", "2");
    testCheckButton("X Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    true, false);
    testComboBoxText("X Scale Type", viewItem,
                     &PhaseShiftViewItem::xScaleType,
                     viewPage,
                     PhaseShiftViewItem::XScaleType::frequency,
                     PhaseShiftViewItem::XScaleType::waveNumber);
    testEntry("Y Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMax, viewPage,
              "0", "3");
    testEntry("Y Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMin, viewPage,
              "0", "4");
    testCheckButton("Y Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    true, false);
    // Switch to another view and back, is the configuration unchanged?
    selectListWidgetRow("Views", 1, true, viewPage);
    selectListWidgetRow("Views", 0, true, viewPage);
    selectListWidgetRow("Sensor Data Sources", 3, true, viewPage);
    testEntry("X Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMax, viewPage,
              "1");
    testEntry("X Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::xMin, viewPage,
              "2");
    testCheckButton("X Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::xAutomatic,
                    viewPage,
                    false);
    testComboBoxText("X Scale Type", viewItem,
                     &PhaseShiftViewItem::xScaleType,
                     viewPage,
                     PhaseShiftViewItem::XScaleType::waveNumber);
    testEntry("Y Max", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMax, viewPage,
              "3");
    testEntry("Y Min", dynamic_cast<DataSeriesViewItem*>(viewItem),
              &DataSeriesViewItem::yMin, viewPage,
              "4");
    testCheckButton("Y Automatic", dynamic_cast<DataSeriesViewItem*>(viewItem),
                    &DataSeriesViewItem::yAutomatic,
                    viewPage,
                    false);
}

// Material slice sub-dialog
TEST_CASE("ViewsDlg_MaterialItemDlg") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& viewPage = app->mainWindow().viewsPage();
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Add two views
    testButton("New View", viewPage);
    testButton("New View", viewPage);
    testListWidgetRow("Views", 0, {"0", "View 0", "View"},
                      viewPage);
    testListWidgetRow("Views", 1, {"1", "View 1", "View"},
                      viewPage);
    // Add a material sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 2, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "2D Material"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 6);
    // Select the first view
    selectListWidgetRow("Views", 0, true, viewPage);
    int id = getListWidgetCell<int>("Views", 0, 0, viewPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    // Enable a sensorId data item
    setListWidgetCell("Sensor Data Sources", 1, 0, true, viewPage);
    auto viewItem = dynamic_cast<MaterialViewItem*>(
            view->viewForSpec(0, "material"));
    REQUIRE(viewItem != nullptr);
}

// Check views are laid out correctly and that data is display by them
TEST_CASE("ViewsDlg_LayoutAndContents") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& varPage = app->mainWindow().variablesPage();
    auto& cfgPage = app->mainWindow().configurationPage();
    auto& srcPage = app->mainWindow().sourcesPage();
    auto& matPage = app->mainWindow().materialsPage();
    auto& shpPage = app->mainWindow().shapesPage();
    auto& ssrPage = app->mainWindow().sensorsPage();
    auto& vwsPage = app->mainWindow().viewsPage();
    auto& seqPage = app->mainWindow().sequencersPage();
    // Set up the variables
    selectNotebookItem(&app->mainWindow().sections(), 8);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 0, 1,
                                          "g", varPage);
    setListWidgetCell<const std::string&>("listview", 0, 2,
                                          "160e-6", varPage);
    setListWidgetCell<const std::string&>("listview", 0, 4,
                                          "Unit cell", varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 1,
                                          "nFdtd", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 2,
                                          "16", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 4,
                                          "Number of FDTD cells per unit cell",
                                          varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 2, 1,
                                          "p", varPage);
    setListWidgetCell<const std::string&>("listview", 2, 2,
                                          "70", varPage);
    setListWidgetCell<const std::string&>("listview", 2, 4,
                                          "Patch ratio", varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 3, 1,
                                          "d", varPage);
    setListWidgetCell<const std::string&>("listview", 3, 2,
                                          "10e-6/g", varPage);
    setListWidgetCell<const std::string&>("listview", 3, 4,
                                          "Minimum feature size", varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 4, 1,
                                          "f1", varPage);
    setListWidgetCell<const std::string&>("listview", 4, 2,
                                          "16e9", varPage);
    setListWidgetCell<const std::string&>("listview", 4, 4,
                                          "First frequency", varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 5, 1,
                                          "dr", varPage);
    setListWidgetCell<const std::string&>("listview", 5, 2,
                                          "g/nFdtd", varPage);
    setListWidgetCell<const std::string&>("listview", 5, 4,
                                          "Size of an FDTD cell", varPage);
    testButton("New Variable", varPage);
    setListWidgetCell<const std::string&>("listview", 6, 1,
                                          "nf", varPage);
    setListWidgetCell<const std::string&>("listview", 6, 2,
                                          "25", varPage);
    setListWidgetCell<const std::string&>("listview", 6, 4,
                                          "Number of frequencies", varPage);
    // Fill in the main configuration page
    selectNotebookItem(&app->mainWindow().sections(), 0);
    auto* p = app->model().p();
    testEntry("Bottom Corner x,y,z (m)", p, &fdtd::Configuration::p1, cfgPage,
              {"-0.4e-3", "-0.4e-3", "-0.4e-3"},
              {"-g/2", "-g/2", "-8*dr"});
    testEntry("Top Corner x,y,z (m)", p, &fdtd::Configuration::p2, cfgPage,
              {"0.4e-3", "0.4e-3", "0.4e-3"},
              {"g/2", "g/2", "0.00025"});
    testEntry("Cell Size x,y,z (m)", p, &fdtd::Configuration::dr, cfgPage,
              {"0.1e-4", "0.1e-4", "0.1e-4"},
              {"dr", "dr", "dr"});
    testEntry("Boundary Size", p, &fdtd::Configuration::boundarySize,
              cfgPage, "0", "40");
    testEntry("Time Span (s)", p, &fdtd::Configuration::tSize, cfgPage,
              "5.0e-8", "1.04/f1");
    testCheckButton("Use Thin Sheet Subcells", p,
                    &fdtd::Configuration::useThinSheetSubcells, cfgPage,
                    false, true);
    // Set up the source
    selectNotebookItem(&app->mainWindow().sections(), 1);
    clickMenuButton("New Source", 0, srcPage);
    int id = getListWidgetCell<int>("Sources", 0, 0, srcPage);
    auto* source = dynamic_cast<source::PlaneWaveSource*>(
            app->model().sources().getSource(id));
    REQUIRE(source != nullptr);
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1e+12", "f1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "1e+12", "f1");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "1", "nf");
    testEntry("Initial Time (s)", source,
              &source::PlaneWaveSource::initialTime, srcPage,
              "0", "0.5/f1");
    // Set up the materials
    selectNotebookItem(&app->mainWindow().sections(), 2);
    clickMenuButton("New Material", 0, matPage);
    setListWidgetCell<const std::string&>("Materials", 2, 1,
                                          "Metal", matPage);
    id = getListWidgetCell<int>("Materials", 2, 0, matPage);
    auto* material = app->model().d()->getMaterial(id);
    REQUIRE(material != nullptr);
    testEntry("Epsilon", material, &domain::Material::epsilon, matPage,
              "8.85418782e-12", "8.85418782e+12");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, matPage,
                     box::Constants::colourBlack,
                     box::Constants::colourYellow);
    // Set up the shapes
    selectNotebookItem(&app->mainWindow().sections(), 3);
    clickMenuButton("New Shape", 5, shpPage);
    id = getListWidgetCell<int>("Shapes", 0, 0, shpPage);
    auto* shape = dynamic_cast<domain::FingerPlate*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::Shape::material, shpPage,
                        0, -1, 3, 10);
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::UnitCellShape::cellSizeX, shpPage,
              "0", "g");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::increment, shpPage,
              {"0", "0", "0"},
              {"g", "g", "0"});
    testEntry("Plate Size (%)", shape,
              &domain::FingerPlate::plateSize, shpPage,
              "50", "p");
    testEntry("Min Feature Size (%)", shape,
              &domain::FingerPlate::minFeatureSize, shpPage,
              "10", "d");
    // Set up the sensors
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 0, ssrPage);
    setListWidgetCell<const std::string&>("Sensors", 0, 1,
                                          "EMag", ssrPage);
    clickMenuButton("New Sensor", 2, ssrPage);
    setListWidgetCell<const std::string&>("Sensors", 1, 1,
                                          "SideMat", ssrPage);
    clickMenuButton("New Sensor", 1, ssrPage);
    setListWidgetCell<const std::string&>("Sensors", 2, 1,
                                          "Sensor", ssrPage);
    id = getListWidgetCell<int>("Sensors", 2, 0, ssrPage);
    auto* sensor = dynamic_cast<sensor::ArraySensor*>(app->model().sensors().find(id));
    REQUIRE(sensor != nullptr);
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::ArraySensor::colour, ssrPage,
                     box::Constants::colourBlack, box::Constants::colourWhite);
    testCheckButton("Average Over Cell", sensor,
                    &sensor::ArraySensor::averageOverCell, ssrPage,
                    false, true);
    testCheckButton("Y", &sensor->componentSel(),
                    &box::ComponentSel::y, ssrPage,
                    true, false);
    testCheckButton("Z", &sensor->componentSel(),
                    &box::ComponentSel::z, ssrPage,
                    true, false);
    // Setup the views
    selectNotebookItem(&app->mainWindow().sections(), 6);
    testButton("New View", vwsPage);
    id = getListWidgetCell<int>("Views", 0, 0, vwsPage);
    auto* view = dynamic_cast<ViewFrame*>(app->model().findView(id));
    REQUIRE(view != nullptr);
    setListWidgetCell("Sensor Data Sources", 0, 0, true, vwsPage);
    auto eMagView = dynamic_cast<TwoDSliceViewItem*>(
            view->viewForSpec(0, "twoDSlice"));
    testEntry("Display Max", eMagView, &TwoDSliceViewItem::displayMax, vwsPage,
              "1", "0.1");
    setListWidgetCell("Sensor Data Sources", 2, 0, true, vwsPage);
    auto materialView = dynamic_cast<TwoDSliceViewItem*>(
            view->viewForSpec(1, "material"));
    setListWidgetCell("Sensor Data Sources", 7, 0, true, vwsPage);
    auto attenView = dynamic_cast<DataSeriesViewItem*>(
            view->viewForSpec(2, "BackAttenuation"));
    testEntry("Y Max", attenView, &DataSeriesViewItem::yMax, vwsPage,
              "0", "1");
    testCheckButton("Y Automatic", attenView, &DataSeriesViewItem::yAutomatic,
                    vwsPage,
                    true, false);
    setListWidgetCell("Sensor Data Sources", 8, 0, true, vwsPage);
    auto phaseView = dynamic_cast<DataSeriesViewItem*>(
            view->viewForSpec(2, "BackPhaseShift"));
    testEntry("Y Max", phaseView, &DataSeriesViewItem::yMax, vwsPage,
              "0", "180");
    testEntry("Y Min", phaseView, &DataSeriesViewItem::yMin, vwsPage,
              "0", "-180");
    testCheckButton("Y Automatic", phaseView, &DataSeriesViewItem::yAutomatic,
                    vwsPage,
                    true, false);
    setListWidgetCell("Sensor Data Sources", 10, 0, true, vwsPage);
    auto timeSeriesView = dynamic_cast<DataSeriesViewItem*>(
            view->viewForSpec(2, "BackTimeSeries"));
    view->resize(600, 500);
    // Setup the sequencer
    selectNotebookItem(&app->mainWindow().sections(), 7);
    clickMenuButton("New Sequencer", 0, seqPage);
    // Run FDTD until cur time step is the same as number of time steps
    selectNotebookItem(&app->mainWindow().sections(), 0);
    clickToolButton("Run", app->mainWindow());
    bool going = true;
    std::string target = getEntryText(
            cfgPage.findWidget("Number Of Time Steps"));
    while(going) {
        processEvents(1000);
        going = getEntryText(cfgPage.findWidget("Cur Time Step")) != target;
    }
    // Check the locations of the view items
    REQUIRE(view->layout().get_child_at(0, 0) == eMagView);
    REQUIRE(view->layout().get_child_at(0, 1) == eMagView);
    REQUIRE(view->layout().get_child_at(0, 2) == eMagView);
    REQUIRE(view->layout().get_child_at(1, 0) == materialView);
    REQUIRE(view->layout().get_child_at(1, 1) == materialView);
    REQUIRE(view->layout().get_child_at(1, 2) == materialView);
    REQUIRE(view->layout().get_child_at(2, 0) == attenView);
    REQUIRE(view->layout().get_child_at(3, 0) == attenView);
    REQUIRE(view->layout().get_child_at(2, 1) == phaseView);
    REQUIRE(view->layout().get_child_at(3, 1) == phaseView);
    REQUIRE(view->layout().get_child_at(2, 2) == timeSeriesView);
    REQUIRE(view->layout().get_child_at(3, 2) == timeSeriesView);
    // Check the contents of the views
    REQUIRE(testPixmap(eMagView->dataDisplay().displayWidget(),
                       "ViewsDlg_LayoutAndContents_eMag"));
    REQUIRE(testPixmap(materialView->dataDisplay().displayWidget(),
                       "ViewsDlg_LayoutAndContents_material"));
    REQUIRE(testPixmap(attenView->dataDisplay().displayWidget(),
                       "ViewsDlg_LayoutAndContents_atten"));
    REQUIRE(testPixmap(phaseView->dataDisplay().displayWidget(),
                       "ViewsDlg_LayoutAndContents_phase"));
    REQUIRE(testPixmap(timeSeriesView->dataDisplay().displayWidget(),
                       "ViewsDlg_LayoutAndContents_timeSeries"));
}
