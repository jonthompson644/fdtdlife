/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <GtkGui/Timer.h>

using namespace Catch::literals;

class TestTimer {
public:
    TestTimer() :
            timer1_(sigc::mem_fun(this, &TestTimer::expiry1)),
        timer2_(sigc::mem_fun(this, &TestTimer::expiry2)) {
    }
    void expiry1() {
        expired1_ = true;
    }
    void expiry2() {
        expired2_ = true;
    }
    Timer timer1_;
    Timer timer2_;
    bool expired1_{false};
    bool expired2_{false};
};

// Basic timer operation
TEST_CASE("Timer_Basic") {
    // Test setup
    TestTimer fixture;
    // Verify initial state
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    // Start the timer
    fixture.timer1_.start(1800);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // Wait for 2 seconds
    processEvents(2000);
    REQUIRE(fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
}

// Timer stop
TEST_CASE("Timer_Stop") {
    // Test setup
    TestTimer fixture;
    // Verify initial state
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    // Start the timer
    fixture.timer1_.start(1800);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // Stop the timer
    fixture.timer1_.stop();
    REQUIRE(!fixture.timer1_.running());
    // Wait for 2 seconds
    processEvents(2000);
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
}

// Timer restart
TEST_CASE("Timer_Restart") {
    // Test setup
    TestTimer fixture;
    // Verify initial state
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    // Start the timer
    fixture.timer1_.start(1800);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // Wait for 1s
    processEvents(1000);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // Restart the timer
    fixture.timer1_.start(1800);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // Another second would see the original expiry
    processEvents(1000);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    // New expiry time
    processEvents(1000);
    REQUIRE(fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
}

// Two timers running simultaneously
TEST_CASE("Timer_TwoTimers") {
    // Test setup
    TestTimer fixture;
    // Verify initial state
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    REQUIRE(!fixture.expired2_);
    REQUIRE(!fixture.timer2_.running());
    // Start the timers
    fixture.timer1_.start(1800);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    fixture.timer2_.start(3800);
    REQUIRE(!fixture.expired2_);
    REQUIRE(fixture.timer2_.running());
    // Wait for 1 second, neither timer should have expired yet
    processEvents(1000);
    REQUIRE(!fixture.expired1_);
    REQUIRE(fixture.timer1_.running());
    REQUIRE(!fixture.expired2_);
    REQUIRE(fixture.timer2_.running());
    // Wait another second, first should have expired, second not yet
    processEvents(1000);
    REQUIRE(fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    REQUIRE(!fixture.expired2_);
    REQUIRE(fixture.timer2_.running());
    // A further 2 seconds should see the second timer expire
    fixture.expired1_ = false;  // Don't want the first to expire again
    processEvents(2000);
    REQUIRE(!fixture.expired1_);
    REQUIRE(!fixture.timer1_.running());
    REQUIRE(fixture.expired2_);
    REQUIRE(!fixture.timer2_.running());
}

