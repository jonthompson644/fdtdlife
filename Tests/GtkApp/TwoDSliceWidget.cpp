/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"
#include <GtkGui/TwoDSliceWidget.h>
#include "TestWidget.h"

using namespace Catch::literals;

// Basic operation, y longer than x
TEST_CASE("TwoDSliceWidget_BasicDisplayHigh") {
    // Create some data
    const size_t xSize = 10;
    const size_t ySize = 20;
    const double delta = 1.0 / (xSize * ySize);
    double d = 0.0;
    std::vector<double> data;
    data.resize(xSize * ySize);
    for(size_t j = 0; j < ySize; j++) {
        for(size_t i = 0; i < xSize; i++) {
            data[j * xSize + i] = d;
            d += delta;
        }
    }
    // Test setup
    TestWidget app(0, nullptr);
    TwoDSliceWidget twoDSlice;
    app.addTestWidget(twoDSlice, 400, 400);
    // Add a data series
    twoDSlice.config(1.0, TwoDSliceWidget::ColourMap::extKindlemann);
    twoDSlice.data(xSize, ySize, data);
    REQUIRE(app.testPixmap("TwoDSliceWidget_BasicDisplayHigh_Screen1"));
}

// Basic operation, x longer than y
TEST_CASE("TwoDSliceWidget_BasicDisplayWide") {
    // Create some data
    const size_t xSize = 20;
    const size_t ySize = 10;
    const double delta = 1.0 / (xSize * ySize);
    double d = 0.0;
    std::vector<double> data;
    data.resize(xSize * ySize);
    for(size_t j = 0; j < ySize; j++) {
        for(size_t i = 0; i < xSize; i++) {
            data[j * xSize + i] = d;
            d += delta;
        }
    }
    // Test setup
    TestWidget app(0, nullptr);
    TwoDSliceWidget twoDSlice;
    app.addTestWidget(twoDSlice, 400, 400);
    // Add a data series
    twoDSlice.config(1.0, TwoDSliceWidget::ColourMap::extKindlemann);
    twoDSlice.data(xSize, ySize, data);
    REQUIRE(app.testPixmap("TwoDSliceWidget_BasicDisplayWide_Screen1"));
}

// Cells too small for boxes
TEST_CASE("TwoDSliceWidget_NoCellBoxes") {
    // Create some data
    const size_t xSize = 20;
    const size_t ySize = 20;
    const double delta = 1.0 / (xSize * ySize);
    double d = 0.0;
    std::vector<double> data;
    data.resize(xSize * ySize);
    for(size_t j = 0; j < ySize; j++) {
        for(size_t i = 0; i < xSize; i++) {
            data[j * xSize + i] = d;
            d += delta;
        }
    }
    // Test setup
    TestWidget app(0, nullptr);
    TwoDSliceWidget twoDSlice;
    app.addTestWidget(twoDSlice, 100, 100);
    // Add a data series
    twoDSlice.config(1.0, TwoDSliceWidget::ColourMap::extKindlemann);
    twoDSlice.data(xSize, ySize, data);
    REQUIRE(app.testPixmap("TwoDSliceWidget_NoCellBoxes_Screen1"));
}

// The diverging blue yellow colour scale
TEST_CASE("TwoDSliceWidget_DivergingBY") {
    // Create some data
    const size_t xSize = 20;
    const size_t ySize = 20;
    const double delta = 2.0 / (xSize * ySize);
    double d = -1.0;
    std::vector<double> data;
    data.resize(xSize * ySize);
    for(size_t j = 0; j < ySize; j++) {
        for(size_t i = 0; i < xSize; i++) {
            data[j * xSize + i] = d;
            d += delta;
        }
    }
    // Test setup
    TestWidget app(0, nullptr);
    TwoDSliceWidget twoDSlice;
    app.addTestWidget(twoDSlice, 400, 400);
    // Add a data series
    twoDSlice.config(1.0, TwoDSliceWidget::ColourMap::divergingBY);
    twoDSlice.data(xSize, ySize, data);
    REQUIRE(app.testPixmap("TwoDSliceWidget_DivergingBY_Screen1"));
}

// The material colour scale
TEST_CASE("TwoDSliceWidget_Material") {
    // Create some data
    const size_t xSize = 20;
    const size_t ySize = 20;
    const double delta = 8.0 / (xSize * ySize);
    double d = 0.0;
    std::vector<double> data;
    data.resize(xSize * ySize);
    for(size_t j = 0; j < ySize; j++) {
        for(size_t i = 0; i < xSize; i++) {
            data[j * xSize + i] = d;
            d += delta;
        }
    }
    // Test setup
    TestWidget app(0, nullptr);
    TwoDSliceWidget twoDSlice;
    app.addTestWidget(twoDSlice, 400, 400);
    // Add a data series
    twoDSlice.config(1.0, TwoDSliceWidget::ColourMap::material);
    twoDSlice.data(xSize, ySize, data);
    REQUIRE(app.testPixmap("TwoDSliceWidget_Material_Screen1"));
}
