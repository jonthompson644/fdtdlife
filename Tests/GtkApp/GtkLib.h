/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_GTKLIB_H
#define FDTDLIFE_GTKLIB_H

#include <GtkGui/FdtdApp.h>
#include "catch.hpp"
#include <thread>
#include <Box/Utility.h>
#include <GtkGui/CustomWidget.h>

const long long UNTIL_CONTINUE = -1;

void processEvents(long long delay);
void setEntryText(Gtk::Widget* widget, const std::string& text, long long delay = 0);
void setCheckActive(Gtk::Widget* widget, bool active, long long delay = 0);
void setCheckActive(const std::string& name, bool value, GuiElement& page);
void selectComboItem(Gtk::Widget* widget, int n, long long delay = 0);
void selectNotebookItem(Gtk::Widget* widget, int n);
std::string getEntryText(Gtk::Widget* widget);
bool getCheckActive(Gtk::Widget* widget);
bool getCheckActive(const std::string& name, GuiElement& page);
void testEntry(const std::string& name, GuiElement& page, const std::string& expected);
void testEntry(const std::string& name, GuiElement& page, const std::string& oldText,
               const std::string& newText);
void testEntryReadOnly(const std::string& name, GuiElement& page, bool readOnly);
void testButton(const std::string& name, GuiElement& page);
void clickButton(const std::string& name, GuiElement& page);
void clickToolButton(const std::string& name, GuiElement& page);
void clickToolMenuButton(const std::string& name, GuiElement& page);
void clickToolMenuButton(const std::string& name, const std::string& itemName,
        GuiElement& page);
void clickMenuButton(const std::string& name, int n, GuiElement& page);
void clickRadioButton(const std::string& name, int n, GuiElement& page);
void clickMenu(const std::string& name, int n, GuiElement& page);
void testListWidgetRow(const std::string& name, int n,
                       const std::vector<std::string>& contents, GuiElement& page);
void selectListWidgetRow(const std::string& name, int n, bool select, GuiElement& page);
void testListWidgetRowSelected(const std::string& name, int n, bool selected,
                               GuiElement& page);
void testListWidgetEmpty(const std::string& name, GuiElement& page);
void testListWidgetNumRows(const std::string& name, size_t numRows, GuiElement& page);
void testListWidgetNumCols(const std::string& name, size_t numCols, GuiElement& page);
template<class T>
T getListWidgetCell(const std::string& name, int row, int col, GuiElement& page);
template<class T>
void setListWidgetCell(const std::string& name, int row, int col, T value, GuiElement& page);
void reorderListWidgetRow(const std::string& name, int fromRow, int toRow, GuiElement& page);
FdtdApp* makeApp(TestSuite* testSuite = nullptr);
void clickBinaryPatternCell(const std::string& name, int x, int y, GuiElement& page);
void clickLeftMouseButton(CustomWidget& widget, double x, double y);
bool getBinaryPatternCell(const std::string& name, int x, int y, GuiElement& page);
void testComboBoxContents(const std::string& name, const std::vector<std::string>& contents,
                          GuiElement& page);
int getComboBoxItem(const std::string& name, GuiElement& page);
void setComboBoxItem(const std::string& name, int n, GuiElement& page);
bool testPixmap(CustomWidget& widget, const std::string& name);

// Get the selection from a combo box widget
template<class T>
T getComboBoxSelection(Gtk::Widget* widget) {
    T result{};
    auto* box = dynamic_cast<Gtk::ComboBoxText*>(widget);
    if(box != nullptr) {
        result = static_cast<T>(box->get_active_row_number());
    }
    return result;
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object, double (O::*getter)() const,
               GuiElement& page, const std::string& oldV, const std::string& newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == Approx(box::Utility::toDouble(oldV)));
    REQUIRE(getEntryText(widget) == oldV);
    setEntryText(widget, newV);
    REQUIRE((object->*getter)() == Approx(box::Utility::toDouble(newV)));
    REQUIRE(getEntryText(widget) == newV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, double (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == Approx(box::Utility::toDouble(oldV)));
    REQUIRE(getEntryText(widget) == oldV);
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object, int (O::*getter)() const,
               GuiElement& page, const std::string& oldV, const std::string& newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == box::Utility::toInt(oldV));
    REQUIRE(getEntryText(widget) == oldV);
    setEntryText(widget, newV);
    REQUIRE((object->*getter)() == Approx(box::Utility::toDouble(newV)));
    REQUIRE(getEntryText(widget) == newV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, int (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == Approx(box::Utility::toDouble(oldV)));
    REQUIRE(getEntryText(widget) == oldV);
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object, std::string (O::*getter)() const,
               GuiElement& page, const std::string& oldV, const std::string& newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getEntryText(widget) == oldV);
    setEntryText(widget, newV);
    REQUIRE((object->*getter)() == newV);
    REQUIRE(getEntryText(widget) == newV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, std::string (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getEntryText(widget) == oldV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, const std::string& (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getEntryText(widget) == oldV);
}

// Test and set an entry widget
template<class O, class T>
void testEntry(const std::string& name, O* object,
               box::VectorExpression<T>& (O::*getter)(),
               GuiElement& page,
               const box::VectorExpression<T>& oldV,
               const box::VectorExpression<T>& newV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x().text() == oldV.x().text());
    REQUIRE((object->*getter)().y().text() == oldV.y().text());
    REQUIRE((object->*getter)().z().text() == oldV.z().text());
    REQUIRE(getEntryText(widgetX) == oldV.x().text());
    REQUIRE(getEntryText(widgetY) == oldV.y().text());
    REQUIRE(getEntryText(widgetZ) == oldV.z().text());
    setEntryText(widgetX, newV.x().text());
    setEntryText(widgetY, newV.y().text());
    setEntryText(widgetZ, newV.z().text());
    REQUIRE((object->*getter)().x().text() == newV.x().text());
    REQUIRE((object->*getter)().y().text() == newV.y().text());
    REQUIRE((object->*getter)().z().text() == newV.z().text());
    REQUIRE(getEntryText(widgetX) == newV.x().text());
    REQUIRE(getEntryText(widgetY) == newV.y().text());
    REQUIRE(getEntryText(widgetZ) == newV.z().text());
}

// Test and set an entry widget
template<class O, class T>
void testEntry(const std::string& name, O* object,
               const box::VectorExpression<T>& (O::*getter)() const,
               GuiElement& page,
               const box::VectorExpression<T>& oldV,
               const box::VectorExpression<T>& newV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x().text() == oldV.x().text());
    REQUIRE((object->*getter)().y().text() == oldV.y().text());
    REQUIRE((object->*getter)().z().text() == oldV.z().text());
    REQUIRE(getEntryText(widgetX) == oldV.x().text());
    REQUIRE(getEntryText(widgetY) == oldV.y().text());
    REQUIRE(getEntryText(widgetZ) == oldV.z().text());
    setEntryText(widgetX, newV.x().text());
    setEntryText(widgetY, newV.y().text());
    setEntryText(widgetZ, newV.z().text());
    REQUIRE((object->*getter)().x().text() == newV.x().text());
    REQUIRE((object->*getter)().y().text() == newV.y().text());
    REQUIRE((object->*getter)().z().text() == newV.z().text());
    REQUIRE(getEntryText(widgetX) == newV.x().text());
    REQUIRE(getEntryText(widgetY) == newV.y().text());
    REQUIRE(getEntryText(widgetZ) == newV.z().text());
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object,
               const box::Vector<double>& (O::*getter)() const,
               GuiElement& page,
               const box::Vector<std::string>& oldV,
               const box::Vector<std::string>& newV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x() == Approx(box::Utility::toDouble(oldV.x())));
    REQUIRE((object->*getter)().y() == Approx(box::Utility::toDouble(oldV.y())));
    REQUIRE((object->*getter)().z() == Approx(box::Utility::toDouble(oldV.z())));
    REQUIRE(getEntryText(widgetX) == oldV.x());
    REQUIRE(getEntryText(widgetY) == oldV.y());
    REQUIRE(getEntryText(widgetZ) == oldV.z());
    setEntryText(widgetX, newV.x());
    setEntryText(widgetY, newV.y());
    setEntryText(widgetZ, newV.z());
    REQUIRE((object->*getter)().x() == Approx(box::Utility::toDouble(newV.x())));
    REQUIRE((object->*getter)().y() == Approx(box::Utility::toDouble(newV.y())));
    REQUIRE((object->*getter)().z() == Approx(box::Utility::toDouble(newV.z())));
    REQUIRE(getEntryText(widgetX) == newV.x());
    REQUIRE(getEntryText(widgetY) == newV.y());
    REQUIRE(getEntryText(widgetZ) == newV.z());
}

// Test an entry widget
template<class O, class T>
void testEntry(const std::string& name, O* object,
               box::VectorExpression<T>& (O::*getter)(),
               GuiElement& page,
               const box::VectorExpression<T>& oldV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x().text() == oldV.x().text());
    REQUIRE((object->*getter)().y().text() == oldV.y().text());
    REQUIRE((object->*getter)().z().text() == oldV.z().text());
    REQUIRE(getEntryText(widgetX) == oldV.x().text());
    REQUIRE(getEntryText(widgetY) == oldV.y().text());
    REQUIRE(getEntryText(widgetZ) == oldV.z().text());
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object,
               const box::Vector<double>& (O::*getter)() const,
               GuiElement& page,
               const box::Vector<std::string>& oldV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x() == Approx(box::Utility::toDouble(oldV.x())));
    REQUIRE((object->*getter)().y() == Approx(box::Utility::toDouble(oldV.y())));
    REQUIRE((object->*getter)().z() == Approx(box::Utility::toDouble(oldV.z())));
    REQUIRE(getEntryText(widgetX) == oldV.x());
    REQUIRE(getEntryText(widgetY) == oldV.y());
    REQUIRE(getEntryText(widgetZ) == oldV.z());
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object,
               const box::Vector<size_t>& (O::*getter)() const,
               GuiElement& page,
               const box::Vector<std::string>& oldV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)().x() == box::Utility::toSizeT(oldV.x()));
    REQUIRE((object->*getter)().y() == box::Utility::toSizeT(oldV.y()));
    REQUIRE((object->*getter)().z() == box::Utility::toSizeT(oldV.z()));
    REQUIRE(getEntryText(widgetX) == oldV.x());
    REQUIRE(getEntryText(widgetY) == oldV.y());
    REQUIRE(getEntryText(widgetZ) == oldV.z());
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object, size_t (O::*getter)() const,
               GuiElement& page, const std::string& oldV, const std::string& newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == box::Utility::toSizeT(oldV));
    REQUIRE(getEntryText(widget) == oldV);
    setEntryText(widget, newV);
    REQUIRE((object->*getter)() == box::Utility::toSizeT(newV));
    REQUIRE(getEntryText(widget) == newV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, size_t (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == box::Utility::toSizeT(oldV));
    REQUIRE(getEntryText(widget) == oldV);
}

// Test and set an entry widget
template<class O>
void testEntry(const std::string& name, O* object, const box::Expression& (O::*getter)() const,
               GuiElement& page, const std::string& oldV, const std::string& newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)().text() == oldV);
    REQUIRE(getEntryText(widget) == oldV);
    setEntryText(widget, newV);
    REQUIRE((object->*getter)().text() == newV);
    REQUIRE(getEntryText(widget) == newV);
}

// Test an entry widget
template<class O>
void testEntry(const std::string& name, O* object, const box::Expression& (O::*getter)() const,
               GuiElement& page, const std::string& oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)().text() == oldV);
    REQUIRE(getEntryText(widget) == oldV);
}

// Test and set a check button widget
template<class O>
void testCheckButton(const std::string& name, const O* object, bool (O::*getter)() const,
                     GuiElement& page, bool oldV, bool newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getCheckActive(widget) == oldV);
    setCheckActive(widget, newV);
    REQUIRE((object->*getter)() == newV);
    REQUIRE(getCheckActive(widget) == newV);
}

// Test a check button widget
template<class O>
void testCheckButton(const std::string& name, const O* object, bool (O::*getter)() const,
                     GuiElement& page, bool oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getCheckActive(widget) == oldV);
}

// Test and set a toggle button widget
template<class O>
void testToggleButton(const std::string& name, const O* object, bool (O::*getter)() const,
                      GuiElement& page, bool oldV, bool newV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getCheckActive(widget) == oldV);
    setCheckActive(widget, newV);
    REQUIRE((object->*getter)() == newV);
    REQUIRE(getCheckActive(widget) == newV);
}

// Test a check button widget
template<class O>
void testToggleButton(const std::string& name, const O* object, bool (O::*getter)() const,
                      GuiElement& page, bool oldV) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getCheckActive(widget) == oldV);
}

// Test and set a combo box widget
template<class O, class T>
void testComboBoxText(const std::string& name, O* object, T (O::*getter)() const,
                      GuiElement& page, T oldV, T newV) {
    INFO(name);
    auto* widget = dynamic_cast<NamedSelection<T>*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(widget->value() == oldV);
    widget->value(newV);
    REQUIRE((object->*getter)() == newV);
    REQUIRE(widget->value() == newV);
}

// Test and set a combo box widget
template<class O, class T>
void testComboBoxText(const std::string& name, O* object,
                      const box::Vector<T>& (O::*getter)() const, GuiElement& page,
                      const box::Vector<T>& oldV, const box::Vector<T>& newV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getComboBoxSelection<T>(widgetX) == oldV.x());
    REQUIRE(getComboBoxSelection<T>(widgetY) == oldV.y());
    REQUIRE(getComboBoxSelection<T>(widgetZ) == oldV.z());
    selectComboItem(widgetX, static_cast<int>(newV.x()));
    selectComboItem(widgetY, static_cast<int>(newV.y()));
    selectComboItem(widgetZ, static_cast<int>(newV.z()));
    REQUIRE((object->*getter)() == newV);
    REQUIRE(getComboBoxSelection<T>(widgetX) == newV.x());
    REQUIRE(getComboBoxSelection<T>(widgetY) == newV.y());
    REQUIRE(getComboBoxSelection<T>(widgetZ) == newV.z());
}

// Test and set a materials selection combo box widget
template<class O>
void testMaterialsSelect(const std::string& name, O* object, int (O::*getter)() const,
                         GuiElement& page, int oldSel, int oldMat, int newSel, int newMat) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldMat);
    REQUIRE(getComboBoxSelection<int>(widget) == oldSel);
    selectComboItem(widget, newSel);
    REQUIRE((object->*getter)() == newMat);
    REQUIRE(getComboBoxSelection<int>(widget) == newSel);
}

// Test and set a materials selection combo box widget
template<class O>
void testMaterialsSelect(const std::string& name, O* object, int (O::*getter)() const,
                         GuiElement& page, int oldSel, int oldMat) {
    INFO(name);
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldMat);
    REQUIRE(getComboBoxSelection<int>(widget) == oldSel);
}

// Test a combo box widget
template<class O, class T>
void testComboBoxText(const std::string& name, O* object, T (O::*getter)() const,
                      GuiElement& page, T oldV) {
    INFO(name);
    auto* widget = dynamic_cast<NamedSelection<T>*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(widget->value() == oldV);
}

// Test a combo box widget
template<class O, class T>
void testComboBoxText(const std::string& name, O* object,
                      const box::Vector<T>& (O::*getter)() const, GuiElement& page,
                      const box::Vector<T>& oldV) {
    INFO(name);
    auto* widgetX = page.findWidget(name + ".x");
    auto* widgetY = page.findWidget(name + ".y");
    auto* widgetZ = page.findWidget(name + ".z");
    REQUIRE(widgetX != nullptr);
    REQUIRE(widgetY != nullptr);
    REQUIRE(widgetZ != nullptr);
    REQUIRE((object->*getter)() == oldV);
    REQUIRE(getComboBoxSelection<T>(widgetX) == oldV.x());
    REQUIRE(getComboBoxSelection<T>(widgetY) == oldV.y());
    REQUIRE(getComboBoxSelection<T>(widgetZ) == oldV.z());
}

class TestSuiteApi : public TestSuite {
public:
    int runModalDialog(const std::string& name, Gtk::Widget* /*dialog*/) override {
        REQUIRE(!waitingDialogs_.empty());
        usedDialogs_.emplace_front(waitingDialogs_.back());
        waitingDialogs_.pop_back();
        INFO(usedDialogs_.front().name_ + " Wrong dialog")
        REQUIRE(name == usedDialogs_.front().name_);
        usedDialogs_.front().success_ = true;
        return usedDialogs_.front().result_;
    }
    int runFileModalDialog(const std::string& name, Gtk::Widget* /*dialog*/,
                           std::string& selected) override {
        REQUIRE(!waitingDialogs_.empty());
        usedDialogs_.emplace_front(waitingDialogs_.back());
        waitingDialogs_.pop_back();
        INFO(usedDialogs_.front().name_ + " Wrong dialog")
        REQUIRE(name == usedDialogs_.front().name_);
        usedDialogs_.front().success_ = true;
        selected = usedDialogs_.front().selectedFile_;
        return usedDialogs_.front().result_;
    }
    void expectModalDialog(const std::string& name, int result) {
        waitingDialogs_.emplace_front();
        waitingDialogs_.front().name_ = name;
        waitingDialogs_.front().result_ = result;
        waitingDialogs_.front().success_ = false;
    }
    void expectFileModalDialog(const std::string& name, const std::string& selectedFile,
                               int result) {
        waitingDialogs_.emplace_front();
        waitingDialogs_.front().name_ = name;
        waitingDialogs_.front().result_ = result;
        waitingDialogs_.front().selectedFile_ = selectedFile;
        waitingDialogs_.front().success_ = false;
    }
    void checkModalDialog() {
        REQUIRE(waitingDialogs_.empty());
        REQUIRE(!usedDialogs_.empty());
        INFO(usedDialogs_.back().name_ + " No dialog")
        REQUIRE(usedDialogs_.back().success_);
        usedDialogs_.pop_back();
    }

    class ModalDialogSpec {
    public:
        ModalDialogSpec() = default;
        int result_{};
        std::string name_;
        std::string selectedFile_;
        bool success_{};
    };

    std::list<ModalDialogSpec> waitingDialogs_;
    std::list<ModalDialogSpec> usedDialogs_;
};


#endif //FDTDLIFE_GTKLIB_H
