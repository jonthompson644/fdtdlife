/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <GtkGui/DataSeriesWidget.h>
#include "TestWidget.h"

using namespace Catch::literals;

// Basic operation
TEST_CASE("DataSeriesWidget_BasicDisplay") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 100, "yBobs");
    dataSeries.data("things", std::vector<double>(
            {10.0, 30.0, 80.0, 70.0, 50.0, 40.0, 30.0, 20.0, 20.0, 10.0}));
    dataSeries.data("more", std::vector<double>(
            {5.0, 20.0, 25.0, 35.0, 90.0, 80.0, 70.0, 50.0, 20.0, 5.0}));
    dataSeries.markersOff();
    REQUIRE(app.testPixmap("DataSeriesWidget_BasicDisplay_Screen1"));
}

// Operation of the markers
TEST_CASE("DataSeriesWidget_Markers") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 100, "yBobs");
    dataSeries.data("things", std::vector<double>(
            {10.0, 30.0, 80.0, 70.0, 50.0, 40.0, 30.0, 20.0, 20.0, 10.0}));
    dataSeries.data("more", std::vector<double>(
            {5.0, 20.0, 25.0, 35.0, 90.0, 80.0, 70.0, 50.0, 20.0, 5.0}));
    dataSeries.markers(5UL);
    REQUIRE(app.testPixmap("DataSeriesWidget_Markers_Screen1"));
    // Check marker values
    REQUIRE(dataSeries.markerX() == 5.0_a);
    REQUIRE(dataSeries.markerY("things") == 40.0_a);
    REQUIRE(dataSeries.markerY("more") == 80.0_a);
    // Move the marker by emulating a mouse click
    clickLeftMouseButton(dataSeries, 315, 233);
    REQUIRE(dataSeries.markerX() == 7.0_a);
    REQUIRE(dataSeries.markerY("things") == 20.0_a);
    REQUIRE(dataSeries.markerY("more") == 50.0_a);
    REQUIRE(app.testPixmap("DataSeriesWidget_Markers_Screen2"));
    // Set the marker using an x value
    dataSeries.markers(3.0);
    REQUIRE(dataSeries.markerX() == 3.0_a);
    REQUIRE(dataSeries.markerY("things") == 70.0_a);
    REQUIRE(dataSeries.markerY("more") == 35.0_a);
}

// Auto scaling Y
TEST_CASE("DataSeriesWidget_AutoScaleY") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY("yBobs");
    dataSeries.data("things", std::vector<double>(
            {10.0, 30.0, 80.0, 70.0, 50.0, 40.0, 30.0, 20.0, 20.0, 10.0}));
    dataSeries.data("more", std::vector<double>(
            {5.0, 20.0, 25.0, 35.0, 90.0, 80.0, 70.0, 50.0, 20.0, 5.0}));
    dataSeries.markersOff();
    REQUIRE(app.testPixmap("DataSeriesWidget_AutoScaleY_Screen1"));
}

// Data changing live
TEST_CASE("DataSeriesWidget_DataChanging") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 100, "yBobs");
    dataSeries.data("things", std::vector<double>(
            {10.0, 30.0, 80.0, 70.0, 50.0, 40.0, 30.0, 20.0, 20.0, 10.0}));
    dataSeries.data("more", std::vector<double>(
            {5.0, 20.0, 25.0, 35.0, 90.0, 80.0, 70.0, 50.0, 20.0, 5.0}));
    dataSeries.markersOff();
    // Now change the second series
    dataSeries.data("more", std::vector<double>(
            {25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0}));
    REQUIRE(app.testPixmap("DataSeriesWidget_DataChanging_Screen1"));
}

// Complex numbers
TEST_CASE("DataSeriesWidget_ComplexNumbers") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 100, "yBobs");
    dataSeries.data("things", std::vector<std::complex<double>>(
            {{10.0, 5.0}, {30.0, 20.0},
             {80.0, 25.0}, {70.0, 35.0}, {50.0, 90.0},
             {40.0, 80.0}, {30.0, 70.0}, {20.0, 50.0},
             {20.0, 20.0}, {10.0, 5.0}}));
    dataSeries.markersOff();
    REQUIRE(app.testPixmap("DataSeriesWidget_ComplexNumbers_Screen1"));
}

// More data series than colours
TEST_CASE("DataSeriesWidget_TooManySeries") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 100, "yBobs");
    dataSeries.data("one", std::vector<double>(
            {10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0}));
    dataSeries.data("two", std::vector<double>(
            {20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0}));
    dataSeries.data("three", std::vector<double>(
            {30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0}));
    dataSeries.data("four", std::vector<double>(
            {40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0}));
    dataSeries.data("five", std::vector<double>(
            {50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0}));
    dataSeries.data("six", std::vector<double>(
            {60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0}));
    dataSeries.markers(5UL);
    REQUIRE(app.testPixmap("DataSeriesWidget_TooManySeries_Screen1"));
}

// Only two tick marks
TEST_CASE("DataSeriesWidget_TwoTickMarks") {
    // Test setup
    TestWidget app(0, nullptr);
    DataSeriesWidget dataSeries;
    app.addTestWidget(dataSeries, 400, 400);
    // Add a data series
    dataSeries.axisX(0, 1, 10, "xBobs");
    dataSeries.axisY(0, 200, "yBobs");
    dataSeries.data("things", std::vector<double>(
            {10.0, 30.0, 80.0, 70.0, 50.0, 40.0, 30.0, 20.0, 20.0, 10.0}));
    dataSeries.data("more", std::vector<double>(
            {5.0, 20.0, 25.0, 35.0, 90.0, 80.0, 70.0, 50.0, 20.0, 5.0}));
    dataSeries.markersOff();
    REQUIRE(app.testPixmap("DataSeriesWidget_TwoTickMarks_Screen1"));
}


