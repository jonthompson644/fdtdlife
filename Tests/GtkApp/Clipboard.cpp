/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"

using namespace Catch::literals;

// The clipboard helper object
TEST_CASE("Clipboard_CutAndPasteText") {
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    app->model().clipboard().put("Hello world");
    REQUIRE(app->model().clipboard().getText() == "Hello world");
    app->model().clipboard().put("More Text", "Special/Target");
    REQUIRE(app->model().clipboard().getText("Special/Target") == "More Text");
}

static const char * blarg_xpm[] = {
        "16 7 2 1",
        "* c #000000",
        ". c #ffffff",
        "**..*...........",
        "*.*.*...........",
        "**..*..**.**..**",
        "*.*.*.*.*.*..*.*",
        "**..*..**.*...**",
        "...............*",
        ".............**."
};

// The clipboard helper object
TEST_CASE("Clipboard_CutAndPasteImages") {
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto image = Gdk::Pixbuf::create_from_xpm_data(blarg_xpm);
    REQUIRE(image.get() != nullptr);
    app->model().clipboard().put(image);
    auto pastedImage = app->model().clipboard().getImage();
    REQUIRE(pastedImage.get() != nullptr);
}
