/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <Sensor/LensPhaseAnalyser.h>
#include <Sensor/LensReflectionAnalyser.h>
#include <Sensor/LensTransmissionAnalyser.h>
#include <GtkGui/NamedDataSeries.h>
#include <Sensor/ArraySensor.h>

using namespace Catch::literals;

// Creating and destroying analysers
TEST_CASE("AnalysersDlg_NewAndDeleteAnalyser") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Add three lens phase analysers
    clickMenuButton("New Analyser", 0, srcPage);
    clickMenuButton("New Analyser", 0, srcPage);
    clickMenuButton("New Analyser", 0, srcPage);
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"1", "Analyser 1", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 2, {"2", "Analyser 2", "Lens Phase"},
                      srcPage);
    testListWidgetRowSelected("Analysers", 2, true, srcPage);
    // Edit the name of the last analyser
    int id = getListWidgetCell<int>("Analysers", 2, 0, srcPage);
    auto* analyser = dynamic_cast<sensor::Analyser*>(app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    selectListWidgetRow("Analysers", 2, true, srcPage);
    setListWidgetCell<const std::string&>("Analysers", 2, 1,
                                          "Changed Name", srcPage);
    REQUIRE(analyser->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Analysers", 2, 1,
                                          "Analyser 2", srcPage);
    REQUIRE(analyser->name() == "Analyser 2");
    // Cancel deletion of the sensorId
    selectListWidgetRow("Analysers", 2, true, srcPage);
    appRequests.expectModalDialog("Delete Analyser", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Analyser", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"1", "Analyser 1", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 2, {"2", "Analyser 2", "Lens Phase"},
                      srcPage);
    testListWidgetRowSelected("Analysers", 2, true, srcPage);
    // Delete the analyser
    selectListWidgetRow("Analysers", 1, true, srcPage);
    appRequests.expectModalDialog("Delete Analyser", Gtk::RESPONSE_OK);
    clickButton("Delete Analyser", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"2", "Analyser 2", "Lens Phase"},
                      srcPage);
    testListWidgetRowSelected("Analysers", 1, true, srcPage);
}

// Operation of the lens phase analyser sub-dialog
TEST_CASE("AnalysersDlg_LensPhase") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Add three lens phase analysers
    clickMenuButton("New Analyser", 0, srcPage);
    clickMenuButton("New Analyser", 0, srcPage);
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Phase"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"1", "Analyser 1", "Lens Phase"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Analysers", 0, true, srcPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, srcPage);
    auto* analyser = dynamic_cast<sensor::LensPhaseAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Check the sub dialog
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensPhaseAnalyser::frequency, srcPage,
              "1000000000", "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::lowestZ,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::x,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Switch to another sensorId and back
    selectListWidgetRow("Analysers", 1, true, srcPage);
    selectListWidgetRow("Analysers", 0, true, srcPage);
    // Is the page still ok
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensPhaseAnalyser::frequency, srcPage,
              "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Add a couple of sensors so we can check their selection
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Check the contents of the sensors list
    testComboBoxContents("Sensor",
                         {"<< None >>", "Sensor 0", "Sensor 1"},
                         srcPage);
    // Now check we can select one
    setComboBoxItem("Sensor", 2, srcPage);
    // Add another sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 2, {"2", "Sensor 2", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Is the original sensorId still selected
    REQUIRE(getComboBoxItem("Sensor", srcPage) == 2);
    // Check the plot widget context menu
    clickMenu("dataseries.contextmenu", 0, srcPage);
    auto pastedImage = app->model().clipboard().getImage();
    REQUIRE(pastedImage.get() != nullptr);
}

// Lens phase analyser sub dialog plot widget operation
TEST_CASE("AnalysersDlg_LensPhasePlotWidget") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& analyserPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    clickMenuButton("New Analyser", 0, analyserPage);
    selectListWidgetRow("Analysers", 0, true, analyserPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, analyserPage);
    auto* analyser = dynamic_cast<sensor::LensPhaseAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Poke some data into the analyser
    std::vector<double> data(100);
    for(size_t i=0; i<data.size(); i++) {
        data[i] = sin(static_cast<double>(i) * 2 * box::Constants::pi_ / data.size());
    }
    analyser->setData(data);
    processEvents(500); // Let the display update so the scaling is set
    // Did the data turn up in the display?
    auto* widget = analyserPage.findWidget("dataseries");
    auto* dataSeriesWidget = dynamic_cast<DataSeriesWidget*>(widget);
    REQUIRE(dataSeriesWidget != nullptr);
    dataSeriesWidget->markers(0.296);
    REQUIRE(dataSeriesWidget->markerX() == 0.292_a);
    REQUIRE(dataSeriesWidget->markerY("data") == -0.801909_a);
}

// Operation of the lens reflection analyser sub-dialog
TEST_CASE("AnalysersDlg_LensReflection") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Add three lens phase analysers
    clickMenuButton("New Analyser", 1, srcPage);
    clickMenuButton("New Analyser", 1, srcPage);
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Reflection"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"1", "Analyser 1", "Lens Reflection"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Analysers", 0, true, srcPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, srcPage);
    auto* analyser = dynamic_cast<sensor::LensReflectionAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Check the sub dialog
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensReflectionAnalyser::frequency, srcPage,
              "1000000000", "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::lowestZ,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::x,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Reference Time (s)", analyser,
              &sensor::LensReflectionAnalyser::referenceTime, srcPage,
              "0", "6");
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Switch to another sensorId and back
    selectListWidgetRow("Analysers", 1, true, srcPage);
    selectListWidgetRow("Analysers", 0, true, srcPage);
    // Is the page still ok
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensReflectionAnalyser::frequency, srcPage,
              "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Reference Time (s)", analyser,
              &sensor::LensReflectionAnalyser::referenceTime, srcPage,
              "6");
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Add a couple of sensors so we can check their selection
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Check the contents of the sensors list
    testComboBoxContents("Sensor",
                         {"<< None >>", "Sensor 0", "Sensor 1"},
                         srcPage);
    // Now check we can select one
    setComboBoxItem("Sensor", 2, srcPage);
    // Add another sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 2, {"2", "Sensor 2", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Is the original sensorId still selected
    REQUIRE(getComboBoxItem("Sensor", srcPage) == 2);
    // Check the plot widget context menu
    clickMenu("dataseries.contextmenu", 0, srcPage);
    auto pastedImage = app->model().clipboard().getImage();
    REQUIRE(pastedImage.get() != nullptr);
}

// Lens reflection analyser sub dialog plot widget operation
// Also checks the capture now button
TEST_CASE("AnalysersDlg_LensReflectionPlotWidget") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& analyserPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    clickMenuButton("New Analyser", 1, analyserPage);
    selectListWidgetRow("Analysers", 0, true, analyserPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, analyserPage);
    auto* analyser = dynamic_cast<sensor::LensReflectionAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Poke some data into the analyser
    std::vector<double> data(100);
    for(size_t i=0; i<data.size(); i++) {
        data[i] = sin(static_cast<double>(i) * 2 * box::Constants::pi_ / data.size());
    }
    analyser->setData(data);
    processEvents(500); // Let the display update so the scaling is set
    // Set the reference time to something non-zero
    testEntry("Reference Time (s)", analyser,
              &sensor::LensReflectionAnalyser::referenceTime, analyserPage,
              "0", "6");
    // Did the data turn up in the display?
    auto* widget = analyserPage.findWidget("dataseries");
    auto* dataSeriesWidget = dynamic_cast<DataSeriesWidget*>(widget);
    REQUIRE(dataSeriesWidget != nullptr);
    dataSeriesWidget->markers(0.296);
    REQUIRE(dataSeriesWidget->markerX() == 0.292_a);
    REQUIRE(dataSeriesWidget->markerY("data") == -0.770513_a);
    REQUIRE(dataSeriesWidget->markerY("reference") == 0.0_a);
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, analyserPage,
              "50");
    // Capture the reference now (no sensorId, so should just copy the data into the reference)
    testButton("Capture Reference Now", analyserPage);
    REQUIRE(dataSeriesWidget->markerX() == 0.292_a);
    REQUIRE(dataSeriesWidget->markerY("data") == -0.770513_a);
    REQUIRE(dataSeriesWidget->markerY("reference") == -0.770513_a);
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, analyserPage,
              "0");
    // The reference time should now have the current time (0)
    testEntry("Reference Time (s)", analyser,
              &sensor::LensReflectionAnalyser::referenceTime, analyserPage,
              "0");
}

// Operation of the lens transmission analyser sub-dialog
TEST_CASE("AnalysersDlg_LensTransmission") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Add three lens phase analysers
    clickMenuButton("New Analyser", 2, srcPage);
    clickMenuButton("New Analyser", 2, srcPage);
    testListWidgetRow("Analysers", 0, {"0", "Analyser 0", "Lens Transmission"},
                      srcPage);
    testListWidgetRow("Analysers", 1, {"1", "Analyser 1", "Lens Transmission"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Analysers", 0, true, srcPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, srcPage);
    auto* analyser = dynamic_cast<sensor::LensTransmissionAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Check the sub dialog
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensTransmissionAnalyser::frequency, srcPage,
              "1000000000", "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::lowestZ,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::x,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Gaussian A", analyser,
              &sensor::LensTransmissionAnalyser::gaussianA, srcPage,
              "0");
    testEntry("Gaussian C", analyser,
              &sensor::LensTransmissionAnalyser::gaussianC, srcPage,
              "0");
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Switch to another sensorId and back
    selectListWidgetRow("Analysers", 1, true, srcPage);
    selectListWidgetRow("Analysers", 0, true, srcPage);
    // Is the page still ok
    testEntry("Frequency (Hz)", analyser,
              &sensor::LensTransmissionAnalyser::frequency, srcPage,
              "5");
    testComboBoxText("Location", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::location, srcPage,
                     sensor::Analyser::SensorLocation::highestZ);
    testComboBoxText("Component", dynamic_cast<sensor::Analyser*>(analyser),
                     &sensor::Analyser::component, srcPage,
                     sensor::Analyser::SensorComponent::y);
    testEntry("Gaussian A", analyser,
              &sensor::LensTransmissionAnalyser::gaussianA, srcPage,
              "0");
    testEntry("Gaussian C", analyser,
              &sensor::LensTransmissionAnalyser::gaussianC, srcPage,
              "0");
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, srcPage,
              "0");
    // Add a couple of sensors so we can check their selection
    auto& sensorPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Check the contents of the sensors list
    testComboBoxContents("Sensor",
                         {"<< None >>", "Sensor 0", "Sensor 1"},
                         srcPage);
    // Now check we can select one
    setComboBoxItem("Sensor", 2, srcPage);
    // Add another sensorId
    selectNotebookItem(&app->mainWindow().sections(), 4);
    clickMenuButton("New Sensor", 1, sensorPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      sensorPage);
    testListWidgetRow("Sensors", 2, {"2", "Sensor 2", "Array Sensor"},
                      sensorPage);
    selectNotebookItem(&app->mainWindow().sections(), 5);
    // Is the original sensorId still selected
    REQUIRE(getComboBoxItem("Sensor", srcPage) == 2);
    // Check the plot widget context menu
    clickMenu("dataseries.contextmenu", 0, srcPage);
    auto pastedImage = app->model().clipboard().getImage();
    REQUIRE(pastedImage.get() != nullptr);
}

// Lens transmission analyser sub dialog plot widget operation
// Also checks the gaussian parameter update
TEST_CASE("AnalysersDlg_LensTransmissionPlotWidget") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& analyserPage = app->mainWindow().analysersPage();
    selectNotebookItem(&app->mainWindow().sections(), 5);
    clickMenuButton("New Analyser", 2, analyserPage);
    selectListWidgetRow("Analysers", 0, true, analyserPage);
    int id = getListWidgetCell<int>("Analysers", 0, 0, analyserPage);
    auto* analyser = dynamic_cast<sensor::LensTransmissionAnalyser*>(
            app->model().analysers().find(id));
    REQUIRE(analyser != nullptr);
    // Poke some data into the analyser
    std::vector<double> data(100);
    for(size_t i=0; i<data.size(); i++) {
        data[i] = sin(static_cast<double>(i) * 2 * box::Constants::pi_ / data.size());
    }
    analyser->setData(data, 0.0001);
    processEvents(500); // Let the display update so the scaling is set
    // Did the data turn up in the display?
    auto* widget = analyserPage.findWidget("dataseries");
    auto* dataSeriesWidget = dynamic_cast<DataSeriesWidget*>(widget);
    REQUIRE(dataSeriesWidget != nullptr);
    dataSeriesWidget->markers(0.296);
    REQUIRE(dataSeriesWidget->markerX() == 0.292_a);
    REQUIRE(dataSeriesWidget->markerY("data") == -0.770513_a);
    REQUIRE(dataSeriesWidget->markerY("reference") == 0.595989_a);
    testEntry("Error", dynamic_cast<sensor::Analyser*>(analyser),
              &sensor::Analyser::error, analyserPage,
              "109.851");
    testEntry("Gaussian A", analyser,
              &sensor::LensTransmissionAnalyser::gaussianA, analyserPage,
              "1");
    testEntry("Gaussian C", analyser,
              &sensor::LensTransmissionAnalyser::gaussianC, analyserPage,
              "0.00353849");
}


