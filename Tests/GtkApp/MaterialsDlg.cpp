/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <Domain/CpmlMaterial.h>

using namespace Catch::literals;

// Creating and destroying sources
TEST_CASE("MaterialsDlg_NewAndDeleteMaterial") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& matPage = app->mainWindow().materialsPage();
    selectNotebookItem(&app->mainWindow().sections(), 2);
    // Check the default materials are present
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      matPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      matPage);
    // The default materials cannot be deleted
    selectListWidgetRow("Materials", 0, true, matPage);
    appRequests.expectModalDialog("Cannot Delete Material", Gtk::RESPONSE_CLOSE);
    clickButton("Delete Material", matPage);
    appRequests.checkModalDialog();
    selectListWidgetRow("Materials", 1, true, matPage);
    appRequests.expectModalDialog("Cannot Delete Material", Gtk::RESPONSE_CLOSE);
    clickButton("Delete Material", matPage);
    appRequests.checkModalDialog();
    // Add a regular material
    clickMenuButton("New Material", 0, matPage);
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      matPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      matPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal", "1"},
                      matPage);
    testListWidgetRowSelected("Materials", 2, true, matPage);
    // Edit the name of the material
    int id = getListWidgetCell<int>("Materials", 2, 0, matPage);
    auto* material = dynamic_cast<domain::Material*>(app->model().d()->getMaterial(id));
    REQUIRE(material != nullptr);
    selectListWidgetRow("Materials", 2, true, matPage);
    setListWidgetCell<const std::string&>("Materials", 2, 1,
                                          "Changed Name", matPage);
    REQUIRE(material->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Materials", 2, 1,
                                          "Material 10", matPage);
    REQUIRE(material->name() == "Material 10");
    // Cancel deletion of the material
    selectListWidgetRow("Materials", 2, true, matPage);
    appRequests.expectModalDialog("Delete Material", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Material", matPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      matPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      matPage);
    testListWidgetRow("Materials", 2,
                      {"10", "Material 10", "Normal", "1"},
                      matPage);
    testListWidgetRowSelected("Materials", 2, true, matPage);
    // Delete the material
    selectListWidgetRow("Materials", 2, true, matPage);
    appRequests.expectModalDialog("Delete Material", Gtk::RESPONSE_OK);
    clickButton("Delete Material", matPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      matPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      matPage);
    testListWidgetRowSelected("Materials", 1, true, matPage);
}

// Operation of the normal material sub-dialog
TEST_CASE("MaterialsDlg_NormalMaterial") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& matPage = app->mainWindow().materialsPage();
    selectNotebookItem(&app->mainWindow().sections(), 2);
    // Add a normal material
    clickMenuButton("New Material", 0, matPage);
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      matPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      matPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal", "1"},
                      matPage);
    // Select the normal material
    selectListWidgetRow("Materials", 2, true, matPage);
    int id = getListWidgetCell<int>("Materials", 2, 0, matPage);
    auto* material = app->model().d()->getMaterial(id);
    REQUIRE(material != nullptr);
    // Check the normal sub dialog
    testEntry("Priority", material, &domain::Material::priority, matPage,
              "0", "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, matPage,
                     box::Constants::colourBlack,
                     box::Constants::colourCyan);
    testEntry("Epsilon", material, &domain::Material::epsilon, matPage,
              "8.85418782e-12", "2");
    testEntry("Relative Epsilon", matPage, "2.25882e+11");
    testEntry("Mu", material, &domain::Material::mu, matPage,
              "1.25663706e-06", "3");
    testEntry("Relative Mu", matPage, "2.38732e+06");
    testEntry("Sigma", material, &domain::Material::sigma, matPage,
              "0", "4");
    testEntry("Sigma*", material, &domain::Material::sigmastar, matPage,
              "0", "5");
    // Switch to the default material page and back
    selectListWidgetRow("Materials", 0, true, matPage);
    selectListWidgetRow("Materials", 2, true, matPage);
    // Is the page still ok
    testEntry("Priority", material, &domain::Material::priority, matPage,
              "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, matPage,
                     box::Constants::colourCyan);
    testEntry("Epsilon", material, &domain::Material::epsilon, matPage,
              "2");
    testEntry("Relative Epsilon", matPage, "2.25882e+11");
    testEntry("Mu", material, &domain::Material::mu, matPage,
              "3");
    testEntry("Relative Mu", matPage, "2.38732e+06");
    testEntry("Sigma", material, &domain::Material::sigma, matPage,
              "4");
    testEntry("Sigma*", material, &domain::Material::sigmastar, matPage,
              "5");
    // Do the relative boxes do the right thing
    testEntry("Relative Epsilon", matPage,
              "2.25882e+11", "1");
    testEntry("Epsilon", material, &domain::Material::epsilon, matPage,
              "8.85418782e-12");
    testEntry("Relative Mu", matPage,
              "2.38732e+06", "1");
    testEntry("Mu", material, &domain::Material::mu, matPage,
              "1.25663706e-06");
}

// Operation of the default material sub-dialog
TEST_CASE("MaterialsDlg_DefaultMaterial") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().materialsPage();
    selectNotebookItem(&app->mainWindow().sections(), 2);
    // Add a normal material
    clickMenuButton("New Material", 0, srcPage);
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      srcPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      srcPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal", "1"},
                      srcPage);
    // Select the default material
    selectListWidgetRow("Materials", 0, true, srcPage);
    int id = getListWidgetCell<int>("Materials", 0, 0, srcPage);
    auto* material = app->model().d()->getMaterial(id);
    REQUIRE(material != nullptr);
    // Check the default sub dialog
    testEntry("Priority", material, &domain::Material::priority, srcPage,
              "0", "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, srcPage,
                     box::Constants::colourBlack,
                     box::Constants::colourCyan);
    testEntry("Epsilon", material, &domain::Material::epsilon, srcPage,
              "8.85418782e-12", "2");
    testEntry("Relative Epsilon", srcPage, "2.25882e+11");
    testEntry("Mu", material, &domain::Material::mu, srcPage,
              "1.25663706e-06", "3");
    testEntry("Relative Mu", srcPage, "2.38732e+06");
    testEntry("Sigma", material, &domain::Material::sigma, srcPage,
              "0", "4");
    testEntry("Sigma*", material, &domain::Material::sigmastar, srcPage,
              "0", "5");
    // Switch to the normal material page and back
    selectListWidgetRow("Materials", 2, true, srcPage);
    selectListWidgetRow("Materials", 0, true, srcPage);
    // Is the page still ok
    testEntry("Priority", material, &domain::Material::priority, srcPage,
              "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, srcPage,
                     box::Constants::colourCyan);
    testEntry("Epsilon", material, &domain::Material::epsilon, srcPage,
              "2");
    testEntry("Relative Epsilon", srcPage, "2.25882e+11");
    testEntry("Mu", material, &domain::Material::mu, srcPage,
              "3");
    testEntry("Relative Mu", srcPage, "2.38732e+06");
    testEntry("Sigma", material, &domain::Material::sigma, srcPage,
              "4");
    testEntry("Sigma*", material, &domain::Material::sigmastar, srcPage,
              "5");
    // Do the relative boxes do the right thing
    testEntry("Relative Epsilon", srcPage,
              "2.25882e+11", "1");
    testEntry("Epsilon", material, &domain::Material::epsilon, srcPage,
              "8.85418782e-12");
    testEntry("Relative Mu", srcPage,
              "2.38732e+06", "1");
    testEntry("Mu", material, &domain::Material::mu, srcPage,
              "1.25663706e-06");
}

// Operation of the CPML boundary material sub-dialog
TEST_CASE("MaterialsDlg_CpmlBoundaryMaterial") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().materialsPage();
    selectNotebookItem(&app->mainWindow().sections(), 2);
    // Add a normal material
    clickMenuButton("New Material", 0, srcPage);
    testListWidgetRow("Materials", 0, {"0", "Default", "Default", "1"},
                      srcPage);
    testListWidgetRow("Materials", 1,
                      {"2", "CPML Bndry", "CPML Bndry", "1"},
                      srcPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal", "1"},
                      srcPage);
    // Select the CPML boundary material
    selectListWidgetRow("Materials", 1, true, srcPage);
    int id = getListWidgetCell<int>("Materials", 1, 0, srcPage);
    auto* material = app->model().d()->getMaterial(id);
    auto* boundaryMaterial = dynamic_cast<domain::CpmlMaterial*>(material);
    REQUIRE(material != nullptr);
    // Check the boundary sub dialog
    testEntry("Priority", material, &domain::Material::priority, srcPage,
              "0", "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, srcPage,
                     box::Constants::colourRed,
                     box::Constants::colourCyan);
    testEntry("Sigma Coeff", boundaryMaterial,
              &domain::CpmlMaterial::sigmaCoeff,
              srcPage, "0.3", "4");
    testEntry("Kappa Max", boundaryMaterial,
              &domain::CpmlMaterial::kappaMax,
              srcPage, "13.5", "5");
    testEntry("A Coeff Max", boundaryMaterial,
              &domain::CpmlMaterial::aMax,
              srcPage, "0.225", "6");
    testEntry("Sigma/Kappa M", boundaryMaterial,
              &domain::CpmlMaterial::msk,
              srcPage, "2.5", "7");
    testEntry("A Coeff M", boundaryMaterial,
              &domain::CpmlMaterial::ma,
              srcPage, "2", "8");
    // Switch to the normal material page and back
    selectListWidgetRow("Materials", 2, true, srcPage);
    selectListWidgetRow("Materials", 1, true, srcPage);
    // Is the page still ok
    testEntry("Priority", material, &domain::Material::priority, srcPage,
              "1");
    testComboBoxText("Display Colour", material,
                     &domain::Material::color, srcPage,
                     box::Constants::colourCyan);
    testEntry("Sigma Coeff", boundaryMaterial,
              &domain::CpmlMaterial::sigmaCoeff,
              srcPage, "4");
    testEntry("Kappa Max", boundaryMaterial,
              &domain::CpmlMaterial::kappaMax,
              srcPage, "5");
    testEntry("A Coeff Max", boundaryMaterial,
              &domain::CpmlMaterial::aMax,
              srcPage, "6");
    testEntry("Sigma/Kappa M", boundaryMaterial,
              &domain::CpmlMaterial::msk,
              srcPage, "7");
    testEntry("A Coeff M", boundaryMaterial,
              &domain::CpmlMaterial::ma,
              srcPage, "8");
}

