/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>

using namespace Catch::literals;

// Creating and destroying variables
TEST_CASE("VariablesDlg_NewAndDeleteVariables") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& varPage = app->mainWindow().variablesPage();
    selectNotebookItem(&app->mainWindow().sections(), 8);
    // Add three variables
    testButton("New Variable", varPage);
    testButton("New Variable", varPage);
    testButton("New Variable", varPage);
    testListWidgetRow("listview", 0, {"0", "Unnamed", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"1", "Unnamed", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Unnamed", "1.0", "1", ""},
                      varPage);
    // Change the names
    setListWidgetCell<const std::string&>("listview", 0, 1,
                                          "One", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 1,
                                          "Two", varPage);
    setListWidgetCell<const std::string&>("listview", 2, 1,
                                          "Three", varPage);
    testListWidgetRow("listview", 0, {"0", "One", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"1", "Two", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Three", "1.0", "1", ""},
                      varPage);
    // Cancel deletion of a variable
    selectListWidgetRow("listview", 2, true, varPage);
    appRequests.expectModalDialog("Delete Variable", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Variable", varPage);
    appRequests.checkModalDialog();
    testListWidgetRow("listview", 0, {"0", "One", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"1", "Two", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Three", "1.0", "1", ""},
                      varPage);
    testListWidgetRowSelected("listview", 2, true, varPage);
    // Delete a variable
    selectListWidgetRow("listview", 1, true, varPage);
    appRequests.expectModalDialog("Delete Variable", Gtk::RESPONSE_OK);
    clickButton("Delete Variable", varPage);
    appRequests.checkModalDialog();
    testListWidgetRow("listview", 0, {"0", "One", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"2", "Three", "1.0", "1", ""},
                      varPage);
    testListWidgetRowSelected("listview", 1, true, varPage);
}

// Editing variables
TEST_CASE("VariablesDlg_EditVariables") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& varPage = app->mainWindow().variablesPage();
    selectNotebookItem(&app->mainWindow().sections(), 8);
    // Add variable
    testButton("New Variable", varPage);
    testListWidgetRow("listview", 0, {"0", "Unnamed", "1.0", "1", ""},
                      varPage);
    // Change the configuration
    setListWidgetCell<const std::string&>("listview", 0, 1,
                                          "One", varPage);
    setListWidgetCell<const std::string&>("listview", 0, 2,
                                          "32", varPage);
    setListWidgetCell<const std::string&>("listview", 0, 4,
                                          "This is a comment", varPage);
    testListWidgetRow("listview", 0,
                      {"0", "One", "32", "32", "This is a comment"},
                      varPage);
    // Add variable
    testButton("New Variable", varPage);
    testListWidgetRow("listview", 1, {"1", "Unnamed", "1.0", "1", ""},
                      varPage);
    // Change the configuration
    setListWidgetCell<const std::string&>("listview", 1, 1,
                                          "Two", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 2,
                                          "2*One", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 4,
                                          "Another comment", varPage);
    testListWidgetRow("listview", 1,
                      {"1", "Two", "2*One", "64", "Another comment"},
                      varPage);
}

// Drag and drop variable reordering
TEST_CASE("VariablesDlg_DragAndDrop") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& varPage = app->mainWindow().variablesPage();
    selectNotebookItem(&app->mainWindow().sections(), 8);
    // Add three variables
    testButton("New Variable", varPage);
    testButton("New Variable", varPage);
    testButton("New Variable", varPage);
    testListWidgetRow("listview", 0, {"0", "Unnamed", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"1", "Unnamed", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Unnamed", "1.0", "1", ""},
                      varPage);
    // Change the names
    setListWidgetCell<const std::string&>("listview", 0, 1,
                                          "One", varPage);
    setListWidgetCell<const std::string&>("listview", 1, 1,
                                          "Two", varPage);
    setListWidgetCell<const std::string&>("listview", 2, 1,
                                          "Three", varPage);
    testListWidgetRow("listview", 0, {"0", "One", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"1", "Two", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Three", "1.0", "1", ""},
                      varPage);
    // Emulate the drag
    reorderListWidgetRow("listview", 1, 0, varPage);
    testListWidgetRow("listview", 0, {"1", "Two", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 1, {"0", "One", "1.0", "1", ""},
                      varPage);
    testListWidgetRow("listview", 2, {"2", "Three", "1.0", "1", ""},
                      varPage);
}

