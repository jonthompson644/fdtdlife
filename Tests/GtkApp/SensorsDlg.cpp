/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <Sensor/ArraySensor.h>
#include <Sensor/TwoDSlice.h>
#include <Sensor/MaterialSensor.h>
#include <Domain/SquarePlate.h>

using namespace Catch::literals;

// Creating and destroying sensors
TEST_CASE("SensorsDlg_NewAndDeleteSensor") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& ssrPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    // Add three array sensors
    clickMenuButton("New Sensor", 1, ssrPage);
    clickMenuButton("New Sensor", 1, ssrPage);
    clickMenuButton("New Sensor", 1, ssrPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 2, {"2", "Sensor 2", "Array Sensor"},
                      ssrPage);
    testListWidgetRowSelected("Sensors", 2, true, ssrPage);
    // Edit the name of the last shape
    int id = getListWidgetCell<int>("Sensors", 2, 0, ssrPage);
    auto* sensor = dynamic_cast<sensor::Sensor*>(app->model().sensors().find(id));
    REQUIRE(sensor != nullptr);
    selectListWidgetRow("Sensors", 2, true, ssrPage);
    setListWidgetCell<const std::string&>("Sensors", 2, 1,
                                          "Changed Name", ssrPage);
    REQUIRE(sensor->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Sensors", 2, 1,
                                          "Sensor 2", ssrPage);
    REQUIRE(sensor->name() == "Sensor 2");
    // Cancel deletion of the sensorId
    selectListWidgetRow("Sensors", 2, true, ssrPage);
    appRequests.expectModalDialog("Delete Sensor", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Sensor", ssrPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 2, {"2", "Sensor 2", "Array Sensor"},
                      ssrPage);
    testListWidgetRowSelected("Sensors", 2, true, ssrPage);
    // Delete the sensorId
    selectListWidgetRow("Sensors", 1, true, ssrPage);
    appRequests.expectModalDialog("Delete Sensor", Gtk::RESPONSE_OK);
    clickButton("Delete Sensor", ssrPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"2", "Sensor 2", "Array Sensor"},
                      ssrPage);
    testListWidgetRowSelected("Sensors", 1, true, ssrPage);
}

// Operation of the array sensorId sub-dialog
TEST_CASE("SensorsDlg_ArraySensor") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& ssrPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    // Add two array sensors
    clickMenuButton("New Sensor", 1, ssrPage);
    clickMenuButton("New Sensor", 1, ssrPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "Array Sensor"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "Array Sensor"},
                      ssrPage);
    // Select the first
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    int id = getListWidgetCell<int>("Sensors", 0, 0, ssrPage);
    auto* sensor = dynamic_cast<sensor::ArraySensor*>(app->model().sensors().find(id));
    REQUIRE(sensor != nullptr);
    // Check the sub dialog
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::ArraySensor::colour, ssrPage,
                     box::Constants::colourBlack, box::Constants::colourRed);
    testEntry("Num Points X", sensor,
              &sensor::ArraySensor::numPointsX, ssrPage,
              "1", "5");
    testEntry("Num Points Y", sensor,
              &sensor::ArraySensor::numPointsY, ssrPage,
              "1", "6");
    testEntry("Current Frequency", sensor,
              &sensor::ArraySensor::frequency, ssrPage,
              "1e+09", "7");
    testCheckButton("Average Over Cell", sensor,
                    &sensor::ArraySensor::averageOverCell, ssrPage,
                    false, true);
    testCheckButton("Include Reflection Data", sensor,
                    &sensor::ArraySensor::includeReflected, ssrPage,
                    false, true);
    testCheckButton("Use Window", sensor,
                    &sensor::ArraySensor::useWindow, ssrPage,
                    false, true);
    testCheckButton("Animate Frequency", sensor,
                    &sensor::ArraySensor::animateFrequency, ssrPage,
                    false, true);
    testEntry("Frequency Step", sensor,
              &sensor::ArraySensor::frequencyStep, ssrPage,
              "1e+09", "8");
    testEntryReadOnly("Transmitted Z (m)", ssrPage, true);
    testEntryReadOnly("Reflected Z (m)", ssrPage, true);
    testCheckButton("Manual Z", sensor,
                    &sensor::ArraySensor::manualZPos, ssrPage,
                    false, true);
    testEntryReadOnly("Transmitted Z (m)", ssrPage, false);
    testEntryReadOnly("Reflected Z (m)", ssrPage, false);
    testEntry("Transmitted Z (m)", sensor,
              &sensor::ArraySensor::transmittedZPos, ssrPage,
              "0", "9");
    testEntry("Reflected Z (m)", sensor,
              &sensor::ArraySensor::reflectedZPos, ssrPage,
              "0", "10");
    testCheckButton("Components: X", &sensor->componentSel(),
                    &box::ComponentSel::x, ssrPage,
                    true, false);
    testCheckButton("Y", &sensor->componentSel(),
                    &box::ComponentSel::y, ssrPage,
                    true, false);
    testCheckButton("Z", &sensor->componentSel(),
                    &box::ComponentSel::z, ssrPage,
                    true, false);
    testEntryReadOnly("Number Of Samples", ssrPage, true);
    testCheckButton("Manual Samples", sensor,
                    &sensor::ArraySensor::manualSamples, ssrPage,
                    false, true);
    testEntryReadOnly("Number Of Samples", ssrPage, false);
    testEntry("Number Of Samples", sensor,
              &sensor::ArraySensor::nSamples, ssrPage,
              "0", "11");
    // Switch to another sensorId and back
    selectListWidgetRow("Sensors", 1, true, ssrPage);
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    // Is the page still ok
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::ArraySensor::colour, ssrPage,
                     box::Constants::colourRed);
    testEntry("Num Points X", sensor,
              &sensor::ArraySensor::numPointsX, ssrPage,
              "5");
    testEntry("Num Points Y", sensor,
              &sensor::ArraySensor::numPointsY, ssrPage,
              "6");
    testEntry("Current Frequency", sensor,
              &sensor::ArraySensor::frequency, ssrPage,
              "7");
    testCheckButton("Average Over Cell", sensor,
                    &sensor::ArraySensor::averageOverCell, ssrPage,
                    true);
    testCheckButton("Include Reflection Data", sensor,
                    &sensor::ArraySensor::includeReflected, ssrPage,
                    true);
    testCheckButton("Use Window", sensor,
                    &sensor::ArraySensor::useWindow, ssrPage,
                    true);
    testCheckButton("Animate Frequency", sensor,
                    &sensor::ArraySensor::animateFrequency, ssrPage,
                    true);
    testEntry("Frequency Step", sensor,
              &sensor::ArraySensor::frequencyStep, ssrPage,
              "8");
    testCheckButton("Manual Z", sensor,
                    &sensor::ArraySensor::manualZPos, ssrPage,
                    true);
    testEntryReadOnly("Transmitted Z (m)", ssrPage, false);
    testEntryReadOnly("Reflected Z (m)", ssrPage, false);
    testEntry("Transmitted Z (m)", sensor,
              &sensor::ArraySensor::transmittedZPos, ssrPage,
              "9");
    testEntry("Reflected Z (m)", sensor,
              &sensor::ArraySensor::reflectedZPos, ssrPage,
              "10");
    testCheckButton("Components: X", &sensor->componentSel(),
                    &box::ComponentSel::x, ssrPage,
                    false);
    testCheckButton("Y", &sensor->componentSel(),
                    &box::ComponentSel::y, ssrPage,
                    false);
    testCheckButton("Z", &sensor->componentSel(),
                    &box::ComponentSel::z, ssrPage,
                    false);
    testCheckButton("Manual Samples", sensor,
                    &sensor::ArraySensor::manualSamples, ssrPage,
                    true);
    testEntryReadOnly("Number Of Samples", ssrPage, false);
    testEntry("Number Of Samples", sensor,
              &sensor::ArraySensor::nSamples, ssrPage,
              "11");
    // The copy to clipboard buttons
    clickMenuButton("Copy To Clipboard", 0, ssrPage);
    REQUIRE(app->model().clipboard().getText() == "\t0,0\t\t\t0,1\t\t\t0,2\t\t\t0,3\t\t\t0,4\t\t\t1,0\t\t\t1,1\t\t\t1,2\t\t\t1,3\t\t\t1,4\t\t\t2,0\t\t\t2,1\t\t\t2,2\t\t\t2,3\t\t\t2,4\t\t\t3,0\t\t\t3,1\t\t\t3,2\t\t\t3,3\t\t\t3,4\t\t\t4,0\t\t\t4,1\t\t\t4,2\t\t\t4,3\t\t\t4,4\t\t\t5,0\t\t\t5,1\t\t\t5,2\t\t\t5,3\t\t\t5,4\t\t\t\n"
                                                  "frequency\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\t\n");
    clickMenuButton("Copy To Clipboard", 1, ssrPage);
    REQUIRE(app->model().clipboard().getText() == "0,0\t\t\t0,1\t\t\t0,2\t\t\t0,3\t\t\t0,4\t\t\t1,0\t\t\t1,1\t\t\t1,2\t\t\t1,3\t\t\t1,4\t\t\t2,0\t\t\t2,1\t\t\t2,2\t\t\t2,3\t\t\t2,4\t\t\t3,0\t\t\t3,1\t\t\t3,2\t\t\t3,3\t\t\t3,4\t\t\t4,0\t\t\t4,1\t\t\t4,2\t\t\t4,3\t\t\t4,4\t\t\t5,0\t\t\t5,1\t\t\t5,2\t\t\t5,3\t\t\t5,4\t\t\t\n"
                                                  "x\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\t\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n");
    clickMenuButton("Copy To Clipboard", 2, ssrPage);
    REQUIRE(app->model().clipboard().getText().empty());
    clickMenuButton("Copy To Clipboard", 3, ssrPage);
    REQUIRE(app->model().clipboard().getText() == "\t0,0\t\t\t0,1\t\t\t0,2\t\t\t0,3\t\t\t0,4\t\t\t1,0\t\t\t1,1\t\t\t1,2\t\t\t1,3\t\t\t1,4\t\t\t2,0\t\t\t2,1\t\t\t2,2\t\t\t2,3\t\t\t2,4\t\t\t3,0\t\t\t3,1\t\t\t3,2\t\t\t3,3\t\t\t3,4\t\t\t4,0\t\t\t4,1\t\t\t4,2\t\t\t4,3\t\t\t4,4\t\t\t5,0\t\t\t5,1\t\t\t5,2\t\t\t5,3\t\t\t5,4\t\t\t\n"
                                                  "frequency\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\t\n");
    clickMenuButton("Copy To Clipboard", 4, ssrPage);
    REQUIRE(app->model().clipboard().getText() == "0,0\t\t\t0,1\t\t\t0,2\t\t\t0,3\t\t\t0,4\t\t\t1,0\t\t\t1,1\t\t\t1,2\t\t\t1,3\t\t\t1,4\t\t\t2,0\t\t\t2,1\t\t\t2,2\t\t\t2,3\t\t\t2,4\t\t\t3,0\t\t\t3,1\t\t\t3,2\t\t\t3,3\t\t\t3,4\t\t\t4,0\t\t\t4,1\t\t\t4,2\t\t\t4,3\t\t\t4,4\t\t\t5,0\t\t\t5,1\t\t\t5,2\t\t\t5,3\t\t\t5,4\t\t\t\n"
                                                  "x\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\tx\ty\tz\t\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n"
                                                  "\n");

}

// Operation of the two D slice sensorId sub-dialog
TEST_CASE("SensorsDlg_TwoDSlice") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& ssrPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    // Add two 2D slice sensors
    clickMenuButton("New Sensor", 0, ssrPage);
    clickMenuButton("New Sensor", 0, ssrPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "2D Slice"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "2D Slice"},
                      ssrPage);
    // Select the first
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    int id = getListWidgetCell<int>("Sensors", 0, 0, ssrPage);
    auto* sensor = dynamic_cast<sensor::TwoDSlice*>(app->model().sensors().find(id));
    REQUIRE(sensor != nullptr);
    // Check the sub dialog
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::TwoDSlice::colour, ssrPage,
                     box::Constants::colourBlack, box::Constants::colourRed);
    testComboBoxText("Orientation", sensor,
                     &sensor::TwoDSlice::orientation, ssrPage,
                     box::Constants::orientationXPlane,
                     box::Constants::orientationZPlane);
    testComboBoxText("Data Source", sensor,
                     &sensor::TwoDSlice::dataSource, ssrPage,
                     sensor::TwoDSlice::eMagnitude, sensor::TwoDSlice::ey);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "0", "5");
    // Switch to another sensorId and back
    selectListWidgetRow("Sensors", 1, true, ssrPage);
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    // Is the page still ok
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::TwoDSlice::colour, ssrPage,
                     box::Constants::colourRed);
    testComboBoxText("Orientation", sensor,
                     &sensor::TwoDSlice::orientation, ssrPage,
                     box::Constants::orientationZPlane);
    testComboBoxText("Data Source", sensor,
                     &sensor::TwoDSlice::dataSource, ssrPage,
                     sensor::TwoDSlice::ey);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "5");
    // Test the layer+ and layer- buttons.
    // We need some layers at different locations
    auto& shapesPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    auto* shape1 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 0, 0, shapesPage)));
    auto* shape2 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 1, 0, shapesPage)));
    auto* shape3 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 2, 0, shapesPage)));
    auto* shape4 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 3, 0, shapesPage)));
    shape1->center({"0", "0", "0"});
    shape2->center({"0", "0", "1"});
    shape3->center({"0", "0", "2"});
    shape4->center({"0", "0", "3"});
    // Back to the 2D slice page
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "5", "0");
    selectNotebookItem(&app->mainWindow().sections(), 4);
    testButton("Layer+", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "1");
    testButton("Layer+", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "2");
    testButton("Layer+", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "3");
    testButton("Layer+", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "3");
    testButton("Layer-", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "2");
    testButton("Layer-", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "1");
    testButton("Layer-", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "0");
    testButton("Layer-", ssrPage);
    testEntry("Offset", sensor,
              &sensor::TwoDSlice::offset, ssrPage,
              "0");
}

// Operation of the material sensorId sub-dialog
TEST_CASE("SensorsDlg_Material") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& ssrPage = app->mainWindow().sensorsPage();
    selectNotebookItem(&app->mainWindow().sections(), 4);
    // Add two 2D slice sensors
    clickMenuButton("New Sensor", 2, ssrPage);
    clickMenuButton("New Sensor", 2, ssrPage);
    testListWidgetRow("Sensors", 0, {"0", "Sensor 0", "2D Material"},
                      ssrPage);
    testListWidgetRow("Sensors", 1, {"1", "Sensor 1", "2D Material"},
                      ssrPage);
    // Select the first
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    int id = getListWidgetCell<int>("Sensors", 0, 0, ssrPage);
    auto* sensor = dynamic_cast<sensor::MaterialSensor*>(app->model().sensors().find(id));
    REQUIRE(sensor != nullptr);
    // Check the sub dialog
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::TwoDSlice::colour, ssrPage,
                     box::Constants::colourBlack, box::Constants::colourRed);
    testComboBoxText("Orientation", dynamic_cast<sensor::TwoDSlice*>(sensor),
                     &sensor::TwoDSlice::orientation, ssrPage,
                     box::Constants::orientationXPlane,
                     box::Constants::orientationZPlane);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "0", "5");
    // Switch to another sensorId and back
    selectListWidgetRow("Sensors", 1, true, ssrPage);
    selectListWidgetRow("Sensors", 0, true, ssrPage);
    // Is the page still ok
    testComboBoxText("Display Colour", dynamic_cast<sensor::Sensor*>(sensor),
                     &sensor::TwoDSlice::colour, ssrPage,
                     box::Constants::colourRed);
    testComboBoxText("Orientation", dynamic_cast<sensor::TwoDSlice*>(sensor),
                     &sensor::TwoDSlice::orientation, ssrPage,
                     box::Constants::orientationZPlane);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "5");
    // Test the layer+ and layer- buttons.
    // We need some layers at different locations
    auto& shapesPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    clickMenuButton("New Shape", 1, shapesPage);
    auto* shape1 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 0, 0, shapesPage)));
    auto* shape2 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 1, 0, shapesPage)));
    auto* shape3 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 2, 0, shapesPage)));
    auto* shape4 = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(
            getListWidgetCell<int>("Shapes", 3, 0, shapesPage)));
    shape1->center({"0", "0", "0"});
    shape2->center({"0", "0", "1"});
    shape3->center({"0", "0", "2"});
    shape4->center({"0", "0", "3"});
    // Back to the 2D slice page
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "5", "0");
    selectNotebookItem(&app->mainWindow().sections(), 4);
    testButton("Layer+", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "1");
    testButton("Layer+", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "2");
    testButton("Layer+", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "3");
    testButton("Layer+", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "3");
    testButton("Layer-", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "2");
    testButton("Layer-", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "1");
    testButton("Layer-", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "0");
    testButton("Layer-", ssrPage);
    testEntry("Offset", dynamic_cast<sensor::TwoDSlice*>(sensor),
              &sensor::TwoDSlice::offset, ssrPage,
              "0");
}
