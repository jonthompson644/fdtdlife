/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GtkLib.h"
#include <gtkmm/button.h>
#include <gtkmm/liststore.h>
#include <GtkGui/ListWidget.h>
#include <GtkGui/TestSuite.h>
#include <GtkGui/BinaryPatternWidget.h>
#include <gtkmm/main.h>
#include <GtkGui/CustomWidget.h>
#include <GtkGui/NamedToolButton.h>

// Run the GTK loop to process events for at least the time delay in milliseconds.
// The special value UNTIL_CONTINUE runs the loop until the continue button toolbar
// button is pressed in the application.
void processEvents(long long delay) {
    if(delay == UNTIL_CONTINUE) {
        gtk_main();
    } else {
        for(int i=0; i<10 && gtk_events_pending(); i++) {
            gtk_main_iteration();
        }
        if(delay > 0) {
            std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            for(int i=0; i<10 && gtk_events_pending(); i++) {
                gtk_main_iteration();
            }
        }
    }
}

// Set an entry's contents and activate it
void setEntryText(Gtk::Widget* widget, const std::string& text, long long delay) {
    auto* entry = dynamic_cast<Gtk::Entry*>(widget);
    REQUIRE(entry != nullptr);
    entry->set_text(text);
    entry->activate();
    processEvents(delay);
}

// Set the active state of a checkbox
void setCheckActive(Gtk::Widget* widget, bool active, long long delay) {
    auto* checkButton = dynamic_cast<Gtk::ToggleButton*>(widget);
    if(checkButton->get_active() != active) {
        checkButton->set_active(active);
        // Note that set_active does emit the toggled signal
    }
    processEvents(delay);
}

// Set the active state of a checkbox
void setCheckActive(const std::string& name, bool value, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<Gtk::ToggleButton*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    if(widget->get_active() != value) {
        widget->set_active(value);
        // Note that set_active does emit the toggled signal
    }
}

// Select an item from notebook list and process events
void selectNotebookItem(Gtk::Widget* widget, int n) {
    auto* notebook = dynamic_cast<Gtk::Notebook*>(widget);
    notebook->set_current_page(n);
    processEvents(0);
}

// Select an item from a combo box list and process events
void selectComboItem(Gtk::Widget* widget, int n, long long delay) {
    auto* comboBox = dynamic_cast<Gtk::ComboBoxText*>(widget);
    REQUIRE(comboBox != nullptr);
    comboBox->set_active(n);
    // Note that this emits the changed signal
    processEvents(delay);
}

// Set the current combo item
void setComboBoxItem(const std::string& name, int n, GuiElement& page) {
    selectComboItem(page.findWidget(name), n, 0);
}

// Get the current combo item
int getComboBoxItem(const std::string& name, GuiElement& page) {
    auto* comboBox = dynamic_cast<Gtk::ComboBoxText*>(page.findWidget(name));
    REQUIRE(comboBox != nullptr);
    return comboBox->get_active_row_number();
}

// Get the text from an Entry widget
std::string getEntryText(Gtk::Widget* widget) {
    std::string result{};
    auto* entry = dynamic_cast<Gtk::Entry*>(widget);
    if(entry != nullptr) {
        result = entry->get_text();
    }
    return result;
}

// Get the check state from a checkbox widget
bool getCheckActive(Gtk::Widget* widget) {
    bool result = false;
    auto* check = dynamic_cast<Gtk::ToggleButton*>(widget);
    if(check != nullptr) {
        result = check->get_active();
    }
    return result;
}

// Return the check state of a check box
bool getCheckActive(const std::string& name, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<Gtk::ToggleButton*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    return widget->get_active();
}

// Test an entry widget
void testEntry(const std::string& name, GuiElement& page, const std::string& expected) {
    INFO(name)
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE(getEntryText(widget) == expected);
}

// Test an entry widget
void testEntry(const std::string& name, GuiElement& page, const std::string& oldText,
               const std::string& newText) {
    INFO(name)
    auto* widget = page.findWidget(name);
    REQUIRE(widget != nullptr);
    REQUIRE(getEntryText(widget) == oldText);
    setEntryText(widget, newText);
}

// Click a button
void testButton(const std::string& name, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<Gtk::Button*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    widget->grab_focus();
    widget->clicked();
    processEvents(0);
}

// Click a toolbar button
void clickToolButton(const std::string& name, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<NamedToolButton*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    widget->grab_focus();
    widget->click();
    processEvents(0);
}

// Click a toolbar menu button
void clickToolMenuButton(const std::string& name, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<NamedToolMenuButton<std::string>*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    widget->grab_focus();
    widget->on_clicked();
    processEvents(0);
}

// Click a toolbar menu button
void clickToolMenuButton(const std::string& name, const std::string& itemName,
        GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<NamedToolMenuButton<std::string>*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    widget->grab_focus();
    widget->onMenuSelect(itemName);
    processEvents(0);
}

// Click a menu button item
void clickMenuButton(const std::string& name, int n, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<Gtk::MenuButton*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto* menu = widget->get_menu();
    REQUIRE(menu != nullptr);
    REQUIRE(n >= 0);
    REQUIRE(n < menu->get_children().size());
    auto* menuItem = dynamic_cast<Gtk::MenuItem*>(menu->get_children()[n]);
    REQUIRE(menuItem != nullptr);
    menuItem->activate();
    processEvents(0);
}

// Click a radio button item
// The name should be the name of the first radio button in the group
void clickRadioButton(const std::string& name, int n, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<NamedRadioButton*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    REQUIRE(n >= 0);
    REQUIRE(static_cast<size_t>(n) < widget->slaves().size());
    widget->slaves()[n].get().clicked();
    processEvents(0);
}

// Click a menu  item
void clickMenu(const std::string& name, int n, GuiElement& page) {
    INFO(name)
    auto* menu = dynamic_cast<Gtk::Menu*>(page.findWidget(name));
    REQUIRE(menu != nullptr);
    REQUIRE(n >= 0);
    REQUIRE(static_cast<size_t>(n) < menu->get_children().size());
    auto* menuItem = dynamic_cast<Gtk::MenuItem*>(menu->get_children()[n]);
    REQUIRE(menuItem != nullptr);
    menuItem->activate();
    processEvents(0);
}

// Click a button item
void clickButton(const std::string& name, GuiElement& page) {
    INFO(name)
    auto* widget = dynamic_cast<Gtk::Button*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    widget->clicked();
    processEvents(0);
}

// Test the contents of a tree view row
void testListWidgetRow(const std::string& name, int n,
                       const std::vector<std::string>& contents,
                       GuiElement& page) {
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto cols = widget->get_columns();
    const GType* colTypes = widget->columns()->types();
    unsigned int numCols = widget->columns()->size();
    REQUIRE(numCols >= contents.size());
    REQUIRE(cols.size() >= contents.size());
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(n >= 0);
    REQUIRE(n < rows.size());
    Gtk::TreeRow row = rows[n];
    for(size_t i = 0; i < contents.size(); i++) {
        std::stringstream info;
        info << name << " row=" << n << " col=" << i;
        INFO(info.str())
        auto colType = colTypes[i];
        switch(colType) {
            case G_TYPE_INT: {
                std::stringstream text(contents[i]);
                int testData;
                text >> testData;
                int data;
                row.get_value(i, data);
                REQUIRE(data == testData);
                break;
            }
            case G_TYPE_ULONG: {
                std::stringstream text(contents[i]);
                unsigned long testData;
                text >> testData;
                unsigned long data;
                row.get_value(i, data);
                REQUIRE(data == testData);
                break;
            }
            case G_TYPE_BOOLEAN: {
                std::stringstream text(contents[i]);
                bool testData;
                text >> testData;
                bool data;
                row.get_value(i, data);
                REQUIRE(data == testData);
                break;
            }
            case G_TYPE_STRING: {
                std::string data;
                row.get_value(i, data);
                REQUIRE(data == contents[i]);
                break;
            }
            case G_TYPE_DOUBLE: {
                std::stringstream text(contents[i]);
                double testData;
                text >> testData;
                double data;
                row.get_value(i, data);
                REQUIRE(data == Approx(testData));
                break;
            }
            default:
                REQUIRE(FALSE);
                break;
        }
    }
}

// Return the contents of a cell in a row of a list widget
template<class T>
T getListWidgetCell(const std::string& name, int n, int col, GuiElement& page) {
    T result{};
    std::stringstream info;
    info << name << " row=" << n << " col=" << col;
    INFO(info.str())
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto cols = widget->get_columns();
    unsigned int numCols = widget->columns()->size();
    REQUIRE(col >= 0);
    REQUIRE(col < static_cast<int>(numCols));
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(n >= 0);
    REQUIRE(n < static_cast<int>(rows.size()));
    Gtk::TreeRow row = rows[n];
    row.get_value(col, result);
    return result;
}
template int getListWidgetCell<int>(const std::string& name, int n, int col,
                                    GuiElement& page);
template double getListWidgetCell<double>(const std::string& name, int n, int col,
                                          GuiElement& page);
template std::string getListWidgetCell<std::string>(const std::string& name, int n, int col,
                                                    GuiElement& page);

// Set the contents of a cell in a row of a list widget
template<class T>
void setListWidgetCell(const std::string& name, int n, int col, T value, GuiElement& page) {
    std::stringstream info;
    info << name << " row=" << n << " col=" << col;
    INFO(info.str())
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto cols = widget->get_columns();
    unsigned int numCols = widget->columns()->size();
    REQUIRE(col >= 0);
    REQUIRE(col < static_cast<int>(numCols));
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(n >= 0);
    REQUIRE(n < static_cast<int>(rows.size()));
    Gtk::TreeRow row = rows[n];
    row.set_value(col, value);
    listStore->row_changed(listStore->get_path(row), row);
}
template void setListWidgetCell<int>(const std::string& name, int n, int col,
                                     int value, GuiElement& page);
template void setListWidgetCell<bool>(const std::string& name, int n, int col,
                                     bool value, GuiElement& page);
template void setListWidgetCell<double>(const std::string& name, int n, int col,
                                        double value, GuiElement& page);
template void setListWidgetCell<const std::string&>(const std::string& name, int n, int col,
                                                    const std::string& value,
                                                    GuiElement& page);

// Check the list widget has no rows
void testListWidgetEmpty(const std::string& name, GuiElement& page) {
    INFO(name);
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(rows.empty());
}

// Check the list has the specified number of rows
void testListWidgetNumRows(const std::string& name, size_t numRows, GuiElement& page) {
    INFO(name);
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(rows.size() == numRows);
}

// Check the list has the specified number of columns
void testListWidgetNumCols(const std::string& name, size_t numCols, GuiElement& page) {
    INFO(name);
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto cols = widget->get_columns();
    REQUIRE(cols.size() == numCols);
}

// Select a row in a tree view
void selectListWidgetRow(const std::string& name, int n, bool select, GuiElement& page) {
    INFO(name);
    INFO(n);
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(n >= 0);
    REQUIRE(n < rows.size());
    Gtk::TreeRow row = rows[n];
    if(select) {
        widget->get_selection()->select(row);
    } else {
        widget->get_selection()->unselect(row);
    }
    processEvents(0);
}

// Emulate drag and drop row reordering
void reorderListWidgetRow(const std::string& name, int fromRow, int toRow, GuiElement& page) {
    INFO(name);
    INFO(fromRow);
    INFO(toRow);
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    // Get the rows
    auto rows = listStore->children();
    REQUIRE(fromRow >= 0);
    REQUIRE(fromRow < rows.size());
    REQUIRE(toRow >= 0);
    REQUIRE(toRow < rows.size());
    // Select the from row
    widget->get_selection()->select(rows[fromRow]);
    // Insert at the to row
    listStore->insert(std::next(rows.begin(), toRow));
    // Finish the drag operation
    Glib::RefPtr<Gdk::DragContext> context;
    widget->on_drag_end(context);
}

// Test the selection state of a row in a tree view
void testListWidgetRowSelected(const std::string& name, int n, bool selected,
                               GuiElement& page) {
    auto* widget = dynamic_cast<ListWidget*>(page.findWidget(name));
    REQUIRE(widget != nullptr);
    std::stringstream path;
    path << n;
    auto row = widget->model()->get_iter(path.str());
    REQUIRE(widget->model()->iter_is_valid(row));
    REQUIRE(widget->get_selection()->is_selected(row) == selected);
}

// Make and initialise the application
FdtdApp* makeApp(TestSuite* testSuite) {
    auto* app = new FdtdApp(0, nullptr, testSuite);
    gtk_widget_show_all(GTK_WIDGET(app->mainWindow().gobj()));
    processEvents(0);
    return app;
}

// Test the entry control is read only
void testEntryReadOnly(const std::string& name, GuiElement& page, bool readOnly) {
    auto* widget = dynamic_cast<Gtk::Entry*>(page.findWidget(name));
    INFO(name)
    REQUIRE(widget != nullptr);
    REQUIRE(!widget->get_editable() == readOnly);
    REQUIRE(!widget->get_can_focus() == readOnly);
}

// Click a cell in a binary pattern widget
void clickBinaryPatternCell(const std::string& name, int x, int y, GuiElement& page) {
    auto* widget = dynamic_cast<BinaryPatternWidget*>(page.findWidget(name));
    INFO(name)
    REQUIRE(widget != nullptr);
    GdkEventButton event;
    event.x = (x + 0.5) * widget->pixelsPerBit();
    event.y = (y + 0.5) * widget->pixelsPerBit();
    event.type = GDK_BUTTON_PRESS;
    event.window = widget->get_window().get()->gobj();
    event.send_event = true;
    event.time = 0;
    event.axes = nullptr;
    event.button = 1;
    widget->on_button_press_event(&event);
    while(Gtk::Main::events_pending()) {
        Gtk::Main::iteration();
    }
    processEvents(1000);
}

// Click with the left mouse button
void clickLeftMouseButton(CustomWidget& widget, double x, double y) {
    GdkEventButton event;
    event.x = x;
    event.y = y;
    event.type = GDK_BUTTON_PRESS;
    event.window = widget.get_window().get()->gobj();
    event.send_event = true;
    event.time = 0;
    event.axes = nullptr;
    event.button = 1;
    widget.on_button_press_event(&event);
    while(Gtk::Main::events_pending()) {
        Gtk::Main::iteration();
    }
}

// Get the current value of a cell in the binary pattern widget
bool getBinaryPatternCell(const std::string& name, int x, int y, GuiElement& page) {
    auto* widget = dynamic_cast<BinaryPatternWidget*>(page.findWidget(name));
    INFO(name)
    REQUIRE(widget != nullptr);
    return widget->pattern().get(static_cast<size_t>(x), static_cast<size_t>(y));
}

// Test the contents of the combo box match the expected
void testComboBoxContents(const std::string& name, const std::vector<std::string>& contents,
                          GuiElement& page) {
    auto* widget = dynamic_cast<Gtk::ComboBoxText*>(page.findWidget(name));
    INFO(name)
    REQUIRE(widget != nullptr);
    auto* listStore = dynamic_cast<Gtk::ListStore*>(widget->get_model().get());
    REQUIRE(listStore != nullptr);
    auto rows = listStore->children();
    REQUIRE(contents.size() == rows.size());
    for(size_t i = 0; i < contents.size(); i++) {
        Gtk::TreeRow row = rows[i];
        std::stringstream info;
        info << name << " row=" << i;
        INFO(info.str())
        std::string data;
        row.get_value(0, data);
        REQUIRE(data == contents[i]);
    }
}

// Check the custom widget contents against a stored pixmap.
// If the file doe snot exist, the current contents are stored.
bool testPixmap(CustomWidget& widget, const std::string& name) {
    bool result = true;
    // Does the file exist?
    if(box::Utility::exists(name + ".png")) {
        // Read the file
        try {
            processEvents(200);
            // Get the reference and the current
            auto reference = Gdk::Pixbuf::create_from_file(name + ".png");
            auto current = widget.capture();
            // Are they different?
            if(reference->get_width() == current->get_width() &&
               reference->get_height() == current->get_height() &&
               reference->get_bits_per_sample() == current->get_bits_per_sample() &&
               reference->get_n_channels() == current->get_n_channels()) {
                // Same dimensions, check the pixels
                int strideRef = reference->get_rowstride();
                int strideCur = current->get_rowstride();
                int numChannels = reference->get_n_channels();
                unsigned char* dRef = reference->get_pixels();
                unsigned char* dCur = current->get_pixels();
                result = true;
                for(int row = 0; row < reference->get_height() && result; row++) {
                    for(int col = 0; col < reference->get_width() && result; col++) {
                        for(int chan = 0; chan < numChannels && result; chan++) {
                            result = dRef[row * strideRef + col * numChannels + chan] ==
                                    dCur[row * strideCur + col * numChannels + chan];
                        }
                    }
                }
            }
            // If we failed, save the current for reference purposes
            if(!result) {
                current->save(name + "_failed.png", "png");
            }
        } catch(Glib::FileError& e) {
        } catch(Gdk::PixbufError& e) {
        }
    } else {
        // Store the file
        auto current = widget.capture();
        current->save(name + ".png", "png");
    }
    return result;
}

