/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <gtkmm/dialog.h>
#include "catch.hpp"
#include "GtkLib.h"

using namespace Catch::literals;

// Operation of tables on the parameters page
TEST_CASE("ParametersDlg_Tables") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add three tables
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 2", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 2, true, paramPage);
    // Change the contents of the last table
    selectListWidgetRow("Parameter Tables", 2, true, paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 2, 0, paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    setListWidgetCell<const std::string&>("Parameter Tables", 2, 1,
                                          "Changed Name", paramPage);
    setListWidgetCell<double>("Parameter Tables", 2, 2,
                              0.1, paramPage);
    setListWidgetCell<double>("Parameter Tables", 2, 3,
                              0.2, paramPage);
    setListWidgetCell<double>("Parameter Tables", 2, 4,
                              0.3, paramPage);
    setListWidgetCell<double>("Parameter Tables", 2, 5,
                              0.4, paramPage);
    REQUIRE(table->name() == "Changed Name");
    REQUIRE(table->unitCell() == 0.1_a);
    REQUIRE(table->layerSpacing() == 0.2_a);
    REQUIRE(table->incidenceAngle() == 0.3_a);
    REQUIRE(table->patchRatio() == 0.4_a);
    // Cancel deletion of the last table
    selectListWidgetRow("Parameter Tables", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Parameter Table",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Delete Table", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(app->model().d()->parameterTables().size() == 3);
    // Delete the last table
    selectListWidgetRow("Parameter Tables", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Parameter Table",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Table", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(app->model().d()->parameterTables().size() == 2);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 1, true, paramPage);
    // Delete the first table
    selectListWidgetRow("Parameter Tables", 0, true, paramPage);
    appRequests.expectModalDialog("Delete Parameter Table",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Table", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(app->model().d()->parameterTables().size() == 1);
    testListWidgetRow("Parameter Tables", 0,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true, paramPage);
}

// Operation of curves on the parameters page
TEST_CASE("ParametersDlg_Curves") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true,
                              paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 0, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    // Add three curves to the table
    clickButton("New Curve", paramPage);
    clickButton("New Curve", paramPage);
    clickButton("New Curve", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 1,
                      {"1", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 2,
                      {"2", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 2, true, paramPage);
    // Change the contents of the last curve
    selectListWidgetRow("Table Curves", 2, true, paramPage);
    id = getListWidgetCell<int>("Table Curves", 2, 0,
                                paramPage);
    auto* curve = table->find(id);
    REQUIRE(curve != nullptr);
    setListWidgetCell<const std::string&>("Table Curves", 2, 1,
                                          "Permeability", paramPage);
    REQUIRE(curve->which() == domain::ParameterCurve::Which::permeability);
    // Cancel deletion of the last item
    selectListWidgetRow("Table Curves", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Curve",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Delete Curve", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(table->size() == 3);
    // Delete the last item
    selectListWidgetRow("Table Curves", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Curve",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Curve", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(table->size() == 2);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 1,
                      {"1", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 1, true,
                              paramPage);
    // Delete the first item
    selectListWidgetRow("Table Curves", 0, true, paramPage);
    appRequests.expectModalDialog("Delete Curve",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Curve", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(table->size() == 1);
    testListWidgetRow("Table Curves", 0,
                      {"1", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 0, true,
                              paramPage);
}

// Operation of points on the parameters page
TEST_CASE("ParametersDlg_Points") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true,
                              paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 0, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    // Add a curve
    clickButton("New Curve", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 0, true, paramPage);
    id = getListWidgetCell<int>("Table Curves", 0, 0, paramPage);
    auto* curve = table->find(id);
    REQUIRE(curve != nullptr);
    // Add three points to the curve
    clickButton("New Point", paramPage);
    clickButton("New Point", paramPage);
    clickButton("New Point", paramPage);
    testListWidgetRow("Curve Points", 0,
                      {"0", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRow("Curve Points", 1,
                      {"1", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRow("Curve Points", 2,
                      {"2", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRowSelected("Curve Points", 2, true, paramPage);
    // Change the contents of the last point
    selectListWidgetRow("Curve Points", 2, true, paramPage);
    id = getListWidgetCell<int>("Curve Points", 2, 0, paramPage);
    auto* point = curve->find(id);
    REQUIRE(point != nullptr);
    setListWidgetCell<double>("Curve Points", 2, 1,
                              500, paramPage);
    setListWidgetCell<double>("Curve Points", 2, 2,
                              501, paramPage);
    setListWidgetCell<double>("Curve Points", 2, 3,
                              502, paramPage);
    REQUIRE(point->frequency() == 500.0_a);
    REQUIRE(point->value().real() == 501.0_a);
    REQUIRE(point->value().imag() == 502.0_a);
    // Cancel deletion of the last point
    selectListWidgetRow("Curve Points", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Point",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Delete Point", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(curve->size() == 3);
    // Delete the last item
    selectListWidgetRow("Curve Points", 2, true, paramPage);
    appRequests.expectModalDialog("Delete Point",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Point", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(curve->size() == 2);
    testListWidgetRow("Curve Points", 0,
                      {"0", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRow("Curve Points", 1,
                      {"1", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRowSelected("Curve Points", 1, true,
                              paramPage);
    // Delete the first item
    selectListWidgetRow("Curve Points", 0, true, paramPage);
    appRequests.expectModalDialog("Delete Point",
                                  Gtk::RESPONSE_OK);
    clickButton("Delete Point", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(curve->size() == 1);
    testListWidgetRow("Curve Points", 0,
                      {"1", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRowSelected("Curve Points", 0, true,
                              paramPage);
}

// Operation of paste from excel on the parameters page
TEST_CASE("ParametersDlg_PasteExcel") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Paste a table
    Clipboard c;
    c.put("\t0\t6.25\t18.75\t31.25\t43.75\t56.25\t68.75\t81.25\t93.75\n"
          "10\t3.449637662\t3.459825389\t3.485056384\t3.550624408\t3.700308708\t"
          "3.960915386\t4.388965561\t5.04289687\t6.274832908\n"
          "20\t3.449637662\t3.459865434\t3.485027471\t3.550692139\t3.700538171\t"
          "3.961618729\t4.390565564\t5.046136567\t6.282919125");
    clickMenuButton("Paste From Excel", 1, paramPage);
    // Are the tables there?
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "6.250000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 2", "0.000050", "0.000100", "0.000000", "18.750000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 3,
                      {"3", "Table 3", "0.000050", "0.000100", "0.000000", "31.250000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 4,
                      {"4", "Table 4", "0.000050", "0.000100", "0.000000", "43.750000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 5,
                      {"5", "Table 5", "0.000050", "0.000100", "0.000000", "56.250000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 6,
                      {"6", "Table 6", "0.000050", "0.000100", "0.000000", "68.750000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 7,
                      {"7", "Table 7", "0.000050", "0.000100", "0.000000", "81.250000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 8,
                      {"8", "Table 8", "0.000050", "0.000100", "0.000000", "93.750000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 8, true, paramPage);
    REQUIRE(app->model().d()->parameterTables().size() == 9);
    // Is the curve there?
    int id = getListWidgetCell<int>("Parameter Tables", 8, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permeability"}, paramPage);
    testListWidgetRowSelected("Table Curves", 0, true, paramPage);
    REQUIRE(table->size() == 1);
    // Are the points there?
    id = getListWidgetCell<int>("Table Curves", 0, 0, paramPage);
    auto* curve = table->find(id);
    REQUIRE(curve != nullptr);
    testListWidgetRow("Curve Points", 0,
                      {"0", "10000000000.000000", "6.274832908", "0.000000"},
                      paramPage);
    testListWidgetRow("Curve Points", 1,
                      {"1", "20000000000.000000", "6.282919125", "0.000000"},
                      paramPage);
    REQUIRE(curve->size() == 2);
}

// Operation of table copy/paste
TEST_CASE("ParametersDlg_TableCopyPaste") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true, paramPage);
    // Copy and paste this table
    clickButton("Copy Table", paramPage);
    clickButton("Paste Tables", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 1, true, paramPage);
    // Copy and paste all the tables
    clickButton("Copy All Tables", paramPage);
    clickButton("Paste Tables", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 3,
                      {"3", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
}

// Operation of curve copy/paste
TEST_CASE("ParametersDlg_CurveCopyPaste") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true, paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 0, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    // Add a curve to the table
    clickButton("New Curve", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 0, true, paramPage);
    // Copy and paste this curve
    clickButton("Copy Curve", paramPage);
    clickButton("Paste Curves", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 1,
                      {"1", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 1, true, paramPage);
}

// Operation of table clearing
TEST_CASE("ParametersDlg_ClearTables") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add three tables
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 2", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 2, true, paramPage);
    // Cancel clear all tables
    appRequests.expectModalDialog("Clear Parameter Tables",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Clear Tables", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(app->model().d()->parameterTables().size() == 3);
    // Clear all the tables
    appRequests.expectModalDialog("Clear Parameter Tables",
                                  Gtk::RESPONSE_OK);
    clickButton("Clear Tables", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(app->model().d()->parameterTables().size() == 0);
}

// Operation of curve clearing
TEST_CASE("ParametersDlg_ClearCurves") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true,
                              paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 0, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    // Add three curves to the table
    clickButton("New Curve", paramPage);
    clickButton("New Curve", paramPage);
    clickButton("New Curve", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 1,
                      {"1", "Permittivity"}, paramPage);
    testListWidgetRow("Table Curves", 2,
                      {"2", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 2, true, paramPage);
    // Cancel curve clearing
    appRequests.expectModalDialog("Clear Curves",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Clear Curves", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(table->size() == 3);
    // Clear curves
    appRequests.expectModalDialog("Clear Curves",
                                  Gtk::RESPONSE_OK);
    clickButton("Clear Curves", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(table->size() == 0);
}

// Operation of point clearing
TEST_CASE("ParametersDlg_ClearPoints") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add a table
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 0, true,
                              paramPage);
    int id = getListWidgetCell<int>("Parameter Tables", 0, 0,
                                    paramPage);
    auto* table = app->model().d()->parameterTables().find(id);
    REQUIRE(table != nullptr);
    // Add a curve
    clickButton("New Curve", paramPage);
    testListWidgetRow("Table Curves", 0,
                      {"0", "Permittivity"}, paramPage);
    testListWidgetRowSelected("Table Curves", 0, true, paramPage);
    id = getListWidgetCell<int>("Table Curves", 0, 0, paramPage);
    auto* curve = table->find(id);
    REQUIRE(curve != nullptr);
    // Add three points to the curve
    clickButton("New Point", paramPage);
    clickButton("New Point", paramPage);
    clickButton("New Point", paramPage);
    testListWidgetRow("Curve Points", 0,
                      {"0", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRow("Curve Points", 1,
                      {"1", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRow("Curve Points", 2,
                      {"2", "0.000000", "0.000000", "0.000000"}, paramPage);
    testListWidgetRowSelected("Curve Points", 2, true, paramPage);
    // Cancel point clearing
    appRequests.expectModalDialog("Clear Points",
                                  Gtk::RESPONSE_CANCEL);
    clickButton("Clear Points", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(curve->size() == 3);
    // Clear points
    appRequests.expectModalDialog("Clear Points",
                                  Gtk::RESPONSE_OK);
    clickButton("Clear Points", paramPage);
    appRequests.checkModalDialog();
    REQUIRE(curve->size() == 0);
}

// Operation of table fill down
TEST_CASE("ParametersDlg_TableFillDown") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& paramPage = app->mainWindow().parametersPage();
    selectNotebookItem(&app->mainWindow().sections(), 9);
    // Add four tables
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    clickButton("New Table", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 2", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 3,
                      {"3", "Table 3", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 3, true,
                              paramPage);
    // Change the second entry
    selectListWidgetRow("Parameter Tables", 1, true, paramPage);
    setListWidgetCell<double>("Parameter Tables", 1, 2,
                              0.1, paramPage);
    setListWidgetCell<double>("Parameter Tables", 1, 3,
                              0.2, paramPage);
    setListWidgetCell<double>("Parameter Tables", 1, 4,
                              0.3, paramPage);
    // Now copy that down and check
    clickButton("Fill Down", paramPage);
    testListWidgetRow("Parameter Tables", 0,
                      {"0", "Table 0", "0.000050", "0.000100", "0.000000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 1,
                      {"1", "Table 1", "0.100000", "0.200000", "0.300000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 2,
                      {"2", "Table 2", "0.100000", "0.200000", "0.300000", "0.000000"},
                      paramPage);
    testListWidgetRow("Parameter Tables", 3,
                      {"3", "Table 3", "0.100000", "0.200000", "0.300000", "0.000000"},
                      paramPage);
    testListWidgetRowSelected("Parameter Tables", 1, true,
                              paramPage);
}
