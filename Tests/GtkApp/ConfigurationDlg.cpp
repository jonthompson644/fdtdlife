/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"

using namespace Catch::literals;

// Basic operation of the configuration part of the dialog
TEST_CASE("ConfigurationDlg_Configuration") {
    // Test setup
    std::unique_ptr<FdtdApp> app(makeApp());
    auto& cfgPage = app->mainWindow().configurationPage();
    auto* p = app->model().p();
    // Check configuration controls
    testCheckButton("FDTD", p, &fdtd::Configuration::doFdtd,
                    cfgPage, true, false);
    testCheckButton("Propagation Matrices", p,
                    &fdtd::Configuration::doPropagationMatrices,
                    cfgPage, false, true);
    testEntry("Stability Factor", p, &fdtd::Configuration::s,
              cfgPage, "0.5", "0.6");
    testEntry("Scattered Zone Size", p, &fdtd::Configuration::scatteredFieldZoneSize,
              cfgPage, "2", "3");
    testEntry("Bottom Corner x,y,z (m)", p, &fdtd::Configuration::p1, cfgPage,
              {"-0.4e-3", "-0.4e-3", "-0.4e-3"},
              {"-160e-6", "-161e-6", "-162e-6"});
    testEntry("Top Corner x,y,z (m)", p, &fdtd::Configuration::p2, cfgPage,
              {"0.4e-3", "0.4e-3", "0.4e-3"},
              {"160e-6", "161e-6", "162e-6"});
    testEntry("Cell Size x,y,z (m)", p, &fdtd::Configuration::dr, cfgPage,
              {"0.1e-4", "0.1e-4", "0.1e-4"},
              {"10e-6", "11e-6", "12e-6"});
    testEntry("Boundary Size", p, &fdtd::Configuration::boundarySize,
              cfgPage, "0", "2");
    testComboBoxText("Boundaries x,y,z", p, &fdtd::Configuration::boundary,
                     cfgPage,
                     box::Vector<fdtd::Configuration::Boundary>(
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::periodic,
                             fdtd::Configuration::Boundary::absorbing),
                     box::Vector<fdtd::Configuration::Boundary>(
                             fdtd::Configuration::Boundary::reflecting));
    testEntry("Time Span (s)", p, &fdtd::Configuration::tSize, cfgPage,
              "5.0e-8", "6.2e-10");
    testEntry("Number Of Threads", p, &fdtd::Configuration::numThreadsCfg,
              cfgPage, "0", "6");
    testComboBoxText("Propagation Matrix Formula",
                     &p->plateAdmittance(), &fdtd::PlateAdmittance::formula,
                     cfgPage,
                     fdtd::PlateAdmittance::formulaLeeEtAl,
                     fdtd::PlateAdmittance::formulaChen);
    testEntry("Sensor Animation Period (s)", p, &fdtd::Configuration::animationPeriod,
              cfgPage, "1", "5");
    testCheckButton("White Background", &app->model(), &FdtdLife::whiteBackground,
                    cfgPage, false, true);
    testCheckButton("Use Thin Sheet Subcells", p,
                    &fdtd::Configuration::useThinSheetSubcells, cfgPage,
                    false, true);
}

// Basic operation of the information part of the dialog
TEST_CASE("ConfigurationDlg_Information") {
    // Test setup
    std::unique_ptr<FdtdApp> app(makeApp());
    auto& cfgPage = app->mainWindow().configurationPage();
    auto* p = app->model().p();
    // Check information controls
    testEntry("Number Of Cells x,y,z", p->geometry(), &fdtd::Geometry::n, cfgPage,
              {"80", "80", "84"});
    testEntry("Number Of Time Steps", p, &fdtd::Configuration::nt, cfgPage,
              "2997925");
    testEntry("Time Step (s)", p, &fdtd::Configuration::dt, cfgPage,
              "1.66782e-14");
    testEntry("Memory Use (MBytes)", cfgPage, "28");
    testEntry("Using openMP", cfgPage, "no");
    testEntry("Num Threads Used", p, &fdtd::Configuration::numThreadsUsed, cfgPage,
              "1");
    testEntry("Available Threads", p, &fdtd::Configuration::maxNumThreads, cfgPage,
              "1");
    testEntry("Cur Time Step", p, &fdtd::Configuration::timeStep, cfgPage,
              "0");
    testEntry("Step Time (s)", p, &fdtd::Configuration::stepProcessingTime, cfgPage,
              "0");
    testEntry("Cell Rate (kcells/s)", p, &fdtd::Configuration::cellProcessingRate,
              cfgPage, "0");
    testEntry("Max Rate (kcells/s)", p, &fdtd::Configuration::maxCellProcessingRate,
              cfgPage, "0");
}

// Invoking the admittance table dialog
TEST_CASE("ConfigurationDlg_AdmittanceTable") {
    // Test setup
    std::unique_ptr<FdtdApp> app(makeApp());
    auto& cfgPage = app->mainWindow().configurationPage();
    // Call up the dialog
    testButton("Admittance Table", cfgPage);
}
