/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <gtkmm/dialog.h>
#include "catch.hpp"
#include "GtkLib.h"

using namespace Catch::literals;

// Creating and destroying sequencers
TEST_CASE("SequencersDlg_NewAndDeleteSequencer") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& seqPage = app->mainWindow().sequencersPage();
    selectNotebookItem(&app->mainWindow().sections(), 7);
    // Add three single sequencers
    clickMenuButton("New Sequencer", 0, seqPage);
    clickMenuButton("New Sequencer", 0, seqPage);
    clickMenuButton("New Sequencer", 0, seqPage);
    testListWidgetRow("Sequencers", 0, {"0", "Sequencer 0", "Single Job"},
                      seqPage);
    testListWidgetRow("Sequencers", 1, {"1", "Sequencer 1", "Single Job"},
                      seqPage);
    testListWidgetRow("Sequencers", 2, {"2", "Sequencer 2", "Single Job"},
                      seqPage);
    testListWidgetRowSelected("Sequencers", 2, true, seqPage);
    // Edit the name of the last sequencer
    int id = getListWidgetCell<int>("Sequencers", 2, 0, seqPage);
    auto* sequencer = dynamic_cast<seq::Sequencer*>(app->model().sequencers().find(id));
    REQUIRE(sequencer != nullptr);
    selectListWidgetRow("Sequencers", 2, true, seqPage);
    setListWidgetCell<const std::string&>("Sequencers", 2, 1,
                                          "Changed Name", seqPage);
    REQUIRE(sequencer->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Sequencers", 2, 1,
                                          "Sequencer 2", seqPage);
    REQUIRE(sequencer->name() == "Sequencer 2");
    // Cancel deletion of the sequencer
    selectListWidgetRow("Sequencers", 2, true, seqPage);
    appRequests.expectModalDialog("Delete Sequencer", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Sequencer", seqPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sequencers", 0, {"0", "Sequencer 0", "Single Job"},
                      seqPage);
    testListWidgetRow("Sequencers", 1, {"1", "Sequencer 1", "Single Job"},
                      seqPage);
    testListWidgetRow("Sequencers", 2, {"2", "Sequencer 2", "Single Job"},
                      seqPage);
    testListWidgetRowSelected("Sequencers", 2, true, seqPage);
    // Delete the sequencer
    selectListWidgetRow("Sequencers", 1, true, seqPage);
    appRequests.expectModalDialog("Delete Sequencer", Gtk::RESPONSE_OK);
    clickButton("Delete Sequencer", seqPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sequencers", 0, {"0", "Sequencer 0", "Single Job"},
                      seqPage);
    testListWidgetRow("Sequencers", 1, {"2", "Sequencer 2", "Single Job"},
                      seqPage);
    testListWidgetRowSelected("Sequencers", 1, true, seqPage);
}

