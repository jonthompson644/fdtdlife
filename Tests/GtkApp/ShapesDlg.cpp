/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <Domain/DielectricLayer.h>
#include <Domain/SquarePlate.h>
#include <Domain/SquareHole.h>
#include <Domain/CuboidArray.h>
#include <Domain/FingerPlate.h>
#include <Domain/BinaryPlateNxN.h>

using namespace Catch::literals;

// Creating and destroying shapes
TEST_CASE("ShapesDlg_NewAndDeleteShape") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add three dielectric shapes
    clickMenuButton("New Shape", 0, srcPage);
    clickMenuButton("New Shape", 0, srcPage);
    clickMenuButton("New Shape", 0, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 2, {"2", "Shape 2", "Dielectric Layer"},
                      srcPage);
    testListWidgetRowSelected("Shapes", 2, true, srcPage);
    // Edit the name of the last shape
    int id = getListWidgetCell<int>("Shapes", 2, 0, srcPage);
    auto* shape = dynamic_cast<domain::Shape*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    selectListWidgetRow("Shapes", 2, true, srcPage);
    setListWidgetCell<const std::string&>("Shapes", 2, 1,
                                          "Changed Name", srcPage);
    REQUIRE(shape->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Shapes", 2, 1,
                                          "Shape 2", srcPage);
    REQUIRE(shape->name() == "Shape 2");
    // Cancel deletion of the material
    selectListWidgetRow("Shapes", 2, true, srcPage);
    appRequests.expectModalDialog("Delete Shape", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Shape", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 2, {"2", "Shape 2", "Dielectric Layer"},
                      srcPage);
    testListWidgetRowSelected("Shapes", 2, true, srcPage);
    // Delete the material
    selectListWidgetRow("Shapes", 1, true, srcPage);
    appRequests.expectModalDialog("Delete Shape", Gtk::RESPONSE_OK);
    clickButton("Delete Shape", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"2", "Shape 2", "Dielectric Layer"},
                      srcPage);
    testListWidgetRowSelected("Shapes", 1, true, srcPage);
}

// Operation of the dielectric layer sub-dialog
TEST_CASE("ShapesDlg_DielectricLayer") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two dielectric layers
    clickMenuButton("New Shape", 0, srcPage);
    clickMenuButton("New Shape", 0, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Dielectric Layer"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Dielectric Layer"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::DielectricLayer*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::DielectricLayer::material, srcPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::DielectricLayer::ignore, srcPage,
                    false, true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::DielectricLayer::center, srcPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::DielectricLayer::cellSizeX, srcPage,
              "0", "4");
    testEntry("Thickness (m)", shape,
              &domain::DielectricLayer::thickness, srcPage,
              "1e-06", "5");
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, srcPage);
    selectListWidgetRow("Shapes", 0, true, srcPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::DielectricLayer::material, srcPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::DielectricLayer::ignore, srcPage,
                    true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::DielectricLayer::center, srcPage,
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::DielectricLayer::cellSizeX, srcPage,
              "4");
    testEntry("Thickness (m)", shape,
              &domain::DielectricLayer::thickness, srcPage,
              "5");
    // Add a couple of materials so we can check their selection
    auto& materialsPage = app->mainWindow().materialsPage();
    selectNotebookItem(&app->mainWindow().sections(), 2);
    clickMenuButton("New Material", 0, materialsPage);
    clickMenuButton("New Material", 0, materialsPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal"},
                      materialsPage);
    testListWidgetRow("Materials", 3, {"11", "Material 11", "Normal"},
                      materialsPage);
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Check the contents of the materials list
    testComboBoxContents("Material",
                         {"<< None >>", "Default", "CPML Bndry",
                          "Material 10", "Material 11"},
                         srcPage);
    // Now check we can select one
    setComboBoxItem("Material", 4, srcPage);
    // Add another material
    selectNotebookItem(&app->mainWindow().sections(), 2);
    clickMenuButton("New Material", 0, materialsPage);
    testListWidgetRow("Materials", 2, {"10", "Material 10", "Normal"},
                      materialsPage);
    testListWidgetRow("Materials", 3, {"11", "Material 11", "Normal"},
                      materialsPage);
    testListWidgetRow("Materials", 4, {"12", "Material 12", "Normal"},
                      materialsPage);
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Is the original material still selected
    REQUIRE(getComboBoxItem("Material", srcPage) == 4);
}

// Operation of the square plate sub-dialog
TEST_CASE("ShapesDlg_SquarePlate") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two square plate layers
    clickMenuButton("New Shape", 1, srcPage);
    clickMenuButton("New Shape", 1, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Square Plate"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Square Plate"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::SquarePlate*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::SquarePlate::material, srcPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::SquarePlate::ignore, srcPage,
                    false, true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::SquarePlate::center, srcPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testCheckButton("Square Cell", dynamic_cast<domain::UnitCellShape*>(shape),
                    &domain::SquarePlate::square, srcPage,
                    true, false);
    testEntry("Cell Size X (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeX, srcPage,
              "0", "4");
    testEntry("Cell Size Y (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeY, srcPage,
              "0", "4.5");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::SquarePlate::layerThickness, srcPage,
              "1e-06", "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquarePlate::increment, srcPage,
              {"0", "0", "0"},
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquarePlate::duplicate, srcPage,
              {"1", "1", "1"},
              {"9", "10", "11"});
    testEntry("Plate Size (%)", shape,
              &domain::SquarePlate::plateSize, srcPage,
              "0.5", "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::SquarePlate::fillMaterial, srcPage,
                        1, 0, 2, 2);
    testEntry("Repeat X", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquarePlate::repeatX, srcPage,
              "1", "13");
    testEntry("Repeat Y", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquarePlate::repeatY, srcPage,
              "1", "14");
    testEntry("Offset X (m)", shape,
              &domain::SquarePlate::plateOffsetX, srcPage,
              "0", "15");
    testEntry("Offset Y (m)", shape,
              &domain::SquarePlate::plateOffsetY, srcPage,
              "0", "16");
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, srcPage);
    selectListWidgetRow("Shapes", 0, true, srcPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::SquarePlate::material, srcPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::SquarePlate::ignore, srcPage,
                    true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::SquarePlate::center, srcPage,
              {"1", "2", "3"});
    testCheckButton("Square Cell", dynamic_cast<domain::UnitCellShape*>(shape),
                    &domain::SquarePlate::square, srcPage,
                    false);
    testEntry("Cell Size X (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeX, srcPage,
              "4");
    testEntry("Cell Size Y (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeY, srcPage,
              "4.5");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::SquarePlate::layerThickness, srcPage,
              "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquarePlate::increment, srcPage,
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquarePlate::duplicate, srcPage,
              {"9", "10", "11"});
    testEntry("Plate Size (%)", shape,
              &domain::SquarePlate::plateSize, srcPage,
              "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::SquarePlate::fillMaterial, srcPage,
                        2, 2);
    testEntry("Repeat X", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquarePlate::repeatX, srcPage,
              "13");
    testEntry("Repeat Y", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquarePlate::repeatY, srcPage,
              "14");
    testEntry("Offset X (m)", shape,
              &domain::SquarePlate::plateOffsetX, srcPage,
              "15");
    testEntry("Offset Y (m)", shape,
              &domain::SquarePlate::plateOffsetY, srcPage,
              "16");
}

// Operation of the square hole sub-dialog
TEST_CASE("ShapesDlg_SquareHole") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two dielectric layers
    clickMenuButton("New Shape", 2, srcPage);
    clickMenuButton("New Shape", 2, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Square Hole"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Square Hole"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::SquareHole*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::SquareHole::material, srcPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::SquareHole::ignore, srcPage,
                    false, true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::SquareHole::center, srcPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquareHole::cellSizeX, srcPage,
              "0", "4");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::SquareHole::layerThickness, srcPage,
              "1e-06", "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::increment, srcPage,
              {"0", "0", "0"},
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::duplicate, srcPage,
              {"1", "1", "1"},
              {"9", "10", "11"});
    testEntry("Hole Size (%)", shape,
              &domain::SquareHole::holeSize, srcPage,
              "50", "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::SquareHole::fillMaterial, srcPage,
                        1, 0, 2, 2);
    testEntry("Repeat X", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquareHole::repeatX, srcPage,
              "1", "13");
    testEntry("Repeat Y", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquareHole::repeatY, srcPage,
              "1", "14");
    testEntry("Offset X (m)", shape,
              &domain::SquareHole::holeOffsetX, srcPage,
              "0", "15");
    testEntry("Offset Y (m)", shape,
              &domain::SquareHole::holeOffsetY, srcPage,
              "0", "16");
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, srcPage);
    selectListWidgetRow("Shapes", 0, true, srcPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::SquareHole::material, srcPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::SquareHole::ignore, srcPage,
                    true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::SquareHole::center, srcPage,
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquareHole::cellSizeX, srcPage,
              "4");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::SquareHole::layerThickness, srcPage,
              "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::increment, srcPage,
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::duplicate, srcPage,
              {"9", "10", "11"});
    testEntry("Hole Size (%)", shape,
              &domain::SquareHole::holeSize, srcPage,
              "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::SquareHole::fillMaterial, srcPage,
                        2, 2);
    testEntry("Repeat X", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquareHole::repeatX, srcPage,
              "13");
    testEntry("Repeat Y", dynamic_cast<domain::RepeatCellShape*>(shape),
              &domain::SquareHole::repeatY, srcPage,
              "14");
    testEntry("Offset X (m)", shape,
              &domain::SquareHole::holeOffsetX, srcPage,
              "15");
    testEntry("Offset Y (m)", shape,
              &domain::SquareHole::holeOffsetY, srcPage,
              "16");
}

// Operation of the cuboid array sub-dialog
TEST_CASE("ShapesDlg_CuboidArray") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two dielectric layers
    clickMenuButton("New Shape", 4, srcPage);
    clickMenuButton("New Shape", 4, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Cuboid Array"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Cuboid Array"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::CuboidArray*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::CuboidArray::material, srcPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::CuboidArray::ignore, srcPage,
                    false, true);
    testEntry("Center x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CuboidArray::center, srcPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testEntry("Size x,y,z (m)", dynamic_cast<domain::SizeShape*>(shape),
              &domain::CuboidArray::size, srcPage,
              {"0", "0", "0"},
              {"4", "5", "6"});
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::increment, srcPage,
              {"0", "0", "0"},
              {"7", "8", "9"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::duplicate, srcPage,
              {"1", "1", "1"},
              {"10", "11", "12"});
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, srcPage);
    selectListWidgetRow("Shapes", 0, true, srcPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::CuboidArray::material, srcPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::CuboidArray::ignore, srcPage,
                    true);
    testEntry("Center x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CuboidArray::center, srcPage,
              {"1", "2", "3"});
    testEntry("Size x,y,z (m)", dynamic_cast<domain::SizeShape*>(shape),
              &domain::CuboidArray::size, srcPage,
              {"4", "5", "6"});
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::increment, srcPage,
              {"7", "8", "9"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::SquareHole::duplicate, srcPage,
              {"10", "11", "12"});
}

// Operation of the finger plate sub-dialog
TEST_CASE("ShapesDlg_FingerPlate") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& shpPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two dielectric layers
    clickMenuButton("New Shape", 5, shpPage);
    clickMenuButton("New Shape", 5, shpPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "Finger Plate"},
                      shpPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "Finger Plate"},
                      shpPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, shpPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, shpPage);
    auto* shape = dynamic_cast<domain::FingerPlate*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::Shape::material, shpPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::Shape::ignore, shpPage,
                    false, true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CenterPosShape::center, shpPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testCheckButton("Square Cell", dynamic_cast<domain::UnitCellShape*>(shape),
                    &domain::SquarePlate::square, shpPage,
                    true, false);
    testEntry("Cell Size X (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeX, shpPage,
              "0", "4");
    testEntry("Cell Size Y (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeY, shpPage,
              "0", "4.5");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::ThinLayerShape::layerThickness, shpPage,
              "1e-06", "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::increment, shpPage,
              {"0", "0", "0"},
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::duplicate, shpPage,
              {"1", "1", "1"},
              {"9", "10", "11"});
    testEntry("Plate Size (%)", shape,
              &domain::FingerPlate::plateSize, shpPage,
              "50", "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::ThinLayerShape::fillMaterial, shpPage,
                        1, 0, 2, 2);
    testCheckButton("Fingers: Left", shape,
                    &domain::FingerPlate::fingersLeft, shpPage,
                    true, false);
    testCheckButton("Right", shape,
                    &domain::FingerPlate::fingersRight, shpPage,
                    false, true);
    testCheckButton("Top", shape,
                    &domain::FingerPlate::fingersTop, shpPage,
                    true, false);
    testCheckButton("Bottom", shape,
                    &domain::FingerPlate::fingersBottom, shpPage,
                    false, true);
    testEntry("Min Feature Size (%)", shape,
              &domain::FingerPlate::minFeatureSize, shpPage,
              "10", "13");
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, shpPage);
    selectListWidgetRow("Shapes", 0, true, shpPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::Shape::material, shpPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::Shape::ignore, shpPage,
                    true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CenterPosShape::center, shpPage,
              {"1", "2", "3"});
    testCheckButton("Square Cell", dynamic_cast<domain::UnitCellShape*>(shape),
                    &domain::SquarePlate::square, shpPage,
                    false);
    testEntry("Cell Size X (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeX, shpPage,
              "4");
    testEntry("Cell Size Y (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::SquarePlate::cellSizeY, shpPage,
              "4.5");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::ThinLayerShape::layerThickness, shpPage,
              "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::increment, shpPage,
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::duplicate, shpPage,
              {"9", "10", "11"});
    testEntry("Plate Size (%)", shape,
              &domain::FingerPlate::plateSize, shpPage,
              "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::ThinLayerShape::fillMaterial, shpPage,
                        2, 2);
    testCheckButton("Fingers: Left", shape,
                    &domain::FingerPlate::fingersLeft, shpPage,
                    false);
    testCheckButton("Right", shape,
                    &domain::FingerPlate::fingersRight, shpPage,
                    true);
    testCheckButton("Top", shape,
                    &domain::FingerPlate::fingersTop, shpPage,
                    false);
    testCheckButton("Bottom", shape,
                    &domain::FingerPlate::fingersBottom, shpPage,
                    true);
    testEntry("Min Feature Size (%)", shape,
              &domain::FingerPlate::minFeatureSize, shpPage,
              "13");
}

// Operation of the binary NxN plate sub-dialog
TEST_CASE("ShapesDlg_BinaryPlateNxN") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two dielectric layers
    clickMenuButton("New Shape", 3, srcPage);
    clickMenuButton("New Shape", 3, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "NxN Binary Plate"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "NxN Binary Plate"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::BinaryPlateNxN*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Check the sub dialog
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::Shape::material, srcPage,
                        0, -1, 2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::Shape::ignore, srcPage,
                    false, true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CenterPosShape::center, srcPage,
              {"0", "0", "0"},
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::UnitCellShape::cellSizeX, srcPage,
              "0", "4");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::ThinLayerShape::layerThickness, srcPage,
              "1e-06", "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::increment, srcPage,
              {"0", "0", "0"},
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::duplicate, srcPage,
              {"1", "1", "1"},
              {"9", "10", "11"});
    testEntry("Bits Per Half Side", shape,
              &domain::BinaryPlateNxN::bitsPerHalfSide, srcPage,
              "8", "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::ThinLayerShape::fillMaterial, srcPage,
                        1, 0, 2, 2);
    // Switch to another shape and back
    selectListWidgetRow("Shapes", 1, true, srcPage);
    selectListWidgetRow("Shapes", 0, true, srcPage);
    // Is the page still ok
    testMaterialsSelect("Material", dynamic_cast<domain::Shape*>(shape),
                        &domain::Shape::material, srcPage,
                        2, 2);
    testCheckButton("Ignore", dynamic_cast<domain::Shape*>(shape),
                    &domain::Shape::ignore, srcPage,
                    true);
    testEntry("Layer Position x,y,z (m)", dynamic_cast<domain::CenterPosShape*>(shape),
              &domain::CenterPosShape::center, srcPage,
              {"1", "2", "3"});
    testEntry("Cell Size (m)", dynamic_cast<domain::UnitCellShape*>(shape),
              &domain::UnitCellShape::cellSizeX, srcPage,
              "4");
    testEntry("Plate Thickness (m)", dynamic_cast<domain::ThinLayerShape*>(shape),
              &domain::ThinLayerShape::layerThickness, srcPage,
              "5");
    testEntry("Increment x,y,z (m)", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::increment, srcPage,
              {"6", "7", "8"});
    testEntry("Duplicate x,y,z", dynamic_cast<domain::DuplicatingShape*>(shape),
              &domain::DuplicatingShape::duplicate, srcPage,
              {"9", "10", "11"});
    testEntry("Bits Per Half Side", shape,
              &domain::BinaryPlateNxN::bitsPerHalfSide, srcPage,
              "12");
    testMaterialsSelect("Fill Material", dynamic_cast<domain::ThinLayerShape*>(shape),
                        &domain::ThinLayerShape::fillMaterial, srcPage,
                        2, 2);
}

// Operation of the binary NxN plate sub-dialog's binary pattern editor
TEST_CASE("ShapesDlg_BinaryPlateNxNPattern") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().shapesPage();
    selectNotebookItem(&app->mainWindow().sections(), 3);
    // Add two layers
    clickMenuButton("New Shape", 3, srcPage);
    clickMenuButton("New Shape", 3, srcPage);
    testListWidgetRow("Shapes", 0, {"0", "Shape 0", "NxN Binary Plate"},
                      srcPage);
    testListWidgetRow("Shapes", 1, {"1", "Shape 1", "NxN Binary Plate"},
                      srcPage);
    // Select the first
    selectListWidgetRow("Shapes", 0, true, srcPage);
    int id = getListWidgetCell<int>("Shapes", 0, 0, srcPage);
    auto* shape = dynamic_cast<domain::BinaryPlateNxN*>(app->model().d()->getShape(id));
    REQUIRE(shape != nullptr);
    // Toggle a bit exercising the inherent two-fold-ness of the binary widget
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 0, 0, srcPage));
    clickBinaryPatternCell(".binarypattern", 0, 0, srcPage);
    REQUIRE(getBinaryPatternCell(".binarypattern", 0, 0, srcPage));
    clickBinaryPatternCell(".binarypattern", 15, 0, srcPage);
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 0, 0, srcPage));
    clickBinaryPatternCell(".binarypattern", 0, 15, srcPage);
    REQUIRE(getBinaryPatternCell(".binarypattern", 0, 0, srcPage));
    clickBinaryPatternCell(".binarypattern", 15, 15, srcPage);
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 0, 0, srcPage));
    // Now test the four-fold-ness
    clickBinaryPatternCell(".binarypattern", 1, 0, srcPage);
    REQUIRE(getBinaryPatternCell(".binarypattern", 1, 0, srcPage));
    REQUIRE(getBinaryPatternCell(".binarypattern", 0, 1, srcPage));
    clickBinaryPatternCell(".binarypattern", 0, 1, srcPage);
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 1, 0, srcPage));
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 0, 1, srcPage));
    // Now turn off the four fold feature
    setCheckActive(".fourfold", false, srcPage);
    REQUIRE_FALSE(getCheckActive(".fourfold", srcPage));
    clickBinaryPatternCell(".binarypattern", 1, 0, srcPage);
    REQUIRE(getBinaryPatternCell(".binarypattern", 1, 0, srcPage));
    REQUIRE_FALSE(getBinaryPatternCell(".binarypattern", 0, 1, srcPage));
    clickBinaryPatternCell(".binarypattern", 0, 1, srcPage);
    REQUIRE(getBinaryPatternCell(".binarypattern", 1, 0, srcPage));
    REQUIRE(getBinaryPatternCell(".binarypattern", 0, 1, srcPage));
}

