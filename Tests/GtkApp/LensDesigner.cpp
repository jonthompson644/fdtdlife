/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <Designer/LensDesigner.h>
#include <GtkGui/LensDesignerDlg.h>
#include "catch.hpp"
#include "GtkLib.h"

using namespace Catch::literals;

// Operation of the phase page of the lens designer panel
TEST_CASE("LensDesigner_PhasePage") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Check the sub dialog
    testEntry("Rel Permittivity Above", designer,
              &designer::LensDesigner::permittivityAbove, desPage,
              "1", "10");
    testEntry("Rel Permittivity Inside", designer,
              &designer::LensDesigner::permittivityIn, desPage,
              "2.2", "11");
    testEntry("Rel Permittivity Below", designer,
              &designer::LensDesigner::permittivityBelow, desPage,
              "2.2", "12");
    testEntry("Focal Length (m)", designer,
              &designer::LensDesigner::focusDistance, desPage,
              "0.002", "13");
    testEntry("Layer Spacing (m)", designer,
              &designer::LensDesigner::layerSpacing, desPage,
              "0.0001", "14");
    testEntry("Centre Frequency (Hz)", designer,
              &designer::LensDesigner::centerFrequency, desPage,
              "1e+11", "15");
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "16");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "17");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "18");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "2");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "19");
    testEntry("Edge Distance (m)", designer,
              &designer::LensDesigner::edgeDistance, desPage,
              "0.0002", "20");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::fingerPatterns);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "21");
    testEntry("Focal Length Fudge Factor", designer,
              &designer::LensDesigner::focalLengthFudgeFactor, desPage,
              "1", "22");
    testEntry("Plate Thickness (m)", designer,
              &designer::LensDesigner::plateThickness, desPage,
              "1e-06", "23");
    testEntry("Slice Thickness (unit cells)", designer,
              &designer::LensDesigner::sliceThickness, desPage,
              "1", "24");
    testEntry("Bits Per Half Side", designer,
              &designer::LensDesigner::bitsPerHalfSide, desPage,
              "10", "25");
    testEntry("Min Feature Size (m)", designer,
              &designer::LensDesigner::minFeatureSize, desPage,
              "1e-05", "26");
    testToggleButton("Curved Columns", designer,
                    &designer::LensDesigner::curvedColumns,desPage,
                    false, true);
    testEntry("Errors", designer,
              &designer::LensDesigner::error, desPage,
              "");
    // Check all still OK
    testEntry("Rel Permittivity Above", designer,
              &designer::LensDesigner::permittivityAbove, desPage,
              "10");
    testEntry("Rel Permittivity Inside", designer,
              &designer::LensDesigner::permittivityIn, desPage,
              "11");
    testEntry("Rel Permittivity Below", designer,
              &designer::LensDesigner::permittivityBelow, desPage,
              "12");
    testEntry("Focal Length (m)", designer,
              &designer::LensDesigner::focusDistance, desPage,
              "13");
    testEntry("Layer Spacing (m)", designer,
              &designer::LensDesigner::layerSpacing, desPage,
              "14");
    testEntry("Centre Frequency (Hz)", designer,
              &designer::LensDesigner::centerFrequency, desPage,
              "15");
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "16");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "17");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "18");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "2");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "19");
    testEntry("Edge Distance (m)", designer,
              &designer::LensDesigner::edgeDistance, desPage,
              "20");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::fingerPatterns);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "21");
    testEntry("Focal Length Fudge Factor", designer,
              &designer::LensDesigner::focalLengthFudgeFactor, desPage,
              "22");
    testEntry("Plate Thickness (m)", designer,
              &designer::LensDesigner::plateThickness, desPage,
              "23");
    testEntry("Slice Thickness (unit cells)", designer,
              &designer::LensDesigner::sliceThickness, desPage,
              "24");
    testEntry("Bits Per Half Side", designer,
              &designer::LensDesigner::bitsPerHalfSide, desPage,
              "25");
    testEntry("Min Feature Size (m)", designer,
              &designer::LensDesigner::minFeatureSize, desPage,
              "26");
    testCheckButton("Curved Columns", designer,
                    &designer::LensDesigner::curvedColumns,desPage,
                     true);
    testEntry("Errors", designer,
              &designer::LensDesigner::error, desPage,
              "");
}

// Operation of the columns page of the lens designer panel, miscellaneous items
TEST_CASE("LensDesigner_ColumnsPageMiscItems") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Select the columns page
    auto* lensDlg = dynamic_cast<LensDesignerDlg*>(desPage.subDialog());
    REQUIRE(lensDlg != nullptr);
    selectNotebookItem(&lensDlg->sections(), 2);
    // Set the items
    testToggleButton("Export Symmetrical", designer,
                     &designer::LensDesigner::exportSymmetricalHalf, desPage,
                     true, false);
    testEntry("Column Num", designer,
              &designer::LensDesigner::columnNumber, desPage,
              "0", "10");
    testEntry("GA Range", designer,
              &designer::LensDesigner::geneticRange, desPage,
              "10", "11");
    testEntry("GA Bits", designer,
              &designer::LensDesigner::geneticBits, desPage,
              "4", "12");
    // Are they all still correct
    testToggleButton("Export Symmetrical", designer,
                     &designer::LensDesigner::exportSymmetricalHalf, desPage,
                     false);
    testEntry("Column Num", designer,
              &designer::LensDesigner::columnNumber, desPage,
              "10");
    testEntry("GA Range", designer,
              &designer::LensDesigner::geneticRange, desPage,
              "11");
    testEntry("GA Bits", designer,
              &designer::LensDesigner::geneticBits, desPage,
              "12");
}

// Operation of the columns page of the lens designer panel, the columns table
TEST_CASE("LensDesigner_ColumnsPageColumns") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    auto* lensDlg = dynamic_cast<LensDesignerDlg*>(desPage.subDialog());
    REQUIRE(lensDlg != nullptr);
    // Select the phase page and set up a lens
    selectNotebookItem(&lensDlg->sections(), 1);
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.0002");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "4");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "2");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.003");
    testButton("Generate", desPage);
    // Select the columns page
    selectNotebookItem(&lensDlg->sections(), 2);
    // Verify the columns
    testListWidgetNumRows("Refractive Indices", 4, desPage);
    testListWidgetNumCols("Refractive Indices", 9, desPage);
    testListWidgetRow("Refractive Indices", 0,
                      {"0", "2.004131", "1.989636", "1.946081", "1.873178",
                       "1.770189", "1.635498", "1.483240", "1.483240"}, desPage);
    testListWidgetRow("Refractive Indices", 1,
                      {"1", "4.016542", "3.958653", "3.787231", "3.508795",
                       "3.133568", "2.674852", "2.141424", "1.483240"}, desPage);
    testListWidgetRow("Refractive Indices", 2,
                      {"2", "4.016542", "3.958653", "3.787231", "3.508795",
                       "3.133568", "2.674852", "2.141424", "1.483240"}, desPage);
    testListWidgetRow("Refractive Indices", 3,
                      {"3", "2.440798", "2.423145", "2.370099", "2.281312",
                       "2.155883", "1.991845", "1.782202", "1.483240"}, desPage);
    // Now change a cell
    setListWidgetCell("Refractive Indices", 0, 1, 20.1, desPage);
    REQUIRE(designer->columns().front().cell(0).refractiveIndex() == 20.1_a);
}

// Operation of the lens designer curved columns mode.
// Geometry calculations for an even number of columns.
TEST_CASE("LensDesigner_CurvedColumnsGeomEven") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "6");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.0_a);
    REQUIRE(designer->lensStructureSize() == 0.000600_a);
    REQUIRE(designer->error().empty());
    // Check the first column calculations
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001657315_a);
    CHECK(col->cell(1).unitCell() == 0.0001704183_a);
    CHECK(col->cell(2).unitCell() == 0.000174062_a);
    CHECK(col->cell(3).unitCell() == 0.0001766638_a);
    CHECK(col->cell(4).unitCell() == 0.0001782246_a);
    CHECK(col->cell(5).unitCell() == 0.0001787448_a);
    CHECK(col->cell(0).incidenceAngle() == 0.0286824507_a);
    CHECK(col->cell(1).incidenceAngle() == 0.0234663957_a);
    CHECK(col->cell(2).incidenceAngle() == 0.0182509792_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0130360592_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0078214938_a);
    CHECK(col->cell(5).incidenceAngle() == 0.002607141_a);
    CHECK(col->cell(0).pathLength() == 0.0001000413_a);
    CHECK(col->cell(1).pathLength() == 0.0001000277_a);
    CHECK(col->cell(2).pathLength() == 0.0001000168_a);
    CHECK(col->cell(3).pathLength() == 0.0001000086_a);
    CHECK(col->cell(4).pathLength() == 0.0001000032_a);
    CHECK(col->cell(5).pathLength() == 0.0001000007_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001658138_a);
    CHECK(col->cell(1).unitCell() == 0.0001705519_a);
    CHECK(col->cell(2).unitCell() == 0.0001742254_a);
    CHECK(col->cell(3).unitCell() == 0.0001768432_a);
    CHECK(col->cell(4).unitCell() == 0.0001784115_a);
    CHECK(col->cell(5).unitCell() == 0.0001789338_a);
    CHECK(col->cell(0).incidenceAngle() == 0.0875870975_a);
    CHECK(col->cell(1).incidenceAngle() == 0.0716318168_a);
    CHECK(col->cell(2).incidenceAngle() == 0.0556947873_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0397719099_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0238591181_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0079523685_a);
    CHECK(col->cell(0).pathLength() == 0.0001003859_a);
    CHECK(col->cell(1).pathLength() == 0.0001002582_a);
    CHECK(col->cell(2).pathLength() == 0.0001001564_a);
    CHECK(col->cell(3).pathLength() == 0.0001000802_a);
    CHECK(col->cell(4).pathLength() == 0.0001000295_a);
    CHECK(col->cell(5).pathLength() == 0.0001000042_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001660072_a);
    CHECK(col->cell(1).unitCell() == 0.0001708654_a);
    CHECK(col->cell(2).unitCell() == 0.0001746092_a);
    CHECK(col->cell(3).unitCell() == 0.000177265_a);
    CHECK(col->cell(4).unitCell() == 0.0001788512_a);
    CHECK(col->cell(5).unitCell() == 0.0001793788_a);
    CHECK(col->cell(0).incidenceAngle() == 0.1514764331_a);
    CHECK(col->cell(1).incidenceAngle() == 0.1237775865_a);
    CHECK(col->cell(2).incidenceAngle() == 0.0961739517_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0686436949_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0411654937_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0137183844_a);
    CHECK(col->cell(0).pathLength() == 0.0001011616_a);
    CHECK(col->cell(1).pathLength() == 0.0001007742_a);
    CHECK(col->cell(2).pathLength() == 0.0001004674_a);
    CHECK(col->cell(3).pathLength() == 0.0001002392_a);
    CHECK(col->cell(4).pathLength() == 0.0001000879_a);
    CHECK(col->cell(5).pathLength() == 0.0001000125_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.000166385_a);
    CHECK(col->cell(1).unitCell() == 0.0001714756_a);
    CHECK(col->cell(2).unitCell() == 0.000175355_a);
    CHECK(col->cell(3).unitCell() == 0.0001780845_a);
    CHECK(col->cell(4).unitCell() == 0.0001797059_a);
    CHECK(col->cell(5).unitCell() == 0.0001802437_a);
    CHECK(col->cell(0).incidenceAngle() == 0.2250598774_a);
    CHECK(col->cell(1).incidenceAngle() == 0.1836187226_a);
    CHECK(col->cell(2).incidenceAngle() == 0.1424947192_a);
    CHECK(col->cell(3).incidenceAngle() == 0.1016124056_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0609001359_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0202888775_a);
    CHECK(col->cell(0).pathLength() == 0.0001025946_a);
    CHECK(col->cell(1).pathLength() == 0.000101717_a);
    CHECK(col->cell(2).pathLength() == 0.000101031_a);
    CHECK(col->cell(3).pathLength() == 0.0001005254_a);
    CHECK(col->cell(4).pathLength() == 0.0001001926_a);
    CHECK(col->cell(5).pathLength() == 0.0001000274_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001671217_a);
    CHECK(col->cell(1).unitCell() == 0.0001726519_a);
    CHECK(col->cell(2).unitCell() == 0.0001767825_a);
    CHECK(col->cell(3).unitCell() == 0.0001796468_a);
    CHECK(col->cell(4).unitCell() == 0.0001813323_a);
    CHECK(col->cell(5).unitCell() == 0.0001818887_a);
    CHECK(col->cell(0).incidenceAngle() == 0.3158555774_a);
    CHECK(col->cell(1).incidenceAngle() == 0.256966532_a);
    CHECK(col->cell(2).incidenceAngle() == 0.1989787361_a);
    CHECK(col->cell(3).incidenceAngle() == 0.1416638723_a);
    CHECK(col->cell(4).incidenceAngle() == 0.084815518_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0282416368_a);
    CHECK(col->cell(0).pathLength() == 0.0001052198_a);
    CHECK(col->cell(1).pathLength() == 0.0001034096_a);
    CHECK(col->cell(2).pathLength() == 0.0001020269_a);
    CHECK(col->cell(3).pathLength() == 0.0001010256_a);
    CHECK(col->cell(4).pathLength() == 0.0001003742_a);
    CHECK(col->cell(5).pathLength() == 0.0001000532_a);
}

// Operation of the lens designer curved columns mode.
// Geometry calculations for an odd number of columns.
TEST_CASE("LensDesigner_CurvedColumnsGeomOdd") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.00144");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "6");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 9U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.0_a);
    REQUIRE(designer->lensStructureSize() == 0.000600_a);
    REQUIRE(designer->error().empty());
    // Check the first column calculations
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001659928_a);
    CHECK(col->cell(1).unitCell() == 0.0001708953_a);
    CHECK(col->cell(2).unitCell() == 0.000174708_a);
    CHECK(col->cell(3).unitCell() == 0.000177431_a);
    CHECK(col->cell(4).unitCell() == 0.0001790647_a);
    CHECK(col->cell(5).unitCell() == 0.0001796093_a);
    CHECK(col->cell(0).incidenceAngle() == 0.0_a);
    CHECK(col->cell(1).incidenceAngle() == 0.0_a);
    CHECK(col->cell(2).incidenceAngle() == 0.0_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0_a);
    CHECK(col->cell(0).pathLength() == 0.0001_a);
    CHECK(col->cell(1).pathLength() == 0.0001_a);
    CHECK(col->cell(2).pathLength() == 0.0001_a);
    CHECK(col->cell(3).pathLength() == 0.0001_a);
    CHECK(col->cell(4).pathLength() == 0.0001_a);
    CHECK(col->cell(5).pathLength() == 0.0001_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001660403_a);
    CHECK(col->cell(1).unitCell() == 0.0001709726_a);
    CHECK(col->cell(2).unitCell() == 0.0001748029_a);
    CHECK(col->cell(3).unitCell() == 0.0001775356_a);
    CHECK(col->cell(4).unitCell() == 0.0001791739_a);
    CHECK(col->cell(5).unitCell() == 0.0001797198_a);
    CHECK(col->cell(0).incidenceAngle() == 0.0605112376_a);
    CHECK(col->cell(1).incidenceAngle() == 0.0494991963_a);
    CHECK(col->cell(2).incidenceAngle() == 0.0384931599_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0274917873_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0164937423_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0054976925_a);
    CHECK(col->cell(0).pathLength() == 0.0001001839_a);
    CHECK(col->cell(1).pathLength() == 0.0001001231_a);
    CHECK(col->cell(2).pathLength() == 0.0001000746_a);
    CHECK(col->cell(3).pathLength() == 0.0001000383_a);
    CHECK(col->cell(4).pathLength() == 0.0001000141_a);
    CHECK(col->cell(5).pathLength() == 0.0001000029_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001661988_a);
    CHECK(col->cell(1).unitCell() == 0.0001712307_a);
    CHECK(col->cell(2).unitCell() == 0.0001751198_a);
    CHECK(col->cell(3).unitCell() == 0.0001778848_a);
    CHECK(col->cell(4).unitCell() == 0.0001795386_a);
    CHECK(col->cell(5).unitCell() == 0.000180089_a);
    CHECK(col->cell(0).incidenceAngle() == 0.1245581635_a);
    CHECK(col->cell(1).incidenceAngle() == 0.1018237356_a);
    CHECK(col->cell(2).incidenceAngle() == 0.079142028_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0565010724_a);
    CHECK(col->cell(4).incidenceAngle() == 0.033889091_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0112944409_a);
    CHECK(col->cell(0).pathLength() == 0.000100783_a);
    CHECK(col->cell(1).pathLength() == 0.0001005228_a);
    CHECK(col->cell(2).pathLength() == 0.0001003161_a);
    CHECK(col->cell(3).pathLength() == 0.000100162_a);
    CHECK(col->cell(4).pathLength() == 0.0001000596_a);
    CHECK(col->cell(5).pathLength() == 0.0001000085_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001665265_a);
    CHECK(col->cell(1).unitCell() == 0.0001717622_a);
    CHECK(col->cell(2).unitCell() == 0.0001757717_a);
    CHECK(col->cell(3).unitCell() == 0.0001786027_a);
    CHECK(col->cell(4).unitCell() == 0.0001802883_a);
    CHECK(col->cell(5).unitCell() == 0.0001808481_a);
    CHECK(col->cell(0).incidenceAngle() == 0.1965752871_a);
    CHECK(col->cell(1).incidenceAngle() == 0.160488212_a);
    CHECK(col->cell(2).incidenceAngle() == 0.1246110517_a);
    CHECK(col->cell(3).incidenceAngle() == 0.0888946451_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0532917464_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0177564344_a);
    CHECK(col->cell(0).pathLength() == 0.0001019693_a);
    CHECK(col->cell(1).pathLength() == 0.0001013073_a);
    CHECK(col->cell(2).pathLength() == 0.0001007868_a);
    CHECK(col->cell(3).pathLength() == 0.0001004017_a);
    CHECK(col->cell(4).pathLength() == 0.0001001474_a);
    CHECK(col->cell(5).pathLength() == 0.000100021_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).unitCell() == 0.0001671648_a);
    CHECK(col->cell(1).unitCell() == 0.0001727879_a);
    CHECK(col->cell(2).unitCell() == 0.0001770218_a);
    CHECK(col->cell(3).unitCell() == 0.0001799746_a);
    CHECK(col->cell(4).unitCell() == 0.0001817187_a);
    CHECK(col->cell(5).unitCell() == 0.0001822956_a);
    CHECK(col->cell(0).incidenceAngle() == 0.2832478431_a);
    CHECK(col->cell(1).incidenceAngle() == 0.2307006086_a);
    CHECK(col->cell(2).incidenceAngle() == 0.1787961912_a);
    CHECK(col->cell(3).incidenceAngle() == 0.127375693_a);
    CHECK(col->cell(4).incidenceAngle() == 0.0762926517_a);
    CHECK(col->cell(5).incidenceAngle() == 0.0254089121_a);
    CHECK(col->cell(0).pathLength() == 0.0001041622_a);
    CHECK(col->cell(1).pathLength() == 0.0001027331_a);
    CHECK(col->cell(2).pathLength() == 0.0001016313_a);
    CHECK(col->cell(3).pathLength() == 0.0001008278_a);
    CHECK(col->cell(4).pathLength() == 0.0001003026_a);
    CHECK(col->cell(5).pathLength() == 0.0001000431_a);
}

// Operation of the lens designer curved columns mode.
// Refractive index determination for zero anti-reflection stages.
TEST_CASE("LensDesigner_CurvedColumnsRefrIndex0Ar") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "6");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "0");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.0_a);
    REQUIRE(designer->lensStructureSize() == 0.000600_a);
    REQUIRE(designer->error().empty());
    // Check the first column refractive indices
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.789852021_a);
    CHECK(col->cell(1).refractiveIndex() == 1.789852021_a);
    CHECK(col->cell(2).refractiveIndex() == 1.789852021_a);
    CHECK(col->cell(3).refractiveIndex() == 1.789852021_a);
    CHECK(col->cell(4).refractiveIndex() == 1.789852021_a);
    CHECK(col->cell(5).refractiveIndex() == 1.789852021_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.7579265425_a);
    CHECK(col->cell(1).refractiveIndex() == 1.7579265425_a);
    CHECK(col->cell(2).refractiveIndex() == 1.7579265425_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7579265425_a);
    CHECK(col->cell(4).refractiveIndex() == 1.7579265425_a);
    CHECK(col->cell(5).refractiveIndex() == 1.7579265425_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.6948956341_a);
    CHECK(col->cell(1).refractiveIndex() == 1.6948956341_a);
    CHECK(col->cell(2).refractiveIndex() == 1.6948956341_a);
    CHECK(col->cell(3).refractiveIndex() == 1.6948956341_a);
    CHECK(col->cell(4).refractiveIndex() == 1.6948956341_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6948956341_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.6024351173_a);
    CHECK(col->cell(1).refractiveIndex() == 1.6024351173_a);
    CHECK(col->cell(2).refractiveIndex() == 1.6024351173_a);
    CHECK(col->cell(3).refractiveIndex() == 1.6024351173_a);
    CHECK(col->cell(4).refractiveIndex() == 1.6024351173_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6024351173_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(4).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(5).refractiveIndex() == 1.4832396974_a);
}

// Operation of the lens designer curved columns mode.
// Refractive index determination for one anti-reflection stage.
TEST_CASE("LensDesigner_CurvedColumnsRefrIndex1Ar") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "6");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "1");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.0_a);
    REQUIRE(designer->lensStructureSize() == 0.000600_a);
    REQUIRE(designer->error().empty());
    // Check the first column refractive indices
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.8948742056_a);
    CHECK(col->cell(2).refractiveIndex() == 1.8948742056_a);
    CHECK(col->cell(3).refractiveIndex() == 1.8948742056_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8948742056_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6764563786_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.8519469071_a);
    CHECK(col->cell(2).refractiveIndex() == 1.8519469071_a);
    CHECK(col->cell(3).refractiveIndex() == 1.8519469071_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8519469071_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6573717658_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.7672375936_a);
    CHECK(col->cell(2).refractiveIndex() == 1.7672375936_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7672375936_a);
    CHECK(col->cell(4).refractiveIndex() == 1.7672375936_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6190234568_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.6430824379_a);
    CHECK(col->cell(2).refractiveIndex() == 1.6430824379_a);
    CHECK(col->cell(3).refractiveIndex() == 1.6430824379_a);
    CHECK(col->cell(4).refractiveIndex() == 1.6430824379_a);
    CHECK(col->cell(5).refractiveIndex() == 1.561116619_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(4).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(5).refractiveIndex() == 1.4832396974_a);
}

// Operation of the lens designer curved columns mode.
// Refractive index determination for two anti-reflection stages.
TEST_CASE("LensDesigner_CurvedColumnsRefrIndex2Ar") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "6");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "2");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.0_a);
    REQUIRE(designer->lensStructureSize() == 0.000600_a);
    REQUIRE(designer->error().empty());
    // Check the first column refractive indices
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.6212552618_a);
    CHECK(col->cell(2).refractiveIndex() == 2.0643203692_a);
    CHECK(col->cell(3).refractiveIndex() == 2.0643203692_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8489358589_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6560238718_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.5917929177_a);
    CHECK(col->cell(2).refractiveIndex() == 2.0083058445_a);
    CHECK(col->cell(3).refractiveIndex() == 2.0083058445_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8153359776_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6409077933_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.5331134214_a);
    CHECK(col->cell(2).refractiveIndex() == 1.8982850543_a);
    CHECK(col->cell(3).refractiveIndex() == 1.8982850543_a);
    CHECK(col->cell(4).refractiveIndex() == 1.7484158402_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6103787697_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.7250664845_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7250664845_a);
    CHECK(col->cell(4).refractiveIndex() == 1.6403669296_a);
    CHECK(col->cell(5).refractiveIndex() == 1.5598260635_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(4).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(5).refractiveIndex() == 1.4832396974_a);
}

// Operation of the lens designer curved columns mode.
// Refractive index determination for three anti-reflection stages.
TEST_CASE("LensDesigner_CurvedColumnsRefrIndex3Ar") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "7");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "3");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 2.5_a);
    REQUIRE(designer->lensStructureSize() == 0.000700_a);
    REQUIRE(designer->error().empty());
    // Check the first column refractive indices
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.7634008399_a);
    CHECK(col->cell(3).refractiveIndex() == 2.1304338688_a);
    CHECK(col->cell(4).refractiveIndex() == 1.9460494801_a);
    CHECK(col->cell(5).refractiveIndex() == 1.7776231566_a);
    CHECK(col->cell(6).refractiveIndex() == 1.6237737629_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.723263238_a);
    CHECK(col->cell(3).refractiveIndex() == 2.0660246454_a);
    CHECK(col->cell(4).refractiveIndex() == 1.901754526_a);
    CHECK(col->cell(5).refractiveIndex() == 1.7505455635_a);
    CHECK(col->cell(6).refractiveIndex() == 1.6113592622_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.6437035321_a);
    CHECK(col->cell(3).refractiveIndex() == 1.9398343758_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8139542251_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6962427163_a);
    CHECK(col->cell(6).refractiveIndex() == 1.5861697681_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.5261814501_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7571475346_a);
    CHECK(col->cell(4).refractiveIndex() == 1.6842595003_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6143949261_a);
    CHECK(col->cell(6).refractiveIndex() == 1.5474283963_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(4).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(5).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(6).refractiveIndex() == 1.4832396974_a);
}

// Operation of the lens designer curved columns mode.
// Refractive index determination for four anti-reflection stages.
TEST_CASE("LensDesigner_CurvedColumnsRefrIndex4Ar") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& desPage = app->mainWindow().designersPage();
    selectNotebookItem(&app->mainWindow().sections(), 10);
    // Select the lens designer
    clickRadioButton("Designers", 1, desPage);
    auto* designer = dynamic_cast<designer::LensDesigner*>(
            app->model().designers().designer());
    REQUIRE(designer != nullptr);
    // Configure for the test
    testEntry("Unit Cell (m)", designer,
              &designer::LensDesigner::unitCell, desPage,
              "5e-05", "0.00016");
    testEntry("Lens Diameter (m)", designer,
              &designer::LensDesigner::lensDiameter, desPage,
              "0.0063", "0.0016");
    testEntry("Total Number Of Layers", designer,
              &designer::LensDesigner::numLayers, desPage,
              "14", "9");
    testEntry("Number Of AR Stages", designer,
              &designer::LensDesigner::numArcStages, desPage,
              "1", "4");
    testEntry("Layers Per AR Stage", designer,
              &designer::LensDesigner::layersPerArcStage, desPage,
              "2", "1");
    testComboBoxText("Modelling", designer,
                     &designer::LensDesigner::modelling, desPage,
                     designer::LensDesigner::Modelling::metamaterialCells,
                     designer::LensDesigner::Modelling::dielectricBlocks);
    testEntry("FDTD Cells Per Unit Cell", designer,
              &designer::LensDesigner::fdtdCellsPerUnitCell, desPage,
              "40", "4");
    testToggleButton("Curved Columns", designer,
                     &designer::LensDesigner::curvedColumns,desPage,
                     false, true);
    // Calculate the lens structure
    clickButton("Generate", desPage);
    // Check the global calculations
    REQUIRE(designer->columnsInDiameter() == 10U);
    REQUIRE(designer->columns().size() == 5U);
    REQUIRE(designer->wavelengthBelow() == 0.0020212003_a);
    REQUIRE(designer->tau() == 3.5_a);
    REQUIRE(designer->lensStructureSize() == 0.000900_a);
    REQUIRE(designer->error().empty());
    // Check the first column refractive indices
    auto colPos = designer->columns().begin();
    auto* col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.5841605036_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7368368206_a);
    CHECK(col->cell(4).refractiveIndex() == 1.993878874_a);
    CHECK(col->cell(5).refractiveIndex() == 1.8793223799_a);
    CHECK(col->cell(6).refractiveIndex() == 1.8066317073_a);
    CHECK(col->cell(7).refractiveIndex() == 1.6369691099_a);
    CHECK(col->cell(8).refractiveIndex() == 1.5736524662_a);
    // Check the second column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.5591624336_a);
    CHECK(col->cell(3).refractiveIndex() == 1.7040001743_a);
    CHECK(col->cell(4).refractiveIndex() == 1.9468704061_a);
    CHECK(col->cell(5).refractiveIndex() == 1.8437919008_a);
    CHECK(col->cell(6).refractiveIndex() == 1.7781230392_a);
    CHECK(col->cell(7).refractiveIndex() == 1.6240020562_a);
    CHECK(col->cell(8).refractiveIndex() == 1.5661612738_a);
    // Check the third column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.509479494_a);
    CHECK(col->cell(3).refractiveIndex() == 1.6390517322_a);
    CHECK(col->cell(4).refractiveIndex() == 1.8545597348_a);
    CHECK(col->cell(5).refractiveIndex() == 1.7735152581_a);
    CHECK(col->cell(6).refractiveIndex() == 1.7214628878_a);
    CHECK(col->cell(7).refractiveIndex() == 1.5979180495_a);
    CHECK(col->cell(8).refractiveIndex() == 1.5510194273_a);
    // Check the fourth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.5338520029_a);
    CHECK(col->cell(4).refractiveIndex() == 1.7069834962_a);
    CHECK(col->cell(5).refractiveIndex() == 1.6596852176_a);
    CHECK(col->cell(6).refractiveIndex() == 1.6288833907_a);
    CHECK(col->cell(7).refractiveIndex() == 1.5543566218_a);
    CHECK(col->cell(8).refractiveIndex() == 1.5255095711_a);
    // Check the fifth column calculations
    colPos++;
    col = &(*colPos);
    CHECK(col->cell(0).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(1).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(2).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(3).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(4).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(5).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(6).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(7).refractiveIndex() == 1.4832396974_a);
    CHECK(col->cell(8).refractiveIndex() == 1.4832396974_a);
}
