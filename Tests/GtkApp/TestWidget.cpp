/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "TestWidget.h"
#include "GtkLib.h"

// Constructor
TestWidget::TestWidget(int argc, char** argv) :
        Application(argc, argv),
        mainWindow_(this) {
}

// Add the test widget to the harness
void TestWidget::addTestWidget(CustomWidget& widget, int width, int height) {
    testWidget_ = &widget;
    width_ = width;
    height_ = height;
    mainWindow_.addTestWidget(widget, width, height);
    gtk_widget_show_all(GTK_WIDGET(mainWindow_.gobj()));
    processEvents(0);
}

// Check the pixel contents of the widget
// If the png file exists, we check against that, otherwise we ask the user.
bool TestWidget::testPixmap(const std::string& name) {
    pixmapResult_ = false;
    // Does the file exist?
    if(box::Utility::exists(name + ".png")) {
        // Read the file
        try {
            processEvents(200);
            // Get the reference and the current
            auto reference = Gdk::Pixbuf::create_from_file(name + ".png");
            auto current = testWidget_->capture();
            // Are they different?
            if(reference->get_width() == current->get_width() &&
               reference->get_height() == current->get_height() &&
               reference->get_bits_per_sample() == current->get_bits_per_sample() &&
               reference->get_n_channels() == current->get_n_channels()) {
                // Same dimensions, check the pixels
                int strideRef = reference->get_rowstride();
                int strideCur = current->get_rowstride();
                int numChannels = reference->get_n_channels();
                unsigned char* dRef = reference->get_pixels();
                unsigned char* dCur = current->get_pixels();
                pixmapResult_ = true;
                for(int row = 0; row < reference->get_height() && pixmapResult_; row++) {
                    for(int col = 0; col < reference->get_width() && pixmapResult_; col++) {
                        for(int chan = 0; chan < numChannels && pixmapResult_; chan++) {
                            pixmapResult_ =
                                    dRef[row * strideRef + col * numChannels + chan] ==
                                    dCur[row * strideCur + col * numChannels + chan];
                        }
                    }
                }
            }
            // If we failed, save the current for reference purposes
            if(!pixmapResult_) {
                current->save(name + "_failed.png", "png");
            }
        } catch(Glib::FileError& e) {
        } catch(Gdk::PixbufError& e) {
        }
    } else {
        // Enable/disable the buttons
        mainWindow_.enablePassFail(true);
        // Wait for an answer
        gtk_main();
        // Return the buttons to the original state
        mainWindow_.enablePassFail(false);
        // Store the file if passed
        if(pixmapResult_) {
            auto current = testWidget_->capture();
            current->save(name + ".png", "png");
        }
    }
    return pixmapResult_;
}

// The user has indicated the correctness of the display
void TestWidget::pixmapResult(bool result) {
    pixmapResult_ = result;
    gtk_main_quit();
}

// Main window constructor
TestWidget::MainWindow::MainWindow(TestWidget* testBench) :
        testBench_(testBench) {
}

// Add the test widget to the main window
void TestWidget::MainWindow::addTestWidget(CustomWidget& widget, int width, int height) {
    set_title("Widget Test Bench");
    continueButton_.signal_clicked().connect(
            sigc::ptr_fun(&MainWindow::onContinue));
    passButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::onPass));
    failButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::onFail));
    passButton_.set_sensitive(false);
    failButton_.set_sensitive(false);
    buttons_.set_layout(Gtk::ButtonBoxStyle::BUTTONBOX_EXPAND);
    buttons_.pack_start(continueButton_, Gtk::PACK_EXPAND_WIDGET);
    buttons_.pack_start(passButton_, Gtk::PACK_EXPAND_WIDGET);
    buttons_.pack_start(failButton_, Gtk::PACK_EXPAND_WIDGET);
    layout_.pack_start(buttons_, Gtk::PACK_SHRINK);
    layout_.pack_start(widget);
    ApplicationWindow::add(layout_);
    widget.set_size_request(width, height);
    show_all_children();
}

// Continue with the current test case
void TestWidget::MainWindow::onContinue() {
    gtk_main_quit();
}

// The widget display is correct
void TestWidget::MainWindow::onPass() const {
    testBench_->pixmapResult(true);
}

// The widget display is not correct
void TestWidget::MainWindow::onFail() const {
    testBench_->pixmapResult(false);
}

// Enable the PASS/FAIL buttons
void TestWidget::MainWindow::enablePassFail(bool enable) {
    continueButton_.set_sensitive(!enable);
    passButton_.set_sensitive(enable);
    failButton_.set_sensitive(enable);
}

