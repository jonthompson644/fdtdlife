/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include "GtkLib.h"
#include <gtkmm/dialog.h>
#include <Source/PlaneWaveSource.h>
#include <Source/ZoneSource.h>
#include <Source/GravyWaveSource.h>

using namespace Catch::literals;

// Creating and destroying sources
TEST_CASE("SourcesDlg_NewAndDeleteSource") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().sourcesPage();
    selectNotebookItem(&app->mainWindow().sections(), 1);
    // Add a plane wave source
    clickMenuButton("New Source", 0, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRowSelected("Sources", 0, true, srcPage);
    // Add a zone source
    clickMenuButton("New Source", 1, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Zone"},
                      srcPage);
    testListWidgetRowSelected("Sources", 1, true, srcPage);
    // Add a gravitational wave source
    clickMenuButton("New Source", 2, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Zone"},
                      srcPage);
    testListWidgetRow("Sources", 2, {"2", "Source 2", "Gravitational Wave"},
                      srcPage);
    testListWidgetRowSelected("Sources", 2, true, srcPage);
    // Edit the name of the zone source
    int id = getListWidgetCell<int>("Sources", 1, 0, srcPage);
    auto* source = dynamic_cast<source::ZoneSource*>(app->model().sources().getSource(id));
    REQUIRE(source != nullptr);
    selectListWidgetRow("Sources", 1, true, srcPage);
    setListWidgetCell<const std::string&>("Sources", 1, 1,
                                          "Changed Name", srcPage);
    REQUIRE(source->name() == "Changed Name");
    setListWidgetCell<const std::string&>("Sources", 1, 1,
                                          "Source 1", srcPage);
    REQUIRE(source->name() == "Source 1");
    // Cancel deletion of the zone source
    selectListWidgetRow("Sources", 1, true, srcPage);
    appRequests.expectModalDialog("Delete Source", Gtk::RESPONSE_CANCEL);
    clickButton("Delete Source", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Zone"},
                      srcPage);
    testListWidgetRow("Sources", 2, {"2", "Source 2", "Gravitational Wave"},
                      srcPage);
    testListWidgetRowSelected("Sources", 1, true, srcPage);
    // Delete the zone source
    selectListWidgetRow("Sources", 1, true, srcPage);
    appRequests.expectModalDialog("Delete Source", Gtk::RESPONSE_OK);
    clickButton("Delete Source", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"2", "Source 2", "Gravitational Wave"},
                      srcPage);
    testListWidgetRowSelected("Sources", 1, true, srcPage);
    // Delete the gravitational wave source
    selectListWidgetRow("Sources", 1, true, srcPage);
    appRequests.expectModalDialog("Delete Source", Gtk::RESPONSE_OK);
    clickButton("Delete Source", srcPage);
    appRequests.checkModalDialog();
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRowSelected("Sources", 0, true, srcPage);
    // Delete the plane wave source
    selectListWidgetRow("Sources", 0, true, srcPage);
    appRequests.expectModalDialog("Delete Source", Gtk::RESPONSE_OK);
    clickButton("Delete Source", srcPage);
    appRequests.checkModalDialog();
    testListWidgetEmpty("Sources", srcPage);
}

// Operation of the plane wave source sub-dialog
TEST_CASE("SourcesDlg_PlaneWaveSource") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().sourcesPage();
    selectNotebookItem(&app->mainWindow().sections(), 1);
    // Add a plane wave source and a zone source
    clickMenuButton("New Source", 0, srcPage);
    clickMenuButton("New Source", 1, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Zone"},
                      srcPage);
    // Select the plane wave source
    selectListWidgetRow("Sources", 0, true, srcPage);
    int id = getListWidgetCell<int>("Sources", 0, 0, srcPage);
    auto* source = dynamic_cast<source::PlaneWaveSource*>(
            app->model().sources().getSource(id));
    REQUIRE(source != nullptr);
    // Check the plane wave sub dialog
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1e+12", "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "1e+12", "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "1", "3");
    testEntry("Polarisation (-90..90 deg)", source,
              &source::PlaneWaveSource::polarisation, srcPage,
              "0", "4");
    testComboBoxText("Distribution", source,
                     &source::PlaneWaveSource::distribution, srcPage,
                     source::PlaneWaveSource::Distribution::flat,
                     source::PlaneWaveSource::Distribution::gaussian);
    testEntry("E Field Amplitude (V/m)", source,
              &source::PlaneWaveSource::amplitude, srcPage,
              "1", "5");
    testEntry("Azimuth (0..90 deg)", source,
              &source::PlaneWaveSource::azimuth, srcPage,
              "0", "6");
    testEntry("PML Conductivity", source,
              &source::PlaneWaveSource::pmlSigma, srcPage,
              "1", "7");
    testEntry("PML Conductivity Factor", source,
              &source::PlaneWaveSource::pmlSigmaFactor, srcPage,
              "1.04", "8");
    testEntry("Initial Time (s)", source,
              &source::PlaneWaveSource::initialTime, srcPage,
              "0", "9");
    testEntry("Run For (s)", source,
              &source::PlaneWaveSource::time, srcPage,
              "0", "10");
    testCheckButton("Continuous", source,
                    &source::PlaneWaveSource::continuous,
                    srcPage, true, false);
    testEntry("Gaussian B", source,
              &source::PlaneWaveSource::gaussianB, srcPage,
              "1", "11");
    testEntry("Gaussian C", source,
              &source::PlaneWaveSource::gaussianC, srcPage,
              "1", "12");
    // Switch to the zone page and back
    selectListWidgetRow("Sources", 1, true, srcPage);
    selectListWidgetRow("Sources", 0, true, srcPage);
    // Is the page still ok
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "3");
    testEntry("Polarisation (-90..90 deg)", source,
              &source::PlaneWaveSource::polarisation, srcPage,
              "4");
    testComboBoxText("Distribution", source,
                     &source::PlaneWaveSource::distribution, srcPage,
                     source::PlaneWaveSource::Distribution::gaussian);
    testEntry("E Field Amplitude (V/m)", source,
              &source::PlaneWaveSource::amplitude, srcPage,
              "5");
    testEntry("Azimuth (0..90 deg)", source,
              &source::PlaneWaveSource::azimuth, srcPage,
              "6");
    testEntry("PML Conductivity", source,
              &source::PlaneWaveSource::pmlSigma, srcPage,
              "7");
    testEntry("PML Conductivity Factor", source,
              &source::PlaneWaveSource::pmlSigmaFactor, srcPage,
              "8");
    testEntry("Initial Time (s)", source,
              &source::PlaneWaveSource::initialTime, srcPage,
              "9");
    testEntry("Run For (s)", source,
              &source::PlaneWaveSource::time, srcPage,
              "10");
    testCheckButton("Continuous", source,
                    &source::PlaneWaveSource::continuous,
                    srcPage, false);
    testEntry("Gaussian B", source,
              &source::PlaneWaveSource::gaussianB, srcPage,
              "11");
    testEntry("Gaussian C", source,
              &source::PlaneWaveSource::gaussianC, srcPage,
              "12");
}

// Operation of the zone source sub-dialog
TEST_CASE("SourcesDlg_ZoneSource") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().sourcesPage();
    selectNotebookItem(&app->mainWindow().sections(), 1);
    // Add a plane wave source and a zone source
    clickMenuButton("New Source", 0, srcPage);
    clickMenuButton("New Source", 1, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Zone"},
                      srcPage);
    // Select the zone source
    selectListWidgetRow("Sources", 1, true, srcPage);
    int id = getListWidgetCell<int>("Sources", 1, 0, srcPage);
    auto* source = dynamic_cast<source::ZoneSource*>(
            app->model().sources().getSource(id));
    REQUIRE(source != nullptr);
    // Check the zone source sub dialog
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1e+12", "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "1e+12", "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "1", "3");
    testEntry("Electric Current Density (A/m)", source,
              &source::ZoneSource::amplitude, srcPage,
              "1", "4");
    testEntry("Polarisation (0..360 deg)", source,
              &source::ZoneSource::polarisation, srcPage,
              "0", "5");
    testEntry("Azimuth (0..90 deg)", source,
              &source::ZoneSource::azimuth, srcPage,
              "0", "6");
    testEntry("Elevation (-90..90 deg)", source,
              &source::ZoneSource::elevation, srcPage,
              "0", "7");
    testEntry("Initial Time (s)", source,
              &source::ZoneSource::initialTime, srcPage,
              "0", "8");
    testEntry("Run For (s)", source,
              &source::ZoneSource::time, srcPage,
              "0", "9");
    testCheckButton("Continuous", source,
                    &source::ZoneSource::continuous,
                    srcPage, true, false);
    testEntry("Zone Center (m)", source,
              &source::ZoneSource::zoneCenter, srcPage,
              {"0", "0", "0"}, {"10", "11", "12"});
    testEntry("Zone Size (m)", source,
              &source::ZoneSource::zoneSize, srcPage,
              {"0", "0", "0"}, {"13", "14", "15"});
    // Switch to the plane wave page and back
    selectListWidgetRow("Sources", 0, true, srcPage);
    selectListWidgetRow("Sources", 1, true, srcPage);
    // Is the page still ok
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "3");
    testEntry("Electric Current Density (A/m)", source,
              &source::ZoneSource::amplitude, srcPage,
              "4");
    testEntry("Polarisation (0..360 deg)", source,
              &source::ZoneSource::polarisation, srcPage,
              "5");
    testEntry("Azimuth (0..90 deg)", source,
              &source::ZoneSource::azimuth, srcPage,
              "6");
    testEntry("Elevation (-90..90 deg)", source,
              &source::ZoneSource::elevation, srcPage,
              "7");
    testEntry("Initial Time (s)", source,
              &source::ZoneSource::initialTime, srcPage,
              "8");
    testEntry("Run For (s)", source,
              &source::ZoneSource::time, srcPage,
              "9");
    testCheckButton("Continuous", source,
                    &source::ZoneSource::continuous,
                    srcPage, false);
    testEntry("Zone Center (m)", source,
              &source::ZoneSource::zoneCenter, srcPage,
              {"10", "11", "12"});
    testEntry("Zone Size (m)", source,
              &source::ZoneSource::zoneSize, srcPage,
              {"13", "14", "15"});
}

// Operation of the gravy wave source sub-dialog
TEST_CASE("SourcesDlg_GraveWaveSource") {
    // Test setup
    TestSuiteApi appRequests;
    std::unique_ptr<FdtdApp> app(makeApp(&appRequests));
    auto& srcPage = app->mainWindow().sourcesPage();
    selectNotebookItem(&app->mainWindow().sections(), 1);
    // Add a plane wave source and a zone source
    clickMenuButton("New Source", 0, srcPage);
    clickMenuButton("New Source", 2, srcPage);
    testListWidgetRow("Sources", 0, {"0", "Source 0", "Plane Wave"},
                      srcPage);
    testListWidgetRow("Sources", 1, {"1", "Source 1", "Gravitational Wave"},
                      srcPage);
    // Select the zone source
    selectListWidgetRow("Sources", 1, true, srcPage);
    int id = getListWidgetCell<int>("Sources", 1, 0, srcPage);
    auto* source = dynamic_cast<source::GravyWaveSource*>(
            app->model().sources().getSource(id));
    REQUIRE(source != nullptr);
    // Check the zone source sub dialog
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1e+12", "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "1e+12", "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "1", "3");
    testEntry("Azimuth (0..90 deg)", source,
              &source::GravyWaveSource::azimuth, srcPage,
              "0", "4");
    testEntry("Elevation (-90..90 deg)", source,
              &source::GravyWaveSource::elevation, srcPage,
              "0", "5");
    testEntry("Polarisation (0..360 deg)", source,
              &source::GravyWaveSource::polarisation, srcPage,
              "0", "6");
    testEntry("Magnetic Field (T)", source,
              &source::GravyWaveSource::magneticField, srcPage,
              "5", "7");
    testEntry("Amplitude", source,
              &source::GravyWaveSource::peakAmplitude, srcPage,
              "1", "8");
    testEntry("Time of Peak (s)", source,
              &source::GravyWaveSource::timeOfMaximum, srcPage,
              "0", "9");
    testEntry("Width (s)", source,
              &source::GravyWaveSource::widthAtHalfHeight, srcPage,
              "0.0001", "10");
    testEntry("Zone Center (m)", source,
              &source::GravyWaveSource::zoneCenter, srcPage,
              {"0", "0", "0"}, {"11", "12", "13"});
    testEntry("Zone Size (m)", source,
              &source::GravyWaveSource::zoneSize, srcPage,
              {"0.0001", "0.0001", "0.0001"},
              {"14", "15", "16"});
    // Switch to the plane wave page and back
    selectListWidgetRow("Sources", 0, true, srcPage);
    selectListWidgetRow("Sources", 1, true, srcPage);
    // Is the page still ok
    testEntry("First Frequency (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::first, srcPage,
              "1");
    testEntry("Frequency Step (Hz)", &source->frequencies(),
              &box::FrequencySeqExpression::spacing, srcPage,
              "2");
    testEntry("Number Of Frequencies", &source->frequencies(),
              &box::FrequencySeqExpression::n, srcPage,
              "3");
    testEntry("Azimuth (0..90 deg)", source,
              &source::GravyWaveSource::azimuth, srcPage,
              "4");
    testEntry("Elevation (-90..90 deg)", source,
              &source::GravyWaveSource::elevation, srcPage,
              "5");
    testEntry("Polarisation (0..360 deg)", source,
              &source::GravyWaveSource::polarisation, srcPage,
              "6");
    testEntry("Magnetic Field (T)", source,
              &source::GravyWaveSource::magneticField, srcPage,
              "7");
    testEntry("Amplitude", source,
              &source::GravyWaveSource::peakAmplitude, srcPage,
              "8");
    testEntry("Time of Peak (s)", source,
              &source::GravyWaveSource::timeOfMaximum, srcPage,
              "9");
    testEntry("Width (s)", source,
              &source::GravyWaveSource::widthAtHalfHeight, srcPage,
              "10");
    testEntry("Zone Center (m)", source,
              &source::GravyWaveSource::zoneCenter, srcPage,
              {"11", "12", "13"});
    testEntry("Zone Size (m)", source,
              &source::GravyWaveSource::zoneSize, srcPage,
              {"14", "15", "16"});
}
