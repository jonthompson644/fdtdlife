/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_TESTWIDGET_H
#define FDTDLIFE_TESTWIDGET_H

#include <gtkmm/applicationwindow.h>
#include <GtkGui/NamedButton.h>
#include <gtkmm/hvbuttonbox.h>
#include <GtkGui/CustomWidget.h>

class TestWidget : public Gtk::Application {
public:
    // Types
    class MainWindow : public Gtk::ApplicationWindow {
    public:
        // Construction
        explicit MainWindow(TestWidget* testBench);
        // API
        void addTestWidget(CustomWidget& widget, int width, int height);
        void enablePassFail(bool enable);
        // Handlers
        static void onContinue();
        void onPass() const;
        void onFail() const;
        // Members
        Gtk::Button continueButton_{"Continue"};
        Gtk::Button passButton_{"Pass"};
        Gtk::Button failButton_{"Fail"};
        Gtk::HButtonBox buttons_;
        Gtk::VBox layout_;
        TestWidget* testBench_;
    };
    // Construction
    explicit TestWidget(int argc=0, char* argv[]=nullptr);
    // API
    void addTestWidget(CustomWidget& widget, int width, int height);
    bool testPixmap(const std::string& name);
    // Used by MainWindow
    void pixmapResult(bool result);
protected:
    // Members
    MainWindow mainWindow_;
    bool pixmapResult_{};
    CustomWidget* testWidget_{};
    int width_{};
    int height_{};
};


#endif //FDTDLIFE_TESTWIDGET_H
