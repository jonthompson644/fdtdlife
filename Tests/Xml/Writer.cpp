/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Xml/Writer.h>
#include <Xml/DomDocument.h>
#include <Xml/DomObject.h>
#include <Xml/Exception.h>

// A simple document with an empty root object
TEST_CASE("Writer_EmptyRoot") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root/>\n");
}

// Stream-like writing of nested values
TEST_CASE("Writer_StreamBasedNestedValues") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_NOTHROW(*root << xml::Obj("int_1") << (int) 55);
    REQUIRE_NOTHROW(*root << xml::Obj("int_2") << -92);
    REQUIRE_NOTHROW(*root << xml::Obj("double_1") << (double) 48.4);
    REQUIRE_NOTHROW(*root << xml::Obj("double_2") << -1e76);
    REQUIRE_NOTHROW(*root << xml::Obj("uint64_t_1") << (uint64_t) 93);
    REQUIRE_NOTHROW(*root << xml::Obj("size_t_1") << (size_t) 94);
    REQUIRE_NOTHROW(*root << xml::Obj("bool_1") << (bool) true);
    REQUIRE_NOTHROW(*root << xml::Obj("bool_2") << (bool) false);
    REQUIRE_NOTHROW(*root << xml::Obj("string") << "This is a string of some kind");
    REQUIRE_NOTHROW(*root << xml::Obj("std__vector_int") << std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(*root << xml::Obj("std__complex__double")
                          << std::complex<double>(34.0, -52.9));
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root>\n"
                    "    <int_1>55</int_1>\n"
                    "    <int_2>-92</int_2>\n"
                    "    <double_1>48.4</double_1>\n"
                    "    <double_2>-1e+76</double_2>\n"
                    "    <uint64_t_1>93</uint64_t_1>\n"
                    "    <size_t_1>94</size_t_1>\n"
                    "    <bool_1>1</bool_1>\n"
                    "    <bool_2>0</bool_2>\n"
                    "    <string>This is a string of some kind</string>\n"
                    "    <std__vector_int>1 2 3 4 </std__vector_int>\n"
                    "    <std__complex__double>(34,-52.9)</std__complex__double>\n"
                    "</root>\n");
}

// Stream-like writing of attribute values
TEST_CASE("Writer_StreamBasedAttributeValues") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_NOTHROW(*root << xml::Attr("int_1") << (int) 55);
    REQUIRE_NOTHROW(*root << xml::Attr("int_2") << -92);
    REQUIRE_NOTHROW(*root << xml::Attr("double_1") << (double) 48.4);
    REQUIRE_NOTHROW(*root << xml::Attr("double_2") << -1e76);
    REQUIRE_NOTHROW(*root << xml::Attr("uint64_t_1") << (uint64_t) 93);
    REQUIRE_NOTHROW(*root << xml::Attr("size_t_1") << (size_t) 94);
    REQUIRE_NOTHROW(*root << xml::Attr("bool_1") << (bool) true);
    REQUIRE_NOTHROW(*root << xml::Attr("bool_2") << (bool) false);
    REQUIRE_NOTHROW(*root << xml::Attr("string") << "This is a string of some kind");
    REQUIRE_NOTHROW(*root << xml::Attr("std__vector_int") << std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(
            *root << xml::Attr("std__complex__double") << std::complex<double>(34.0, -52.9));
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root"
                    " bool_1=\"1\""
                    " bool_2=\"0\""
                    " double_1=\"48.4\""
                    " double_2=\"-1e+76\""
                    " int_1=\"55\""
                    " int_2=\"-92\""
                    " size_t_1=\"94\""
                    " std__complex__double=\"(34,-52.9)\""
                    " std__vector_int=\"1 2 3 4 \""
                    " string=\"This is a string of some kind\""
                    " uint64_t_1=\"93\"/>\n");
}

// Stream-like writing of nested objects
TEST_CASE("Writer_StreamBasedNestedObjects") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_NOTHROW(*root << xml::Open("level1"));
    for(int i = 0; i < 5; i++) {
        REQUIRE_NOTHROW(*root << xml::Open("level2"));
        REQUIRE_NOTHROW(*root << xml::Obj("member") << 10 + i);
        REQUIRE_NOTHROW(*root << xml::Close("level2"));
    }
    REQUIRE_NOTHROW(*root << xml::Close("level1"));
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root>\n"
                    "    <level1>\n"
                    "        <level2>\n"
                    "            <member>10</member>\n"
                    "        </level2>\n"
                    "        <level2>\n"
                    "            <member>11</member>\n"
                    "        </level2>\n"
                    "        <level2>\n"
                    "            <member>12</member>\n"
                    "        </level2>\n"
                    "        <level2>\n"
                    "            <member>13</member>\n"
                    "        </level2>\n"
                    "        <level2>\n"
                    "            <member>14</member>\n"
                    "        </level2>\n"
                    "    </level1>\n"
                    "</root>\n");
}

// Stream-like writing exceptions
TEST_CASE("Writer_StreamBasedExceptions") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    REQUIRE_THROWS_AS(*root << 55, xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Obj("one") << 55 << 56,
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Attr("one") << 1 << xml::Attr("one") << 2,
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Obj("one") << xml::Obj("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Attr("one") << xml::Attr("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Obj("one") << xml::Attr("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Attr("one") << xml::Obj("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Close("one"), xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Obj("one") << xml::Open("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Attr("one") << xml::Open("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Open("one") << xml::Obj("one") << xml::Close("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root << xml::Open("one") << xml::Close("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_NOTHROW(*root << xml::Open("one"));
    root->setState(xml::StreamContext::State::objectDefault);
    REQUIRE_THROWS_AS(*root << xml::Open("two"), xml::Exception);
    root->reset();
    REQUIRE_NOTHROW(*root << xml::Open("one"));
    root->setState(xml::StreamContext::State::objectDefault);
    REQUIRE_THROWS_AS(*root << 52, xml::Exception);
    root->reset();
}

// Stream-like writing of nested objects with base classes
TEST_CASE("Writer_StreamBasedNestedDerivedObjects") {
    // Build a test DOM
    auto* root = new xml::DomObject("root");
    xml::DomDocument doc(root);
    // The standard practice for a nested object is to name the container externally:
    REQUIRE_NOTHROW(*root << xml::Obj("level1"));
    // Then the base class gets to put its things in
    REQUIRE_NOTHROW(*root << xml::Open());
    REQUIRE_NOTHROW(*root << xml::Obj("basemember") << 34);
    REQUIRE_NOTHROW(*root << xml::Close());
    // Finally the derived class things
    REQUIRE_NOTHROW(*root << xml::Reopen());
    REQUIRE_NOTHROW(*root << xml::Obj("derivedmember") << 35);
    REQUIRE_NOTHROW(*root << xml::Close());
    // Convert to text
    xml::Writer writer;
    std::string text;
    writer.writeString(&doc, text);
    REQUIRE(text == "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
                    "<root>\n"
                    "    <level1>\n"
                    "        <basemember>34</basemember>\n"
                    "        <derivedmember>35</derivedmember>\n"
                    "    </level1>\n"
                    "</root>\n");
}

