/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "catch.hpp"
#include <Xml/Reader.h>
#include <Xml/DomDocument.h>
#include <Xml/DomObject.h>
#include <Xml/Exception.h>

using namespace Catch::literals;

// A simple document with an empty root object
TEST_CASE("Reader_EmptyRoot") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root/>\n"));
    REQUIRE(doc.getObject()->getName() == "root");
}

// A simple document with an incorrect root object
TEST_CASE("Reader_IncorrectRoot") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_THROWS_AS(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<notroot/>\n"), xml::Exception);
}

// Stream-like writing of nested values
TEST_CASE("Reader_StreamBasedNestedValues") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "    <int_1>55</int_1>\n"
            "    <int_2>-92</int_2>\n"
            "    <double_1>48.4</double_1>\n"
            "    <double_2>-1e+76</double_2>\n"
            "    <uint64_t_1>93</uint64_t_1>\n"
            "    <size_t_1>94</size_t_1>\n"
            "    <bool_1>1</bool_1>\n"
            "    <bool_2>0</bool_2>\n"
            "    <string>This is a string of some kind</string>\n"
            "    <std__vector_int>1 2 3 4 </std__vector_int>\n"
            "    <std__complex__double>(34,-52.9)</std__complex__double>\n"
            "</root>\n"));
    int a = 0;
    double b = 0.0;
    uint64_t c = 0;
    size_t d = 0;
    bool e = false;
    bool f = true;
    std::string g;
    std::vector<int> h;
    std::complex<double> i;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Obj("int_1") >> a);
    REQUIRE(a == 55);
    REQUIRE_NOTHROW(*root >> xml::Obj("int_2") >> a);
    REQUIRE(a == -92);
    REQUIRE_NOTHROW(*root >> xml::Obj("double_1") >> b);
    REQUIRE(b == 48.4_a);
    REQUIRE_NOTHROW(*root >> xml::Obj("double_2") >> b);
    REQUIRE(b == -1e76_a);
    REQUIRE_NOTHROW(*root >> xml::Obj("uint64_t_1") >> c);
    REQUIRE(c == 93);
    REQUIRE_NOTHROW(*root >> xml::Obj("size_t_1") >> d);
    REQUIRE(d == 94);
    REQUIRE_NOTHROW(*root >> xml::Obj("bool_1") >> e);
    REQUIRE(e);
    REQUIRE_NOTHROW(*root >> xml::Obj("bool_2") >> f);
    REQUIRE_FALSE(f);
    REQUIRE_NOTHROW(*root >> xml::Obj("string") >> g);
    REQUIRE(g == "This is a string of some kind");
    REQUIRE_NOTHROW(*root >> xml::Obj("std__vector_int") >> h);
    REQUIRE(h == std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(*root >> xml::Obj("std__complex__double") >> i);
    REQUIRE(i == std::complex<double>(34.0, -52.9));
}

// Stream-like writing of nested values
TEST_CASE("Reader_StreamBasedAttributeValues") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root"
            " bool_1=\"1\""
            " bool_2=\"0\""
            " double_1=\"48.4\""
            " double_2=\"-1e+76\""
            " int_1=\"55\""
            " int_2=\"-92\""
            " size_t_1=\"94\""
            " std__complex__double=\"(34,-52.9)\""
            " std__vector_int=\"1 2 3 4 \""
            " string=\"This is a string of some kind\""
            " uint64_t_1=\"93\"/>\n"));
    int a = 0;
    double b = 0.0;
    uint64_t c = 0;
    size_t d = 0;
    bool e = false;
    bool f = true;
    std::string g;
    std::vector<int> h;
    std::complex<double> i;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Attr("int_1") >> a);
    REQUIRE(a == 55);
    REQUIRE_NOTHROW(*root >> xml::Attr("int_2") >> a);
    REQUIRE(a == -92);
    REQUIRE_NOTHROW(*root >> xml::Attr("double_1") >> b);
    REQUIRE(b == 48.4_a);
    REQUIRE_NOTHROW(*root >> xml::Attr("double_2") >> b);
    REQUIRE(b == -1e76_a);
    REQUIRE_NOTHROW(*root >> xml::Attr("uint64_t_1") >> c);
    REQUIRE(c == 93);
    REQUIRE_NOTHROW(*root >> xml::Attr("size_t_1") >> d);
    REQUIRE(d == 94);
    REQUIRE_NOTHROW(*root >> xml::Attr("bool_1") >> e);
    REQUIRE(e);
    REQUIRE_NOTHROW(*root >> xml::Attr("bool_2") >> f);
    REQUIRE_FALSE(f);
    REQUIRE_NOTHROW(*root >> xml::Attr("string") >> g);
    REQUIRE(g == "This is a string of some kind");
    REQUIRE_NOTHROW(*root >> xml::Attr("std__vector_int") >> h);
    REQUIRE(h == std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(*root >> xml::Attr("std__complex__double") >> i);
    REQUIRE(i == std::complex<double>(34.0, -52.9));
}

// Stream-like writing of nested values
TEST_CASE("Reader_StreamBasedNestedDefaultValues") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "</root>\n"));
    int a = 0;
    double b = 0.0;
    uint64_t c = 0;
    size_t d = 0;
    bool e = false;
    bool f = true;
    std::string g;
    std::vector<int> h;
    std::complex<double> i;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Obj("int_1") >> xml::Default(55) >> a);
    REQUIRE(a == 55);
    REQUIRE_NOTHROW(*root >> xml::Obj("int_2") >> xml::Default(-92) >> a);
    REQUIRE(a == -92);
    REQUIRE_NOTHROW(*root >> xml::Obj("double_1") >> xml::Default(48.4) >> b);
    REQUIRE(b == 48.4_a);
    REQUIRE_NOTHROW(*root >> xml::Obj("double_2") >> xml::Default(-1e76) >> b);
    REQUIRE(b == -1e76_a);
    REQUIRE_NOTHROW(*root >> xml::Obj("uint64_t_1") >> xml::Default(93) >> c);
    REQUIRE(c == 93);
    REQUIRE_NOTHROW(*root >> xml::Obj("size_t_1") >> xml::Default(94) >> d);
    REQUIRE(d == 94);
    REQUIRE_NOTHROW(*root >> xml::Obj("bool_1") >> xml::Default(true) >> e);
    REQUIRE(e);
    REQUIRE_NOTHROW(*root >> xml::Obj("bool_2") >> xml::Default(false) >> f);
    REQUIRE_FALSE(f);
    REQUIRE_NOTHROW(*root >> xml::Obj("string")
                          >> xml::Default("This is a string of some kind") >> g);
    REQUIRE(g == "This is a string of some kind");
    REQUIRE_NOTHROW(*root >> xml::Obj("std__vector_int")
                          >> xml::Default(std::vector<int>({1, 2, 3, 4})) >> h);
    REQUIRE(h == std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(*root >> xml::Obj("std__complex__double")
                          >> xml::Default(std::complex<double>(34.0, -52.9)) >> i);
    REQUIRE(i == std::complex<double>(34.0, -52.9));
}

// File content faults
TEST_CASE("Reader_FileContentFaults") {
    // Build a test DOM
    xml::DomDocument doc("root");
    xml::Reader reader;
    // Duplicate attribute
    REQUIRE_THROWS_AS(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root one=\"56\" one=\"57\">\n"
            "    <one>55</one>\n"
            "</root>\n"), xml::Exception);
}

// Stream-like writing of nested values
TEST_CASE("Reader_StreamBasedNestedNoDefaultValues") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "</root>\n"));
    int a = 0;
    double b = 0.0;
    uint64_t c = 0;
    size_t d = 0;
    bool e = false;
    bool f = true;
    std::string g;
    std::vector<int> h;
    std::complex<double> i;
    xml::DomObject* root = doc.getObject();
    REQUIRE_THROWS_AS(*root >> xml::Obj("int_1") >> a, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("int_2") >> a, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("double_1") >> b, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("double_2") >> b, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("uint64_t_1") >> c,
                      xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("size_t_1") >> d, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("bool_1") >> e, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("bool_2") >> f, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("string") >> g, xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("std__vector_int") >> h,
                      xml::Exception);
    REQUIRE_THROWS_AS(*root >> xml::Obj("std__complex__double") >> i,
                      xml::Exception);
}

// Stream-like writing of nested values
TEST_CASE("Reader_StreamBasedAttributeDefaultValues") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert to text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "</root>\n"));
    int a = 0;
    double b = 0.0;
    uint64_t c = 0;
    size_t d = 0;
    bool e = false;
    bool f = true;
    std::string g;
    std::vector<int> h;
    std::complex<double> i;
    xml::DomObject* root = doc.getObject();
    REQUIRE_NOTHROW(*root >> xml::Attr("int_1") >> xml::Default(55) >> a);
    REQUIRE(a == 55);
    REQUIRE_NOTHROW(*root >> xml::Attr("int_2") >> xml::Default(-92) >> a);
    REQUIRE(a == -92);
    REQUIRE_NOTHROW(*root >> xml::Attr("double_1") >> xml::Default(48.4) >> b);
    REQUIRE(b == 48.4_a);
    REQUIRE_NOTHROW(*root >> xml::Attr("double_2") >> xml::Default(-1e76) >> b);
    REQUIRE(b == -1e76_a);
    REQUIRE_NOTHROW(*root >> xml::Attr("uint64_t_1") >> xml::Default(93) >> c);
    REQUIRE(c == 93);
    REQUIRE_NOTHROW(*root >> xml::Attr("size_t_1") >> xml::Default(94) >> d);
    REQUIRE(d == 94);
    REQUIRE_NOTHROW(*root >> xml::Attr("bool_1") >> xml::Default(true) >> e);
    REQUIRE(e);
    REQUIRE_NOTHROW(*root >> xml::Attr("bool_2") >> xml::Default(false) >> f);
    REQUIRE_FALSE(f);
    REQUIRE_NOTHROW(
            *root >> xml::Attr("string") >> xml::Default("This is a string of some kind")
                  >> g);
    REQUIRE(g == "This is a string of some kind");
    REQUIRE_NOTHROW(*root >> xml::Attr("std__vector_int")
                          >> xml::Default(std::vector<int>({1, 2, 3, 4})) >> h);
    REQUIRE(h == std::vector<int>({1, 2, 3, 4}));
    REQUIRE_NOTHROW(*root >> xml::Attr("std__complex__double")
                          >> xml::Default(std::complex<double>(34.0, -52.9)) >> i);
    REQUIRE(i == std::complex<double>(34.0, -52.9));
}

// Stream-like reading of nested objects
TEST_CASE("Reader_StreamBasedNestedObjects") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert from text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "    <level1>\n"
            "        <level2>\n"
            "            <member>10</member>\n"
            "        </level2>\n"
            "        <level2>\n"
            "            <member>11</member>\n"
            "        </level2>\n"
            "        <level2>\n"
            "            <member>12</member>\n"
            "        </level2>\n"
            "        <level2>\n"
            "            <member>13</member>\n"
            "        </level2>\n"
            "        <level2>\n"
            "            <member>14</member>\n"
            "        </level2>\n"
            "    </level1>\n"
            "</root>\n"));
    xml::DomObject* root = doc.getObject();
    int v = 0;
    int t = 10;
    *root >> xml::Open("level1");
    for(auto& o : root->obj("level2")) {
        REQUIRE(o->getName() == "level2");
        REQUIRE_NOTHROW(*o >> xml::Obj("member") >> v);
        REQUIRE(v == t);
        t++;
    }
    *root >> xml::Close("level1");
    REQUIRE(t == 15);
}

// Stream-like reading exceptions
TEST_CASE("Reader_StreamBasedExceptions") {
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert from text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root one=\"56\">\n"
            "    <one>55</one>\n"
            "</root>\n"));
    int a = 0;
    xml::DomObject* root = doc.getObject();
    REQUIRE_THROWS_AS(*root >> xml::Obj("one") >> xml::Obj("two") >> a,
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Attr("one") >> xml::Attr("two") >> a,
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Close("one"), xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Obj("one") >> xml::Open("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Attr("one") >> xml::Open("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Open("one") >> xml::Obj("one") >> xml::Close("one"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Open("one") >> xml::Close("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Open("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Obj("three") >> xml::Open(),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Obj("three") >> xml::Default("two") >> xml::Open(),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Default("two"),
                      xml::Exception);
    root->reset();
    REQUIRE_THROWS_AS(*root >> xml::Attr("one") >> xml::Close(),
                      xml::Exception);
    root->reset();
    int val;
    REQUIRE_THROWS_AS(*root >> xml::Obj("one") >> xml::Close() >> val,
                      xml::Exception);
    root->reset();
}

// Stream-like reading of nested objects with base classes
TEST_CASE("Reader_StreamBasedNestedDerivedObjects") {
    double a = 0.0;
    // Build a test DOM
    xml::DomDocument doc("root");
    // Convert from text
    xml::Reader reader;
    REQUIRE_NOTHROW(reader.readString(
            &doc,
            "<?xml encoding=\"UTF-8\" version=\"1.0\"?>\n"
            "<root>\n"
            "    <level1>\n"
            "        <basemember>34</basemember>\n"
            "        <derivedmember>35</derivedmember>\n"
            "    </level1>\n"
            "</root>\n"));
    xml::DomObject* root = doc.getObject();
    // The standard practice for a nested object is to name the container externally:
    REQUIRE_NOTHROW(*root >> xml::Obj("level1"));
    // Then the base class gets to take its things in
    REQUIRE_NOTHROW(*root >> xml::Open());
    REQUIRE_NOTHROW(*root >> xml::Obj("basemember") >> a);
    REQUIRE(a == 34.0_a);
    REQUIRE_NOTHROW(*root >> xml::Close());
    // Finally the derived class things
    REQUIRE_NOTHROW(*root >> xml::Reopen());
    REQUIRE_NOTHROW(*root >> xml::Obj("derivedmember") >> a);
    REQUIRE(a == 35.0_a);
    REQUIRE_NOTHROW(*root >> xml::Close());
}

