/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Ga/Individual.h>
#include <Ga/GeneTemplate.h>
#include <Ga/Gene.h>
#include <Ga/MicroAlgorithm.h>
#include <memory>
#include <cmath>

// Gene an gene template type identifiers
enum TestGeneMode {
    testGeneModeNumber = 0
};

// Individuals in the genetic algorithm
class TestIndividual : public ga::Individual {
public:
    explicit TestIndividual(int id) :
            ga::Individual(id),
            value_(0) {
    }
    int value_;
};

// The gene
class TestGeneTemplate;

class TestGene : public ga::Gene {
public:
    static const size_t nBits_ = 8;
    explicit TestGene(const std::shared_ptr<TestGeneTemplate>& t) :
            ga::Gene(testGeneModeNumber, std::dynamic_pointer_cast<ga::GeneTemplate>(t)),
            t_(t) {
    }
    void encode(const std::shared_ptr<ga::Individual>& gaInd) override {
        auto ind = std::dynamic_pointer_cast<TestIndividual>(gaInd);
        ind->encodeChromosome(ind->value_, nBits_, 0);
    }
    void decode(const std::shared_ptr<ga::Individual>& gaInd) override {
        auto ind = std::dynamic_pointer_cast<TestIndividual>(gaInd);
        ind->decodeChromosome(ind->value_, nBits_, 0);
    }
    std::shared_ptr<TestGeneTemplate> t_;
};

// The gene template
class TestGeneTemplate : public ga::GeneTemplate {
public:
    TestGeneTemplate() :
            GeneTemplate(testGeneModeNumber) {
    }
    std::shared_ptr<ga::Gene> makeGene() override {
        return std::dynamic_pointer_cast<ga::Gene>(
                std::make_shared<TestGene>(
                        std::dynamic_pointer_cast<TestGeneTemplate>(shared_from_this())));
    }
    size_t size() const override {
        return TestGene::nBits_;
    };
};

// The micro genetic algorithm itself
class TestAlgorithm : public ga::MicroAlgorithm {
public:
    explicit TestAlgorithm(int target) :
            target_(target) {
    }
    std::shared_ptr<ga::Individual> makeIndividual(int id) override {
        return std::dynamic_pointer_cast<ga::Individual>(std::make_shared<TestIndividual>(id));
    }
    void evaluateIndividual(std::shared_ptr<ga::Individual> gaInd) override {
        auto ind = std::dynamic_pointer_cast<TestIndividual>(gaInd);
        ind->evaluated(std::abs(ind->value_ - target_));
    }
    int target_;
};

// Operation of the box::Trackable base class
TEST_CASE("MicroAlgorithmTest_BasicOperation") {
    int generation = 0;
    int id = 0;
    bool stepResult;
    // Create the algorithm, set the target to 55
    TestAlgorithm algo(55);
    REQUIRE(algo.state() == ga::MicroAlgorithm::stateReset);
    REQUIRE(algo.populationSize() == 5);
    REQUIRE(algo.unfitnessThreshold() == 0.01);
    // Create the gene template
    algo.clearGeneTemplates();
    auto t = std::make_shared<TestGeneTemplate>();
    algo.addGeneTemplate(std::dynamic_pointer_cast<ga::GeneTemplate>(t));
    REQUIRE(algo.geneTemplates().size() == 1);
    // The first step initialises the algorithm for a run
    stepResult = algo.step();
    REQUIRE_FALSE(stepResult);
    REQUIRE(algo.state() == ga::MicroAlgorithm::stateEvaluate);
    REQUIRE(algo.population().size() == algo.populationSize());
    REQUIRE(algo.generationNumber() == generation);
    for(int j=0; j<5; j++) {
        INFO("j=" << j);
        REQUIRE_FALSE(algo.population()[j]->evaluated());
        REQUIRE(algo.population()[j]->id() == id+j);
    }
    // The next five steps should evaluate the five individuals
    for(int i=0; i<5; i++) {
        INFO("i=" << i);
        stepResult = algo.step();
        REQUIRE_FALSE(stepResult);
        REQUIRE(algo.state() == ga::MicroAlgorithm::stateEvaluate);
        REQUIRE(algo.population().size() == algo.populationSize());
        REQUIRE(algo.generationNumber() == 0);
        for(int j=0; j<5; j++) {
            INFO("j=" << j);
            REQUIRE(algo.population()[j]->evaluated() == (j <= i));
            REQUIRE(algo.population()[j]->id() == id+j);
        }
    }
    // Another step should decide whether or not we have finished
    stepResult = algo.step();
    id++;
    while(algo.state() != ga::MicroAlgorithm::stateComplete) {
        REQUIRE_FALSE(stepResult);
        generation++;
        id += 4;
        // We should have the best from the previous, plus four new ones
        INFO("generation=" << generation);
        REQUIRE(algo.generationNumber() == generation);
        for(int j=0; j<5; j++) {
            INFO("j=" << j);
            REQUIRE(algo.population()[j]->evaluated() == (j == 0));
            if(j > 0) {
                REQUIRE(algo.population()[j]->id() == id + j - 1);
            }
        }
        // Evaluate these four new ones
        for(int i=1; i<5; i++) {
            algo.step();
            for(int j=0; j<5; j++) {
                INFO("j=" << j);
                REQUIRE(algo.population()[j]->evaluated() == (j <= i));
                if(j > 0) {
                    REQUIRE(algo.population()[j]->id() == id + j - 1);
                }
            }
        }
        // Another step to decide whether we have finished
        stepResult = algo.step();
    }
    REQUIRE(stepResult);
    // We have finished, verify the final state
    auto result = std::dynamic_pointer_cast<TestIndividual>(algo.population()[0]);
    REQUIRE(result->value_ == 55);
}
