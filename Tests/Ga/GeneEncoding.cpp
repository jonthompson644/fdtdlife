/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "catch.hpp"
#include <Ga/Individual.h>
#include <memory>

// Individuals in the genetic algorithm
class TestIndividual : public ga::Individual {
public:
    explicit TestIndividual(int id) :
            ga::Individual(id),
            value_(0) {
    }
    int value_;
};

// Compare two doubles for equality to within the given limits
bool doubleEquals(double a, double b, double delta) {
    double diff = fabs(a - b);
    return diff < delta;
}

// Check the encoding and decoding of doubles
TEST_CASE("GeneEncoding_Double") {
    // Create an individual
    TestIndividual ind(1);
    // Encode some doubles
    ind.encodeChromosomeStart();
    REQUIRE(ind.chromosome().empty());
    REQUIRE(ind.nextChromosomeBit() == 0);
    ind.encodeChromosome(37.5, 7, 4.0, 40.0);
    ind.encodeChromosome(2.7, 15, 0.0, 3.0);
    ind.encodeChromosome(-4.5, 8, -5.0, 5.0);
    REQUIRE(ind.nextChromosomeBit() == 30);
    REQUIRE(ind.chromosome().size() == 4);
    double v;
    ind.decodeChromosomeStart();
    REQUIRE(ind.nextChromosomeBit() == 0);
    REQUIRE(ind.chromosome().size() == 4);
    ind.decodeChromosome(v, 7, 4.0, 40.0);
    REQUIRE(doubleEquals(v, 37.5, (40.0-4.0)/128.0));
    ind.decodeChromosome(v, 15, 0.0, 3.0);
    REQUIRE(doubleEquals(v, 2.7, (3.0-0.0)/32768.0));
    ind.decodeChromosome(v, 8, -5.0, 5.0);
    REQUIRE(doubleEquals(v, -4.5, (5.0+5.0)/256.0));
}

// Check the encoding and decoding of integers
TEST_CASE("GeneEncoding_Int") {
    // Create an individual
    TestIndividual ind(1);
    // Encode some integers
    ind.encodeChromosomeStart();
    REQUIRE(ind.chromosome().empty());
    REQUIRE(ind.nextChromosomeBit() == 0);
    ind.encodeChromosome(37, 7, 4);
    ind.encodeChromosome(2, 15, 0);
    ind.encodeChromosome(-4, 8, -127);
    REQUIRE(ind.nextChromosomeBit() == 30);
    REQUIRE(ind.chromosome().size() == 4);
    int v;
    ind.decodeChromosomeStart();
    REQUIRE(ind.nextChromosomeBit() == 0);
    REQUIRE(ind.chromosome().size() == 4);
    ind.decodeChromosome(v, 7, 4);
    REQUIRE(v == 37);
    ind.decodeChromosome(v, 15, 0);
    REQUIRE(v == 2);
    ind.decodeChromosome(v, 8, -127);
    REQUIRE(v == -4);
}
