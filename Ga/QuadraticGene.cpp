/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "QuadraticGene.h"
#include "QuadraticTemplate.h"
#include "Individual.h"

// Constructor
ga::QuadraticGene::QuadraticGene(const std::shared_ptr<QuadraticTemplate>& t) :
        Gene(std::dynamic_pointer_cast<GeneTemplate>(t)),
        t_(t) {
}

// Encode the gene into an individual
void ga::QuadraticGene::encode(ga::Individual* ind) {
    // Base class first
    Gene::encode(ind);
    // Now my stuff
    ind->encodeChromosome(a_, t_->a());
    ind->encodeChromosome(b_, t_->b());
    ind->encodeChromosome(c_, t_->c());
}

// Decode the gene from an individual
void ga::QuadraticGene::decode(ga::Individual* ind) {
    // Base class first
    Gene::decode(ind);
    // Now my stuff
    ind->decodeChromosome(a_, t_->a());
    ind->decodeChromosome(b_, t_->b());
    ind->decodeChromosome(c_, t_->c());
}
