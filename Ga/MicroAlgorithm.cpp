/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "MicroAlgorithm.h"
#include "Individual.h"
#include <Xml/DomObject.h>
#include "GeneTemplate.h"
#include <set>

// Perform one step of the genetic algorithm
// Returns true if the run is complete after the step
bool ga::MicroAlgorithm::step() {
    bool result = false;
    // What should we do?
    switch(state_) {
        case State::reset:
            // Initialise the algorithm
            createInitialPopulation();
            generationNumber_ = 0;
            state_ = State::evaluate;
            break;
        case State::evaluate: {
            // Are there any individuals left to evaluate?
            bool nextGeneration = true;
            for(auto& ind : population_) {
                if(!ind->evaluated()) {
                    nextGeneration = false;
                    evaluateIndividual(ind);
                    break;  // Just do one evaluation per step
                }
            }
            if(nextGeneration) {
                // All individuals have been assessed
                // Have we met the target
                double minUnfitness = population_.front()->unfitness();
                for(auto& ind : population_) {
                    std::min(minUnfitness, ind->unfitness());
                }
                if(minUnfitness < unfitnessThreshold_) {
                    state_ = State::complete;
                    result = true;
                } else {
                    breedNextGeneration();
                    generationNumber_++;
                }
            }
            break;
        }
        case State::complete:
            // Already complete, do nothing
            result = true;
            break;
    }
    return result;
}

// Reset the algorithm ready for another run
void ga::MicroAlgorithm::reset() {
    population_.clear();
    createInitialPopulation();
    state_ = State::reset;
}

// Write the configuration to the DOM object
void ga::MicroAlgorithm::writeConfig(xml::DomObject& root) const {
    // My stuff
    root << xml::Open();
    root << xml::Obj("unfitnessthreshold") << unfitnessThreshold_;
    for(auto& t : geneTemplates_) {
        root << xml::Obj("genetemplate") << *t;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void ga::MicroAlgorithm::readConfig(xml::DomObject& root) {
    // My stuff
    root >> xml::Open();
    root >> xml::Obj("unfitnessthreshold") >> unfitnessThreshold_;
    for(auto& o : root.obj("genetemplate")) {
        auto t = geneTemplateFactory_.restore(*o);
        geneTemplates_.emplace_back(t);
        *o >> *t;
    }
    root >> xml::Close();
}

// Make a gene template
ga::GeneTemplate* ga::MicroAlgorithm::makeGeneTemplate(int mode) {
    auto result = geneTemplateFactory_.make(mode);
    geneTemplates_.emplace_back(result);
    return result;
}

// Make a gene template
ga::GeneTemplate* ga::MicroAlgorithm::makeGeneTemplate(xml::DomObject& o) {
    auto result = geneTemplateFactory_.duplicate(o);
    geneTemplates_.emplace_back(result);
    o >> *result;
    return result;
}

// Write out the current population to the DOM object
void ga::MicroAlgorithm::writePopulation(xml::DomObject& root) const {
    root << xml::Open();
    // My stuff
    root << xml::Obj("generationnumber") << generationNumber_;
    // The population
    for(auto& ind : population_) {
        root << xml::Obj("individual") << *ind;
    }
    root << xml::Close();
}

// Read in the current population
void ga::MicroAlgorithm::readPopulation(xml::DomObject& root) {
    population_.clear();
    root >> xml::Open();
    // My stuff
    root >> xml::Obj("generationnumber") >> generationNumber_;
    // The population
    for(auto& o : root.obj("individual")) {
        auto ind = populationFactory_.restore(*o);
        population_.emplace_back(ind);
        *o >> *ind;
    }
    root >> xml::Close();
}

// Creates the initial population.
// Any individuals already in the population list are left there.
void ga::MicroAlgorithm::createInitialPopulation() {
    while(population_.size() < populationSize_) {
        auto ind = populationFactory_.make(theIndividualMode_);
        population_.emplace_back(ind);
        ind->breedNewcomer(random_, chromosomeSize());
        ind->makeGenes(geneTemplates_);
        ind->decodeGenes();
    }
    doNotification(notifyPopulationChanged);
}

// Breed the next generation
void ga::MicroAlgorithm::breedNextGeneration() {
    // Find the best of this generation
    std::shared_ptr<Individual> best = population_.front();
    for(auto& ind : population_) {
        if(ind->unfitness() < best->unfitness()) {
            best = ind;
        }
    }
    // Start the next generation with the best
    std::vector<std::shared_ptr<Individual>> nextGen;
    nextGen.push_back(best);
    // Is the population too in-bred?
    geneticDistance_ = 0;
    for(auto& ind : population_) {
        geneticDistance_ = std::max(geneticDistance_, ind->distanceBetween(best));
    }
    size_t chromosomeSizeBits = chromosomeSize();
    bool converged = (double) geneticDistance_ <= (double) chromosomeSizeBits * 5.0 / 100.0;
    if(converged) {
        // Generate fresh incomers
        newcomerInfluxCount_++;
        while(nextGen.size() < populationSize_) {
            std::shared_ptr<Individual> child(populationFactory_.make(theIndividualMode_));
            child->breedNewcomer(random_, chromosomeSizeBits);
            child->makeGenes(geneTemplates_);
            child->decodeGenes();
            nextGen.push_back(child);
        }
    } else {
        // Breed new children
        std::set<std::shared_ptr<Individual>> breedingPair;
        std::list<std::pair<std::shared_ptr<Individual>, std::shared_ptr<Individual>>> parentsUsed;
        while(nextGen.size() < populationSize_) {
            std::shared_ptr<Individual> child(populationFactory_.make(theIndividualMode_));
            // Select the parents
            bool usedAlready;
            do {
                breedingPair.clear();
                selectTournament(breedingPair, 2, tournamentSize_);
                // Has this pair already been used?
                usedAlready = false;
                for(auto& prev : parentsUsed) {
                    if(prev.first == *breedingPair.begin() && prev.second == *breedingPair.rbegin()) {
                        usedAlready = true;
                        break;
                    } else if(prev.second == *breedingPair.begin() && prev.first == *breedingPair.rbegin()) {
                        usedAlready = true;
                        break;
                    }
                }
            } while(usedAlready);
            // Use the parents
            std::shared_ptr<Individual> father = *breedingPair.begin();
            std::shared_ptr<Individual> mother = *breedingPair.rbegin();
            parentsUsed.emplace_back(
                    std::pair<std::shared_ptr<Individual>, std::shared_ptr<Individual>>(
                            father, mother));
            // Breed the child
            child->breedUniformCrossover(random_, father, mother);
            child->makeGenes(geneTemplates_);
            child->decodeGenes();
            nextGen.push_back(child);
        }
    }
    // Replace the current generation with the new
    population_ = nextGen;
}

// Select the breeding population using the tournament scheme
void ga::MicroAlgorithm::selectTournament(std::set<std::shared_ptr<Individual>>& breedingPool,
                                          size_t breedingPoolSize, size_t tournamentSize) {
    breedingPoolSize = std::min(breedingPoolSize, population_.size());
    tournamentSize = std::max(tournamentSize, (size_t) 1);
    tournamentSize = std::min(tournamentSize, population_.size());
    while(breedingPool.size() < breedingPoolSize) {
        // Select an individual using the tournament method
        std::shared_ptr<Individual> parent;
        for(size_t i = 0; i < tournamentSize; i++) {
            auto random = (int) random_.get(population_.size());
            std::shared_ptr<Individual> possible = population_[random];
            if(parent == nullptr || possible->unfitness() < parent->unfitness()) {
                parent = possible;
            }
        }
        // Place into the breeding pool (make sure it is not already present)
        breedingPool.insert(parent);
    }
}

// Remove a gene template from the chromosome definition
void ga::MicroAlgorithm::removeGeneTemplate(GeneTemplate* geneTemplate) {
    geneTemplates_.remove_if([geneTemplate](const std::shared_ptr<GeneTemplate>& item) {
        return item.get() == geneTemplate;
    });
}

// Clear the gene template
void ga::MicroAlgorithm::clearGeneTemplates() {
    geneTemplates_.clear();
}

// Return the current size of the chromosome
size_t ga::MicroAlgorithm::chromosomeSize() const {
    size_t result = 0;
    for(auto& t : geneTemplates_) {
        result += t->size();
    }
    return result;
}

// Find the gene template with the identifier
ga::GeneTemplate* ga::MicroAlgorithm::getGeneTemplate(int identifier) {
    ga::GeneTemplate* result = nullptr;
    for(auto& t : geneTemplates_) {
        if(t->id() == identifier) {
            result = t.get();
            break;
        }
    }
    return result;
}

// Reorder the gene templates according to the given identifier order
void ga::MicroAlgorithm::reorderGeneTemplates(const std::list<int>& order) {
    std::list<std::shared_ptr<GeneTemplate>> newTemplates;
    for(auto& id : order) {
        for(auto& t : geneTemplates_) {
            if(t->id() == id) {
                newTemplates.push_back(t);
                break;
            }
        }
    }
    geneTemplates_ = newTemplates;
}

// Return the individual with the given identifier
ga::Individual* ga::MicroAlgorithm::getIndividual(int identifier) {
    ga::Individual* result = nullptr;
    for(auto& t : population_) {
        if(t->id() == identifier) {
            result = t.get();
            break;
        }
    }
    return result;
}

