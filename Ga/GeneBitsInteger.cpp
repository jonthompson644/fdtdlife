/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsInteger.h"
#include <cmath>
#include <memory>
#include <Xml/DomObject.h>

// Equality operator
template <class T>
bool ga::GeneBitsInteger<T>::operator==(const ga::GeneBitsInteger<T>& other) const {
    return bits_ == other.bits_ && min_ == other.min_;
}

// Inequality operator
template <class T>
bool ga::GeneBitsInteger<T>::operator!=(const ga::GeneBitsInteger<T>& other) const {
    return !(*this == other);
}

// Write the specification to the XML DOM
template <class T>
void ga::GeneBitsInteger<T>::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("bits") << bits_;
    p << xml::Obj("min") << min_;
    p << xml::Close();
}

// Read the specification from the XML DOM
template <class T>
void ga::GeneBitsInteger<T>::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("bits") >> xml::Default(bits_) >> bits_;
    p >> xml::Obj("min") >> xml::Default(min_) >> min_;
    p >> xml::Close();
}

// Encode a value into the chromosome
template <class T>
uint64_t ga::GeneBitsInteger<T>::encode(T value) const {
    uint64_t result = 0ULL;
    if(bits_ > 0) {
        T max = static_cast<T>(1ULL << bits_) - 1;
        if(value > max) {
            value = max;
        }
        if(value >= min_) {
            result = static_cast<uint64_t>(value - min_);
        }
    }
    return result;
}

// Decode a value from the chromosome
template <class T>
T ga::GeneBitsInteger<T>::decode(uint64_t bits) const {
    T result = min_;
    if(bits_ > 0) {
        result += static_cast<T>(bits);
    }
    return result;
}

// Explicit instantiation
template class ga::GeneBitsInteger<int>;
template class ga::GeneBitsInteger<size_t>;
