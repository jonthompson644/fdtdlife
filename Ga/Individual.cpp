/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Individual.h"
#include <Xml/DomObject.h>
#include "GeneTemplate.h"
#include "Gene.h"
#include <cmath>

// Second stage construction used by factory
void ga::Individual::construct(int mode, int id) {
    mode_ = mode;
    id_ = id;
}

// Record individual evaluation information
void ga::Individual::evaluated(double unfitness) {
    evaluated_ = true;
    unfitness_ = unfitness;
}

// Write the individual
void ga::Individual::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << id_;
    root << xml::Obj("evaluated") << evaluated_;
    root << xml::Obj("unfitness") << unfitness_;
    root << xml::Obj("chromosome") << chromosome_;
    root << xml::Obj("mother") << mother_;
    root << xml::Obj("father") << father_;
    root << xml::Close();
}

// Read the individual
void ga::Individual::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("evaluated") >> xml::Default(false) >> evaluated_;
    root >> xml::Obj("unfitness") >> xml::Default(0.0) >> unfitness_;
    root >> xml::Obj("chromosome") >> chromosome_;
    root >> xml::Obj("mother") >> mother_;
    root >> xml::Obj("father") >> father_;
    root >> xml::Close();
}

// Initialise the chromosome ready for encoding the genes
void ga::Individual::encodeChromosomeStart() {
    chromosome_.clear();
    nextChromosomeBit_ = 0;
}

// Encode a double into the chromosome
void ga::Individual::encodeChromosome(double value, const GeneBitsFloat& spec) {
    encodeGene(spec.encode(value), spec.bits());
}

// Encode a double into the chromosome
void ga::Individual::encodeChromosome(double value, const GeneBitsLogFloat& spec) {
    encodeGene(spec.encode(value), spec.bits());
}

// Encode an integer into the chromosome
template <class T>
void ga::Individual::encodeChromosome(T value, const GeneBitsInteger<T>& spec) {
    encodeGene(spec.encode(value), spec.bits());
}
template void ga::Individual::encodeChromosome<int>(int value, const GeneBitsInteger<int>& spec);
template void ga::Individual::encodeChromosome<size_t>(size_t value, const GeneBitsInteger<size_t>& spec);

// Encode bits into the chromosome
void ga::Individual::encodeGene(uint64_t v, size_t numBits) {
    for(size_t bit=0; bit<numBits; bit++) {
        uint64_t dataBit = (v >> bit) & 1U;
        size_t byteNum = nextChromosomeBit_ / bitsPerByte_;
        size_t bitNum = nextChromosomeBit_ % bitsPerByte_;
        chromosome_.resize(byteNum + 1, 0);
        chromosome_[byteNum] |= static_cast<uint8_t>(dataBit << bitNum);
        nextChromosomeBit_++;
    }
}

// Initialise the chromosome ready for decoding the genes
void ga::Individual::decodeChromosomeStart() {
    nextChromosomeBit_ = 0;
}

// Decode bits from the chromosome
uint64_t ga::Individual::decodeGene(size_t numBits) {
    uint64_t v = 0;
    for(size_t bit=0; bit<numBits; bit++) {
        size_t byteNum = nextChromosomeBit_ / bitsPerByte_;
        size_t bitNum = nextChromosomeBit_ % bitsPerByte_;
        if(byteNum < chromosome_.size()) {
            auto byteVal = static_cast<uint64_t>(chromosome_[byteNum]);
            auto dataBit = static_cast<uint64_t>((byteVal >> bitNum) & 1U);
            v |= (dataBit << bit);
        }
        nextChromosomeBit_++;
    }
    return v;
}

// Decode a double from the chromosome
void ga::Individual::decodeChromosome(double& value, const GeneBitsFloat& spec) {
    value = spec.decode(decodeGene(spec.bits()));
}

// Decode a double from the chromosome
void ga::Individual::decodeChromosome(double& value, const GeneBitsLogFloat& spec) {
    value = spec.decode(decodeGene(spec.bits()));
}

// Decode an int from the chromosome
template <class T>
void ga::Individual::decodeChromosome(T& value, const GeneBitsInteger<T>& spec) {
    value = spec.decode(decodeGene(spec.bits()));
}
template void ga::Individual::decodeChromosome<int>(int& value, const GeneBitsInteger<int>& spec);
template void ga::Individual::decodeChromosome<size_t>(size_t& value, const GeneBitsInteger<size_t>& spec);

// Create a chromosome containing random data and no parents
void ga::Individual::breedNewcomer(box::Random& random, size_t numBits) {
    size_t numBytes = (numBits + bitsPerByte_ - 1) / bitsPerByte_;
    uint8_t lastByteMask = (uint8_t{1U} << static_cast<uint8_t>(numBits % bitsPerByte_)) - uint8_t{1U};
    chromosome_.resize(numBytes);
    for (uint8_t &v : chromosome_) {
        v = static_cast<uint8_t>(random.get(size_t{256U}));
    }
    if(!chromosome_.empty() && lastByteMask != 0) {
        chromosome_.back() &= lastByteMask;
    }
    mother_ = noParent_;
    father_ = noParent_;
}

// Populate the individual's genes
void ga::Individual::makeGenes(const std::list<std::shared_ptr<GeneTemplate>>& geneTemplates) {
    genes_.clear();
    for(auto& t : geneTemplates) {
        genes_.emplace_back(t->makeGene());
    }
}

// Encode the genes into the chromosome
void ga::Individual::encodeGenes() {
    encodeChromosomeStart();
    for(auto& g : genes_) {
        g->encode(this);
    }
}

// Decode the genes from the chromosome
void ga::Individual::decodeGenes() {
    decodeChromosomeStart();
    for(auto& g : genes_) {
        g->decode(this);
    }
}

// Return the genetic distance between this individual and the other
size_t ga::Individual::distanceBetween(const std::shared_ptr<Individual>& other) {
    size_t smallest = std::min(chromosome_.size(), other->chromosome_.size());
    size_t largest = std::max(chromosome_.size(), other->chromosome_.size());
    size_t result = largest - smallest;
    for(size_t i=0; i<smallest; i++) {
        size_t bytePos = i / bitsPerByte_;
        size_t bitPos = i % bitsPerByte_;
        if((chromosome_[bytePos] & (1U << bitPos)) != ((other->chromosome_[bytePos] & (1U << bitPos)))) {
            result++;
        }
    }
    return result;
}

// Create a chromosome consisting of the parent's genetic data using
// uniform crossover
void ga::Individual::breedUniformCrossover(box::Random& random, const
std::shared_ptr<ga::Individual>& father,
                                           const std::shared_ptr<ga::Individual>& mother) {
    mother_ = mother->id();
    father_ = father->id();
    // The size of the genome will be that of the father
    chromosome_.resize(father->chromosome_.size());
    // For each byte in the two genomes...
    for(size_t i=0; i<chromosome_.size(); i++) {
        auto fatherMask = static_cast<uint8_t>(random.get(static_cast<size_t>(256)));
        uint8_t motherMask = ~fatherMask;
        uint8_t newData = father->chromosome_[i];
        if(i < mother->chromosome_.size()) {
            newData = static_cast<uint8_t>(newData & fatherMask) |
                    static_cast<uint8_t>(mother->chromosome_[i] & motherMask);
        }
        chromosome_[i] = newData;
    }
}
