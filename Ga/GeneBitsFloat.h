/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GENEBITSFLOAT_H
#define FDTDLIFE_GENEBITSFLOAT_H

#include <cstddef>
#include <cstdint>
namespace xml {class DomObject;}

namespace ga {

    // A class the represents the gene bits specification for a double
    // with a min and a max range.
    class GeneBitsFloat {
    public:
        // Construction
        GeneBitsFloat() = default;
        GeneBitsFloat(size_t bits, double min, double max) : bits_(bits), min_(min), max_(max) {}
        // Operators
        bool operator==(const GeneBitsFloat& other) const;
        bool operator!=(const GeneBitsFloat& other) const;
        // API
        void writeXml(xml::DomObject& p) const;
        void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneBitsFloat& v) {v.writeXml(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneBitsFloat& v) {v.readXml(o); return o;}
        virtual uint64_t encode(double v) const;
        virtual double decode(uint64_t bits) const;
        // Getters
        size_t bits() const {return bits_;}
        double min() const {return min_;}
        double max() const {return max_;}
        // Setters
        void bits(size_t v) {bits_ = v;}
        void min(double v) {min_ = v;}
        void max(double v) {max_ = v;}
    protected:
        // Members
        size_t bits_ {};
        double min_ {};
        double max_ {};
        // Helpers
        virtual double scaling() const;
    };

}


#endif //FDTDLIFE_GENEBITSFLOAT_H
