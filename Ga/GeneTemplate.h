/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GA_GENETEMPLATE_H
#define FDTDLIFE_GA_GENETEMPLATE_H

#include <memory>
namespace xml {class DomObject;}

namespace ga {
    class Gene;

    // Base class for gene templates
    class GeneTemplate : public std::enable_shared_from_this<GeneTemplate>  {
    public:
        // Construction
        GeneTemplate() = default;
        virtual ~GeneTemplate() = default;
        virtual void construct(int mode, int id);

        // API
        virtual std::shared_ptr<Gene> makeGene() = 0;
        virtual size_t size() const = 0;
        virtual void writeXml(xml::DomObject& p) const;
        virtual void readXml(xml::DomObject& p);
        friend xml::DomObject& operator<<(xml::DomObject& o, const GeneTemplate& v) {v.writeXml(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, GeneTemplate& v) {v.readXml(o); return o;}

        // Getters
        int mode() const {return mode_;}
        int id() const {return id_;}

    protected:
        // Attributes
        int mode_ {-1};   // The type of this gene template
        int id_ {-1};  // The instance identifier
    };
}


#endif //FDTDLIFE_GA_GENETEMPLATE_H
