/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GeneBitsFloat.h"
#include <cmath>
#include <memory>
#include <Xml/DomObject.h>

// Equality operator
bool ga::GeneBitsFloat::operator==(const ga::GeneBitsFloat& other) const {
    return bits_ == other.bits_ && min_ == other.min_ &&
           max_ == other.max_;
}

// Inequality operator
bool ga::GeneBitsFloat::operator!=(const ga::GeneBitsFloat& other) const {
    return !(*this == other);
}

// Write the specification to the XML DOM
void ga::GeneBitsFloat::writeXml(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("bits") << bits_;
    p << xml::Obj("min") << min_;
    p << xml::Obj("max") << max_;
    p << xml::Close();
}

// Read the specification from the XML DOM
void ga::GeneBitsFloat::readXml(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("bits") >> xml::Default(bits_) >> bits_;
    p >> xml::Obj("min") >> xml::Default(min_) >> min_;
    p >> xml::Obj("max") >> xml::Default(max_) >> max_;
    p >> xml::Close();
}

// Calculate the scaling factor
double ga::GeneBitsFloat::scaling() const {
    double result;
    result = (max_ - min_) / static_cast<double>(1ULL << bits_);
    if(result <= 0.0) {
        result = 1.0;
    }
    return result;
}

// Encode a value into the chromosome
uint64_t ga::GeneBitsFloat::encode(double value) const {
    uint64_t result = 0ULL;
    if(bits_ > 0) {
        result = static_cast<uint64_t>(std::round((value - min_) / scaling()));
    }
    return result;
}

// Decode a value from the chromosome
double ga::GeneBitsFloat::decode(uint64_t bits) const {
    double result = min_;
    if(bits_ > 0) {
        result = static_cast<double>(bits) * scaling() + min_;
    }
    return result;
}

