/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_QUADRATICTEMPLATE_H
#define FDTDLIFE_QUADRATICTEMPLATE_H

#include "GeneTemplate.h"
#include "GeneBitsFloat.h"

namespace ga {

    // A gene template base class that implements quadratic behaviour
    // y = ax^2 + bx + c
    class QuadraticTemplate : public GeneTemplate {
    public:
        // Construction
        QuadraticTemplate() = default;

        // Overrides of GeneTemplate
        size_t size() const override;
        void writeXml(xml::DomObject& p) const override;
        void readXml(xml::DomObject& p) override;

        // Getters
        const ga::GeneBitsFloat& a() const {return a_;}
        const ga::GeneBitsFloat& b() const {return b_;}
        const ga::GeneBitsFloat& c() const {return c_;}

        // Setters
        void a(const ga::GeneBitsFloat& v) {a_ = v;}
        void b(const ga::GeneBitsFloat& v) {b_ = v;}
        void c(const ga::GeneBitsFloat& v) {c_ = v;}

    protected:
        // Gene bits layout
        ga::GeneBitsFloat a_;
        ga::GeneBitsFloat b_;
        ga::GeneBitsFloat c_;
    };

}


#endif //FDTDLIFE_QUADRATICTEMPLATE_H
