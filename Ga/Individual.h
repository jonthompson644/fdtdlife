/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GA_INDIVIDUAL_H
#define FDTDLIFE_GA_INDIVIDUAL_H

#include <cstdint>
#include <vector>
#include <memory>
#include <list>
#include <Box/Random.h>
#include "GeneBitsInteger.h"
#include "GeneBitsFloat.h"
#include "GeneBitsLogFloat.h"

namespace xml { class DomObject; }

namespace ga {

    class Gene;
    class GeneTemplate;

    // Base class for genetic algorithm individuals
    class Individual : public std::enable_shared_from_this<Individual> {
    public:
        // Construction
        Individual() = default;
        virtual ~Individual() = default;
        virtual void construct(int mode, int id);

        // API
        void evaluated(double unfitness);
        virtual void write(xml::DomObject& root) const;
        virtual void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Individual& e) {e.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Individual& e) {e.read(o); return o;}
        void encodeChromosomeStart();
        void encodeChromosome(double value, const GeneBitsFloat& spec);
        void encodeChromosome(double value, const GeneBitsLogFloat& spec);
        template <class T>
        void encodeChromosome(T value, const GeneBitsInteger<T>& spec);
        void decodeChromosomeStart();
        void decodeChromosome(double& value, const GeneBitsFloat& spec);
        void decodeChromosome(double& value, const GeneBitsLogFloat& spec);
        template <class T>
        void decodeChromosome(T& value, const GeneBitsInteger<T>& spec);
        void breedNewcomer(box::Random& random, size_t numBits);
        void breedUniformCrossover(box::Random& random, const std::shared_ptr<Individual>&
                father,
                   const std::shared_ptr<Individual>& mother);
        void makeGenes(const std::list<std::shared_ptr<GeneTemplate>>& geneTemplates);
        void decodeGenes();
        void encodeGenes();
        size_t distanceBetween(const std::shared_ptr<Individual>& other);

        // Getters
        double unfitness() const {return unfitness_;}
        bool evaluated() const {return evaluated_;}
        int id() const {return id_;}
        size_t nextChromosomeBit() const {return nextChromosomeBit_;}
        const std::vector<uint8_t>& chromosome() const {return chromosome_;}
        const std::vector<std::shared_ptr<Gene>> genes() const {return genes_;}
        int mother() const {return mother_;}
        int father() const {return father_;}

    protected:
        // State of the individual
        double unfitness_ {};   // The measured unfitness
        bool evaluated_ {};   // True if the unfitness has been measured
        int id_ {-1};  // The identity number of the individual
        std::vector<uint8_t> chromosome_;  // The encoded genome
        size_t nextChromosomeBit_ {};  // Used during encoding and decoding to track bit position
        std::vector<std::shared_ptr<Gene>> genes_;  // The decoded genes
        int mother_ {noParent_};  // The identifier of the mother
        int father_ {noParent_};  // The identifier of the father
        int mode_ {};  // The individual mode

        // Constants
        static const size_t bitsPerByte_ = 8;  // Number of bits in a byte
        static const int noParent_ = -1;  // Identifier of a missing parent

        // Helpers
        void encodeGene(uint64_t v, size_t numBits);
        uint64_t decodeGene(size_t numBits);
    };
}


#endif //FDTDLIFE_GA_INDIVIDUAL_H
