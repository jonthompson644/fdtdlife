/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GA_MICROALGORITHM_H
#define FDTDLIFE_GA_MICROALGORITHM_H

#include <Box/Random.h>
#include <cstddef>
#include <vector>
#include <memory>
#include <list>
#include <set>
#include <Box/Factory.h>
#include <Fdtd/Notifier.h>

namespace xml { class DomObject; }

namespace ga {

    class Individual;
    class GeneTemplate;

    // Base class for a micro genetic algorithm
    class MicroAlgorithm : public fdtd::Notifier {
    public:
        // Types
        enum class State {reset, evaluate, complete};
        enum {notifyPopulationChanged};

        // Constants
        static const size_t defaultPopulationSize_ = 5;
        static const size_t defaultTournamentSize_ = 2;
        static const int theIndividualMode_ = 0;  // The one and only individual mode

        // Construction
        MicroAlgorithm() = default;
        ~MicroAlgorithm() override = default;
        void construct(fdtd::NotificationMgr* mgr) {Notifier::connectMgr(mgr);}

        // API
        bool step();    // Call this to perform one step of the genetic algorithm
        void reset();   // Abandons the current run and prepares for a new one
        //virtual std::vector<std::shared_ptr<Individual>> breedNextGeneration() = 0;  // Override to breed the next generation
        virtual void evaluateIndividual(std::shared_ptr<Individual> ind) = 0;  // Override to evaluate an individual
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const MicroAlgorithm& e) {e.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, MicroAlgorithm& e) {e.readConfig(o); return o;}
        void writePopulation(xml::DomObject& root) const;
        void readPopulation(xml::DomObject& root);
        void removeGeneTemplate(GeneTemplate* geneTemplate);
        GeneTemplate* getGeneTemplate(int identifier);
        Individual* getIndividual(int identifier);
        void clearGeneTemplates();
        size_t chromosomeSize() const;
        GeneTemplate* makeGeneTemplate(int mode);
        GeneTemplate* makeGeneTemplate(xml::DomObject& o);
        void reorderGeneTemplates(const std::list<int>& order);

        // Getters
        const std::vector<double>& unfitnessProgress() const {return unfitnessProgress_;}
        double unfitnessThreshold() const {return unfitnessThreshold_;}
        State state() const {return state_;}
        const std::vector<std::shared_ptr<Individual>>& population() const {return population_;}
        size_t populationSize() const {return populationSize_;}
        int generationNumber() const {return generationNumber_;}
        const std::list<std::shared_ptr<GeneTemplate>>& geneTemplates() const {return geneTemplates_;}
        size_t tournamentSize() const {return tournamentSize_;}
        const box::Factory<GeneTemplate>& geneTemplateFactory() const {return geneTemplateFactory_;}

        // Setters
        void unfitnessThreshold(double v) {unfitnessThreshold_ = v;}
    protected:
        // Configuration
        double unfitnessThreshold_ {0.01};
        size_t populationSize_ {defaultPopulationSize_};
        size_t tournamentSize_ {defaultTournamentSize_};
        // Attributes
        std::vector<double> unfitnessProgress_;
        box::Factory<Individual> populationFactory_;
        std::vector<std::shared_ptr<Individual>> population_;
        int generationNumber_ {};
        State state_ {State::reset};
        box::Factory<GeneTemplate> geneTemplateFactory_;
        std::list<std::shared_ptr<GeneTemplate>> geneTemplates_;
        box::Random random_;
        size_t geneticDistance_ {};
        size_t newcomerInfluxCount_ {};

        // Helper functions
        void createInitialPopulation();
        void breedNextGeneration();
        void selectTournament(std::set<std::shared_ptr<Individual>>& breedingPool,
                size_t breedingPoolSize, size_t tournamentSize);
    };
}


#endif //FDTDLIFE_GA_MICROALGORITHM_H
