#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Library
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AdmittanceCatalogue.cpp \
    AdmittanceData.cpp \
    Catalogue.cpp \
    CatalogueBuilder.cpp \
    CatalogueItem.cpp \
    CatalogueItemJob.cpp \
    CatalogueItemWrapper.cpp \
    CpmlField.cpp \
    CubeGeometry.cpp \
    ECpml.cpp \
    EField.cpp \
    EHexCylinder.cpp \
    ENormal.cpp \
    ESplit.cpp \
    Field.cpp \
    HCpml.cpp \
    HField.cpp \
    HHexCylinder.cpp \
    HNormal.cpp \
    HSplit.cpp \
    HexCylinderGeometry.cpp \
    IndividualJob.cpp \
    Job.cpp \
    LocalNode.cpp \
    Model.cpp \
    Node.cpp \
    NodeManager.cpp \
    NormalField.cpp \
    Notifiable.cpp \
    NotificationData.cpp \
    NotificationMgr.cpp \
    Notifier.cpp \
    Parameters.cpp \
    PlateAdmittance.cpp \
    PropMatrix.cpp \
    RecordedData.cpp \
    SingleJob.cpp \
    SplitField.cpp \
    StripeJob.cpp \
    Geometry.cpp

HEADERS += \
    AdmittanceCatalogue.h \
    AdmittanceData.h \
    Catalogue.h \
    CatalogueBuilder.h \
    CatalogueItem.h \
    CatalogueItemJob.h \
    CatalogueItemWrapper.h \
    CpmlField.h \
    CubeGeometry.h \
    ECpml.h \
    EField.h \
    EHexCylinder.h \
    ENormal.h \
    ESplit.h \
    Field.h \
    Geometry.h \
    HCpml.h \
    HField.h \
    HHexCylinder.h \
    HNormal.h \
    HSplit.h \
    HexCylinderGeometry.h \
    IndividualJob.h \
    Job.h \
    LocalNode.h \
    Model.h \
    Node.h \
    NodeManager.h \
    NormalField.h \
    Notifiable.h \
    NotificationData.h \
    NotificationMgr.h \
    Notifier.h \
    Parameters.h \
    PlateAdmittance.h \
    PropMatrix.h \
    RecordedData.h \
    SingleJob.h \
    SplitField.h \
    StripeJob.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Library.pro

DISTFILES += \
    Library.pro.user \
    CMakeLists.txt \
    Makefile
