/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CpmlField.h"
#include "Model.h"
#include <Box/BinaryFileHeader.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>

// Constructor
// m - The model
fdtd::CpmlField::CpmlField(Configuration* p) :
        p_(p) {
}

// Initialise the data
void fdtd::CpmlField::initialise() {
    // Create the data
    arraySize_ = p_->geometry()->arraySize();
    x_.resize(arraySize_);
    y_.resize(arraySize_);
    z_.resize(arraySize_);
    // Initialise the data
    for(size_t i=0; i<arraySize_; i++) {
        x_[i] = 0.0;
        y_[i] = 0.0;
        z_[i] = 0.0;
    }
    // Create the psi arrays.  We need to work out how big to make them first
    arraySizeWxPsi_ = static_cast<size_t>(p_->geometry()->n().y() * p_->geometry()->n().z() * p_->boundarySize() * 2);
    arraySizeWyPsi_ = static_cast<size_t>(p_->geometry()->n().x() * p_->geometry()->n().z() * p_->boundarySize() * 2);
    arraySizeWzPsi_ = static_cast<size_t>(p_->geometry()->n().x() * p_->geometry()->n().y() * p_->boundarySize() * 2);
    xyPsi_.resize(arraySizeWyPsi_);
    xzPsi_.resize(arraySizeWzPsi_);
    yzPsi_.resize(arraySizeWzPsi_);
    yxPsi_.resize(arraySizeWxPsi_);
    zxPsi_.resize(arraySizeWxPsi_);
    zyPsi_.resize(arraySizeWyPsi_);
    // Initialise the data
    for(size_t i=0; i<arraySizeWxPsi_; i++) {
        yxPsi_[i] = 0.0;
        zxPsi_[i] = 0.0;
    }
    for(size_t i=0; i<arraySizeWyPsi_; i++) {
        xyPsi_[i] = 0.0;
        zyPsi_[i] = 0.0;
    }
    for(size_t i=0; i<arraySizeWzPsi_; i++) {
        xzPsi_[i] = 0.0;
        yzPsi_[i] = 0.0;
    }
}

// Return the vector at a point
box::Vector<double> fdtd::CpmlField::value(const box::Vector<size_t>& cell) {
    size_t idx = p_->geometry()->index(cell.x(),cell.y(),cell.z());
    return box::Vector<double>(x_[idx], y_[idx], z_[idx]);
}

// Print the vector field contents
void fdtd::CpmlField::print(const char* title) {
    fdtd::Geometry* g = p_->geometry();
    if(g->n().x()<=10 && g->n().y()<=10 && g->n().z()<=10) {
        if(title != nullptr) {
            std::cout << title << std::endl;
        }
        std::ios oldState(nullptr);
        oldState.copyfmt(std::cout);
        std::cout << std::scientific << std::setprecision(0) << std::showpos;
        for(size_t j=0; j<g->n().y(); j++) {
            for(size_t k=0; k<g->n().z(); k++) {
                for(size_t i=0; i<g->n().x(); i++) {
                    double v = x_[g->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(size_t j=0; j<g->n().y(); j++) {
            for(size_t k=0; k<g->n().z(); k++) {
                for(size_t i=0; i<g->n().x(); i++) {
                    double v = y_[g->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(size_t j=0; j<g->n().y(); j++) {
            for(size_t k=0; k<g->n().z(); k++) {
                for(size_t i=0; i<g->n().x(); i++) {
                    double v = z_[g->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "=======" << std::endl;
        std::cout.copyfmt(oldState);
    }
}

// Return the amount of memory used
size_t fdtd::CpmlField::memoryUse() {
    return 3 * p_->geometry()->arraySize() * sizeof(double);
}

// Write the data to a file
bool fdtd::CpmlField::writeData(std::ofstream &file) {
    box::BinaryFileHeader header("CUDACPMLFIELD", 1, arraySize_*sizeof(double)*3);
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    file.write(reinterpret_cast<char*>(x_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    file.write(reinterpret_cast<char*>(y_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    file.write(reinterpret_cast<char*>(z_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    return true;
}

// Read the data from a file
bool fdtd::CpmlField::readData(std::ifstream &file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("CUDACPMLFIELD")) {
        switch(header.version()) {
            case 1:
                arraySize_ = header.recordSize() / 3U / sizeof(double);
                file.read(reinterpret_cast<char*>(x_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                file.read(reinterpret_cast<char*>(y_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                file.read(reinterpret_cast<char*>(z_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                result = arraySize_ == p_->geometry()->arraySize();
            break;
            default:
                break;
        }
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
size_t fdtd::CpmlField::wxPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary) {
    inBoundary = false;
    size_t result = 0;
    fdtd::Geometry* g = p_->geometry();
    // Which PML are we in?
    if(x < p_->boundarySize()) {
        // The left hand PML
        result = y + (z * g->n().y()) + (x * g->n().y() * g->n().z());
        inBoundary = true;
    } else if(x >= g->n().x()-p_->boundarySize() && x < g->n().x()) {
        // The right hand PML
        result = y + (z * g->n().y()) + ((g->n().x()-x-1+p_->boundarySize()) * g->n().y() * g->n().z());
        inBoundary = true;
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
size_t fdtd::CpmlField::wyPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary) {
    inBoundary = false;
    size_t result = 0;
    fdtd::Geometry* g = p_->geometry();
    // Which PML are we in?
    if(y < p_->boundarySize()) {
        // The left hand PML
        result = x + (z * g->n().x()) + (y * g->n().x() * g->n().z());
        inBoundary = true;
    } else if(y >= g->n().y()-p_->boundarySize() && y < g->n().y()) {
        // The right hand PML
        result = x + (z * g->n().x()) + ((g->n().y()-y-1+p_->boundarySize()) * g->n().x() * g->n().z());
        inBoundary = true;
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
size_t fdtd::CpmlField::wzPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary) {
    inBoundary = false;
    size_t result = 0;
    fdtd::Geometry* g = p_->geometry();
    // Which PML are we in?
    if(z < p_->boundarySize()) {
        // The left hand PML
        result = x + (y * g->n().x()) + (z * g->n().x() * g->n().y());
        inBoundary = true;
    } else if(z >= g->n().z()-p_->boundarySize() && z < g->n().z()) {
        // The right hand PML
        result = x + (y * g->n().x()) + ((g->n().z()-z-1+p_->boundarySize()) * g->n().x() * g->n().y());
        inBoundary = true;
    }
    return result;
}
