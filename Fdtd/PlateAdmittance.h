/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PLATEADMITTANCE_H
#define FDTDLIFE_PLATEADMITTANCE_H

#include <complex>
#include <map>
#include "AdmittanceData.h"
#include <Xml/DomObject.h>

namespace fdtd {

    // A mixer class that calculates the admittance of a plate
    class PlateAdmittance {
    public:
        enum PlateType {
            plateTypeSquarePlate, plateTypeSquareHole
        };
        enum Formula {
            formulaLeeEtAl = 0, formulaUlrich, formulaArnaudEtAl, formulaChen, formulaTable
        };
    public:
        // Construction
        PlateAdmittance();
        std::complex<double> admittance(PlateType plateType, double frequency, double cellSize, double plateSize);

        // API
        void writeConfig(xml::DomObject& p) const;
        void readConfig(xml::DomObject& p);
        void clear();
        friend xml::DomObject& operator<<(xml::DomObject& o, const PlateAdmittance& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, PlateAdmittance& v) {v.readConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, PlateType& v) {o >> (int&)v; return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Formula& v) {o >> (int&)v; return o;}

        // Getters
        Formula formula() const {return formula_;}
        const std::map<double, AdmittanceData>& admittanceTable() const {return table_;}
        PlateType tableType() const {return tableType_;}

        // Setters
        void formula(Formula v) {formula_ = v;}
        void admittanceTable(const std::map<double, AdmittanceData>& v) {table_ = v;}
        void tableType(PlateType v) {tableType_ = v;}

    protected:
        // Members
        Formula formula_;
        std::map<double, AdmittanceData> table_;  // Admittance data sorted by plate/hole size
        PlateType tableType_;  // Kind of admittance data stored in the table
        static constexpr double defaultPlateSize_ = 50.0;

        // Helpers
        std::complex<double> getTableAdmittance(double frequency, double cellSize, double plateSize);
    };
}


#endif //FDTDLIFE_PLATEADMITTANCE_H
