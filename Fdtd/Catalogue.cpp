/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Catalogue.h"
#include <Xml/DomObject.h>
#include <Box/BinaryPattern.h>
#include "CatalogueItem.h"
#include <Box/Constants.h>
#include <Box/BinaryPattern4Fold.h>

// Constructor
fdtd::Catalogue::Catalogue(size_t bitsPerHalfSide, bool fourFoldSymmetry, double unitCell) :
        bitsPerHalfSide_(bitsPerHalfSide),
        fourFoldSymmetry_(fourFoldSymmetry),
        unitCell_(unitCell),
        startFrequency_(1 * box::Constants::giga_),
        frequencyStep_(1 * box::Constants::giga_) {
}

// Write the catalogue to the XML
void fdtd::Catalogue::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    root << xml::Obj("fourfoldsymmetry") << fourFoldSymmetry_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("startfrequency") << startFrequency_;
    root << xml::Obj("frequencystep") << frequencyStep_;
    for(auto& pos : items_) {
        root << xml::Obj("catalogueitem") << *pos.second;
    }
    root << xml::Obj("inclcornertocorner") << inclCornerToCorner_;
    root << xml::Close();
}

// Read the catalogue from the XML
void fdtd::Catalogue::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("bitsperhalfside") >> xml::Default(bitsPerHalfSide_)
         >> bitsPerHalfSide_;
    root >> xml::Obj("fourfoldsymmetry") >> xml::Default(fourFoldSymmetry_)
         >> fourFoldSymmetry_;
    root >> xml::Obj("unitcell") >> xml::Default(0.0) >> unitCell_;
    root >> xml::Obj("startfrequency") >> xml::Default(0.0) >> startFrequency_;
    root >> xml::Obj("frequencystep") >> xml::Default(0.0) >> frequencyStep_;
    for(auto& p : root.obj("catalogueitem")) {
        auto item = std::make_shared<CatalogueItem>();
        *p >> *item;
        items_[item->code()] = item;
    }
    root >> xml::Obj("inclcornertocorner") >> xml::Default(inclCornerToCorner_)
         >> inclCornerToCorner_;
    root >> xml::Close();
    setRequiredCodes();
}

// Return the last code number
uint64_t fdtd::Catalogue::lastCode() {
    size_t numCodingBits = 0;
    if(fourFoldSymmetry_) {
        for(size_t i = 1; i <= bitsPerHalfSide_; i++) {
            numCodingBits += i;
        }
    } else {
        numCodingBits = bitsPerHalfSide_ * bitsPerHalfSide_;
    }
    // The maximum coding number
    return ((uint64_t) 1 << (uint16_t) numCodingBits) - 1ULL;
}

// Return a block of items missing from the catalogue.
// Note that we do not add the items to the catalogue here, they
// will be added once they have been modelled.
void fdtd::Catalogue::getMissingItems(
        int numItems, std::list<std::shared_ptr<CatalogueItem>>& missingItems) {
    // The maximum coding number
    uint64_t maxCoding = lastCode();
    // Now create items that are missing from the catalogue
    uint64_t coding = 0;
    while(coding < maxCoding && (int) missingItems.size() < numItems) {
        makeMissingItem(coding, missingItems);
        coding++;
    }
    // Do one more, the maximum coding.  Doing it like this allows
    // the case where all the values in a uint64_t are valid codes.
    makeMissingItem(coding, missingItems);
}

// Make a missing item and add it to the list
void fdtd::Catalogue::makeMissingItem(uint64_t coding,
                                      std::list<std::shared_ptr<CatalogueItem>>& missingItems) {
    // Is this coding missing?
    if(requiredCodes_.count(coding) > 0 && items_.find(coding) == items_.end() &&
       symmetricalCodes_.count(coding) == 0) {
        // Convert the coding to a 2 fold pattern
        uint64_t pattern;
        if(fourFoldSymmetry_) {
            // The bit location table
            std::vector<uint64_t> bitLocation;
            bitLocation.resize((size_t) bitsPerHalfSide_ * (size_t) bitsPerHalfSide_);
            uint64_t v = 0;
            for(size_t i = 0; i < bitsPerHalfSide_; i++) {
                for(size_t j = i; j < bitsPerHalfSide_; j++) {
                    bitLocation[i * bitsPerHalfSide_ + j] = v;
                    bitLocation[j * bitsPerHalfSide_ + i] = v;
                    v++;
                }
            }
            // Convert the pattern
            pattern = 0;
            for(uint64_t bitNum2 = 0ULL;
                bitNum2 < bitsPerHalfSide_ * bitsPerHalfSide_; bitNum2++) {
                uint64_t bitNum4 = bitLocation[(size_t) bitNum2];
                uint64_t bit = (coding >> bitNum4) & (uint64_t) 1;
                pattern |= (bit << bitNum2);
            }
        } else {
            // The coding is the pattern
            pattern = coding;
        }
        // Make the missing item
        auto missingItem = std::make_shared<CatalogueItem>(coding, pattern);
        missingItems.push_back(missingItem);
    }
}

// Return an item identified by its code
std::shared_ptr<fdtd::CatalogueItem> fdtd::Catalogue::getItem(uint64_t code) {
    std::shared_ptr<fdtd::CatalogueItem> result;
    auto pos = items_.find(code);
    if(pos != items_.end()) {
        result = pos->second;
    }
    return result;
}

// Add an item to the catalogue
void fdtd::Catalogue::addItem(const std::shared_ptr<fdtd::CatalogueItem>& item) {
    // Add this item
    items_[item->code()] = item;
    // Now add any items using symmetries
    if(fourFoldSymmetry_) {
        // TODO: Any 4 fold symmetries
    } else {
        box::BinaryPattern t;
        // Half cell X translation
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateX();
        add2FoldSymmetryItem(t.pattern()[0], item, false);
        // Half cell Y translation
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateY();
        add2FoldSymmetryItem(t.pattern()[0], item, false);
        // Both translations
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateX();
        t.translateY();
        add2FoldSymmetryItem(t.pattern()[0], item, false);
        // Rotate 90
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.rotate90();
        add2FoldSymmetryItem(t.pattern()[0], item, true);
        // X translate and rotate 90
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateX();
        t.rotate90();
        add2FoldSymmetryItem(t.pattern()[0], item, true);
        // Y translate and rotate 90
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateY();
        t.rotate90();
        add2FoldSymmetryItem(t.pattern()[0], item, true);
        // Both translations and rotate 90
        t.initialise((size_t) bitsPerHalfSide_, {item->pattern()});
        t.translateX();
        t.translateY();
        t.rotate90();
        add2FoldSymmetryItem(t.pattern()[0], item, true);
    }
}

// Add a two fold symmetry item to the catalogue if it does not already exist
void fdtd::Catalogue::add2FoldSymmetryItem(
        uint64_t pattern, const std::shared_ptr<fdtd::CatalogueItem>& item, bool xySwap) {
    if(items_.count(pattern) == 0) {
        auto txItem = std::make_shared<CatalogueItem>(pattern, pattern);
        txItem->copyData(*item, xySwap);
        items_[txItem->code()] = txItem;
    }
}

// Fill the set of codes required by this catalogue
void fdtd::Catalogue::setRequiredCodes() {
    requiredCodes_.clear();
    symmetricalCodes_.clear();
    if((fourFoldSymmetry_ && bitsPerHalfSide_ <= 8) ||
       (!fourFoldSymmetry_ && bitsPerHalfSide_ <= 6)) {
        box::BinaryPattern twoFoldPattern(bitsPerHalfSide_);
        box::BinaryPattern4Fold fourFoldPattern(bitsPerHalfSide_);
        uint64_t last = lastCode();
        uint64_t code = 0U;
        bool going = true;
        while(going) {
            going = code != last;
            if(fourFoldSymmetry_) {
                fourFoldPattern.pattern({code});
                twoFoldPattern = fourFoldPattern;
            } else {
                twoFoldPattern.pattern({code});
            }
            if(!twoFoldPattern.hasCornerToCorner() || inclCornerToCorner_) {
                if(symmetricalCodes_.count(code) == 0) {
                    requiredCodes_.insert(code);
                    if(fourFoldSymmetry_) {
                        // TODO: Any 4 fold symmetries
                    } else {
                        box::BinaryPattern t;
                        // Half cell X translation
                        t = twoFoldPattern;
                        t.translateX();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // Half cell Y translation
                        t = twoFoldPattern;
                        t.translateY();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // Both translations
                        t = twoFoldPattern;
                        t.translateX();
                        t.translateY();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // Rotate 90
                        t = twoFoldPattern;
                        t.rotate90();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // X translate and rotate 90
                        t = twoFoldPattern;
                        t.translateX();
                        t.rotate90();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // Y translate and rotate 90
                        t = twoFoldPattern;
                        t.translateY();
                        t.rotate90();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                        // Both translations and rotate 90
                        t = twoFoldPattern;
                        t.translateX();
                        t.translateY();
                        t.rotate90();
                        if(requiredCodes_.count(t.pattern()[0]) == 0) {
                            symmetricalCodes_.insert(t.pattern()[0]);
                        }
                    }
                }
            }
            code++;
        }
    }
}

// Clear an reinitialise the catalogue
void fdtd::Catalogue::clear() {
    items_.clear();
    setRequiredCodes();
}

// Print the set of items for debug purposes
void fdtd::Catalogue::printItems() const {
    std::cout << "Catalogue items {";
    for(auto& item : items_) {
        std::cout << item.second->code() << " ";
    }
    std::cout << "}" << std::endl;
    std::cout << "Required codes are: {";
    for(auto& c : requiredCodes_) {
        std::cout << c << " ";
    }
    std::cout << "}" << std::endl;
    std::cout << "Symmetry codes are: {";
    for(auto& c : symmetricalCodes_) {
        std::cout << c << " ";
    }
    std::cout << "}" << std::endl;
}
