/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceData.h"
#include <Xml/DomObject.h>
#include <Xml/Exception.h>

// Default constructor
fdtd::AdmittanceData::AdmittanceData() :
        cellSize_(0.0) {
}

// Copy constructor
fdtd::AdmittanceData::AdmittanceData(const fdtd::AdmittanceData& other) {
    *this = other;
}

// Clear the admittance data
void fdtd::AdmittanceData::clear() {
    admittance_.clear();
    cellSize_ = 0.0;
}

// Add an admittance point
void fdtd::AdmittanceData::addPoint(long long int frequency, std::complex<double> admittance) {
    admittance_.emplace_back(std::make_pair(frequency, admittance));
}

// Write to an XML object
void fdtd::AdmittanceData::writeXml(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("cellsize") << cellSize_;
    for(auto& a : admittance_) {
        root << xml::Open("point");
        root << xml::Obj("frequency") << a.first;
        root << xml::Obj("real") << a.second.real();
        root << xml::Obj("imaginary") << a.second.imag();
        root << xml::Close("point");
    }
    root << xml::Close();
}

// Read from an XML object
void fdtd::AdmittanceData::readXml(xml::DomObject& root) {
    admittance_.clear();
    root >> xml::Open();
    root >> xml::Obj("cellsize") >> cellSize_;
    for(auto& p : root.obj("point")) {
        auto& o = *p;
        try {
            double frequency=0.0, real=0.0, imag=0.0;
            o >> xml::Obj("frequency") >> frequency;
            o >> xml::Obj("real") >> real;
            o >> xml::Obj("imaginary") >> imag;
            admittance_.emplace_back(std::make_pair(static_cast<long long>(frequency),
                    std::complex<double>(real, imag)));
        } catch(xml::Exception&) {
        }
    }
    // Backwards compatibility
    for(auto& p : root.obj("admittanceentry")) {
        auto& o = *p;
        try {
            double frequency, real, imag;
            o >> xml::Obj("frequency") >> frequency;
            o >> xml::Obj("real") >> real;
            o >> xml::Obj("imaginary") >> imag;
            admittance_.emplace_back(std::make_pair(static_cast<long long>(frequency),
                                                    std::complex<double>(real, imag)));
        } catch(xml::Exception&) {
        }
    }
    root >> xml::Close();
}

// Use the table to interpolate the admittance for the specified frequency and cellSize
std::complex<double> fdtd::AdmittanceData::admittanceAt(long long frequency,
                                                        double cellSize) {
    std::complex<double> result;
    // Adjust the frequency to account for the cell size
    if(cellSize_ > 0.0) {
        frequency = static_cast<long long>(std::round(static_cast<double>(frequency) *
                                                          cellSize / cellSize_));
    }
    // First find the entries smaller and bigger than the desired frequency.
    auto below = admittance_.end();
    auto above = admittance_.end();
    auto pos = admittance_.begin();
    while(pos != admittance_.end()) {
        if(pos->first == frequency) {
            below = pos;
            above = pos;
        } else if(pos->first < frequency) {
            if(below->first < pos->first) {
                below = pos;
            }
        } else {
            if(above->first > pos->first) {
                above = pos;
            }
        }
        pos++;
    }
    // Is there an exact entry in the table to use?
    if(below == admittance_.end() && above == admittance_.end()) {
        // Table empty
    } else if(below == admittance_.end()) {
        // No below, use the above
        result = above->second;
    } else if(above == admittance_.end() || above == below) {
        // No above or positions same
        result = below->second;
    } else {
        // Interpolate between below and above
        std::complex<double> admittancePerHz = (above->second - below->second) /
                                               static_cast<double>(above->first -
                                                                   below->first);
        result = below->second +
                 static_cast<double>(frequency - below->first) * admittancePerHz;
    }
    return result;
}
