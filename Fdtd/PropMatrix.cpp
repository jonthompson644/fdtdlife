/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PropMatrix.h"

// Construct a propagation identity matrix
fdtd::PropMatrix::PropMatrix() {
    m11_ = 1;
    m12_ = 0;
    m21_ = 0;
    m22_ = 1;
}

// Construct a propagation matrix with a propagation coefficient
fdtd::PropMatrix::PropMatrix(std::complex<double> v) {
    m11_ = v;
    m12_ = 0;
    m21_ = 0;
    m22_ = std::conj(v);
}

// Construct a propagation matrix with matching coefficients
fdtd::PropMatrix::PropMatrix(double eta, double etaprime) {
    double factor = (eta + etaprime) / (2 * etaprime);
    double entry = (etaprime - eta) / (etaprime + eta);
    m11_ = factor;
    m12_ = factor * entry;
    m21_ = factor * entry;
    m22_ = factor;
}

// Construct a propagation matrix
fdtd::PropMatrix::PropMatrix(std::complex<double> a1, std::complex<double> a2,
        std::complex<double> b1, std::complex<double> b2) {
    m11_ = a1;
    m12_ = a2;
    m21_ = b1;
    m22_ = b2;
}

// Multiply operator
fdtd::PropMatrix fdtd::PropMatrix::operator*(const PropMatrix& other) {
    PropMatrix result;
    result.m11_ = m11_ * other.m11_ + m12_ * other.m21_;
    result.m12_ = m11_ * other.m12_ + m12_ * other.m22_;
    result.m21_ = m21_ * other.m11_ + m22_ * other.m21_;
    result.m22_ = m21_ * other.m12_ + m22_ * other.m22_;
    return result;
}
