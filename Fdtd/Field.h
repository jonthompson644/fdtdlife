/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_FIELD_H
#define FDTDLIFE_FIELD_H

#include <cstddef>
#include <Box/Vector.h>
#include "Configuration.h"

namespace source {class Sources;}
namespace domain {class Domain;}

namespace fdtd {
    class Model;

    // A base class for all the field calculation classes
    class Field {
    protected:
        // Attributes
        Model* m_;
    public:
        // Construction
        explicit Field(Model* m);
        virtual ~Field() {}
        // Methods
        virtual void initialise() {}
        virtual box::Vector<double> value(const box::Vector<size_t>& /*cell*/) {return box::Vector<double>(0);}
        virtual void print() {}
        virtual size_t memoryUse() {return 0;}
        virtual void stepBegin() {}
        virtual void step(const box::ThreadInfo* /*info*/) {}
        virtual void stepEnd() {}
        virtual bool writeData(std::ofstream& file) = 0;
        virtual bool readData(std::ifstream& file) = 0;
    protected:
        // Useful functions
        Configuration* p() const;
        domain::Domain* d();
        source::Sources& sources();
    };
}



#endif //FDTDLIFE_FIELD_H
