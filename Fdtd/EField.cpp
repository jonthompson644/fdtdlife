/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "EField.h"
#include "HField.h"
#include "Model.h"
#include <algorithm>

// Constructor
fdtd::EField::EField(Model* m) :
        Field(m) {
}

// Handle any periodic boundary conditions
void fdtd::EField::doPeriodicBoundaries(const box::ThreadInfo* info,
                                        std::vector<double>& x, std::vector<double>& y,
                                        std::vector<double>& z) {
    size_t i, j, k;
    Configuration *p = m_->p();
    Geometry* g = p->geometry();
    // Handle X axis boundary conditions
    switch(p->boundary().x()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            for(j=0; j<g->n().y(); j++) {
                for(k=info->startZ(); k<info->startZ()+info->numZ(); k++) {
                    y[g->index(-1,j,k)] = y[g->index(g->n().x()-1,j,k)];
                    z[g->index(-1,j,k)] = z[g->index(g->n().x()-1,j,k)];
                }
            }
            break;
    }
    // Handle Y axis boundary conditions
    switch(p->boundary().y()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            for(i=0; i<g->n().x(); i++) {
                for(k=info->startZ(); k<info->startZ()+info->numZ(); k++) {
                    z[g->index(i,-1,k)] = z[g->index(i,g->n().y()-1,k)];
                    x[g->index(i,-1,k)] = x[g->index(i,g->n().y()-1,k)];
                }
            }
            break;
    }
    // Handle Z axis boundary conditions
    switch(p->boundary().z()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            if((g->n().z()-1) >= info->startZ() && (g->n().z()-1) < info->startZ()+info->numZ()) {
                for(i=0; i<g->n().x(); i++) {
                    for(j=0; j<g->n().y(); j++) {
                        x[g->index(i,j,-1)] = x[g->index(i,j,g->n().z()-1)];
                        y[g->index(i,j,-1)] = y[g->index(i,j,g->n().z()-1)];
                    }
                }
            }
            break;
    }
}

// One thread only step initialisation
void fdtd::EField::stepBegin() {
    m_->sources().stepBeginE();
}

// One thread only step finalisation
void fdtd::EField::stepEnd() {
    m_->sources().stepEndE();
}

