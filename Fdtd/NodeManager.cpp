/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "NodeManager.h"
#include "LocalNode.h"
#include "StripeJob.h"
#include "SingleJob.h"
#include "IndividualJob.h"
#include "CatalogueItemJob.h"
#include "Model.h"
#include <Seq/SingleSeq.h>

// Constructor
fdtd::NodeManager::NodeManager(Model* model) :
        Notifier(model),
        model_(model),
        nextNodeId_(0),
        nextJobId_(0),
        slave_(false) {
    // Make the local node
    localNode_ = new LocalNode(this);
    addNode(localNode_);
}

// Destructor
fdtd::NodeManager::~NodeManager() {
    clear();
    for(auto node : nodes_) {
        delete node;
    }
    nodes_.clear();
}

// Add a node to the manager
void fdtd::NodeManager::addNode(Node* node) {
    nodes_.push_back(node);
    idleNodes_.push_back(node);
}

// Add a single job to be processed
std::shared_ptr<fdtd::Job> fdtd::NodeManager::addSingleJob(seq::SingleSeq* sequencer) {
    auto job = std::make_shared<SingleJob>(allocJobId(), sequencer);
    addJob(job);
    return job;
}

// Add a stripe job to be processed
std::shared_ptr<fdtd::Job> fdtd::NodeManager::addStripeJob(bool eField, int zStart, int zCount) {
    auto job = std::make_shared<StripeJob>(allocJobId(), eField, zStart, zCount);
    addJob(job);
    return job;
}

// Add an individual job to be processed
std::shared_ptr<fdtd::Job> fdtd::NodeManager::addIndividualJob(
        gs::GeneticSearch* sequencer,
        const std::shared_ptr<gs::Individual>& individual) {
    auto job = std::make_shared<IndividualJob>(allocJobId(), sequencer, individual);
    addJob(job);
    return job;
}

// Create a catalogue item job
std::shared_ptr<fdtd::Job> fdtd::NodeManager::addCatalogueItemJob(
        fdtd::CatalogueBuilder* sequencer,
        const std::shared_ptr<fdtd::CatalogueItem>& item) {
    auto job = std::make_shared<CatalogueItemJob>(allocJobId(), sequencer, item);
    addJob(job);
    return job;
}

// Add a job to be processed
void fdtd::NodeManager::addJob(const std::shared_ptr<Job>& job) {
    waitingJobs_.push_back(job);
    process();
}

// Execute the queued waitingJobs
void fdtd::NodeManager::process() {
    // Give each node a job (if one is available)
    while(!idleNodes_.empty() && !waitingJobs_.empty()) {
        auto job = waitingJobs_.front();
        waitingJobs_.pop_front();
        Node* node = idleNodes_.front();
        idleNodes_.pop_front();
        busyNodes_.push_back(node);
        node->process(job);
        runningJobs_.push_back(job);
    }
    doNotification(notifyJobListChanged);
}

// A job has been completed by a node
void fdtd::NodeManager::jobDone(Node* node) {
    doNotification(notifyJobComplete);
    // Free the job
    runningJobs_.remove(node->job());
    // Are there any more waiting jobs to hand out?
    if(waitingJobs_.empty()) {
        // No more waiting jobs
        busyNodes_.remove(node);
        idleNodes_.push_back(node);
        // Are all the nodes idle?
        if(busyNodes_.empty()) {
            // All done
            doNotification(notifyAllJobsComplete);
        }
    } else {
        // Give this node the next job
        auto job = waitingJobs_.front();
        waitingJobs_.pop_front();
        node->process(job);
        runningJobs_.push_back(job);
    }
    doNotification(notifyJobListChanged);
}

// Initialise the system for the next model run
void fdtd::NodeManager::clear() {
    waitingJobs_.clear();
    runningJobs_.clear();
    busyNodes_.clear();
    idleNodes_.clear();
    for(auto& node : nodes_) {
        idleNodes_.push_back(node);
    }
}

// Find the processing node with the given identifier
fdtd::Node* fdtd::NodeManager::findNode(int id) {
    fdtd::Node* result = nullptr;
    for(auto node : nodes_) {
        if(node->id() == id) {
            result = node;
            break;
        }
    }
    return result;
}

// The job running on the local host is complete
void fdtd::NodeManager::localJobComplete() {
    auto job = localNode_->job();
    if(job) {
        job->complete(model_);
        job->sequencer()->jobComplete(job);
        jobDone(localNode_);
    }
}

