/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "HCpml.h"
#include "ECpml.h"
#include "Model.h"
#include <Domain/Material.h>
#include <Domain/CpmlMaterial.h>
#include <Domain/ThinSheetMaterial.h>
#include <Source/Sources.h>
#include <algorithm>

// Constructor
fdtd::HCpml::HCpml(Model* m) :
        HField(m),
        CpmlField(m->p()) {
}

// Initialise the model
void fdtd::HCpml::initialise() {
    HField::initialise();
    CpmlField::initialise();
}

// Print the data
void fdtd::HCpml::print() {
    CpmlField::print("H Field");
}

// Return the amount of memory used
size_t fdtd::HCpml::memoryUse() {
    return CpmlField::memoryUse();
}

// Return a value from the model
box::Vector<double> fdtd::HCpml::value(const box::Vector<size_t>& cell) {
    return CpmlField::value(cell);
}

// Calculate the main model update
void fdtd::HCpml::stepField(const box::ThreadInfo* info) {
    size_t i, j, k;
    double b, c;
    double cahx=0, cahy=0, cahz=0;
    double cbhx=0, cbhy=0, cbhz=0;
    bool inBoundary = false;
    size_t psiIndex;
    Configuration* p = m_->p();
    fdtd::Geometry* g = p->geometry();
    domain::Domain* d = m_->d();
    CpmlField* e = dynamic_cast<ECpml*>(m_->e());
    if(e != nullptr) {
        // Only calculate the zeroth entries on periodic axes
        box::Vector<size_t> start(1, 1, std::max<size_t>(info->startZ(), 1U));
        if(p->boundary().x() == Configuration::Boundary::periodic) {
            start.x(0);
        }
        if(p->boundary().y() == Configuration::Boundary::periodic) {
            start.y(0);
        }
        if(p->boundary().z() == Configuration::Boundary::periodic) {
            start.z(std::max<size_t>(info->startZ(), 0U));
        }
        // Step the grid
        box::Vector<size_t> boundaryDepth;
        for(k = start.z(); k < info->startZ() + info->numZ(); k++) {
            // Z boundary depth if this is an absorbing boundary
            boundaryDepth.z(0);
            if(p->boundary().z() == Configuration::Boundary::absorbing) {
                if(k < p->boundarySize()) {
                    boundaryDepth.z(p->boundarySize() - k - 1);
                } else if(k >= (g->n().z() - p->boundarySize())) {
                    boundaryDepth.z(k - (g->n().z() - p->boundarySize()));
                }
            }
            for(j = start.y(); j < g->n().y(); j++) {
                // Y boundary depth if this is an absorbing boundary
                boundaryDepth.y(0);
                if(p->boundary().y() == Configuration::Boundary::absorbing) {
                    if(j < p->boundarySize()) {
                        boundaryDepth.y(p->boundarySize() - j - 1);
                    } else if(j >= (g->n().y() - p->boundarySize())) {
                        boundaryDepth.y(j - (g->n().y() - p->boundarySize()));
                    }
                }
                for(i = start.x(); i < g->n().x(); i++) {
                    // X boundary depth if this is an absorbing boundary
                    boundaryDepth.x(0);
                    if(p->boundary().x() == Configuration::Boundary::absorbing) {
                        if(i < p->boundarySize()) {
                            boundaryDepth.x(p->boundarySize() - i - 1);
                        } else if(i >= (g->n().x() - p->boundarySize())) {
                            boundaryDepth.x(i - (g->n().x() - p->boundarySize()));
                        }
                    }
                    domain::Material* mat = d->getMaterialAt(i, j, k);
                    if(mat != nullptr) {
                        box::Vector<double> dr =
                                mat->stretch(boundaryDepth) * p_->dr().value();
                        size_t idx = g->index(i, j, k);
                        double dexk = e->x_[idx] - e->x_[g->index(i, j, k - 1)];
                        double dexj = e->x_[idx] - e->x_[g->index(i, j - 1, k)];
                        double deyi = e->y_[idx] - e->y_[g->index(i - 1, j, k)];
                        double deyk = e->y_[idx] - e->y_[g->index(i, j, k - 1)];
                        double dezj = e->z_[idx] - e->z_[g->index(i, j - 1, k)];
                        double dezi = e->z_[idx] - e->z_[g->index(i - 1, j, k)];
                        mat->cah(boundaryDepth, cahx, cahy, cahz);
                        mat->cbh(boundaryDepth, cbhx, cbhy, cbhz);
                        auto thinMat = dynamic_cast<domain::ThinSheetMaterial*>(mat);
                        if(m_->p()->useThinSheetSubcells() && thinMat != nullptr) {
                            // Update for a cell containing a thin sheet
                            double dezjin =
                                    *thinMat->extraEz(i, j) - *thinMat->extraEz(i, j - 1);
                            double deziin =
                                    *thinMat->extraEz(i, j) - *thinMat->extraEz(i - 1, j);
                            x_[idx] =
                                    (-(dezj * thinMat->outRatio() +
                                       dezjin * thinMat->inRatio()) / dr.y() + deyk / dr.z()) *
                                    cahx +
                                    x_[idx] * cbhx;
                            y_[idx] =
                                    (-dexk / dr.z() + (dezi * thinMat->outRatio() +
                                                       deziin * thinMat->inRatio()) / dr.x()) *
                                    cahy +
                                    y_[idx] * cbhy;
                            z_[idx] =
                                    (-deyi / dr.x() + dexj / dr.y()) * cahz +
                                    z_[idx] * cbhz;
                        } else {
                            // Normal update
                            x_[idx] =
                                    (-dezj / dr.y() + deyk / dr.z()) * cahx +
                                    x_[idx] * cbhx;
                            y_[idx] =
                                    (-dexk / dr.z() + dezi / dr.x()) * cahy +
                                    y_[idx] * cbhy;
                            z_[idx] =
                                    (-deyi / dr.x() + dexj / dr.y()) * cahz +
                                    z_[idx] * cbhz;
                        }
                        // Now the CPML boundary modifications
                        auto boundaryMat = dynamic_cast<domain::CpmlMaterial*>(mat);
                        if(boundaryMat != nullptr) {
                            psiIndex = wxPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->bx(boundaryDepth.x());
                                c = boundaryMat->cx(boundaryDepth.x());
                                yxPsi_[psiIndex] = b * yxPsi_[psiIndex] +
                                                   c * dezi / m_->p()->dr().x().value();
                                y_[idx] += yxPsi_[psiIndex] * cahy;
                                zxPsi_[psiIndex] = b * zxPsi_[psiIndex] +
                                                   c * deyi / m_->p()->dr().x().value();
                                z_[idx] -= zxPsi_[psiIndex] * cahz;
                            }
                            psiIndex = wyPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->by(boundaryDepth.y());
                                c = boundaryMat->cy(boundaryDepth.y());
                                xyPsi_[psiIndex] = b * xyPsi_[psiIndex] +
                                                   c * dezj / m_->p()->dr().y().value();
                                x_[idx] -= xyPsi_[psiIndex] * cahx;
                                zyPsi_[psiIndex] = b * zyPsi_[psiIndex] +
                                                   c * dexj / m_->p()->dr().y().value();
                                z_[idx] += zyPsi_[psiIndex] * cahz;
                            }
                            psiIndex = wzPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->bz(boundaryDepth.z());
                                c = boundaryMat->cz(boundaryDepth.z());
                                xzPsi_[psiIndex] = b * xzPsi_[psiIndex] +
                                                   c * deyk / m_->p()->dr().z().value();
                                x_[idx] += xzPsi_[psiIndex] * cahx;
                                yzPsi_[psiIndex] = b * yzPsi_[psiIndex] +
                                                   c * dexk / m_->p()->dr().z().value();
                                y_[idx] -= yzPsi_[psiIndex] * cahy;
                            }
                        }
                    }
                }
            }
        }
    }
}

// Perform a single step on part of the model
// For shared memory multiprocessing we partition the model
// along the Z axis.
void fdtd::HCpml::step(const box::ThreadInfo* info) {
    // Step the grid
    stepField(info);
    // Now correct for any incident waves at the points on the
    // boundary between the total field and scattered field zones
    m_->sources().stepH(info, x_, y_, z_);
    // Handle any periodic boundary conditions
    doPeriodicBoundaries(info, x_, y_, z_);
    print();
}
