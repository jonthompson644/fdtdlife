/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CONFIGURATION_H
#define FDTDLIFE_CONFIGURATION_H

#include <Box/Vector.h>
#include <Box/Configurable.h>
#include <Box/ThreadInfo.h>
#include <Box/Factory.h>
#include <Box/VectorExpression.h>
#include "PlateAdmittance.h"
#include "Geometry.h"
#include <vector>
#include <iostream>
#include <memory>

namespace xml { class DomObject; }

namespace fdtd {

    class Model;

    // The main configuration of the FDTD model
    class Configuration : public box::Configurable {
    public:
        // Types
        enum Phase {
            phaseElectric = 0, phaseMagnetic, phaseDone
        };
        enum class Boundary {
            reflecting = 0, periodic, absorbing
        };

    public:
        // Construction
        explicit Configuration(Model* model);
    private:
        Configuration() = default;
        Configuration(const Configuration& other);

    protected:
        // Model variables
        box::VectorExpression<double> p1_;  // Lower left corner of the total field domain (m)
        box::VectorExpression<double> p2_;  // Upper right corner of the total field domain (m)
        box::VectorExpression<double> dr_;  // The size of spatial steps (m)
        box::Expression tSize_{};  // The time span over which to run the model (s)
        double s_{};  // The Courant stability factor
        size_t scatteredFieldZoneSize_{};  // The size, in cells, of the scattered field zone.
        box::Vector<Boundary> boundary_; // Boundaries
        size_t boundarySize_{};  // The size, in cells, of an outer boundary layer
        size_t numThreadsCfg_{};  // The number of threads we could like
        bool doPropagationMatrices_{};  // Run the propagation matrix method
        bool doFdtd_{};  // Run the FDTD method
        double animationPeriod_{};   // The sensorId animation time period
        PlateAdmittance plateAdmittance_{};  // The admittance to use for square plates/holes
        bool useThinSheetSubcells_{};  // Use the thin sheet subcell feature for certain shapes

        // Derived variables
        box::Vector<double> size_;  // The total size of the domain (m)
        int nt_{};  // The number of time steps
        double dt_{};  // The size of a time step
        int timeStep_{};  // The current time step
        Phase phase_{};  // The current step phase
        double stepProcessingTime_{};  // The time taken for processing a whole step
        double cellProcessingRate_{};  // The rate at which grid cells are being processed
        double maxCellProcessingRate_{};  // The maximum value of cellProcessingRate_
        std::vector<box::ThreadInfo> threadInfo_;  // Partioning information for the threads
        Model* model_{};
        bool usingOpenMp_{};
        size_t numThreadsUsed_{};
        std::unique_ptr<Geometry> geometry_;  // A class that handles the model's geometry
        size_t maxNumThreads_{};

    public:
        // Methods
        void initialise();
        void clear();
        double calcDt();
        bool writeData(std::ofstream& file);
        bool readData(std::ifstream& file);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        void setupThreadInfo(size_t zStart, size_t zCount);
        PlateAdmittance& plateAdmittance() { return plateAdmittance_; }
        friend xml::DomObject& operator<<(xml::DomObject& o, const Configuration& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Configuration& v) {
            v.readConfig(o);
            return o;
        }
        void evaluate();
        Geometry* geometry() { return geometry_.get(); }

        // Getters
        [[nodiscard]] const box::VectorExpression<double>& p1() const { return p1_; }
        [[nodiscard]] const box::VectorExpression<double>& p2() const { return p2_; }
        [[nodiscard]] const box::VectorExpression<double>& dr() const { return dr_; }
        [[nodiscard]] const box::Expression& tSize() const { return tSize_; }
        [[nodiscard]] box::Vector<double> size() const { return size_; }
        [[nodiscard]] double s() const { return s_; }
        [[nodiscard]] size_t scatteredFieldZoneSize() const { return scatteredFieldZoneSize_; }
        [[nodiscard]] const box::Vector<Boundary>& boundary() const { return boundary_; }
        [[nodiscard]] size_t boundarySize() const { return boundarySize_; }
        [[nodiscard]] double dt() const { return dt_; }
        [[nodiscard]] int nt() const { return nt_; }
        [[nodiscard]] int timeStep() const { return timeStep_; }
        [[nodiscard]] Phase phase() const { return phase_; }
        [[nodiscard]] double stepProcessingTime() const { return stepProcessingTime_; }
        [[nodiscard]] double cellProcessingRate() const { return cellProcessingRate_; }
        [[nodiscard]] double maxCellProcessingRate() const { return maxCellProcessingRate_; }
        [[nodiscard]] size_t numThreadsCfg() const { return numThreadsCfg_; }
        [[nodiscard]] size_t numThreadsUsed() const { return numThreadsUsed_; }
        [[nodiscard]] const box::ThreadInfo* threadInfo(int t) const { return &threadInfo_[t]; }
        [[nodiscard]] bool doPropagationMatrices() const { return doPropagationMatrices_; }
        [[nodiscard]] bool doFdtd() const { return doFdtd_; }
        [[nodiscard]] double animationPeriod() const { return animationPeriod_; }
        [[nodiscard]] bool usingOpenMp() const { return usingOpenMp_; }
        [[nodiscard]] bool useThinSheetSubcells() const { return useThinSheetSubcells_; }
        [[nodiscard]] size_t maxNumThreads() const { return maxNumThreads_; }

        // Setters
        void p1(const box::VectorExpression<double>& v) { p1_ = v; }
        void p2(const box::VectorExpression<double>& v) { p2_ = v; }
        void dr(const box::VectorExpression<double>& v) { dr_ = v; }
        void tSize(const box::Expression& v) { tSize_ = v; }
        void s(double v) { s_ = v; }
        void scatteredFieldZoneSize(size_t v) { scatteredFieldZoneSize_ = v; }
        void boundary(const box::Vector<Boundary>& v) { boundary_ = v; }
        void boundarySize(size_t v) { boundarySize_ = v; }
        void phase(Phase v) { phase_ = v; }
        void timeStep(int v) { timeStep_ = v; }
        void stepProcessingTime(double v);
        void numThreadsCfg(int v) { numThreadsCfg_ = v; }
        void doPropagationMatrices(bool v) { doPropagationMatrices_ = v; }
        void doFdtd(bool v) { doFdtd_ = v; }
        void animationPeriod(double v) { animationPeriod_ = v; }
        void useThinSheetSubcells(bool v) { useThinSheetSubcells_ = v; }
    };
}

#endif //FDTDLIFE_CONFIGURATION_H
