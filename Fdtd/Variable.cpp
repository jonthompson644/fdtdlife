#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Variable.h"

// Constructor
fdtd::Variable::Variable(int identifier, std::string name, const std::string& value) :
        identifier_(identifier),
        name_(std::move(name)),
        value_(value) {
}

// Add the current value of this variable to the context
void fdtd::Variable::addToContext(box::Expression::Context& context) {
    double v = NAN;
    try {
        v = value_.evaluate(context);
    } catch (box::Expression::Exception& e) {
        // Just swallow any exceptions that occur right now
    }
    context.add(box::Expression::Parameter(name_, v));
}

// Return a string that represents the current value or error
std::string fdtd::Variable::displayValue(box::Expression::Context& context) {
    std::stringstream result;
    try {
        double v;
        v = value_.evaluate(context);
        result << v;
    } catch (box::Expression::Exception& e) {
        result << e.what();
    }
    return result.str();
}

// Write the variable to the DOM object
void fdtd::Variable::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("value") << value_;
    root << xml::Obj("comment") << comment_;
    root << xml::Close();
}

// Read the variable from the DOM object
void fdtd::Variable::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("identifier") >> identifier_;
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("value") >> value_;
    root >> xml::Obj("comment") >> xml::Default("") >> comment_;
    root >> xml::Close();
}
