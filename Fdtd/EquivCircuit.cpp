/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "EquivCircuit.h"
#include <Box/Constants.h>
#include <Box/Utility.h>

// Constructor that creates a random individual
fdtd::EquivCircuit::EquivCircuit(box::Random& random, size_t numStages) {
    for(size_t i = 0; i < numStages; i++) {
        stages_.emplace_back(random.get(componentRange_),
                             random.get(componentRange_),
                             random.get(componentRange_));
    }
    encode();
    decode();
}

// Constructor that breeds the individual from parents
fdtd::EquivCircuit::EquivCircuit(box::Random& random,
                                 EquivCircuit* mother, EquivCircuit* father) {
    // Number of stages will be that of the father
    stages_.resize(father->stages_.size());
    // The size of the genome will be that of the father
    chromosome_.resize(father->chromosome_.size());
    chromosomeBitsUsed_ = father->chromosomeBitsUsed_;
    // For each byte in the two genomes...
    for(size_t i=0; i<chromosome_.size(); i++) {
        auto fatherMask = static_cast<uint8_t>(random.get(static_cast<size_t>(256)));
        uint8_t motherMask = ~fatherMask;
        uint8_t newData = father->chromosome_[i];
        if(i < mother->chromosome_.size()) {
            newData = static_cast<uint8_t>(newData & fatherMask) |
                      static_cast<uint8_t>(mother->chromosome_[i] & motherMask);
        }
        chromosome_[i] = newData;
    }
    decode();
}

// Encode the circuit into the chromosome
void fdtd::EquivCircuit::encode() {
    chromosome_.clear();
    chromosomeBitsUsed_ = 0U;
    for(auto& stage : stages_) {
        encodeComponent(stage.r_);
        encodeComponent(stage.l_);
        encodeComponent(stage.c_);
    }
}

// Encode a stage component into the chromosome
void fdtd::EquivCircuit::encodeComponent(size_t value) {
    size_t shift = 0;
    while(value > 1) {
        shift++;
        value >>= 1U;
    }
    for(size_t i = 0; i < componentNumBits_; i++) {
        if(chromosomeBitsUsed_ % box::Constants::bitsPerByte_ == 0) {
            chromosome_.emplace_back(0U);
            chromosomeBitsUsed_ = 0;
        }
        uint8_t bit = ((shift >> i) & 1U) << chromosomeBitsUsed_;
        chromosome_.back() = chromosome_.back() | bit;
        chromosomeBitsUsed_++;
    }
}

// Decode a circuit from the chromosome
void fdtd::EquivCircuit::decode() {
    auto pos = chromosome_.begin();
    size_t nextBitInByte = 0;
    for(auto& stage : stages_) {
        stage.r_ = decodeComponent(pos, nextBitInByte);
        stage.l_ = decodeComponent(pos, nextBitInByte);
        stage.c_ = decodeComponent(pos, nextBitInByte);
    }
}

// Decode a stage component from the chromosome
size_t fdtd::EquivCircuit::decodeComponent(std::vector<uint8_t>::iterator& pos,
                                     size_t& nextBitInByte) {
    size_t shift = 0U;
    for(size_t i = 0U; i < componentNumBits_ && pos != chromosome_.end(); i++) {
        size_t bit = *pos >> nextBitInByte;
        shift |= (bit & 1U) << i;
        nextBitInByte++;
        if(nextBitInByte % box::Constants::bitsPerByte_ == 0) {
            pos++;
            nextBitInByte = 0U;
        }
    }
    size_t result = 1U << shift;
    return result;
}

// Evaluate the unfitness of this circuit if not already done
void fdtd::EquivCircuit::evaluate(double firstFrequency, double frequencyStep,
                            const std::vector<std::complex<double>>* targetAdmittance) {
    if(!evaluated_) {
        unfitness_ = 0.0;
        double frequency = firstFrequency;
        for(auto& target : *targetAdmittance) {
            //  Calculate the admittance of the circuit at this frequency
            std::complex<double> candidate = admittance(frequency);
            unfitness_ += box::Utility::square(target.real() - candidate.real());
            unfitness_ += box::Utility::square(target.imag() - candidate.imag());
            frequency += frequencyStep;
        }
        evaluated_ = true;
    }
}

// Calculate the admittance of this circuit
std::complex<double> fdtd::EquivCircuit::admittance(double frequency) {
    std::complex<double> admittance;
    double omega = box::Constants::pi_ * 2.0 * frequency;
    for(auto& stage : stages_) {
        std::complex<double> impedance =
                1.0 / std::complex<double>(0.0, omega * stage.c_) +
                std::complex<double>(0.0, omega * stage.l_) +
                std::complex<double>(stage.r_, 0.0);
        admittance += 1.0 / impedance;
    }
    return admittance;
}

// Return the genetic distance between this circuit and the other
size_t fdtd::EquivCircuit::geneticDistanceFrom(EquivCircuit* other) {
    size_t result = 0;
    for(size_t bytePos = 0; bytePos < chromosome_.size(); bytePos++) {
        for(size_t bitPos = 0; bitPos < box::Constants::bitsPerByte_; bitPos++) {
            uint8_t mask = 1U << bitPos;
            if((chromosome_.at(bytePos) & mask) != (other->chromosome_.at(bytePos) & mask)) {
                result++;
            }
        }
    }
    return result;
}

// Return the number of bits in the chromosome
size_t fdtd::EquivCircuit::chromosomeBits() {
    return chromosome_.size() * box::Constants::bitsPerByte_;
}
