/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FINDEQUIVCIRCUIT_H
#define FDTDLIFE_FINDEQUIVCIRCUIT_H

#include <Box/Random.h>
#include <Box/Factory.h>
#include <cstddef>
#include "EquivCircuit.h"

namespace fdtd {

    class FindEquivCircuit {
    public:
        // Construction
        FindEquivCircuit() = default;

        // API
        std::shared_ptr<EquivCircuit> find(double firstFrequency, double frequencyStep,
                                           const std::vector<std::complex<double>>* admittance);

    protected:
        // Configuration
        double unfitnessThreshold_{0.0001}; // The completion threshold for the search
        size_t populationSize_{5}; // The population size
        size_t tournamentSize_{2}; // The selection tournament size
        size_t numStages_{2}; // The maximum number of stages in the solution
        size_t maxGenerations_{10000}; // The maximum number of generations to run
        // Attributes
        std::vector<std::shared_ptr<EquivCircuit>> population_;
        size_t generationNumber_{};
        box::Random random_;
        size_t geneticDistance_{};
        size_t newcomerInfluxCount_{};
        const std::vector<std::complex<double>>* admittance_{};
        double firstFrequency_{};
        double frequencyStep_{};

        // Helper functions
        void createInitialPopulation();
        void evaluatePopulation();
        void breedNextGeneration();
        EquivCircuit* elite();
    };

}

#endif //FDTDLIFE_FINDEQUIVCIRCUIT_H
