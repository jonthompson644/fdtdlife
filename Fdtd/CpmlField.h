/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CPMLFIELD_H
#define FDTDLIFE_CPMLFIELD_H

#include <Box/Vector.h>
#include <vector>

namespace fdtd {

    class Configuration;

    // A mixer class for normal representation of the fields
    class CpmlField {
    public:
        // Attributes
        std::vector<double> x_;
        std::vector<double> y_;
        std::vector<double> z_;
        std::vector<double> xyPsi_;
        std::vector<double> xzPsi_;
        std::vector<double> yzPsi_;
        std::vector<double> yxPsi_;
        std::vector<double> zxPsi_;
        std::vector<double> zyPsi_;
        Configuration* p_ {};
        size_t arraySize_ {};
        size_t arraySizeWxPsi_ {};
        size_t arraySizeWyPsi_ {};
        size_t arraySizeWzPsi_ {};
    public:
        // Construction
        explicit CpmlField(Configuration* p);
        // Methods
        void initialise();
        void print(const char* title);
        size_t memoryUse();
        box::Vector<double> value(const box::Vector<size_t>& cell);
        bool writeData(std::ofstream& file);
        bool readData(std::ifstream& file);
        size_t wxPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary);
        size_t wyPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary);
        size_t wzPsiIndex(size_t x, size_t y, size_t z, bool& inBoundary);
    };

}


#endif //FDTDLIFE_CPMLFIELD_H
