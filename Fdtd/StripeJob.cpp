/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "StripeJob.h"
#include "Model.h"
#include <iostream>

// Constructor
fdtd::StripeJob::StripeJob(int id, bool eField, int zStart, int zCount) :
        Job(id, nullptr, modeSingle),
        eField_(eField),
        zStart_(zStart),
        zCount_(zCount) {
}

// Process this job
void fdtd::StripeJob::process(fdtd::Model* model, bool localHost) {
    Job::process(model, localHost);
    model->performStepFragment(eField_, zStart_, zCount_);
}

// Return a string that indicates the purpose of the job
std::string fdtd::StripeJob::displayName() {
    std::stringstream buf;
    buf << "Stripe: " << (eField_ ? "E" : "H") << " from " << zStart_ << " for " << zCount_;
    return buf.str();
}

// The job is complete
void fdtd::StripeJob::complete(fdtd::Model* /*model*/) {

}

