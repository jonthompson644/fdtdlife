/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FindEquivCircuit.h"

// Find an equivalent circuit for the item using a genetic algorithm
std::shared_ptr<fdtd::EquivCircuit> fdtd::FindEquivCircuit::find(
        double firstFrequency, double frequencyStep,
        const std::vector<std::complex<double>>* admittance) {
    admittance_ = admittance;
    firstFrequency_ = firstFrequency;
    frequencyStep_ = frequencyStep;
    // Initialise the algorithm
    createInitialPopulation();
    evaluatePopulation();
    // Loop until we find a result
    while(generationNumber_ < maxGenerations_ && elite()->unfitness() > unfitnessThreshold_) {
        generationNumber_++;
        breedNextGeneration();
        evaluatePopulation();
    }
    return population_.front();
}

// Randomly generate an initial population for the algorithm
void fdtd::FindEquivCircuit::createInitialPopulation() {
    population_.clear();
    while(population_.size() < populationSize_) {
        population_.emplace_back(std::make_shared<EquivCircuit>(random_, numStages_));
    }
    generationNumber_ = 0;
}

// Return the elite member of the population, conventionally the first in the vector
fdtd::EquivCircuit* fdtd::FindEquivCircuit::elite() {
    return population_.front().get();
}

// Calculate the unfitness factors for the population members that have not
// yet been evaluated.
void fdtd::FindEquivCircuit::evaluatePopulation() {
    // Evaluate members
    for(auto& member : population_) {
        member->evaluate(firstFrequency_, frequencyStep_, admittance_);
    }
    // Sort into fitness order
    std::sort(population_.begin(), population_.end(),
              [](const std::shared_ptr<EquivCircuit>& a,
                 const std::shared_ptr<EquivCircuit>& b) {
                  return a->unfitness() < b->unfitness();
              });
}

// Breed the next generation of solutions, current generation is already sorted
// by unfitness.
void fdtd::FindEquivCircuit::breedNextGeneration() {
    // Start the next generation with the elite
    std::vector<std::shared_ptr<EquivCircuit>> nextGeneration;
    nextGeneration.emplace_back(population_.front());
    // Is the population too in-bred?
    geneticDistance_ = 0;
    for(auto& ind : population_) {
        geneticDistance_ = std::max(geneticDistance_, ind->geneticDistanceFrom(elite()));
    }
    size_t chromosomeSizeBits = elite()->chromosomeBits();
    bool converged = (double) geneticDistance_ <= (double) chromosomeSizeBits * 5.0 / 100.0;
    if(converged) {
        // Generate fresh incomers
        newcomerInfluxCount_++;
        while(nextGeneration.size() < populationSize_) {
            nextGeneration.emplace_back(std::make_shared<EquivCircuit>(random_, numStages_));
        }
    } else {
        // Breed children
        std::list<std::pair<std::shared_ptr<EquivCircuit>, std::shared_ptr<EquivCircuit>>>
                parentsUsed;
        while(nextGeneration.size() < populationSize_) {
            // Select the parents using the tournament method
            std::set<std::shared_ptr<EquivCircuit>> breedingPair;
            while(breedingPair.size() < 2) {
                // Select an individual using the tournament method
                std::shared_ptr<EquivCircuit> parent;
                for(size_t i = 0; i < tournamentSize_; i++) {
                    auto random = random_.get(population_.size());
                    std::shared_ptr<EquivCircuit> possible = population_[random];
                    if(parent == nullptr || possible->unfitness() < parent->unfitness()) {
                        parent = possible;
                    }
                }
                // Place into the breeding pool (make sure it is not already present)
                breedingPair.insert(parent);
            }
            // Has this pair already been used?
            std::shared_ptr<EquivCircuit> father = *breedingPair.begin();
            std::shared_ptr<EquivCircuit> mother = *breedingPair.rbegin();
            bool usedAlready = false;
            for(auto& prev : parentsUsed) {
                if((prev.first == father && prev.second == mother)
                   || (prev.first == mother && prev.second == father)) {
                    usedAlready = true;
                    break;
                }
            }
            if(!usedAlready) {
                // Record the parents
                parentsUsed.emplace_back(std::make_pair(father, mother));
                // Breed the child
                nextGeneration.emplace_back(std::make_shared<EquivCircuit>(
                        random_, mother.get(), father.get()));
            }
        }
    }
    // Replace the current generation with the new
    population_ = nextGeneration;

}
