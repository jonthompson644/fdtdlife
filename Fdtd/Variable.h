/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_VARIABLE_H
#define FDTDLIFE_VARIABLE_H

#include <Box/Expression.h>

namespace fdtd {

    // Represents a single variable
    class Variable {
    public:
        // Construction
        Variable(int identifier, std::string name, const std::string& text);
        Variable() = default;

        // API
        std::string displayValue(box::Expression::Context& context);
        void addToContext(box::Expression::Context& context);
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Variable& v) {
            v.write(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Variable& v) {
            v.read(o);
            return o;
        }

        // Getters
        const std::string& text() const {return value_.text();}
        const std::string& name() const {return name_;}
        int identifier() const {return identifier_;}
        const std::string& comment() const {return comment_;}
        double value() const {return value_.value();}

        // Setters
        void text(const std::string& v) {value_.text(v);}
        void name(const std::string& v) {name_ = v;}
        void comment(const std::string& v) {comment_ = v;}

    protected:
        // Members
        int identifier_ {};  // The unique identifier of the variable
        std::string name_;   // The name of the variable
        box::Expression value_;   // The value as an expression
        std::string comment_;  // A comment associated with the variable
    };
}


#endif //FDTDLIFE_VARIABLE_H
