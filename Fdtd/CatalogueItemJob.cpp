#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueItemJob.h"
#include "Model.h"
#include "CatalogueBuilder.h"
#include "CatalogueItem.h"
#include <Xml/DomObject.h>
#include <iostream>

// Constructor
fdtd::CatalogueItemJob::CatalogueItemJob(int id, CatalogueBuilder* sequencer,
                                         std::shared_ptr<CatalogueItem> item) :
        Job(id, sequencer, modeCatalogItem),
        item_(std::move(item)),
        sequencer_(sequencer) {
}

// Constructor for use by the job factory
fdtd::CatalogueItemJob::CatalogueItemJob(int id, seq::Sequencer* sequencer) :
        Job(id, sequencer, modeCatalogItem),
        sequencer_(dynamic_cast<CatalogueBuilder*>(sequencer)) {
}

// Process this job
void fdtd::CatalogueItemJob::process(fdtd::Model* model, bool localHost) {
    Job::process(model, localHost);
    sequencer_->loadItemIntoModel(item_);
    model->initialise();
    model->p()->phase(Configuration::phaseElectric);
}

// Return a string that indicates the purpose of the job
std::string fdtd::CatalogueItemJob::displayName() {
    std::stringstream buf;
    buf << "Catalogue Item: pattern=" << std::hex << item_->pattern();
    return buf.str();
}

// The job has completed
void fdtd::CatalogueItemJob::complete(fdtd::Model* /*model*/) {
    if(localHost_) {
        sequencer_->recordData(item_);
    }
    sequencer_->addItem(item_);
    sequencer_->doNotification(seq::Sequencer::notifyJobComplete);
}

// Write the job to the XML DOM
void fdtd::CatalogueItemJob::write(xml::DomObject& root) const {
    // Base class
    Job::write(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("catitem") << *item_;
    root << xml::Close();
}

// Read the job from the XML DOM
void fdtd::CatalogueItemJob::read(xml::DomObject& root) {
    // Base class
    Job::read(root);
    // My stuff
    root >> xml::Reopen();
    auto o = root.obj("catitem");
    if(!o.empty()) {
        // Only create a new catalogue item if we do not already have one
        if(!item_) {
            item_ = std::make_shared<CatalogueItem>();
        }
        *o.front() >> *item_;
    }
    root >> xml::Close();
}
