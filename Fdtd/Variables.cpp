/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Variables.h"

// Construction
fdtd::Variables::Variables(fdtd::Model* model) :
        model_(model) {
}

// Load the variables into the given context up to but not
// including toHere.  If toHere is null, all are loaded.
void fdtd::Variables::fillContext(box::Expression::Context& context,
                                  Variable* toHere) {
    for(auto& v : variables_) {
        if(&v != toHere) {
            v.addToContext(context);
        } else {
            break;
        }
    }
}

// Write the configuration to the XML DOM object
void fdtd::Variables::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& p : variables_) {
        root << xml::Obj("variable") << p;
    }
    root << xml::Close();
}

// Read the configuration from the XML DOM object
void fdtd::Variables::readConfig(xml::DomObject& root) {
    variables_.clear();
    root >> xml::Open();
    for(auto& o : root.obj("variable")) {
        variables_.emplace_back();
        *o >> variables_.back();
    }
    root >> xml::Close();
}

// Clear the parameters back to the pristine state
void fdtd::Variables::clear() {
    variables_.clear();
}

// Add a parameter to the list
fdtd::Variable& fdtd::Variables::add(const std::string& name, const std::string& value) {
    // Find an available identifier
    int identifier = 0;
    for(auto& v : variables_) {
        identifier = std::max(identifier, v.identifier()+1);
    }
    // Create the variable
    variables_.emplace_back(identifier, name, value);
    return variables_.back();
}

// Remove a parameter from the list
void fdtd::Variables::remove(Variable* var) {
    for(auto pos=variables_.begin(); pos!=variables_.end(); ++pos) {
        if(&(*pos) == var) {
            variables_.erase(pos);
            break;
        }
    }
}

// Find the variable from its identifier.  Returns null if not found.
fdtd::Variable* fdtd::Variables::find(int identifier) {
    Variable* result = nullptr;
    for(auto& v : variables_) {
        if(v.identifier() == identifier) {
            result = &v;
            break;
        }
    }
    return result;
}

// Order the variables in the list according to the new order specified
void fdtd::Variables::reorder(const std::list<int>& newOrder) {
    std::list<Variable> newList;
    for(auto id : newOrder) {
        auto var = find(id);
        if(var != nullptr) {
            newList.push_back(*var);
        }
    }
    variables_ = newList;
}

// Move the variable with the specified identifier to the position
void fdtd::Variables::move(int varId, int position) {
    // Get the positions in the list of the drop location and the drop variable
    auto toPos = std::next(variables_.begin(), position);
    auto fromPos = std::find_if(
            variables_.begin(),variables_.end(),
            [varId](const fdtd::Variable& var) {return var.identifier() == varId;});
    // If we found the variable...
    if(fromPos != variables_.end()) {
        variables_.splice(toPos, variables_, fromPos);
    }
}
