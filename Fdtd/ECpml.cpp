/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ECpml.h"
#include "Model.h"
#include <Domain/Material.h>
#include "HCpml.h"
#include <Domain/CpmlMaterial.h>
#include <Domain/ThinSheetMaterial.h>
#include <algorithm>

// Constructor
fdtd::ECpml::ECpml(Model* m) :
        EField(m),
        CpmlField(m->p()) {
}

// Initialise the model
void fdtd::ECpml::initialise() {
    EField::initialise();
    CpmlField::initialise();
}

// Print the data
void fdtd::ECpml::print() {
    CpmlField::print("E Field");
}

// Return the amount of memory used
size_t fdtd::ECpml::memoryUse() {
    return CpmlField::memoryUse();
}

// Return a value from the model
box::Vector<double> fdtd::ECpml::value(const box::Vector<size_t>& cell) {
    return CpmlField::value(cell);
}

// Calculate the main model update
void fdtd::ECpml::stepField(const box::ThreadInfo* info) {
    size_t i, j, k;
    double b, c;
    double caex=0, caey=0, caez=0;
    double cbex=0, cbey=0, cbez=0;
    bool inBoundary = false;
    size_t psiIndex;
    Configuration* p = m_->p();
    fdtd::Geometry* g = p->geometry();
    domain::Domain* d = m_->d();
    CpmlField* h = dynamic_cast<HCpml*>(m_->h());
    if(h != nullptr) {
        // Only calculate the nth entries on periodic axes
        box::Vector<size_t> end(g->n().x() - 1, g->n().y() - 1,
                                std::min(g->n().z() - 1, info->startZ() + info->numZ()));
        if(p->boundary().x() == Configuration::Boundary::periodic) {
            end.x(end.x() + 1);
        }
        if(p->boundary().y() == Configuration::Boundary::periodic) {
            end.y(end.y() + 1);
        }
        if(p->boundary().z() == Configuration::Boundary::periodic) {
            end.z(std::min(g->n().z(), info->startZ() + info->numZ()));
        }
        // Step the grid
        box::Vector<size_t> boundaryDepth;
        for(k = info->startZ(); k < end.z(); k++) {
            // Z boundary depth if this is an absorbing boundary
            boundaryDepth.z(0);
            if(p->boundary().z() == Configuration::Boundary::absorbing) {
                if(k < p->boundarySize()) {
                    boundaryDepth.z(p->boundarySize() - k - 1);
                } else if(k >= (g->n().z() - p->boundarySize())) {
                    boundaryDepth.z(k - (g->n().z() - p->boundarySize()));
                }
            }
            for(j = 0; j < end.y(); j++) {
                // Y boundary depth if this is an absorbing boundary
                boundaryDepth.y(0);
                if(p->boundary().y() == Configuration::Boundary::absorbing) {
                    if(j < p->boundarySize()) {
                        boundaryDepth.y(p->boundarySize() - j - 1);
                    } else if(j >= (g->n().y() - p->boundarySize())) {
                        boundaryDepth.y(j - (g->n().y() - p->boundarySize()));
                    }
                }
                for(i = 0; i < end.x(); i++) {
                    // X boundary depth if this is an absorbing boundary
                    boundaryDepth.x(0);
                    if(p->boundary().x() == Configuration::Boundary::absorbing) {
                        if(i < p->boundarySize()) {
                            boundaryDepth.x(p->boundarySize() - i - 1);
                        } else if(i >= (g->n().x() - p->boundarySize())) {
                            boundaryDepth.x(i - (g->n().x() - p->boundarySize()));
                        }
                    }
                    domain::Material* mat = d->getMaterialAt(i, j, k);
                    if(mat != nullptr) {
                        box::Vector<double> dr = mat->stretch(boundaryDepth) *
                                                 p_->dr().value(); // Stretched coordinates
                        size_t idx = g->index(i, j, k);
                        double dhxj = h->x_[g->index(i, j + 1, k)] - h->x_[idx];
                        double dhxk = h->x_[g->index(i, j, k + 1)] - h->x_[idx];
                        double dhyi = h->y_[g->index(i + 1, j, k)] - h->y_[idx];
                        double dhyk = h->y_[g->index(i, j, k + 1)] - h->y_[idx];
                        double dhzj = h->z_[g->index(i, j + 1, k)] - h->z_[idx];
                        double dhzi = h->z_[g->index(i + 1, j, k)] - h->z_[idx];
                        mat->cae(boundaryDepth, caex, caey, caez);
                        mat->cbe(boundaryDepth, cbex, cbey, cbez);
                        auto thinMat = dynamic_cast<domain::ThinSheetMaterial*>(mat);
                        if(m_->p()->useThinSheetSubcells() && thinMat != nullptr) {
                            // Update for a cell containing a thin sheet
                            x_[idx] =
                                    (dhzj / dr.y() - dhyk / dr.z()) *
                                    thinMat->caeav() +
                                    x_[idx] * thinMat->cbeav();
                            y_[idx] =
                                    (dhxk / dr.z() - dhzi / dr.x()) *
                                    thinMat->caeav() +
                                    y_[idx] * thinMat->cbeav();
                            z_[idx] =
                                    (dhyi / dr.x() - dhxj / dr.y()) * caez +
                                    z_[idx] * cbez;
                            *thinMat->extraEz(i, j) =
                                    (dhyi / dr.x() - dhxj / dr.y()) *
                                    thinMat->caein() +
                                    *thinMat->extraEz(i, j) * thinMat->cbein();
                        } else {
                            // Normal update
                            x_[idx] =
                                    (dhzj / dr.y() - dhyk / dr.z()) * caex +
                                    x_[idx] * cbex;
                            y_[idx] =
                                    (dhxk / dr.z() - dhzi / dr.x()) * caey +
                                    y_[idx] * cbey;
                            z_[idx] =
                                    (dhyi / dr.x() - dhxj / dr.y()) * caez +
                                    z_[idx] * cbez;
                        }
                        // Now the CPML boundary modifications (note uses
                        // non-stretched coordinate values)
                        auto boundaryMat = dynamic_cast<domain::CpmlMaterial*>(mat);
                        if(boundaryMat != nullptr) {
                            psiIndex = wxPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->bx(boundaryDepth.x());
                                c = boundaryMat->cx(boundaryDepth.x());
                                yxPsi_[psiIndex] = b * yxPsi_[psiIndex] +
                                                   c * dhzi / m_->p()->dr().x().value();
                                y_[idx] -= yxPsi_[psiIndex] * caey;
                                zxPsi_[psiIndex] = b * zxPsi_[psiIndex] +
                                                   c * dhyi / m_->p()->dr().x().value();
                                z_[idx] += zxPsi_[psiIndex] * caez;
                            }
                            psiIndex = wyPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->by(boundaryDepth.y());
                                c = boundaryMat->cy(boundaryDepth.y());
                                xyPsi_[psiIndex] = b * xyPsi_[psiIndex] +
                                                   c * dhzj / m_->p()->dr().y().value();
                                x_[idx] += xyPsi_[psiIndex] * caex;
                                zyPsi_[psiIndex] = b * zyPsi_[psiIndex] +
                                                   c * dhxj / m_->p()->dr().y().value();
                                z_[idx] -= zyPsi_[psiIndex] * caez;
                            }
                            psiIndex = wzPsiIndex(i, j, k, inBoundary);
                            if(inBoundary) {
                                b = boundaryMat->bz(boundaryDepth.z());
                                c = boundaryMat->cz(boundaryDepth.z());
                                xzPsi_[psiIndex] = b * xzPsi_[psiIndex] +
                                                   c * dhyk / m_->p()->dr().z().value();
                                x_[idx] -= xzPsi_[psiIndex] * caex;
                                yzPsi_[psiIndex] = b * yzPsi_[psiIndex] +
                                                   c * dhxk / m_->p()->dr().z().value();
                                y_[idx] += yzPsi_[psiIndex] * caey;
                            }
                        }
                    }
                }
            }
        }
    }
}

// Perform a single step
// For shared memory multiprocessing we partition the model
// along the Z axis.
void fdtd::ECpml::step(const box::ThreadInfo* info) {
    // Step the grid
    stepField(info);
    // Now correct for any incident waves at the points on the
    // boundary between the total field and scattered field zones
    m_->sources().stepE(info, x_, y_, z_);
    // Handle any periodic boundary conditions
    doPeriodicBoundaries(info, x_, y_, z_);
    print();
}



