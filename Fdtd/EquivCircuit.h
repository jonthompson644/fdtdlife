/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_EQUIVCIRCUIT_H
#define FDTDLIFE_EQUIVCIRCUIT_H

#include <Box/Random.h>
#include <complex>

namespace fdtd {

    class EquivCircuit {
    public:
        // Construction
        EquivCircuit(box::Random& random, size_t numStages);
        EquivCircuit(box::Random& random, EquivCircuit* mother, EquivCircuit* father);

        // Types
        class Stage {
        public:
            Stage(size_t r, size_t l, size_t c) :
                    r_(r),
                    l_(l),
                    c_(c) {}
            Stage() = default;
            size_t r_{};  // Resistance in milliohms, 0..4294967.296 ohms
            size_t l_{};  // Inductance in pH, 0..4.294967296H
            size_t c_{};  // Capacitance in pF, 0..4.294967296F
        };

        // Getters
        [[nodiscard]] double unfitness() const { return unfitness_; }
        [[nodiscard]] const std::vector<uint8_t>& chromosome() const { return chromosome_; }
        [[nodiscard]] size_t chromosomeBitsUsed() const { return chromosomeBitsUsed_; }

        // API
        void evaluate(double firstFrequency, double frequencyStep,
                      const std::vector<std::complex<double>>* targetAdmittance);
        size_t geneticDistanceFrom(EquivCircuit* other);
        size_t chromosomeBits();
        std::vector<Stage>::iterator begin() { return stages_.begin(); }
        std::vector<Stage>::iterator end() { return stages_.end(); }
        Stage& front() { return stages_.front(); }
        Stage& back() { return stages_.back(); }
        void encode();
        void decode();

    protected:
        // Constants
        static const size_t componentRange_ = 4294967296U;  // Range of values of components
        static const size_t componentBitMask_ = 63U;  // Bit mask for a stage component
        static const size_t componentNumBits_ = 6U;  // Number of bits for a stage component
        // Members
        std::vector<Stage> stages_;
        std::vector<uint8_t> chromosome_;
        size_t chromosomeBitsUsed_{};  // Num bits used in the last byte of the chromosome
        double unfitness_{};
        bool evaluated_{false};

        // Helpers
        void encodeComponent(size_t value);
        size_t decodeComponent(std::vector<uint8_t>::iterator& pos, size_t& nextBitInByte);
        std::complex<double> admittance(double frequency);
    };

}

#endif //FDTDLIFE_EQUIVCIRCUIT_H
