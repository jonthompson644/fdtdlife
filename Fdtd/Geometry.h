/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GEOMETRY_H
#define FDTDLIFE_GEOMETRY_H

#include <Box/Vector.h>

namespace fdtd {
    class Configuration;

    // Base class for the FDTD grid geometries
    class Geometry {
    public:
        // Construction
        explicit Geometry() = default;
        virtual ~Geometry() = default;
        virtual void construct(int /*mode*/, int /*id*/, Configuration* p) {
            p_ = p;
        }

        // API
        virtual void initialise();
        inline size_t index(size_t x, size_t y, size_t z) {
            // +1 for the periodic boundary allowance
            size_t idx = ((z+1)*nFull_.y() + (y+1))*nFull_.x() + (x+1);
            if(idx >= arraySize_) {
                idx = 0;
            }
            return idx;
        }
        [[nodiscard]] virtual box::Vector<size_t> cellFloor(
                const box::Vector<double>& /*p*/) const {
            return {0U,0U,0U};
        }
        [[nodiscard]] virtual box::Vector<size_t> cellCeil(
                const box::Vector<double>& /*p*/) const {
            return {0U,0U,0U};
        }
        [[nodiscard]] virtual box::Vector<size_t> cellRound(
                const box::Vector<double>& /*p*/) const {
            return {0U,0U,0U};
        }
        [[nodiscard]] virtual box::Vector<double> cellCenter(
                const box::Vector<size_t>& /*p*/) const {
            return {0.0,0.0,0.0};
        }
        [[nodiscard]] virtual box::Vector<size_t> tfStart() const {
            return edge_;
        }
        [[nodiscard]] virtual box::Vector<size_t> tfEnd() const {
            return n_ - edge_ - 1;
        }

        // Getters
        [[nodiscard]] size_t arraySize() const {return arraySize_;}
        [[nodiscard]] const box::Vector<size_t>& n() const {return n_;}
        [[nodiscard]] const box::Vector<size_t>& nFull() const {return nFull_;}

    protected:
        // Members
        Configuration* p_ {};
        size_t arraySize_ {};
        box::Vector<size_t> n_;  // The number of steps in the domain
        box::Vector<size_t> nFull_;  // The number of steps incl periodic boundary allowance
        box::Vector<size_t> edge_;  // The width of the edge outside the total field zone
    };
}


#endif //FDTDLIFE_GEOMETRY_H
