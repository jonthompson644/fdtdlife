/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueItemWrapper.h"
#include "CatalogueItem.h"
#include "Catalogue.h"
#include <Xml/DomObject.h>
#include <Box/Constants.h>

// Constructor using an existing item
fdtd::CatalogueItemWrapper::CatalogueItemWrapper(const std::shared_ptr<fdtd::CatalogueItem>& item,
                                                 const Catalogue& catalogue) :
        item_(item),
        bitsPerHalfSide_(catalogue.bitsPerHalfSide()),
        fourFoldSymmetry_(catalogue.fourFoldSymmetry()),
        unitCell_(catalogue.unitCell()),
        startFrequency_(catalogue.startFrequency()),
        frequencyStep_(catalogue.frequencyStep()) {
}

// Constructor that makes a new item
fdtd::CatalogueItemWrapper::CatalogueItemWrapper() :
        bitsPerHalfSide_(10),
        fourFoldSymmetry_(true),
        unitCell_(0.0005),
        startFrequency_(1 * box::Constants::giga_),
        frequencyStep_(1 * box::Constants::giga_) {
    item_ = std::make_shared<CatalogueItem>();
}

// Read the object from XML
void fdtd::CatalogueItemWrapper::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> *item_;
    root >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
    root >> xml::Obj("fourfoldsymmetry") >> fourFoldSymmetry_;
    root >> xml::Obj("unitcell") >> unitCell_;
    root >> xml::Obj("startfrequency") >> startFrequency_;
    root >> xml::Obj("frequencystep") >> frequencyStep_;
    root >> xml::Close();
}

// Write the object to XML
void fdtd::CatalogueItemWrapper::write(xml::DomObject& root) const {
    root << xml::Open();
    root << *item_;
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    root << xml::Obj("fourfoldsymmetry") << fourFoldSymmetry_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("startfrequency") << startFrequency_;
    root << xml::Obj("frequencystep") << frequencyStep_;
    root << xml::Close();
}

// Return an admittance table for the item
fdtd::AdmittanceData fdtd::CatalogueItemWrapper::admittance() const {
    return item_->admittance(unitCell_, startFrequency_, frequencyStep_);
}

