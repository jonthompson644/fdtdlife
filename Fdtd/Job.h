/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_JOB_H
#define FDTDLIFE_JOB_H

#include <string>
#include <Xml/DomObject.h>

namespace seq {class Sequencer;}

namespace fdtd {

    class Model;

    // A base class that represents a job to be executed by the
    // processing node pool
    class Job {
    public:
        enum Mode {modeSingle, modeIndividual, modeCatalogItem, modeCharacterizer, modeLensDesigner};
        friend xml::DomObject& operator>>(xml::DomObject& o, Mode& v) {o >> (int&)v; return o;}
        // Construction
        Job(int id, seq::Sequencer* sequencer, Mode mode);
        virtual ~Job() {}

        // Getters
        int id() const {return id_;}
        int nodeId() const {return nodeId_;}
        seq::Sequencer* sequencer() const {return sequencer_;}
        Mode mode() const {return mode_;}

        // Setters
        void nodeId(int nodeId) {nodeId_ = nodeId;}

        // API
        virtual void process(Model* /*model*/, bool localHost) {localHost_ = localHost;}
        virtual std::string displayName() = 0;
        virtual void complete(Model* model) = 0;
        virtual void write(xml::DomObject& root) const;
        virtual void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Job& v) {v.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Job& v) {v.read(o); return o;}
        static Job* factory(xml::DomObject& root, Model* model);

    protected:
        // Constants
        static const int invalidNodeId_ = -1;

        // Variables
        Mode mode_;
        int id_;
        int nodeId_;
        seq::Sequencer* sequencer_;
        bool localHost_;
    };

}


#endif //FDTDLIFE_JOB_H
