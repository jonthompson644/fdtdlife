/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PROPMATRIX_H
#define FDTDLIFE_PROPMATRIX_H

#include <complex>

namespace fdtd {

    // A class that holds the propagation matrix
    class PropMatrix {
    public:
        PropMatrix();   // Initialise to an identity matrix
        explicit PropMatrix(std::complex<double> v);  // Initialise with a propagation coefficent
        PropMatrix(double eta, double etaprime);  // Initialise with matching coefficients
        PropMatrix(std::complex<double> a1, std::complex<double> a2,
                   std::complex<double> b1, std::complex<double> b2);
        PropMatrix operator*(const PropMatrix& other_);
        [[nodiscard]] std::complex<double> m11() const {return m11_;}
        [[nodiscard]] std::complex<double> m21() const {return m21_;}
    protected:
        std::complex<double> m11_;
        std::complex<double> m12_;
        std::complex<double> m21_;
        std::complex<double> m22_;
    };

}


#endif //FDTDLIFE_PROPMATRIX_H
