/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCECATALOGUE_H
#define FDTDLIFE_ADMITTANCECATALOGUE_H

#include <Xml/DomObject.h>
#include <string>
#include <vector>
#include <memory>

namespace fdtd {

    class Model;
    class CatalogueItem;

    // A catalogue of layer patterns and their admittances
    // for use with the appropriate gene
class AdmittanceCatalogue {
    protected:
        Model* m_;
        std::vector<std::shared_ptr<CatalogueItem>> items_;
        int bitsPerHalfSide_;
        bool fourFoldSymmetry_;
        double unitCell_;
        double startFrequency_;
        double frequencyStep_;
        std::string fileName_;
        bool relativeFileName_;
    public:
        // Construction
        explicit AdmittanceCatalogue(Model* m);
        // API
        std::shared_ptr<CatalogueItem> get(uint64_t code);
        void readCatalogue();
        size_t catalogueSize() const {return items_.size();}
        std::string catalogueFilePath();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const AdmittanceCatalogue& v) {v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, AdmittanceCatalogue& v) {v.readConfig(o); return o;}
        // Getters
        const std::vector<std::shared_ptr<CatalogueItem>>& items() const {return items_;}
        double startFrequency() const {return startFrequency_;}
        double frequencyStep() const {return frequencyStep_;}
        int bitsPerHalfSide() const {return bitsPerHalfSide_;}
        bool fourFoldSymmetry() const {return fourFoldSymmetry_;}
        double unitCell() const {return unitCell_;}
        std::string fileName() const {return fileName_;}
        // Setters
        void fileName(const std::string& v, bool r) {fileName_ = v; relativeFileName_=r; }
    };

}


#endif //FDTDLIFE_ADMITTANCECATALOGUE_H
