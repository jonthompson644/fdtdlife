/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <iostream>
#include <memory>
#include "Model.h"
#include <Source/Sources.h>
#include <Domain/Material.h>
#include <Domain/UnitCellShape.h>
#include <Source/PlaneWaveSource.h>
#include <Box/BinaryFileHeader.h>
#include "NodeManager.h"
#include <Sensor/ArraySensor.h>
#include <Xml/DomDocument.h>
#include <Xml/DomObject.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include "ECpml.h"
#include "HCpml.h"
#include <Seq/SingleSeq.h>

#if defined(USE_OPENMP)

#include <omp.h>

#else
#define omp_get_thread_num() 0
#endif

#if defined(_WIN32)
#define CLOCK_REALTIME 0
int clock_gettime(int, struct timespec* tv) {
    return timespec_get(tv, TIME_UTC);
}
#endif

// Constructor
// p - The model parameters
fdtd::Model::Model() :
        Notifier(this),
        variables_(this),
        p_(this),
        d_(this),
        e_(new ECpml(this)),
        h_(new HCpml(this)),
        sources_(this),
        sensors_(this),
        analysers_(this),
        sequencers_(this),
        designers_(this),
        processingMgr_(new NodeManager(this)),
        propMatrix_(this),
        admittanceCat_(this),
        stepStartTime_({0, 0}) {
}

// Destructor
fdtd::Model::~Model() {
    Model::clear();
    // Destroy components
    delete e_;
    delete h_;
    delete processingMgr_;
}

// Clear the model configuration
void fdtd::Model::clear() {
    d_.clear();
    sources_.clear();
    sensors_.clear();
    analysers_.clear();
    sequencers_.clear();
    variables_.clear();
    designers_.clear();
}

// Initialise the model
void fdtd::Model::initialise() {
    // Create the right kind of model
    delete e_;
    delete h_;
    createFields();
    // Now initialise everything
    p_.initialise();
    d_.initialise();
    sources_.initialise();
    e_->initialise();
    h_->initialise();
    sensors_.initialise();
    analysers_.initialise();
    sensors_.initialCollect();
    // Perform the propagation matrix method if required
    if(p()->doPropagationMatrices()) {
        propMatrix_.calculate();
    }
}

// Create the field objects that do most of the work
void fdtd::Model::createFields() {
    e_ = new ECpml(this);
    h_ = new HCpml(this);
}

// Move the model one time step
void fdtd::Model::step() {
    switch(p_.phase()) {
        case Configuration::phaseElectric:
            if(p()->doFdtd()) {
                // Perform the FDTD electric field step
                doStepE();
                struct timespec now = {0, 0};
                clock_gettime(CLOCK_REALTIME, &now);
                double nowSeconds = now.tv_sec +
                                    static_cast<double>(now.tv_nsec) / 1000000000.0;
                double thenSeconds = stepStartTime_.tv_sec
                                     + static_cast<double>(stepStartTime_.tv_nsec) /
                                       1000000000.0;
                p_.stepProcessingTime(nowSeconds - thenSeconds);
            } else {
                // The propagation matrix model run is complete
                doNotification(notifySteppedE);
                doNotification(notifySteppedH);
                doNotification(notifyStepComplete);
                // Run complete
                p_.phase(Configuration::phaseDone);
                processingMgr_->localJobComplete();
                doNotification(notifyRunComplete);
            }
            break;
        case Configuration::phaseMagnetic:
            clock_gettime(CLOCK_REALTIME, &stepStartTime_);
            doStepH();
            break;
        case Configuration::phaseDone:
            break;
    }
}

// The processing is complete
void fdtd::Model::processingComplete() {
    switch(p_.phase()) {
        case Configuration::phaseElectric:
            doNotification(notifySteppedE);
            p_.phase(Configuration::phaseMagnetic);
            break;
        case Configuration::phaseMagnetic:
            sensors_.collect();
            //sensors_.calculate();
            doNotification(notifySteppedH);
            p_.timeStep(p_.timeStep() + 1);
            sequencers_.stepComplete();
            analysers_.stepComplete();
            if(p_.timeStep() < p_.nt()) {
                p_.phase(Configuration::phaseElectric);
            } else {
                // Run complete
                sensors_.calculate();
                analysers_.calculate();
                sensors_.finalCalculate();
                std::cout << "processingComplete" << std::endl;
                p_.phase(Configuration::phaseDone);
                processingMgr_->localJobComplete();
                doNotification(notifyRunComplete);
            }
            break;
        case Configuration::phaseDone:
            break;
    }
    doNotification(notifyStepComplete);
}

// Perform the H field step
void fdtd::Model::doStepH() {
    // This function architecture is set up to make
    // conversion to an MPI job easier
    performStepFragment(false, 0, p()->geometry()->n().z());
    processingComplete();
}

// Perform the E field step
void fdtd::Model::doStepE() {
    // This function architecture is set up to make
    // conversion to an MPI job easier
    performStepFragment(true, 0, p()->geometry()->n().z());
    processingComplete();
}

// Perform the step calculation over the specified portion of the domain
void fdtd::Model::performStepFragment(bool eField, size_t zStart, size_t zCount) {

    // Setup the thread information for this fragment
    p()->setupThreadInfo(zStart, zCount);

    // Do the one thread only initial step processing
    if(eField) {
        e()->stepBegin();
    } else {
        h()->stepBegin();
    }

    // Perform the step processing
#pragma omp parallel default(shared)
    {
        if(eField) {
            e()->step(p_.threadInfo(omp_get_thread_num()));
        } else {
            h()->step(p_.threadInfo(omp_get_thread_num()));
        }
    }

    // Do the one thread only final step processing
    if(eField) {
        e()->stepEnd();
    } else {
        h()->stepEnd();
    }
}


// Return the amount of memory used
size_t fdtd::Model::memoryUse() {
    return e()->memoryUse() + h()->memoryUse() + sources_.memoryUse() + d_.memoryUse();
}

// Write the entire state of the model as binary data
// to the given file
bool fdtd::Model::writeData(const std::string& fileName) {
    bool result = false;
    // Open the file
    std::ofstream file;
    file.open(fileName, std::ios::out | std::ios::binary | std::ios::trunc);
    if(file.is_open()) {
        result = true;
        // Write the overall identity and version
        box::BinaryFileHeader header("FDTDLIFE", 1, 0);
        file.write(header.data(), static_cast<std::streamsize>(header.size()));
        // The model progress state
        result = result && p_.writeData(file);
        // Now write the main grid data
        result = result && e_->writeData(file);
        result = result && h_->writeData(file);
        // The auxiliary grid data
        result = result && sources_.writeData(file);
        // The sensorId data
        result = result && sensors_.writeData(file);
        // Clean up
        file.close();
    }
    return result;
}

// Read the entire state of the model from the given file
bool fdtd::Model::readData(const std::string& fileName) {
    bool result = false;
    // Open the file
    std::ifstream file;
    file.open(fileName, std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Read the overall identity and version
        box::BinaryFileHeader header{};
        file.read(header.data(), static_cast<std::streamsize>(header.size()));
        if(header.is("FDTDLIFE")) {
            // Initialise the model first
            initialise();
            // Now read the state
            result = true;
            switch(header.version()) {
                case 1:
                    result = result && p_.readData(file);
                    result = result && e_->readData(file);
                    result = result && h_->readData(file);
                    result = result && sources_.readData(file);
                    result = result && sensors_.readData(file);
                    break;
                default:
                    break;
            }
        }
        // Clean up
        file.close();
    }
    return result;
}

// Write out the model configuration as an XML file
bool fdtd::Model::writeConfigFile(const std::string& pathName, const std::string& fileName) {
    bool result = false;
    // Record file name for later
    pathName_ = pathName;
    fileName_ = fileName;
    try {
        // Make the DOM
        auto* root = new xml::DomObject("fdtd");
        xml::DomDocument dom(root);
        writeConfig(*root);
        // Write to the file
        xml::Writer writer;
        writer.writeFile(&dom, pathName_ + fileName_ + ".fdtd");
        result = true;
    } catch(xml::Exception& e) {
        std::cout << "Failed to write config XML file: " << e.what() << std::endl;
    }
    return result;
}

// Write out the model configuration as an XML file
void fdtd::Model::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("variables") << variables_;
    root << xml::Obj("parameters") << p_;
    root << sources_;
    root << d_;
    root << sensors_;
    root << analysers_;
    root << sequencers_;
    root << designers_;
    root << xml::Obj("admittancecat") << admittanceCat_;
    root << xml::Close();
}

// Read in the model configuration from an XML file
// The filename must not have the extension
// Returns true if the file was successfully read
bool fdtd::Model::readConfigFile(const std::string& pathName, const std::string& fileName) {
    bool result = false;
    // Open the file
    pathName_ = pathName;
    fileName_ = fileName;
    // Parse the file into a DOM
    xml::Reader reader;
    xml::DomDocument dom("fdtd");
    try {
        reader.readFile(&dom, pathName_ + fileName_ + ".fdtd");
        // Now process the DOM
        readConfig(*dom.getObject());
        // Read a catalogue if present
        admittanceCat_.readCatalogue();
        result = true;
    } catch(xml::Exception& e) {
        std::cout << "Failed to read config XML file: " << e.what() << std::endl;
    }
    return result;
}

void fdtd::Model::readConfig(xml::DomObject& root) {
    clear();
    root >> xml::Open();
    if(!root.obj("variables").empty()) {
        root >> xml::Obj("variables") >> variables_;
    }
    root >> xml::Obj("parameters") >> p_;
    root >> sources_;
    root >> d_;
    root >> sensors_;
    root >> analysers_;
    root >> sequencers_;
    if(!root.obj("admittancecat").empty()) {
        root >> xml::Obj("admittancecat") >> admittanceCat_;
    }
    root >> designers_;
    // Backwards compatible, read an old genetic search object as a sequencer
    auto p = root.obj("geneticsearch");
    if(!p.empty()) {
        bool enabled;
        *(p.front()) >> xml::Obj("enabled") >> xml::Default(false) >> enabled;
        if(enabled) {
            auto s = sequencers_.makeSequencer(seq::Sequencers::modeGeneticSearch);
            *(p.front()) >> *s;
        }
    }
    // Backwards compatible, if no sequencer, create a single
    if(sequencers_.empty()) {
        sequencers_.makeSequencer(seq::Sequencers::modeSingle);
    }
    root >> xml::Close();
}

// Start running the model.  Returns true if the model was able to start.
bool fdtd::Model::start() {
    // Clear the job manager
    processingMgr_->clear();
    // Run all the sequencers
    return sequencers_.run();
}

// Stop running the model.
void fdtd::Model::stop() {
    sequencers_.stop();
}

// Pause the model.
void fdtd::Model::pause() {
    sequencers_.pause();
}

// Clear the sequence generated objects
void fdtd::Model::clearSeqGenerated() {
    d_.clearSeqGenerated();
    sensors_.clearSeqGenerated();
    sources_.clearSeqGenerated();
}

// Export the model to the given export writer
void fdtd::Model::writeExport(box::ExportWriter& writer,
                              const std::set<int>& selectedMaterials) {
    // The domain boundary
    if(selectedMaterials.count(domain::Domain::defaultMaterial) > 0) {
        writer.writeCuboid(p_.p1().value() / box::Constants::milli_,
                           p_.p2().value() / box::Constants::milli_,
                           "Domain", "Background");
    }
    // The shapes
    for(auto& shape : d_.shapes()) {
        shape->write(writer, selectedMaterials);
    }
}
