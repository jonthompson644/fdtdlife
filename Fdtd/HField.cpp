/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "HField.h"
#include "Model.h"
#include "EField.h"
#include <algorithm>

// Constructor
// p - Model parameters
fdtd::HField::HField(Model* m) :
        Field(m) {
}

// Handle any periodic boundary conditions
void fdtd::HField::doPeriodicBoundaries(const box::ThreadInfo* info,
                                        std::vector<double>& x, std::vector<double>& y,
                                        std::vector<double>& z) {
    size_t i, j, k;
    Configuration *p = m_->p();
    fdtd::Geometry* g = p->geometry();
    // Handle X axis boundary conditions
    switch(p->boundary().x()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            for(j=0; j<g->n().y(); j++) {
                for(k=info->startZ(); k<info->startZ()+info->numZ(); k++) {
                    y[g->index(g->n().x(),j,k)] = y[g->index(0,j,k)];
                    z[g->index(g->n().x(),j,k)] = z[g->index(0,j,k)];
                }
            }
            break;
    }
    // Handle Y axis boundary conditions
    switch(p->boundary().y()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            for(i=0; i<g->n().x(); i++) {
                for(k=info->startZ(); k<info->startZ()+info->numZ(); k++) {
                    z[g->index(i,g->n().y(),k)] = z[g->index(i,0,k)];
                    x[g->index(i,g->n().y(),k)] = x[g->index(i,0,k)];
                }
            }
            break;
    }
    // Handle Z axis boundary conditions
    switch(p->boundary().z()) {
        case Configuration::Boundary::reflecting:
        case Configuration::Boundary::absorbing:
            break;
        case Configuration::Boundary::periodic:
            if(info->startZ() == 0) {
                for(i = 0; i < g->n().x(); i++) {
                    for(j = 0; j < g->n().y(); j++) {
                        x[g->index(i, j, g->n().z())] = x[g->index(i, j, 0)];
                        y[g->index(i, j, g->n().z())] = y[g->index(i, j, 0)];
                    }
                }
            }
            break;
    }
}

// One thread only step initialisation
void fdtd::HField::stepBegin() {
    m_->sources().stepBeginH();
}

// One thread only step finalisation
void fdtd::HField::stepEnd() {
    m_->sources().stepEndH();
}

