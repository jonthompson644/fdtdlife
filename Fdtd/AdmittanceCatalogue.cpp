/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include <fstream>
#include <iostream>
#include <memory>
#include "AdmittanceCatalogue.h"
#include "CatalogueItem.h"
#include "Model.h"
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>

// Constructor
fdtd::AdmittanceCatalogue::AdmittanceCatalogue(fdtd::Model* m) :
        m_(m),
        bitsPerHalfSide_(8),
        fourFoldSymmetry_(true),
        unitCell_(0.0008),
        startFrequency_(4e9),
        frequencyStep_(4e9),
        relativeFileName_(false) {
}

// Return the item with the given identifier, nullptr for none.
std::shared_ptr<fdtd::CatalogueItem> fdtd::AdmittanceCatalogue::get(uint64_t code) {
    std::shared_ptr<CatalogueItem> result;
    if(code < items_.size()) {
        result = items_[(size_t)code];
    }
    return result;
}

// Return the catalogue file path
std::string fdtd::AdmittanceCatalogue::catalogueFilePath() {
    std::string result;
    if(fileName_.empty()) {
        fileName_ = m_->fileName();
        relativeFileName_ = true;
    }
    if(relativeFileName_) {
        result = m_->pathName() + fileName_;
    } else {
        result = fileName_;
    }
    result += ".catalogue";
    return result;
}

// Read the catalog from the XML object
void fdtd::AdmittanceCatalogue::readCatalogue() {
    // Open the file
    std::string filePath = catalogueFilePath();
    std::ifstream file;
    file.open(filePath, std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Read the whole file
        std::stringstream buffer;
        buffer << file.rdbuf();
        // Parse into a DOM
        xml::Reader reader;
        xml::DomDocument dom("binarycatalogue");
        try {
            reader.readString(&dom, buffer.str());
            // Now process the DOM
            xml::DomObject* root = dom.getObject();
            *root >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
            *root >> xml::Obj("fourfoldsymmetry") >> fourFoldSymmetry_;
            *root >> xml::Obj("unitcell") >> unitCell_;
            *root >> xml::Obj("startfrequency") >> startFrequency_;
            *root >> xml::Obj("frequencystep") >> frequencyStep_;
            for(auto& p : root->obj("catalogueitem")) {
                auto item = std::make_shared<CatalogueItem>();
                *p >> *item;
                if(item->code() >= items_.size()) {
                    items_.resize((size_t)(item->code() + 1));
                }
                items_[(size_t)item->code()] = item;
            }
        } catch(xml::Exception& e) {
            std::cout << "Failed to read catalog XML file: " << e.what() << std::endl;
        }
        // Clean up
        file.close();
    }
}

// Write the parameters to the XML DOM object
void fdtd::AdmittanceCatalogue::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("filename") << fileName_;
    root << xml::Obj("relativefilename") << relativeFileName_;
    root << xml::Close();
}

// Read the parameters from the XML DOM object
void fdtd::AdmittanceCatalogue::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("filename") >> xml::Default("") >> fileName_;
    root >> xml::Obj("relativefilename") >> xml::Default(false) >> relativeFileName_;
    root >> xml::Close();
}
