/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUE_H
#define FDTDLIFE_CATALOGUE_H

#include <memory>
#include <map>
#include <list>
#include <set>

namespace xml { class DomObject; }

namespace fdtd {

    class CatalogueItem;

    // A class that represents an NxN binary plate catalogue.
    class Catalogue {
    public:
        explicit Catalogue(size_t bitsPerHalfSide, bool fourFoldSymmetry, double unitCell);
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Catalogue& c) {
            c.write(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Catalogue& c) {
            c.read(o);
            return o;
        }
        void getMissingItems(int numItems,
                             std::list<std::shared_ptr<CatalogueItem>>& missingItems);
        std::shared_ptr<CatalogueItem> getItem(uint64_t code);
        void addItem(const std::shared_ptr<CatalogueItem>& item);
        uint64_t lastCode();
        uint64_t numItems() { return (uint64_t) items_.size(); }
        size_t numRequired() const { return requiredCodes_.size(); }
        size_t numSymmetries() const { return symmetricalCodes_.size(); }
        void clear();
        void printItems() const;
        // Getters
        size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
        bool fourFoldSymmetry() const { return fourFoldSymmetry_; }
        double unitCell() const { return unitCell_; }
        double startFrequency() const { return startFrequency_; }
        double frequencyStep() const { return frequencyStep_; }
        bool inclCornerToCorner() const { return inclCornerToCorner_; }
        const std::map<uint64_t, std::shared_ptr<CatalogueItem>>& items() const {
            return items_;
        }
        // Setters
        void bitsPerHalfSide(size_t v) { bitsPerHalfSide_ = v; }
        void fourFoldSymmetry(bool v) { fourFoldSymmetry_ = v; }
        void unitCell(double v) { unitCell_ = v; }
        void startFrequency(double v) { startFrequency_ = v; }
        void frequencyStep(double v) { frequencyStep_ = v; }
        void inclCornerToCorner(bool v) { inclCornerToCorner_ = v; }

    protected:
        size_t bitsPerHalfSide_;
        bool inclCornerToCorner_{true};
        bool fourFoldSymmetry_;
        double unitCell_;
        double startFrequency_;
        double frequencyStep_;
        std::map<uint64_t, std::shared_ptr<CatalogueItem>> items_;
        std::set<uint64_t> requiredCodes_;
        std::set<uint64_t> symmetricalCodes_;

    protected:
        // Helpers
        void makeMissingItem(uint64_t coding,
                             std::list<std::shared_ptr<CatalogueItem>>& missingItems);
        void add2FoldSymmetryItem(uint64_t pattern,
                                  const std::shared_ptr<fdtd::CatalogueItem>& item,
                                  bool xySwap);
        void setRequiredCodes();
    };

}


#endif //FDTDLIFE_CATALOGUE_H
