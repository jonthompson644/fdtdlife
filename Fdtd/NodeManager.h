/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PROCESSINGNODEMANAGER_H
#define FDTDLIFE_PROCESSINGNODEMANAGER_H

#include "Notifier.h"
#include <list>
#include <memory>

namespace seq { class SingleSeq; }
namespace gs { class GeneticSearch; }
namespace gs { class Individual; }

namespace fdtd {

    class Model;

    class Node;

    class Job;

    class LocalNode;

    class CatalogueItem;

    class CatalogueBuilder;

    // Manages the pool of processing nodes
    class NodeManager : public Notifier {
    public:
        // Constants
        enum {
            notifyJobListChanged, notifyJobComplete, notifyAllJobsComplete
        };

        // Construction
        explicit NodeManager(Model* model);
        ~NodeManager() override;

        // API
        void clear();
        std::shared_ptr<Job> addStripeJob(bool eField, int zStart, int zCount);
        std::shared_ptr<Job> addIndividualJob(gs::GeneticSearch* sequencer,
                                              const std::shared_ptr<gs::Individual>& individual);
        std::shared_ptr<Job> addSingleJob(seq::SingleSeq* sequencer);
        std::shared_ptr<Job> addCatalogueItemJob(CatalogueBuilder* sequencer,
                                                 const std::shared_ptr<CatalogueItem>& item);
        void addJob(const std::shared_ptr<Job>& job);
        void process();
        void jobDone(Node* node);
        Node* findNode(int id);
        void localJobComplete();
        int allocNodeId() { return nextNodeId_++; }
        int allocJobId() { return nextJobId_++; }
        void addNode(Node* node);

        // Getters
        std::list<std::shared_ptr<Job>>& waitingJobs() { return waitingJobs_; }
        std::list<std::shared_ptr<Job>>& runningJobs() { return runningJobs_; }
        std::list<Node*>& nodes() { return nodes_; }
        Model* model() { return model_; }

        // Setters
        void slave(bool v) { slave_ = v; }

    protected:
        // Variables
        Model* model_;
        std::list<Node*> idleNodes_;
        std::list<Node*> busyNodes_;
        std::list<Node*> nodes_;
        std::list<std::shared_ptr<Job>> waitingJobs_;
        std::list<std::shared_ptr<Job>> runningJobs_;
        int nextNodeId_;
        int nextJobId_;
        LocalNode* localNode_;
        bool slave_;
    };

}


#endif //FDTDLIFE_PROCESSINGNODEMANAGER_H
