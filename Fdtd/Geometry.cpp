/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Geometry.h"
#include "Configuration.h"

// Initialise the cube geometry object
void fdtd::Geometry::initialise() {
    // The edge zones
    edge_ = 0U;
    // Add the scattered field zone to the edges that are not periodic
    if(p_->boundary().x() != Configuration::Boundary::periodic) {
        edge_.x(edge_.x() + p_->scatteredFieldZoneSize());
    }
    if(p_->boundary().y() != Configuration::Boundary::periodic) {
        edge_.y(edge_.y() + p_->scatteredFieldZoneSize());
    }
    if(p_->boundary().z() != Configuration::Boundary::periodic) {
        edge_.z(edge_.z() + p_->scatteredFieldZoneSize());
    }
    // Add in the boundary zone for those edges that need one
    if(p_->boundary().x() == Configuration::Boundary::absorbing) {
        edge_.x(edge_.x() + p_->boundarySize());
    }
    if(p_->boundary().y() == Configuration::Boundary::absorbing) {
        edge_.y(edge_.y() + p_->boundarySize());
    }
    if(p_->boundary().z() == Configuration::Boundary::absorbing) {
        edge_.z(edge_.z() + p_->boundarySize());
    }
}

