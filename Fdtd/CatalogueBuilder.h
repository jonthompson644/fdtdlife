/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUEBUILDER_H
#define FDTDLIFE_CATALOGUEBUILDER_H

#include <Seq/Sequencer.h>
#include "Catalogue.h"
#include <set>

namespace fdtd {

    // A sequencer class that builds a catalogue of binary NxN plates
    class CatalogueBuilder : public seq::Sequencer {
    public:
        // Constants
        enum {
            notifyCatalogueItemComplete = seq::Sequencer::notifyNextAvailableCode,
            notifyBlockComplete
        };
        // Construction
        CatalogueBuilder();
        // API
        void run() override;
        void stop() override;
        void pause() override;
        void jobComplete(const std::shared_ptr<Job>& job) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void clearAndDiscard() override;
        bool writeCatalogue();
        bool readCatalogue();
        void loadItemIntoModel(const std::shared_ptr<CatalogueItem>& item);
        std::shared_ptr<CatalogueItem> getItem(uint64_t code) {
            return catalogue_.getItem(code);
        }
        void recordData(const std::shared_ptr<CatalogueItem>& item);
        void addItem(const std::shared_ptr<CatalogueItem>& item) { catalogue_.addItem(item); }
        uint64_t numItems() { return catalogue_.numItems(); }
        // Getters
        size_t bitsPerHalfSide() const { return catalogue_.bitsPerHalfSide(); }
        bool fourFoldSymmetry() const { return catalogue_.fourFoldSymmetry(); }
        double unitCell() const { return catalogue_.unitCell(); }
        double frequencyRange() const { return frequencyRange_; }
        int numFrequencies() const { return numFrequencies_; }
        int itemsPerBlock() const { return itemsPerBlock_; }
        int cellsPerPixel() const { return cellsPerPixel_; }
        double sensorDistance() const { return sensorDistance_; }
        double frequencySpacing() const { return frequencyRange_ / numFrequencies_; }
        const std::map<uint64_t, std::shared_ptr<CatalogueItem>>&
        items() const { return catalogue_.items(); }
        const Catalogue& catalogue() const { return catalogue_; }
        bool thinSheetSubcells() const { return thinSheetSubcells_; }
        bool inclCornerToCorner() const { return catalogue_.inclCornerToCorner(); }
        size_t numRequired() const { return catalogue_.numRequired(); }
        size_t numSymmetries() const { return catalogue_.numSymmetries(); }
        // Setters
        void bitsPerHalfSide(size_t v) { catalogue_.bitsPerHalfSide(v); }
        void fourFoldSymmetry(bool v) { catalogue_.fourFoldSymmetry(v); }
        void unitCell(double v) { catalogue_.unitCell(v); }
        void frequencyRange(double v) { frequencyRange_ = v; }
        void numFrequencies(int v) { numFrequencies_ = v; }
        void itemsPerBlock(int v) { itemsPerBlock_ = v; }
        void cellsPerPixel(int v) { cellsPerPixel_ = v; }
        void sensorDistance(double v) { sensorDistance_ = v; }
        void thinSheetSubcells(bool v) { thinSheetSubcells_ = v; }
        void inclCornerToCorner(bool v) { catalogue_.inclCornerToCorner(v); }
        void name(const std::string& v) override;
    protected:
        // Attributes
        std::set<std::shared_ptr<Job>> jobs_;
        Catalogue catalogue_;
        // Configuration
        double frequencyRange_{300e9};  // The frequency range to model over
        int numFrequencies_{64};     // The number of frequencies in the range
        int itemsPerBlock_{128};      // How many items to create jobs for at once
        int cellsPerPixel_{2};      // Number of FDTD cells per pixel of the NxN plate
        double sensorDistance_{0.002};  // The distance from the grid to the sensorId
        int sensorId_{-1};           // The identifier of the sensorId used by the cat builder
        bool thinSheetSubcells_{false}; // Use the thin sheet subcell feature
        // Private constants
        static const int defaultBitsPerHalfSide_ = 8;
        static const int gridToTopCells_ = 10;
        static const int sensorToBottomCells_ = 4;
        static const int absorbingBoundarySize_ = 40;
        static constexpr double sourceAmplitude_ = 1.0;
        static constexpr double sourcePolarisation4Fold_ = 0.0;
        static constexpr double sourcePolarisation2Fold_ = 45.0;
        static constexpr double sourcePmlSigma_ = 1.0;
        static constexpr double sourceTime_ = 0.0;
        static constexpr double sourcePmlSigmaFactor_ = 1.04;
        static constexpr double sourceAzimuth_ = 0.0;
        static constexpr double layerThickness_ = 1e-6;
        static constexpr double defaultUnitCell = 0.0008;
    protected:
        // Helpers
        void runGeneration();
    };
}


#endif //FDTDLIFE_CATALOGUEBUILDER_H
