/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PROPMATRIXMETHOD_H
#define FDTDLIFE_PROPMATRIXMETHOD_H

#include <list>
#include "PropMatrix.h"

namespace domain { class UnitCellShape; }

namespace fdtd {

    class Model;

    // This class implements the propagation matrix method of
    // electrodynamic modelling.
    class PropMatrixMethod {
    public:
        // Types
        enum class LayerMode {
            begin, end, thin
        };

        class Layer {
        public:
            Layer(LayerMode mode, double z, domain::UnitCellShape* shape) :
                    mode_(mode),
                    z_(z),
                    shape_(shape) {}
            LayerMode mode() const { return mode_; }
            double z() const { return z_; }
            domain::UnitCellShape* shape() const { return shape_; }
        protected:
            LayerMode mode_;
            double z_;
            domain::UnitCellShape* shape_;
        };

        // Construction
        explicit PropMatrixMethod(Model* m);

        // API
        void calculate();

    protected:
        // Parameters
        Model* m_{};  // The main model object

        // Helpers
        void createLayerList(std::list<Layer>& layers);
        void calculateSignal(double frequency, std::list<Layer>& layers, PropMatrix& m);
        void calculateReference(double frequency, PropMatrix& m);
    };
}


#endif //FDTDLIFE_PROPMATRIXMETHOD_H
