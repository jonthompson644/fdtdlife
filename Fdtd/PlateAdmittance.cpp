/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PlateAdmittance.h"
#include "Configuration.h"
#include <Box/Constants.h>
#include <memory>

// Constructor
fdtd::PlateAdmittance::PlateAdmittance() :
        formula_(formulaLeeEtAl),
        tableType_(plateTypeSquarePlate) {
    clear();
}

// Return the admittance of the plate at the specified frequency
std::complex<double> fdtd::PlateAdmittance::admittance(PlateType plateType, double frequency,
                                                       double cellSize, double plateSize) {
    double a = cellSize;
    double c = cellSize * plateSize / 100.0;
    double lambda = 1.0 / sqrt(box::Constants::mu0_ * box::Constants::epsilon0_) / frequency;
    double alpha;
    double beta;
    double delta;
    double gamma;
    std::complex<double> Y;
    switch(formula_) {
        case formulaLeeEtAl:
            delta = (a - c) / 2.0;
            beta = (1.0 - 0.41 * delta / a) * lambda / a;
            Y = std::complex<double>(0.0, -(beta - 1.0 / beta) * (a / c + a * a / 2.0 / lambda / lambda) /
                                          log(1.0 / sin(box::Constants::pi_ * delta / 2.0 / a)));
            if(plateType == plateTypeSquarePlate) {
                Y = 1.0 / Y;
            }
            break;
        case formulaUlrich:
            delta = (a - c) / 2.0;
            beta = (1.0 - 0.27 * delta / a) * lambda / a;
            Y = std::complex<double>(0.0, -(beta - 1.0 / beta) /
                                          log(1.0 / sin(box::Constants::pi_ * delta / 2.0 / a)));
            if(plateType == plateTypeSquarePlate) {
                Y = 1.0 / Y;
            }
            break;
        case formulaArnaudEtAl:
            delta = (a - c) / 2.0;
            Y = std::complex<double>(0.0, -1 / (a / lambda *
                                                log(1.0 / sin(box::Constants::pi_ * delta / 2.0 / a))));
            if(plateType == plateTypeSquarePlate) {
                Y = 1.0 / Y;
            }
            break;
        case formulaChen:
            alpha = sqrt((lambda / a) * (lambda / a) - 1);
            beta = sqrt(2 * (lambda / a) * (lambda / a) - 1);
            gamma = cos(box::Constants::pi_ * c / a) / (1 - (2 * c / a) * (2 * c / a));
            gamma = gamma * gamma;
            delta = sin(box::Constants::pi_ * c / a) / (box::Constants::pi_ * c / a);
            delta = delta * delta;
            Y = std::complex<double>(0.0, -(2 * alpha * gamma - delta / alpha + (beta - 1 / beta) * gamma * delta));
            if(plateType == plateTypeSquarePlate) {
                Y = 1.0 / Y;
            }
            break;
        case formulaTable:
            // The breakpoint table.
            Y = getTableAdmittance(frequency, cellSize, plateSize);
            if(plateType != tableType_) {
                Y = 1.0 / Y;
            }
            break;
    }
    return Y;
}

// Write the config to the XML DOM
void fdtd::PlateAdmittance::writeConfig(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("tabletype") << tableType_;
    p << xml::Obj("formula") << formula_;
    for(auto& pos : table_) {
        p << xml::Open("admittance");
        p << xml::Obj("platesize") << pos.first;
        p << pos.second;
        p << xml::Close("admittance");
    }
    p << xml::Close();
}

// Read the config from the XML DOM
void fdtd::PlateAdmittance::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("tabletype") >> xml::Default(tableType_) >> tableType_;
    root >> xml::Obj("formula") >> xml::Default(formula_) >> formula_;
    for(auto& p : root.obj("admittance")) {
        auto& o = *p;
        double plateSize = 0.0;
        o >> xml::Obj("platesize") >> xml::Default(defaultPlateSize_) >> plateSize;
        auto pos = table_.insert(std::pair<double, AdmittanceData>(
                plateSize, AdmittanceData())).first;
        o >> pos->second;
    }
    root >> xml::Close();
}

// Return an admittance from the admittance table
std::complex<double> fdtd::PlateAdmittance::getTableAdmittance(
        double frequency, double cellSize, double plateSize) {
    std::complex<double> result(0);
    // Are there any table entries?
    if(!table_.empty()) {
        // Find the tables before and after the desired plate size
        AdmittanceData* before = &table_.begin()->second;
        double beforeSize = table_.begin()->first;
        AdmittanceData* after = before;
        double afterSize = beforeSize;
        for(auto& pos : table_) {
            if(pos.first <= plateSize) {
                // Still before the desired plate size
                before = &pos.second;
                beforeSize = pos.first;
                after = &pos.second;
                afterSize = pos.first;
            } else {
                // This is the first after the desired plate size
                after = &pos.second;
                afterSize = pos.first;
                break;
            }
        }
        // Interpolate the result between the two plate sizes
        std::complex<double> beforeVal = before->admittanceAt(frequency, cellSize);
        std::complex<double> afterVal = after->admittanceAt(frequency, cellSize);
        std::complex<double> admittancePerPercent = 0;
        if(beforeSize < afterSize) {
            admittancePerPercent = (afterVal - beforeVal) / (afterSize - beforeSize);
        }
        result = beforeVal + admittancePerPercent * (plateSize - beforeSize);
    }
    return result;
}

// Clear the plate admittance back to the pristine state
void fdtd::PlateAdmittance::clear() {
    formula_ = formulaLeeEtAl;
    table_.clear();
    tableType_ = plateTypeSquareHole;
}