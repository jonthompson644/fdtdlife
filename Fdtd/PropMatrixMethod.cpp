/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PropMatrixMethod.h"
#include "Model.h"
#include <Domain/UnitCellShape.h>
#include <Source/PlaneWaveSource.h>
#include <Sensor/ArraySensor.h>
#include <Domain/Material.h>
#include "PropMatrix.h"

// Constructor
fdtd::PropMatrixMethod::PropMatrixMethod(fdtd::Model* m) :
        m_(m) {
}

// Fill the ordered layer list to represent the shapes
void fdtd::PropMatrixMethod::createLayerList(std::list<Layer>& layers) {
    layers.clear();
    // Run through the unit cell based shapes...
    for(auto& s : m_->d()->shapes()) {
        auto u = dynamic_cast<domain::UnitCellShape*>(s.get());
        if(u != nullptr && !u->ignore()) {
            u->makeLayers(layers);
        }
    }
    // Sort the layers into increasing z order
    layers.sort([](Layer& a, Layer& b) {
        return a.z() < b.z();
    });
}

// Evaluate the propagation matrix method for a single frequency for the signal
void fdtd::PropMatrixMethod::calculateSignal(double frequency, std::list<Layer>& layers,
                                             PropMatrix& m) {
    //std::cout << "========" << std::endl;
    // Initialise things
    domain::Material* vacuum = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    domain::Material* previousMat = vacuum;
    double z = m_->p()->p1().z().value();
    bool ended = false;
    // For each layer
    for(auto layer : layers) {
        if(ended) {
            // The last transition ended a dielectric
            // Do we need to transition to vacuum before this edge?
            if(layer.z() > z) {
                //std::cout << "Match from " << previousMat->name();
                //std::cout << " into " << vacuum->name() << std::endl;
                vacuum->match(&m, previousMat);
                previousMat = vacuum;
            }
            ended = false;
        }
        // Propagate through the previous material to this boundary
        if(layer.z() > z) {
            //std::cout << "Propagate " << layer.z() - z << " through "
            //          << previousMat->name() << std::endl;
            previousMat->propagate(&m, layer.z() - z, frequency);
            z = layer.z();
        } else {
            //std::cout << "Skipped propagation z=" << z << " layerZ=" << layer.z() << std::endl;
        }
        // What kind of boundary?
        switch(layer.mode()) {
            case LayerMode::begin:
                //std::cout << "Match from " << previousMat->name();
                previousMat = layer.shape()->match(&m, previousMat, frequency);
                //std::cout << " into " << previousMat->name() << std::endl;
                break;
            case LayerMode::end:
                //std::cout << "End transition" << std::endl;
                ended = true;
                break;
            case LayerMode::thin:
                //std::cout << "Match thin layer " << layer.shape()->name() << std::endl;
                layer.shape()->match(&m, previousMat, frequency);
                break;
        }
    }
    // Is there a gap to the final boundary?
    double z1 = m_->p()->p2().z().value();
    if(z1 > z) {
        // Match to the vacuum
        if(previousMat != vacuum) {
            //std::cout << "Match from " << previousMat->name();
            //std::cout << " into " << vacuum->name() << std::endl;
            vacuum->match(&m, previousMat);
        }
        // Propagate through the vacuum
        //std::cout << "Propagate " << z1 - z << " through "
        //          << vacuum->name() << std::endl;
        vacuum->propagate(&m, z1 - z, frequency);
    }
    //std::cout << "----------" << std::endl;
}

// Evaluate the propagation matrix method for a single frequency or the reference
void fdtd::PropMatrixMethod::calculateReference(double frequency, PropMatrix& m) {
    // Initialise things
    domain::Material* vacuum = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    // Calculate what the wave would have been if no shapes in the way
    vacuum->propagate(
            &m,
            m_->p()->p2().z().value() - m_->p()->p1().z().value(),
            frequency);
}

// Execute the model using the propagation matrix method
void fdtd::PropMatrixMethod::calculate() {
    // Get the array sensors
    std::list<sensor::ArraySensor*> sensors;
    m_->sensors().getArraySensors(sensors);
    // Get the layers
    std::list<Layer> layers;
    createLayerList(layers);
    // We only use the first source...
    if(!m_->sources().sources().empty()) {
        auto* source = dynamic_cast<source::PlaneWaveSource*>(m_->sources().sources().front());
        if(source != nullptr) {
            // For each sensorId...
            for(auto sensor : sensors) {
                // For each point in the sensorId
                for(const auto& point : sensor->pointCoordinates()) {
                    // For each frequency in the source
                    for(size_t i = 0; i < source->frequencies().n().value(); i++) {
                        double freq = source->frequencies().value().frequency(i);
                        // Initialise the matrix
                        PropMatrix m;
                        calculateSignal(freq, layers, m);
                        // Calculate the transmitted and reflected waves
                        std::complex<double> transmitted = 1.0 / m.m11();
                        std::complex<double> reflected = transmitted * m.m21();
                        // Calculate what the wave would have been if no shapes in the way
                        PropMatrix mn;
                        calculateReference(freq, mn);
                        std::complex<double> natural = 1.0 / mn.m11();
                        // Write into the sensorId
                        double transAtten = transmitted.real() * transmitted.real() +
                                            transmitted.imag() * transmitted.imag();
                        double transPhShift = (std::arg(transmitted) - std::arg(natural)) /
                                              box::Constants::deg2rad_;
                        while(transPhShift < -180.0) { transPhShift += 360.0; }
                        while(transPhShift > 180.0) { transPhShift -= 360.0; }
                        double reflAtten = reflected.real() * reflected.real() +
                                           reflected.imag() * reflected.imag();
                        double reflPhShift = std::arg(reflected) / box::Constants::deg2rad_;
                        sensor->writeData(sensor::ArraySensor::dataSetPropMatrix,
                                          i, point, transAtten,
                                          transPhShift, reflAtten, reflPhShift);
                    }
                }
            }
        }
    }
}


