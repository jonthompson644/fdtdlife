/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Job.h"
#include <Xml/DomObject.h>
#include <Xml/Exception.h>
#include "SingleJob.h"
#include "IndividualJob.h"
#include "CatalogueItemJob.h"
#include <Seq/Sequencer.h>
#include <Seq/CharacterizerJob.h>
#include "Model.h"
#include <memory>

// Constructor
fdtd::Job::Job(int id, seq::Sequencer* sequencer, Mode mode) :
        mode_(mode),
        id_(id),
        nodeId_(invalidNodeId_),
        sequencer_(sequencer),
        localHost_(false) {
}

// Write the job to the DOM object
void fdtd::Job::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("id") << id_;
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("sequencer") << sequencer_->identifier();
    root << xml::Close();
}

// Read the job from the DOM object
void fdtd::Job::read(xml::DomObject& root) {
    // Id, mode and sequencer are read by the factory
    root >> xml::Open();
    root >> xml::Close();
}

// Create a job and read it from the DOM object
fdtd::Job* fdtd::Job::factory(xml::DomObject& root, Model* model) {
    Job* result = nullptr;
    try {
        Mode mode;
        int id;
        int sequencerId;
        root >> xml::Obj("mode") >> mode;
        root >> xml::Obj("id") >> id;
        root >> xml::Obj("sequencer") >> sequencerId;
        seq::Sequencer* sequencer = model->sequencers().find(sequencerId);
        switch(mode) {
            case modeSingle:
                result = new SingleJob(id, sequencer);
                break;
            case modeIndividual:
                result = new IndividualJob(id, sequencer);
                break;
            case modeCatalogItem:
                result = new CatalogueItemJob(id, sequencer);
                break;
            case modeCharacterizer:
                result = new seq::CharacterizerJob(id, sequencer);
                break;
            default:
                break;
        }
        root >> *result;
    } catch(xml::Exception&) {
        delete result;
        result = nullptr;
    }
    return result;
}
