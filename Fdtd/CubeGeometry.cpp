/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CubeGeometry.h"
#include "Configuration.h"

// Initialise the cube geometry object
void fdtd::CubeGeometry::initialise() {
    // Base class first
    Geometry::initialise();
    // Calculate the size of the arrays
    n_ = (p_->size() / p_->dr().value()).round<size_t>() + edge_ * 2U;
    nFull_ = n_ + 2U;  // Add in periodic boundaries for the full size
    arraySize_ = nFull_.x() * nFull_.y() * nFull_.z();
}

// Return the cell coordinates of a position rounded down
box::Vector<size_t> fdtd::CubeGeometry::cellFloor(const box::Vector<double>& p) const {
    return ((p - p_->p1().value()) / p_->dr().value()).floor<size_t>() + edge_;
}

// Return the cell coordinates of a position rounded up
box::Vector<size_t> fdtd::CubeGeometry::cellCeil(const box::Vector<double>& p) const {
    return ((p - p_->p1().value()) / p_->dr().value()).ceil<size_t>() + edge_;
}

// Return the cell coordinates of a position rounded
box::Vector<size_t> fdtd::CubeGeometry::cellRound(const box::Vector<double>& p) const {
    return ((p - p_->p1().value()) / p_->dr().value()).round<size_t>() + edge_;
}

// Return the position of the center of a cell
box::Vector<double> fdtd::CubeGeometry::cellCenter(const box::Vector<size_t>& p) const {
    return box::Vector<double>(p - edge_) * p_->dr().value() +
           p_->p1().value() + p_->dr().value() / 2.0;
}
