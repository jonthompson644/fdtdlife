/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "NormalField.h"
#include "Model.h"
#include <Box/BinaryFileHeader.h>

// Constructor
// m - The model
fdtd::NormalField::NormalField(Configuration* p) :
        p_(p) {
}

// Initialise the data
void fdtd::NormalField::initialise() {
    // Create the data
    arraySize_ = p_->geometry()->arraySize();
    x_.resize(arraySize_);
    y_.resize(arraySize_);
    z_.resize(arraySize_);
    // Initialise the data
    for(size_t i=0; i<p_->geometry()->arraySize(); i++) {
        x_[i] = 0.0;
        y_[i] = 0.0;
        z_[i] = 0.0;
    }
}

// Return the vector at a point
box::Vector<double> fdtd::NormalField::value(const box::Vector<size_t>& cell) {
    size_t idx = p_->geometry()->index(cell.x(),cell.y(),cell.z());
    return box::Vector<double>(x_[idx], y_[idx], z_[idx]);
}

// Print the vector field contents
void fdtd::NormalField::print(const char* title) {
    if(p_->geometry()->n().x()<=10 && p_->geometry()->n().y()<=10 && p_->geometry()->n().z()<=10) {
        if(title != nullptr) {
            std::cout << title << std::endl;
        }
        std::ios oldState(nullptr);
        oldState.copyfmt(std::cout);
        std::cout << std::scientific << std::setprecision(0) << std::showpos;
        for(size_t j=0; j<p_->geometry()->n().y(); j++) {
            for(size_t k=0; k<p_->geometry()->n().z(); k++) {
                for(size_t i=0; i<p_->geometry()->n().x(); i++) {
                    double v = x_[p_->geometry()->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(size_t j=0; j<p_->geometry()->n().y(); j++) {
            for(size_t k=0; k<p_->geometry()->n().z(); k++) {
                for(size_t i=0; i<p_->geometry()->n().x(); i++) {
                    double v = y_[p_->geometry()->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(size_t j=0; j<p_->geometry()->n().y(); j++) {
            for(size_t k=0; k<p_->geometry()->n().z(); k++) {
                for(size_t i=0; i<p_->geometry()->n().x(); i++) {
                    double v = z_[p_->geometry()->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "=======" << std::endl;
        std::cout.copyfmt(oldState);
    }
}

// Return the amount of memory used
size_t fdtd::NormalField::memoryUse() {
    return 3 * p_->geometry()->arraySize() * sizeof(double);
}

// Write the data to a file
bool fdtd::NormalField::writeData(std::ofstream &file) {
    box::BinaryFileHeader header("NORMALFIELD", 1, arraySize_*sizeof(double)*3);
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    file.write(reinterpret_cast<const char*>(x_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    file.write(reinterpret_cast<const char*>(y_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    file.write(reinterpret_cast<const char*>(z_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
    return true;
}

// Read the data from a file
bool fdtd::NormalField::readData(std::ifstream &file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("NORMALFIELD")) {
        switch(header.version()) {
            case 1:
                arraySize_ = header.recordSize() / 3 / sizeof(double);
                file.read(reinterpret_cast<char*>(x_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                file.read(reinterpret_cast<char*>(y_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                file.read(reinterpret_cast<char*>(z_.data()), static_cast<std::streamsize>(arraySize_*sizeof(double)));
                result = arraySize_ == p_->geometry()->arraySize();
                break;
            default:
                break;
        }
    }
    return result;
}
