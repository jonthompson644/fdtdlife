/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueItem.h"
#include <Sensor/ArraySensor.h>

// Constructor
fdtd::CatalogueItem::CatalogueItem(uint64_t code, uint64_t pattern) :
        code_(code),
        pattern_(pattern) {
}

// Write the item to the XML
void fdtd::CatalogueItem::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("code") << code_;
    root << xml::Obj("pattern") << pattern_;
    root << data_;
    root << xml::Close();
}

// Read the item from the XML
void fdtd::CatalogueItem::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("code") >> xml::Default(0) >> code_;
    root >> xml::Obj("pattern") >> xml::Default(0) >> pattern_;
    root >> data_;
    root >> xml::Close();
}

// Record the data from the sensorId for this item
void fdtd::CatalogueItem::recordData(sensor::ArraySensor* sensor) {
    // Copy the data from the sensorId into here
    sensor->copyOutputData(0, 0, data_);
}

// Copy the data from another item with possible a swap of components
void fdtd::CatalogueItem::copyData(const fdtd::CatalogueItem& other, bool xySwap) {
    data_ = other.data_;
    if(xySwap) {
        data_.xySwap();
    }
}
