/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VARIABLES_H
#define FDTDLIFE_VARIABLES_H

#include <list>
#include "Variable.h"
#include <Box/Configurable.h>

namespace fdtd {

    class Model;

    // The set of variables
    class Variables : public box::Configurable {
    public:
        // Construction
        explicit Variables(Model* model);

        // API
        void fillContext(box::Expression::Context& context, Variable* toHere = nullptr);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        void clear();
        size_t size() {return variables_.size();}
        Variable& add(const std::string& name, const std::string& value);
        void remove(Variable* var);
        void move(int varId, int position);
        std::list<Variable>::iterator begin() {return variables_.begin();}
        std::list<Variable>::iterator end() {return variables_.end();}
        friend xml::DomObject& operator<<(xml::DomObject& o, const Variables& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Variables& v) {
            v.readConfig(o);
            return o;
        }
        Variable* find(int identifier);
        void reorder(const std::list<int>& newOrder);
        Model* m() { return model_; }

    protected:
        // Members
        std::list<Variable> variables_;
        Model* model_ {};
    };
}


#endif //FDTDLIFE_VARIABLES_H
