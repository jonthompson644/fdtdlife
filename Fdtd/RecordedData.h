/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_RECORDEDDATA_H
#define FDTDLIFE_RECORDEDDATA_H

#include <vector>
#include <cstddef>
#include "AdmittanceData.h"
#include <complex>
#include <Xml/DomObject.h>

namespace fdtd {

    // Recorded transmittance/phase shift or real/imaginary data
    class RecordedData {
    public:
        // API
        RecordedData() = default;
        RecordedData& operator=(const RecordedData& other);
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const RecordedData& d) {d.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, RecordedData& d) {d.read(o); return o;}
        const std::vector<double>& transmittance() const { return transmittance_; }
        const std::vector<double>& transmittanceY() const { return transmittanceY_; }
        const std::vector<double>& phaseShift() const { return phaseShift_; }
        const std::vector<double>& phaseShiftY() const { return phaseShiftY_; }
        const std::vector<double>& reflectance() const { return reflectance_; }
        const std::vector<double>& reflectanceY() const { return reflectanceY_; }
        AdmittanceData admittance(double unitCell, double startFrequency, double frequencyStep) const;
        bool empty() const { return transmittance_.empty(); }
        std::vector<std::complex<double>> admittance() const;
        std::vector<std::complex<double>> admittanceY() const;
        size_t numPoints() const { return transmittance_.size(); }
        bool xyComponents() const { return xyComponents_; }
        bool hasReflectance() const {return hasReflectance_;}
        void xySwap();
        // API for array sensorId use
        void resize(size_t s, bool xyComponents, bool hasReflectance);
        void setData(size_t index, double transmittance, double phaseShift,
                     double transmittanceY, double phaseShiftY,
                     double reflectance, double reflectanceY);
    protected:
        std::vector<double> transmittance_;  // The power transmission ratio
        std::vector<double> phaseShift_;     // In degrees
        std::vector<double> transmittanceY_;  // The power transmission ratio
        std::vector<double> phaseShiftY_;     // In degrees
        std::vector<double> reflectance_;  // The power reflection ratio
        std::vector<double> reflectanceY_;  // The power reflection ratio
        bool xyComponents_ {};  // Separate X and Y components
        bool hasReflectance_ {};  // The reflectance data is valid
    };
}


#endif //FDTDLIFE_RECORDEDDATA_H
