/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Configuration.h"
#include "Model.h"
#include <Box/BinaryFileHeader.h>
#include <fstream>
#include <algorithm>
#include <Xml/DomObject.h>
#include <Box/Constants.h>
#include "CubeGeometry.h"
#include "Variables.h"

#if defined(USE_OPENMP)

#include <omp.h>

#else
#define omp_set_num_threads(p)
#define omp_get_max_threads() (1)
#endif

// Constructor
fdtd::Configuration::Configuration(Model* model) :
        model_(model) {
    maxNumThreads_ = static_cast<size_t>(omp_get_max_threads());
    geometry_ = std::make_unique<CubeGeometry>();
    geometry_->construct(0, 0, this);
    clear();
}

// Put the parameters into their pristine state
void fdtd::Configuration::clear() {
    p1_.text("-0.4e-3", "-0.4e-3", "-0.4e-3");
    p2_.text("0.4e-3", "0.4e-3", "0.4e-3");
    dr_.text("0.1e-4", "0.1e-4", "0.1e-4");
    tSize_.text("5.0e-8");
    s_ = 0.5;
    scatteredFieldZoneSize_ = 2;
    boundary_ = {Boundary::periodic, Boundary::periodic, Boundary::absorbing};
    boundarySize_ = 0;
    numThreadsCfg_ = 0;
    numThreadsUsed_ = 1;
    doPropagationMatrices_ = false;
    doFdtd_ = true;
    animationPeriod_ = 1.0;
    useThinSheetSubcells_ = false;
    plateAdmittance_.clear();
    phase_ = phaseDone;
#if defined(USE_OPENMP)
    usingOpenMp_ = true;
#else
    usingOpenMp_ = false;
#endif

    initialise();
}

// Calculate the dependent parameters for the run
void fdtd::Configuration::initialise() {
    evaluate();
    // Normalise the boundaries
    if(p1_.x().value() > p2_.x().value()) {
        box::Expression::swap(p1_.x(), p2_.x());
    }
    if(p1_.y().value() > p2_.y().value()) {
        box::Expression::swap(p1_.y(), p2_.y());
    }
    if(p1_.z().value() > p2_.z().value()) {
        box::Expression::swap(p1_.z(), p2_.z());
    }
    // Total field zone size
    size_ = p2_.value() - p1_.value();
    // The geometry
    geometry_->initialise();
    // Calculate the time parameters
    dt_ = calcDt();
    nt_ = static_cast<int>(ceil(tSize_.value() / dt_));
    timeStep_ = 0;
    stepProcessingTime_ = 0.0;
    cellProcessingRate_ = 0.0;
    maxCellProcessingRate_ = 0.0;
    // Initial phase
    phase_ = phaseDone;
    // The thread parameters
    numThreadsUsed_ = maxNumThreads_;
    if(numThreadsCfg_ > 0) {
        numThreadsUsed_ = std::min(numThreadsUsed_, numThreadsCfg_);
    }
    omp_set_num_threads(static_cast<int>(numThreadsUsed_));
    threadInfo_.resize(numThreadsUsed_);
    size_t numZ =
            (static_cast<size_t>(geometry_->n().z()) + numThreadsUsed_ - 1) / numThreadsUsed_;
    for(size_t i = 0; i < numThreadsUsed_; i++) {
        threadInfo_[i].startZ(numZ * i);
        threadInfo_[i].numZ(std::min(geometry_->n().z() - threadInfo_[i].startZ(), numZ));
    }
}

// Set the thread information up for the given calculation zone
void fdtd::Configuration::setupThreadInfo(size_t zStart, size_t zCount) {
    omp_set_num_threads(static_cast<int>(numThreadsUsed_));
    threadInfo_.resize(numThreadsUsed_);
    size_t numZ = (zCount + numThreadsUsed_ - 1) / numThreadsUsed_;
    for(size_t i = 0; i < numThreadsUsed_; i++) {
        threadInfo_[i].startZ(numZ * i + zStart);
        threadInfo_[i].numZ(std::min((zStart + zCount) - threadInfo_[i].startZ(), numZ));
    }
}

// Calculate the time step from the configured parameters.
double fdtd::Configuration::calcDt() {
    return s_ * std::min(std::min(dr_.x().value(), dr_.y().value()), dr_.z().value())
           / box::Constants::c_;
}

// Write the model state to the binary file
bool fdtd::Configuration::writeData(std::ofstream& file) {
    box::BinaryFileHeader header("PARAMETERS", 1, 0);
    header.userInt(0, timeStep_);
    header.userInt(1, phase_);
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    return true;
}

// Read the model state from the binary file
bool fdtd::Configuration::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("PARAMETERS")) {
        switch(header.version()) {
            case 1:
                timeStep_ = static_cast<int>(header.userInt(0));
                phase_ = static_cast<Phase>(header.userInt(1));
                result = true;
                break;
            default:
                result = true;
                break;
        }
    }
    return result;
}

// Write the parameters to the XML DOM object
void fdtd::Configuration::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("x1") << p1_.x();
    root << xml::Obj("y1") << p1_.y();
    root << xml::Obj("z1") << p1_.z();
    root << xml::Obj("x2") << p2_.x();
    root << xml::Obj("y2") << p2_.y();
    root << xml::Obj("z2") << p2_.z();
    root << xml::Obj("drx") << dr_.x();
    root << xml::Obj("dry") << dr_.y();
    root << xml::Obj("drz") << dr_.z();
    root << xml::Obj("tsize") << tSize_;
    root << xml::Obj("s") << s_;
    root << xml::Obj("scatteredfieldzonesize") << scatteredFieldZoneSize_;
    root << xml::Obj("boundary") << boundary_;
    root << xml::Obj("boundarysize") << boundarySize_;
    root << xml::Obj("numthreads") << numThreadsCfg_;
    root << xml::Obj("dopropagationmatrices") << doPropagationMatrices_;
    root << xml::Obj("dofdtd") << doFdtd_;
    root << xml::Obj("animationperiod") << animationPeriod_;
    root << xml::Obj("usethinsheetsubcells") << useThinSheetSubcells_;
    root << plateAdmittance_;
    root << xml::Close();
}

// Read the parameters from the XML DOM object
void fdtd::Configuration::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("x1") >> p1_.x();
    root >> xml::Obj("y1") >> p1_.y();
    root >> xml::Obj("z1") >> p1_.z();
    root >> xml::Obj("x2") >> p2_.x();
    root >> xml::Obj("y2") >> p2_.y();
    root >> xml::Obj("z2") >> p2_.z();
    root >> xml::Obj("drx") >> dr_.x();
    root >> xml::Obj("dry") >> dr_.y();
    root >> xml::Obj("drz") >> dr_.z();
    root >> xml::Obj("tsize") >> tSize_;
    root >> xml::Obj("s") >> s_;
    root >> xml::Obj("scatteredfieldzonesize") >> scatteredFieldZoneSize_;
    if(root.obj("boundary").empty()) {
        // Backwards compatible
        Boundary b;
        root >> xml::Obj("boundaryx") >> b;
        boundary_.x(b);
        root >> xml::Obj("boundaryy") >> b;
        boundary_.y(b);
        root >> xml::Obj("boundaryz") >> b;
        boundary_.z(b);
    } else {
        root >> xml::Obj("boundary") >> boundary_;
    }
    root >> xml::Obj("boundarysize") >> boundarySize_;
    root >> xml::Obj("numthreads") >> numThreadsCfg_;
    root >> xml::Obj("dopropagationmatrices") >> xml::Default(doPropagationMatrices_)
         >> doPropagationMatrices_;
    root >> xml::Obj("dofdtd") >> xml::Default(doFdtd_) >> doFdtd_;
    root >> xml::Obj("usethinsheetsubcells") >> xml::Default(useThinSheetSubcells_)
         >> useThinSheetSubcells_;
    root >> xml::Obj("animationperiod") >> xml::Default(animationPeriod_)
         >> animationPeriod_;
    root >> plateAdmittance_;
    root >> xml::Close();
}

// Set the processing time statistics
void fdtd::Configuration::stepProcessingTime(double v) {
    // The elapsed step time
    stepProcessingTime_ = v;
    // The cell rate
    cellProcessingRate_ = static_cast<double>(geometry_->n().x()) *
                          static_cast<double>(geometry_->n().y()) *
                          static_cast<double>(geometry_->n().z()) /
                          stepProcessingTime_ / box::Constants::kilo_;
    maxCellProcessingRate_ = std::max(maxCellProcessingRate_, cellProcessingRate_);
}

// Evaluate the expressions
void fdtd::Configuration::evaluate() {
    if(model_ != nullptr) {
        box::Expression::Context c;
        model_->variables().fillContext(c);
        try {
            p1_.evaluate(c);
            p2_.evaluate(c);
            dr_.evaluate(c);
            tSize_.evaluate(c);
        } catch(box::Expression::Exception& e) {
            std::cout << e.what() << std::endl;
        }
    }
}

