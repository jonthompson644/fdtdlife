/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCEDATA_H
#define FDTDLIFE_ADMITTANCEDATA_H

#include <list>
#include <complex>

namespace xml { class DomObject; }

namespace fdtd {

    // A class that encapsulates admittance data
    class AdmittanceData {
    public:
        // Construction
        AdmittanceData();
        AdmittanceData(const AdmittanceData& other);

        // API
        AdmittanceData& operator=(const AdmittanceData& other) = default;
        void clear();
        double cellSize() const { return cellSize_; }
        void cellSize(double v) { cellSize_ = v; }
        void addPoint(long long frequency, std::complex<double> admittance);
        void writeXml(xml::DomObject& root) const;
        void readXml(xml::DomObject& root);
        std::complex<double> admittanceAt(long long frequency, double cellSize);
        friend xml::DomObject& operator<<(xml::DomObject& o, const AdmittanceData& v) {
            v.writeXml(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, AdmittanceData& v) {
            v.readXml(o);
            return o;
        }

        // Getters
        const std::list<std::pair<long long, std::complex<double>>>& admittance() const {
            return admittance_;
        }

    protected:
        // Members
        std::list<std::pair<long long, std::complex<double>>> admittance_;
        double cellSize_{};   // The cell size the admittance data is valid for
    };
}


#endif //FDTDLIFE_ADMITTANCEDATA_H
