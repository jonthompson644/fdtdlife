/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_INDIVIDUALJOB_H
#define FDTDLIFE_INDIVIDUALJOB_H

#include "Job.h"
#include <memory>

namespace seq {class Sequencer;}
namespace gs {class Individual;}
namespace gs {class GeneticSearch;}

namespace fdtd {


    // A class that represents a job to be executed by the
    // processing node pool
    class IndividualJob : public Job {
    public:
        // Construction
        IndividualJob(int id, gs::GeneticSearch* sequencer, const std::shared_ptr<gs::Individual>& individual);
        IndividualJob(int id, seq::Sequencer* sequencer);

        // Overrides of Job
        void process(Model* model, bool localHost) override;
        std::string displayName() override;
        void complete(Model* model) override;
        void write(xml::DomObject& root) const override;
        void read(xml::DomObject& root) override;

        // Getters
        gs::GeneticSearch* sequencer() const {return sequencer_;}

    protected:
        // Variables
        std::shared_ptr<gs::Individual> individual_;
        gs::GeneticSearch* sequencer_;
    };

}

#endif //FDTDLIFE_INDIVIDUALJOB_H
