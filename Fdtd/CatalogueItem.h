/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUEITEM_H
#define FDTDLIFE_CATALOGUEITEM_H

#include <cstdint>
#include "RecordedData.h"
#include "AdmittanceData.h"
#include <Xml/DomObject.h>

namespace sensor { class ArraySensor; }

namespace fdtd {

    // A class that represents a single pattern in a binary catalogue
    class CatalogueItem {
    public:
        // Construction
        explicit CatalogueItem(uint64_t code = 0, uint64_t pattern = 0);
        // API
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const CatalogueItem& c) {
            c.write(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, CatalogueItem& c) {
            c.read(o);
            return o;
        }
        [[nodiscard]] const std::vector<double>& transmittance() const {
            return data_.transmittance();
        }
        [[nodiscard]] const std::vector<double>& transmittanceY() const {
            return data_.transmittanceY();
        }
        [[nodiscard]] const std::vector<double>& phaseShift() const {
            return data_.phaseShift();
        }
        [[nodiscard]] const std::vector<double>& phaseShiftY() const {
            return data_.phaseShiftY();
        }
        [[nodiscard]] std::vector<std::complex<double>> admittance() const {
            return data_.admittance();
        }
        [[nodiscard]] std::vector<std::complex<double>> admittanceY() const {
            return data_.admittanceY();
        }
        [[nodiscard]] AdmittanceData admittance(double unitCell, double startFrequency,
                                                double frequencyStep) const {
            return data_.admittance(unitCell, startFrequency, frequencyStep);
        }
        void recordData(sensor::ArraySensor* sensor);
        [[nodiscard]] size_t numPoints() const { return data_.numPoints(); }
        void copyData(const CatalogueItem& other, bool xySwap);
        // Getters
        [[nodiscard]] uint64_t code() const { return code_; }
        [[nodiscard]] uint64_t pattern() const { return pattern_; }
        [[nodiscard]] bool xyComponents() const { return data_.xyComponents(); }
        [[nodiscard]] const RecordedData& data() const { return data_; }
        [[nodiscard]] size_t classification() const { return classification_; }
        // Setters
        void classification(size_t v) { classification_ = v; }
    protected:
        uint64_t code_;     // The pattern code
        uint64_t pattern_;  // The two-fold pattern
        RecordedData data_;  // The transmittance and phase shift data
        size_t classification_{};  // The classification number
    };

}

#endif //FDTDLIFE_CATALOGUEITEM_H
