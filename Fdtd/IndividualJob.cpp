/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "IndividualJob.h"
#include "Model.h"
#include <Gs/GeneticSearch.h>
#include <Gs/Individual.h>
#include <Xml/DomObject.h>
#include <iostream>
#include <memory>

// Constructor
fdtd::IndividualJob::IndividualJob(int id, gs::GeneticSearch* sequencer,
                                   const std::shared_ptr<gs::Individual>& individual) :
        Job(id, sequencer, modeIndividual),
        individual_(individual),
        sequencer_(sequencer) {
}

// Constructor for use by the job factory
fdtd::IndividualJob::IndividualJob(int id, seq::Sequencer* sequencer) :
        Job(id, sequencer, modeIndividual),
        sequencer_(dynamic_cast<gs::GeneticSearch*>(sequencer)) {
}

// Process this job
void fdtd::IndividualJob::process(fdtd::Model* model, bool localHost) {
    Job::process(model, localHost);
    individual_->decodeGenes(sequencer_);
    sequencer_->loadIndividualIntoModel(individual_);
    individual_->setRunning();
    sequencer_->doNotification(gs::GeneticSearch::notifyIndividualLoaded, individual_.get());
    model->initialise();
    model->p()->phase(Configuration::phaseElectric);
}

// Return a string that indicates the purpose of the job
std::string fdtd::IndividualJob::displayName() {
    std::stringstream buf;
    buf << "Individual: ";
    return buf.str();
}

// The job has completed
void fdtd::IndividualJob::complete(fdtd::Model* /*model*/) {
    if(localHost_) {
        sequencer_->evaluateFitness(individual_);
    }
    sequencer_->doNotification(seq::Sequencer::notifyJobComplete);
}

// Write the job to the XML DOM
void fdtd::IndividualJob::write(xml::DomObject& root) const {
    // Base class
    Job::write(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("individual") << *individual_;
    root << xml::Close();
}

// Read the job from the XML DOM
void fdtd::IndividualJob::read(xml::DomObject& root) {
    // Base class
    Job::read(root);
    // My stuff
    root >> xml::Reopen();
    auto o = root.obj("individual");
    if(!o.empty()) {
        // Only create a new individual if we do not already have one
        if(!individual_) {
            individual_ = gs::Individual::factory(*o.front(), sequencer_);
        }
        *o.front() >> *individual_;
    }
    root >> xml::Close();
}
