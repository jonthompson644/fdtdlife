/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUEITEMWRAPPER_H
#define FDTDLIFE_CATALOGUEITEMWRAPPER_H

#include <memory>
#include "CatalogueItem.h"
#include <Xml/DomObject.h>

namespace fdtd {

    class CatalogueItem;
    class Catalogue;

    // A class that wraps a catalogue item and adds information
    // the raw item is missing.  This allows catalogue items to
    // be passed around the system with all the necessary information
    // without burdening the catalogue itself with loads of
    // redundant data.
    class CatalogueItemWrapper {
    public:
        // Construction
        CatalogueItemWrapper(const std::shared_ptr<CatalogueItem>& item,
                const Catalogue& catalogue);
        CatalogueItemWrapper();
        // API
        void read(xml::DomObject& root);
        void write(xml::DomObject& root) const;
        friend xml::DomObject& operator<<(xml::DomObject& o, const CatalogueItemWrapper& c) {c.write(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, CatalogueItemWrapper& c) {c.read(o); return o;}
        // Getters
        const std::shared_ptr<CatalogueItem>& item() const {return item_;}
        size_t bitsPerHalfSide() const {return bitsPerHalfSide_;}
        bool fourFoldSymmetry() const {return fourFoldSymmetry_;}
        double unitCell() const {return unitCell_;}
        double startFrequency() const {return startFrequency_;}
        double frequencyStep() const {return frequencyStep_;}
        uint64_t pattern() const {return item_->pattern();}
        AdmittanceData admittance() const;
    protected:
        // Attributes
        std::shared_ptr<CatalogueItem> item_;
        size_t bitsPerHalfSide_;
        bool fourFoldSymmetry_;
        double unitCell_;
        double startFrequency_;
        double frequencyStep_;
    };
}


#endif //FDTDLIFE_CATALOGUEITEMWRAPPER_H
