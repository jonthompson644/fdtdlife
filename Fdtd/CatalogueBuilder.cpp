/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueBuilder.h"
#include "Model.h"
#include "NodeManager.h"
#include <Xml/DomObject.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <Source/PlaneWaveSource.h>
#include <Source/Sources.h>
#include <Domain/Material.h>
#include <Domain/BinaryPlateNxN.h>
#include "CatalogueItem.h"
#include <Sensor/ArraySensor.h>
#include "Job.h"
#include <Box/Constants.h>

// Constructor
fdtd::CatalogueBuilder::CatalogueBuilder() :
        catalogue_(defaultBitsPerHalfSide_, true, defaultUnitCell) {
}

// Write the configuration to the DOM object
void fdtd::CatalogueBuilder::writeConfig(xml::DomObject& root) const {
    // Base class first
    Sequencer::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide();
    root << xml::Obj("fourfoldsymmetry") << fourFoldSymmetry();
    root << xml::Obj("unitcell") << unitCell();
    root << xml::Obj("frequencyrange") << frequencyRange_;
    root << xml::Obj("numfrequencies") << numFrequencies_;
    root << xml::Obj("itemsperblock") << itemsPerBlock_;
    root << xml::Obj("cellsperpixel") << cellsPerPixel_;
    root << xml::Obj("sensordistance") << sensorDistance_;
    root << xml::Obj("thinsheetsubcells") << thinSheetSubcells_;
    root << xml::Obj("inclcornertocorner") << inclCornerToCorner();
    root << xml::Obj("sensorid") << sensorId_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void fdtd::CatalogueBuilder::readConfig(xml::DomObject& root) {
    // Base class first
    Sequencer::readConfig(root);
    // Now my stuff
    int bphs;
    bool ffs;
    double uc;
    bool inclCtoC;
    root >> xml::Reopen();
    root >> xml::Obj("bitsperhalfside") >> bphs;
    bitsPerHalfSide(bphs);
    root >> xml::Obj("fourfoldsymmetry") >> ffs;
    fourFoldSymmetry(ffs);
    root >> xml::Obj("unitcell") >> uc;
    unitCell(uc);
    root >> xml::Obj("inclcornertocorner") >> xml::Default(true) >> inclCtoC;
    inclCornerToCorner(inclCtoC);
    root >> xml::Obj("frequencyrange") >> frequencyRange_;
    root >> xml::Obj("numfrequencies") >> numFrequencies_;
    root >> xml::Obj("itemsperblock") >> itemsPerBlock_;
    root >> xml::Obj("cellsperpixel") >> cellsPerPixel_;
    root >> xml::Obj("sensordistance") >> sensorDistance_;
    root >> xml::Obj("thinsheetsubcells") >> thinSheetSubcells_;
    root >> xml::Obj("sensorid") >> sensorId_;
    root >> xml::Close();
    // Read the catalog
    readCatalogue();
}

// Start running the sequence
void fdtd::CatalogueBuilder::run() {
    if(!slave_) {
        runGeneration();
    }
}

// A job has completed
void fdtd::CatalogueBuilder::jobComplete(const std::shared_ptr<Job>& job) {
    // Is this a job we are waiting for?
    std::cout << "Job Complete: " << job->id() << std::endl;
    auto pos = jobs_.find(job);
    if(pos != jobs_.end()) {
        jobs_.erase(pos);
        doNotification(notifyCatalogueItemComplete);
        // Is the block complete?
        if(jobs_.empty()) {
            // All items in the block have been run
            if(!slave_) {
                writeCatalogue();
            }
            doNotification(notifyBlockComplete);
            if(!slave_) {
                runGeneration();
            } else {
                std::cout << "This is a slave sequencer: " << job->id() << std::endl;
            }
        } else {
            std::cout << "More jobs in the block: " << job->id() << std::endl;
        }
    } else {
        std::cout << "Not one of my jobs: " << job->id() << std::endl;
    }
}

// Run the next generation of the catalogue
void fdtd::CatalogueBuilder::runGeneration() {
    // Create jobs for a block of catalogue items that need modelling
    std::list<std::shared_ptr<CatalogueItem>> items;
    catalogue_.getMissingItems(itemsPerBlock_, items);
    // Create jobs for the missing items
    for(auto& item : items) {
        auto job = m_->processingMgr()->addCatalogueItemJob(this, item);
        jobs_.insert(job);
        std::cout << "Run generation item " << item->code() << " pattern "
                  << item->pattern() << std::endl;
    }
}

// Load a catalogue item into the model
void fdtd::CatalogueBuilder::loadItemIntoModel(
        const std::shared_ptr<fdtd::CatalogueItem>& item) {
    // Clear out any previous shapes, materials, sources
    m_->d()->clear();
    m_->sources().clear();
    // Set general parameters
    m_->p()->boundary({Configuration::Boundary::periodic, Configuration::Boundary::periodic,
                       Configuration::Boundary::absorbing});
    m_->p()->boundarySize(absorbingBoundarySize_);
    m_->p()->doFdtd(true);
    m_->p()->doPropagationMatrices(false);
    m_->p()->useThinSheetSubcells(thinSheetSubcells_);
    // The domain size
    double cellSize = unitCell() / (double) bitsPerHalfSide() / 2.0 / (double) cellsPerPixel_;
    double gridToTop = gridToTopCells_ * cellSize;
    double sensorToBottom = sensorToBottomCells_ * cellSize;
    m_->p()->s(0.5);
    m_->p()->scatteredFieldZoneSize(2);
    m_->p()->p1({-unitCell() / 2.0, -unitCell() / 2.0, -gridToTop});
    m_->p()->p2({unitCell() / 2.0, unitCell() / 2.0,
                 sensorToBottom + sensorDistance_});
    m_->p()->dr({cellSize, cellSize, cellSize});
    // The test frequencies
    double timeStep = m_->p()->calcDt();
    auto nSamples = (int) std::floor(1.0 / frequencySpacing() / timeStep);
    double initialTime = timeStep * nSamples * 0.5;
    m_->p()->tSize(box::Expression(
            timeStep * nSamples * 1.01 + m_->p()->size().z() / box::Constants::c_));
    auto* source = dynamic_cast<source::PlaneWaveSource*>(m_->sources().factory()->make(
            source::SourceFactory::modePlaneWave, m_));
    source->name("CatalogueSource");
    source->frequencies().value({frequencySpacing(), frequencySpacing(),
                                 (size_t) numFrequencies_});
    source->amplitude(box::Expression(sourceAmplitude_));
    if(fourFoldSymmetry()) {
        source->polarisation(box::Expression(sourcePolarisation4Fold_));
    } else {
        source->polarisation(box::Expression(sourcePolarisation2Fold_));
    }
    source->pmlSigma(sourcePmlSigma_);
    source->continuous(true);
    source->time(box::Expression(sourceTime_));
    source->pmlSigmaFactor(sourcePmlSigmaFactor_);
    source->azimuth(box::Expression(sourceAzimuth_));
    source->initialTime(box::Expression(initialTime));
    // The PEC material
    auto pecMat = m_->d()->makeMaterial(domain::Domain::materialModeNormal);
    pecMat->name("CatalogueMaterial");
    pecMat->epsilon(box::Expression(box::Constants::epsilonMetal_));
    pecMat->mu(box::Expression(box::Constants::mu0_));
    pecMat->sigma(box::Expression(box::Constants::sigma0_));
    pecMat->sigmastar(box::Expression(box::Constants::sigmastar0_));
    pecMat->color(box::Constants::colourYellow);
    pecMat->priority(0);
    // Create the binary shape
    auto* plate = dynamic_cast<domain::BinaryPlateNxN*>(m_->d()->makeShape(
            domain::Domain::shapeModeBinaryPlateNxN));
    plate->name("CatalogueShape");
    plate->material(pecMat->index());
    plate->bitsPerHalfSide(catalogue_.bitsPerHalfSide());
    plate->coding({item->pattern()});
    plate->center().value(box::Vector<double>(0.0));
    plate->cellSizeX(box::Expression(catalogue_.unitCell()));
    plate->layerThickness(box::Expression(layerThickness_));
    plate->fillMaterial(domain::Domain::defaultMaterial);
    // Create a sensorId
    sensor::Sensor* sensor = m_->sensors().find(sensorId_);
    auto* arraySensor = dynamic_cast<sensor::ArraySensor*>(sensor);
    if(arraySensor == nullptr) {
        m_->sensors().remove(sensor);
        arraySensor = dynamic_cast<sensor::ArraySensor*>(
                m_->sensors().makeSensor(sensor::Sensors::modeArraySensor));
        sensorId_ = arraySensor->identifier();
        arraySensor->name("CatalogueSensor");
        arraySensor->colour(box::Constants::colourWhite);
        arraySensor->saveCsvData(false);
    }
    arraySensor->numPointsX(1);
    arraySensor->numPointsY(1);
    arraySensor->currentPointX(0);
    arraySensor->currentPointY(0);
    arraySensor->frequency(frequencySpacing());
    arraySensor->animateFrequency(false);
    arraySensor->frequencyStep(frequencySpacing());
    arraySensor->averageOverCell(true);
    arraySensor->componentSel({true, !fourFoldSymmetry(), false});
}

// Record the data from the sensorId for this item
void fdtd::CatalogueBuilder::recordData(const std::shared_ptr<CatalogueItem>& item) {
    sensor::Sensor* sensor = m_->sensors().find(sensorId_);
    auto* arraySensor = dynamic_cast<sensor::ArraySensor*>(sensor);
    if(arraySensor != nullptr) {
        item->recordData(arraySensor);
    }
    std::cout << "Recorded data for " << item->code() << std::endl;
}

// Write the current state of the catalogue to a file
bool fdtd::CatalogueBuilder::writeCatalogue() {
    bool result = false;
    // Set required information
    catalogue_.frequencyStep(frequencySpacing());
    catalogue_.startFrequency(frequencySpacing());
    try {
        // Make the DOM
        auto* root = new xml::DomObject("binarycatalogue");
        xml::DomDocument dom(root);
        catalogue_.write(*root);
        xml::Writer writer;
        writer.writeFile(&dom, productFilename() + ".catalogue");
        result = true;
    } catch(xml::Exception& e) {
        std::cout << "Failed to write catalogue XML file: " << e.what() << std::endl;
    }
    return result;
}

// Read the catalogue from a file
bool fdtd::CatalogueBuilder::readCatalogue() {
    bool result = false;
    // Open the file
    std::ifstream file;
    file.open(productFilename() + ".catalogue", std::ios::in | std::ios::binary);
    if(file.is_open()) {
        // Read the whole file
        std::stringstream buffer;
        buffer << file.rdbuf();
        // Parse into a DOM
        xml::Reader reader;
        xml::DomDocument dom("binarycatalogue");
        try {
            reader.readString(&dom, buffer.str());
            // Now process the DOM
            catalogue_.read(*dom.getObject());
        } catch(xml::Exception& e) {
            std::cout << "Failed to read catalogue XML file: " << e.what() << std::endl;
        }
        // The result
        result = true;
    }
    return result;
}

// The job is being stopped
void fdtd::CatalogueBuilder::stop() {
    // Base class first
    Sequencer::stop();
    // Now me
    writeCatalogue();
    jobs_.clear();
}

// The job is being paused
void fdtd::CatalogueBuilder::pause() {
    // Base class first
    Sequencer::pause();
    // Now me
    writeCatalogue();
}

// The sequencer name is being changed
void fdtd::CatalogueBuilder::name(const std::string& v) {
    // Get the original product file name
    std::string oldFileRoot = productFilename();
    // Call base class to actually change the name
    Sequencer::name(v);
    // Do we need to change the name of any product files?
    std::string newFileRoot = productFilename();
    if(oldFileRoot != newFileRoot) {
        renameFiles(oldFileRoot, newFileRoot, {".catalogue"});
    }
}

// Celar the sequencer and discard any previous results
void fdtd::CatalogueBuilder::clearAndDiscard() {
    catalogue_.clear();
    ::remove((productFilename() + ".catalogue").c_str());
}
