/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "RecordedData.h"
#include <Xml/DomObject.h>
#include "Model.h"
#include <Xml/Exception.h>
#include <Box/Constants.h>
#include <algorithm>

// Assignment operator
fdtd::RecordedData& fdtd::RecordedData::operator=(const fdtd::RecordedData& other) {
    transmittance_ = other.transmittance_;
    phaseShift_ = other.phaseShift_;
    transmittanceY_ = other.transmittanceY_;
    phaseShiftY_ = other.phaseShiftY_;
    xyComponents_ = other.xyComponents_;
    hasReflectance_ = other.hasReflectance_;
    return *this;
}

// Write the recorded data to the XML
void fdtd::RecordedData::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("transmittance") << transmittance_;
    root << xml::Obj("phaseshift") << phaseShift_;
    if(hasReflectance_) {
        root << xml::Obj("reflectance") << reflectance_;
    }
    if(xyComponents_) {
        root << xml::Obj("transmittancey") << transmittanceY_;
        root << xml::Obj("phaseshifty") << phaseShiftY_;
        if(hasReflectance_) {
            root << xml::Obj("reflectancey") << reflectanceY_;
        }
    }
    root << xml::Close();
}

// Read the recorded data from the XML
void fdtd::RecordedData::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("transmittance") >> transmittance_;
    root >> xml::Obj("phaseshift") >> phaseShift_;
    try {
        root >> xml::Obj("reflectance") >> reflectance_;
        hasReflectance_ = true;
    } catch(xml::Exception&) {
        hasReflectance_ = false;
    }
    try {
        root >> xml::Obj("transmittancey") >> transmittanceY_;
        root >> xml::Obj("phaseshifty") >> phaseShiftY_;
        xyComponents_ = true;
        if(hasReflectance_) {
            root >> xml::Obj("reflectancey") >> reflectanceY_;
        }
    } catch(xml::Exception&) {
        xyComponents_ = false;
    }
    root >> xml::Close();
}

// Set the size of the output arrays
void fdtd::RecordedData::resize(size_t s, bool xyComponents, bool hasReflectance) {
    transmittance_.resize(s);
    phaseShift_.resize(s);
    xyComponents_ = xyComponents;
    hasReflectance_ = hasReflectance;
    if(hasReflectance_) {
        reflectance_.resize(s);
    } else {
        reflectance_.clear();
    }
    if(xyComponents_) {
        transmittanceY_.resize(s);
        phaseShiftY_.resize(s);
        if(hasReflectance_) {
            reflectanceY_.resize(s);
        } else {
            reflectanceY_.clear();
        }
    } else {
        transmittanceY_.clear();
        phaseShiftY_.clear();
        reflectanceY_.clear();
    }
}

// Set a data point
void fdtd::RecordedData::setData(size_t index, double transmittance, double phaseShift,
                                 double transmittanceY, double phaseShiftY,
                                 double reflectance, double reflectanceY) {
    transmittance_[index] = transmittance;
    phaseShift_[index] = phaseShift;
    if(hasReflectance_) {
        reflectance_[index] = reflectance;
    }
    if(xyComponents_) {
        transmittanceY_[index] = transmittanceY;
        phaseShiftY_[index] = phaseShiftY;
        if(hasReflectance_) {
            reflectanceY_[index] = reflectanceY;
        }
    }
}

// Calculate and return the admittance data
fdtd::AdmittanceData fdtd::RecordedData::admittance(
        double unitCell, double startFrequency, double frequencyStep) const {
    AdmittanceData result;
    result.cellSize(unitCell);
    size_t len = std::min(transmittance_.size(), phaseShift_.size());
    double frequency = startFrequency;
    for(size_t i = 0; i < len; i++) {
        double tanp = tan(phaseShift_[i] * box::Constants::deg2rad_);
        double den = sqrt(transmittance_[i] * (1 + tanp * tanp));
        std::complex<double> a;
        a.real(1 / den - 1);
        a.imag(-tanp / den);
        result.addPoint(frequency, a);
        frequency += frequencyStep;
    }
    return result;
}

// Return the complex admittance data
std::vector<std::complex<double>> fdtd::RecordedData::admittance() const {
    std::vector<std::complex<double>> result;
    size_t len = std::min(transmittance_.size(), phaseShift_.size());
    for(size_t i = 0; i < len; i++) {
        double tanp = tan(phaseShift_[i] * box::Constants::deg2rad_);
        double den = sqrt(transmittance_[i] * (1 + tanp * tanp));
        result.emplace_back(1 / den - 1, -tanp / den);
    }
    return result;
}

// Return the complex admittance data
std::vector<std::complex<double>> fdtd::RecordedData::admittanceY() const {
    std::vector<std::complex<double>> result;
    if(xyComponents_) {
        size_t len = std::min(transmittanceY_.size(), phaseShiftY_.size());
        for(size_t i = 0; i < len; i++) {
            double tanp = tan(phaseShiftY_[i] * box::Constants::deg2rad_);
            double den = sqrt(transmittanceY_[i] * (1 + tanp * tanp));
            result.emplace_back(1 / den - 1, -tanp / den);
        }
    }
    return result;
}

// Swap the xy components over
void fdtd::RecordedData::xySwap() {
    if(xyComponents_) {
        std::swap(transmittance_, transmittanceY_);
        std::swap(phaseShift_, phaseShiftY_);
        if(hasReflectance_) {
            std::swap(reflectance_, reflectanceY_);
        }
    }
}
