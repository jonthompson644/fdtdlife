/*
* ===========================================================================
* Copyright 2017 Jonathan Thompson
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
* ===========================================================================
*/

#ifndef FDTDLIFE_MODEL_H
#define FDTDLIFE_MODEL_H

#include <list>
#include <map>
#include <ctime>
#include "Configuration.h"
#include <Domain/Domain.h>
#include "Notifier.h"
#include "NotificationMgr.h"
#include <Seq/Sequencers.h>
#include "AdmittanceCatalogue.h"
#include "Variables.h"
#include "PropMatrixMethod.h"
#include <Sensor/Sensors.h>
#include <Sensor/Analysers.h>
#include <Source/Sources.h>
#include <Designer/Designers.h>
#include <string>
#include <Box/ExportWriter.h>

namespace xml { class DomObject; }
namespace domain { class Material; }
namespace domain { class Shape; }

namespace fdtd {

    // Forward declarations
    class EField;

    class HField;

    class NodeManager;

    class Individual;

    // The main model class.  Inherit from this class to create
    // an FDTD application.
    class Model : public NotificationMgr, public Notifier {

    protected:
        // Members
        Variables variables_;
        Configuration p_;
        domain::Domain d_;
        EField* e_;
        HField* h_;
        source::Sources sources_;
        sensor::Sensors sensors_;
        sensor::Analysers analysers_;
        seq::Sequencers sequencers_;
        designer::Designers designers_;
        NodeManager* processingMgr_;
        PropMatrixMethod propMatrix_;
        std::string pathName_;  // The file path (with trailing directory seperator)
        std::string fileName_;  // The file name (without the extension)
        AdmittanceCatalogue admittanceCat_;
        struct timespec stepStartTime_;

    public:
        enum {
            notifySteppedE = 0, notifySteppedH, notifyStepComplete, notifyRunComplete,
            notifyVariableChange, notifySensorChange, notifyMaterialChange,
            notifySequencerChange, notifyAnalysersCalculate, notifyAnalyserChange,
            notifyShapeChange, notifyNextAvailableCode
        };

    public:
        // Construction
        Model();
        ~Model() override;

        // Methods
        virtual void initialise();
        void step();
        bool start();
        void stop();
        void pause();
        virtual void clear();
        size_t memoryUse();
        void doStepE();
        void doStepH();
        void processingComplete();
        bool writeData(const std::string& fileName);
        bool readData(const std::string& fileName);
        void performStepFragment(bool eField, size_t zStart, size_t zCount);
        bool writeConfigFile(const std::string& pathName, const std::string& fileName);
        bool readConfigFile(const std::string& pathName, const std::string& fileName);
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        virtual void createFields();
        void clearSeqGenerated();
        void writeExport(box::ExportWriter& writer, const std::set<int>& selectedMaterials);

        // Getters
        Configuration* p() { return &p_; }
        EField* e() { return e_; }
        HField* h() { return h_; }
        domain::Domain* d() { return &d_; }
        source::Sources& sources() { return sources_; }
        NodeManager* processingMgr() { return processingMgr_; }
        seq::Sequencers& sequencers() { return sequencers_; }
        AdmittanceCatalogue& admittanceCat() { return admittanceCat_; }
        [[nodiscard]] const std::string& fileName() const { return fileName_; }
        [[nodiscard]] const std::string& pathName() const { return pathName_; }
        sensor::Sensors& sensors() { return sensors_; }
        sensor::Analysers& analysers() { return analysers_; }
        Variables& variables() { return variables_; }
        designer::Designers& designers() { return designers_; }
    };
}

#endif //FDTDLIFE_MODEL_H
