/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "cuda_runtime.h"

cudaError_t kernelFdtdStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           double* ex, double* ey, double* ez,
                           double* hx, double* hy, double* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           double* drX, double* drY, double* drZ,
                           double* caX, double* cbX,
                           double* caY, double* cbY,
                           double* caZ, double* cbZ,
                           double* thinExtraZ, double* caThin, double* cbThin);
cudaError_t kernelCpmlEFieldXLeft(int startX, int startY, int startZ,
                                  int endX, int endY, int endZ,
                                  int arraySizeX, int arraySizeY, int arraySizeZ,
                                  double* ex, double* ey, double* ez,
                                  double* hx, double* hy, double* hz,
                                  char* material, int boundarySize,
                                  char* boundaryDepthX, char* boundaryDepthY,
                                  char* boundaryDepthZ,
                                  double* yxPsiL, double* zxPsiL,
                                  double* cpmlBX, double* cpmlCX,
                                  double* caY, double* caZ,
                                  int psiWidth, double dx);
cudaError_t kernelCpmlEFieldXRight(int startX, int startY, int startZ,
                                   int endX, int endY, int endZ,
                                   int arraySizeX, int arraySizeY, int arraySizeZ,
                                   double* ex, double* ey, double* ez,
                                   double* hx, double* hy, double* hz,
                                   char* material, int boundarySize,
                                   char* boundaryDepthX, char* boundaryDepthY,
                                   char* boundaryDepthZ,
                                   double* yxPsiR, double* zxPsiR,
                                   double* cpmlBX, double* cpmlCX,
                                   double* caY, double* caZ,
                                   int psiWidth, double dx, int nx);
