/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaProxy.h"
#include "CudaException.h"
#include "CudaIndexing.h"
#include <sstream>
#include <iostream>

#if defined(USE_CUDA)
#include "Kernel.cuh"
// A macro that makes error checking easier
#define ERRCHK(code) cudaAssert((code), __FILE__, __LINE__, #code)


// A function that checks a CUDA return code and throws an exception
// when an error occurs
inline void cudaAssert(cudaError_t code, const char* file, int line, const char* callText) {
    //std::cout << callText << "| " << file << ":" << line << " Cuda result=" << cudaGetErrorString(code) << std::endl;
    if (code != cudaSuccess) {
        std::stringstream text;
        text << cudaGetErrorString(code) << " at " << file << ":" << line;
        throw CudaException(text.str());
    }
}   
#endif

// Constructor
CudaProxy::CudaProxy() :
        numGpus_(0) {
#if defined(USE_CUDA)
    // What CUDA software do we have?
    ERRCHK(cudaDriverGetVersion(&driverVersion_));
    ERRCHK(cudaRuntimeGetVersion(&runtimeVersion_));
    std::cout << "CUDA version=" << (double)runtimeVersion_ / 1000.0
              << ", driver version=" << (double)driverVersion_ / 1000.0 << std::endl;
    // Discover what kinds of GPUs we have
    ERRCHK(cudaGetDeviceCount(&numGpus_));
    gpuProps_.resize((size_t)numGpus_);
    for(int device=0; device<numGpus_; device++) {
        // Device properties
        struct cudaDeviceProp props;
        ERRCHK(cudaGetDeviceProperties(&props, device));
        gpuProps_[device] = {
            props.clockRate, (ComputeMode)props.computeMode,
            props.major, props.minor, 
            props.name, 0, 0, 
            props.multiProcessorCount, props.maxThreadsPerBlock,
            props.maxThreadsPerMultiProcessor
        };
        // The memory each device has
        ERRCHK(cudaSetDevice(device));
        ERRCHK(cudaMemGetInfo(&gpuProps_[device].freeMemory_, 
                              &gpuProps_[device].totalMemory_));
        std::cout << "[" << device << "]" << props.name 
                  << ": clock=" << (double)props.clockRate/1000000.0 
                  << "GHz, computeMode=" << props.computeMode 
                  << ", capability=" << props.major << "." << props.minor 
                  << ", totalMemory=" << (double)gpuProps_[device].totalMemory_ / 1000000.0 
                  << "M, freeMemory=" << (double)gpuProps_[device].freeMemory_ / 1000000.0
                  << "M, multiProcessors=" << props.multiProcessorCount << std::endl;
    }
    // Select a GPU to use
    ERRCHK(cudaSetDevice(0));
#endif
    std::cout << "CUDA proxy initialised" << std::endl;
}

// Desructor
CudaProxy::~CudaProxy() {

}

// Allocate CUDA memory
void CudaProxy::malloc(void** memory, size_t size) {
#if defined(USE_CUDA)
    ERRCHK(cudaMalloc(memory, size));
#else
    *memory = ::malloc(size);
#endif
}

// Free cuda memory
void CudaProxy::free(void* memory) {
#if defined(USE_CUDA)
    ERRCHK(cudaFree(memory));
#else
    ::free(memory);
#endif
}

// Copy data between host and GPU
void CudaProxy::memcpy(void* destination, void* source, size_t count, bool toHost) {
#if defined(USE_CUDA)
    ERRCHK(cudaMemcpy(destination, source, count, toHost ? cudaMemcpyDeviceToHost : cudaMemcpyHostToDevice));
#else
    ::memcpy(destination, source, count);
#endif
}

// Call the FDTD electric step function
void CudaProxy::fdtdEFieldStep(int startX, int startY, int startZ,
                               int endX, int endY, int endZ,
                               int nX, int nY, int nZ,
                               CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                               CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                               CudaMemory<char>& material, int boundarySize,
                               CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                               CudaMemory<char>& boundaryDepthZ,
                               CudaMemory<double>& drX, CudaMemory<double>& drY, CudaMemory<double>& drZ,
                               CudaMemory<double>& caX, CudaMemory<double>& cbX,
                               CudaMemory<double>& caY, CudaMemory<double>& cbY,
                               CudaMemory<double>& caZ, CudaMemory<double>& cbZ,
                               CudaMemory<double>& thinExtraZ, CudaMemory<double>& caThin, CudaMemory<double>& cbThin) {
#if defined(USE_CUDA)
    ERRCHK(kernelFdtdStep(startX, startY, startZ,
                               endX, endY, endZ,
                               nX, nY, nZ,
                               ex.gpu(), ey.gpu(), ez.gpu(),
                               hx.gpu(), hy.gpu(), hz.gpu(),
                               material.gpu(), boundarySize,
                               boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                               boundaryDepthZ.gpu(),
                               drX.gpu(), drY.gpu(), drZ.gpu(),
                               caX.gpu(), cbX.gpu(),
                               caY.gpu(), cbY.gpu(),
                               caZ.gpu(), cbZ.gpu(),
                               thinExtraZ.gpu(), caThin.gpu(), cbThin.gpu()));
#else
    int i, j, k;
    // Step the grid
    for(k = startZ; k < endZ; k++) {
        for(j = startY; j < endY; j++) {
            for(i = startX; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, nX, nY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx], boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx], boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx], boundaryDepthZ.host()[idx], boundarySize);
                double drx = drX.host()[matIdxX];
                double dry = drY.host()[matIdxY];
                double drz = drZ.host()[matIdxZ];
                double dhxj = hx.host()[CUDAINDEX(i, j + 1, k, nX, nY)] - hx.host()[idx];
                double dhxk = hx.host()[CUDAINDEX(i, j, k + 1, nX, nY)] - hx.host()[idx];
                double dhyi = hy.host()[CUDAINDEX(i + 1, j, k, nX, nY)] - hy.host()[idx];
                double dhyk = hy.host()[CUDAINDEX(i, j, k + 1, nX, nY)] - hy.host()[idx];
                double dhzj = hz.host()[CUDAINDEX(i, j + 1, k, nX, nY)] - hz.host()[idx];
                double dhzi = hz.host()[CUDAINDEX(i + 1, j, k, nX, nY)] - hz.host()[idx];
                ex.host()[idx] =
                        (dhzj / dry - dhyk / drz) * caX.host()[matIdxX] +
                        ex.host()[idx] * cbX.host()[matIdxX];
                ey.host()[idx] =
                        (dhxk / drz - dhzi / drx) * caY.host()[matIdxY] +
                        ey.host()[idx] * cbY.host()[matIdxY];
                ez.host()[idx] =
                        (dhyi / drx - dhxj / dry) * caZ.host()[matIdxZ] +
                        ez.host()[idx] * cbZ.host()[matIdxZ];
                thinExtraZ.host()[idx] =
                        (dhyi / drx - dhxj / dry) * caThin.host()[matIdxZ] +
                        thinExtraZ.host()[idx] * cbThin.host()[matIdxZ];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the X left boundary
void CudaProxy::cpmlEFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<double>& yxPsiL, CudaMemory<double>& zxPsiL,
                                CudaMemory<double>& cpmlBX, CudaMemory<double>& cpmlCX,
                                CudaMemory<double>& caY, CudaMemory<double>& caZ,
                                int psiWidth, double dx) {
#if defined(USE_CUDA)
    ERRCHK(kernelCpmlEFieldXLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiL.gpu(), zxPsiL.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx));
#else
    for(int k = startZ; k < endZ; k++) {
        for(int j = startY; j < endY; j++) {
            for(int i = startX; i < boundarySize; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthZ.host()[idx], boundarySize);
                double dhyi = hy.host()[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy.host()[idx];
                double dhzi = hz.host()[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz.host()[idx];
                int psiIndex = CUDAINDEXPSI(i, j, k, psiWidth);
                yxPsiL.host()[psiIndex] = cpmlBX.host()[matIdxX] * yxPsiL.host()[psiIndex] +
                                          cpmlCX.host()[matIdxX] * dhzi / dx;
                ey.host()[idx] -= yxPsiL.host()[psiIndex] * caY.host()[matIdxY];
                zxPsiL.host()[psiIndex] = cpmlBX.host()[matIdxX] * zxPsiL.host()[psiIndex] +
                                          cpmlCX.host()[matIdxX] * dhyi / dx;
                ez.host()[idx] += zxPsiL.host()[psiIndex] * caZ.host()[matIdxZ];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the X right boundary
void CudaProxy::cpmlEFieldXRight(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<double>& yxPsiR, CudaMemory<double>& zxPsiR,
                                CudaMemory<double>& cpmlBX, CudaMemory<double>& cpmlCX,
                                CudaMemory<double>& caY, CudaMemory<double>& caZ,
                                int psiWidth, double dx, int nx) {
#if defined(USE_CUDA)
    ERRCHK(kernelCpmlEFieldXRight(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                yxPsiR.gpu(), zxPsiR.gpu(),
                                cpmlBX.gpu(), cpmlCX.gpu(),
                                caY.gpu(), caZ.gpu(),
                                psiWidth, dx, nx));
#else
    for(int k = startZ; k < endZ; k++) {
        for(int j = startY; j < endY; j++) {
            for(int i = nx - boundarySize; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthZ.host()[idx], boundarySize);
                double dhyi = hy.host()[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy.host()[idx];
                double dhzi = hz.host()[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz.host()[idx];
                int psiIndex = CUDAINDEXPSI((nx - i - 1 + boundarySize), j, k, psiWidth);
                yxPsiR.host()[psiIndex] = cpmlBX.host()[matIdxX] * yxPsiR.host()[psiIndex] +
                                          cpmlCX.host()[matIdxX] * dhzi / dx;
                ey.host()[idx] -= yxPsiR.host()[psiIndex] * caY.host()[matIdxY];
                zxPsiR.host()[psiIndex] = cpmlBX.host()[matIdxX] * zxPsiR.host()[psiIndex] +
                                          cpmlCX.host()[matIdxX] * dhyi / dx;
                ez.host()[idx] += zxPsiR.host()[psiIndex] * caZ.host()[matIdxZ];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the Y left boundary
void CudaProxy::cpmlEFieldYLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<double>& xyPsiL, CudaMemory<double>& zyPsiL,
                                CudaMemory<double>& cpmlBY, CudaMemory<double>& cpmlCY,
                                CudaMemory<double>& caX, CudaMemory<double>& caZ,
                                int psiWidth, double dy) {
#if 0
    ERRCHK(kernelCpmlEFieldYLeft(startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xyPsiL.gpu(), zyPsiL.gpu(),
                                cpmlBY.gpu(), cpmlCY.gpu(),
                                caX.gpu(), caZ.gpu(),
                                psiWidth, dy));
#else
    for(int k = startZ; k < endZ; k++) {
        for(int j = startY; j < boundarySize; j++) {
            for(int i = startX; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthZ.host()[idx], boundarySize);
                double dhxj = hx.host()[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hx.host()[idx];
                double dhzj = hz.host()[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hz.host()[idx];
                int psiIndex = CUDAINDEXPSI(i, j, k, psiWidth);
                xyPsiL.host()[psiIndex] = cpmlBY.host()[matIdxY] * xyPsiL.host()[psiIndex] +
                                          cpmlCY.host()[matIdxY] * dhzj / dy;
                ex.host()[idx] += xyPsiL.host()[psiIndex] * caX.host()[matIdxX];
                zyPsiL.host()[psiIndex] = cpmlBY.host()[matIdxY] * zyPsiL.host()[psiIndex] +
                                          cpmlCY.host()[matIdxY] * dhxj / dy;
                ez.host()[idx] -= zyPsiL.host()[psiIndex] * caZ.host()[matIdxZ];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the Y right boundary
void CudaProxy::cpmlEFieldYRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                 CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<double>& xyPsiR, CudaMemory<double>& zyPsiR,
                                 CudaMemory<double>& cpmlBY, CudaMemory<double>& cpmlCY,
                                 CudaMemory<double>& caX, CudaMemory<double>& caZ,
                                 int psiWidth, double dy, int ny) {
#if 0
    ERRCHK(kernelcpmlEFieldYRight(startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xyPsiR.gpu(), zyPsiR.gpu(),
                                 cpmlBY.gpu(), cpmlCY.gpu(),
                                 caX.gpu(), caZ.gpu(),
                                 psiWidth, dy, ny));
#else
    for(int k = startZ; k < endZ; k++) {
        for(int j = ny - boundarySize; j < endY; j++) {
            for(int i = startX; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthZ.host()[idx], boundarySize);
                double dhxj = hx.host()[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hx.host()[idx];
                double dhzj = hz.host()[CUDAINDEX(i, j + 1, k, arraySizeX, arraySizeY)] - hz.host()[idx];
                int psiIndex = CUDAINDEXPSI(i, (ny - j - 1 + boundarySize), k, psiWidth);
                xyPsiR.host()[psiIndex] = cpmlBY.host()[matIdxY] * xyPsiR.host()[psiIndex] +
                                          cpmlCY.host()[matIdxY] * dhzj / dy;
                ex.host()[idx] += xyPsiR.host()[psiIndex] * caX.host()[matIdxX];
                zyPsiR.host()[psiIndex] = cpmlBY.host()[matIdxY] * zyPsiR.host()[psiIndex] +
                                          cpmlCY.host()[matIdxY] * dhxj / dy;
                ez.host()[idx] -= zyPsiR.host()[psiIndex] * caZ.host()[matIdxZ];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the Z left boundary
void CudaProxy::cpmlEFieldZLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                CudaMemory<char>& material, int boundarySize,
                                CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                CudaMemory<char>& boundaryDepthZ,
                                CudaMemory<double>& xzPsiL, CudaMemory<double>& yzPsiL,
                                CudaMemory<double>& cpmlBZ, CudaMemory<double>& cpmlCZ,
                                CudaMemory<double>& caX, CudaMemory<double>& caY,
                                int psiWidth, double dz) {
#if 0
    ERRCHK(kernelCpmlEFieldZLeft(
            startX, startY, startZ,
                                endX, endY, endZ,
                                arraySizeX, arraySizeY, arraySizeZ,
                                ex.gpu(), ey.gpu(), ez.gpu(),
                                hx.gpu(), hy.gpu(), hz.gpu(),
                                material.gpu(), boundarySize,
                                boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                boundaryDepthZ.gpu(),
                                xzPsiL.gpu(), yzPsiL.gpu(),
                                cpmlBZ.gpu(), cpmlCZ.gpu(),
                                caX.gpu(), caY.gpu(),
                                psiWidth, dz));
#else
    for(int k = startZ; k < boundarySize; k++) {
        for(int j = startY; j < endY; j++) {
            for(int i = startX; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                boundaryDepthZ.host()[idx], boundarySize);
                double dhxk = hx.host()[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hx.host()[idx];
                double dhyk = hy.host()[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hy.host()[idx];
                int psiIndex = CUDAINDEXPSI(i, j, k, psiWidth);
                xzPsiL.host()[psiIndex] = cpmlBZ.host()[matIdxZ] * xzPsiL.host()[psiIndex] +
                                         cpmlCZ.host()[matIdxZ] * dhyk / dz;
                ex.host()[idx] -= xzPsiL.host()[psiIndex] * caX.host()[matIdxX];
                yzPsiL.host()[psiIndex] = cpmlBZ.host()[matIdxZ] * yzPsiL.host()[psiIndex] +
                                         cpmlCZ.host()[matIdxZ] * dhxk / dz;
                ey.host()[idx] += yzPsiL.host()[psiIndex] * caY.host()[matIdxY];
            }
        }
    }
#endif
}

// Call the E Field CPML correction for the Z right boundary
void CudaProxy::cpmlEFieldZRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                                 CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                                 CudaMemory<char>& material, int boundarySize,
                                 CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                                 CudaMemory<char>& boundaryDepthZ,
                                 CudaMemory<double>& xzPsiR, CudaMemory<double>& yzPsiR,
                                 CudaMemory<double>& cpmlBZ, CudaMemory<double>& cpmlCZ,
                                 CudaMemory<double>& caX, CudaMemory<double>& caY,
                                 int psiWidth, double dz, int nz) {
#if 0
    ERRCHK(kernelCpmlEFieldZRight(
            startX, startY, startZ,
                                 endX, endY, endZ,
                                 arraySizeX, arraySizeY, arraySizeZ,
                                 ex.gpu(), ey.gpu(), ez.gpu(),
                                 hx.gpu(), hy.gpu(), hz.gpu(),
                                 material.gpu(), boundarySize,
                                 boundaryDepthX.gpu(), boundaryDepthY.gpu(),
                                 boundaryDepthZ.gpu(),
                                 xzPsiR.gpu(), yzPsiR.gpu(),
                                 cpmlBZ.gpu(), cpmlCZ.gpu(),
                                 caX.gpu(), caY.gpu(),
                                 psiWidth, dz, nz));
#else
    for(int k = nz - boundarySize; k < endZ; k++) {
        for(int j = startY; j < endY; j++) {
            for(int i = startX; i < endX; i++) {
                int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
                int matIdxX = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthX.host()[idx], boundarySize);
                int matIdxY = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthY.host()[idx], boundarySize);
                int matIdxZ = CUDAINDEXMATDEPTH(material.host()[idx],
                                                     boundaryDepthZ.host()[idx], boundarySize);
                double dhxk = hx.host()[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hx.host()[idx];
                double dhyk = hy.host()[CUDAINDEX(i, j, k + 1, arraySizeX, arraySizeY)] - hy.host()[idx];
                int psiIndex = CUDAINDEXPSI(i, j, (nz - k - 1 + boundarySize), psiWidth);
                xzPsiR.host()[psiIndex] = cpmlBZ.host()[matIdxZ] * xzPsiR.host()[psiIndex] +
                                          cpmlCZ.host()[matIdxZ] * dhyk / dz;
                ex.host()[idx] -= xzPsiR.host()[psiIndex] * caX.host()[matIdxX];
                yzPsiR.host()[psiIndex] = cpmlBZ.host()[matIdxZ] * yzPsiR.host()[psiIndex] +
                                          cpmlCZ.host()[matIdxZ] * dhxk / dz;
                ey.host()[idx] += yzPsiR.host()[psiIndex] * caY.host()[matIdxY];
            }
        }
    }
#endif
}
