/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDAPROXY_H
#define FDTDLIFE_CUDAPROXY_H

#include <stddef.h>
#include <string>
#include <vector>
#include "CudaMemory.h"

// This class acts as the interface to the CUDA engine
class CudaProxy {
public:
    // Construction
    CudaProxy();
    ~CudaProxy();

    // Capabilities information
    int numGpus_;
    enum ComputeMode {computeModeDefault, computeModeExclusive, computeModeProhibited};
    struct GpuProps {
        int clockRateKhz_;
        ComputeMode computeMode_;
        int majorCapability_;
        int minorCapability_;
        std::string name_;
        size_t totalMemory_;
        size_t freeMemory_;
        int numMultiprocessors_;
        int maxThreadsPerBlock_;
        int maxThreadsPerMultiprocessor_;
    };
    std::vector<GpuProps> gpuProps_;
    int driverVersion_;
    int runtimeVersion_;

    // API to the wrapped CUDA library
    void malloc(void** memory, size_t size);
    void free(void* memory);
    void memcpy(void* destination, void* source, size_t count, bool toHost);

    // API to the kernels
    void fdtdEFieldStep(int startX, int startY, int startZ,
                        int endX, int endY, int endZ,
                        int nX, int nY, int nZ,
                        CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                        CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                        CudaMemory<char>& material, int boundarySize,
                        CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                        CudaMemory<char>& boundaryDepthZ,
                        CudaMemory<double>& drX, CudaMemory<double>& drY, CudaMemory<double>& drZ,
                        CudaMemory<double>& caX, CudaMemory<double>& cbX,
                        CudaMemory<double>& caY, CudaMemory<double>& cbY,
                        CudaMemory<double>& caZ, CudaMemory<double>& cbZ,
                        CudaMemory<double>& thinExtraZ, CudaMemory<double>& caThin, CudaMemory<double>& cbThin);
    void cpmlEFieldXLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                         CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<double>& yxPsiL, CudaMemory<double>& zxPsiL,
                         CudaMemory<double>& cpmlBX, CudaMemory<double>& cpmlCX,
                         CudaMemory<double>& caY, CudaMemory<double>& caZ,
                         int psiWidth, double dx);
    void cpmlEFieldXRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                          CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<double>& yxPsiR, CudaMemory<double>& zxPsiR,
                          CudaMemory<double>& cpmlBX, CudaMemory<double>& cpmlCX,
                          CudaMemory<double>& caY, CudaMemory<double>& caZ,
                          int psiWidth, double dx, int nx);
    void cpmlEFieldYLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                         CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<double>& xyPsiL, CudaMemory<double>& zyPsiL,
                         CudaMemory<double>& cpmlBY, CudaMemory<double>& cpmlCY,
                         CudaMemory<double>& caX, CudaMemory<double>& caZ,
                         int psiWidth, double dy);
    void cpmlEFieldYRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                          CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<double>& xyPsiR, CudaMemory<double>& zyPsiR,
                          CudaMemory<double>& cpmlBY, CudaMemory<double>& cpmlCY,
                          CudaMemory<double>& caX, CudaMemory<double>& caZ,
                          int psiWidth, double dy, int ny);
    void cpmlEFieldZLeft(int startX, int startY, int startZ,
                         int endX, int endY, int endZ,
                         int arraySizeX, int arraySizeY, int arraySizeZ,
                         CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                         CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                         CudaMemory<char>& material, int boundarySize,
                         CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                         CudaMemory<char>& boundaryDepthZ,
                         CudaMemory<double>& xzPsiL, CudaMemory<double>& yzPsiL,
                         CudaMemory<double>& cpmlBZ, CudaMemory<double>& cpmlCZ,
                         CudaMemory<double>& caX, CudaMemory<double>& caY,
                         int psiWidth, double dz);
    void cpmlEFieldZRight(int startX, int startY, int startZ,
                          int endX, int endY, int endZ,
                          int arraySizeX, int arraySizeY, int arraySizeZ,
                          CudaMemory<double>& ex, CudaMemory<double>& ey, CudaMemory<double>& ez,
                          CudaMemory<double>& hx, CudaMemory<double>& hy, CudaMemory<double>& hz,
                          CudaMemory<char>& material, int boundarySize,
                          CudaMemory<char>& boundaryDepthX, CudaMemory<char>& boundaryDepthY,
                          CudaMemory<char>& boundaryDepthZ,
                          CudaMemory<double>& xzPsiR, CudaMemory<double>& yzPsiR,
                          CudaMemory<double>& cpmlBZ, CudaMemory<double>& cpmlCZ,
                          CudaMemory<double>& caX, CudaMemory<double>& caY,
                          int psiWidth, double dz, int nz);
};


#endif //FDTDLIFE_CUDAPROXY_H
