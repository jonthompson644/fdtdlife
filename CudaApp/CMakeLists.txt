cmake_minimum_required(VERSION 3.8)
project(FdTdCudaApp)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmakemodules")
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(SOURCE_FILES main.cpp FdTdCuda.cpp FdTdCuda.h CudaCpmlField.cpp CudaCpmlField.h CudaIncludes.h ECudaCpml.cpp ECudaCpml.h HCudaCpml.cpp HCudaCpml.h CudaCommon.cpp CudaCommon.h CudaException.cpp CudaException.h CudaMemory.h CudaProxy.cpp CudaProxy.h CudaIndexing.h CudaMemory.cpp)

add_executable(FdTdCudaApp ${SOURCE_FILES})
target_link_libraries(FdTdCudaApp FdTdLibrary)
target_link_libraries(FdTdCudaApp BoxLibrary)
target_link_libraries(FdTdCudaApp SourceLibrary)
target_link_libraries(FdTdCudaApp SensorLibrary)
target_link_libraries(FdTdCudaApp XmlLibrary)
target_link_libraries(FdTdCudaApp GaLibrary)

if(DEFINED CMAKE_USE_OPEN_MP)
    target_link_libraries(FdTdCudaApp /usr/local/opt/llvm/lib/libc++.dylib)
    target_link_libraries(FdTdCudaApp /usr/local/opt/llvm/lib/libgomp.dylib)
endif(DEFINED CMAKE_USE_OPEN_MP)
