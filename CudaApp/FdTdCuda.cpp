/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FdTdCuda.h"
#include <Fdtd/Individual.h>
#include <Fdtd/GeneticSearch.h>
#include <Fdtd/Population.h>
#include <Fdtd/NodeManager.h>
#include "ECudaCpml.h"
#include "HCudaCpml.h"
#include "CudaCommon.h"

// Constructor
// The parameter is the pathname of the configuration file without the .fdtd on the end
FdTdCuda::FdTdCuda(const std::string& filePath, const std::string& fileName) :
        Notifiable(this),
        going_(false),
        filePath_(filePath),
        fileName_(fileName),
        common_(nullptr) {
    // Load the file
    readConfigFile(filePath_.c_str(), fileName_.c_str());
    // Run the model
    p_.initialise();  // To calculate the number of cores being used
    std::cout << "Running " << filePath_ << fileName_ << ".fdtd using "
              << p()->numThreadsUsed() << " cores" << std::endl;
    run();
}

// Destructor
FdTdCuda::~FdTdCuda() {
    delete common_;
}

// Run the model
void FdTdCuda::run() {
    // Run the model
    lastTime_ = std::chrono::steady_clock::now();
    going_ = start();
    while(going_) {
        step();
    }
    // Store the results
    writeData(filePath_ + fileName_ + ".data");
}

// Handle a notification
void FdTdCuda::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* /*data*/) {
    if(source == processingMgr_) {
        switch(why) {
            case fdtd::NodeManager::notifyAllJobsComplete:
                std::cout << "All jobs complete" << std::endl;
                going_ = false;
                break;
            default:
                break;
        }
    } else {
        auto* geneticSearch = dynamic_cast<fdtd::GeneticSearch*>(source);
        if(geneticSearch != nullptr) {
            switch(why) {
                case fdtd::GeneticSearch::notifyJobComplete:
                    geneticIndividualComplete(geneticSearch);
                    break;
                case fdtd::GeneticSearch::notifyGenerationComplete:
                    geneticGenerationComplete(geneticSearch);
                    break;
                default:
                    break;
            }
        } else {
            auto* model = dynamic_cast<fdtd::Model*>(source);
            if(model != nullptr) {
                switch(why) {
                    case fdtd::Model::notifySteppedE:
                        //model->e()->print();
                        break;
                    case fdtd::Model::notifySteppedH:
                        //model->h()->print();
                        break;
                }
            }
        }
    }
}

// A genetic model iteration has completed
void FdTdCuda::geneticIndividualComplete(fdtd::GeneticSearch* geneticSearch) {
    // The elapsed time
    auto thisTime = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = thisTime - lastTime_;
    lastTime_ = thisTime;
    // Calculate the cell rate
    int numCells = p_.n().x() * p_.n().y() * p_.n().z();
    int totalCellsCalculated = numCells * p_.nt();
    double cellRate = (double) totalCellsCalculated / elapsed.count() / 1000.0;
    // Print iteration information
    auto current = geneticSearch->population()->getIndividual(
            geneticSearch->population()->currentIndividual());
    auto elite = geneticSearch->population()->getIndividual(
            geneticSearch->population()->eliteIndividual());
    std::cout << "Iteration complete:";
    if(current != nullptr) {
        std::cout << " individual=" << current->id() << " unfitness=" << current->unfitness()
                  << " kcells/s=" << cellRate;
    }
    if(elite != nullptr) {
        std::cout << " elite=" << elite->id() << " unfitness=" << elite->unfitness();
    }
    std::cout << std::endl;
}

// A genetic model generation has completed
void FdTdCuda::geneticGenerationComplete(fdtd::GeneticSearch* geneticSearch) {
    std::cout << "Generation " << geneticSearch->generationNumber() << " complete:" << std::endl;
    for(auto& ind : geneticSearch->population()->individuals()) {
        std::cout << "    Individual=" << ind->id() << " unfitness=" << ind->unfitness() << std::endl;
    }
}

// Create the field objects that do most of the work
void FdTdCuda::createFields() {
    // Is the configuration such that we can create CUDA accelerated fields
    switch(p()->absorbing()) {
        case fdtd::Configuration::absorbingNormal:
        case fdtd::Configuration::absorbingBerengerSplitField:
            Model::createFields();
            break;
        case fdtd::Configuration::absorbingCpml:
            delete common_;
            common_ = new CudaCommon(this);
            e_ = new ECudaCpml(this, common_);
            h_ = new HCudaCpml(this, common_);
            break;
    }
}




