/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ECUDACPML_H
#define FDTDLIFE_ECUDACPML_H

#include <Fdtd/EField.h>
#include "CudaCpmlField.h"

namespace fdtd { class Model; }

// A class that calculates the electric field using a total
// field representation and a simple normally absorbing boundary
class ECudaCpml : public fdtd::EField, public CudaCpmlField {
public:
    // Construction
    ECudaCpml(fdtd::Model* m, CudaCommon* common);

    // Overrides of fdtd::EField
    void initialise() override;
    box::Vector<double> value(const box::Vector<size_t>& cell) override;
    void print() override;
    size_t memoryUse() override;
    bool writeData(std::ofstream& file) override { return CudaCpmlField::writeData(file); }
    bool readData(std::ifstream& file) override { return CudaCpmlField::readData(file); }
    void step(const box::ThreadInfo* info) override;

protected:
    // Helper functions
    void stepField(const box::ThreadInfo* info);
    void initialiseConstantArrays();
    void initialiseStartEnd();

public:
    // Thin layer constants indexed by material identifier
    CudaMemory<double> caThin_;  // The A constant inside a thin material (0 for none)
    CudaMemory<double> cbThin_;  // The B constant inside a thin material (0 for none)
    // Extra FDTD grid memory indexed by x,y,z
    CudaMemory<double> thinExtraZ_;  // The extra Z component for the thin layer model
};

#endif //FDTDLIFE_ECUDACPML_H
