/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ECudaCpml.h"
#include <Fdtd/Model.h>
#include <Domain/Material.h>
#include <Domain/BoundaryMaterial.h>
#include "HCudaCpml.h"
#include "CudaIncludes.h"
#include "CudaCommon.h"
#include "CudaIndexing.h"
#include <Domain/CpmlMaterial.h>
#include <Domain/ThinSheetMaterial.h>
#include <Source/Sources.h>
#include <algorithm>

// Constructor
ECudaCpml::ECudaCpml(fdtd::Model* m, CudaCommon* common) :
        EField(m),
        CudaCpmlField(m->p(), common),
        caThin_(&common->cudaProxy_),
        cbThin_(&common->cudaProxy_),
        thinExtraZ_(&common->cudaProxy_) {
}

// Initialise the model
void ECudaCpml::initialise() {
    EField::initialise();
    common_->initialise();  // Initialise the common with the E field as this is first
    CudaCpmlField::initialise();
    // Memory for the thin layer constants
    caThin_.alloc(common_->matDepthArraySize_);
    cbThin_.alloc(common_->matDepthArraySize_);
    thinExtraZ_.alloc(arraySize_);
    // Now generate the constants
    initialiseConstantArrays();
    // And the zone to calculate
    initialiseStartEnd();
}

// Print the data
void ECudaCpml::print() {
    CudaCpmlField::print("E Field");
}

// Return the amount of memory used
size_t ECudaCpml::memoryUse() {
    return CudaCpmlField::memoryUse();
}

// Return a value from the model
box::Vector<double> ECudaCpml::value(const box::Vector<size_t>& cell) {
    return CudaCpmlField::value(cell);
}

// Calculate the main model update
void ECudaCpml::stepField(const box::ThreadInfo* /*info*/) {
    auto* h = dynamic_cast<HCudaCpml*>(m_->h());
    if(h != nullptr) {
        // Step the grid
        common_->cudaProxy_.fdtdEFieldStep(
                start_.x(), start_.y(), start_.z(),
                end_.x(), end_.y(), end_.z(),
                p_->nx(), p_->ny(), p_->nz(),
                x_, y_, z_,
                h->x_, h->y_, h->z_,
                common_->material_, m_->p()->boundarySize(),
                common_->boundaryDepthX_, common_->boundaryDepthY_, common_->boundaryDepthZ_,
                common_->drX_, common_->drY_, common_->drZ_,
                caX_, cbX_,
                caY_, cbY_,
                caZ_, cbZ_,
                thinExtraZ_, caThin_, cbThin_);
        // Now the CPML boundary modifications (note uses non-stretched coordinate values)
        // The X CPML modifications
        if(p_->boundaryX() == fdtd::Configuration::Boundary::absorbing) {
            common_->cudaProxy_.cpmlEFieldXLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    yxPsiL_, zxPsiL_,
                    cpmlBX_, cpmlCX_,
                    caY_, caZ_,
                    psiWidth_, p_->dr().x());
            common_->cudaProxy_.cpmlEFieldXRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    yxPsiR_, zxPsiR_,
                    cpmlBX_, cpmlCX_,
                    caY_, caZ_,
                    psiWidth_, p_->dr().x(), p_->n().x());
        }
        // The Y CPML modifications
        if(p_->boundaryY() == fdtd::Configuration::Boundary::absorbing) {
            common_->cudaProxy_.cpmlEFieldYLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xyPsiL_, zyPsiL_,
                    cpmlBY_, cpmlCY_,
                    caX_, caZ_,
                    psiWidth_, p_->dr().y());
            common_->cudaProxy_.cpmlEFieldYRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xyPsiR_, zyPsiR_,
                    cpmlBY_, cpmlCY_,
                    caX_, caZ_,
                    psiWidth_, p_->dr().y(), p_->n().y());

        }
        // The Z CPML modifications
        if(p_->boundaryZ() == fdtd::Configuration::Boundary::absorbing) {
            common_->cudaProxy_.cpmlEFieldZLeft(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xzPsiL_, yzPsiL_,
                    cpmlBZ_, cpmlCZ_,
                    caX_, caY_,
                    psiWidth_, p_->dr().z());
            common_->cudaProxy_.cpmlEFieldZRight(
                    start_.x(), start_.y(), start_.z(),
                    end_.x(), end_.y(), end_.z(),
                    p_->nx(), p_->ny(), p_->nz(),
                    x_, y_, z_,
                    h->x_, h->y_, h->z_,
                    common_->material_, m_->p()->boundarySize(),
                    common_->boundaryDepthX_, common_->boundaryDepthY_,
                    common_->boundaryDepthZ_,
                    xzPsiR_, yzPsiR_,
                    cpmlBZ_, cpmlCZ_,
                    caX_, caY_,
                    psiWidth_, p_->dr().z(), p_->n().z());
        }
    }
}

// Fill the constant arrays
void ECudaCpml::initialiseConstantArrays() {
    int i, j, k;
    fdtd::Configuration* p = m_->p();
    domain::Domain* d = m_->d();
    // Zero the thin plate extra Z
    for(k = 0; k < p->n().z(); k++) {
        for(j = 0; j < p->n().y(); j++) {
            for(i = 0; i < p->n().x(); i++) {
                int idx = p->index(i, j, k);
                thinExtraZ_.host()[idx] = 0.0;
            }
        }
    }
    // Fill the material constant tables
    for(auto& pos : d->materialLookup()) {
        domain::Material* mat = pos.second.get();
        for(int boundaryDepth = -1; boundaryDepth <= p->boundarySize(); boundaryDepth++) {
            int idx = common_->matDepthIndex(mat->index(), boundaryDepth);
            auto thinMat = dynamic_cast<domain::ThinSheetMaterial*>(mat);
            if(m_->p()->useThinSheetSubcells() && thinMat != nullptr) {
                caX_.host()[idx] = thinMat->caeav();
                caY_.host()[idx] = thinMat->caeav();
                caZ_.host()[idx] = mat->cae(domain::Material::Direction::z, boundaryDepth);
                caThin_.host()[idx] = thinMat->caein();
                cbX_.host()[idx] = thinMat->cbeav();
                cbY_.host()[idx] = thinMat->cbeav();
                cbZ_.host()[idx] = mat->cbe(domain::Material::Direction::z, boundaryDepth);
                cbThin_.host()[idx] = thinMat->cbein();
            } else {
                caX_.host()[idx] = mat->cae(domain::Material::Direction::x, boundaryDepth);
                caY_.host()[idx] = mat->cae(domain::Material::Direction::y, boundaryDepth);
                caZ_.host()[idx] = mat->cae(domain::Material::Direction::z, boundaryDepth);
                caThin_.host()[idx] = 0.0;
                cbX_.host()[idx] = mat->cbe(domain::Material::Direction::x, boundaryDepth);
                cbY_.host()[idx] = mat->cbe(domain::Material::Direction::y, boundaryDepth);
                cbZ_.host()[idx] = mat->cbe(domain::Material::Direction::z, boundaryDepth);
                cbThin_.host()[idx] = 0.0;
            }
            auto boundaryMat = dynamic_cast<domain::CpmlMaterial*>(mat);
            if(boundaryMat != nullptr) {
                cpmlCX_.host()[idx] = boundaryMat->c(domain::Material::Direction::x, boundaryDepth);
                cpmlBX_.host()[idx] = boundaryMat->b(domain::Material::Direction::x, boundaryDepth);
                cpmlBY_.host()[idx] = boundaryMat->b(domain::Material::Direction::y, boundaryDepth);
                cpmlCY_.host()[idx] = boundaryMat->c(domain::Material::Direction::y, boundaryDepth);
                cpmlBZ_.host()[idx] = boundaryMat->b(domain::Material::Direction::z, boundaryDepth);
                cpmlCZ_.host()[idx] = boundaryMat->c(domain::Material::Direction::z, boundaryDepth);
            } else {
                cpmlBX_.host()[idx] = 0.0;
                cpmlCX_.host()[idx] = 0.0;
                cpmlBY_.host()[idx] = 0.0;
                cpmlCY_.host()[idx] = 0.0;
                cpmlBZ_.host()[idx] = 0.0;
                cpmlCZ_.host()[idx] = 0.0;
            }
        }
    }
}

// Perform a single step
// For shared memory multiprocessing we partition the model
// along the Z axis.
void ECudaCpml::step(const box::ThreadInfo* info) {
    // Step the grid
    stepField(info);
    // Now correct for any incident waves at the points on the
    // boundary between the total field and scattered field zones
    m_->sources().stepE(info, x_.host(), y_.host(), z_.host());
    // Handle any periodic boundary conditions
    doPeriodicBoundaries(info, x_.host(), y_.host(), z_.host());
    print();
}

// Initialise the start and end cell coordinates
void ECudaCpml::initialiseStartEnd() {
    start_.x(0);
    start_.y(0);
    start_.z(0);
    // Only calculate the nth entries on periodic axes
    end_.x(m_->p()->n().x() - 1);
    end_.y(m_->p()->n().y() - 1);
    end_.z(m_->p()->n().z() - 1);
    if(m_->p()->boundaryX() == fdtd::Configuration::Boundary::periodic) {
        end_.x(end_.x() + 1);
    }
    if(m_->p()->boundaryY() == fdtd::Configuration::Boundary::periodic) {
        end_.y(end_.y() + 1);
    }
    if(m_->p()->boundaryZ() == fdtd::Configuration::Boundary::periodic) {
        end_.z(end_.z() + 1);
    }
}

