/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaCommon.h"
#include "CudaIndexing.h"
#include <Fdtd/Model.h>
#include <Domain/Material.h>
#include <Fdtd/Configuration.h>

// Constructor
CudaCommon::CudaCommon(fdtd::Model* model) :
        m_(model),
        material_(&cudaProxy_),
        boundaryDepthX_(&cudaProxy_),
        boundaryDepthY_(&cudaProxy_),
        boundaryDepthZ_(&cudaProxy_),
        drX_(&cudaProxy_),
        drY_(&cudaProxy_),
        drZ_(&cudaProxy_) {
}

// Initialise the CUDA memory ready for a run
void CudaCommon::initialise() {
    arraySize_ = (size_t) m_->p()->arraySize();
    matDepthArraySize_ = fdtd::Configuration::maxNumMaterials * ((size_t)m_->p()->boundarySize() + 2);
    material_.alloc(arraySize_);
    boundaryDepthX_.alloc(arraySize_);
    boundaryDepthY_.alloc(arraySize_);
    boundaryDepthZ_.alloc(arraySize_);
    drX_.alloc(matDepthArraySize_);
    drY_.alloc(matDepthArraySize_);
    drZ_.alloc(matDepthArraySize_);
    initialiseMaterial();
    initialiseBoundaryDepth();
    initialiseCellSize();
}

// Initialise the CUDA material array
void CudaCommon::initialiseMaterial() {
    for(int k = 0; k < m_->p()->n().z(); k++) {
        for(int j = 0; j < m_->p()->n().y(); j++) {
            for(int i = 0; i < m_->p()->n().x(); i++) {
                int idx = m_->p()->index(i, j, k);
                material_.host()[idx] = (char) m_->d()->getMaterialAt(i, j, k)->index();
            }
        }
    }
}

// Initialise the boundary depth arrays
void CudaCommon::initialiseBoundaryDepth() {
    int i, j, k;
    fdtd::Configuration* p = m_->p();
    box::Vector<int> boundaryDepth;
    for(k = 0; k < p->n().z(); k++) {
        // Z boundary depth if this is an absorbing boundary
        boundaryDepth.z(-1);
        if(p->boundaryZ() == fdtd::Configuration::Boundary::absorbing) {
            if(k < p->boundarySize()) {
                boundaryDepth.z(p->boundarySize() - k - 1);
            } else if(k >= (p->n().z() - p->boundarySize())) {
                boundaryDepth.z(k - (p->n().z() - p->boundarySize()));
            }
        }
        for(j = 0; j < p->n().y(); j++) {
            // Y boundary depth if this is an absorbing boundary
            boundaryDepth.y(-1);
            if(p->boundaryY() == fdtd::Configuration::Boundary::absorbing) {
                if(j < p->boundarySize()) {
                    boundaryDepth.y(p->boundarySize() - j - 1);
                } else if(j >= (p->n().y() - p->boundarySize())) {
                    boundaryDepth.y(j - (p->n().y() - p->boundarySize()));
                }
            }
            for(i = 0; i < p->n().x(); i++) {
                // X boundary depth if this is an absorbing boundary
                boundaryDepth.x(-1);
                if(p->boundaryX() == fdtd::Configuration::Boundary::absorbing) {
                    if(i < p->boundarySize()) {
                        boundaryDepth.x(p->boundarySize() - i - 1);
                    } else if(i >= (p->n().x() - p->boundarySize())) {
                        boundaryDepth.x(i - (p->n().x() - p->boundarySize()));
                    }
                }
                int idx = p->index(i, j, k);
                boundaryDepthX_.host()[idx] = (char) boundaryDepth.x();
                boundaryDepthY_.host()[idx] = (char) boundaryDepth.y();
                boundaryDepthZ_.host()[idx] = (char) boundaryDepth.z();
            }
        }
    }
}

// Return an index into a material constant array
// Boundary depth ranges from -1 to p_->boundarySize() inclusive
int CudaCommon::matDepthIndex(int materialId, int boundaryDepth) {
    int result = CUDAINDEXMATDEPTH(materialId, boundaryDepth, m_->p()->boundarySize());
    if(result >= matDepthArraySize_) {
        result = 0;
    }
    return result;
}

// Initialise the arrays that return the cell size from the material and boundary depth
// This performs the coordinate stretching required in the CPML material
void CudaCommon::initialiseCellSize() {
    for(auto& pos : m_->d()->materialLookup()) {
        domain::Material* mat = pos.second.get();
        for(int boundaryDepth = -1; boundaryDepth <= m_->p()->boundarySize(); boundaryDepth++) {
            int idx = matDepthIndex(mat->index(), boundaryDepth);
            box::Vector<double> dr = mat->dr(box::Vector<int>(boundaryDepth));
            drX_.host()[idx] = dr.x();
            drY_.host()[idx] = dr.y();
            drZ_.host()[idx] = dr.z();
        }
    }
}
