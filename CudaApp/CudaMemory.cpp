/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaMemory.h"
#include "CudaProxy.h"

// Constructor
template<class T>
CudaMemory<T>::CudaMemory(CudaProxy* proxy) :
        proxy_(proxy),
        host_(nullptr),
        gpu_(nullptr), 
        dataInGpu_(false) {
}

// Destructor
template<class T>
CudaMemory<T>::~CudaMemory() {
    free();
}

// Allocate host and GPU memory
template<class T>
void CudaMemory<T>::alloc(size_t size) {
    free();
    size_ = size;
    host_ = new T[size_];
    proxy_->malloc((void**) &gpu_, size_ * sizeof(T));
}

// Free host and GPU memory
template<class T>
void CudaMemory<T>::free() {
    delete[] host_;
    if(gpu_ != nullptr) {
        proxy_->free(gpu_);
    }
    host_ = nullptr;
    gpu_ = nullptr;
}

// Set host memory to a particular value
template<class T>
void CudaMemory<T>::set(T v) {
    for(size_t i = 0; i < size_; i++) {
        host_[i] = v;
    }
}

// Return a pointer to the host memory
template<class T>
T* CudaMemory<T>::host() {
    // Does the memory need copying?
    if(dataInGpu_) {
        proxy_->memcpy(host_, gpu_, size_ * sizeof(T), true);
        dataInGpu_ = false;
    }
    // The pointer
    return host_;
}

// Return a pointer to the GPU memory
template<class T>
T* CudaMemory<T>::gpu() {
    // Does the memory need copying?
    if(!dataInGpu_) {
        proxy_->memcpy(gpu_, host_, size_ * sizeof(T), false);
        dataInGpu_ = true;
    }
    // The pointer
    return gpu_;
}

// Instantiate particular types to make linking work
template class CudaMemory<double>;
template class CudaMemory<char>;


