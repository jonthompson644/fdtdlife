/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETERCURVE_H
#define FDTDLIFE_PARAMETERCURVE_H

#include <list>
#include <initializer_list>
#include <Xml/DomObject.h>
#include <Box/Factory.h>
#include "ParameterPoint.h"

namespace domain {
    // A class representing the frequency dependent curve of a parameter
    class ParameterCurve {
    public:
        // Constants and types
        enum {
            modeParameterPoint
        };
        enum class Which {
            permittivity = 0, permeability, admittance, refractiveIndex
        };
        constexpr static std::initializer_list<const char*> whichNames_{
                "Permittivity", "Permeability", "Admittance", "Refractive Index"
        };
        static const char* whichName(Which w);
        static Which whichValue(const std::string& n);

        // Construction
        ParameterCurve() = default;
        virtual void construct(int mode, int identifier);
        virtual ~ParameterCurve() = default;

        // API
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const ParameterCurve& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, ParameterCurve& v) {
            v.readConfig(o);
            return o;
        }
        void sort();
        ParameterPoint* find(int identifier);
        ParameterPoint* make();
        void remove(ParameterPoint* point);
        void clear();
        [[nodiscard]] size_t size() const { return points_.size(); }
        typedef std::list<std::unique_ptr<ParameterPoint>>::iterator iterator;
        iterator begin() { return points_.begin(); }
        iterator end() { return points_.end(); }
        std::complex<double> valueAt(double frequency);

        // Getters
        [[nodiscard]] int identifier() const { return identifier_; }
        [[nodiscard]] int mode() const { return mode_; }
        [[nodiscard]] Which which() const { return which_; }

        // Setters
        void which(Which v) { which_ = v; }

    protected:
        // Members
        int identifier_{-1};  // The unique identifier
        int mode_{};  // The type of table
        Which which_{};  // Which parameter the curve represents
        std::list<std::unique_ptr<ParameterPoint>> points_; // The parameter against frequency
        box::Factory<ParameterPoint> factory_;  // The point factory
    };
}


#endif //FDTDLIFE_PARAMETERCURVE_H
