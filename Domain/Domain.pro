#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T08:13:11
#
#-------------------------------------------------

QT       -= core gui

TARGET = Domain
TEMPLATE = lib
CONFIG += staticlib
QMAKE_CXXFLAGS += -openmp
DEFINES += USE_OPENMP
INCLUDEPATH += ..

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AdmittanceFnLayer.cpp \
    AdmittanceLayerShape.cpp \
    ArcMaterial.cpp \
    BinaryCodedPlate.cpp \
    BinaryPlateNxN.cpp \
    BoundaryMaterial.cpp \
    CpmlMaterial.cpp \
    Cuboid.cpp \
    DielectricLayer.cpp \
    Domain.cpp \
    ExtrudedPlane.cpp \
    ExtrudedPolygon.cpp \
    Material.cpp \
    PlateStack.cpp \
    Shape.cpp \
    SquareHole.cpp \
    SquarePlate.cpp \
    ThinLayerShape.cpp \
    ThinSheetMaterial.cpp \
    UnitCellShape.cpp

HEADERS += \
    AdmittanceFnLayer.h \
    AdmittanceLayerShape.h \
    ArcMaterial.h \
    BinaryCodedPlate.h \
    BinaryPlateNxN.h \
    BoundaryMaterial.h \
    CpmlMaterial.h \
    Cuboid.h \
    DefaultMaterial.h \
    DielectricLayer.h \
    Domain.h \
    ExtrudedPlane.h \
    ExtrudedPolygon.h \
    Material.h \
    PlateStack.h \
    Shape.h \
    SquareHole.h \
    SquarePlate.h \
    ThinLayerShape.h \
    ThinSheetMaterial.h \
    UnitCellShape.h

unix {cpp
    target.path = /usr/lib
    INSTALLS += target
}

SUBDIRS += \
    Domain.pro

DISTFILES += \
    Domain.pro.user \
    CMakeLists.txt \
    Makefile
