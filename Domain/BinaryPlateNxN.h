/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_BINARYPLATENXN_H
#define FDTDLIFE_BINARYPLATENXN_H

#include "AdmittanceLayerShape.h"
#include "DuplicatingShape.h"
#include "ThinLayerShape.h"
#include <set>

namespace domain {

    // An NxN binary coded plate material
    class BinaryPlateNxN
            : public AdmittanceLayerShape, public DuplicatingShape, public ThinLayerShape {
    public:
        // Construction
        BinaryPlateNxN() = default;

        // Overrides of AdmittanceLayerShape
        void fill(std::vector<uint16_t>& d) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;
        void evaluate(box::Expression::Context& c) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;

        // API
        static size_t requiredWords(size_t bitsPerHalfSide);

        // Constants
        static const size_t bitsPerCodingWord_ = 64;

        // Getters
        size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
        const std::vector<uint64_t>& coding() const { return coding_; }

        // Setters
        void bitsPerHalfSide(size_t v);
        void coding(const std::vector<uint64_t>& v);

    protected:
        // Parameters
        std::vector<uint64_t> coding_;
        size_t bitsPerHalfSide_{8};

        // Helpers
        void fillOnePlate(std::vector<uint16_t>& d, Material* myMat, Material* fillMat,
                          size_t t, double bitSize, const box::Vector<double>& p);
        void exportOnePlate(box::ExportWriter& writer,
                            Material* onMat, Material* offMat, bool plantOn, bool plantOff,
                            double bitSize, const box::Vector<double>& p) const;
        size_t identifyCentralSquare() const;
        bool isSet(size_t x, size_t y) const;
    };

}


#endif //FDTDLIFE_BINARYPLATENXN_H
