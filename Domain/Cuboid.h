/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUBOID_H
#define FDTDLIFE_CUBOID_H

#include "Shape.h"
#include <Box/Vector.h>

namespace domain {

    // A cuboid material shape.
    class Cuboid : public Shape {
    public:
        // Construction
        Cuboid() = default;

        // Overrides of Shape
        void fill(std::vector<uint16_t>& d) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                const std::set<int>& selectedMaterials) override;

        // Getters
        box::Vector<double> p1() const {return p1_;}
        box::Vector<double> p2() const {return p2_;}

        // Setters
        void p1(const box::Vector<double>& v) {p1_ = v;}
        void p2(const box::Vector<double>& v) {p2_ = v;}

    protected:
        // Parameters
        box::Vector<double> p1_; // Bottom left corner of the cuboid
        box::Vector<double> p2_; // Top right corner of the cuboid
    };

}


#endif //FDTDLIFE_CUBOID_H
