/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include "ParameterCurve.h"

// Second stage constructor used by factory
void domain::ParameterCurve::construct(int mode, int identifier) {
    mode_ = mode;
    identifier_ = identifier;
    // Initialise the factory
    factory_.define(new box::Factory<ParameterPoint>::Creator<ParameterPoint>(
            modeParameterPoint, "Parameter Point"));
}

// Write the configuration to the XML object
void domain::ParameterCurve::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("which") << which_;
    for(auto& point : points_) {
        root << xml::Obj("point") << *point;
    }
    root << xml::Close();
}

// Read the configuration from the XML object
void domain::ParameterCurve::readConfig(xml::DomObject& root) {
    // Don't need to read mode_ or identifier_ as they were read by the factory
    root >> xml::Open();
    points_.clear();
    root >> xml::Obj("which") >> which_;
    for(auto& o : root.obj("point")) {
        points_.emplace_back(factory_.restore(*o));
        *o >> *points_.back();
    }
    root >> xml::Close();
}

// Sort the points into ascending frequency order
void domain::ParameterCurve::sort() {
    points_.sort([](const std::unique_ptr<ParameterPoint>& a,
                    const std::unique_ptr<ParameterPoint>& b) {
        return b->frequency() > a->frequency();
    });
}

// Return the point with the given identifier
domain::ParameterPoint* domain::ParameterCurve::find(int identifier) {
    domain::ParameterPoint* result = nullptr;
    auto pos = std::find_if(points_.begin(), points_.end(),
                            [identifier](std::unique_ptr<ParameterPoint>& s) {
                                return s->identifier() == identifier;
                            });
    if(pos != points_.end()) {
        result = pos->get();
    }
    return result;
}

// Make a new curve point
domain::ParameterPoint* domain::ParameterCurve::make() {
    points_.emplace_back(factory_.make(modeParameterPoint));
    return points_.back().get();
}

// Remove a point from the curve
void domain::ParameterCurve::remove(domain::ParameterPoint* point) {
    auto pos = std::find_if(points_.begin(), points_.end(),
                            [point](std::unique_ptr<ParameterPoint>& s) {
                                return s.get() == point;
                            });
    if(pos != points_.end()) {
        points_.erase(pos);
    }
}

// Clear the points
void domain::ParameterCurve::clear() {
    points_.clear();
}

// Return the which enum associated with the name
domain::ParameterCurve::Which domain::ParameterCurve::whichValue(const std::string& n) {
    size_t result = 0;
    for(auto name : whichNames_) {
        if(n == name) {
            break;
        }
        result++;
    }
    return static_cast<Which>(result);
}

// Return the name associated with the enum value
const char* domain::ParameterCurve::whichName(domain::ParameterCurve::Which which) {
    const char* result = nullptr;
    int w = 0;
    for(auto name : whichNames_) {
        if(w == static_cast<int>(which)) {
            result = name;
            break;
        }
        w++;
    }
    return result;
}

// Return the value for the specified frequency
std::complex<double> domain::ParameterCurve::valueAt(double frequency) {
    std::complex<double> result;
    // Find the points above and below the desired frequency
    ParameterPoint* below = nullptr;
    ParameterPoint* above = nullptr;
    for(auto& point : points_) {
        if(point->frequency() >= frequency) {
            if(above == nullptr || point->frequency() < above->frequency()) {
                above = point.get();
            }
        }
        if(point->frequency() <= frequency) {
            if(below == nullptr || point->frequency() > below->frequency()) {
                below = point.get();
            }
        }
    }
    // Which points did we get?
    if(below && !above) {
        result = below->value();
    } else if(!below && above) {
        result = above->value();
    } else if(below && above) {
        result = box::Utility::interpolate(below->frequency(), above->frequency(),
                                           below->value(), above->value(), frequency);
    }
    return result;
}

