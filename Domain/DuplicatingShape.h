/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DUPLICATINGSHAPE_H
#define FDTDLIFE_DUPLICATINGSHAPE_H

#include <Box/VectorExpression.h>
#include <Fdtd/PropMatrixMethod.h>

namespace xml { class DomObject; }

namespace domain {

    // A mixer class for shapes that have the ability to duplicate in all directions.
    class DuplicatingShape {
    public:
        // Construction
        DuplicatingShape() = default;
        virtual ~DuplicatingShape() = default;

        // API
        virtual void writeConfig(xml::DomObject& p) const;
        virtual void readConfig(xml::DomObject& p);
        virtual void evaluate(box::Expression::Context& c);
        virtual void doMakeLayers(double z, UnitCellShape* shape,
                                  std::list<fdtd::PropMatrixMethod::Layer>& layers);

        // Accessors
        box::VectorExpression<double>& increment() { return increment_; }
        box::VectorExpression<size_t>& duplicate() { return duplicate_; }

        void increment(const box::VectorExpression<double>& v) { increment_ = v; }
        void duplicate(const box::VectorExpression<size_t>& v) { duplicate_ = v; }

    protected:
        // Parameters
        box::VectorExpression<double> increment_;  // The increment for the duplications
        box::VectorExpression<size_t> duplicate_{"1", "1", "1"};  // Number of dups
    };
}


#endif //FDTDLIFE_DUPLICATINGSHAPE_H
