/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquareHole.h"
#include "Material.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Box/DxfWriter.h>

// Fill the domain with the material representing this shape
void domain::SquareHole::fill(std::vector<uint16_t>& d) {
    // The plate material
    Material* myMat;
    if(m_->p()->useThinSheetSubcells()) {
        // Make a thin sheet material for this shape
        myMat = m_->d()->makeThinSheetMaterial(fillMaterial_,
                                               material_, this).get();
    } else {
        myMat = m_->d()->getMaterial(material_);
    }
    Material* fillMat = m_->d()->getMaterial(fillMaterial_);
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // The array of holes
    for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
        for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
            for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                fillOneHole(d, myMat, fillMat, p + offset);
            }
        }
    }
}

// Apply the matching matrix for this shape's material
domain::Material* domain::SquareHole::match(fdtd::PropMatrix* m, Material* other,
                                            double frequency) {
    std::complex<double> Yl;
    Yl = admittance(frequency);
    *m = *m * fdtd::PropMatrix(1.0 + Yl, Yl, -Yl, 1.0 - Yl);
    return other;
}

// Return the admittance of the square hole at the specified frequency
std::complex<double> domain::SquareHole::admittance(double frequency) {
    return m_->p()->plateAdmittance().admittance(
            fdtd::PlateAdmittance::plateTypeSquareHole,
            frequency, cellSizeX_.value(), holeSize_.value());
}

// Write the configuration to the DOM object
void domain::SquareHole::writeConfig(xml::DomObject& p) const {
    // Base class first
    AdmittanceLayerShape::writeConfig(p);
    RepeatCellShape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    ThinLayerShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("platesize") << holeSize_;
    p << xml::Obj("offsetx") << holeOffsetX_;
    p << xml::Obj("offsety") << holeOffsetY_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::SquareHole::readConfig(xml::DomObject& p) {
    // Base class first
    AdmittanceLayerShape::readConfig(p);
    RepeatCellShape::readConfig(p);
    DuplicatingShape::readConfig(p);
    ThinLayerShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("platesize") >> holeSize_;
    p >> xml::Obj("offsetx") >> holeOffsetX_;
    p >> xml::Obj("offsety") >> holeOffsetY_;
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::SquareHole::write(box::ExportWriter& writer,
                               const std::set<int>& selectedMaterials) {
    // Some global parameters
    double totalSizeX = cellSizeX_.value() * repeatX_.value();
    double totalSizeY = cellSizeX_.value() * repeatY_.value();
    double holeSize = cellSizeX_.value() * holeSize_.value() / 100.0;
    double borderSize = (cellSizeX_.value() - holeSize) / 2.0;
    // The materials
    Material* onMat = m_->d()->getMaterial(material_);
    Material* offMat = m_->d()->getMaterial(fillMaterial_);
    bool plantOn = onMat != nullptr && selectedMaterials.count(onMat->index()) > 0;
    bool plantOff = offMat != nullptr && selectedMaterials.count(offMat->index()) > 0;
    std::string onName("METAL");
    std::string offName("VACUUM");
    if(plantOn) {
        onName = onMat->name();
    }
    if(plantOff) {
        offName = offMat->name();
    }
    // Iterate over the repeats
    box::Vector<double> bottomLeft =
            center_.value() - box::Vector<double>(totalSizeX / 2.0,
                                                  totalSizeY / 2.0, 0);
    box::Vector<double> p1;
    box::Vector<double> p2;
    for(size_t ni = 0; ni < repeatX_.value(); ni++) {
        for(size_t nj = 0; nj < repeatY_.value(); nj++) {
            p1 = bottomLeft / box::Constants::milli_;
            p2 = (bottomLeft +
                  box::Vector<double>(borderSize, cellSizeX_.value(),
                                      layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOn) {
                writer.writeCuboid(p1, p2, name_, onName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSize, borderSize + holeSize, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSize + holeSize, cellSizeX_.value(),
                    layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOn) {
                writer.writeCuboid(p1, p2, name_, onName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSize, borderSize, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSize + holeSize, borderSize + holeSize,
                    layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOff) {
                writer.writeCuboid(p1, p2, name_, offName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSize, 0, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSize + holeSize, borderSize, layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOn) {
                writer.writeCuboid(p1, p2, name_, onName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSize + holeSize, 0, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft +
                  box::Vector<double>(cellSizeX_.value(), cellSizeX_.value(),
                                      layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOn) {
                writer.writeCuboid(p1, p2, name_, onName);
            }
            bottomLeft.y(bottomLeft.y() + cellSizeX_.value());
        }
        bottomLeft.x(bottomLeft.x() + cellSizeX_.value());
        bottomLeft.y(center_.value().y() - totalSizeY / 2.0);
    }
}

// Evaluate expressions
void domain::SquareHole::evaluate(box::Expression::Context& c) {
    AdmittanceLayerShape::evaluate(c);
    RepeatCellShape::evaluate(c);
    DuplicatingShape::evaluate(c);
    ThinLayerShape::evaluate(c);
    holeSize_.evaluate(c);
}

// Fill the domain with one occurrence of the plate
void domain::SquareHole::fillOneHole(std::vector<uint16_t>& d, Material* myMat,
                                     Material* fillMat,
                                     const box::Vector<double>& p) {
    // Some global parameters
    double halfTotalSizeX = cellSizeX_.value() * repeatX_.value() / 2.0;
    double halfTotalSizeY = cellSizeX_.value() * repeatY_.value() / 2.0;
    box::Vector<double> totalTopLeft = p - box::Vector<double>(
            halfTotalSizeX, halfTotalSizeY, 0.0);
    totalTopLeft.z(0.0);
    box::Vector<double> totalBottomRight = p + box::Vector<double>(
            halfTotalSizeX, halfTotalSizeY, 0.0);
    totalBottomRight.z(0.0);
    box::Vector<double> totalSize = box::Vector<double>(
            2 * halfTotalSizeX, 2 * halfTotalSizeY, 0.0);
    box::Vector<double> repeatSize = totalSize / box::Vector<double>(
            repeatX_.value(), repeatY_.value(), 1.0);
    double holeSize = cellSizeX_.value() * holeSize_.value() / 100.0;
    double borderSize = (cellSizeX_.value() - holeSize) / 2.0;
    // Iterate over the cells in the z layer that the structure resides
    box::Vector<size_t> cellPos = m_->p()->geometry()->cellFloor(p);
    box::Vector<size_t> n = m_->p()->geometry()->n();
    size_t k = cellPos.z();
    for(size_t i = 0; i < n.x(); i++) {
        for(size_t j = 0; j < n.y(); j++) {
            // The domain position of the cell
            box::Vector<double> pos = m_->p()->geometry()->cellCenter({i, j, k});
            pos.z(0.0);
            // Calculate some position dependent values
            box::Vector<double> offsetIntoRepeatCell = pos - totalTopLeft;
            offsetIntoRepeatCell = offsetIntoRepeatCell +
                                   box::Vector<double>(holeOffsetX_,
                                                       holeOffsetY_, 0.0);
            //offsetIntoRepeatCell.modulo(repeatSize);
            offsetIntoRepeatCell.z(0.0);
            box::Vector<double> repeatNumber =
                    (offsetIntoRepeatCell / repeatSize).floor<double>();
            repeatNumber.z(0.0);
            box::Vector<double> offsetIntoUnitCell =
                    offsetIntoRepeatCell - repeatNumber * repeatSize;
            offsetIntoUnitCell.z(0.0);
            box::Vector<double> holeTopLeftOffset = box::Vector<double>(
                    borderSize, borderSize, 0.0);
            box::Vector<double> holeBottomRightOffset = box::Vector<double>(
                    borderSize + holeSize, borderSize + holeSize, 0.0);
            bool requiresHole = (pos >= totalTopLeft && pos <= totalBottomRight);
            requiresHole = requiresHole && (offsetIntoUnitCell >= holeTopLeftOffset &&
                                          offsetIntoUnitCell <= holeBottomRightOffset);
            Material* existingMat =
                    m_->d()->getMaterial(d[m_->p()->geometry()->index(i, j, k)]);
            if(requiresHole) {
                // This is the hole
                if(fillMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMaterial_);
                } else if(existingMat == nullptr
                          || fillMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMat->index());
                }
            } else {
                // This is the surrounding grid
                if(myMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(material_);
                } else if(existingMat == nullptr
                          || myMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(myMat->index());
                }
            }
        }
    }
}

// Populate the layers list with information from this shape
void domain::SquareHole::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    DuplicatingShape::doMakeLayers(center_.z().value(), this, layers);
}

