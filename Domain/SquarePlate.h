/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SQUAREPLATE_H
#define FDTDLIFE_SQUAREPLATE_H

#include "AdmittanceLayerShape.h"
#include "CenterPosShape.h"
#include "RepeatCellShape.h"
#include "ThinLayerShape.h"
#include "DuplicatingShape.h"
#include <Box/Expression.h>

namespace domain {

    // A square metallic plate in the center of the unit cell
    // with the minimum plate thickness.
    class SquarePlate
            : public AdmittanceLayerShape
              , public RepeatCellShape, public DuplicatingShape, public ThinLayerShape {
    public:
        // Construction
        SquarePlate() = default;

        // Overrides of AdmittanceLayerShape
        void fill(std::vector<uint16_t>& d) override;
        Material* match(fdtd::PropMatrix* m, Material* other, double frequency) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;
        std::complex<double> admittance(double frequency) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;
        void evaluate(box::Expression::Context& c) override;

        // Getters
        [[nodiscard]] const box::Expression& plateSize() const { return plateSize_; }
        [[nodiscard]] double plateOffsetX() const { return plateOffsetX_; }
        [[nodiscard]] double plateOffsetY() const { return plateOffsetY_; }

        // Setters
        void plateOffsetX(double v) { plateOffsetX_ = v; }
        void plateOffsetY(double v) { plateOffsetY_ = v; }
        void plateSize(const box::Expression& v) { plateSize_ = v; }

    protected:
        // Parameters
        box::Expression plateSize_{0.5};  // Size of plate as percentage of the unit cell
        double plateOffsetX_{0.0};  // Offset in the X direction from the center of the cell
        double plateOffsetY_{0.0};  // Offset in the Y direction from the center of the cell

        // Helpers
        void fillOnePlate(std::vector<uint16_t>& d, Material* myMat, Material* fillMat,
                          const box::Vector<double>& p);
    };
}


#endif //FDTDLIFE_SQUAREPLATE_H
