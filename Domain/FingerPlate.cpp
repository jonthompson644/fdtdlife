/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FingerPlate.h"
#include "Material.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Box/DxfWriter.h>
#include <Box/Rectangle.h>
#include <cfloat>

// Fill the domain with the material representing this shape
void domain::FingerPlate::fill(std::vector<uint16_t>& domain) {
    // The plate material
    Material* myMat;
    if(m_->p()->useThinSheetSubcells()) {
        // Make a thin sheet material for this shape
        myMat = m_->d()->makeThinSheetMaterial(fillMaterial_,
                                               material_, this).get();
    } else {
        myMat = m_->d()->getMaterial(material_);
    }
    Material* fillMat = m_->d()->getMaterial(fillMaterial_);
    // My square and finger sizes
    calcFingerLengths();
    // The adjacent plates
    FingerPlate* rightPlate = nullptr;
    FingerPlate* belowPlate = nullptr;
    FingerPlate* leftPlate = nullptr;
    FingerPlate* abovePlate = nullptr;
    findAdjacentPlates(rightPlate, belowPlate, leftPlate, abovePlate);
    // Their finger lengths
    if(rightPlate != nullptr) {
        rightPlate->calcFingerLengths();
    }
    if(belowPlate != nullptr) {
        belowPlate->calcFingerLengths();
    }
    if(leftPlate != nullptr) {
        leftPlate->calcFingerLengths();
    }
    if(abovePlate != nullptr) {
        abovePlate->calcFingerLengths();
    }
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // Plant the fill material first
    doFillMaterial(domain, fillMat, p);
    // The array of plates
    for(size_t ri = 0; ri < duplicate_.value().x(); ri++) {
        FingerPlate* right = this;
        if(ri == duplicate_.value().x() - 1) {
            right = rightPlate;
        }
        FingerPlate* left = this;
        if(ri == 0) {
            left = leftPlate;
        }
        for(size_t rj = 0; rj < duplicate_.value().y(); rj++) {
            FingerPlate* below = this;
            if(rj == duplicate_.value().y() - 1) {
                below = belowPlate;
            }
            FingerPlate* above = this;
            if(rj == 0) {
                above = abovePlate;
            }
            for(size_t rk = 0; rk < duplicate_.value().z(); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                fillOnePlate(domain, myMat, p + offset, right, below, left, above);
            }
        }
    }
    // Prepare the admittance table cache
    initAdmittanceCache();
}

// Initialise the admittance cache with the two tables that surround
// this shape's configuration.
void domain::FingerPlate::initAdmittanceCache() {
    // Prepare the admittance table cache
    cacheBefore_ = nullptr;
    cacheAfter_ = nullptr;
    for(auto& a : admittances_) {
        double minFeatureSizeDiff = fabs(a.minFeatureSize() - minFeatureSize_.value());
        if(minFeatureSizeDiff < 0.01) {
            if(a.plateSize() < plateSize_.value()
               || box::Utility::equals(a.plateSize(), plateSize_.value())) {
                if(cacheBefore_ == nullptr || a.plateSize() > cacheBefore_->plateSize()) {
                    cacheBefore_ = &a;
                }
            }
            if(a.plateSize() > plateSize_.value()
               || box::Utility::equals(a.plateSize(), plateSize_.value())) {
                if(cacheAfter_ == nullptr || a.plateSize() < cacheAfter_->plateSize()) {
                    cacheAfter_ = &a;
                }
            }
        }
    }
}

// Calculate the square size and finger lengths of this finger plate
void domain::FingerPlate::calcFingerLengths() {
    g_ = {cellSizeX_.value(), cellSizeY_.value(), 0.0};
    d_ = g_ * minFeatureSize_.value() * box::Constants::percent_;
    o_ = g_ * fingerOffset_.value() * box::Constants::percent_;
    o_ = o_.max(d_ * 3.0);
    p_ = g_ * plateSize_.value() * box::Constants::percent_;
    b_ = g_ - d_;
    fingers_.clear();
    centralSquare_ = b_.min(p_);
    box::Vector<double> p = p_ - b_;
    box::Vector<double> maxFingerLen = g_ - o_ - d_ * 3.0;
    while(p.x() > DBL_EPSILON * 20.0 && maxFingerLen.x() > 0.0) {
        fingers_.push_back(maxFingerLen.min(p));
        p = p - maxFingerLen;
        maxFingerLen = maxFingerLen - d_ * 4.0;
    }
}

// Find the finger plates that are surrounding this one.  Take
// int account periodic boundaries.  Returns null if there isn't one.
void domain::FingerPlate::findAdjacentPlates(FingerPlate*& right, FingerPlate*& below,
                                             FingerPlate*& left, FingerPlate*& above) const {
    // Calculate the center of the (theoretical) adjacent plates
    box::Vector<double> increment = increment_.value();
    box::Vector<size_t> duplicate = duplicate_.value();
    box::Vector<double> rightCenter = center_.value();
    rightCenter.x(rightCenter.x() + duplicate.x() * increment.x());
    box::Vector<double> leftCenter = center_.value();
    leftCenter.x(leftCenter.x() - duplicate.x() * increment.x());
    box::Vector<double> belowCenter = center_.value();
    belowCenter.y(belowCenter.y() + duplicate.y() * increment.y());
    box::Vector<double> aboveCenter = center_.value();
    aboveCenter.y(aboveCenter.y() - duplicate.y() * increment.y());
    // Account for periodic boundaries (redo the size calc
    // here in case initialise has not been called)
    box::Vector<double> p1 = m_->p()->p1().value();
    box::Vector<double> p2 = m_->p()->p2().value();
    box::Vector<double> size = p2 - p1;
    if(m_->p()->boundary().x() == fdtd::Configuration::Boundary::periodic) {
        if(rightCenter.x() > p2.x()) {
            rightCenter.x(rightCenter.x() - size.x());
        }
        if(leftCenter.x() < p1.x()) {
            leftCenter.x(leftCenter.x() + size.x());
        }
        if(belowCenter.x() > p2.x()) {
            belowCenter.x(belowCenter.x() - size.x());
        }
        if(aboveCenter.x() < p1.x()) {
            aboveCenter.x(aboveCenter.x() + size.x());
        }
    }
    if(m_->p()->boundary().y() == fdtd::Configuration::Boundary::periodic) {
        if(rightCenter.y() > p2.y()) {
            rightCenter.y(rightCenter.y() - size.y());
        }
        if(leftCenter.y() < p1.y()) {
            leftCenter.y(leftCenter.y() + size.y());
        }
        if(belowCenter.y() > p2.y()) {
            belowCenter.y(belowCenter.y() - size.y());
        }
        if(aboveCenter.y() < p1.y()) {
            aboveCenter.y(aboveCenter.y() + size.y());
        }
    }
    // Now look for finger shapes with these centers
    right = nullptr;
    below = nullptr;
    left = nullptr;
    above = nullptr;
    for(auto& s : m_->d()->shapes()) {
        auto* f = dynamic_cast<FingerPlate*>(s.get());
        if(f != nullptr) {
            box::Vector<double> fCenter = f->center_.value();
            if(box::Utility::equals(fCenter.x(), rightCenter.x()) &&
               box::Utility::equals(fCenter.y(), rightCenter.y()) &&
               box::Utility::equals(fCenter.z(), rightCenter.z())) {
                right = f;
            }
            if(box::Utility::equals(fCenter.x(), leftCenter.x()) &&
               box::Utility::equals(fCenter.y(), leftCenter.y()) &&
               box::Utility::equals(fCenter.z(), leftCenter.z())) {
                left = f;
            }
            if(box::Utility::equals(fCenter.x(), belowCenter.x()) &&
               box::Utility::equals(fCenter.y(), belowCenter.y()) &&
               box::Utility::equals(fCenter.z(), belowCenter.z())) {
                below = f;
            }
            if(box::Utility::equals(fCenter.x(), aboveCenter.x()) &&
               box::Utility::equals(fCenter.y(), aboveCenter.y()) &&
               box::Utility::equals(fCenter.z(), aboveCenter.z())) {
                above = f;
            }
        }
    }
}

// Fill the main cell rectangles
void domain::FingerPlate::getMainRects(const box::Vector<double>& pos,
                                       box::Rectangle<double>& unitCell,
                                       box::Rectangle<double>& plate,
                                       box::Rectangle<double>& maxPlate) const {
    // The cell position
    unitCell = box::Rectangle<double>::rectangle(
            pos.x(), pos.y(), g_.x(), g_.y());
    // Position of the central plate
    plate = box::Rectangle<double>::rectangle(
            pos.x(), pos.y(), centralSquare_.x(), centralSquare_.y());
    maxPlate = box::Rectangle<double>::rectangle(
            pos.x(), pos.y(), b_.x(), b_.y());
}

// Fill the indent rectangles
void domain::FingerPlate::getIndentRects(const FingerPlate* right, const FingerPlate* below,
                                         const FingerPlate* left, const FingerPlate* above,
                                         const box::Rectangle<double>& plate,
                                         const box::Rectangle<double>& maxPlate,
                                         std::list<box::Rectangle<double>>& indents) const {
    // Position of the right indents
    if(right != nullptr && right->fingersLeft() && right->plateSize().value() >= 100.0) {
        double rectTop = maxPlate.top() + o_.y() - 2.0 * d_.y();
        for(auto& f : right->fingers_) {
            indents.emplace_back(maxPlate.right() - f.x(), rectTop,
                                 plate.right(), rectTop + 3 * d_.y());
            rectTop += 4 * d_.y();
        }
    }
    // Position of the left indents
    if(left != nullptr && left->fingersRight() && left->plateSize().value() >= 100.0) {
        double rectTop = maxPlate.bottom() - o_.y() + 2.0 * d_.y();
        for(auto& f : left->fingers_) {
            indents.emplace_back(plate.left(), rectTop - 3 * d_.y(),
                                 maxPlate.left() + f.x(), rectTop);
            rectTop -= 4 * d_.y();
        }
    }
    // Position of the bottom indents
    if(below != nullptr && below->fingersTop() && below->plateSize().value() >= 100.0) {
        double rectLeft = maxPlate.left() + o_.x() - 2.0 * d_.x();
        for(auto& f : below->fingers_) {
            indents.emplace_back(rectLeft, maxPlate.bottom() - f.y(),
                                 rectLeft + 3 * d_.x(), plate.bottom());
            rectLeft += 4 * d_.x();
        }
    }
    // Position of the top indents
    if(above != nullptr && above->fingersBottom() && above->plateSize().value() >= 100.0) {
        double rectLeft = maxPlate.right() - o_.x() + 2.0 * d_.x();
        for(auto& f : above->fingers_) {
            indents.emplace_back(rectLeft - 3 * d_.x(), plate.top(),
                                 rectLeft, maxPlate.top() + f.y());
            rectLeft -= 4 * d_.x();
        }
    }
}

// Position of the fingers (these need to account for periodic boundaries)
// and the finger stubs
void domain::FingerPlate::getFingerRects(const box::Rectangle<double>& unitCell,
                                         const box::Rectangle<double>& plate,
                                         std::list<box::Rectangle<double>>& fingers,
                                         std::list<box::Rectangle<double>>& stumps) const {
    if(plateSize_.value() >= 100.0) {
        double top = unitCell.top() + o_.y() - d_.y() / 2;
        double left = unitCell.left() + o_.x() - d_.x() / 2;
        double bottom = unitCell.bottom() - o_.y() + d_.y() / 2;
        double right = unitCell.right() - o_.x() + d_.x() / 2;
        box::Vector<double> p2 = m_->p()->p2().value();
        box::Vector<double> p1 = m_->p()->p1().value();
        box::Vector<double> size = p2 - p1;
        for(auto& f : fingers_) {
            if(fingersLeft()) {
                double x1 = unitCell.left() - f.x() + d_.x() / 2.0;
                double x2 = unitCell.left();
                if(m_->p()->boundary().x() == fdtd::Configuration::Boundary::periodic) {
                    if(x1 < p1.x()) {
                        x1 += size.x();
                        x2 += size.x();
                    }
                }
                fingers.emplace_back(x1, top, x2, top + d_.y());
                stumps.emplace_back(unitCell.left(), top, plate.left(), top + d_.y());
            }
            if(fingersRight()) {
                double x1 = unitCell.right();
                double x2 = unitCell.right() + f.x() - d_.x() / 2.0;
                if(m_->p()->boundary().x() == fdtd::Configuration::Boundary::periodic) {
                    if(x2 > p2.x()) {
                        x1 -= size.x();
                        x2 -= size.x();
                    }
                }
                fingers.emplace_back(x1, bottom - d_.y(), x2, bottom);
                stumps.emplace_back(plate.right(), bottom - d_.y(), unitCell.right(),
                                    bottom);
            }
            if(fingersTop()) {
                double y1 = unitCell.top() - f.y() + d_.y() / 2.0;
                double y2 = unitCell.top();
                if(m_->p()->boundary().y() == fdtd::Configuration::Boundary::periodic) {
                    if(y1 < p1.y()) {
                        y1 += size.y();
                        y2 += size.y();
                    }
                }
                fingers.emplace_back(left, y1, left + d_.x(), y2);
                stumps.emplace_back(left, unitCell.top(), left + d_.x(), plate.top());
            }
            if(fingersBottom()) {
                double y1 = unitCell.bottom();
                double y2 = unitCell.bottom() + f.y() - d_.y() / 2.0;
                if(m_->p()->boundary().y() == fdtd::Configuration::Boundary::periodic) {
                    if(y2 > p2.y()) {
                        y1 -= size.y();
                        y2 -= size.y();
                    }
                }
                fingers.emplace_back(right - d_.x(), y1, right, y2);
                stumps.emplace_back(right - d_.x(), plate.bottom(), right,
                                    unitCell.bottom());
            }
            top += 4.0 * d_.y();
            left += 4.0 * d_.x();
            bottom -= 4.0 * d_.y();
            right -= 4.0 * d_.x();
        }
    }
}

// Fill the domain with one finger plate.  The adjacent finger plates are given
// but are null if there is not one.
void domain::FingerPlate::fillOnePlate(std::vector<uint16_t>& dom, Material* myMat,
                                       const box::Vector<double>& pos,
                                       FingerPlate* right, FingerPlate* below,
                                       FingerPlate* left, FingerPlate* above) {
    // Main rectangles
    box::Rectangle<double> unitCell;
    box::Rectangle<double> plate;
    box::Rectangle<double> maxPlate;
    getMainRects(pos, unitCell, plate, maxPlate);
    // The indent rectangles
    std::list<box::Rectangle<double>> indents;
    getIndentRects(right, below, left, above, plate, maxPlate, indents);
    // The finger rectangles
    std::list<box::Rectangle<double>> fingers;
    std::list<box::Rectangle<double>> stumps;
    getFingerRects(unitCell, plate, fingers, stumps);
    // Iterate over the cells in the z layer that the structure resides
    box::Vector<size_t> n = m_->p()->geometry()->n();
    size_t k = m_->p()->geometry()->cellFloor(pos).z();
    for(size_t i = 0; i < n.x(); i++) {
        for(size_t j = 0; j < n.y(); j++) {
            // The domain position of the cell
            box::Vector<double> dpos = m_->p()->geometry()->cellCenter({i, j, k});
            // Where is this position?
            bool inCentralSquare = plate.isIn(dpos.x(), dpos.y());
            bool inIndents = box::Rectangle<double>::isInAny(indents, dpos.x(), dpos.y());
            bool inFingers = box::Rectangle<double>::isInAny(fingers, dpos.x(), dpos.y()) ||
                             box::Rectangle<double>::isInAny(stumps, dpos.x(), dpos.y());
            bool inPattern = (inCentralSquare && !inIndents) || inFingers;
            // Fill this location
            Material* existingMat =
                    m_->d()->getMaterial(dom[m_->p()->geometry()->index(i, j, k)]);
            if(inPattern) {
                // This is the plate
                if(myMat == nullptr) {
                    dom[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(material_);
                } else if(existingMat == nullptr
                          || myMat->priority() >= existingMat->priority()) {
                    dom[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(myMat->index());
                }
            }
        }
    }
}

// Put fill material in the whole z plane covered by the plate
void domain::FingerPlate::doFillMaterial(std::vector<uint16_t>& dom,
                                         domain::Material* fillMat,
                                         const box::Vector<double>& pos) {
    // Iterate over the cells in the z layer that the structure resides
    box::Vector<size_t> n = m_->p()->geometry()->n();
    size_t k = m_->p()->geometry()->cellFloor(pos).z();
    for(size_t i = 0; i < n.x(); i++) {
        for(size_t j = 0; j < n.y(); j++) {
            Material* existingMat =
                    m_->d()->getMaterial(dom[m_->p()->geometry()->index(i, j, k)]);
            if(fillMat == nullptr) {
                dom[m_->p()->geometry()->index(i, j, k)] =
                        static_cast<uint16_t>(fillMaterial_);
            } else if(existingMat == nullptr
                      || fillMat->priority() >= existingMat->priority()) {
                dom[m_->p()->geometry()->index(i, j, k)] =
                        static_cast<uint16_t>(fillMat->index());
            }
        }
    }
}

// Write the configuration to the DOM object
void domain::FingerPlate::writeConfig(xml::DomObject& p) const {
    // Base class first
    AdmittanceLayerShape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    ThinLayerShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("platesize") << plateSize_;
    p << xml::Obj("minfeaturesize") << minFeatureSize_;
    p << xml::Obj("fingersleft") << fingersLeft_;
    p << xml::Obj("fingersright") << fingersRight_;
    p << xml::Obj("fingerstop") << fingersTop_;
    p << xml::Obj("fingersbottom") << fingersBottom_;
    p << xml::Obj("fingeroffset") << fingerOffset_;
    for(auto& a : admittances_) {
        p << xml::Obj("shapeadmittance") << a;
    }
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::FingerPlate::readConfig(xml::DomObject& p) {
    // Base class first
    AdmittanceLayerShape::readConfig(p);
    DuplicatingShape::readConfig(p);
    ThinLayerShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("platesize") >> plateSize_;
    p >> xml::Obj("minfeaturesize") >> minFeatureSize_;
    p >> xml::Obj("fingersleft") >> xml::Default(fingersLeft_) >> fingersLeft_;
    p >> xml::Obj("fingersright") >> xml::Default(fingersRight_) >> fingersRight_;
    p >> xml::Obj("fingerstop") >> xml::Default(fingersTop_) >> fingersTop_;
    p >> xml::Obj("fingersbottom") >> xml::Default(fingersBottom_) >> fingersBottom_;
    p >> xml::Obj("fingeroffset") >> xml::Default(0.0) >> fingerOffset_;
    for(auto& o : p.obj("shapeadmittance")) {
        admittances_.emplace_back();
        *o >> admittances_.back();
    }
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::FingerPlate::write(box::ExportWriter& writer,
                                const std::set<int>& selectedMaterials) {
    // The materials
    bool plantOn = selectedMaterials.count(material_) > 0;
    std::string onName("METAL");
    if(plantOn) {
        if(!box::Utility::equals(plateSize_.value(), 0.0)) {
            calcFingerLengths();
            // The adjacent plates
            FingerPlate* rightPlate = nullptr;
            FingerPlate* belowPlate = nullptr;
            FingerPlate* leftPlate = nullptr;
            FingerPlate* abovePlate = nullptr;
            findAdjacentPlates(rightPlate, belowPlate, leftPlate, abovePlate);
            // Their finger lengths
            if(rightPlate != nullptr) {
                rightPlate->calcFingerLengths();
            }
            if(belowPlate != nullptr) {
                belowPlate->calcFingerLengths();
            }
            if(leftPlate != nullptr) {
                leftPlate->calcFingerLengths();
            }
            if(abovePlate != nullptr) {
                abovePlate->calcFingerLengths();
            }
            // The position
            box::Vector<double> p(center_.value());
            box::Vector<double> dp(increment_.value());
            // The array of plates
            for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
                const FingerPlate* right = this;
                if(ri == duplicate_.value().x() - 1) {
                    right = rightPlate;
                }
                const FingerPlate* left = this;
                if(ri == 0) {
                    left = leftPlate;
                }
                for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
                    const FingerPlate* below = this;
                    if(rj == duplicate_.value().y() - 1) {
                        below = belowPlate;
                    }
                    const FingerPlate* above = this;
                    if(rj == 0) {
                        above = abovePlate;
                    }
                    // A plate, note that exportOnePlate takes care of the z duplication
                    box::Vector<double> offset = box::Vector<double>(ri, rj, 0) * dp;
                    exportOnePlate(writer, onName, p + offset, right, below, left, above);
                }
            }
        }
    }
}

// Export one plate at the specified location
void domain::FingerPlate::exportOnePlate(
        box::ExportWriter& writer, const std::string& onMatName, const box::Vector<double>& p,
        const FingerPlate* right, const FingerPlate* below,
        const FingerPlate* left, const FingerPlate* above) const {
    // Main rectangles
    box::Rectangle<double> unitCell;
    box::Rectangle<double> plate;
    box::Rectangle<double> maxPlate;
    getMainRects(p, unitCell, plate, maxPlate);
    // The indent rectangles
    std::list<box::Rectangle<double>> indents;
    getIndentRects(right, below, left, above, plate, maxPlate, indents);
    // The finger rectangles
    std::list<box::Rectangle<double>> fingers;
    std::list<box::Rectangle<double>> stumps;
    getFingerRects(unitCell, plate, fingers, stumps);
    // Make the export shapes
    box::Rectangle<double> r;
    double z1 = p.z() / box::Constants::milli_;
    double z2 = z1 + layerThickness_.value() / box::Constants::milli_;
    writer.groupStart();
    if(duplicate_.z().value() > 1) {
        writer.duplicateStart();
    }
    // Do we have finger stumps?
    if(!stumps.empty()) {
        writer.combineStart();
    }
    // Do we have sockets?
    if(!indents.empty()) {
        writer.subtractStart();
    }
    // The central square
    r = box::Rectangle<double>(plate.left(), plate.top(), plate.right(), plate.bottom())
        / box::Constants::milli_;
    writer.writeCuboid({r.left(), r.top(), z1},
                       {r.right(), r.bottom(), z2},
                       name_, onMatName);
    // Minus the indents
    for(auto& indent : indents) {
        r = indent / box::Constants::milli_;
        writer.writeCuboid({r.left(), r.top(), z1},
                           {r.right(), r.bottom(), z2},
                           name_, onMatName);
    }
    // Do we have sockets?
    if(!indents.empty()) {
        writer.subtractEnd();
    }
    // The finger stubs
    for(auto& s : stumps) {
        r = s / box::Constants::milli_;
        writer.writeCuboid({r.left(), r.top(), z1},
                           {r.right(), r.bottom(), z2},
                           name_, onMatName);
    }
    // Do we have finger stumps?
    if(!stumps.empty()) {
        writer.combineEnd();
    }
    // The fingers themselves (which may be disconnected from the body by wrap around)
    for(auto& s : fingers) {
        r = s / box::Constants::milli_;
        writer.writeCuboid({r.left(), r.top(), z1},
                           {r.right(), r.bottom(), z2},
                           name_, onMatName);
    }
    if(duplicate_.z().value() > 1) {
        writer.duplicateEnd({0.0, 0.0,
                             increment_.z().value() / box::Constants::milli_},
                            duplicate_.z().value());
    }
    writer.groupEnd();
}

// Evaluate expressions
void domain::FingerPlate::evaluate(box::Expression::Context& c) {
    // Base classes
    AdmittanceLayerShape::evaluate(c);
    DuplicatingShape::evaluate(c);
    ThinLayerShape::evaluate(c);
    // My stuff
    plateSize_.evaluate(c);
    minFeatureSize_.evaluate(c);
    fingerOffset_.evaluate(c);
}

// Populate the layers list with information from this shape
void domain::FingerPlate::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    DuplicatingShape::doMakeLayers(center_.z().value(), this, layers);
}

// We override tbe admittance function of AdmittanceLayerShape so we
// can use the admittance tables specific for this shape.
// The fill function has already set up the cache with the necessary admittance tables.
std::complex<double> domain::FingerPlate::admittance(double frequency) {
    auto f = static_cast<long long>(std::round(frequency));
    std::complex<double> result;
    if(cacheBefore_ == nullptr && cacheAfter_ == nullptr) {
        // No tables, do nothing
    } else if(cacheBefore_ == nullptr) {
        // Use the above table
        result = cacheAfter_->admittance().admittanceAt(f, cellSizeX_.value());
    } else if(cacheAfter_ == nullptr || cacheBefore_ == cacheAfter_) {
        // No above or exact match, use the below
        result = cacheBefore_->admittance().admittanceAt(f, cellSizeX_.value());
    } else {
        // Interpolate between below and above
        std::complex<double> after = cacheAfter_->admittance().admittanceAt(
                f, cellSizeX_.value());
        std::complex<double> before = cacheBefore_->admittance().admittanceAt(
                f, cellSizeX_.value());
        result = std::complex<double>(
                box::Utility::interpolate(
                        cacheBefore_->plateSize(),
                        cacheAfter_->plateSize(),
                        before.real(), after.real(),
                        plateSize_.value()),
                box::Utility::interpolate(
                        cacheBefore_->plateSize(),
                        cacheAfter_->plateSize(),
                        before.imag(), after.imag(),
                        plateSize().value()));
    }
    return result;
}

// Write to the XML DOM
void domain::FingerPlate::ShapeAdmittance::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("platesize") << plateSize_;
    root << xml::Obj("admittancedata") << admittance_;
    root << xml::Obj("minfeaturesize") << minFeatureSize_;
    root << xml::Obj("name") << name_;
    root << xml::Close();
}

// Read from the XML DOM
void domain::FingerPlate::ShapeAdmittance::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("platesize") >> plateSize_;
    root >> xml::Obj("admittancedata") >> admittance_;
    root >> xml::Obj("minfeaturesize") >> xml::Default(0.0) >> minFeatureSize_;
    root >> xml::Obj("name") >> name_;
    root >> xml::Close();
}
