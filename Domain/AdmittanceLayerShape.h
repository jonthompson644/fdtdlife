/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ADMITTANCELAYERSHAPE_H
#define FDTDLIFE_ADMITTANCELAYERSHAPE_H

#include <Fdtd/AdmittanceData.h>
#include "UnitCellShape.h"
namespace xml {class DomObject;}

namespace domain {

    // A variation of the UnitCellShape mixer class for layer shapes that support
    // arbitrary admittance data for use with the propagation matrix method
    class AdmittanceLayerShape : public UnitCellShape {
    public:
        // Construction
        AdmittanceLayerShape() = default;

        // Overrides of UnitCellShape
        Material* match(fdtd::PropMatrix* m, Material* other, double frequency) override;
        std::complex<double> admittance(double frequency) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;

        // Accessors
        fdtd::AdmittanceData& admittanceTable() {return admittanceTable_;}

    protected:
        // Parameters
        fdtd::AdmittanceData admittanceTable_;
    };
}

#endif //FDTDLIFE_ADMITTANCELAYERSHAPE_H
