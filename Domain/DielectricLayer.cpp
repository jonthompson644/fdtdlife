/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DielectricLayer.h"
#include "Material.h"
#include <Fdtd/Model.h>
#include "Domain.h"
#include <Xml/DomObject.h>

// Fill the domain with the material representing this shape
void domain::DielectricLayer::fill(std::vector<uint16_t>& d) {
    Material* myMat = m_->d()->getMaterial(material_);
    // Work out the top left position of the first unit cell
    double halfSizeXY = cellSizeX_.value() * box::Constants::half_;
    double halfSizeZ = thickness_.value() * box::Constants::half_;
    box::Vector<double> totalTopLeft =
            center_.value() - box::Vector<double>(halfSizeXY, halfSizeXY, halfSizeZ);
    // Plant the material
    box::Vector<size_t> cell1 = m_->p()->geometry()->cellRound(totalTopLeft);
    box::Vector<size_t> cell2 = m_->p()->geometry()->cellRound(
            totalTopLeft +
            box::Vector<double>(cellSizeX_.value(), cellSizeX_.value(),
                                thickness_.value()));
    for(size_t i = cell1.x(); i < cell2.x(); i++) {
        for(size_t j = cell1.y(); j < cell2.y(); j++) {
            for(size_t k = cell1.z(); k < cell2.z(); k++) {
                Material* existingMat =
                        m_->d()->getMaterial(d[m_->p()->geometry()->index(i, j, k)]);
                if(myMat == nullptr || existingMat == nullptr
                   || myMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(material_);
                }
            }
        }
    }
}

// Apply the matching matrix for this shape's material
domain::Material* domain::DielectricLayer::match(fdtd::PropMatrix* m, Material* other,
                                                 double /*frequency*/) {
    // The shape's material
    Material* mat = m_->d()->getMaterial(material_);
    if(mat == nullptr) {
        mat = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    }
    // Is there a change of material at this boundary?
    if(mat != other) {
        mat->match(m, other);
    }
    // Return my material for further matching
    return mat;
}

// Write the configuration to the DOM object
void domain::DielectricLayer::writeConfig(xml::DomObject& p) const {
    // Base class first
    UnitCellShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("thickness") << thickness_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::DielectricLayer::readConfig(xml::DomObject& p) {
    // Base class first
    UnitCellShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("thickness") >> thickness_;
    p >> xml::Close();
}

// Evaluate expressions
void domain::DielectricLayer::evaluate(box::Expression::Context& c) {
    UnitCellShape::evaluate(c);
    thickness_.evaluate(c);
}

// Populate the layers list with information from this shape
void domain::DielectricLayer::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    layers.emplace_back(
            fdtd::PropMatrixMethod::LayerMode::begin,
            center_.z().value() - thickness_.value() * box::Constants::half_,
            this);
    layers.emplace_back(
            fdtd::PropMatrixMethod::LayerMode::end,
            center_.z().value() + thickness_.value() * box::Constants::half_,
            this);
}

//  Write the layer to the export object
void domain::DielectricLayer::write(box::ExportWriter& writer,
                                    const std::set<int>& selectedMaterials) {
    Material* onMat = m_->d()->getMaterial(material_);
    bool plantOn = onMat != nullptr && selectedMaterials.count(onMat->index()) > 0;
    if(plantOn) {
        box::Vector<double> center = center_.value();
        box::Vector<double> halfSize(cellSizeX_.value() * box::Constants::half_,
                                     cellSizeX_.value() * box::Constants::half_,
                                     thickness_.value() * box::Constants::half_);
        writer.writeCuboid((center - halfSize) / box::Constants::milli_,
                           (center + halfSize) / box::Constants::milli_,
                           name_, onMat->name());
    }
}
