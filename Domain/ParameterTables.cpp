/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ParameterTables.h"
#include <Box/Utility.h>

// Constructor
domain::ParameterTables::ParameterTables() {
    // Initialise the factory
    factory_.define(new box::Factory<ParameterTable>::Creator<ParameterTable>(
            modeParameterTable, "Parameter Table"));
}

// Write configuration to the XML DOM
void domain::ParameterTables::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& t : tables_) {
        root << xml::Obj("parametertable") << *t;
    }
    root << xml::Close();
}

// Read the configuration from the XML DOM
void domain::ParameterTables::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    for(auto p : root.obj("parametertable")) {
        tables_.emplace_back(factory_.restore(*p));
        *p >> *tables_.back();
    }
    root >> xml::Close();
}

// Remove a parameter table
void domain::ParameterTables::remove(ParameterTable* table) {
    auto pos = std::find_if(tables_.begin(), tables_.end(),
                            [table](std::unique_ptr<ParameterTable>& s) {
                                return s.get() == table;
                            });
    if(pos != tables_.end()) {
        tables_.erase(pos);
    }
}

// Create a new parameter table
domain::ParameterTable* domain::ParameterTables::make() {
    tables_.emplace_back(factory_.make(modeParameterTable));
    return tables_.back().get();
}

// Clear the tables
void domain::ParameterTables::clear() {
    tables_.clear();
}

// Find the parameter table with the given identifier
domain::ParameterTable* domain::ParameterTables::find(int identifier) {
    domain::ParameterTable* result = nullptr;
    auto pos = std::find_if(tables_.begin(), tables_.end(),
                            [identifier](std::unique_ptr<ParameterTable>& s) {
                                return s->identifier() == identifier;
                            });
    if(pos != tables_.end()) {
        result = pos->get();
    }
    return result;
}

// Find the patch ratio required for the specified refractive index.
// The layer spacing must be an exact match.  The unit cell, incidence angle,
// frequency and refractive index are all interpolated between the nearest pairs.
double domain::ParameterTables::findPatchRatio(double unitCell, double layerSpacing,
                                               double incidenceAngle, double frequency,
                                               double refractiveIndex, double& error) {
    double result = 0.0;
    error = refractiveIndex;
    // Find the unit cell and incidence angles that are immediately above and below the desired
    double unitCellBelow = 0.0;
    double unitCellAbove = 0.0;
    bool gotUnitCellBelow = false;
    bool gotUnitCellAbove = false;
    double incAngleBelow = 0.0;
    double incAngleAbove = 0.0;
    bool gotIncAngleBelow = false;
    bool gotIncAngleAbove = false;
    for(auto& table : tables_) {
        if(box::Utility::equals(table->layerSpacing(), layerSpacing)) {
            if(table->unitCell() <= unitCell) {
                if(!gotUnitCellBelow || table->unitCell() > unitCellBelow) {
                    unitCellBelow = table->unitCell();
                    gotUnitCellBelow = true;
                }
            }
            if(table->unitCell() >= unitCell) {
                if(!gotUnitCellAbove || table->unitCell() < unitCellAbove) {
                    unitCellAbove = table->unitCell();
                    gotUnitCellAbove = true;
                }
            }
            if(table->incidenceAngle() <= incidenceAngle) {
                if(!gotIncAngleBelow || table->incidenceAngle() > incAngleBelow) {
                    incAngleBelow = table->incidenceAngle();
                    gotIncAngleBelow = true;
                }
            }
            if(table->incidenceAngle() >= incidenceAngle) {
                if(!gotIncAngleAbove || table->incidenceAngle() < incAngleAbove) {
                    incAngleAbove = table->incidenceAngle();
                    gotIncAngleAbove = true;
                }
            }
        }
    }
    // Did we hit a limit?
    if(gotUnitCellBelow && !gotUnitCellAbove) {
        unitCellAbove = unitCellBelow;
        gotUnitCellAbove = true;
    }
    if(!gotUnitCellBelow && gotUnitCellAbove) {
        unitCellBelow = unitCellAbove;
        gotUnitCellBelow = true;
    }
    if(gotIncAngleBelow && !gotIncAngleAbove) {
        incAngleAbove = incAngleBelow;
        gotIncAngleAbove = true;
    }
    if(!gotIncAngleBelow && gotIncAngleAbove) {
        incAngleBelow = incAngleAbove;
        gotIncAngleBelow = true;
    }
    if(gotUnitCellBelow && gotUnitCellAbove && gotIncAngleBelow && gotIncAngleAbove) {
        // Now find the patch ratio corresponding to the desired refractive
        // index at the given frequency for each of these unit cells and incidence angles
        double error11 = 0.0;
        double patchRatio11 = findPatchRatioFor(unitCellBelow, layerSpacing, incAngleBelow,
                                                frequency, refractiveIndex, error11);
        double error12 = 0.0;
        double patchRatio12 = findPatchRatioFor(unitCellBelow, layerSpacing, incAngleAbove,
                                                frequency, refractiveIndex, error12);
        double error21 = 0.0;
        double patchRatio21 = findPatchRatioFor(unitCellAbove, layerSpacing, incAngleBelow,
                                                frequency, refractiveIndex, error21);
        double error22 = 0.0;
        double patchRatio22 = findPatchRatioFor(unitCellAbove, layerSpacing, incAngleAbove,
                                                frequency, refractiveIndex, error22);
        // Now do a 2D interpolation between these
        double patchRatio1 = box::Utility::interpolate(
                incAngleBelow, incAngleAbove, patchRatio11, patchRatio12, incidenceAngle);
        double patchRatio2 = box::Utility::interpolate(
                incAngleBelow, incAngleAbove, patchRatio21, patchRatio22, incidenceAngle);
        double error1 = box::Utility::interpolate(
                incAngleBelow, incAngleAbove, error11, error12, incidenceAngle);
        double error2 = box::Utility::interpolate(
                incAngleBelow, incAngleAbove, error21, error22, incidenceAngle);
        result = box::Utility::interpolate(
                unitCellBelow, unitCellAbove, patchRatio1, patchRatio2, unitCell);
        error = box::Utility::interpolate(
                unitCellBelow, unitCellAbove, error1, error2, unitCell);
    }
    return result;
}

// Find the patch ratio for the specified refractive index at the frequency for the
// exact unit cell, layer spacing and incidence angle.
double domain::ParameterTables::findPatchRatioFor(double unitCell, double layerSpacing,
                                                  double incidenceAngle, double frequency,
                                                  double refractiveIndex, double& error) {
    double result = 0.0;
    error = refractiveIndex;
    // Collect the tables with the correct unit cell, layer spacing and incidence angle
    std::list<ParameterTable*> theTables;
    for(auto& table : tables_) {
        if(box::Utility::equals(unitCell, table->unitCell()) &&
           box::Utility::equals(layerSpacing, table->layerSpacing()) &&
           box::Utility::equals(incidenceAngle, table->incidenceAngle())) {
            theTables.push_back(table.get());
        }
    }
    // Now sort them into ascending patch ratio order
    theTables.sort([](const ParameterTable* a, const ParameterTable* b) {
        return a->patchRatio() < b->patchRatio();
    });
    // Search through them tables for the first refractive index above the desired
    double aboveRefIndex = 0.0;
    double belowRefIndex = 0.0;
    ParameterTable* below = nullptr;
    ParameterTable* above = nullptr;
    for(auto& table : theTables) {
        std::complex<double> refIndex = table->valueAt(
                frequency, ParameterCurve::Which::refractiveIndex);
        if(refIndex.real() >= refractiveIndex) {
            above = table;
            aboveRefIndex = refIndex.real();
            break;
        } else {
            below = table;
            belowRefIndex = refIndex.real();
        }
    }
    // Have we hit a limit
    if(below && above) {
        error = 0.0;
        result = box::Utility::interpolate(
                belowRefIndex, aboveRefIndex, below->patchRatio(), above->patchRatio(),
                refractiveIndex);
    } else if(below) {
        result = below->patchRatio();
        error = belowRefIndex - refractiveIndex;
    } else if(above) {
        result = above->patchRatio();
        error = aboveRefIndex - refractiveIndex;
    }
    return result;
}
