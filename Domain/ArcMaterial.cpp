/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ArcMaterial.h"
#include <Fdtd/Model.h>
#include "Domain.h"
#include <Xml/DomObject.h>

// Initialise the material's parameters
void domain::ArcMaterial::initialise() {
    // First we deduce our parameters from the material to match with
    Material* defaultMat = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    defaultMat->evaluate();
    Material* matchMat = m_->d()->getMaterial(otherMaterialId_);
    if(matchMat == nullptr) {
        matchMat = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    }
    matchMat->evaluate();
    epsilon_ = sqrt(matchMat->epsilon().value() / defaultMat->epsilon().value()) *
                   defaultMat->epsilon().value();
    mu_ = matchMat->mu().value();
    sigma_ = matchMat->sigma().value();
    sigmastar_ = matchMat->sigmastar().value();
    // Call the base class to calculate the constants
    Material::initialise();
}

// Write the material to the XML object
void domain::ArcMaterial::writeConfig(xml::DomObject& root) const {
    // Base class first
    Material::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("othermaterialid") << otherMaterialId_;
    root << xml::Close();
}

// Read the material from the XML object
void domain::ArcMaterial::readConfig(xml::DomObject& root) {
    // Base class first
    Material::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("othermaterialid") >> xml::Default(-1) >> otherMaterialId_;
    root >> xml::Close();
}
