/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Cuboid.h"
#include <Fdtd/Model.h>
#include "Material.h"
#include <Xml/DomObject.h>

// Fill the domain with this shape
void domain::Cuboid::fill(std::vector<uint16_t>& d) {
    // My material
    Material* myMat = m_->d()->getMaterial(material_);
    if(myMat != nullptr) {
        // Convert space coords to grid coords
        box::Vector<size_t> cell1;
        cell1 = m_->p()->geometry()->cellRound(p1_);
        box::Vector<size_t> cell2;
        cell2 = m_->p()->geometry()->cellRound(p2_);
        // Plant the cuboid
        for(size_t i = cell1.x(); i < cell2.x(); i++) {
            for(size_t j = cell1.y(); j < cell2.y(); j++) {
                for(size_t k = cell1.z(); k < cell2.z(); k++) {
                    Material* existingMat = m_->d()->getMaterial(
                            d[m_->p()->geometry()->index(i, j, k)]);
                    if(existingMat == nullptr
                       || myMat->priority() >= existingMat->priority()) {
                        d[m_->p()->geometry()->index(i, j, k)] =
                                static_cast<uint16_t>(material_);
                    }
                }
            }
        }
    }
}

// Write the configuration to the DOM object
void domain::Cuboid::writeConfig(xml::DomObject& p) const {
    // Base class first
    Shape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("x1") << p1_.x();
    p << xml::Obj("y1") << p1_.y();
    p << xml::Obj("z1") << p1_.z();
    p << xml::Obj("x2") << p2_.x();
    p << xml::Obj("y2") << p2_.y();
    p << xml::Obj("z2") << p2_.z();
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::Cuboid::readConfig(xml::DomObject& p) {
    // Base class first
    Shape::readConfig(p);
    // Now my things
    double v;
    p >> xml::Reopen();
    p >> xml::Obj("x1") >> v;
    p1_.x(v);
    p >> xml::Obj("y1") >> v;
    p1_.y(v);
    p >> xml::Obj("z1") >> v;
    p1_.z(v);
    p >> xml::Obj("x2") >> v;
    p2_.x(v);
    p >> xml::Obj("y2") >> v;
    p2_.y(v);
    p >> xml::Obj("z2") >> v;
    p2_.z(v);
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::Cuboid::write(box::ExportWriter& writer,
                           const std::set<int>& selectedMaterials) {
    Material* mat = m_->d()->getMaterial(material_);
    if(mat != nullptr && selectedMaterials.count(mat->index())) {
        writer.writeCuboid(p1_ * 1000.0, p2_ * 1000.0, name_, mat->name());
    }
}
