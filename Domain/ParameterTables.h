/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETERTABLES_H
#define FDTDLIFE_PARAMETERTABLES_H

#include <list>
#include <memory>
#include <Xml/DomObject.h>
#include <Box/Factory.h>
#include "ParameterTable.h"

namespace fdtd { class Model; }

namespace domain {

    // An object that manages parameter tables for the
    // various shapes the domain supports
    class ParameterTables {
    public:
        // Constants
        enum {
            modeParameterTable
        };

        // Construction
        ParameterTables();

        // API
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const ParameterTables& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, ParameterTables& s) {
            s.readConfig(o);
            return o;
        }
        void remove(ParameterTable* table);
        ParameterTable* make();
        ParameterTable* find(int identifier);
        void clear();
        [[nodiscard]] size_t size() const { return tables_.size(); }
        typedef std::list<std::unique_ptr<ParameterTable>>::iterator iterator;
        iterator begin() { return tables_.begin(); }
        iterator end() { return tables_.end(); }
        double findPatchRatio(double unitCell, double layerSpacing, double incidenceAngle,
                              double frequency, double refractiveIndex, double& error);

    protected:
        // Members
        std::list<std::unique_ptr<ParameterTable>> tables_;  // The tables
        box::Factory<ParameterTable> factory_;
        double findPatchRatioFor(double unitCell, double layerSpacing, double incidenceAngle,
                                 double frequency, double refractiveIndex, double& error);
    };
}


#endif //FDTDLIFE_PARAMETERTABLES_H
