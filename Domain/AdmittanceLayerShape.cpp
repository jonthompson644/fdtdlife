/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceLayerShape.h"
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>

// Apply the matching matrix for this shape's material
domain::Material* domain::AdmittanceLayerShape::match(fdtd::PropMatrix* m, Material* other,
                                                      double frequency) {
    std::complex<double> Yc;
    Yc = admittance(frequency);
    *m = *m * fdtd::PropMatrix(1.0 + Yc, Yc, -Yc, 1.0 - Yc);
    return other;
}

// Return the admittance of the square plate at the specified frequency
std::complex<double> domain::AdmittanceLayerShape::admittance(double frequency) {
    auto f = static_cast<long long>(std::round(frequency));
    return admittanceTable_.admittanceAt(f, cellSizeX_.value());
}

// Write the configuration to the DOM object
void domain::AdmittanceLayerShape::writeConfig(xml::DomObject& p) const {
    // Base class first
    UnitCellShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("admittance") << admittanceTable_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::AdmittanceLayerShape::readConfig(xml::DomObject& p) {
    // Base class first
    UnitCellShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    if(!p.obj("admittance").empty()) {
        p >> xml::Obj("admittance") >> admittanceTable_;
    }
    p >> xml::Close();
}
