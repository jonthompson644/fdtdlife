/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatalogueLayer.h"

// Write a single item to the XML DOM
void domain::CatalogueLayer::Item::writeConfig(xml::DomObject& p) const {
    p << xml::Open();
    p << xml::Obj("coding") << coding_;
    p << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    p << xml::Obj("admittancedata") << admittanceData_;
    p << xml::Obj("name") << name_;
    p << xml::Close();
}

// Read a single item from the XML DOM
void domain::CatalogueLayer::Item::readConfig(xml::DomObject& p) {
    p >> xml::Open();
    p >> xml::Obj("coding") >> coding_;
    p >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
    p >> xml::Obj("admittancedata") >> admittanceData_;
    p >> xml::Obj("name") >> xml::Default("") >> name_;
    p >> xml::Close();
}

// Set the size of the bit pattern
void domain::CatalogueLayer::Item::bitsPerHalfSide(size_t v) {
    bitsPerHalfSide_ = v;
    coding_.resize(BinaryPlateNxN::requiredWords(bitsPerHalfSide_));
}

// Constructor for an item
domain::CatalogueLayer::Item::Item() {
    coding_.resize(BinaryPlateNxN::requiredWords(bitsPerHalfSide_));
}

// Write the shape to the XML DOM object
void domain::CatalogueLayer::writeConfig(xml::DomObject& p) const {
    // Base class first
    BinaryPlateNxN::writeConfig(p);
    // My stuff
    p << xml::Reopen();
    p << xml::Obj("select") << select_;
    for(auto& item : items_) {
        p << xml::Obj("item") << item;
    }
    p << xml::Close();
}

// Read the configuration from the XML DOM object
void domain::CatalogueLayer::readConfig(xml::DomObject& p) {
    // Base class first
    BinaryPlateNxN::readConfig(p);
    // My stuff
    p >> xml::Reopen();
    p >> xml::Obj("select") >> select_;
    items_.clear();
    for(auto& o : p.obj("item")) {
        items_.emplace_back();
        *o >> items_.back();
    }
    p >> xml::Close();
}

// Fill the domain with this shape
void domain::CatalogueLayer::fill(std::vector <uint16_t>& d) {
    // Is there something to select?
    if(!items_.empty()) {
        // Select the plate we are to use
        size_t i = static_cast<size_t>(std::round(select_.value())) % items_.size();
        bitsPerHalfSide(items_[i].bitsPerHalfSide());
        coding(items_[i].coding());
        admittanceTable() = items_[i].admittanceData();
        // Now let the base class fill the domain
        BinaryPlateNxN::fill(d);
    }
}

// Evaluate any expressions
void domain::CatalogueLayer::evaluate(box::Expression::Context& c) {
    // Base class first
    BinaryPlateNxN::evaluate(c);
    // Now my stuff
    select_.evaluate(c);
}

// Add a new empty item to the catalogue
size_t domain::CatalogueLayer::newItem() {
    size_t result = items_.size();
    items_.emplace_back();
    return result;
}

// Add a new item to the catalogue
size_t domain::CatalogueLayer::newItem(size_t bitsPerHalfSide,
        const std::vector<uint64_t>& coding) {
    size_t result = items_.size();
    items_.emplace_back();
    items_.back().bitsPerHalfSide(bitsPerHalfSide);
    items_.back().coding(coding);
    return result;
}

// Delete the item at the specified location
void domain::CatalogueLayer::deleteItem(size_t index) {
    auto pos = std::next(items_.begin(), index);
    if(pos != items_.end()) {
        items_.erase(pos);
    }
}
