/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_THINLAYERSHAPE_H
#define FDTDLIFE_THINLAYERSHAPE_H

#include <Box/Expression.h>
#include "Domain.h"
namespace xml {class DomObject;}

namespace domain {

    // Mixer class for shapes that can implement the thin layer
    class ThinLayerShape {
    public:
        // Construction
        ThinLayerShape() = default;
        virtual ~ThinLayerShape() = default;

        // Getters
        [[nodiscard]] const box::Expression& layerThickness() const {return layerThickness_;}
        [[nodiscard]] int fillMaterial() const {return fillMaterial_;}

        // Setters
        void layerThickness(const box::Expression& v) {layerThickness_ = v;}
        void fillMaterial(int v) {fillMaterial_ = v;}

        // API
        virtual void writeConfig(xml::DomObject& p) const;
        virtual void readConfig(xml::DomObject& p);
        virtual void evaluate(box::Expression::Context& c);

    protected:
        // Parameters
        box::Expression layerThickness_{0.000001};   // The thickness of the thin layer
        int fillMaterial_{Domain::defaultMaterial};  // The material to fill with
    };

}
#endif //FDTDLIFE_THINLAYERSHAPE_H
