/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_DIELECTRICLAYER_H
#define FDTDLIFE_DIELECTRICLAYER_H

#include "UnitCellShape.h"
#include "Shape.h"
#include "CenterPosShape.h"
#include <Box/Expression.h>

namespace domain {

    // A dielectric layer filling the unit cell with a specified thickness
    class DielectricLayer : public UnitCellShape {
    public:
        // Construction
        DielectricLayer() = default;

        // Overrides of Shape
        void fill(std::vector<uint16_t>& d) override;
        Material* match(fdtd::PropMatrix* m, Material* other, double frequency) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;
        std::complex<double> admittance(double /*frequency*/) override { return 0.0; }
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void evaluate(box::Expression::Context& c) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;

        // Getters
        [[nodiscard]] const box::Expression& thickness() const { return thickness_; }

        // Setters
        void thickness(const box::Expression& v) { thickness_ = v; }

    protected:
        // Parameters
        box::Expression thickness_{1e-6};   // The thickness of the layer
    };

}


#endif //FDTDLIFE_DIELECTRICLAYER_H
