#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "BinaryPlateNxN.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Box/DxfWriter.h>
#include "Material.h"
#include <memory>
#include <algorithm>

// Fill the domain with the material representing this shape
void domain::BinaryPlateNxN::fill(std::vector<uint16_t>& d) {
    // The plate material
    Material* myMat;
    if(m_->p()->useThinSheetSubcells()) {
        // Make a thin sheet material for this shape
        myMat = m_->d()->makeThinSheetMaterial(fillMaterial_,
                                               material_, this).get();
    } else {
        myMat = m_->d()->getMaterial(material_);
    }
    Material* fillMat = m_->d()->getMaterial(fillMaterial_);
    // How many cells thick?
    size_t t = static_cast<size_t>(std::max(
            ceil(layerThickness_.value() / m_->p()->dr().z().value()), 1.0));
    // The bit dimension
    double bitSize = cellSizeX_.value() / bitsPerHalfSide_ / 2.0;
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // The array of plates
    for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
        for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
            for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                fillOnePlate(d, myMat, fillMat, t, bitSize, p + offset);
            }
        }
    }
}

// Fill the domain with the material representing this shape
void domain::BinaryPlateNxN::fillOnePlate(
        std::vector<uint16_t>& d, Material* myMat, Material* fillMat, size_t t,
        double bitSize, const box::Vector<double>& p) {
    if(!coding_.empty()) {
        // For each binary bit location...
        double x1 = p.x() - cellSizeX_.value() / 2.0;
        double y1 = p.y() - cellSizeX_.value() / 2.0;
        double x2 = x1 + cellSizeX_.value();
        double y2 = y1 + cellSizeX_.value();
        double z = p.z();
        for(size_t m = 0; m < bitsPerHalfSide_; m++) {
            for(size_t n = 0; n < bitsPerHalfSide_; n++) {
                // Calculate the word and bit number
                size_t bitNum = m * bitsPerHalfSide_ + n;
                size_t wordNum = bitNum / bitsPerCodingWord_;
                size_t bitInWord = bitNum % bitsPerCodingWord_;
                // Top left quadrant
                box::Vector<double> p1 = box::Vector<double>(x1, y1,
                                                             z);
                box::Vector<double> p2 = box::Vector<double>(x1 + bitSize,
                                                             y1 + bitSize,
                                                             z);
                box::Vector<size_t> c1 = m_->p()->geometry()->cellRound(p1);
                box::Vector<size_t> c2 = m_->p()->geometry()->cellRound(p2);
                // Fill the cells that correspond to this bit
                for(size_t i = c1.x(); i < c2.x(); i++) {
                    for(size_t j = c1.y(); j < c2.y(); j++) {
                        for(size_t k = c1.z(); k < c1.z() + t; k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d[m_->p()->geometry()->index(i, j, k)]);
                            // Should this bit be filled?
                            if((coding_[wordNum] & (1ULL << bitInWord)) != 0) {
                                if(myMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(material_);
                                } else if(existingMat == nullptr
                                          ||
                                          myMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(myMat->index());
                                }
                            } else {
                                if(fillMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMaterial_);
                                } else if(existingMat == nullptr ||
                                          fillMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMat->index());
                                }
                            }
                        }
                    }
                }
                // Top right quadrant
                p1 = box::Vector<double>(x2 - bitSize, y1, z);
                p2 = box::Vector<double>(x2, y1 + bitSize, z);
                c1 = m_->p()->geometry()->cellRound(p1);
                c2 = m_->p()->geometry()->cellRound(p2);
                // Fill the cells that correspond to this bit
                for(size_t i = c1.x(); i < c2.x(); i++) {
                    for(size_t j = c1.y(); j < c2.y(); j++) {
                        for(size_t k = c1.z(); k < c1.z() + t; k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d[m_->p()->geometry()->index(i, j, k)]);
                            // Should this bit be filled?
                            if((coding_[wordNum] & (1ULL << bitInWord)) != 0) {
                                if(myMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(material_);
                                } else if(existingMat == nullptr
                                          ||
                                          myMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(myMat->index());
                                }
                            } else {
                                if(fillMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMaterial_);
                                } else if(existingMat == nullptr ||
                                          fillMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMat->index());
                                }
                            }
                        }
                    }
                }
                // Bottom left quadrant
                p1 = box::Vector<double>(x1, y2 - bitSize, z);
                p2 = box::Vector<double>(x1 + bitSize, y2, z);
                c1 = m_->p()->geometry()->cellRound(p1);
                c2 = m_->p()->geometry()->cellRound(p2);
                // Fill the cells that correspond to this bit
                for(size_t i = c1.x(); i < c2.x(); i++) {
                    for(size_t j = c1.y(); j < c2.y(); j++) {
                        for(size_t k = c1.z(); k < c1.z() + t; k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d[m_->p()->geometry()->index(i, j, k)]);
                            // Should this bit be filled?
                            if((coding_[wordNum] & (1ULL << bitInWord)) != 0) {
                                if(myMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(material_);
                                } else if(existingMat == nullptr
                                          ||
                                          myMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(myMat->index());
                                }
                            } else {
                                if(fillMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMaterial_);
                                } else if(existingMat == nullptr ||
                                          fillMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMat->index());
                                }
                            }
                        }
                    }
                }
                // Bottom right quadrant
                p1 = box::Vector<double>(x2 - bitSize, y2 - bitSize,
                                         z);
                p2 = box::Vector<double>(x2, y2, z);
                c1 = m_->p()->geometry()->cellRound(p1);
                c2 = m_->p()->geometry()->cellRound(p2);
                // Fill the cells that correspond to this bit
                for(size_t i = c1.x(); i < c2.x(); i++) {
                    for(size_t j = c1.y(); j < c2.y(); j++) {
                        for(size_t k = c1.z(); k < c1.z() + t; k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d[m_->p()->geometry()->index(i, j, k)]);
                            // Should this bit be filled?
                            if((coding_[wordNum] & (1ULL << bitInWord)) != 0) {
                                if(myMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(material_);
                                } else if(existingMat == nullptr
                                          ||
                                          myMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(myMat->index());
                                }
                            } else {
                                if(fillMat == nullptr) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMaterial_);
                                } else if(existingMat == nullptr ||
                                          fillMat->priority() >=
                                          existingMat->priority()) {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(fillMat->index());
                                }
                            }
                        }
                    }
                }
                x1 += bitSize;
                x2 -= bitSize;
            }
            y1 += bitSize;
            y2 -= bitSize;
            x1 = p.x() - cellSizeX_.value() / 2.0;
            x2 = x1 + cellSizeX_.value();
        }
    }
}

// Write the configuration to the DOM object
void domain::BinaryPlateNxN::writeConfig(xml::DomObject& p) const {
    // Base class first
    AdmittanceLayerShape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    ThinLayerShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    for(auto& c : coding_) {
        p << xml::Obj("bitcoding") << c;
    }
    p << xml::Obj("bitsperside") << bitsPerHalfSide_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::BinaryPlateNxN::readConfig(xml::DomObject& p) {
    // Base class first
    AdmittanceLayerShape::readConfig(p);
    DuplicatingShape::readConfig(p);
    ThinLayerShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    coding_.clear();
    for(auto& c : p.obj("bitcoding")) {
        uint64_t v = 0;
        *c >> v;
        coding_.push_back(v);
    }
    p >> xml::Obj("bitsperside") >> xml::Default(16) >> bitsPerHalfSide_;
    coding_.resize(requiredWords(bitsPerHalfSide_), 0);
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::BinaryPlateNxN::write(box::ExportWriter& writer,
                                   const std::set<int>& selectedMaterials) {
    // The materials
    Material* onMat = m_->d()->getMaterial(material_);
    Material* offMat = m_->d()->getMaterial(fillMaterial_);
    bool plantOn = onMat != nullptr && selectedMaterials.count(onMat->index()) > 0;
    bool plantOff = offMat != nullptr && selectedMaterials.count(offMat->index()) > 0;
    std::string onName("METAL");
    std::string offName("VACUUM");
    if(plantOn) {
        onName = onMat->name();
    }
    if(plantOff) {
        offName = offMat->name();
    }
    // The bit dimension
    double bitSize = cellSizeX_.value() / bitsPerHalfSide_ / 2.0;
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // The array of plates
    for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
        for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
            for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                exportOnePlate(writer, onMat, offMat, plantOn, plantOff,
                               bitSize, p + offset);
            }
        }
    }
}

// Convert to an export format (dimensions are written in mm)
void domain::BinaryPlateNxN::exportOnePlate(
        box::ExportWriter& writer,
        Material* onMat, Material* offMat, bool plantOn, bool plantOff,
        double bitSize, const box::Vector<double>& p) const {
    // Make a central square
    size_t central = identifyCentralSquare();
    if(central > 0) {
        box::Vector<double> offset(static_cast<double>(central) * bitSize,
                                   static_cast<double>(central) * bitSize,
                                   layerThickness_.value() / 2.0);
        writer.writeCuboid((p - offset) / box::Constants::milli_,
                           (p + offset) / box::Constants::milli_,
                           name_, onMat->name());
    }
    // For each binary bit location...
    double x1 = p.x() - cellSizeX_.value() / 2.0;
    double y1 = p.y() - cellSizeX_.value() / 2.0;
    double x2 = x1 + cellSizeX_.value();
    double y2 = y1 + cellSizeX_.value();
    double z1 = p.z() - layerThickness_.value() / 2.0;
    double z2 = p.z() + layerThickness_.value() / 2.0;
    for(size_t m = 0; m < bitsPerHalfSide_; m++) {
        for(size_t n = 0; n < bitsPerHalfSide_; n++) {
            if(m < (bitsPerHalfSide_ - central) || n < (bitsPerHalfSide_ - central)) {
                // Should this bit be filled?
                bool filled = isSet(n, m);
                // Top left quadrant
                box::Vector<double> p1 = box::Vector<double>(
                        x1, y1, z1) / box::Constants::milli_;
                box::Vector<double> p2 = box::Vector<double>(
                        x1 + bitSize, y1 + bitSize, z2) / box::Constants::milli_;
                if(plantOn && filled) {
                    writer.writeCuboid(p1, p2, name_, onMat->name());
                }
                if(plantOff && !filled) {
                    writer.writeCuboid(p1, p2, name_, offMat->name());
                }
                // Top right quadrant
                p1 = box::Vector<double>(x2 - bitSize, y1, z1) / box::Constants::milli_;
                p2 = box::Vector<double>(x2, y1 + bitSize, z2) / box::Constants::milli_;
                if(plantOn && filled) {
                    writer.writeCuboid(p1, p2, name_, onMat->name());
                }
                if(plantOff && !filled) {
                    writer.writeCuboid(p1, p2, name_, offMat->name());
                }
                // Bottom left quadrant
                p1 = box::Vector<double>(x1, y2 - bitSize, z1) / box::Constants::milli_;
                p2 = box::Vector<double>(x1 + bitSize, y2, z2) / box::Constants::milli_;
                if(plantOn && filled) {
                    writer.writeCuboid(p1, p2, name_, onMat->name());
                }
                if(plantOff && !filled) {
                    writer.writeCuboid(p1, p2, name_, offMat->name());
                }
                // Bottom right quadrant
                p1 = box::Vector<double>(
                        x2 - bitSize, y2 - bitSize, z1) / box::Constants::milli_;
                p2 = box::Vector<double>(x2, y2, z2) / box::Constants::milli_;
                if(plantOn && filled) {
                    writer.writeCuboid(p1, p2, name_, onMat->name());
                }
                if(plantOff && !filled) {
                    writer.writeCuboid(p1, p2, name_, offMat->name());
                }
            }
            x1 += bitSize;
            x2 -= bitSize;
        }
        y1 += bitSize;
        y2 -= bitSize;
        x1 = p.x() - cellSizeX_.value() / 2.0;
        x2 = x1 + cellSizeX_.value();
    }
}

// Return the size of the central filled square
size_t domain::BinaryPlateNxN::identifyCentralSquare() const {
    size_t result = 0;
    bool allFilled = true;
    while(allFilled && result < bitsPerHalfSide_) {
        size_t l = bitsPerHalfSide_ - 1 - result;
        for(size_t x = l; x < bitsPerHalfSide_; x++) {
            for(size_t y = l; y < bitsPerHalfSide_; y++) {
                allFilled = allFilled & isSet(x, y);  // Coding contains the top right quadrant
            }
        }
        if(allFilled) {
            result++;
        }
    }
    return result;
}

// Return whether the specified bit is set.
bool domain::BinaryPlateNxN::isSet(size_t x, size_t y) const {
    bool result = false;
    // Calculate the word and bit number
    size_t bitNum = y * bitsPerHalfSide_ + x;
    size_t wordNum = bitNum / bitsPerCodingWord_;
    size_t bitInWord = bitNum % bitsPerCodingWord_;
    // Return the value
    if(wordNum < coding_.size()) {
        result = (coding_[wordNum] & (1ULL << bitInWord)) != 0ULL;
    }
    return result;
}

// Set the bits per side of the plate
void domain::BinaryPlateNxN::bitsPerHalfSide(size_t v) {
    bitsPerHalfSide_ = v;
    coding_.clear();
    coding_.resize(requiredWords(bitsPerHalfSide_), 0);
}

// Set the coding bits
void domain::BinaryPlateNxN::coding(const std::vector<uint64_t>& v) {
    coding_ = v;
    coding_.resize(requiredWords(bitsPerHalfSide_), 0);
}

// Return the number of words required for the data
size_t domain::BinaryPlateNxN::requiredWords(size_t bitsPerHalfSide) {
    return size_t((bitsPerHalfSide * bitsPerHalfSide + bitsPerCodingWord_ - 1) /
                  bitsPerCodingWord_);
}

// Evaluate any expressions
void domain::BinaryPlateNxN::evaluate(box::Expression::Context& c) {
    AdmittanceLayerShape::evaluate(c);
    DuplicatingShape::evaluate(c);
    ThinLayerShape::evaluate(c);
}

// Populate the layers list with information from this shape
void domain::BinaryPlateNxN::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    DuplicatingShape::doMakeLayers(center_.z().value(), this, layers);
}

