/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Shape.h"
#include <Fdtd/Model.h>
#include <Xml/DomObject.h>

// Second stage constructor used by the factory template
void domain::Shape::construct(int mode, int identifier, fdtd::Model* m) {
    m_ = m;
    identifier_ = identifier;
    mode_ = mode;
}

// Write the configuration to the DOM object
void domain::Shape::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("material") << material_;
    root << xml::Obj("seqgenerated") << seqGenerated_;
    root << xml::Obj("ignore") << ignore_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void domain::Shape::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("material") >> material_;
    root >> xml::Obj("seqgenerated") >> xml::Default(false) >> seqGenerated_;
    root >> xml::Obj("ignore") >> xml::Default(false) >> ignore_;
    root >> xml::Close();
    // Note: Don't read mode and identifier as they are filled in
    // on object creation and may be different to the DOM contents.
}
