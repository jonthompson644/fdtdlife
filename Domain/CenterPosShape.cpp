/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CenterPosShape.h"

// Write the configuration to the DOM object
void domain::CenterPosShape::writeConfig(xml::DomObject& p) const {
    p << xml::Reopen();
    p << xml::Obj("center") << xml::Open()
      << xml::Obj("x") << center_.x()
      << xml::Obj("y") << center_.y()
      << xml::Obj("z") << center_.z()
      << xml::Close();
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::CenterPosShape::readConfig(xml::DomObject& p) {
    p >> xml::Reopen();
    if(!p.obj("center").empty()) {
        p >> xml::Obj("center") >> xml::Open()
          >> xml::Obj("x") >> center_.x()
          >> xml::Obj("y") >> center_.y()
          >> xml::Obj("z") >> center_.z()
          >> xml::Close();
    } else {
        // Backwards compatible
        p >> xml::Obj("x1") >> center_.x();
        p >> xml::Obj("y1") >> center_.y();
        p >> xml::Obj("z1") >> center_.z();
    }
    p >> xml::Close();
}

// Evaluate the expressions
void domain::CenterPosShape::evaluate(box::Expression::Context& c) {
    center_.evaluate(c);
}

