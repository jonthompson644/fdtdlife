/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CPMLMATERIAL_H
#define FDTDLIFE_CPMLMATERIAL_H

#include "Material.h"

namespace domain {

    // A special material that represents the weird stuff that
    // forms the various types of matched layer boundary.
    class CpmlMaterial : public Material {
    protected:
        std::vector<box::Vector<double>> cpmlSigma_;  // The sigma CPML factor
        std::vector<box::Vector<double>> cpmlKappa_;  // The kappa CPML factor
        std::vector<box::Vector<double>> cpmlA_;  // The a CPML factor
        std::vector<box::Vector<double>> cpmlB_;  // The b CPML factor
        std::vector<box::Vector<double>> cpmlC_;  // The c CPML factor
        std::vector<box::Vector<double>> cpmlCae_;  // Precalculated electric field constant
        std::vector<box::Vector<double>> cpmlCbe_;  // Precalculated electric field constant
        std::vector<box::Vector<double>> cpmlCah_;  // Precalculated electric field constant
        std::vector<box::Vector<double>> cpmlCbh_;  // Precalculated electric field constant
        // Configuration
        double sigmaCoeff_ {0.3};  // The coefficient used for calculating optimal PML sigmamax (Taflove has 0.8)
        double kappaMax_ {13.5};  // The maximum value of PML kappa
        double aMax_ {0.225};  // The maximum value of the PML attenuation parameter A
        double msk_ {2.5};  // The PML power scaling for sigma and kappa (Taflove has 3.5)
        double ma_ {2.0};   // The PML power scaling for parameter a
    public:
        // Construction
        CpmlMaterial() = default;

        // Overrides of Material
        void initialise() override;
        void cae(const box::Vector<size_t>& boundaryDepth, double& caex, double& caey, double& caez) const override;
        void cbe(const box::Vector<size_t>& boundaryDepth, double& cbex, double& cbey, double& cbez) const override;
        void cah(const box::Vector<size_t>& boundaryDepth, double& cahx, double& cahy, double& cahz) const override;
        void cbh(const box::Vector<size_t>& boundaryDepth, double& cbhx, double& cbhy, double& cbhz) const override;
        box::Vector<double> stretch(const box::Vector<size_t>& boundaryDepth) const override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;

        // API
        double bx(size_t boundaryDepth) const;
        double cx(size_t boundaryDepth) const;
        double by(size_t boundaryDepth) const;
        double cy(size_t boundaryDepth) const;
        double bz(size_t boundaryDepth) const;
        double cz(size_t boundaryDepth) const;

        // Getters
        double sigmaCoeff() const {return sigmaCoeff_;}
        double kappaMax() const {return kappaMax_;}
        double aMax() const {return aMax_;}
        double msk() const {return msk_;}
        double ma() const {return ma_;}

        // Setters
        void sigmaCoeff(double v) {sigmaCoeff_ = v;}
        void kappaMax(double v) {kappaMax_ = v;}
        void aMax(double v) {aMax_ = v;}
        void msk(double v) {msk_ = v;}
        void ma(double v) {ma_ = v;}
    };

}


#endif //FDTDLIFE_CPMLMATERIAL_H
