/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "AdmittanceFnLayer.h"
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Fdtd/Model.h>

// Apply the matching matrix for this shape
domain::Material* domain::AdmittanceFnLayer::match(fdtd::PropMatrix* m, Material* other,
        double frequency) {
    std::complex<double> Yc;
    Yc = admittance(frequency);
    *m = *m * fdtd::PropMatrix(1.0 + Yc, Yc, -Yc, 1.0 - Yc);
    return other;
}

// Calculate the admittance for this layer
std::complex<double> domain::AdmittanceFnLayer::admittance(double frequency) {
    std::complex<double> result;
    result.real(real_.value(frequency));
    result.imag(imag_.value(frequency));
    return result;
}

// Write the configuration to the DOM object
void domain::AdmittanceFnLayer::writeConfig(xml::DomObject& p) const {
    // Base classes first
    UnitCellShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("real") << real_;
    p << xml::Obj("imag") << imag_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::AdmittanceFnLayer::readConfig(xml::DomObject& p) {
    // Base classes first
    UnitCellShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("real") >> real_;
    p >> xml::Obj("imag") >> imag_;
    p >> xml::Close();
}

// Evaluate any expressions
void domain::AdmittanceFnLayer::evaluate(box::Expression::Context& c) {
    UnitCellShape::evaluate(c);
}

// Populate the layers list with information from this shape
void domain::AdmittanceFnLayer::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    layers.emplace_back(
            fdtd::PropMatrixMethod::LayerMode::thin,
            center_.z().value(),
            this);
}

