/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "DuplicatingShape.h"

// Write the configuration to the DOM object
void domain::DuplicatingShape::writeConfig(xml::DomObject& p) const {
    p << xml::Reopen();
    p << xml::Obj("increment") << xml::Open()
      << xml::Obj("x") << increment_.x()
      << xml::Obj("y") << increment_.y()
      << xml::Obj("z") << increment_.z()
      << xml::Close();
    p << xml::Obj("duplicate") << xml::Open()
      << xml::Obj("x") << duplicate_.x()
      << xml::Obj("y") << duplicate_.y()
      << xml::Obj("z") << duplicate_.z()
      << xml::Close();
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::DuplicatingShape::readConfig(xml::DomObject& p) {
    p >> xml::Reopen();
    if(!p.obj("increment").empty()) {
        p >> xml::Obj("increment") >> xml::Open()
          >> xml::Obj("x") >> increment_.x()
          >> xml::Obj("y") >> increment_.y()
          >> xml::Obj("z") >> increment_.z()
          >> xml::Close();
    }
    if(!p.obj("duplicate").empty()) {
        p >> xml::Obj("duplicate") >> xml::Open()
          >> xml::Obj("x") >> duplicate_.x()
          >> xml::Obj("y") >> duplicate_.y()
          >> xml::Obj("z") >> duplicate_.z()
          >> xml::Close();
    } else if(!p.obj("repeat").empty()) {   // Backwards compatibility
        p >> xml::Obj("repeat") >> xml::Open()
          >> xml::Obj("x") >> xml::Default(1) >> duplicate_.x()
          >> xml::Obj("y") >> xml::Default(1) >> duplicate_.y()
          >> xml::Obj("z") >> xml::Default(1) >> duplicate_.z()
          >> xml::Close();
    }
    p >> xml::Close();
}

// Evaluate the expressions
void domain::DuplicatingShape::evaluate(box::Expression::Context& c) {
    increment_.evaluate(c);
    duplicate_.evaluate(c);
}

// Populate the layers list with information from this shape
void domain::DuplicatingShape::doMakeLayers(double z, UnitCellShape* shape,
                                            std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    for(size_t i = 0; i < duplicate_.z().value(); i++) {
        layers.emplace_back(fdtd::PropMatrixMethod::LayerMode::thin,
                            z + i * increment_.z().value(), shape);
    }
}


