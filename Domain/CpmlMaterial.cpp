/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CpmlMaterial.h"
#include <Fdtd/Model.h>
#include "Domain.h"
#include <Xml/DomObject.h>
#include <Box/Constants.h>

// Initialise
void domain::CpmlMaterial::initialise() {
    Material::initialise();
    // Update parameters from the default material
    Material* defaultMaterial = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    if(defaultMaterial != nullptr) {
        defaultMaterial->evaluate();
        epsilon_ = defaultMaterial->epsilon().value();
        mu_ = defaultMaterial->mu().value();
    }
    // Size the arrays to cover the boundary
    cpmlSigma_.resize(m_->p()->boundarySize());
    cpmlKappa_.resize(m_->p()->boundarySize());
    cpmlA_.resize(m_->p()->boundarySize());
    cpmlB_.resize(m_->p()->boundarySize());
    cpmlC_.resize(m_->p()->boundarySize());
    cpmlCae_.resize(m_->p()->boundarySize());
    cpmlCbe_.resize(m_->p()->boundarySize());
    cpmlCah_.resize(m_->p()->boundarySize());
    cpmlCbh_.resize(m_->p()->boundarySize());
    // Calculate some parameters
    double eta = sqrt(mu_.value() / epsilon_.value());
    box::Vector<double> part(sigmaCoeff_ * (msk_ + 1) / eta);
    box::Vector<double> sigmaMax = part / m_->p()->dr().value();
    // Fill the parameter tables
    size_t d = m_->p()->boundarySize();
    double dt = m_->p()->dt();
    for(size_t w = 0; w < d; w++) {
        double powM = std::pow(static_cast<double>(w) / static_cast<double>(d), msk_);
        cpmlSigma_[w] = box::Vector<double>(sigmaMax * powM);
        cpmlKappa_[w] = box::Vector<double>((kappaMax_ - 1.0) * powM + 1);
        double powMa = std::pow(static_cast<double>(d - w) / static_cast<double>(d), ma_);
        cpmlA_[w] = box::Vector<double>(aMax_ * powMa);
        box::Vector<double> g = (cpmlSigma_[w] / cpmlKappa_[w] + cpmlA_[w]) *
                                (dt / box::Constants::epsilon0_);
        cpmlB_[w] = exp(-g);
        box::Vector<double> sigmaKappa = cpmlSigma_[w] * cpmlKappa_[w];
        box::Vector<double> kappa2a = cpmlKappa_[w] * cpmlKappa_[w] * cpmlA_[w];
        cpmlC_[w] = cpmlSigma_[w] / (sigmaKappa + kappa2a) * (cpmlB_[w] - 1);
        cpmlCae_[w] =
                (dt / epsilon_.value()) / (1.0 + cpmlSigma_[w] * dt / 2.0 / epsilon_.value());
        cpmlCbe_[w] = (1.0 - cpmlSigma_[w] * dt / 2.0 / epsilon_.value()) /
                      (1.0 + cpmlSigma_[w] * dt / 2.0 / epsilon_.value());
        cpmlCah_[w] = (dt / mu_.value()) / (1.0 + cpmlSigma_[w] * dt / 2.0 / epsilon_.value());
        cpmlCbh_[w] = (1.0 - cpmlSigma_[w] * dt / 2.0 / epsilon_.value()) /
                      (1.0 + cpmlSigma_[w] * dt / 2.0 / epsilon_.value());
    }
}

// Return the constant appropriate for the boundary
void domain::CpmlMaterial::cae(const box::Vector<size_t>& boundaryDepth,
                               double& caex, double& caey, double& caez) const {
    caex = cae_;
    if(boundaryDepth.x() < cpmlCae_.size()) {
        caex = cpmlCae_[boundaryDepth.x()].x();
    }
    caey = cae_;
    if(boundaryDepth.y() < cpmlCae_.size()) {
        caey = cpmlCae_[boundaryDepth.y()].y();
    }
    caez = cae_;
    if(boundaryDepth.z() < cpmlCae_.size()) {
        caez = cpmlCae_[boundaryDepth.z()].z();
    }
}

// Return the constant appropriate for the boundary
void domain::CpmlMaterial::cbe(const box::Vector<size_t>& boundaryDepth,
                               double& cbex, double& cbey, double& cbez) const {
    cbex = cbe_;
    if(boundaryDepth.x() < cpmlCbe_.size()) {
        cbex = cpmlCbe_[boundaryDepth.x()].x();
    }
    cbey = cbe_;
    if(boundaryDepth.y() < cpmlCbe_.size()) {
        cbey = cpmlCbe_[boundaryDepth.y()].y();
    }
    cbez = cbe_;
    if(boundaryDepth.z() < cpmlCbe_.size()) {
        cbez = cpmlCbe_[boundaryDepth.z()].z();
    }
}

// Return the constant appropriate for the boundary
void domain::CpmlMaterial::cah(const box::Vector<size_t>& boundaryDepth,
                               double& cahx, double& cahy, double& cahz) const {
    cahx = cah_;
    if(boundaryDepth.x() < cpmlCah_.size()) {
        cahx = cpmlCah_[boundaryDepth.x()].x();
    }
    cahy = cah_;
    if(boundaryDepth.y() < cpmlCah_.size()) {
        cahy = cpmlCah_[boundaryDepth.y()].y();
    }
    cahz = cah_;
    if(boundaryDepth.z() < cpmlCah_.size()) {
        cahz = cpmlCah_[boundaryDepth.z()].z();
    }
}

// Return the constant appropriate for the boundary
void domain::CpmlMaterial::cbh(const box::Vector<size_t>& boundaryDepth,
                               double& cbhx, double& cbhy, double& cbhz) const {
    cbhx = cbh_;
    if(boundaryDepth.x() < cpmlCbh_.size()) {
        cbhx = cpmlCbh_[boundaryDepth.x()].x();
    }
    cbhy = cbh_;
    if(boundaryDepth.y() < cpmlCbh_.size()) {
        cbhy = cpmlCbh_[boundaryDepth.y()].y();
    }
    cbhz = cbh_;
    if(boundaryDepth.z() < cpmlCbh_.size()) {
        cbhz = cpmlCbh_[boundaryDepth.z()].z();
    }
}

// Return the coordinate sizes.  This function allows an opportunity for
// the boundary material to stretch the coordinates.
box::Vector<double>
domain::CpmlMaterial::stretch(const box::Vector<size_t>& boundaryDepth) const {
    box::Vector<double> k(1.0);
    if(boundaryDepth.x() < cpmlKappa_.size()) {
        k.x(cpmlKappa_[boundaryDepth.x()].x());
    }
    if(boundaryDepth.y() < cpmlKappa_.size()) {
        k.y(cpmlKappa_[boundaryDepth.y()].y());
    }
    if(boundaryDepth.z() < cpmlKappa_.size()) {
        k.z(cpmlKappa_[boundaryDepth.z()].z());
    }
    return k;
}

// Write the material to the XML DOM
void domain::CpmlMaterial::writeConfig(xml::DomObject& root) const {
    // Base class
    Material::writeConfig(root);
    // My stuff
    root << xml::Reopen();
    root << xml::Obj("sigmacoeff") << sigmaCoeff_;
    root << xml::Obj("kappamax") << kappaMax_;
    root << xml::Obj("amax") << aMax_;
    root << xml::Obj("msk") << msk_;
    root << xml::Obj("ma") << ma_;
    root << xml::Close();
}

// Read the material from the XML DOM
void domain::CpmlMaterial::readConfig(xml::DomObject& root) {
    // Base class
    Material::readConfig(root);
    // My stuff
    root >> xml::Reopen();
    root >> xml::Obj("sigmacoeff") >> xml::Default(sigmaCoeff_) >> sigmaCoeff_;
    root >> xml::Obj("kappamax") >> xml::Default(kappaMax_) >> kappaMax_;
    root >> xml::Obj("amax") >> xml::Default(aMax_) >> aMax_;
    root >> xml::Obj("msk") >> xml::Default(msk_) >> msk_;
    root >> xml::Obj("ma") >> xml::Default(ma_) >> ma_;
    root >> xml::Close();
}

// Return the psi calculate 'b' coefficient in the x direction
double domain::CpmlMaterial::bx(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlB_.size()) {
        result = cpmlB_[boundaryDepth].x();
    }
    return result;
}

// Return the psi calculate 'b' coefficient in the y direction
double domain::CpmlMaterial::by(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlB_.size()) {
        result = cpmlB_[boundaryDepth].y();
    }
    return result;
}

// Return the psi calculate 'b' coefficient in the z direction
double domain::CpmlMaterial::bz(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlB_.size()) {
        result = cpmlB_[boundaryDepth].z();
    }
    return result;
}

// Return the psi calculate 'c' coefficient in the x direction
double domain::CpmlMaterial::cx(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlC_.size()) {
        result = cpmlC_[boundaryDepth].x();
    }
    return result;
}

// Return the psi calculate 'c' coefficient in the y direction
double domain::CpmlMaterial::cy(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlC_.size()) {
        result = cpmlC_[boundaryDepth].y();
    }
    return result;
}

// Return the psi calculate 'c' coefficient in the z direction
double domain::CpmlMaterial::cz(size_t boundaryDepth) const {
    double result = 0;
    if(boundaryDepth < cpmlC_.size()) {
        result = cpmlC_[boundaryDepth].z();
    }
    return result;
}
