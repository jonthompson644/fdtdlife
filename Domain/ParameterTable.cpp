/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include "ParameterTable.h"

// Second stage constructor used by factory
void domain::ParameterTable::construct(int mode, int identifier) {
    mode_ = mode;
    identifier_ = identifier;
    // Initialise the factory
    factory_.define(new box::Factory<ParameterCurve>::Creator<ParameterCurve>(
            modeParameterCurve, "Parameter Curve"));
}

// Write the configuration to the XML object
void domain::ParameterTable::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("layerspacing") << layerSpacing_;
    root << xml::Obj("incidenceangle") << incidenceAngle_;
    root << xml::Obj("patchratio") << patchRatio_;
    for(auto& item: curves_) {
        root << xml::Obj("curve") << *item;
    }
    root << xml::Close();
}

// Read the configuration from the XML object
void domain::ParameterTable::readConfig(xml::DomObject& root) {
    // Don't need to read mode_ or identifier_ as they were read by the factory
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("unitcell") >> unitCell_;
    root >> xml::Obj("layerspacing") >> layerSpacing_;
    root >> xml::Obj("incidenceangle") >> incidenceAngle_;
    root >> xml::Obj("patchratio") >> patchRatio_;
    curves_.clear();
    for(auto& o : root.obj("curve")) {
        curves_.emplace_back(factory_.restore(*o));
        *o >> *curves_.back();
    }
    root >> xml::Close();
}

// Remove a parameter table item
void domain::ParameterTable::remove(ParameterCurve* item) {
    auto pos = std::find_if(curves_.begin(), curves_.end(),
                            [item](std::unique_ptr<ParameterCurve>& s) {
                                return s.get() == item;
                            });
    if(pos != curves_.end()) {
        curves_.erase(pos);
    }
}

// Create a new parameter table item
domain::ParameterCurve* domain::ParameterTable::make() {
    curves_.emplace_back(factory_.make(modeParameterCurve));
    return curves_.back().get();
}

// Clear the tables
void domain::ParameterTable::clear() {
    curves_.clear();
}

// Find the parameter curve with the given identifier
domain::ParameterCurve* domain::ParameterTable::find(int identifier) {
    domain::ParameterCurve* result = nullptr;
    auto pos = std::find_if(curves_.begin(), curves_.end(),
                            [identifier](std::unique_ptr<ParameterCurve>& s) {
                                return s->identifier() == identifier;
                            });
    if(pos != curves_.end()) {
        result = pos->get();
    }
    return result;
}

// Find a curve of the specified type
domain::ParameterCurve* domain::ParameterTable::find(domain::ParameterCurve::Which which) {
    domain::ParameterCurve* result = nullptr;
    auto pos = std::find_if(curves_.begin(), curves_.end(),
                            [which](std::unique_ptr<ParameterCurve>& s) {
                                return s->which() == which;
                            });
    if(pos != curves_.end()) {
        result = pos->get();
    }
    return result;
}

// Return true if this table is able to supply values of the given type
bool domain::ParameterTable::has(domain::ParameterCurve::Which which) {
    bool result = false;
    for(auto& curve : curves_) {
        result = result || curve->which() == which;
        result = result ||
                 (curve->which() == ParameterCurve::Which::permittivity &&
                  which == ParameterCurve::Which::refractiveIndex);
    }
    return result;
}

// Create a curve of the specified type that interpolates between below and above
// for the specified incidence angle.
void domain::ParameterTable::interpolateIncidenceAngle(
        domain::ParameterCurve::Which which, domain::ParameterTable* below,
        domain::ParameterTable* above, double incidenceAngle) {
    incidenceAngle_ = incidenceAngle;
    auto* curve = make();
    curve->which(which);
    // Which tables do we have?
    if(!below) {
        below = above;
    }
    if(!above) {
        above = below;
    }
    if(below && above) {
        for(auto frequency : below->frequencies(which)) {
            auto* point = curve->make();
            point->frequency(frequency);
            point->value(box::Utility::interpolate(
                    below->incidenceAngle(), above->incidenceAngle(),
                    below->valueAt(frequency, which), above->valueAt(frequency, which),
                    incidenceAngle));
        }
    }
}

// Return the list of frequencies for the curve supporting the parameter which
std::list<double> domain::ParameterTable::frequencies(domain::ParameterCurve::Which which) {
    auto* curve = find(which);
    if(!curve && which == ParameterCurve::Which::refractiveIndex) {
        curve = find(ParameterCurve::Which::permittivity);
    }
    std::list<double> result;
    if(curve) {
        for(auto& point : *curve) {
            result.emplace_back(point->frequency());
        }
    }
    return result;
}

// Return a value for the specified frequency from the curve with the parameter which.
std::complex<double> domain::ParameterTable::valueAt(double frequency,
                                                     domain::ParameterCurve::Which which) {
    std::complex<double> result;
    auto* curve = find(which);
    if(!curve && which == ParameterCurve::Which::refractiveIndex) {
        auto* permittivityCurve = find(ParameterCurve::Which::permittivity);
        if(permittivityCurve) {
            auto* permeabilityCurve = find(ParameterCurve::Which::permeability);
            if(permeabilityCurve) {
                result = std::sqrt(permittivityCurve->valueAt(frequency) *
                                   permeabilityCurve->valueAt(frequency));
            } else {
                result = std::sqrt(permittivityCurve->valueAt(frequency));
            }
        }
    } else {
        result = curve->valueAt(frequency);
    }
    return result;
}
