/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_JIGSAWLAYER_H
#define FDTDLIFE_JIGSAWLAYER_H

#include "Shape.h"
#include <Box/VectorExpression.h>
#include <Box/Utility.h>
#include "DuplicatingShape.h"
#include "CenterPosShape.h"
#include "AdmittanceLayerShape.h"
#include "RepeatCellShape.h"
#include "ThinLayerShape.h"
#include <list>

namespace domain {

    // A layer containing the special jigsaw shape
    class JigsawLayer
            : public AdmittanceLayerShape, public RepeatCellShape, public DuplicatingShape
              , public ThinLayerShape {
    public:
        // A class that encapsulates the four parameters that
        // together specify the admittance of the shape
        class ShapeAdmittance {
        public:
            ShapeAdmittance() = default;
            ShapeAdmittance(double p, double f1, double f2, double f3, double unitCell);
            void writeConfig(xml::DomObject& root) const;
            void readConfig(xml::DomObject& root);
            friend xml::DomObject& operator<<(xml::DomObject& o, const ShapeAdmittance& v) {
                v.writeConfig(o);
                return o;
            }
            friend xml::DomObject& operator>>(xml::DomObject& o, ShapeAdmittance& v) {
                v.readConfig(o);
                return o;
            }
            double plateSize() const { return plateSize_; }
            double finger1Size() const { return finger1Size_; }
            double finger2Size() const { return finger2Size_; }
            double finger3Size() const { return finger3Size_; }
            double nPlateSize() const { return plateSize_ / admittance_.cellSize(); }
            double nFinger1Size() const { return finger1Size_ / admittance_.cellSize(); }
            double nFinger2Size() const { return finger2Size_ / admittance_.cellSize(); }
            double nFinger3Size() const { return finger3Size_ / admittance_.cellSize(); }
            const std::string& name() const { return name_; }
            fdtd::AdmittanceData& admittance() { return admittance_; }
            void plateSize(double v) { plateSize_ = v; }
            void finger1Size(double v) { finger1Size_ = v; }
            void finger2Size(double v) { finger2Size_ = v; }
            void finger3Size(double v) { finger3Size_ = v; }
            void name(const std::string& v) { name_ = v; }
            bool operator<(const ShapeAdmittance& other) const;
            bool operator==(const ShapeAdmittance& other) const;
        protected:
            double plateSize_{};
            double finger1Size_{};
            double finger2Size_{};
            double finger3Size_{};
            fdtd::AdmittanceData admittance_;
            std::string name_;
        };

        // Construction
        JigsawLayer() = default;

        // Overrides of AdmittanceLayerShape
        void fill(std::vector<uint16_t>& d) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;
        void evaluate(box::Expression::Context& c) override;
        std::complex<double> admittance(double frequency) override;

        // Accessors
        box::Expression& plateSize() { return plateSize_; }
        box::Expression& protrusionSize() { return protrusionSize_; }
        box::Expression& protrusionWidth() { return protrusionWidth_; }
        box::Expression& indentWidth() { return indentWidth_; }
        box::Expression& indentPos() { return indentPos_; }
        box::Expression& indentSize() { return indentSize_; }
        box::Expression& protrusion2Size() { return protrusion2Size_; }
        box::Expression& indent2Pos() { return indent2Pos_; }
        box::Expression& indent2Size() { return indent2Size_; }
        box::Expression& protrusion3Size() { return protrusion3Size_; }
        box::Expression& indent3Pos() { return indent3Pos_; }
        box::Expression& indent3Size() { return indent3Size_; }

        // Getters
        bool useSecondProtrusion() const { return useSecondProtrusion_; }
        bool useThirdProtrusion() const { return useThirdProtrusion_; }
        std::list<ShapeAdmittance>& admittances() { return admittances_; }

        // Setters
        void useSecondProtrusion(bool v) { useSecondProtrusion_ = v; }
        void useThirdProtrusion(bool v) { useThirdProtrusion_ = v; }

    protected:
        // Configuration
        box::Expression plateSize_{0.0001};  // Size of central plate (m)
        box::Expression protrusionSize_{0.0001};  // Length of the protrusion (m)
        box::Expression protrusionWidth_{0.00001};  // The width of the protrusion (m)
        box::Expression indentWidth_{0.00003};  // The width of the indent (m)
        box::Expression indentPos_{0.0};  // Position of the indent (m)
        box::Expression indentSize_{0.00004};  // Length of the indent (m)
        bool useSecondProtrusion_{false};  // Set to true to enable the second protrusion
        box::Expression protrusion2Size_{0.0001};  // Length of the 2nd protrusion (m)
        box::Expression indent2Pos_{0.0};  // Position of the 2nd indent (m)
        box::Expression indent2Size_{0.00004};  // Length of the 2nd indent (m)
        bool useThirdProtrusion_{false};  // Set to true to enable the third protrusion
        box::Expression protrusion3Size_{0.0001};  // Length of the 3rd protrusion (m)
        box::Expression indent3Pos_{0.0};  // Position of the 3rd indent (m)
        box::Expression indent3Size_{0.00004};  // Length of the 3rd indent (m)
        std::list<ShapeAdmittance> admittances_;
        // Admittance cache
        ShapeAdmittance cacheMe_;
        ShapeAdmittance* cacheBefore_{nullptr};
        ShapeAdmittance* cacheAfter_{nullptr};

        // Helpers
        void fillOnePlate(std::vector<uint16_t>& d, Material* myMat, Material* fillMat,
                          const box::Vector<double>& p);
    };
}


#endif //FDTDLIFE_JIGSAWLAYER_H
