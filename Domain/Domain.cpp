/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <iostream>
#include <cstring>
#include <memory>
#include <algorithm>
#include "Domain.h"
#include "Material.h"
#include "Shape.h"
#include <Fdtd/Model.h>
#include "DefaultMaterial.h"
#include "CpmlMaterial.h"
#include "ThinSheetMaterial.h"
#include "ArcMaterial.h"
#include <Box/Constants.h>
#include <Box/Utility.h>
#include <Xml/Exception.h>
#include "Cuboid.h"
#include "DielectricLayer.h"
#include "SquarePlate.h"
#include "SquareHole.h"
#include "BinaryPlateNxN.h"
#include "AdmittanceFnLayer.h"
#include "ExtrudedPolygon.h"
#include "CuboidArray.h"
#include "CatalogueLayer.h"
#include "JigsawLayer.h"
#include "FingerPlate.h"

// Constructor
domain::Domain::Domain(fdtd::Model* m) :
        m_(m),
        materialFactory_(domain::Domain::firstUserMaterial) {
    // Initialise the material factory
    materialFactory_.define(new box::Factory<Material>::Creator<Material>(
            materialModeNormal, "Normal"));
    materialFactory_.define(new box::Factory<Material>::Creator<ArcMaterial>(
            materialModeArc, "Anti-reflection"));
    materialFactory_.define(new box::Factory<Material>::Creator<DefaultMaterial>(
            materialModeBackground, "Default"));
    materialFactory_.define(new box::Factory<Material>::Creator<CpmlMaterial>(
            materialModeCpmlBoundary, "CPML Bndry"));
    materialFactory_.define(new box::Factory<Material>::Creator<ThinSheetMaterial>(
            materialModeThinSheet, "Thin Sheet"));
    // Initialise the shape factory
    shapeFactory_.define(new box::Factory<Shape>::Creator<Cuboid>(
            shapeModeCuboid, "Cuboid"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<DielectricLayer>(
            shapeModeDielectricLayer, "Dielectric Layer"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<SquarePlate>(
            shapeModeSquarePlate, "Square Plate"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<SquareHole>(
            shapeModeSquareHole, "Square Hole"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<BinaryPlateNxN>(
            shapeModeBinaryPlateNxN, "NxN Binary Plate"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<AdmittanceFnLayer>(
            shapeModeAdmittanceFn, "Admittance Function Layer"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<ExtrudedPolygon>(
            shapeModeExtrudedPolygon, "Extruded Polygon"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<CuboidArray>(
            shapeModeCuboidArray, "Cuboid Array"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<CatalogueLayer>(
            shapeModeCatalogueLayer, "Catalogue Pattern"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<JigsawLayer>(
            shapeModeJigsawLayer, "Jigsaw Plate"));
    shapeFactory_.define(new box::Factory<Shape>::Creator<FingerPlate>(
            shapeModeFingerPlate, "Super-capacitive Patch"));
    // The default materials
    makeDefaultMaterials();
}

//Destructor
domain::Domain::~Domain() {
    // Delete the materials and shapes
    deleteItems();
}

// Make a material using the factory
std::shared_ptr<domain::Material> domain::Domain::makeMaterial(int mode) {
    // Find the first unused index
    int index = firstUserMaterial;
    while(materialLookup_.count(index) > 0 && index <= lastUserMaterial) {
        index++;
    }
    std::shared_ptr<Material> mat(materialFactory_.createOne(index, mode, m_));
    add(mat);
    return mat;
}

// Make a material using the factory
std::shared_ptr<domain::Material> domain::Domain::makeMaterial(int id, int mode) {
    std::shared_ptr<Material> mat(materialFactory_.createOne(id, mode, m_));
    add(mat);
    return mat;
}

// Make a material using the factory
std::shared_ptr<domain::Material> domain::Domain::makeMaterial(xml::DomObject& o) {
    // Backwards compatible mode
    for(auto& i : o.obj("index")) {
        i->fixName("identifier");
    }
    int index;
    o >> xml::Obj("identifier") >> index;
    int defaultMode = materialModeNormal;
    if(index == defaultMaterial) {
        defaultMode = materialModeBackground;
    } else if(index == cpmlMaterial) {
        defaultMode = materialModeCpmlBoundary;
    }
    // Read the material from the DOM
    std::shared_ptr<Material> mat;
    try {
        mat.reset(materialFactory_.restore(defaultMode, o, m_));
        add(mat);
    } catch(box::Factory<Material>::Exception&) {
        // Just don't add the material if this fails
    }
    return mat;
}

// Make a thin sheet material using the factory
std::shared_ptr<domain::Material> domain::Domain::makeThinSheetMaterial(
        int mainMaterialId, int sheetMaterialId, ThinLayerShape* shape) {
    int id = static_cast<int>(thinSheetMaterialFlag) + nextThinSheetIndex_++;
    std::shared_ptr<Material> mat(materialFactory_.createOne(
            id, materialModeThinSheet, m_));
    add(mat);
    dynamic_cast<ThinSheetMaterial*>(mat.get())->set(mainMaterialId, sheetMaterialId, shape);
    return mat;
}

// Clear the materials
void domain::Domain::clear() {
    deleteItems();
    nextThinSheetIndex_ = 0;
    makeDefaultMaterials();
    materialFactory_.clear(domain::Domain::firstUserMaterial);
    shapeFactory_.clear();
}

// Make sure the default materials are present
void domain::Domain::makeDefaultMaterials() {
    // Create the default material (the vacuum)
    if(materialLookup_.count(domain::Domain::defaultMaterial) == 0) {
        std::shared_ptr<DefaultMaterial> defaultMat =
                std::dynamic_pointer_cast<DefaultMaterial>(
                        makeMaterial(domain::Domain::defaultMaterial,
                                     materialModeBackground));
        defaultMat->name("Default");
        defaultMat->epsilon(box::Expression(box::Constants::epsilon0_));
        defaultMat->mu(box::Expression(box::Constants::mu0_));
        defaultMat->sigma(box::Expression(box::Constants::sigma0_));
        defaultMat->sigmastar(box::Expression(box::Constants::sigmastar0_));
        defaultMat->color(box::Constants::colourBlack);
    }
    // Create the CPML material
    if(materialLookup_.count(domain::Domain::cpmlMaterial) == 0) {
        std::shared_ptr<CpmlMaterial> cpmlMat =
                std::dynamic_pointer_cast<CpmlMaterial>(
                        makeMaterial(domain::Domain::cpmlMaterial,
                                     materialModeCpmlBoundary));
        cpmlMat->name("CPML Bndry");
        cpmlMat->epsilon(box::Expression(box::Constants::epsilon0_));
        cpmlMat->mu(box::Expression(box::Constants::mu0_));
        cpmlMat->sigma(box::Expression(box::Constants::sigma0_));
        cpmlMat->sigmastar(box::Expression(box::Constants::sigmastar0_));
        cpmlMat->color(box::Constants::colourRed);
        cpmlMat->sigmaCoeff(0.3);  // Taflove has 0.8
        cpmlMat->kappaMax(13.5);
        cpmlMat->aMax(0.225);
        cpmlMat->msk(2.5);  // Taflove has 3.5
        cpmlMat->ma(2.0);
    }
}

// Clear the sequence generated materials and shapes
void domain::Domain::clearSeqGenerated() {
    auto nextMatPos = materials_.begin();
    while(nextMatPos != materials_.end()) {
        auto matPos = nextMatPos++;
        auto mat = *matPos;
        if(mat->seqGenerated()) {
            remove(mat.get());
        }
    }
    thinSheets_.clear();
    shapes_.remove_if([](const std::unique_ptr<Shape>& i) {
        return i->seqGenerated();
    });
}

// Delete all the items
void domain::Domain::deleteItems() {
    materials_.clear();
    materialLookup_.clear();
    thinSheets_.clear();
    shapes_.clear();
    parameterTables_.clear();
}

// Delete all the thin sheet materials
void domain::Domain::clearThinSheetMaterials() {
    thinSheets_.clear();
    nextThinSheetIndex_ = 0;
}

// Add a material to the domain
// m - The material to add
void domain::Domain::add(const std::shared_ptr<Material>& m) {
    materialLookup_[m->index()] = m;
    if((static_cast<unsigned int>(m->index()) & thinSheetMaterialFlag) != 0) {
        thinSheets_.push_back(m);
    } else {
        materials_.push_back(m);
    }
}

// Remove a material from the domain
void domain::Domain::remove(Material* m) {
    materialLookup_.erase(m->index());
    materials_.remove_if([m](const std::shared_ptr<Material>& i) {
        return i.get() == m;
    });
    thinSheets_.remove_if([m](const std::shared_ptr<Material>& i) {
        return i.get() == m;
    });
}

// Remove a material from the domain
void domain::Domain::remove(Shape* s) {
    shapes_.remove_if([s](const std::unique_ptr<Shape>& i) {
        return i.get() == s;
    });
}

// Initialise the domain ready for a model run
void domain::Domain::initialise() {
    // Clear the thin sheet materials
    clearThinSheetMaterials();
    // Evaluate any expressions
    evaluate();
    // Pre-calculate the material constants
    initialiseMaterials();
    // Prepare the domain
    generate();
}

// Initialise the materials
void domain::Domain::initialiseMaterials() {
    // Pre-calculate the material constants
    for(auto& material : materials_) {
        material->initialise();
    }
}

// Return the material in the specified cell
domain::Material* domain::Domain::getMaterialAt(size_t i, size_t j, size_t k) const {
    int m = d_[m_->p()->geometry()->index(i, j, k)];
    return materialLookup_.at(m).get();
}

// Allocate a material index
int domain::Domain::allocMaterialIndex() {
    // Find the first available user material identifier
    int id = firstUserMaterial;
    while(materialLookup_.find(id) != materialLookup_.end()) {
        id++;
    }
    return id;
}

// Return the material at the given location
domain::Material* domain::Domain::getMaterialAt(const box::Vector<size_t>& p) {
    size_t idx = m_->p()->geometry()->index(p.x(), p.y(), p.z());
    int index = d_[idx];
    return materialLookup_[index].get();
}

// Evaluate any expressions
void domain::Domain::evaluate() {
    // Make a context
    box::Expression::Context c;
    m_->variables().fillContext(c);
    // Now evaluate the expressions
    try {
        // The shapes
        for(auto& s : shapes_) {
            s->evaluate(c);
        }
    } catch(box::Expression::Exception& e) {
        std::cout << e.what() << std::endl;
    }
}

// Generate the material grid from the shapes
void domain::Domain::generate() {
    // Create the empty domain
    d_.resize(m_->p()->geometry()->arraySize());
    memset(d_.data(), defaultMaterial,
           sizeof(d_[0]) * m_->p()->geometry()->arraySize());
    // Fill the domain with the shapes
    for(auto& shape: shapes_) {
        if(!shape->ignore()) {
            shape->fill(d_);
        }
    }
    // Fill the boundary material
    Material* mat;
    mat = getMaterial(cpmlMaterial);
    if(mat != nullptr) {
        box::Vector<size_t> n = m_->p()->geometry()->n();
        size_t boundarySize = m_->p()->boundarySize();
        switch(m_->p()->boundary().x()) {
            case fdtd::Configuration::Boundary::absorbing:
                for(size_t i = 0; i < boundarySize; i++) {
                    for(size_t j = 0; j < n.y(); j++) {
                        for(size_t k = 0; k < n.z(); k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d_[m_->p()->geometry()->index(i, j, k)]);
                            if(existingMat == nullptr ||
                               mat->priority() >= existingMat->priority()) {
                                d_[m_->p()->geometry()->index(i, j, k)] =
                                        static_cast<char>(mat->index());
                            }
                            existingMat = m_->d()->getMaterial(
                                    d_[m_->p()->geometry()->index(
                                            m_->p()->geometry()->n().x() - i - 1, j, k)]);
                            if(existingMat == nullptr ||
                               mat->priority() >= existingMat->priority()) {
                                d_[m_->p()->geometry()->index(
                                        m_->p()->geometry()->n().x() - i - 1, j,
                                        k)] = static_cast<char>(mat->index());
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
        switch(m_->p()->boundary().y()) {
            case fdtd::Configuration::Boundary::absorbing:
                for(size_t i = 0; i < n.x(); i++) {
                    for(size_t j = 0; j < boundarySize; j++) {
                        for(size_t k = 0; k < n.z(); k++) {
                            Material* existingMat = m_->d()->getMaterial(
                                    d_[m_->p()->geometry()->index(i, j, k)]);
                            if(existingMat == nullptr ||
                               mat->priority() >= existingMat->priority()) {
                                d_[m_->p()->geometry()->index(i, j, k)] =
                                        static_cast<char>(mat->index());
                            }
                            existingMat = m_->d()->getMaterial(
                                    d_[m_->p()->geometry()->index(
                                            i, m_->p()->geometry()->n().y() - j - 1, k)]);
                            if(existingMat == nullptr ||
                               mat->priority() >= existingMat->priority()) {
                                d_[m_->p()->geometry()->index(
                                        i, m_->p()->geometry()->n().y() - j - 1,
                                        k)] = static_cast<char>(mat->index());
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
        switch(m_->p()->boundary().z()) {
            case fdtd::Configuration::Boundary::absorbing:
                for(size_t i = 0; i < n.x(); i++) {
                    for(size_t j = 0; j < n.y(); j++) {
                        for(size_t k = 0; k < boundarySize; k++) {
                            size_t idx = m_->p()->geometry()->index(i, j, k);
                            Material* existingMat = m_->d()->getMaterial(d_[idx]);
                            if(existingMat == nullptr
                               || mat->priority() >= existingMat->priority()) {
                                d_[idx] = static_cast<char>(mat->index());
                            }
                            size_t otherK = n.z() - k - 1;
                            size_t otherIdx = m_->p()->geometry()->index(i, j, otherK);
                            existingMat = m_->d()->getMaterial(d_[otherIdx]);
                            if(existingMat == nullptr
                               || mat->priority() >= existingMat->priority()) {
                                d_[otherIdx] = static_cast<char>(mat->index());
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    // Fix up the periodic boundaries

}

// Return the memory being used
size_t domain::Domain::memoryUse() {
    return m_->p()->geometry()->arraySize();
}

// Print the material list
void domain::Domain::print() {
    std::cout << "Materials (" << materials_.size() << ")" << std::endl;
    for(auto& material : materialLookup_) {
        material.second->print();
    }
}

// Allocate a shape identifier
int domain::Domain::allocShapeIdentifier() {
    return shapeFactory_.allocId();
}

// Return the material with the given index
domain::Material* domain::Domain::getMaterial(int index) {
    Material* result = nullptr;
    auto pos = materialLookup_.find(index);
    if(pos != materialLookup_.end()) {
        result = pos->second.get();
    }
    return result;
}

// Return the material that matches the specification given
domain::Material* domain::Domain::getMaterial(double epsilon,
                                              double mu, double sigma,
                                              double sigmastar,
                                              box::Constants::Colour color) {
    Material* result = nullptr;
    for(auto& mat : materials_) {
        if(box::Utility::equals(mat->epsilon().value(), epsilon) &&
           box::Utility::equals(mat->mu().value(), mu) &&
           box::Utility::equals(mat->sigma().value(), sigma) &&
           box::Utility::equals(mat->sigmastar().value(), sigmastar) &&
           mat->color() == color) {
            result = mat.get();
            break;
        }
    }
    return result;
}

// Return the shape with the specified id
domain::Shape* domain::Domain::getShape(int id) {
    Shape* result = nullptr;
    auto pos = std::find_if(shapes_.begin(), shapes_.end(),
            [id](const std::unique_ptr<Shape>& i) {
        return i->identifier() == id;
    });
    if(pos != shapes_.end()) {
        result = pos->get();
    }
    return result;
}

// Return the first shape with the specified name
domain::Shape* domain::Domain::getShape(const std::string& name) {
    Shape* result = nullptr;
    auto pos = std::find_if(shapes_.begin(), shapes_.end(),
                            [name](const std::unique_ptr<Shape>& i) {
                                return i->name() == name;
                            });
    if(pos != shapes_.end()) {
        result = pos->get();
    }
    return result;
}

// Write the configuration to the XML object
void domain::Domain::writeConfig(xml::DomObject& root) const {
    for(auto& mat : materials_) {  // Note do not save the thin sheet material objects
        root << xml::Obj("material") << *mat;
    }
    for(auto& shape : shapes_) {
        root << xml::Obj("shape") << *shape;
    }
    root << xml::Obj("parametertables") << parameterTables_;
}

// Read the configuration from the XML object
void domain::Domain::readConfig(xml::DomObject& root) {
    deleteItems();
    // Read the materials
    for(auto& p : root.obj("material")) {
        auto mat = makeMaterial(*p);
        if(mat) {
            *p >> *mat;
        }
    }
    // Read the shapes
    for(auto p : root.obj("shape")) {
        shapes_.emplace_back(shapeFactory_.restore(*p, m_));
        *p >> *shapes_.back();
    }
    if(!root.obj("parametertables").empty()) {
        root >> xml::Obj("parametertables") >> parameterTables_;
    }
    // Make sure we have all the required materials
    makeDefaultMaterials();
}

// Make a shape of the specified type
domain::Shape* domain::Domain::makeShape(int mode) {
    shapes_.emplace_back(shapeFactory_.make(mode, m_));
    return shapes_.back().get();
}

// Make a shape of the specified type using a given identifier
domain::Shape* domain::Domain::makeShape(int id, int mode) {
    shapes_.emplace_back(shapeFactory_.createOne(id, mode, m_));
    return shapes_.back().get();
}

// Make a shape using the given XML DOM but using a new identifier
domain::Shape* domain::Domain::makeShape(xml::DomObject& o) {
    shapes_.emplace_back(shapeFactory_.duplicate(o, m_));
    return shapes_.back().get();
}
