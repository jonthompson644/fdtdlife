/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DOMAIN_H
#define FDTDLIFE_DOMAIN_H

#include <Box/Vector.h>
#include <Fdtd/Configuration.h>
#include <Box/Constants.h>
#include <Box/Factory.h>
#include <Xml/DomObject.h>
#include <map>
#include <list>
#include <memory>
#include <vector>
#include "ParameterTables.h"

namespace fdtd { class Model; }

namespace domain {

    class Material;
    class Shape;
    class BoundaryMaterial;
    class ThinSheetMaterial;
    class ThinLayerShape;

    // This class represents the space the model operates in,
    // the arrangement of the materials that make up the space.
    class Domain {
    public:
        // Constants
        enum {
            materialModeNormal, materialModeArc, materialModeBackground,
            materialModeBoundary, materialModeCpmlBoundary, materialModeThinSheet
        };
        static constexpr int defaultMaterial = 0;  // The index num of the default material
        static constexpr int cpmlMaterial = 2;  // The index num of the CPML boundary material
        static constexpr int firstUserMaterial = 10;  // The index num of the 1st user material
        static constexpr int lastUserMaterial = 0x7fff;  // The max number of materials allowed
        static constexpr unsigned int thinSheetMaterialFlag = 0x8000;  // Indicates a thin sheet
        enum {
            shapeModeCuboid, shapeModeExtrudedPlaneZPlusRemoved,
            shapeModeCapacitorStackZPlusRemoved, shapeModeBitEncodedPlateRemoved,
            shapeModeDielectricLayer, shapeModeSquarePlate,
            shapeModeSquareHole, shapeModeBinaryPlateNxN,
            shapeModeAdmittanceFn,
            shapeModeExtrudedPolygon, shapeModeCuboidArray, shapeModeCatalogueLayer,
            shapeModeJigsawLayer, shapeModeFingerPlate
        };

    protected:
        // Members
        fdtd::Model* m_;
        std::vector<uint16_t> d_;
        std::map<int, std::shared_ptr<Material>> materialLookup_;
        std::list<std::shared_ptr<Material>> materials_;
        std::list<std::shared_ptr<Material>> thinSheets_;
        int nextThinSheetIndex_ {0};
        std::list<std::unique_ptr<Shape>> shapes_;
        box::Factory<Material> materialFactory_;
        box::Factory<Shape> shapeFactory_;
        ParameterTables parameterTables_;

        // Helpers
        void deleteItems();
    public:
        // Construction
        explicit Domain(fdtd::Model* p);
        ~Domain();

        // Methods
        std::shared_ptr<Material> makeMaterial(int mode);
        std::shared_ptr<Material> makeMaterial(int id, int mode);
        std::shared_ptr<Material> makeMaterial(xml::DomObject& o);
        std::shared_ptr<Material> makeThinSheetMaterial(
                int mainMaterialId, int sheetMaterialId, ThinLayerShape* shape);
        void makeDefaultMaterials();
        void add(const std::shared_ptr<Material>& m);
        void remove(Material* m);
        void remove(Shape* s);
        void initialise();
        void initialiseMaterials();
        Material* getMaterialAt(size_t i, size_t j, size_t k) const;
        Material* getMaterialAt(const box::Vector<size_t>& p);
        Material* getMaterial(int index);
        Material* getMaterial(double epsilon, double mu, double sigma,
                              double sigmastar, box::Constants::Colour color);
        box::Factory<Material>& materialFactory() { return materialFactory_; };
        Shape* getShape(int id);
        Shape* getShape(const std::string& name);
        Shape* makeShape(int mode);
        Shape* makeShape(int id, int mode);
        Shape* makeShape(xml::DomObject& o);
        box::Factory<Shape>& shapeFactory() { return shapeFactory_; }
        int allocMaterialIndex();
        int allocShapeIdentifier();
        void clear();
        void clearSeqGenerated();
        void clearThinSheetMaterials();
        void generate();
        void evaluate();
        size_t memoryUse();
        void print();
        const std::list<std::shared_ptr<Material>>& materials() const { return materials_; }
        const std::map<int, std::shared_ptr<Material>>& materialLookup() const {
            return materialLookup_;
        }
        const std::list<std::unique_ptr<Shape>>& shapes() const { return shapes_; }
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Domain& d) {
            d.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Domain& d) {
            d.readConfig(o);
            return o;
        }
        fdtd::Model* m() { return m_; }
        ParameterTables& parameterTables() { return parameterTables_; }
    };

}

#endif //FDTDLIFE_DOMAIN_H
