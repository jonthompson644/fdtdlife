/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "RepeatCellShape.h"
#include <Xml/DomObject.h>

// Read from the XML DOM
void domain::RepeatCellShape::readConfig(xml::DomObject& p) {
    p >> xml::Reopen();
    p >> xml::Obj("repeat") >> xml::Default(1) >> repeatX_; // Backwards compatibility
    p >> xml::Obj("repeat") >> xml::Default(1) >> repeatY_; // Backwards compatibility
    p >> xml::Obj("repeatx") >> xml::Default(repeatX_.text()) >> repeatX_;
    p >> xml::Obj("repeaty") >> xml::Default(repeatY_.text()) >> repeatY_;
    p >> xml::Close();
}

// Write to the XML DOM
void domain::RepeatCellShape::writeConfig(xml::DomObject& p) const {
    p << xml::Reopen();
    p << xml::Obj("repeatx") << repeatX_;
    p << xml::Obj("repeaty") << repeatY_;
    p << xml::Close();
}

// Evaluate any expressions
void domain::RepeatCellShape::evaluate(box::Expression::Context& c) {
    repeatX_.evaluate(c);
    repeatY_.evaluate(c);
}
