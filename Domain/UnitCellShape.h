/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_UNITCELLSHAPE_H
#define FDTDLIFE_UNITCELLSHAPE_H

#include <Box/Expression.h>
#include "CenterPosShape.h"
#include "Shape.h"
#include <complex>
#include <Fdtd/PropMatrixMethod.h>

namespace xml { class DomObject; }
namespace fdtd { class PropMatrix; }

namespace domain {

    class Material;

    // A mixer class for all shapes that use a unit cell
    class UnitCellShape : public Shape, public CenterPosShape {
    public:
        // Construction
        UnitCellShape() = default;

        // Getters
        [[nodiscard]] const box::Expression& cellSizeX() const { return cellSizeX_; }
        [[nodiscard]] const box::Expression& cellSizeY() const { return cellSizeY_; }
        [[nodiscard]] bool square() const { return square_; }

        // Setters
        void cellSizeX(const box::Expression& v);
        void cellSizeY(const box::Expression& v);
        void square(bool v);

        // API
        virtual Material* match(fdtd::PropMatrix* m, Material* other, double frequency) = 0;
        virtual std::complex<double> admittance(double frequency) = 0;
        virtual void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) = 0;

        // Overrides of Shape
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void evaluate(box::Expression::Context& c) override;

    protected:
        // Parameters
        box::Expression cellSizeX_;   // The x size of the cell
        box::Expression cellSizeY_;   // The y size of the cell
        bool square_{true};  //  Keep the cell square (uses x size for both dimensions)
    };

}


#endif //FDTDLIFE_UNITCELLSHAPE_H
