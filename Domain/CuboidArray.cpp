/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CuboidArray.h"
#include <Fdtd/Model.h>
#include "Material.h"
#include <Xml/DomObject.h>

// Fill the domain with this shape
void domain::CuboidArray::fill(std::vector<uint16_t>& d)
{
    // My material
    Material* myMat = m_->d()->getMaterial(material_);
    if(myMat != nullptr)
    {
        // Build position information
        box::Vector<double> size(size_.value());
        box::Vector<double> center(center_.value());
        box::Vector<double> increment(increment_.value());
        // The array of cuboids
        for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++)
        {
            for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++)
            {
                for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++)
                {
                    // Extent of this cuboid
                    box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * increment;
                    box::Vector<double> p1 = center + offset - size / 2.0;
                    box::Vector<double> p2 = center + offset + size / 2.0;
                    p1 = p1.max(m_->p()->p1().value());
                    p2 = p2.min(m_->p()->p2().value());
                    // Convert space coords to grid coords
                    box::Vector<size_t> cell1 = m_->p()->geometry()->cellFloor(p1);
                    box::Vector<size_t> cell2 = m_->p()->geometry()->cellCeil(p2);
                    if(cell1.x() == cell2.x())
                    { cell2.x(cell2.x() + 1); }
                    if(cell1.y() == cell2.y())
                    { cell2.y(cell2.y() + 1); }
                    if(cell1.z() == cell2.z())
                    { cell2.z(cell2.z() + 1); }
                    // Plant the cuboid
                    for(size_t i = cell1.x(); i < cell2.x(); i++)
                    {
                        for(size_t j = cell1.y(); j < cell2.y(); j++)
                        {
                            for(size_t k = cell1.z(); k < cell2.z(); k++)
                            {
                                Material* existingMat = m_->d()->getMaterial(
                                        d[m_->p()->geometry()->index(i, j, k)]);
                                if(existingMat == nullptr
                                   || myMat->priority() >= existingMat->priority())
                                {
                                    d[m_->p()->geometry()->index(i, j, k)] =
                                            static_cast<uint16_t>(material_);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

// Write the configuration to the DOM object
void domain::CuboidArray::writeConfig(xml::DomObject& p) const
{
    // Base class first
    Shape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    CenterPosShape::writeConfig(p);
    SizeShape::writeConfig(p);
}

// Read the configuration from the DOM object
void domain::CuboidArray::readConfig(xml::DomObject& p)
{
    // Base class first
    Shape::readConfig(p);
    DuplicatingShape::readConfig(p);
    CenterPosShape::readConfig(p);
    SizeShape::readConfig(p);
}

// Convert to an export format (dimensions are written in mm)
void domain::CuboidArray::write(box::ExportWriter& writer,
                                const std::set<int>& selectedMaterials)
{
    Material* mat = m_->d()->getMaterial(material_);
    if(mat != nullptr && selectedMaterials.count(mat->index()))
    {
        // Create the material
        writer.createMaterial(mat->epsilon().value() / box::Constants::epsilon0_,
                              mat->mu().value() / box::Constants::mu0_, mat->name());
        // Build position information
        box::Vector<double> size(size_.value());
        box::Vector<double> center(center_.value());
        box::Vector<double> increment(increment_.value());
        // The array of cuboids
        for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++)
        {
            for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++)
            {
                for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++)
                {
                    // Extent of this cuboid
                    box::Vector<double> offset =
                            box::Vector<double>(ri, rj, rk) * increment;
                    box::Vector<double> p1 = center + offset - size / 2.0;
                    box::Vector<double> p2 = center + offset + size / 2.0;
                    writer.writeCuboid(p1 / box::Constants::milli_,
                                       p2 / box::Constants::milli_, name_, mat->name());
                }
            }
        }
    }
}

// Evaluate any expressions
void domain::CuboidArray::evaluate(box::Expression::Context& c)
{
    DuplicatingShape::evaluate(c);
    CenterPosShape::evaluate(c);
    SizeShape::evaluate(c);
}
