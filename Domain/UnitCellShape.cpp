/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "UnitCellShape.h"
#include <Xml/DomObject.h>

// Write the configuration to the DOM object
void domain::UnitCellShape::writeConfig(xml::DomObject& p) const {
    Shape::writeConfig(p);
    CenterPosShape::writeConfig(p);
    p << xml::Reopen();
    p << xml::Obj("cellsize") << cellSizeX_;
    p << xml::Obj("cellsizey") << cellSizeY_;
    p << xml::Obj("square") << square_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::UnitCellShape::readConfig(xml::DomObject& p) {
    Shape::readConfig(p);
    CenterPosShape::readConfig(p);
    p >> xml::Reopen();
    p >> xml::Obj("cellsize") >> cellSizeX_;
    p >> xml::Obj("cellsizey") >> xml::Default(0.0) >> cellSizeY_;
    p >> xml::Obj("square") >> xml::Default(true) >> square_;
    p >> xml::Close();
}

// Evaluate any expressions
void domain::UnitCellShape::evaluate(box::Expression::Context& c) {
    Shape::evaluate(c);
    CenterPosShape::evaluate(c);
    cellSizeX_.evaluate(c);
    cellSizeY_.evaluate(c);
}

// Set the X dimension
void domain::UnitCellShape::cellSizeX(const box::Expression& v) {
    cellSizeX_ = v;
    if(square_) {
        cellSizeY_ = v;
    }
}

// Set the Y dimension
void domain::UnitCellShape::cellSizeY(const box::Expression& v) {
    if(!square_) {
        cellSizeY_ = v;
    }
}

// Set the square flag
void domain::UnitCellShape::square(bool v) {
    if(square_ && !v) {
        // Changing to square, fix up the y dimension
        cellSizeY_ = cellSizeX_;
    }
    square_ = v;
}
