/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SQUAREHOLE_H
#define FDTDLIFE_SQUAREHOLE_H

#include "AdmittanceLayerShape.h"
#include "CenterPosShape.h"
#include "RepeatCellShape.h"
#include "ThinLayerShape.h"
#include "DuplicatingShape.h"
#include <Box/Expression.h>

namespace domain {

    // A square metallic plate with a square hole in the center of the unit cell
    // with the minimum plate thickness.
    class SquareHole
            : public AdmittanceLayerShape
              , public RepeatCellShape, public DuplicatingShape, public ThinLayerShape {
    public:
        // Construction
        SquareHole() = default;

        // Overrides of AdmittanceLayerShape
        void fill(std::vector<uint16_t>& d) override;
        Material* match(fdtd::PropMatrix* m, Material* other, double frequency) override;
        std::complex<double> admittance(double frequency) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;
        void evaluate(box::Expression::Context& c) override;

        // Getters
        [[nodiscard]] double holeOffsetX() const { return holeOffsetX_; }
        [[nodiscard]] double holeOffsetY() const { return holeOffsetY_; }
        [[nodiscard]] const box::Expression& holeSize() const { return holeSize_; }

        // Setters
        void holeOffsetX(double v) { holeOffsetX_ = v; }
        void holeOffsetY(double v) { holeOffsetY_ = v; }
        void holeSize(const box::Expression& v) { holeSize_ = v; }

    protected:
        // Parameters
        box::Expression holeSize_{50.0};  // Size of hole as percentage of the unit cell
        double holeOffsetX_{0.0};  // Offset in the X direction from the center of the cell
        double holeOffsetY_{0.0};  // Offset in the Y direction from the center of the cell

        // Helpers
        void fillOneHole(std::vector<uint16_t>& d, Material* myMat, Material* fillMat,
                         const box::Vector<double>& p);
    };
}


#endif //FDTDLIFE_SQUAREHOLE_H
