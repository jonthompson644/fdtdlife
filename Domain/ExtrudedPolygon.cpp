/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ExtrudedPolygon.h"
#include <Fdtd/Model.h>
#include "Material.h"
#include <Xml/DomObject.h>
#include <Box/Line2D.h>
#include <algorithm>

// Fill the domain with this shape
void domain::ExtrudedPolygon::fill(std::vector<uint16_t>& d) {
    if(points_.size() >= 3) {
        // Use a raster algorithm to fill the domain.
        // Remember we are extruding in the y direction.
        size_t width = m_->p()->geometry()->n().x();
        // Make the list of lines that make the polygon
        box::Point2D<double> offset(center_.x(), center_.z());
        std::vector<box::Line2D<double>> lines;
        box::Point2D<double> a = points_.back() + offset;
        box::Point2D<double> b;
        for(auto& p : points_) {
            b = p + offset;
            lines.emplace_back(a, b);
            a = b;
        }
        // For each line in the raster...
        for(size_t z = 0; z < m_->p()->geometry()->n().z(); z++) {
            // Get the line that runs through the center of this row of pixels
            box::Vector<double> left = m_->p()->geometry()->cellCenter({0, 0, z});
            box::Vector<double> right = m_->p()->geometry()->cellCenter(
                    {width - 1, 0, z});
            box::Line2D<double> rasterLine({left.x(), left.z()}, {right.x(), right.z()});
            // Make the list of intersections between the edges and this line
            std::vector<box::Point2D<double>> intersections;
            rasterLine.intersections(lines, intersections);
            // Sort them into ascending x order
            std::sort(intersections.begin(), intersections.end(),
                      [](const box::Point2D<double>& a, const box::Point2D<double>& b) {
                          return a.x() < b.x();
                      });
            // My material
            Material* myMat = m_->d()->getMaterial(material_);
            if(myMat != nullptr) {
                // Initialise the point traversing
                size_t p = 0;
                bool inside = false;
                // For each cell in the raster...
                for(size_t x = 0; x < width; x++) {
                    box::Vector<double> cell = m_->p()->geometry()->cellCenter({x, 0, z});
                    while(p < intersections.size() && cell.x() >= intersections[p].x()) {
                        inside = !inside;
                        p++;
                    }
                    if(inside) {
                        for(size_t y = 0; y < m_->p()->geometry()->n().y(); y++) {
                            Material* existingMat =
                                    m_->d()->getMaterial(
                                            d[m_->p()->geometry()->index(x, y, z)]);
                            if(existingMat == nullptr
                               || myMat->priority() >= existingMat->priority()) {
                                d[m_->p()->geometry()->index(x, y, z)] =
                                        static_cast<uint16_t>(material_);
                            }
                        }
                    }
                }
            }
        }
    }
}

// Write the configuration to the DOM object
void domain::ExtrudedPolygon::writeConfig(xml::DomObject& p) const {
    // Base class first
    Shape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("centerx") << center_.x();
    p << xml::Obj("centery") << center_.y();
    p << xml::Obj("centerz") << center_.z();
    for(auto& point : points_) {
        p << xml::Open("point");
        p << xml::Obj("x") << point.x();
        p << xml::Obj("y") << point.y();
        p << xml::Close("point");
    }
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::ExtrudedPolygon::readConfig(xml::DomObject& p) {
    // Base class first
    Shape::readConfig(p);
    // Now my things
    double v, x, y;
    p >> xml::Reopen();
    p >> xml::Obj("centerx") >> v;
    center_.x(v);
    p >> xml::Obj("centery") >> v;
    center_.y(v);
    p >> xml::Obj("centerz") >> v;
    center_.z(v);
    points_.clear();
    for(auto& o : p.obj("point")) {
        *o >> xml::Obj("x") >> x;
        *o >> xml::Obj("y") >> y;
        points_.emplace_back(x, y);
    }
    p >> xml::Close();
}
