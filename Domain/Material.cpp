/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <iostream>
#include <complex>
#include "Material.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Xml/Exception.h>
#include <Box/Constants.h>

// Constructor
domain::Material::Material() {
}

// Called by the factory as the second stage of construction
void domain::Material::construct(int mode, int id, fdtd::Model* m) {
    mode_ = mode;
    index_ = id;
    m_ = m;
    epsilon_.value(box::Constants::epsilon0_);
    mu_.value(box::Constants::mu0_);
    sigma_.value(box::Constants::sigma0_);
    sigmastar_.value(box::Constants::sigmastar0_);
}

// Pre-calculate the material constants
void domain::Material::initialise() {
    evaluate();
    cae_ = (m_->p()->dt() / epsilon_.value()) /
            (1 + sigma_.value() * m_->p()->dt() / 2 / epsilon_.value());
    cbe_ = (1 - sigma_.value() * m_->p()->dt() / 2 / epsilon_.value()) /
           (1 + sigma_.value() * m_->p()->dt() / 2 / epsilon_.value());
    cah_ = (m_->p()->dt() / mu_.value()) /
            (1 + sigmastar_.value() * m_->p()->dt() / 2 / mu_.value());
    cbh_ = (1 - sigmastar_.value() * m_->p()->dt() / 2 / mu_.value()) /
           (1 + sigmastar_.value() * m_->p()->dt() / 2 / mu_.value());
}

// Print the material
void domain::Material::print() {
    std::cout << "    " << name_ << ", index=" << index_ << ", pri=" << priority_
              << ", epsilon=" << epsilon_.value() << ", sigma=" << sigma_.value()
              << ", mu=" << mu_.value() << ", sigmastar=" << sigmastar_.value()
              << ", cae=" << cae_ << ", cbe=" << cbe_
              << ", cah=" << cah_ << ", cbh=" << cbh_ << std::endl;
}

// Apply the propagation matrix for this material
void domain::Material::propagate(fdtd::PropMatrix* m, double distance, double frequency) {
    double k = 2.0 * box::Constants::pi_ * frequency * sqrt(mu_.value() * epsilon_.value());
    std::complex<double> v(cos(k * distance), sin(k * distance));
    *m = *m * fdtd::PropMatrix(v);
}

// Apply the matching matrix from the other material to this one
void domain::Material::match(fdtd::PropMatrix* m, Material* other) {
    double eta = sqrt(other->mu_.value() / other->epsilon_.value());
    double etaprime = sqrt(mu_.value() / epsilon_.value());
    *m = *m * fdtd::PropMatrix(eta, etaprime);
}

// Write the material to the XML DOM
void domain::Material::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("name") << name_;
    root << xml::Obj("identifier") << index_;
    root << xml::Obj("epsilon") << epsilon_;
    root << xml::Obj("mu") << mu_;
    root << xml::Obj("sigma") << sigma_;
    root << xml::Obj("sigmastar") << sigmastar_;
    root << xml::Obj("color") << color_;
    root << xml::Obj("priority") << priority_;
    root << xml::Obj("seqgenerated") << seqGenerated_;
    root << xml::Obj("mode") << mode_;
    root << xml::Close();
}

// Read the material from the XML DOM
void domain::Material::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("epsilon") >> epsilon_;
    root >> xml::Obj("mu") >> mu_;
    root >> xml::Obj("sigma") >> sigma_;
    root >> xml::Obj("sigmastar") >> sigmastar_;
    root >> xml::Obj("color") >> color_;
    root >> xml::Obj("priority") >> xml::Default(priority_) >> priority_;
    root >> xml::Obj("seqgenerated") >> xml::Default(false) >> seqGenerated_;
    root >> xml::Close();
    // Mode and index are read by the factory
}

// Return the coordinate stretch factor.
box::Vector<double> domain::Material::stretch(
        const box::Vector<size_t>& /*boundaryDepth*/) const {
    return {1.0, 1.0, 1.0};
}

// Evaluate the expression members
void domain::Material::evaluate() {
    box::Expression::Context c;
    m_->variables().fillContext(c);
    try {
        epsilon_.evaluate(c);
        mu_.evaluate(c);
        sigma_.evaluate(c);
        sigmastar_.evaluate(c);
    } catch(box::Expression::Exception& e) {
        std::cout << e.what() << std::endl;
    }
}

