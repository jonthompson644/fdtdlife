/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "SquarePlate.h"
#include "Material.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Box/DxfWriter.h>

// Fill the domain with the material representing this shape
void domain::SquarePlate::fill(std::vector<uint16_t>& d) {
    // The plate material
    Material* myMat;
    if(m_->p()->useThinSheetSubcells()) {
        // Make a thin sheet material for this shape
        myMat = m_->d()->makeThinSheetMaterial(fillMaterial_,
                                               material_, this).get();
    } else {
        myMat = m_->d()->getMaterial(material_);
    }
    Material* fillMat = m_->d()->getMaterial(fillMaterial_);
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // The array of plates
    for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
        for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
            for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                fillOnePlate(d, myMat, fillMat, p + offset);
            }
        }
    }
}

// Fill the domain with one occurrence of the plate
void domain::SquarePlate::fillOnePlate(std::vector<uint16_t>& d, Material* myMat,
                                       Material* fillMat,
                                       const box::Vector<double>& p) {
    // Some global parameters
    box::Vector<double> unitCell(cellSizeX_.value(),
                                 square_ ? cellSizeX_.value() : cellSizeY_.value(),
                                 0.0);
    double halfTotalSizeX = unitCell.x() * repeatX_.value() / 2.0;
    double halfTotalSizeY = unitCell.y() * repeatY_.value() / 2.0;
    box::Vector<double> totalTopLeft = p - box::Vector<double>(
            halfTotalSizeX, halfTotalSizeY, 0.0);
    totalTopLeft.z(0.0);
    box::Vector<double> totalBottomRight = p + box::Vector<double>(
            halfTotalSizeX, halfTotalSizeY, 0.0);
    totalBottomRight.z(0.0);
    box::Vector<double> totalSize = box::Vector<double>(
            2 * halfTotalSizeX, 2 * halfTotalSizeY, 0.0);
    box::Vector<double> repeatSize = totalSize / box::Vector<double>(
            repeatX_.value(), repeatY_.value(), 1.0);
    double plateSizeX = unitCell.x() * plateSize_.value() / 100.0;
    double plateSizeY = unitCell.y() * plateSize_.value() / 100.0;
    double borderSizeX = (unitCell.x() - plateSizeX) / 2.0;
    double borderSizeY = (unitCell.y() - plateSizeY) / 2.0;
    // Iterate over the cells in the z layer that the structure resides
    box::Vector<size_t> cellPos = m_->p()->geometry()->cellFloor(p);
    box::Vector<size_t> n = m_->p()->geometry()->n();
    size_t k = cellPos.z();
    for(size_t i = 0; i < n.x(); i++) {
        for(size_t j = 0; j < n.y(); j++) {
            // The domain position of the cell
            box::Vector<double> pos = m_->p()->geometry()->cellCenter({i, j, k});
            pos.z(0.0);
            // Calculate some position dependent values
            box::Vector<double> offsetIntoRepeatCell = pos - totalTopLeft;
            offsetIntoRepeatCell = offsetIntoRepeatCell +
                                   box::Vector<double>(plateOffsetX_,
                                                       plateOffsetY_, 0.0);
            //offsetIntoRepeatCell.modulo(repeatSize);
            offsetIntoRepeatCell.z(0.0);
            box::Vector<double> repeatNumber =
                    (offsetIntoRepeatCell / repeatSize).floor<double>();
            repeatNumber.z(0.0);
            box::Vector<double> offsetIntoUnitCell =
                    offsetIntoRepeatCell - repeatNumber * repeatSize;
            offsetIntoUnitCell.z(0.0);
            box::Vector<double> holeTopLeftOffset = box::Vector<double>(
                    borderSizeX, borderSizeY, 0.0);
            box::Vector<double> holeBottomRightOffset = box::Vector<double>(
                    borderSizeX + plateSizeX, borderSizeY + plateSizeY, 0.0);
            bool requiresMat = (pos >= totalTopLeft && pos <= totalBottomRight);
            requiresMat = requiresMat && (offsetIntoUnitCell >= holeTopLeftOffset &&
                                          offsetIntoUnitCell <= holeBottomRightOffset);
            Material* existingMat =
                    m_->d()->getMaterial(d[m_->p()->geometry()->index(i, j, k)]);
            if(requiresMat) {
                // This is the plate
                if(myMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(material_);
                } else if(existingMat == nullptr
                          || myMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(myMat->index());
                }
            } else {
                // This is the gap
                if(fillMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMaterial_);
                } else if(existingMat == nullptr
                          || fillMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMat->index());
                }
            }
        }
    }
}

// Apply the matching matrix for this shape's material
domain::Material* domain::SquarePlate::match(fdtd::PropMatrix* m, Material* other,
                                             double frequency) {
    std::complex<double> Yc;
    Yc = admittance(frequency);
    *m = *m * fdtd::PropMatrix(1.0 + Yc, Yc, -Yc, 1.0 - Yc);
    return other;
}

// Return the admittance of the square plate at the specified frequency
std::complex<double> domain::SquarePlate::admittance(double frequency) {
    return m_->p()->plateAdmittance().admittance(
            fdtd::PlateAdmittance::plateTypeSquarePlate,
            frequency, cellSizeX_.value(), plateSize_.value());
}

// Write the configuration to the DOM object
void domain::SquarePlate::writeConfig(xml::DomObject& p) const {
    // Base class first
    AdmittanceLayerShape::writeConfig(p);
    RepeatCellShape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    ThinLayerShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("platesize") << plateSize_;
    p << xml::Obj("offsetx") << plateOffsetX_;
    p << xml::Obj("offsety") << plateOffsetY_;
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::SquarePlate::readConfig(xml::DomObject& p) {
    // Base class first
    AdmittanceLayerShape::readConfig(p);
    RepeatCellShape::readConfig(p);
    DuplicatingShape::readConfig(p);
    ThinLayerShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("platesize") >> plateSize_;
    p >> xml::Obj("offsetx") >> plateOffsetX_;
    p >> xml::Obj("offsety") >> plateOffsetY_;
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::SquarePlate::write(box::ExportWriter& writer,
                                const std::set<int>& selectedMaterials) {
    // Some global parameters
    box::Vector<double> unitCell(cellSizeX_.value(),
                                 square_ ? cellSizeX_.value() : cellSizeY_.value(),
                                 0.0);
    double totalSizeX = unitCell.x() * repeatX_.value();
    double totalSizeY = unitCell.y() * repeatY_.value();
    double plateSizeX = unitCell.x() * plateSize_.value() / 100.0;
    double plateSizeY = unitCell.y() * plateSize_.value() / 100.0;
    double borderSizeX = (unitCell.x() - plateSizeX) / 2.0;
    double borderSizeY = (unitCell.y() - plateSizeY) / 2.0;
    // The materials
    bool plantOn = selectedMaterials.count(material_) > 0;
    bool plantOff = selectedMaterials.count(fillMaterial_) > 0 &&
            fillMaterial_ != domain::Domain::defaultMaterial;
    std::string onName("METAL");
    std::string offName("VACUUM");
    // Iterate over the repeats
    box::Vector<double> bottomLeft = center_.value() - box::Vector<double>(
            totalSizeX / 2.0, totalSizeY / 2.0, 0);
    box::Vector<double> p1;
    box::Vector<double> p2;
    for(size_t ni = 0; ni < repeatX_.value(); ni++) {
        for(size_t nj = 0; nj < repeatY_.value(); nj++) {
            p1 = bottomLeft * 1000.0;
            p2 = (bottomLeft +
                  box::Vector<double>(borderSizeX, unitCell.y(),
                                      layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOff) {
                writer.writeCuboid(p1, p2, name_, offName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSizeX,
                                                   borderSizeY + plateSizeY, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSizeX + plateSizeX, unitCell.y(),
                    layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOff) {
                writer.writeCuboid(p1, p2, name_, offName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSizeX, borderSizeY, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSizeX + plateSizeX, borderSizeY + plateSizeY,
                    layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOn) {
                writer.writeCuboid(p1, p2, name_, onName);
            }
            p1 = (bottomLeft + box::Vector<double>(borderSizeX, 0, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft + box::Vector<double>(
                    borderSizeX + plateSizeX, borderSizeY, layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOff) {
                writer.writeCuboid(p1, p2, name_, offName);
            }
            p1 = (bottomLeft + box::Vector<double>(
                    borderSizeX + plateSizeX, 0, 0))
                 / box::Constants::milli_;
            p2 = (bottomLeft +
                  box::Vector<double>(unitCell.x(), unitCell.y(),
                                      layerThickness_.value()))
                 / box::Constants::milli_;
            if(plantOff) {
                writer.writeCuboid(p1, p2, name_, offName);
            }
            bottomLeft.y(bottomLeft.y() + unitCell.y());
        }
        bottomLeft.x(bottomLeft.x() + unitCell.x());
        bottomLeft.y(center_.value().y() - totalSizeY / 2.0);
    }
}

// Evaluate expressions
void domain::SquarePlate::evaluate(box::Expression::Context& c) {
    AdmittanceLayerShape::evaluate(c);
    RepeatCellShape::evaluate(c);
    DuplicatingShape::evaluate(c);
    ThinLayerShape::evaluate(c);
    plateSize_.evaluate(c);
}

// Populate the layers list with information from this shape
void domain::SquarePlate::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    DuplicatingShape::doMakeLayers(center_.z().value(), this, layers);
}
