/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETERTABLE_H
#define FDTDLIFE_PARAMETERTABLE_H

#include <list>
#include <memory>
#include <Xml/DomObject.h>
#include <Box/Factory.h>
#include "ParameterCurve.h"

namespace domain {
    // A table of parameters for a particular configuration
    // of metamaterial mesh.
    class ParameterTable {
    public:
        // Constants
        enum {
            modeParameterCurve
        };

        // Construction
        ParameterTable() = default;
        virtual void construct(int mode, int identifier);
        virtual ~ParameterTable() = default;

        // API
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const ParameterTable& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, ParameterTable& v) {
            v.readConfig(o);
            return o;
        }
        ParameterCurve* make();
        void remove(ParameterCurve* item);
        ParameterCurve* find(int identifier);
        ParameterCurve* find(ParameterCurve::Which which);
        std::list<double> frequencies(ParameterCurve::Which which);
        void clear();
        [[nodiscard]] size_t size() const { return curves_.size(); }
        typedef std::list<std::unique_ptr<ParameterCurve>>::iterator iterator;
        iterator begin() { return curves_.begin(); }
        iterator end() { return curves_.end(); }
        bool has(ParameterCurve::Which which);
        void interpolateIncidenceAngle(ParameterCurve::Which which, ParameterTable* below,
                                       ParameterTable* above, double incidenceAngle);
        std::complex<double> valueAt(double frequency, ParameterCurve::Which which);

        // Getters
        [[nodiscard]] int identifier() const { return identifier_; }
        [[nodiscard]] int mode() const { return mode_; }
        [[nodiscard]] double unitCell() const { return unitCell_; }
        [[nodiscard]] double layerSpacing() const { return layerSpacing_; }
        [[nodiscard]] double incidenceAngle() const { return incidenceAngle_; }
        [[nodiscard]] const std::string& name() const { return name_; }
        [[nodiscard]] double patchRatio() const { return patchRatio_; }

        // Setters
        void unitCell(double v) { unitCell_ = v; }
        void layerSpacing(double v) { layerSpacing_ = v; }
        void name(const std::string& v) { name_ = v; }
        void incidenceAngle(double v) { incidenceAngle_ = v; }
        void patchRatio(double v) { patchRatio_ = v; }

    protected:
        // Members
        int identifier_{-1};  // The unique identifier
        int mode_{};  // The type of table
        std::string name_;  // The name of the table
        double unitCell_{50e-6};  // The unit cell the information was recorded at
        double layerSpacing_{100e-6};  // The layer spacing the information was recorded at
        double incidenceAngle_{0.0};  // The angle of incidence the information was recorded
        double patchRatio_{0.0};  // The patch ratio of the pattern
        std::list<std::unique_ptr<ParameterCurve>> curves_;  // The parameter curves
        box::Factory<ParameterCurve> factory_;  // The item factory
    };
}


#endif //FDTDLIFE_PARAMETERTABLE_H
