/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETERPOINT_H
#define FDTDLIFE_PARAMETERPOINT_H

#include <complex>
#include <Xml/DomObject.h>

namespace domain {

    //  A class that represents a single point in a parameter curve
    class ParameterPoint {
    public:
        // Construction
        ParameterPoint() = default;
        virtual void construct(int mode, int identifier);
        virtual ~ParameterPoint() = default;

        // API
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const ParameterPoint& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, ParameterPoint& v) {
            v.readConfig(o);
            return o;
        }

        // Getters
        [[nodiscard]] double frequency() const {return frequency_;}
        [[nodiscard]] std::complex<double> value() const {return value_;}
        [[nodiscard]] int identifier() const { return identifier_; }
        [[nodiscard]] int mode() const { return mode_; }

        // Setters
        void frequency(double v) {frequency_ = v;}
        void value(std::complex<double> v) {value_ = v;}

    protected:
        // Members
        int identifier_ {-1};  // The unique identifier
        int mode_ {};  // The type of table
        double frequency_ {};  // The frequency
        std::complex<double> value_ {};  // The parameter value
    };
}


#endif //FDTDLIFE_PARAMETERPOINT_H
