/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "JigsawLayer.h"
#include "Material.h"
#include <Fdtd/Model.h>
#include <Fdtd/PropMatrix.h>
#include <Xml/DomObject.h>
#include <Box/DxfWriter.h>

// Fill the domain with the material representing this shape
void domain::JigsawLayer::fill(std::vector<uint16_t>& d) {
    // Clear the cache
    cacheBefore_ = nullptr;
    cacheAfter_ = nullptr;
    // The plate material
    Material* myMat;
    if(m_->p()->useThinSheetSubcells()) {
        // Make a thin sheet material for this shape
        myMat = m_->d()->makeThinSheetMaterial(fillMaterial_,
                                               material_, this).get();
    } else {
        myMat = m_->d()->getMaterial(material_);
    }
    Material* fillMat = m_->d()->getMaterial(fillMaterial_);
    // The position
    box::Vector<double> p(center_.value());
    box::Vector<double> dp(increment_.value());
    // The array of plates
    for(size_t ri = 0; ri < static_cast<size_t>(duplicate_.value().x()); ri++) {
        for(size_t rj = 0; rj < static_cast<size_t>(duplicate_.value().y()); rj++) {
            for(size_t rk = 0; rk < static_cast<size_t>(duplicate_.value().z()); rk++) {
                box::Vector<double> offset = box::Vector<double>(ri, rj, rk) * dp;
                fillOnePlate(d, myMat, fillMat, p + offset);
            }
        }
    }
}

// Fill the domain with one occurrence of the jigsaw plate
void domain::JigsawLayer::fillOnePlate(std::vector<uint16_t>& d, Material* myMat,
                                       Material* fillMat,
                                       const box::Vector<double>& p) {
    // The cell size
    double halfG = cellSizeX_.value() * box::Constants::half_;
    box::Vector<double> unitCellTL = p - box::Vector<double>(halfG, halfG, 0.0);
    box::Vector<double> unitCellBR = p + box::Vector<double>(halfG, halfG, 0.0);
    // Position of the central plate
    double halfP = plateSize_.value() * box::Constants::half_;
    box::Vector<double> plateTL = p - box::Vector<double>(halfP, halfP, 0.0);
    box::Vector<double> plateBR = p + box::Vector<double>(halfP, halfP, 0.0);
    // Position of the indents
    double halfIw = indentWidth_.value() * box::Constants::half_;
    double ip = indentPos_.value();
    double is = indentSize_.value();
    box::Vector<double> ind1TL = p + box::Vector<double>(-ip - halfIw, halfG - is, 0);
    box::Vector<double> ind1BR = p + box::Vector<double>(-ip + halfIw, halfP, 0);
    box::Vector<double> ind2TL = p + box::Vector<double>(halfG - is, -ip - halfIw, 0);
    box::Vector<double> ind2BR = p + box::Vector<double>(halfG, -ip + halfIw, 0);
    // Position of the second indents
    double ip2 = indent2Pos_.value();
    double is2 = indent2Size_.value();
    box::Vector<double> ind21TL = p + box::Vector<double>(-ip2 - halfIw, halfG - is2, 0);
    box::Vector<double> ind21BR = p + box::Vector<double>(-ip2 + halfIw, halfP, 0);
    box::Vector<double> ind22TL = p + box::Vector<double>(halfG - is2, -ip2 - halfIw, 0);
    box::Vector<double> ind22BR = p + box::Vector<double>(halfG, -ip2 + halfIw, 0);
    // Position of the third indents
    double ip3 = indent3Pos_.value();
    double is3 = indent3Size_.value();
    box::Vector<double> ind31TL = p + box::Vector<double>(-ip3 - halfIw, halfG - is3, 0);
    box::Vector<double> ind31BR = p + box::Vector<double>(-ip3 + halfIw, halfP, 0);
    box::Vector<double> ind32TL = p + box::Vector<double>(halfG - is3, -ip3 - halfIw, 0);
    box::Vector<double> ind32BR = p + box::Vector<double>(halfG, -ip3 + halfIw, 0);
    // Position of the protrusions
    double halfPw = protrusionWidth_.value() * box::Constants::half_;
    double ps = protrusionSize_.value();
    box::Vector<double> prot1TL = p + box::Vector<double>(-ip - halfPw, -ps, 0);
    box::Vector<double> prot1BR = p + box::Vector<double>(-ip + halfPw, -halfP, 0);
    box::Vector<double> prot2TL = p + box::Vector<double>(-ps, -ip - halfPw, 0);
    box::Vector<double> prot2BR = p + box::Vector<double>(-halfP, -ip + halfPw, 0);
    // Position of the second protrusions
    double ps2 = protrusion2Size_.value();
    box::Vector<double> prot21TL = p + box::Vector<double>(-ip2 - halfPw, -ps2, 0);
    box::Vector<double> prot21BR = p + box::Vector<double>(-ip2 + halfPw, -halfP, 0);
    box::Vector<double> prot22TL = p + box::Vector<double>(-ps2, -ip2 - halfPw, 0);
    box::Vector<double> prot22BR = p + box::Vector<double>(-halfP, -ip2 + halfPw, 0);
    // Position of the third protrusions
    double ps3 = protrusion3Size_.value();
    box::Vector<double> prot31TL = p + box::Vector<double>(-ip3 - halfPw, -ps3, 0);
    box::Vector<double> prot31BR = p + box::Vector<double>(-ip3 + halfPw, -halfP, 0);
    box::Vector<double> prot32TL = p + box::Vector<double>(-ps3, -ip3 - halfPw, 0);
    box::Vector<double> prot32BR = p + box::Vector<double>(-halfP, -ip3 + halfPw, 0);
    // Position of wrapped around protrusions
    bool hasWrapped1 = false;
    bool hasWrapped2 = false;
    box::Vector<double> p1 = m_->p()->p1().value();
    box::Vector<double> p2 = m_->p()->p2().value();
    box::Vector<double> wprot1TL = prot1TL;
    if(wprot1TL.y() < p1.y()) {
        wprot1TL.y(wprot1TL.y() + p2.y() - p1.y());
        hasWrapped1 = true;
    }
    box::Vector<double> wprot1BR = box::Vector<double>(prot1BR.x(), p2.y(), 0);
    box::Vector<double> wprot2TL = prot2TL;
    if(wprot2TL.x() < p1.x()) {
        wprot2TL.x(wprot2TL.x() + p2.x() - p1.x());
        hasWrapped2 = true;
    }
    box::Vector<double> wprot2BR = box::Vector<double>(p2.x(), prot2BR.y(), 0);
    // Position of wrapped around second protrusions
    bool hasWrapped21 = false;
    bool hasWrapped22 = false;
    box::Vector<double> wprot21TL = prot21TL;
    if(wprot21TL.y() < p1.y()) {
        wprot21TL.y(wprot21TL.y() + p2.y() - p1.y());
        hasWrapped21 = true;
    }
    box::Vector<double> wprot21BR = box::Vector<double>(prot21BR.x(), p2.y(), 0);
    box::Vector<double> wprot22TL = prot22TL;
    if(wprot22TL.x() < p1.x()) {
        wprot22TL.x(wprot22TL.x() + p2.x() - p1.x());
        hasWrapped22 = true;
    }
    box::Vector<double> wprot22BR = box::Vector<double>(p2.x(), prot22BR.y(), 0);
    // Position of wrapped around third protrusions
    bool hasWrapped31 = false;
    bool hasWrapped32 = false;
    box::Vector<double> wprot31TL = prot31TL;
    if(wprot31TL.y() < p1.y()) {
        wprot31TL.y(wprot31TL.y() + p2.y() - p1.y());
        hasWrapped31 = true;
    }
    box::Vector<double> wprot31BR = box::Vector<double>(prot31BR.x(), p2.y(), 0);
    box::Vector<double> wprot32TL = prot32TL;
    if(wprot32TL.x() < p1.x()) {
        wprot32TL.x(wprot32TL.x() + p2.x() - p1.x());
        hasWrapped32 = true;
    }
    box::Vector<double> wprot32BR = box::Vector<double>(p2.x(), prot32BR.y(), 0);
    // Iterate over the cells in the z layer that the structure resides
    box::Vector<size_t> n = m_->p()->geometry()->n();
    size_t k = m_->p()->geometry()->cellFloor(p).z();
    for(size_t i = 0; i < n.x(); i++) {
        for(size_t j = 0; j < n.y(); j++) {
            // The domain position of the cell
            box::Vector<double> pos = m_->p()->geometry()->cellCenter({i, j, k});
            // Is this location inside the unit cell?
            bool inUnitCell = pos.x() >= unitCellTL.x() &&
                              pos.x() <= unitCellBR.x() &&
                              pos.y() >= unitCellTL.y() &&
                              pos.y() <= unitCellBR.y();
            // Is this location inside the central square?
            bool inCentralSquare = pos.x() >= plateTL.x() &&
                                   pos.x() <= plateBR.x() &&
                                   pos.y() >= plateTL.y() &&
                                   pos.y() <= plateBR.y();
            // Is this location inside indents
            bool inIndent1 = pos.x() >= ind1TL.x() &&
                             pos.x() <= ind1BR.x() &&
                             pos.y() >= ind1TL.y() &&
                             pos.y() <= ind1BR.y();
            bool inIndent2 = pos.x() >= ind2TL.x() &&
                             pos.x() <= ind2BR.x() &&
                             pos.y() >= ind2TL.y() &&
                             pos.y() <= ind2BR.y();
            // Is this location inside the second indents
            bool inIndent21 = useSecondProtrusion_ &&
                              pos.x() >= ind21TL.x() &&
                              pos.x() <= ind21BR.x() &&
                              pos.y() >= ind21TL.y() &&
                              pos.y() <= ind21BR.y();
            bool inIndent22 = useSecondProtrusion_ &&
                              pos.x() >= ind22TL.x() &&
                              pos.x() <= ind22BR.x() &&
                              pos.y() >= ind22TL.y() &&
                              pos.y() <= ind22BR.y();
            // Is this location inside the third indents
            bool inIndent31 = useThirdProtrusion_ &&
                              pos.x() >= ind31TL.x() &&
                              pos.x() <= ind31BR.x() &&
                              pos.y() >= ind31TL.y() &&
                              pos.y() <= ind31BR.y();
            bool inIndent32 = useThirdProtrusion_ &&
                              pos.x() >= ind32TL.x() &&
                              pos.x() <= ind32BR.x() &&
                              pos.y() >= ind32TL.y() &&
                              pos.y() <= ind32BR.y();
            // Is this location inside the protrusions
            bool inProtrusion1 = pos.x() >= prot1TL.x() &&
                                 pos.x() <= prot1BR.x() &&
                                 pos.y() >= prot1TL.y() &&
                                 pos.y() <= prot1BR.y();
            bool inProtrusion2 = pos.x() >= prot2TL.x() &&
                                 pos.x() <= prot2BR.x() &&
                                 pos.y() >= prot2TL.y() &&
                                 pos.y() <= prot2BR.y();
            // Is this location inside the second protrusions
            bool inProtrusion21 = useSecondProtrusion_ &&
                                  pos.x() >= prot21TL.x() &&
                                  pos.x() <= prot21BR.x() &&
                                  pos.y() >= prot21TL.y() &&
                                  pos.y() <= prot21BR.y();
            bool inProtrusion22 = useSecondProtrusion_ &&
                                  pos.x() >= prot22TL.x() &&
                                  pos.x() <= prot22BR.x() &&
                                  pos.y() >= prot22TL.y() &&
                                  pos.y() <= prot22BR.y();
            // Is this location inside the third protrusions
            bool inProtrusion31 = useThirdProtrusion_ &&
                                  pos.x() >= prot31TL.x() &&
                                  pos.x() <= prot31BR.x() &&
                                  pos.y() >= prot31TL.y() &&
                                  pos.y() <= prot31BR.y();
            bool inProtrusion32 = useThirdProtrusion_ &&
                                  pos.x() >= prot32TL.x() &&
                                  pos.x() <= prot32BR.x() &&
                                  pos.y() >= prot32TL.y() &&
                                  pos.y() <= prot32BR.y();
            // Is this location inside the protrusions wrapped round
            bool inWProtrusion1 = hasWrapped1 && pos.x() >= wprot1TL.x() &&
                                  pos.x() <= wprot1BR.x() &&
                                  pos.y() >= wprot1TL.y() &&
                                  pos.y() <= wprot1BR.y();
            bool inWProtrusion2 = hasWrapped2 && pos.x() >= wprot2TL.x() &&
                                  pos.x() <= wprot2BR.x() &&
                                  pos.y() >= wprot2TL.y() &&
                                  pos.y() <= wprot2BR.y();
            // Is this location inside the second protrusions wrapped round
            bool inWProtrusion21 = useSecondProtrusion_ && hasWrapped21 &&
                                   pos.x() >= wprot21TL.x() &&
                                   pos.x() <= wprot21BR.x() &&
                                   pos.y() >= wprot21TL.y() &&
                                   pos.y() <= wprot21BR.y();
            bool inWProtrusion22 = useSecondProtrusion_ && hasWrapped22 &&
                                   pos.x() >= wprot22TL.x() &&
                                   pos.x() <= wprot22BR.x() &&
                                   pos.y() >= wprot22TL.y() &&
                                   pos.y() <= wprot22BR.y();
            // Is this location inside the third protrusions wrapped round
            bool inWProtrusion31 = useThirdProtrusion_ && hasWrapped31 &&
                                   pos.x() >= wprot31TL.x() &&
                                   pos.x() <= wprot31BR.x() &&
                                   pos.y() >= wprot31TL.y() &&
                                   pos.y() <= wprot31BR.y();
            bool inWProtrusion32 = useThirdProtrusion_ && hasWrapped32 &&
                                   pos.x() >= wprot32TL.x() &&
                                   pos.x() <= wprot32BR.x() &&
                                   pos.y() >= wprot32TL.y() &&
                                   pos.y() <= wprot32BR.y();
            // Do the filling
            Material* existingMat =
                    m_->d()->getMaterial(d[m_->p()->geometry()->index(i, j, k)]);
            bool inPattern = inCentralSquare && !inIndent1 && !inIndent2 &&
                             !inIndent21 && !inIndent22 && !inIndent31 && !inIndent32;
            inPattern = inPattern || inProtrusion1 || inProtrusion2 || inWProtrusion1 ||
                        inWProtrusion2;
            inPattern = inPattern || inProtrusion21 || inProtrusion22 || inWProtrusion21 ||
                        inWProtrusion22;
            inPattern = inPattern || inProtrusion31 || inProtrusion32 || inWProtrusion31 ||
                        inWProtrusion32;
            if(inPattern) {
                // This is the plate
                if(myMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(material_);
                } else if(existingMat == nullptr
                          || myMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(myMat->index());
                }
            } else if(inUnitCell) {
                // This is the gap inside the unit cell
                if(fillMat == nullptr) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMaterial_);
                } else if(existingMat == nullptr
                          || fillMat->priority() >= existingMat->priority()) {
                    d[m_->p()->geometry()->index(i, j, k)] =
                            static_cast<uint16_t>(fillMat->index());
                }
            }
        }
    }
}

// Write the configuration to the DOM object
void domain::JigsawLayer::writeConfig(xml::DomObject& p) const {
    // Base class first
    AdmittanceLayerShape::writeConfig(p);
    RepeatCellShape::writeConfig(p);
    DuplicatingShape::writeConfig(p);
    ThinLayerShape::writeConfig(p);
    // Now my things
    p << xml::Reopen();
    p << xml::Obj("platesize") << plateSize_;
    p << xml::Obj("protrusionsize") << protrusionSize_;
    p << xml::Obj("protrusionwidth") << protrusionWidth_;
    p << xml::Obj("indentwidth") << indentWidth_;
    p << xml::Obj("indentpos") << indentPos_;
    p << xml::Obj("indentsize") << indentSize_;
    p << xml::Obj("usesecondprotrusion") << useSecondProtrusion_;
    p << xml::Obj("protrusion2size") << protrusion2Size_;
    p << xml::Obj("indent2pos") << indent2Pos_;
    p << xml::Obj("indent2size") << indent2Size_;
    p << xml::Obj("usethirdprotrusion") << useThirdProtrusion_;
    p << xml::Obj("protrusion3size") << protrusion3Size_;
    p << xml::Obj("indent3pos") << indent3Pos_;
    p << xml::Obj("indent3size") << indent3Size_;
    for(auto& a : admittances_) {
        p << xml::Obj("shapeadmittance") << a;
    }
    p << xml::Close();
}

// Read the configuration from the DOM object
void domain::JigsawLayer::readConfig(xml::DomObject& p) {
    // Base class first
    AdmittanceLayerShape::readConfig(p);
    RepeatCellShape::readConfig(p);
    DuplicatingShape::readConfig(p);
    ThinLayerShape::readConfig(p);
    // Now my things
    p >> xml::Reopen();
    p >> xml::Obj("platesize") >> plateSize_;
    p >> xml::Obj("protrusionsize") >> protrusionSize_;
    p >> xml::Obj("protrusionwidth") >> protrusionWidth_;
    p >> xml::Obj("indentwidth") >> indentWidth_;
    p >> xml::Obj("indentpos") >> indentPos_;
    p >> xml::Obj("indentsize") >> indentSize_;
    p >> xml::Obj("usesecondprotrusion") >> xml::Default(false)
      >> useSecondProtrusion_;
    p >> xml::Obj("protrusion2size") >> xml::Default(0.0) >> protrusion2Size_;
    p >> xml::Obj("indent2pos") >> xml::Default(0.0) >> indent2Pos_;
    p >> xml::Obj("indent2size") >> xml::Default(0.0) >> indent2Size_;
    p >> xml::Obj("usethirdprotrusion") >> xml::Default(false)
      >> useThirdProtrusion_;
    p >> xml::Obj("protrusion3size") >> xml::Default(0.0) >> protrusion3Size_;
    p >> xml::Obj("indent3pos") >> xml::Default(0.0) >> indent3Pos_;
    p >> xml::Obj("indent3size") >> xml::Default(0.0) >> indent3Size_;
    for(auto& o : p.obj("shapeadmittance")) {
        admittances_.emplace_back();
        *o >> admittances_.back();
    }
    p >> xml::Close();
}

// Convert to an export format (dimensions are written in mm)
void domain::JigsawLayer::write(box::ExportWriter& /*writer*/,
                                const std::set<int>& /*selectedMaterials*/) {
}

// Evaluate expressions
void domain::JigsawLayer::evaluate(box::Expression::Context& c) {
    AdmittanceLayerShape::evaluate(c);
    RepeatCellShape::evaluate(c);
    DuplicatingShape::evaluate(c);
    ThinLayerShape::evaluate(c);
    plateSize_.evaluate(c);
    protrusionSize_.evaluate(c);
    protrusionWidth_.evaluate(c);
    indentWidth_.evaluate(c);
    indentPos_.evaluate(c);
    indentSize_.evaluate(c);
    protrusion2Size_.evaluate(c);
    indent2Pos_.evaluate(c);
    indent2Size_.evaluate(c);
    protrusion3Size_.evaluate(c);
    indent3Pos_.evaluate(c);
    indent3Size_.evaluate(c);
}

// Populate the layers list with information from this shape
void domain::JigsawLayer::makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) {
    DuplicatingShape::doMakeLayers(center_.z().value(), this, layers);
}

// We override tbe admittance function of AdmittanceLayerShape so we
// can use the admittance tables specific for this shape.
std::complex<double> domain::JigsawLayer::admittance(double frequency) {
    auto f = static_cast<long long>(std::round(frequency));
    std::complex<double> result;
    // First find the admittance tables 'before' and 'after' the one required
    // by the current shape
    ShapeAdmittance me(plateSize_.value(), protrusionSize_.value(),
                       protrusion2Size_.value(), protrusion3Size_.value(),
                       cellSizeX_.value());
    if((cacheBefore_ != nullptr || cacheAfter_ != nullptr) && me == cacheMe_) {
        // The tables are already in the cache
    } else {
        cacheMe_ = me;
        cacheBefore_ = nullptr;
        cacheAfter_ = nullptr;
        for(auto& a : admittances_) {
            if(a < me) {
                if(cacheBefore_ == nullptr || *cacheBefore_ < a) {
                    cacheBefore_ = &a;
                }
            } else if(me < a) {
                if(cacheAfter_ == nullptr || a < *cacheAfter_) {
                    cacheAfter_ = &a;
                }
            } else {
                cacheBefore_ = &a;
                cacheAfter_ = &a;
            }
        }
        if(cacheBefore_ == nullptr && cacheAfter_ == nullptr) {
            std::cout << "No tables" << std::endl;
        } else if(cacheBefore_ == nullptr) {
            std::cout << "Use " << cacheAfter_->name() << std::endl;
        } else if(cacheAfter_ == nullptr || cacheBefore_ == cacheAfter_) {
            std::cout << "Use " << cacheBefore_->name() << std::endl;
        } else {
            std::cout << "Interpolate " << cacheBefore_->name() << ", " << cacheAfter_->name()
                      << std::endl;
        }
    }
    // Now get the admittance
    if(cacheBefore_ == nullptr && cacheAfter_ == nullptr) {
        // No tables, do nothing
    } else if(cacheBefore_ == nullptr) {
        // Use the above table
        result = cacheAfter_->admittance().admittanceAt(f, cellSizeX_.value());
    } else if(cacheAfter_ == nullptr || cacheBefore_ == cacheAfter_) {
        // No above or exact match, use the below
        result = cacheBefore_->admittance().admittanceAt(f, cellSizeX_.value());
    } else {
        // Interpolate between below and above
        std::complex<double> a = cacheAfter_->admittance().admittanceAt(
                f, cellSizeX_.value());
        std::complex<double> b = cacheBefore_->admittance().admittanceAt(
                f, cellSizeX_.value());
        double nPlateSize = plateSize_.value() / cellSizeX_.value();
        double nFinger1Size = protrusionSize_.value() / cellSizeX_.value();
        double nFinger2Size = protrusion2Size_.value() / cellSizeX_.value();
        double nFinger3Size = protrusion3Size_.value() / cellSizeX_.value();
        if(box::Utility::equals(cacheBefore_->nPlateSize(), nPlateSize)) {
            if(box::Utility::equals(cacheBefore_->nFinger1Size(), nFinger1Size)) {
                if(box::Utility::equals(cacheBefore_->nFinger2Size(), nFinger2Size)) {
                    result = std::complex<double>(
                            box::Utility::interpolate(
                                    cacheBefore_->nFinger3Size(),
                                    cacheAfter_->nFinger3Size(),
                                    b.real(), a.real(),
                                    nFinger3Size),
                            box::Utility::interpolate(
                                    cacheBefore_->nFinger3Size(),
                                    cacheAfter_->nFinger3Size(),
                                    b.imag(), a.imag(),
                                    nFinger3Size));
                } else {
                    result = std::complex<double>(
                            box::Utility::interpolate(
                                    cacheBefore_->nFinger2Size(),
                                    cacheAfter_->nFinger2Size(),
                                    b.real(), a.real(),
                                    nFinger2Size),
                            box::Utility::interpolate(
                                    cacheBefore_->nFinger2Size(),
                                    cacheAfter_->nFinger2Size(),
                                    b.imag(), a.imag(),
                                    nFinger2Size));
                }
            } else {
                result = std::complex<double>(
                        box::Utility::interpolate(
                                cacheBefore_->nFinger1Size(),
                                cacheAfter_->nFinger1Size(),
                                b.real(), a.real(),
                                nFinger1Size),
                        box::Utility::interpolate(
                                cacheBefore_->nFinger1Size(),
                                cacheAfter_->nFinger1Size(),
                                b.imag(), a.imag(),
                                nFinger1Size));
            }
        } else {
            result = std::complex<double>(
                    box::Utility::interpolate(
                            cacheBefore_->nPlateSize(),
                            cacheAfter_->nPlateSize(),
                            b.real(), a.real(),
                            nPlateSize),
                    box::Utility::interpolate(
                            cacheBefore_->nPlateSize(),
                            cacheAfter_->nPlateSize(),
                            b.imag(), a.imag(),
                            nPlateSize));
        }
    }
    if(result == std::complex<double>(0.0, 0.0)) {
        std::cout << "Zero admittance" << std::endl;
    }
    return result;
}

// Shape admittance constructor
domain::JigsawLayer::ShapeAdmittance::ShapeAdmittance(double p, double f1, double f2,
                                                      double f3, double unitCell) :
        plateSize_(p),
        finger1Size_(f1),
        finger2Size_(f2),
        finger3Size_(f3) {
    admittance_.cellSize(unitCell);
}

// Less than operator compares two shape admittance features
bool domain::JigsawLayer::ShapeAdmittance::operator<(
        const domain::JigsawLayer::ShapeAdmittance& other) const {
    bool result;
    if(box::Utility::equals(nPlateSize(), other.nPlateSize())) {
        if(box::Utility::equals(nFinger1Size(), other.nFinger1Size())) {
            if(box::Utility::equals(nFinger2Size(), other.nFinger2Size())) {
                result = nFinger3Size() < other.nFinger3Size() &&
                         !box::Utility::equals(nFinger3Size(), other.nFinger3Size());
            } else {
                result = nFinger2Size() < other.nFinger2Size();
            }
        } else {
            result = nFinger1Size() < other.nFinger1Size();
        }
    } else {
        result = nPlateSize() < other.nPlateSize();
    }
    return result;
}

// Equality operator
bool domain::JigsawLayer::ShapeAdmittance::operator==(
        const domain::JigsawLayer::ShapeAdmittance& other) const {
    return box::Utility::equals(nPlateSize(), other.nPlateSize()) &&
           box::Utility::equals(nFinger1Size(), other.nFinger1Size()) &&
           box::Utility::equals(nFinger2Size(), other.nFinger2Size()) &&
           box::Utility::equals(nFinger3Size(), other.nFinger3Size());
}

// Write to the XML DOM
void domain::JigsawLayer::ShapeAdmittance::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("platesize") << plateSize_;
    root << xml::Obj("finger1size") << finger1Size_;
    root << xml::Obj("finger2size") << finger2Size_;
    root << xml::Obj("finger3size") << finger3Size_;
    root << xml::Obj("admittancedata") << admittance_;
    root << xml::Obj("name") << name_;
    root << xml::Close();
}

// Read from the XML DOM
void domain::JigsawLayer::ShapeAdmittance::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("platesize") >> plateSize_;
    root >> xml::Obj("finger1size") >> finger1Size_;
    root >> xml::Obj("finger2size") >> finger2Size_;
    root >> xml::Obj("finger3size") >> finger3Size_;
    root >> xml::Obj("admittancedata") >> admittance_;
    root >> xml::Obj("name") >> name_;
    root >> xml::Close();
}
