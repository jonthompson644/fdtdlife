/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATALOGUELAYER_H
#define FDTDLIFE_CATALOGUELAYER_H

#include "BinaryPlateNxN.h"

namespace domain {

    // A shape that selects from a catalogue of binary NxN plates.
    class CatalogueLayer : public BinaryPlateNxN {
    public:
        // Types
        class Item {
        public:
            // Construction
            Item();

            // API
            void writeConfig(xml::DomObject& p) const;
            void readConfig(xml::DomObject& p);
            friend xml::DomObject& operator<<(xml::DomObject& o, const Item& v) {
                v.writeConfig(o);
                return o;
            }
            friend xml::DomObject& operator>>(xml::DomObject& o, Item& v) {
                v.readConfig(o);
                return o;
            }

            // Getters
            std::vector<uint64_t> coding() { return coding_; }
            size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
            fdtd::AdmittanceData& admittanceData() { return admittanceData_; }
            const std::string& name() const { return name_; }

            // Setters
            void bitsPerHalfSide(size_t v);
            void coding(const std::vector<uint64_t>& v) { coding_ = v; }
            void name(const std::string& v) { name_ = v; }

        protected:
            // Configuration
            std::vector<uint64_t> coding_;
            size_t bitsPerHalfSide_{8};
            fdtd::AdmittanceData admittanceData_;
            std::string name_;
        };

        // Construction
        CatalogueLayer() = default;

        // Overrides of BinaryPlateNxN
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void evaluate(box::Expression::Context& c) override;
        void fill(std::vector<uint16_t>& d) override;

        // API
        size_t newItem();
        size_t newItem(size_t bitsPerHalfSide, const std::vector<uint64_t>& coding);
        void deleteItem(size_t index);

        // Getters
        box::Expression& select() { return select_; }
        std::vector<Item>& items() { return items_; }

    protected:
        // Configuration
        box::Expression select_{0};  // The item to select
        std::vector<Item> items_;  // The item catalogue
    };
}


#endif //FDTDLIFE_CATALOGUELAYER_H
