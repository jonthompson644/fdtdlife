/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FINGERPLATE_H
#define FDTDLIFE_FINGERPLATE_H

#include "Shape.h"
#include <Box/VectorExpression.h>
#include <Box/Utility.h>
#include <Box/Rectangle.h>
#include "DuplicatingShape.h"
#include "CenterPosShape.h"
#include "AdmittanceLayerShape.h"
#include "ThinLayerShape.h"
#include <list>

namespace domain {

    // A layer containing the special jigsaw shape
    class FingerPlate
            : public AdmittanceLayerShape, public DuplicatingShape
              , public ThinLayerShape {
    public:
        // A class that encapsulates the parameters that
        // together specify the admittance of the shape
        class ShapeAdmittance {
        public:
            ShapeAdmittance() = default;
            ShapeAdmittance(double p, double d, double unitCell) :
                    plateSize_(p),
                    minFeatureSize_(d) {
                admittance_.cellSize(unitCell);
            }
            void writeConfig(xml::DomObject& root) const;
            void readConfig(xml::DomObject& root);
            friend xml::DomObject& operator<<(xml::DomObject& o, const ShapeAdmittance& v) {
                v.writeConfig(o);
                return o;
            }
            friend xml::DomObject& operator>>(xml::DomObject& o, ShapeAdmittance& v) {
                v.readConfig(o);
                return o;
            }
            [[nodiscard]] double plateSize() const { return plateSize_; }
            [[nodiscard]] double minFeatureSize() const { return minFeatureSize_; }
            [[nodiscard]] const std::string& name() const { return name_; }
            fdtd::AdmittanceData& admittance() { return admittance_; }
            void plateSize(double v) { plateSize_ = v; }
            void minFeatureSize(double v) { minFeatureSize_ = v; }
            void name(const std::string& v) { name_ = v; }
        protected:
            double plateSize_{};   // As a percentage of g
            double minFeatureSize_{};  // As a percentage of g
            fdtd::AdmittanceData admittance_;
            std::string name_;
        };

        // Construction
        FingerPlate() = default;

        // Overrides of AdmittanceLayerShape
        void fill(std::vector<uint16_t>& d) override;
        void makeLayers(std::list<fdtd::PropMatrixMethod::Layer>& layers) override;
        void writeConfig(xml::DomObject& p) const override;
        void readConfig(xml::DomObject& p) override;
        void write(box::ExportWriter& writer,
                   const std::set<int>& selectedMaterials) override;
        void evaluate(box::Expression::Context& c) override;
        std::complex<double> admittance(double frequency) override;

        // Accessors
        box::Expression& plateSize() { return plateSize_; }
        box::Expression& minFeatureSize() { return minFeatureSize_; }
        std::list<ShapeAdmittance>& admittances() { return admittances_; }

        // Getters
        [[nodiscard]] bool fingersLeft() const { return fingersLeft_; }
        [[nodiscard]] bool fingersRight() const { return fingersRight_; }
        [[nodiscard]] bool fingersTop() const { return fingersTop_; }
        [[nodiscard]] bool fingersBottom() const { return fingersBottom_; }
        [[nodiscard]] const box::Expression& plateSize() const { return plateSize_; }
        [[nodiscard]] const box::Expression& minFeatureSize() const { return minFeatureSize_; }
        [[nodiscard]] const box::Expression& fingerOffset() const { return fingerOffset_; }

        // Setters
        void fingersLeft(bool v) { fingersLeft_ = v; }
        void fingersRight(bool v) { fingersRight_ = v; }
        void fingersTop(bool v) { fingersTop_ = v; }
        void fingersBottom(bool v) { fingersBottom_ = v; }
        void plateSize(const box::Expression& v) { plateSize_ = v; }
        void minFeatureSize(const box::Expression& v) { minFeatureSize_ = v; }
        void fingerOffset(const box::Expression& v) { fingerOffset_ = v; }

    protected:
        // Configuration
        box::Expression plateSize_{50.0};  // Overall size of square and fingers (% of g)
        box::Expression minFeatureSize_{10.0};  // Minimum feature size (% of g)
        box::Expression fingerOffset_{0.0};  // Finger offset from unit cell edge
        std::list<ShapeAdmittance> admittances_;  // The table of admittances for the shape
        bool fingersLeft_{true};  // True for sockets on the left side
        bool fingersRight_{false};  // True for sockets on the right side
        bool fingersTop_{true};  // True for sockets on the top side
        bool fingersBottom_{false};  // True for sockets on the bottom side
        // Derived values
        box::Vector<double> g_{};  // The unit cell in m
        box::Vector<double> p_{};  // The overall plate size in m
        box::Vector<double> d_{};  // The minimum feature size in m
        box::Vector<double> b_{};  // The size of the maximum central square patch in m
        box::Vector<double> o_{};  // Finger offset in m
        box::Vector<double> centralSquare_{};  // The size of the central square patch in m
        std::list<box::Vector<double>> fingers_;  // The size of the fingers in m
        ShapeAdmittance* cacheBefore_{};  // The cached admittance table before
        ShapeAdmittance* cacheAfter_{};  // The cached admittance table after

        // Helpers
        void fillOnePlate(std::vector<uint16_t>& d, Material* myMat,
                          const box::Vector<double>& pos, FingerPlate* right,
                          FingerPlate* below, FingerPlate* left, FingerPlate* above);
        void calcFingerLengths();
        void findAdjacentPlates(FingerPlate*& right, FingerPlate*& below,
                                FingerPlate*& left, FingerPlate*& above) const;
        void initAdmittanceCache();
        void exportOnePlate(box::ExportWriter& writer, const std::string& onMatName,
                            const box::Vector<double>& p,
                            const FingerPlate* right, const FingerPlate* below,
                            const FingerPlate* left, const FingerPlate* above) const;
        void getMainRects(const box::Vector<double>& pos, box::Rectangle<double>& unitCell,
                          box::Rectangle<double>& plate,
                          box::Rectangle<double>& maxPlate) const;
        void getIndentRects(const FingerPlate* right, const FingerPlate* below,
                            const FingerPlate* left, const FingerPlate* above,
                            const box::Rectangle<double>& plate,
                            const box::Rectangle<double>& maxPlate,
                            std::list<box::Rectangle<double>>& indents) const;
        void getFingerRects(const box::Rectangle<double>& unitCell,
                            const box::Rectangle<double>& plate,
                            std::list<box::Rectangle<double>>& fingers,
                            std::list<box::Rectangle<double>>& stumps) const;
        void doFillMaterial(std::vector<uint16_t>& dom,
                            domain::Material* fillMat,
                            const box::Vector<double>& pos);
    };
}

#endif //FDTDLIFE_FINGERPLATE_H
