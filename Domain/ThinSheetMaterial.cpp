/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ThinSheetMaterial.h"
#include <Fdtd/Model.h>
#include "Domain.h"
#include "ThinLayerShape.h"

// Set the configuration of the thin sheet
// This sheet material objects should be created during initialisation
// after the material and shape objects have been initialised
void domain::ThinSheetMaterial::set(int mainMaterialId, int sheetMaterialId,
                                    ThinLayerShape* shape) {
    // Get the materials
    Material* mainMat = m_->d()->getMaterial(mainMaterialId);
    Material* sheetMat = m_->d()->getMaterial(sheetMaterialId);
    // Copy the main material parameters
    if(mainMat != nullptr) {
        mainMat->evaluate();
        epsilon_ = mainMat->epsilon().value();
        mu_ = mainMat->mu().value();
        sigma_ = mainMat->sigma().value();
        sigmastar_ = mainMat->sigmastar().value();
        this->initialise();
    }
    // Copy the sheet material parameters
    if(sheetMat != nullptr) {
        sheetMat->evaluate();
        sheetEpsilon_ = sheetMat->epsilon().value();
        sheetSigma_ = sheetMat->sigma().value();
        color_ = sheetMat->color();
        priority_ = sheetMat->priority();
    }
    // The thickness of the sheet
    thickness_ = shape->layerThickness().value();
    // The extra storage
    sizeX_ = m_->p()->geometry()->n().x();
    sizeY_ = m_->p()->geometry()->n().y();
    extraEz_.resize(sizeX_ * sizeY_);
    for(size_t i = 0; i < sizeX_ * sizeY_; i++) {
        extraEz_[i] = 0.0;
    }
    // The precalculated constants
    inRatio_ = thickness_ / m_->p()->dr().z().value();
    outRatio_ = 1.0 - thickness_ / m_->p()->dr().z().value();
    double epsilonAv = outRatio_ * epsilon_.value() + inRatio_ * sheetEpsilon_;
    double sigmaAv = outRatio_ * sigma_.value() + inRatio_ * sheetSigma_;
    caein_ = (m_->p()->dt() / sheetEpsilon_) /
             (1.0 + sheetSigma_ * m_->p()->dt() / 2.0 / sheetEpsilon_);
    cbein_ = (1.0 - sheetSigma_ * m_->p()->dt() / 2.0 / sheetEpsilon_) /
             (1.0 + sheetSigma_ * m_->p()->dt() / 2.0 / sheetEpsilon_);
    caeav_ = (m_->p()->dt() / epsilonAv) / (1.0 + sigmaAv * m_->p()->dt() / 2.0 / epsilonAv);
    cbeav_ = (1.0 - sigmaAv * m_->p()->dt() / 2.0 / epsilonAv) /
             (1.0 + sigmaAv * m_->p()->dt() / 2.0 / epsilonAv);
}

// Return a pointer to an entry in the extra Ez component array
double* domain::ThinSheetMaterial::extraEz(size_t i, size_t j) {
    // The extra Ez component
    i = i % sizeX_;
    j = j % sizeY_;
    size_t index = j * sizeX_ + i;
    return &extraEz_[static_cast<size_t>(index)];
}
