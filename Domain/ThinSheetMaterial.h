/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_THINSHEETMATERIAL_H
#define FDTDLIFE_THINSHEETMATERIAL_H

#include <string>
#include <vector>
#include "Material.h"

namespace domain {

    class ThinLayerShape;

    // A special 'material' used to represent a thin sheet embedded in a cell.
    // This object also provides the necessary storage for the extra electric field
    // component in such cells so a new instance must be created for each shape.
    class ThinSheetMaterial : public Material {
    public:
        // Construction
        ThinSheetMaterial() = default;

        // API
        void set(int mainMaterialId, int sheetMaterialId, ThinLayerShape* shape);
        double* extraEz(size_t i, size_t j);

        // Getters
        double caein() const {return caein_;}
        double cbein() const {return cbein_;}
        double caeav() const {return caeav_;}
        double cbeav() const {return cbeav_;}
        double inRatio() const {return inRatio_;}
        double outRatio() const {return outRatio_;}
    protected:
        // Attributes
        double thickness_ {}; // The thickness of the sheet
        double sheetEpsilon_ {}; // The electric permittivity of the sheet
        double sheetSigma_ {}; // The electric conductivity of the sheet
        std::vector<double> extraEz_;  // The storage required for the extra electric field Z component
        size_t sizeX_ {};  // The X size of the extra storage
        size_t sizeY_ {};  // The Y size of the extra storage
        double caein_ {};
        double cbein_ {};
        double caeav_ {};
        double cbeav_ {};
        double inRatio_ {};
        double outRatio_ {};
    };

}


#endif //FDTDLIFE_THINSHEETMATERIAL_H
