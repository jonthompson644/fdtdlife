/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_PLANEWAVESOURCE_H
#define FDTDLIFE_PLANEWAVESOURCE_H

#include <Box/Vector.h>
#include "Source.h"
#include <string>
#include <vector>
#include <Box/Expression.h>

namespace xml { class DomObject; }
namespace fdtd { class Model; }
namespace fdtd { class Configuration; }

namespace source {

    // Represents a plane wave entering along the z axis
    class PlaneWaveSource : public Source {
    public:
        // Types
        enum class Distribution {
            flat, gaussian
        };
        friend xml::DomObject& operator>>(xml::DomObject& o, Distribution& v) {
            int t = 0;
            o >> t;
            v = static_cast<Distribution>(t);
            return o;
        }
        friend xml::DomObject& operator<<(xml::DomObject& o, Distribution v) {
            o << static_cast<int>(v);
            return o;
        }

    protected:
        // Persistent Attributes
        box::Expression amplitude_{1.0}; // The amplitude in Volts
        box::Expression polarisation_{0.0};  // The polarization in degrees
        double pmlSigma_{1.0}; // The conductivity of the PML zone
        double pmlSigmaFactor_{
                1.04}; // Factor by which the sigma increases as we get further into the PML
        bool continuous_{true};  // True if the source is to be continuous
        box::Expression time_
                {0.0};  // If not continuous, time in seconds the source is to run for
        box::Expression azimuth_
                {0.0};  // Angle between the x axis and the projection onto the zx plane
        box::Expression initialTime_
                {0.0};  // The length of time the source is offset
        Distribution distribution_
                {Distribution::flat};  // The amplitude shape of the wave along the wavefront
        double gaussianB_{1.0};  // Gaussian wave B parameter
        double gaussianC_{1.0};  // Gaussian wave C parameter

        // Derived attributes
        size_t incidentN_{};  // Number of cells in the auxiliary grid
        double sinPolarisation_{};  // Precalculated sine
        double cosPolarisation_{};  // Precalculated cosine
        double sinAzimuth_{};  // Precalculated sine
        double cosAzimuth_{};  // Precalculated cosine
        std::vector<double> ex_;  // The Ex auxiliary grid
        std::vector<double> hy_;  // The Hy auxiliary grid
        box::Vector<size_t> incidentCorner_;  // The corner the incident wave first encounters
        double dr_{};  // Auxiliary grid cell size
        double epsilon_{};  // The epsilon to use for the generating medium
        double mu_{};  // The mu to use for the generating region
        double gaussianA_{};  // Gaussian wave A parameter
        double wavefrontLength_{};  // The length of the wavefront at the total field zone edge

    public:
        // Construction
        PlaneWaveSource() = default;

        // Methods
        void initialise() override;
        void evaluate() override;
        box::Vector<double> eIncident(double i, double j, double k) override;
        box::Vector<double> hIncident(double i, double j, double k) override;
        size_t memoryUse() override;
        void clear() override;
        [[nodiscard]] size_t index(size_t i) const;
        [[nodiscard]] int pmlZoneDist(size_t i) const {
            return static_cast<int>(i) - static_cast<int>((incidentN_ / 2 + 1));
        }
        std::vector<double>& e() { return ex_; }
        std::vector<double>& h() { return hy_; }
        bool writeData(std::ofstream& file) override;
        bool readData(std::ifstream& file) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        [[nodiscard]] bool inPmlZone(size_t i) const { return pmlZoneDist(i) >= 0; }
        void stepE(const box::ThreadInfo* info, std::vector<double>& x, std::vector<double>& y,
                   std::vector<double>& z) override;
        void stepH(const box::ThreadInfo* info, std::vector<double>& x, std::vector<double>& y,
                   std::vector<double>& z) override;
        void stepEndE() override;
        void stepEndH() override;

        // Getters
        [[nodiscard]] const box::Expression& amplitude() const { return amplitude_; }
        [[nodiscard]] const box::Expression& polarisation() const { return polarisation_; }
        [[nodiscard]] double pmlSigma() const { return pmlSigma_; }
        [[nodiscard]] double pmlSigmaFactor() const { return pmlSigmaFactor_; }
        [[nodiscard]] size_t incidentN() const { return incidentN_; }
        [[nodiscard]] bool continuous() const { return continuous_; }
        [[nodiscard]] const box::Expression& time() const { return time_; }
        [[nodiscard]] const box::Expression& azimuth() const { return azimuth_; }
        [[nodiscard]] const box::Expression& initialTime() const { return initialTime_; }
        [[nodiscard]] Distribution distribution() const { return distribution_; }
        [[nodiscard]] double gaussianB() const { return gaussianB_; }
        [[nodiscard]] double gaussianC() const { return gaussianC_; }

        // Setters
        void pmlSigma(double v) { pmlSigma_ = v; }
        void pmlSigmaFactor(double v) { pmlSigmaFactor_ = v; }
        void continuous(bool v) { continuous_ = v; }
        void distribution(Distribution v) { distribution_ = v; }
        void gaussianB(double v) { gaussianB_ = v; }
        void gaussianC(double v) { gaussianC_ = v; }
        void amplitude(const box::Expression& v) { amplitude_ = v; }
        void polarisation(const box::Expression& v) { polarisation_ = v; }
        void time(const box::Expression& v) { time_ = v; }
        void azimuth(const box::Expression& v) { azimuth_ = v; }
        void initialTime(const box::Expression& v) { initialTime_ = v; }

    protected:
        // Private functions
        double distanceToIncidentCorner(double i, double j, double k);
        double interpolate(double d, const std::vector<double>& data);
        void stepAuxiliaryE();
        void stepAuxiliaryH();
        void doTfsfE(const box::ThreadInfo* info, std::vector<double>& x,
                     std::vector<double>& y, std::vector<double>& z);
        void doTfsfH(const box::ThreadInfo* info, std::vector<double>& x,
                     std::vector<double>& y, std::vector<double>& z);
        double positionAmplitude(double i, double j, double k);
    };

}


#endif //FDTDLIFE_PLANEWAVESOURCE_H
