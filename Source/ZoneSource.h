/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_ZONESOURCE_H
#define FDTDLIFE_ZONESOURCE_H

#include "Source.h"
#include <Box/SignalGenerator.h>

namespace fdtd { class Model; }

namespace source {

    // A source that injects a wave into a zone within the model
    class ZoneSource : public Source {
    public:
        // Construction
        ZoneSource() = default;
        ~ZoneSource() override;

        // API
        void initialise() override;
        size_t memoryUse() override;
        bool writeData(std::ofstream& file) override;
        bool readData(std::ifstream& file) override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void stepE(const box::ThreadInfo* info, std::vector<double>& x, std::vector<double>& y, std::vector<double>& z) override;
        void stepH(const box::ThreadInfo* info, std::vector<double>& x, std::vector<double>& y, std::vector<double>& z) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        box::Vector<double> eIncident(double i, double j, double k) override;

        // Getters
        double amplitude() const {return amplitude_;}
        double azimuth() const {return azimuth_;}
        double elevation() const {return elevation_;}
        double polarisation() const {return polarisation_;}
        bool continuous() const {return continuous_;}
        double time() const {return time_;}
        double initialTime() const {return initialTime_;}
        const box::Vector<double>& zoneCenter() const {return zoneCenter_;}
        const box::Vector<double>& zoneSize() const {return zoneSize_;}
        double ex(size_t i, size_t j, size_t k) const {return ex_[index(i,j,k)];}
        double ey(size_t i, size_t j, size_t k) const {return ey_[index(i,j,k)];}
        double ez(size_t i, size_t j, size_t k) const {return ez_[index(i,j,k)];}
        double hx(size_t i, size_t j, size_t k) const {return hx_[index(i,j,k)];}
        double hy(size_t i, size_t j, size_t k) const {return hy_[index(i,j,k)];}
        double hz(size_t i, size_t j, size_t k) const {return hz_[index(i,j,k)];}

        // Setters
        void amplitude(double v) {amplitude_ = v;}
        void azimuth(double v) {azimuth_ = v;}
        void elevation(double v) {elevation_ = v;}
        void polarisation(double v) {polarisation_ = v;}
        void continuous(bool v) {continuous_ = v;}
        void time(double v) {time_ = v;}
        void initialTime(double v) {initialTime_ = v;}
        void zoneCenter(const box::Vector<double>& v) {zoneCenter_ = v;}
        void zoneSize(const box::Vector<double>& v) {zoneSize_ = v;}

    protected:
        // Persistent Attributes
        double amplitude_{1.0}; // The electric field amplitude in Volts/m
        double azimuth_{0.0};  // Angle between the x axis and the projection onto the zx plane
        double elevation_{0.0};  // Angle between the zx plane the vector
        double polarisation_{0.0};  // The polarization in degrees
        bool continuous_{true};  // True if the source is to be continuous
        double time_{0.0};  // If not continuous, time in seconds the source is to run for
        double initialTime_{0.0};  // The length of time the source is offset
        box::Vector<double> zoneCenter_;  // The coordinate of the center of the zone
        box::Vector<double> zoneSize_;  // The size of the zone

        // Derived attributes
        double* ex_{nullptr};  // The wave electric field x component
        double* ey_{nullptr};  // The wave electric field y component
        double* ez_{nullptr};  // The wave electric field z component
        double* hx_{nullptr};  // The wave magnetic field x component
        double* hy_{nullptr};  // The wave magnetic field y component
        double* hz_{nullptr};  // The wave magnetic field z component
        box::Vector<size_t> zoneTopLeft_;  // The top left zone cell
        box::Vector<size_t> zoneBottomRight_;  // The bottom right zone cell
        box::Vector<size_t> zoneCells_;  // The number of cells in the zone
        size_t totalCellsInZone_{0};  // The number of cells in the zone
        box::SignalGenerator signal_;  // The basic signal generator

    protected:
        size_t index(size_t i, size_t j, size_t k) const;
    };

}


#endif //FDTDLIFE_ZONESOURCE_H
