/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Sources.h"
#include <Fdtd/Model.h>
#include "Source.h"
#include <Xml/DomObject.h>
#include <memory>

// Constructor
source::Sources::Sources(fdtd::Model* m) :
        m_(m) {
}

// Destructor
source::Sources::~Sources() {
    clear();
}

// Add a source to the list
void source::Sources::add(Source* s) {
    sources_.push_back(s);
}

// Remove a source from the list
void source::Sources::remove(Source* s) {
    sources_.remove(s);
}

// Clear the sources back to the pristine state
// leaving the except if given
void source::Sources::clear(Source* except) {
    if(except != nullptr) {
        sources_.remove(except);
    }
    while(!sources_.empty()) {
        delete sources_.front();
    }
    if(except != nullptr) {
        sources_.push_back(except);
    }
    factory_.clear();
}

// Initialise the sources ready for a run
void source::Sources::initialise() {
    for(auto source : sources_) {
        source->initialise();
    }
}

// Step the E field of all the sources on one
void source::Sources::stepE(const box::ThreadInfo* info, std::vector<double>& x,
                            std::vector<double>& y, std::vector<double>& z) {
    for(auto source : sources_) {
        source->stepE(info, x, y, z);
    }
}

// Step the H field of all the sources on one
void source::Sources::stepH(const box::ThreadInfo* info, std::vector<double>& x,
                            std::vector<double>& y, std::vector<double>& z) {
    for(auto source : sources_) {
        source->stepH(info, x, y, z);
    }
}

// Step initialisation
void source::Sources::stepBeginE() {
    for(auto source : sources_) {
        source->stepBeginE();
    }
}

// Step finalisation
void source::Sources::stepEndE() {
    for(auto source : sources_) {
        source->stepEndE();
    }
}

// Step initialisation
void source::Sources::stepBeginH() {
    for(auto source : sources_) {
        source->stepBeginH();
    }
}

// Step finalisation
void source::Sources::stepEndH() {
    for(auto source : sources_) {
        source->stepEndH();
    }
}

// Return the memory usage of all the sources
size_t source::Sources::memoryUse() {
    size_t result = 0;
    for(auto source : sources_) {
        result += source->memoryUse();
    }
    return result;
}

// Return frequency information for the sources
box::FrequencySeq source::Sources::frequencyInfo() {
    box::FrequencySeq result;
    for(auto& source : sources_) {
        result |= source->frequencies().value();
    }
    return result;
}

// Return the source with the given identifier
source::Source* source::Sources::getSource(int id) {
    Source* result = nullptr;
    for(auto s : sources_) {
        if(s->identifier() == id) {
            result = s;
            break;
        }
    }
    return result;
}

// Write the sources state as binary data
bool source::Sources::writeData(std::ofstream& file) {
    bool result = true;
    for(auto source : sources_) {
        result = result && source->writeData(file);
    }
    return result;
}

// Read the sources state as binary data
bool source::Sources::readData(std::ifstream& file) {
    bool result = true;
    for(auto source : sources_) {
        result = result && source->readData(file);
    }
    return result;
}

// Write the configuration
void source::Sources::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    for(auto& s : sources_) {
        root << xml::Obj("source") << *s;
    }
    root << xml::Close();
}

// Read the configuration
void source::Sources::readConfig(xml::DomObject& root) {
    clear();
    root >> xml::Open();
    for(auto& p : root.obj("source")) {
        auto s = factory_.make(*p, m_);
        *p >> *s;
    }
    root >> xml::Close();
}

// Return the incident E field at a particular coordinate
box::Vector<double> source::Sources::eIncident(double i, double j, double k) {
    box::Vector<double> result;
    for(auto& s : sources_) {
        result = result + s->eIncident(i, j, k);
    }
    return result;
}

// Return the incident H field at a particular coordinate
box::Vector<double> source::Sources::hIncident(double i, double j, double k) {
    box::Vector<double> result;
    for(auto& s : sources_) {
        result = result + s->hIncident(i, j, k);
    }
    return result;
}

// Return the source at a specific location
source::Source* source::Sources::getAt(const box::Vector<size_t>& p) {
    Source* result = nullptr;
    for(auto& source : sources_) {
        if(source->isAt(p)) {
            result = source;
        }
    }
    return result;
}

// Return the source with the given name
source::Source* source::Sources::find(const std::string& name) {
    Source* result = nullptr;
    for(auto& source : sources_) {
        if(source->name() == name) {
            result = source;
            break;
        }
    }
    return result;
}

// Clear sources created by a sequencer
void source::Sources::clearSeqGenerated() {
    auto pos = sources_.begin();
    while(pos != sources_.end()) {
        auto next = std::next(pos);
        if((*pos)->seqGenerated()) {
            sources_.erase(pos);
        }
        pos = next;
    }
}
