/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ZoneSource.h"
#include <Fdtd/Model.h>
#include <Fdtd/Configuration.h>
#include <Domain/Material.h>
#include <Box/BinaryFileHeader.h>
#include <Xml/DomObject.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "SourceFactory.h"

// Destructor
source::ZoneSource::~ZoneSource() {
    delete hx_;
    delete hy_;
    delete hz_;
    hx_ = nullptr;
    hy_ = nullptr;
    hz_ = nullptr;
    delete ex_;
    delete ey_;
    delete ez_;
    ex_ = nullptr;
    ey_ = nullptr;
    ez_ = nullptr;
}

// Initialise ready for a run
void source::ZoneSource::initialise() {
    evaluate();
    // Delete any old data
    delete hx_;
    delete hy_;
    delete hz_;
    hx_ = nullptr;
    hy_ = nullptr;
    hz_ = nullptr;
    delete ex_;
    delete ey_;
    delete ez_;
    ex_ = nullptr;
    ey_ = nullptr;
    ez_ = nullptr;
    // The extent of the zone in cells
    zoneTopLeft_ = m_->p()->geometry()->cellFloor(zoneCenter_ - (zoneSize_ / 2.0));
    zoneBottomRight_ = m_->p()->geometry()->cellFloor(zoneCenter_ + (zoneSize_ / 2.0));
    zoneCells_ = static_cast<box::Vector<size_t>>(zoneBottomRight_ - zoneTopLeft_);
    totalCellsInZone_ = zoneCells_.x() * zoneCells_.y() * zoneCells_.z();
    // Allocate and initialise the memory
    hx_ = new double[totalCellsInZone_];
    hy_ = new double[totalCellsInZone_];
    hz_ = new double[totalCellsInZone_];
    ex_ = new double[totalCellsInZone_];
    ey_ = new double[totalCellsInZone_];
    ez_ = new double[totalCellsInZone_];
    for(size_t i = 0; i < totalCellsInZone_; i++) {
        hx_[i] = 0.0;
        hy_[i] = 0.0;
        hz_[i] = 0.0;
        ex_[i] = 0.0;
        ey_[i] = 0.0;
        ez_[i] = 0.0;
    }
    // The signal generator
    signal_.initialise(m_->p()->dt(), frequencies_.value(), initialTime_, amplitude_);
}

// Return the amount of memory used
size_t source::ZoneSource::memoryUse() {
    return 6 * totalCellsInZone_ * sizeof(double);
}

// Write the binary data
bool source::ZoneSource::writeData(std::ofstream& file) {
    box::BinaryFileHeader header("SOURCE", 1,
                                 totalCellsInZone_ * 3 * sizeof(double));
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    auto partSize = static_cast<std::streamsize>(totalCellsInZone_ * sizeof(double));
    file.write(reinterpret_cast<const char*>(hx_), partSize);
    file.write(reinterpret_cast<const char*>(hy_), partSize);
    file.write(reinterpret_cast<const char*>(hz_), partSize);
    file.write(reinterpret_cast<const char*>(ex_), partSize);
    file.write(reinterpret_cast<const char*>(ey_), partSize);
    file.write(reinterpret_cast<const char*>(ez_), partSize);
    return true;
}

// Read the binary data
bool source::ZoneSource::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("SOURCE")) {
        auto partSize = static_cast<std::streamsize>(totalCellsInZone_ * sizeof(double));
        size_t arraySize = 0;
        switch(header.version()) {
            case 1:
                arraySize = header.recordSize() / 3 / sizeof(double);
                file.read(reinterpret_cast<char*>(hx_), partSize);
                file.read(reinterpret_cast<char*>(hy_), partSize);
                file.read(reinterpret_cast<char*>(hz_), partSize);
                file.read(reinterpret_cast<char*>(ex_), partSize);
                file.read(reinterpret_cast<char*>(ey_), partSize);
                file.read(reinterpret_cast<char*>(ez_), partSize);
                result = totalCellsInZone_ == arraySize;
                break;
            default:
                break;
        }
    }
    return result;
}

// Write the configuration to the DOM object
void source::ZoneSource::writeConfig(xml::DomObject& root) const {
    // Base class first
    Source::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("amplitude") << amplitude_;
    root << xml::Obj("azimuth") << azimuth_;
    root << xml::Obj("elevation") << elevation_;
    root << xml::Obj("polarisation") << polarisation_;
    root << xml::Obj("continuous") << continuous_;
    root << xml::Obj("time") << time_;
    root << xml::Obj("initialtime") << initialTime_;
    root << xml::Obj("xcenter") << zoneCenter_.x();
    root << xml::Obj("ycenter") << zoneCenter_.y();
    root << xml::Obj("zcenter") << zoneCenter_.z();
    root << xml::Obj("xsize") << zoneSize_.x();
    root << xml::Obj("ysize") << zoneSize_.y();
    root << xml::Obj("zsize") << zoneSize_.z();
    root << xml::Close();
}

// Read the configuration from the DOM object
void source::ZoneSource::readConfig(xml::DomObject& root) {
    double v;
    // Base class first
    Source::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("amplitude") >> amplitude_;
    root >> xml::Obj("azimuth") >> azimuth_;
    root >> xml::Obj("elevation") >> elevation_;
    root >> xml::Obj("polarisation") >> polarisation_;
    root >> xml::Obj("continuous") >> continuous_;
    root >> xml::Obj("time") >> time_;
    root >> xml::Obj("initialtime") >> initialTime_;
    root >> xml::Obj("xcenter") >> v;
    zoneCenter_.x(v);
    root >> xml::Obj("ycenter") >> v;
    zoneCenter_.y(v);
    root >> xml::Obj("zcenter") >> v;
    zoneCenter_.z(v);
    root >> xml::Obj("xsize") >> v;
    zoneSize_.x(v);
    root >> xml::Obj("ysize") >> v;
    zoneSize_.y(v);
    root >> xml::Obj("zsize") >> v;
    zoneSize_.z(v);
    root >> xml::Close();
}

// Step the electric field on one
void source::ZoneSource::stepE(const box::ThreadInfo* info, std::vector<double>& x,
                               std::vector<double>& /*y*/, std::vector<double>& /*z*/) {
    // The region this thread is responsible for
    size_t i, j, k;
    box::Vector<size_t> regionStart;
    box::Vector<size_t> regionEnd;
    regionStart = zoneTopLeft_;
    regionStart.z(std::max(regionStart.z(), info->startZ()));
    regionEnd = zoneBottomRight_;
    regionEnd.z(std::min(regionEnd.z(), info->startZ() + info->numZ()));
    // Still planting waves?
    if(continuous_ || signal_.time(m_->p()->timeStep()) <= time_) {
        // For each cell in this region...
        for(i = regionStart.x(); i < regionEnd.x(); i++) {
            for(j = regionStart.y(); j < regionEnd.y(); j++) {
                for(k = regionStart.z(); k < regionEnd.z(); k++) {
                    // TODO: This version assumes propagation in Z direction
                    //       Need to use azimuth and elevation.
                    // The material at this location
                    domain::Material* mat = m_->d()->getMaterialAt(i, j, k);
                    // Calculate the electric field intensity in this cell
                    box::Vector<double> pos = m_->p()->geometry()->cellCenter({i, j, k});
                    double distance = pos.z() - zoneCenter_.z();
                    double ex = signal_.sample(m_->p()->timeStep(), distance);
                    ex *= mat->caetangential(0);
                    // Add in the electric field change to the model grid
                    size_t modelIndex = m_->p()->geometry()->index(i, j, k);
                    x[modelIndex] -= ex;
                }
            }
        }
    }
}

// Step the magnetic field on one
void source::ZoneSource::stepH(const box::ThreadInfo* /*info*/, std::vector<double>& /*x*/,
                               std::vector<double>& /*y*/, std::vector<double>& /*z*/) {
#if 0
    // The region this thread is responsible for
    int i, j, k;
    Parameters* p = m_->p();
    box::Vector<int> regionStart;
    box::Vector<int> regionEnd;
    regionStart = zoneTopLeft_;
    regionStart.z(std::max(regionStart.z(), info->startZ()));
    regionEnd = zoneBottomRight_;
    regionEnd.z(std::min(regionEnd.z(), info->startZ() + info->numZ()));
    // Calculations for the whole zone
    double time = (double)m_->p()->timeStep() * m_->p()->dt() + initialTime_;
    double omega = 2.0 * Parameters::pi * frequency_;
    double waveNumber = omega / Parameters::c;
    // Still planting waves?
    if(continuous_ || time <= time_) {
        // For each cell in this region...
        for(i = regionStart.x(); i < regionEnd.x(); i++) {
            for(j = regionStart.y(); j < regionEnd.y(); j++) {
                for(k = regionStart.z(); k < regionEnd.z(); k++) {
                    // TODO: This version assumes propagation in Z direction
                    //       Need to use azimuth and elevation.
                    // Calculate the electric field intensity in this cell
                    box::Vector<double> pos = m_->p()->cellCenter({i, j, k});
                    double distance = pos.z() - zoneCenter_.z();
                    double hx = amplitude_ * Parameters::epsilon0 / Parameters::mu0 *
                                cos(waveNumber * distance - omega * time);
                    // Add in the magnetic field change to the model grid
                    int modelIndex = p->index(i, j, k);
                    int auxIndex = index(i, j, k);
                    x[modelIndex] -= (hx - hx_[auxIndex]);
                    hx_[auxIndex] = hx;
                }
            }
        }
    }
#endif
}

// Return an index into the zone array
size_t source::ZoneSource::index(size_t i, size_t j, size_t k) const {
    size_t result = ((k - zoneTopLeft_.z()) * zoneCells_.y() +
                     (j - zoneTopLeft_.y())) * zoneCells_.x() +
                    (i - zoneTopLeft_.x());
    if(result >= totalCellsInZone_) {
        result = 0;
        std::cout << "ZoneSource::index out of range: " << i << "," << j << ","
                  << k << std::endl;
    }
    return result;
}

// Return true if the source covers this location
bool source::ZoneSource::isAt(const box::Vector<size_t>& pos) {
    return pos.x() >= zoneTopLeft_.x() && pos.x() < zoneBottomRight_.x() &&
           pos.y() >= zoneTopLeft_.y() && pos.y() < zoneBottomRight_.y() &&
           pos.z() >= zoneTopLeft_.z() && pos.z() < zoneBottomRight_.z();
}

// Return the incident E field at the given coordinate if there was nothing in the way
box::Vector<double> source::ZoneSource::eIncident(double i, double j, double k) {
    box::Vector<double> pos(i, j, k);
    double distance = pos.z() - zoneCenter_.z();
    double ex = signal_.sample(m_->p()->timeStep(), distance);
    domain::Material* mat = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    return {ex * mat->caetangential(0), 0.0, 0.0};
}
