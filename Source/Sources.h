/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_SOURCES_H
#define FDTDLIFE_SOURCES_H

#include <list>
#include <cstddef>
#include <Box/Vector.h>
#include <Fdtd/Configuration.h>
#include <Box/FrequencySeq.h>
#include <Xml/DomObject.h>
#include "SourceFactory.h"
#include <vector>
namespace fdtd {class Model;}

namespace source {

    class Source;

    // A class that manages the sources
    class Sources {
    protected:
        // Members
        std::list<Source*> sources_;
        fdtd::Model* m_;
        SourceFactory factory_;
    public:
        // Construction
        explicit Sources(fdtd::Model* m);
        ~Sources();

        // API
        void add(Source* s);
        void remove(Source* s);
        void initialise();
        size_t memoryUse();
        void clear(Source* except=nullptr);
        Source* getSource(int id);
        [[nodiscard]] const std::list<Source*>& sources() const {return sources_;}
        bool writeData(std::ofstream& file);
        bool readData(std::ifstream& file);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        void stepE(const box::ThreadInfo* info, std::vector<double>& x,
                std::vector<double>& y, std::vector<double>& z);
        void stepH(const box::ThreadInfo* info, std::vector<double>& x,
                std::vector<double>& y, std::vector<double>& z);
        void stepBeginE();
        void stepEndE();
        void stepBeginH();
        void stepEndH();
        SourceFactory* factory() {return &factory_;}
        box::FrequencySeq frequencyInfo();
        box::Vector<double> eIncident(double i, double j, double k);
        box::Vector<double> hIncident(double i, double j, double k);
        Source* getAt(const box::Vector<size_t>& p);
        Source* find(const std::string& name);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Sources& v) {
            v.writeConfig(o); return o;}
        friend xml::DomObject& operator>>(xml::DomObject& o, Sources& v) {
            v.readConfig(o); return o;}
        void clearSeqGenerated();
    };
}



#endif //FDTDLIFE_SOURCES_H
