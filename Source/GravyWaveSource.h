/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_GRAVYWAVESOURCE_H
#define FDTDLIFE_GRAVYWAVESOURCE_H


#include "Source.h"
#include <Box/SignalGenerator.h>

namespace fdtd { class Model; }

namespace source {

    // A source that injects a wave into a zone within the model
    class GravyWaveSource : public source::Source {
    public:
        // Construction
        GravyWaveSource() = default;

        // Overrides of Source
        void initialise() override;
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;
        void stepH(const box::ThreadInfo* info, std::vector<double>& x,
                   std::vector<double>& y, std::vector<double>& z) override;
        bool isAt(const box::Vector<size_t>& pos) override;
        void evaluate() override;

        // Getters
        [[nodiscard]]const box::Expression& peakAmplitude() const { return peakAmplitude_; }
        [[nodiscard]]const box::Expression& timeOfMaximum() const { return timeOfMaximum_; }
        [[nodiscard]]const box::Expression& widthAtHalfHeight() const { return widthAtHalfHeight_; }
        [[nodiscard]]const box::Expression& azimuth() const { return azimuth_; }
        [[nodiscard]]const box::Expression& elevation() const { return elevation_; }
        [[nodiscard]]const box::Expression& polarisation() const { return polarisation_; }
        [[nodiscard]]const box::Expression& magneticField() const { return magneticField_; }
        [[nodiscard]]const box::VectorExpression<double>& zoneCenter() const { return zoneCenter_; }
        [[nodiscard]]const box::VectorExpression<double>& zoneSize() const { return zoneSize_; }
        [[nodiscard]]const box::Expression& modulationWavelength() const { return modulationWavelength_; }
        [[nodiscard]]const box::Expression& modulationPhase() const { return modulationPhase_; }
        [[nodiscard]]const box::Expression& modulationAmplitude() const { return modulationAmplitude_; }

        // Setters
        void peakAmplitude(const box::Expression& v) { peakAmplitude_ = v; }
        void timeOfMaximum(const box::Expression& v) { timeOfMaximum_ = v; }
        void widthAtHalfHeight(const box::Expression& v) { widthAtHalfHeight_ = v; }
        void azimuth(const box::Expression& v) { azimuth_ = v; }
        void elevation(const box::Expression& v) { elevation_ = v; }
        void polarisation(const box::Expression& v) { polarisation_ = v; }
        void magneticField(const box::Expression& v) { magneticField_ = v; }
        void zoneCenter(const box::VectorExpression<double>& v) { zoneCenter_ = v; }
        void zoneSize(const box::VectorExpression<double>& v) { zoneSize_ = v; }
        void modulationWavelength(const box::Expression& v) { modulationWavelength_ = v; }
        void modulationPhase(const box::Expression& v) { modulationPhase_ = v; }
        void modulationAmplitude(const box::Expression& v) { modulationAmplitude_ = v; }

        // API
        std::string getMagneticFileProfileCsv();

    protected:
        // Persistent Attributes
        box::Expression azimuth_{0.0};  // Angle between the x axis and the projection onto the zx plane
        box::Expression elevation_{0.0};  // Angle to the z axis in degrees
        box::Expression polarisation_{0.0};  // The polarization in degrees
        box::Expression magneticField_{5.0};  // The amplitude of the magnetic field
        box::Expression peakAmplitude_{1.0}; // The peak amplitude of the Gaussian
        box::Expression timeOfMaximum_{0.0};  // The time at which the maximum will occur
        box::Expression widthAtHalfHeight_{0.0001};  // The width of the gaussian at half height
        box::VectorExpression<double> zoneCenter_{0.0, 0.0, 0.0};  // The coordinate of the center of the zone
        box::VectorExpression<double> zoneSize_{0.0001,0.0001,0.0001};  // The size of the zone
        box::Expression modulationWavelength_{0.0};  // Magnetic field modulation wavelength, 0=constant
        box::Expression modulationPhase_{0.0};  // Magnetic field modulation phase, in degrees
        box::Expression modulationAmplitude_{0.0};  // Magnetic field modulation phase, in degrees

        // Derived attributes
        box::Vector<size_t> zoneTopLeft_;  // The top left zone cell
        box::Vector<size_t> zoneBottomRight_;  // The bottom right zone cell
        box::SignalGenerator signal_;  // The generator of the basic signal
        box::Vector<double> incidentCorner_;  // The coordinate of the corner first encountered
        double sinElevation_{};   // Precalculated cos and sine values
        double cosElevation_{};
        double sinAzimuth_{};
        double cosAzimuth_{};
        double cosPolarisation_{};

        // Helpers
        double calcMagneticField(size_t zOffset);
    };

}

#endif //FDTDLIFE_GRAVYWAVESOURCE_H
