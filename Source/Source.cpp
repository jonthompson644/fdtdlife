#include <utility>

/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Source.h"
#include <Fdtd/Model.h>
#include <Xml/DomObject.h>

// Constructor
source::Source::Source() :
        frequencies_(1 * box::Constants::tera_,
                     1 * box::Constants::tera_, 1) {
    if(m_ != nullptr) {
        m_->sources().add(this);
    }
}

// Second stage constructor used by factory
void source::Source::construct(int mode, int id, fdtd::Model* m, std::string name) {
    identifier_ = id;
    name_ = std::move(name);
    mode_ = mode;
    m_ = m;
    m_->sources().add(this);
}

// Destructor
source::Source::~Source() {
    if(m_ != nullptr) {
        m_->sources().remove(this);
    }
}

// Clear the source to its pristine state
void source::Source::clear() {
    name_ = "";
    frequencies_ = {1 * box::Constants::tera_, 1 * box::Constants::tera_, 1};
}

// Return the parameters from the model
fdtd::Configuration* source::Source::p() {
    return m_->p();
}

// Write the parameters to the XML DOM object
void source::Source::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("name") << name_;
    root << frequencies_;
    root << xml::Obj("colour") << colour_;
    root << xml::Obj("seqgenerated") << seqGenerated_;
    root << xml::Close();
}

// Read the parameters from the XML DOM object
void source::Source::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> frequencies_;
    root >> xml::Obj("colour") >> xml::Default(box::Constants::colourCyan) >> colour_;
    root >> xml::Obj("seqgenerated") >> xml::Default(false) >> seqGenerated_;
    root >> xml::Close();
    // Mode and identifier are read by the factory
}

// Evaluate the expressions
void source::Source::evaluate() {
    box::Expression::Context c;
    m_->variables().fillContext(c);
    try {
        frequencies_.evaluate(c);
    } catch(box::Expression::Exception& e) {
        std::cout << e.what() << std::endl;
    }
}
