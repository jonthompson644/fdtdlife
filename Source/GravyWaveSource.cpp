/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "GravyWaveSource.h"
#include <Fdtd/Model.h>
#include <algorithm>

// Initialise ready for a run
void source::GravyWaveSource::initialise() {
    evaluate();
    // The extent of the zone in cells
    zoneTopLeft_ = m_->p()->geometry()->cellFloor(zoneCenter_.value() - (zoneSize_.value() / 2.0));
    zoneBottomRight_ = m_->p()->geometry()->cellFloor(zoneCenter_.value() + (zoneSize_.value() / 2.0));
    // Set up the signal generator
    signal_.initialise(m_->p()->dt(), frequencies_.value(),
                       0.0, peakAmplitude_.value());
    // The first corner of the magnetic field zone the wave encounters
    if (elevation_.value() <= 90.0) {
        if (azimuth_.value() >= 0.0 && azimuth_.value() <= 90.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(0.5, 0.5, -0.5));
        } else if (azimuth_.value() > 90.0 && azimuth_.value() <= 180.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(-0.5, 0.5, -0.5));
        } else if (azimuth_.value() > 180.0 && azimuth_.value() <= 270.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(-0.5, -0.5, -0.5));
        } else {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(0.5, -0.5, -0.5));
        }
    } else {
        if (azimuth_.value() >= 0.0 && azimuth_.value() <= 90.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(0.5, 0.5, 0.5));
        } else if (azimuth_.value() > 90.0 && azimuth_.value() <= 180.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(-0.5, 0.5, 0.5));
        } else if (azimuth_.value() > 180.0 && azimuth_.value() <= 270.0) {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(-0.5, -0.5, 0.5));
        } else {
            incidentCorner_ = zoneCenter_.value() +
                              (zoneSize_.value() * box::Vector<double>(0.5, -0.5, 0.5));
        }
    }
    // Precalculate some cos and sin values
    // Note that the elevation is entered measured from the negative going z axis
    // but the calculation requires it from the positive z axis, hence the '180-' below.
    sinElevation_ = sin((180.0 - elevation_.value()) * box::Constants::deg2rad_);
    cosElevation_ = cos((180.0 - elevation_.value()) * box::Constants::deg2rad_);
    sinAzimuth_ = sin(azimuth_.value() * box::Constants::deg2rad_);
    cosAzimuth_ = cos(azimuth_.value() * box::Constants::deg2rad_);
    cosPolarisation_ = cos(polarisation_.value() * box::Constants::deg2rad_);
}

// Write the configuration to the DOM object
void source::GravyWaveSource::writeConfig(xml::DomObject& root) const {
    // Base class first
    Source::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("peakamplitude") << peakAmplitude_;
    root << xml::Obj("timeofmaximum") << timeOfMaximum_;
    root << xml::Obj("widthathalfheight") << widthAtHalfHeight_;
    root << xml::Obj("azimuth") << azimuth_;
    root << xml::Obj("elevation") << elevation_;
    root << xml::Obj("polarisation") << polarisation_;
    root << xml::Obj("magneticfield") << magneticField_;
    root << xml::Obj("center") << xml::Open()
         << xml::Obj("x") << zoneCenter_.x()
         << xml::Obj("y") << zoneCenter_.y()
         << xml::Obj("z") << zoneCenter_.z()
         << xml::Close();
    root << xml::Obj("size") << xml::Open()
         << xml::Obj("x") << zoneSize_.x()
         << xml::Obj("y") << zoneSize_.y()
         << xml::Obj("z") << zoneSize_.z()
         << xml::Close();
    root << xml::Obj("modulationwavelength") << modulationWavelength_;
    root << xml::Obj("modulationphase") << modulationPhase_;
    root << xml::Obj("modulationamplitude") << modulationAmplitude_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void source::GravyWaveSource::readConfig(xml::DomObject& root) {
    // Base class first
    Source::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("peakamplitude") >> peakAmplitude_;
    root >> xml::Obj("timeofmaximum") >> timeOfMaximum_;
    root >> xml::Obj("widthathalfheight") >> widthAtHalfHeight_;
    root >> xml::Obj("azimuth") >> azimuth_;
    root >> xml::Obj("elevation") >> elevation_;
    root >> xml::Obj("polarisation") >> polarisation_;
    root >> xml::Obj("magneticfield") >> magneticField_;
    if (!root.obj("center").empty()) {
        root >> xml::Obj("center") >> xml::Open()
             >> xml::Obj("x") >> zoneCenter_.x()
             >> xml::Obj("y") >> zoneCenter_.y()
             >> xml::Obj("z") >> zoneCenter_.z()
             >> xml::Close();
    } else {
        // Backwards compatible
        root >> xml::Obj("xcenter") >> zoneCenter_.x();
        root >> xml::Obj("ycenter") >> zoneCenter_.y();
        root >> xml::Obj("zcenter") >> zoneCenter_.z();
    }
    if (!root.obj("size").empty()) {
        root >> xml::Obj("size") >> xml::Open()
             >> xml::Obj("x") >> zoneSize_.x()
             >> xml::Obj("y") >> zoneSize_.y()
             >> xml::Obj("z") >> zoneSize_.z()
             >> xml::Close();
    } else {
        // Backwards compatible
        root >> xml::Obj("xsize") >> zoneSize_.x();
        root >> xml::Obj("ysize") >> zoneSize_.y();
        root >> xml::Obj("zsize") >> zoneSize_.z();
    }
    root >> xml::Obj("modulationwavelength") >> xml::Default(0) >> modulationWavelength_;
    root >> xml::Obj("modulationphase") >> xml::Default(0) >> modulationPhase_;
    root >> xml::Obj("modulationamplitude") >> xml::Default(0) >> modulationAmplitude_;
    root >> xml::Close();
}

// Step the magnetic field on one
// In this first version we will just imprint a disturbance in the
// magnetic field.
// This version only copes with the wave travelling in the z direction
void source::GravyWaveSource::stepH(const box::ThreadInfo* info, std::vector<double>& x,
                                    std::vector<double>& /*y*/, std::vector<double>& /*z*/) {
    // The region this thread is responsible for
    size_t i, j, k;
    fdtd::Configuration* p = m_->p();
    fdtd::Geometry* g = p->geometry();
    box::Vector<size_t> regionStart;
    box::Vector<size_t> regionEnd;
    regionStart = zoneTopLeft_;
    regionStart.z(std::max(regionStart.z(), info->startZ()));
    regionEnd = zoneBottomRight_;
    regionEnd.z(std::min(regionEnd.z(), info->startZ() + info->numZ()));
    // Update the zone we are responsible for
    for (i = regionStart.x() + 1; i < regionEnd.x(); i++) {
        for (j = regionStart.y() + 1; j < regionEnd.y(); j++) {
            for (k = regionStart.z() + 1; k < regionEnd.z(); k++) {
                size_t modelIndex = g->index(i, j, k);
                if (isAt({i, j, k})) {
                    box::Vector<double> r = g->cellCenter({i, j, k}) - incidentCorner_;
                    double l = r.x() * sinElevation_ * cosAzimuth_ +
                               r.y() * sinElevation_ * sinAzimuth_ +
                               r.z() * cosElevation_;
                    double t0 = m_->p()->dt() * (p->timeStep() - 1) + l / box::Constants::c_;
                    double t1 = m_->p()->dt() * p->timeStep() + l / box::Constants::c_;
                    double magneticField = calcMagneticField(k - zoneTopLeft_.z());
                    double common0 = magneticField / box::Constants::mu0_ *
                                     cosPolarisation_ * cosElevation_ *
                                     exp(-(t0 - timeOfMaximum_.value()) *
                                         (t0 - timeOfMaximum_.value()) /
                                         (2.0 * widthAtHalfHeight_.value() * widthAtHalfHeight_.value()));
                    double common1 = magneticField / box::Constants::mu0_ *
                                     cosPolarisation_ * cosElevation_ *
                                     exp(-(t1 - timeOfMaximum_.value()) *
                                         (t1 - timeOfMaximum_.value()) /
                                         (2.0 * widthAtHalfHeight_.value() * widthAtHalfHeight_.value()));
                    double h0 = signal_.sample(p->timeStep() - 1, l) * common0;
                    double h1 = signal_.sample(p->timeStep(), l) * common1;
                    x[modelIndex] -= (h1 - h0);
                }
            }
        }
    }
}

// Return true if the source covers the specified position
bool source::GravyWaveSource::isAt(const box::Vector<size_t>& pos) {
    return pos.x() >= zoneTopLeft_.x() && pos.x() < zoneBottomRight_.x() &&
           pos.y() >= zoneTopLeft_.y() && pos.y() < zoneBottomRight_.y() &&
           pos.z() >= zoneTopLeft_.z() && pos.z() < zoneBottomRight_.z();
}

// Calculate the magnetic field at the specified z offset into the zone
double source::GravyWaveSource::calcMagneticField(size_t zOffset) {
    double result = magneticField_.value();
    if (modulationWavelength_.value() > 0.0) {
        double modPhase = modulationPhase_.value() * M_PI / 180.0;
        double modPos = static_cast<double>(zOffset) * m_->p()->dr().z().value() /
                        modulationWavelength_.value() * 2.0 * M_PI;
        double modAngle = std::fmod(modPos + modPhase, 2.0 * M_PI);
        result += modulationAmplitude_.value() * std::sin(modAngle);
    }
    return result;
}

// Evaluate any expressions
void source::GravyWaveSource::evaluate() {
    Source::evaluate();
    box::Expression::Context c;
    m_->variables().fillContext(c);
    try {
        azimuth_.evaluate(c);
        elevation_.evaluate(c);
        polarisation_.evaluate(c);
        magneticField_.evaluate(c);
        peakAmplitude_.evaluate(c);
        timeOfMaximum_.evaluate(c);
        widthAtHalfHeight_.evaluate(c);
        zoneCenter_.evaluate(c);
        zoneSize_.evaluate(c);
        modulationWavelength_.evaluate(c);
        modulationPhase_.evaluate(c);
        modulationAmplitude_.evaluate(c);
    } catch (box::Expression::Exception& e) {
        std::cout << e.what() << std::endl;
    }
}

// Return a string containing the magnetic field profile of the source in CSV format
std::string source::GravyWaveSource::getMagneticFileProfileCsv() {
    fdtd::Configuration* p = m_->p();
    std::stringstream text;
    text << "z(m)\tH(T)" << std::endl;
    for (size_t k = 0; k < (zoneBottomRight_.z() - zoneTopLeft_.z()); k++) {
        double magneticField = calcMagneticField(k);
        text << static_cast<double>(k) * p->dr().z().value()
             << "\t" << magneticField << std::endl;
    }
    return text.str();
}
