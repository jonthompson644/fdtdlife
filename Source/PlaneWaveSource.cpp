/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "PlaneWaveSource.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <Fdtd/Model.h>
#include <Domain/Material.h>
#include <Box/BinaryFileHeader.h>
#include <Xml/DomObject.h>
#include <Source/SourceFactory.h>
#include <Box/Constants.h>

// Clear the source to its pristine state
void source::PlaneWaveSource::clear() {
    amplitude_ = 1.0;
    polarisation_ = 0.0;
    pmlSigma_ = 100.0;
    pmlSigmaFactor_ = 1.0;
    name_ = "";
    continuous_ = true;
    time_ = 1.0;
    azimuth_ = 0.0;
    initialTime_ = 0.0;
    distribution_ = Distribution::flat;
    gaussianB_ = 1.0;
    gaussianC_ = 1.0;
}

// Initialise the source ready for a run
void source::PlaneWaveSource::initialise() {
    evaluate();
    // How long, in cells, does the auxiliary grid need to be?
    // We will use the z size of the main grid.  Then we multiply
    // by two to allow room for a significant PML.
    incidentN_ = static_cast<size_t>(p()->geometry()->n().z()) * 2U;
    // Create the data
    if(incidentN_ > 0) {
        ex_.resize(incidentN_);
        hy_.resize(incidentN_);
        for(size_t i = 0; i < incidentN_; i++) {
            ex_[i] = 0.0;
            hy_[i] = 0.0;
        }
    }
    // Material properties
    domain::Material* defaultMat = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    epsilon_ = defaultMat->epsilon().value();
    mu_ = defaultMat->mu().value();
    // Polarisation
    sinPolarisation_ = sin(polarisation_.value() * box::Constants::deg2rad_);
    cosPolarisation_ = cos(polarisation_.value() * box::Constants::deg2rad_);
    // Azimuth
    sinAzimuth_ = sin(azimuth_.value() * box::Constants::deg2rad_);
    cosAzimuth_ = cos(azimuth_.value() * box::Constants::deg2rad_);
    // The first corner the wave encounters
    if(azimuth_.value() >= 0.0 && azimuth_.value() <= 90.0) {
        incidentCorner_ = p()->geometry()->tfStart();
    } else if(azimuth_.value() > 90.0 && azimuth_.value() <= 180.0) {
        incidentCorner_ = p()->geometry()->tfStart();
        incidentCorner_.z(p()->geometry()->tfEnd().z());
    } else if(azimuth_.value() > 180.0 && azimuth_.value() <= 270.0) {
        incidentCorner_ = p()->geometry()->tfEnd();
    } else {
        incidentCorner_ = p()->geometry()->tfEnd();
        incidentCorner_.z(p()->geometry()->tfStart().z());
    }
    // Cell size
    if(fabs(p()->dr().z().value() * tan(azimuth_.value())) > p()->dr().y().value()) {
        dr_ = fabs(p()->dr().y().value() / sin(azimuth_.value()));
    } else {
        dr_ = fabs(p()->dr().z().value() / cos(azimuth_.value()));
    }
    // Gaussian wave calculations
    gaussianA_ = std::exp((gaussianB_ * gaussianB_) / (2 * gaussianC_ * gaussianC_));
    wavefrontLength_ = static_cast<double>(p()->geometry()->n().x());
}

// Return the incident wave component
box::Vector<double> source::PlaneWaveSource::eIncident(double i, double j, double k) {
    // Get the distance into the wave
    double d = distanceToIncidentCorner(i, j, k);
    // Interpolate from the array
    double value = interpolate(d, ex_) * positionAmplitude(i, j, k);
    box::Vector<double> result = box::Vector<double>(
            value * cosPolarisation_,
            value * sinPolarisation_ * cosAzimuth_,
            -value * sinPolarisation_ * sinAzimuth_);
    return result;
}

// Return the incident wave component
// Note the distance calculation accounting for the fact that
// the magnetic field array is half a step advanced.
box::Vector<double> source::PlaneWaveSource::hIncident(double i, double j, double k) {
    // Get the distance into the wave
    // Account for the magnetic field being a half step advanced
    double d = distanceToIncidentCorner(i, j, k) - 0.5;
    d = std::max(d, 0.0);
    // Interpolate from the array
    double value = interpolate(d, hy_) * positionAmplitude(i, j, k);
    box::Vector<double> result = box::Vector<double>(
            -value * sinPolarisation_,
            value * cosPolarisation_ * cosAzimuth_,
            -value * cosPolarisation_ * sinAzimuth_);
    return result;
}

// Calculate the distance from the point projected onto the incident wave to
// the corner the wave first encounters.
double source::PlaneWaveSource::distanceToIncidentCorner(double /*i*/, double j, double k) {
    // Calculate the distance of the point projected onto the wave
    // from the incident corner
    double d = (j - static_cast<double>(incidentCorner_.y())) * sinAzimuth_ +
               (k - static_cast<double>(incidentCorner_.z())) * cosAzimuth_;
    // Now add in the scattered field zone size
    d += p()->scatteredFieldZoneSize();
    return d;
}

// Interpolate into an array of data
double source::PlaneWaveSource::interpolate(double d, const std::vector<double>& data) {
    double n = floor(d);
    double frac = d - n;
    double result = 0.0;
    if(n >= 0.0) {
        result = data[index(static_cast<size_t>(n))] * (1 - frac) +
                 data[index(static_cast<size_t>(n) + 1)] * frac;
    }
    return result;
}

// Move the auxiliary grid on one time step
void source::PlaneWaveSource::stepAuxiliaryE() {
    bool printing = p()->geometry()->n().x() <= 10 &&
                    p()->geometry()->n().y() <= 10 &&
                    p()->geometry()->n().z() <= 10;
    if(printing) {
        std::cout << "E: ";
    }
    // Calculate the material coefficients
    double c1 = 1;
    double c2 = p()->dt() / epsilon_ / p()->dr().z().value();
    // Now advance the grid
    double sigmaFactor = pmlSigmaFactor_;
    for(size_t i = 0; i < incidentN_; i++) {
        int pmlD = pmlZoneDist(i);
        if(i == 0) {
            // The source is here
            ex_[index(i)] = 0.0;
            if(continuous_ || p()->dt() * p()->timeStep() < time_.value()) {
                for(size_t j = 0; j < frequencies_.n().value(); j++) {
                    ex_[index(i)] +=
                            sin(2 * box::Constants::pi_ * frequencies_.value().frequency(j) *
                                (p()->dt() * p()->timeStep() + initialTime_.value())) *
                            amplitude_.value() / frequencies_.n().value();
                }
            }
        } else if(pmlD >= 0) {
            // Calculate sigma
            if(pmlD > 0) {
                sigmaFactor *= pmlSigmaFactor_;
            }
            double sigma = pmlSigma_ * sigmaFactor;
            // Calculate the PML coefficients
            double d1 = (1 - sigma * p()->dt() / 2.0 / epsilon_) /
                        (1 + sigma * p()->dt() / 2.0 / epsilon_);
            double d2 = p()->dt() / epsilon_ /
                        (1 + sigma * p()->dt() / 2.0 / epsilon_) / dr_;
            // Do the step
            ex_[index(i)] = d1 * ex_[index(i)] - d2 * (hy_[index(i)] - hy_[index(i - 1)]);
        } else {
            // Normal stepping zone
            ex_[index(i)] = c1 * ex_[index(i)] - c2 * (hy_[index(i)] - hy_[index(i - 1)]);
        }
        if(printing) {
            std::cout << ex_[index(i)] << "  ";
        }
    }
    if(printing) {
        std::cout << std::endl;
    }
}

// Move the auxiliary grid on one time step
void source::PlaneWaveSource::stepAuxiliaryH() {
    bool printing = p()->geometry()->n().x() <= 10 &&
                    p()->geometry()->n().y() <= 10 &&
                    p()->geometry()->n().z() <= 10;
    if(printing) {
        std::cout << "H: ";
    }
    // Calculate the vacuum coefficients
    double c1 = 1;
    double c2 = p()->dt() / mu_ / p()->dr().z().value();
    // Now advance the grid
    double sigmaFactor = pmlSigmaFactor_;
    for(size_t i = 0; i < incidentN_ - 1; i++) {
        int pmlD = pmlZoneDist(i);
        if(pmlD >= 0) {
            // Calculate the PML coefficients
            if(pmlD > 0) {
                sigmaFactor *= pmlSigmaFactor_;
            }
            double sigmastar = pmlSigma_ * sigmaFactor * mu_ / epsilon_;
            double d1 = (1 - sigmastar * p()->dt() / 2.0 / mu_) /
                        (1 + sigmastar * p()->dt() / 2.0 / mu_);
            double d2 =
                    p()->dt() / mu_ / (1 + sigmastar * p()->dt() / 2.0 / mu_) / dr_;
            // PML zone
            hy_[index(i)] = d1 * hy_[index(i)] - d2 * (ex_[index(i + 1)] - ex_[index(i)]);
        } else {
            // Normal stepping zone
            hy_[index(i)] = c1 * hy_[index(i)] - c2 * (ex_[index(i + 1)] - ex_[index(i)]);
        }
        if(printing) {
            std::cout << hy_[index(i)] << "  ";
        }
    }
    if(printing) {
        std::cout << hy_[index(incidentN_ - 1)] << "  " << std::endl;
    }
}

// Return the amount of memory used
size_t source::PlaneWaveSource::memoryUse() {
    size_t result = 2 * incidentN_ * sizeof(double);
    return result;
}

// Return the array index for a coordinate
size_t source::PlaneWaveSource::index(size_t i) const {
    size_t result = i;
    if(result >= incidentN_) {
        std::cout << "PlaneWaveSource: index out of range " << i << std::endl;
        result = 0;
    }
    return result;
}

// Write the source data to the binary file
bool source::PlaneWaveSource::writeData(std::ofstream& file) {
    box::BinaryFileHeader header("SOURCE", 2,
                                 incidentN_ * 4 * sizeof(double));
    file.write(header.data(), static_cast<std::streamsize>(header.size()));
    auto partSize = static_cast<std::streamsize>(incidentN_ * sizeof(double));
    file.write(reinterpret_cast<const char*>(ex_.data()), partSize);
    file.write(reinterpret_cast<const char*>(hy_.data()), partSize);
    return true;
}

// Read the source data from the binary file
bool source::PlaneWaveSource::readData(std::ifstream& file) {
    bool result = false;
    box::BinaryFileHeader header{};
    file.read(header.data(), static_cast<std::streamsize>(header.size()));
    if(header.is("SOURCE")) {
        size_t arraySize = 0;
        auto partSize = static_cast<std::streamsize>(incidentN_ * sizeof(double));
        switch(header.version()) {
            case 1:
                arraySize = header.recordSize() / 2 / sizeof(double);
                file.read(reinterpret_cast<char*>(ex_.data()), partSize);
                file.read(reinterpret_cast<char*>(hy_.data()), partSize);
                result = incidentN_ == arraySize;
                break;
            default:
                break;
        }
    }
    return result;
}

// Write the parameters to the XML DOM object
void source::PlaneWaveSource::writeConfig(xml::DomObject& root) const {
    Source::writeConfig(root);
    root << xml::Reopen();
    root << xml::Obj("amplitude") << amplitude_;
    root << xml::Obj("polarisation") << polarisation_;
    root << xml::Obj("pmlsigma") << pmlSigma_;
    root << xml::Obj("pmlsigmafactor") << pmlSigmaFactor_;
    root << xml::Obj("continuous") << continuous_;
    root << xml::Obj("time") << time_;
    root << xml::Obj("azimuth") << azimuth_;
    root << xml::Obj("initialtime") << initialTime_;
    root << xml::Obj("distribution") << distribution_;
    root << xml::Obj("gaussianb") << gaussianB_;
    root << xml::Obj("gaussianc") << gaussianC_;
    root << xml::Close();
}

// Read the parameters from the XML DOM object
void source::PlaneWaveSource::readConfig(xml::DomObject& root) {
    Source::readConfig(root);
    root >> xml::Reopen();
    root >> xml::Obj("amplitude") >> amplitude_;
    root >> xml::Obj("polarisation") >> polarisation_;
    root >> xml::Obj("pmlsigma") >> pmlSigma_;
    root >> xml::Obj("pmlsigmafactor") >> pmlSigmaFactor_;
    root >> xml::Obj("continuous") >> continuous_;
    root >> xml::Obj("time") >> time_;
    root >> xml::Obj("azimuth") >> azimuth_;
    root >> xml::Obj("initialtime") >> xml::Default(initialTime_.value())
         >> initialTime_;
    root >> xml::Obj("distribution")
         >> xml::Default(static_cast<int>(Distribution::flat))
         >> distribution_;
    root >> xml::Obj("gaussianb") >> xml::Default(1.0) >> gaussianB_;
    root >> xml::Obj("gaussianc") >> xml::Default(1.0) >> gaussianC_;
    root >> xml::Close();
}

// Perform the electric total field/scattered field corrections for the plane wave
void source::PlaneWaveSource::doTfsfE(const box::ThreadInfo* info, std::vector<double>& x,
                                      std::vector<double>& y, std::vector<double>& z) {
    // Do the TFSF corrections around the boundary
    size_t i, j, k;
    fdtd::Configuration* p = m_->p();
    fdtd::Geometry* g = p->geometry();
    box::Vector<size_t> tfStart;
    box::Vector<size_t> tfEnd;
    tfStart = g->tfStart();
    tfStart.z(std::max(static_cast<size_t>(tfStart.z()), info->startZ()));
    tfEnd = g->tfEnd();
    tfEnd.z(std::min(static_cast<size_t>(tfEnd.z()), info->startZ() + info->numZ() - 1));
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfStart.x() - 1;
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hy = hIncident(i + 1.0, j + 0.5, k + 0.0).y();
                size_t idx = g->index(i, j, k);
                z[idx] -= p->dt() / epsilon_ / p->dr().y().value() * hy;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfStart.x() - 1;
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hz = hIncident(i + 1.0, j + 0.0, k + 0.5).z();
                size_t idx = g->index(i, j, k);
                y[idx] += p->dt() / epsilon_ / p->dr().y().value() * hz;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfEnd.x();
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hy = hIncident(i + 1.0, j + 0.5, k + 0.0).y();
                size_t idx = g->index(i, j, k);
                z[idx] += p->dt() / epsilon_ / p->dr().y().value() * hy;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfEnd.x();
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hz = hIncident(i + 1.0, j + 0.0, k + 0.5).z();
                size_t idx = g->index(i, j, k);
                y[idx] -= p->dt() / epsilon_ / p->dr().y().value() * hz;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfStart.y() - 1;
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hz = hIncident(i + 0.0, j + 1.0, k + 0.5).z();
                size_t idx = g->index(i, j, k);
                x[idx] -= p->dt() / epsilon_ / p->dr().x().value() * hz;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfStart.y() - 1;
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hx = hIncident(i + 0.5, j + 1.0, k + 0.0).x();
                size_t idx = g->index(i, j, k);
                z[idx] += p->dt() / epsilon_ / p->dr().x().value() * hx;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfEnd.y();
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hz = hIncident(i + 0.0, j + 1.0, k + 0.5).z();
                size_t idx = g->index(i, j, k);
                x[idx] += p->dt() / epsilon_ / p->dr().x().value() * hz;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfEnd.y();
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double hx = hIncident(i + 0.5, j + 1.0, k + 0.0).x();
                size_t idx = g->index(i, j, k);
                z[idx] += p->dt() / epsilon_ / p->dr().x().value() * hx;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfStart().z() >= info->startZ() &&
       g->tfStart().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfStart().z() - 1;
                double hy = hIncident(i + 0.0, j + 0.5, k + 1.0).y();
                size_t idx = g->index(i, j, k);
                x[idx] += p->dt() / epsilon_ / p->dr().z().value() * hy;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfStart().z() >= info->startZ() &&
       g->tfStart().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfStart().z() - 1;
                double hx = hIncident(i + 0.5, j + 0.0, k + 1.0).x();
                size_t idx = g->index(i, j, k);
                y[idx] -= p->dt() / epsilon_ / p->dr().z().value() * hx;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfEnd().z() >= info->startZ() &&
       g->tfEnd().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfEnd().z();
                double hy = hIncident(i + 0.0, j + 0.5, k + 1.0).y();
                size_t idx = g->index(i, j, k);
                x[idx] -= p->dt() / epsilon_ / p->dr().z().value() * hy;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfEnd().z() >= info->startZ() &&
       g->tfEnd().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfEnd().z();
                double hx = hIncident(i + 0.5, j + 0.0, k + 1.0).x();
                size_t idx = g->index(i, j, k);
                y[idx] += p->dt() / epsilon_ / p->dr().z().value() * hx;
            }
        }
    }
}

// Perform the magnetic total field/scattered field corrections for the plane wave
void source::PlaneWaveSource::doTfsfH(const box::ThreadInfo* info, std::vector<double>& x,
                                      std::vector<double>& y, std::vector<double>& z) {
    size_t i, j, k;
    fdtd::Configuration* p = m_->p();
    fdtd::Geometry* g = p->geometry();
    box::Vector<size_t> tfStart;
    box::Vector<size_t> tfEnd;
    tfStart = g->tfStart();
    tfStart.z(std::max(static_cast<size_t>(tfStart.z()), info->startZ()));
    tfEnd = g->tfEnd();
    tfEnd.z(std::min(static_cast<size_t>(tfEnd.z()), info->startZ() + info->numZ() - 1));
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfStart.x();
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ez = eIncident(i - 0.5, j + 0.5, k + 0.0).z();
                size_t idx = g->index(i, j, k);
                y[idx] -= p->dt() / mu_ / p->dr().y().value() * ez;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfStart.x();
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ey = eIncident(i - 0.5, j + 0.0, k + 0.5).y();
                size_t idx = g->index(i, j, k);
                z[idx] += p->dt() / mu_ / p->dr().y().value() * ey;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfEnd.x() + 1;
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ez = eIncident(i - 0.5, j + 0.5, k + 0.0).z();
                size_t idx = g->index(i, j, k);
                y[idx] += p->dt() / mu_ / p->dr().y().value() * ez;
            }
        }
    }
    if(p->boundary().x() != fdtd::Configuration::Boundary::periodic) {
        i = tfEnd.x() + 1;
        for(j = tfStart.y(); j <= tfEnd.y(); j++) {
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ey = eIncident(i - 0.5, j + 0.0, k + 0.5).y();
                size_t idx = g->index(i, j, k);
                z[idx] -= p->dt() / mu_ / p->dr().y().value() * ey;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfStart.y();
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ex = eIncident(i + 0.0, j - 0.5, k + 0.5).x();
                size_t idx = g->index(i, j, k);
                z[idx] -= p->dt() / mu_ / p->dr().x().value() * ex;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfStart.y();
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ez = eIncident(i + 0.5, j - 0.5, k + 0.0).z();
                size_t idx = g->index(i, j, k);
                x[idx] += p->dt() / mu_ / p->dr().x().value() * ez;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfEnd.y() + 1;
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ex = eIncident(i + 0.0, j - 0.5, k + 0.5).x();
                size_t idx = g->index(i, j, k);
                z[idx] += p->dt() / mu_ / p->dr().x().value() * ex;
            }
        }
    }
    if(p->boundary().y() != fdtd::Configuration::Boundary::periodic) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            j = tfEnd.y() + 1;
            for(k = tfStart.z(); k <= tfEnd.z(); k++) {
                double ez = eIncident(i + 0.5, j - 0.5, k + 0.0).z();
                size_t idx = g->index(i, j, k);
                x[idx] -= p->dt() / mu_ / p->dr().x().value() * ez;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfStart().z() >= info->startZ() &&
       g->tfStart().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfStart().z();
                double ex = eIncident(i + 0.0, j + 0.5, k - 0.5).x();
                size_t idx = g->index(i, j, k);
                y[idx] += p->dt() / mu_ / p->dr().z().value() * ex;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfStart().z() >= info->startZ() &&
       g->tfStart().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfStart().z();
                double ey = eIncident(i + 0.5, j + 0.0, k - 0.5).y();
                size_t idx = g->index(i, j, k);
                x[idx] -= p->dt() / mu_ / p->dr().z().value() * ey;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfEnd().z() >= info->startZ() &&
       g->tfEnd().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfEnd().z() + 1;
                double ex = eIncident(i + 0.0, j + 0.5, k - 0.5).x();
                size_t idx = g->index(i, j, k);
                y[idx] -= p->dt() / mu_ / p->dr().z().value() * ex;
            }
        }
    }
    if(p->boundary().z() != fdtd::Configuration::Boundary::periodic &&
       g->tfEnd().z() >= info->startZ() &&
       g->tfEnd().z() < (info->startZ() + info->numZ())) {
        for(i = tfStart.x(); i <= tfEnd.x(); i++) {
            for(j = tfStart.y(); j <= tfEnd.y(); j++) {
                k = g->tfEnd().z() + 1;
                double ey = eIncident(i + 0.5, j + 0.0, k - 0.5).y();
                size_t idx = g->index(i, j, k);
                x[idx] += p->dt() / mu_ / p->dr().z().value() * ey;
            }
        }
    }
}


// Perform the electric field step
void source::PlaneWaveSource::stepE(const box::ThreadInfo* info, std::vector<double>& x,
                                    std::vector<double>& y, std::vector<double>& z) {
    doTfsfE(info, x, y, z);
}

// Perform the magnetic field step
void source::PlaneWaveSource::stepH(const box::ThreadInfo* info, std::vector<double>& x,
                                    std::vector<double>& y, std::vector<double>& z) {
    doTfsfH(info, x, y, z);
}

// Electric field step finalisation
void source::PlaneWaveSource::stepEndE() {
    stepAuxiliaryE();
}

// Magnetic field step finalisation
void source::PlaneWaveSource::stepEndH() {
    stepAuxiliaryH();
}

// Return the amplitude multiplier of the incident wave that depends on the
// position along the wavefront.
double source::PlaneWaveSource::positionAmplitude(double i, double /*j*/, double /*k*/) {
    double result;
    switch(distribution_) {
        case Distribution::flat:
            result = 1.0;
            break;
        case Distribution::gaussian: {
            // Calculate the distance along the wave front from its center
            double distance = i - wavefrontLength_ / 2.0;
            double xminusb = distance - gaussianB_;
            result = gaussianA_ *
                     std::exp(-(xminusb * xminusb) / (2 * gaussianC_ * gaussianC_));
            break;
        }
    }
    return result;
}

// Evaluate the expressions
void source::PlaneWaveSource::evaluate() {
    // Base class first
    Source::evaluate();
    // My ones
    box::Expression::Context c;
    m_->variables().fillContext(c);
    try {
        amplitude_.evaluate(c);
        polarisation_.evaluate(c);
        time_.evaluate(c);
        azimuth_.evaluate(c);
        initialTime_.evaluate(c);
    } catch(box::Expression::Exception& e) {
        std::cout << e.what() << std::endl;
    }
}

