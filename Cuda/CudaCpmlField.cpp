/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CudaCpmlField.h"
#include "CudaIncludes.h"
#include "CudaCommon.h"
#include "CudaIndexing.h"
#include <Parameters.h>
#include <BinaryFileHeader.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>

// Constructor
CudaCpmlField::CudaCpmlField(fdtd::Parameters* p, CudaCommon* common) :
        x_(&common->cudaProxy_),
        y_(&common->cudaProxy_),
        z_(&common->cudaProxy_),
        xyPsi_(&common->cudaProxy_),
        xzPsi_(&common->cudaProxy_),
        yzPsi_(&common->cudaProxy_),
        yxPsi_(&common->cudaProxy_),
        zxPsi_(&common->cudaProxy_),
        zyPsi_(&common->cudaProxy_),
        yxPsiL_(&common->cudaProxy_),
        yxPsiR_(&common->cudaProxy_),
        zxPsiL_(&common->cudaProxy_),
        zxPsiR_(&common->cudaProxy_),
        xyPsiL_(&common->cudaProxy_),
        xyPsiR_(&common->cudaProxy_),
        zyPsiL_(&common->cudaProxy_),
        zyPsiR_(&common->cudaProxy_),
        xzPsiL_(&common->cudaProxy_),
        xzPsiR_(&common->cudaProxy_),
        yzPsiL_(&common->cudaProxy_),
        yzPsiR_(&common->cudaProxy_),
        caX_(&common->cudaProxy_),
        caY_(&common->cudaProxy_),
        caZ_(&common->cudaProxy_),
        cbX_(&common->cudaProxy_),
        cbY_(&common->cudaProxy_),
        cbZ_(&common->cudaProxy_),
        cpmlBX_(&common->cudaProxy_),
        cpmlBY_(&common->cudaProxy_),
        cpmlBZ_(&common->cudaProxy_),
        cpmlCX_(&common->cudaProxy_),
        cpmlCY_(&common->cudaProxy_),
        cpmlCZ_(&common->cudaProxy_),
        common_(common),
        p_(p),
        arraySize_(0),
        arraySizePsi_(0),
        arraySizePsiLR_(0) {
}

// Initialise ready for a run
void CudaCpmlField::initialise() {
    // The FDTD cells
    arraySize_ = (size_t)p_->arraySize();
    x_.alloc(arraySize_);
    y_.alloc(arraySize_);
    z_.alloc(arraySize_);
    x_.set(0.0);
    y_.set(0.0);
    z_.set(0.0);
    // Create the psi arrays.  We need to work out how big to make them first
    psiWidth_ = std::max(std::max(p_->n().x(), p_->n().y()), p_->n().z());
    arraySizePsi_ = (size_t)psiWidth_ * (size_t)psiWidth_ * (size_t)p_->boundarySize() * 2;
    arraySizePsiLR_ = (size_t)psiWidth_ * (size_t)psiWidth_ * (size_t)p_->boundarySize();
    xyPsi_.alloc(arraySizePsi_);
    xzPsi_.alloc(arraySizePsi_);
    yzPsi_.alloc(arraySizePsi_);
    yxPsi_.alloc(arraySizePsi_);
    zxPsi_.alloc(arraySizePsi_);
    zyPsi_.alloc(arraySizePsi_);
    yxPsiL_.alloc(arraySizePsiLR_);
    yxPsiR_.alloc(arraySizePsiLR_);
    zxPsiL_.alloc(arraySizePsiLR_);
    zxPsiR_.alloc(arraySizePsiLR_);
    zyPsiL_.alloc(arraySizePsiLR_);
    zyPsiR_.alloc(arraySizePsiLR_);
    xyPsiL_.alloc(arraySizePsiLR_);
    xyPsiR_.alloc(arraySizePsiLR_);
    xzPsiL_.alloc(arraySizePsiLR_);
    xzPsiR_.alloc(arraySizePsiLR_);
    yzPsiL_.alloc(arraySizePsiLR_);
    yzPsiR_.alloc(arraySizePsiLR_);
    xyPsi_.set(0.0);
    xzPsi_.set(0.0);
    yzPsi_.set(0.0);
    yxPsi_.set(0.0);
    zxPsi_.set(0.0);
    zyPsi_.set(0.0);
    yxPsiL_.set(0.0);
    yxPsiR_.set(0.0);
    zxPsiL_.set(0.0);
    zxPsiR_.set(0.0);
    xyPsiL_.set(0.0);
    xyPsiR_.set(0.0);
    zyPsiL_.set(0.0);
    zyPsiR_.set(0.0);
    xzPsiL_.set(0.0);
    xzPsiR_.set(0.0);
    yzPsiL_.set(0.0);
    yzPsiR_.set(0.0);
    // The memory for the constants
    caX_.alloc(common_->matDepthArraySize_);
    caY_.alloc(common_->matDepthArraySize_);
    caZ_.alloc(common_->matDepthArraySize_);
    cbX_.alloc(common_->matDepthArraySize_);
    cbY_.alloc(common_->matDepthArraySize_);
    cbZ_.alloc(common_->matDepthArraySize_);
    cpmlBX_.alloc(common_->matDepthArraySize_);
    cpmlBY_.alloc(common_->matDepthArraySize_);
    cpmlBZ_.alloc(common_->matDepthArraySize_);
    cpmlCX_.alloc(common_->matDepthArraySize_);
    cpmlCY_.alloc(common_->matDepthArraySize_);
    cpmlCZ_.alloc(common_->matDepthArraySize_);
}

// Return the vector at a point
box::Vector<double> CudaCpmlField::value(const box::Vector<int>& cell) {
    int idx = p_->index(cell.x(),cell.y(),cell.z());
    return box::Vector<double>(x_.host()[idx], y_.host()[idx], z_.host()[idx]);
}

// Return the amount of memory used
size_t CudaCpmlField::memoryUse() {
    return 3 * arraySize_ * sizeof(double);
}

// Write the data to a file
bool CudaCpmlField::writeData(std::ofstream &file) {
    fdtd::BinaryFileHeader header("CUDACPMLFIELD", 1, arraySize_*sizeof(double)*3);
    file.write(header.data(), header.size());
    file.write((const char*)x_.host(), arraySize_*sizeof(double));
    file.write((const char*)y_.host(), arraySize_*sizeof(double));
    file.write((const char*)z_.host(), arraySize_*sizeof(double));
    return true;
}

// Read the data from a file
bool CudaCpmlField::readData(std::ifstream &file) {
    bool result = false;
    fdtd::BinaryFileHeader header{};
    file.read(header.data(), header.size());
    if(header.is("CUDACPMLFIELD")) {
        switch(header.version()) {
            case 1:
                arraySize_ = header.recordSize() / 3 / sizeof(double);
                file.read((char*)x_.host(), arraySize_*sizeof(double));
                file.read((char*)y_.host(), arraySize_*sizeof(double));
                file.read((char*)z_.host(), arraySize_*sizeof(double));
                // Deliberate fall through
            default:
                result = arraySize_ == p_->arraySize();
                break;
        }
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
int CudaCpmlField::wxPsiIndex(int x, int y, int z) const {
    int result = -1;
    // Which PML are we in?
    if(x >= 0 && x < p_->boundarySize()) {
        // The left hand PML
        result = y + (z * psiWidth_) + (x * psiWidth_ * psiWidth_);
    } else if(x >= p_->n().x()-p_->boundarySize() && x < p_->n().x()) {
        // The right hand PML
        result = y + (z * psiWidth_) + ((p_->n().x()-x-1+p_->boundarySize()) * psiWidth_ * psiWidth_);
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
int CudaCpmlField::wyPsiIndex(int x, int y, int z) const {
    int result = -1;
    // Which PML are we in?
    if(y >= 0 && y < p_->boundarySize()) {
        // The left hand PML
        result = x + (z * psiWidth_) + (y * psiWidth_ * psiWidth_);
    } else if(y >= p_->n().y()-p_->boundarySize() && y < p_->n().y()) {
        // The right hand PML
        result = x + (z * psiWidth_) + ((p_->n().y()-y-1+p_->boundarySize()) * psiWidth_ * psiWidth_);
    }
    return result;
}

// Return an index into a PSI array, returns -1 if the coordinate is not in the PML
int CudaCpmlField::wzPsiIndex(int x, int y, int z) const {
    int result = -1;
    // Which PML are we in?
    if(z >= 0 && z < p_->boundarySize()) {
        // The left hand PML
        result = x + (y * psiWidth_) + (z * psiWidth_ * psiWidth_);
    } else if(z >= p_->n().z()-p_->boundarySize() && z < p_->n().z()) {
        // The right hand PML
        result = x + (y * psiWidth_) + ((p_->n().z()-z-1+p_->boundarySize()) * psiWidth_ * psiWidth_);
    }
    return result;
}

// Return an index into the main arrays
int CudaCpmlField::index(int i, int j, int k) const {
    int idx = CUDAINDEX(i, j, k, p_->nx(), p_->ny());
    if(idx < 0 || idx >= p_->arraySize()) {
        idx = 0;
        std::cout << "Main arrays bound error" << std::endl;
    }
    return idx;
}

// Print the vector field contents
void CudaCpmlField::print(const char* title) {
    if(p_->n().x()<=10 && p_->n().y()<=10 && p_->n().z()<=10) {
        if(title != nullptr) {
            std::cout << title << std::endl;
        }
        std::ios oldState(nullptr);
        oldState.copyfmt(std::cout);
        std::cout << std::scientific << std::setprecision(0) << std::showpos;
        for(int j=0; j<p_->n().y(); j++) {
            for(int k=0; k<p_->n().z(); k++) {
                for( int i=0; i<p_->n().x(); i++) {
                    double v = x_.host()[p_->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(int j=0; j<p_->n().y(); j++) {
            for(int k=0; k<p_->n().z(); k++) {
                for( int i=0; i<p_->n().x(); i++) {
                    double v = y_.host()[p_->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        for(int j=0; j<p_->n().y(); j++) {
            for(int k=0; k<p_->n().z(); k++) {
                for( int i=0; i<p_->n().x(); i++) {
                    double v = z_.host()[p_->index(i,j,k)];
                    if(v == 0.0) {
                        std::cout << "------ ";
                    } else {
                        std::cout << v << " ";
                    }
                }
                std::cout << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "=======" << std::endl;
        std::cout.copyfmt(oldState);
    }
}
