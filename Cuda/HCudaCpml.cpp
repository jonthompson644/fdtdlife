/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "HCudaCpml.h"
#include "ECudaCpml.h"
#include "CudaIncludes.h"
#include "CudaCommon.h"
#include <Model.h>
#include <Domain.h>
#include <Material.h>
#include <CpmlMaterial.h>
#include <ThinSheetMaterial.h>
#include <Sources.h>
#include <algorithm>

// Constructor
HCudaCpml::HCudaCpml(fdtd::Model* m, CudaCommon* common) :
        HField(m),
        CudaCpmlField(m->p(), common),
        thinInRatio_(&common->cudaProxy_),
        thinOutRatio_(&common->cudaProxy_) {
}

// Initialise the model
void HCudaCpml::initialise() {
    HField::initialise();
    CudaCpmlField::initialise();
    // CUDA unified memory for the thin layer constants
    thinInRatio_.alloc(common_->matDepthArraySize_);
    thinOutRatio_.alloc(common_->matDepthArraySize_);
    // Now generate the constants
    initialiseConstantArrays();
    // And the cell ranges
    initialiseStartEnd();
}

// Print the data
void HCudaCpml::print() {
    CudaCpmlField::print("H Field");
}

// Return the amount of memory used
size_t HCudaCpml::memoryUse() {
    return CudaCpmlField::memoryUse();
}

// Return a value from the model
box::Vector<double> HCudaCpml::value(const box::Vector<int>& cell) {
    return CudaCpmlField::value(cell);
}

// Calculate the main model update
void HCudaCpml::stepField(const box::ThreadInfo* /*info*/) {
    int i, j, k;
    int psiIndex;
    auto* e = dynamic_cast<ECudaCpml*>(m_->e());
    if(e != nullptr) {
        // Step the grid
        for(k = start_.z(); k < end_.z(); k++) {
            for(j = start_.y(); j < end_.y(); j++) {
                for(i = start_.x(); i < end_.x(); i++) {
                    int idx = index(i, j, k);
                    int matIdxX = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthX_.host()[idx]);
                    int matIdxY = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthY_.host()[idx]);
                    int matIdxZ = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthZ_.host()[idx]);
                    box::Vector<double> dr; // Stretched coordinates
                    dr.x(common_->drX_.host()[matIdxX]);
                    dr.y(common_->drY_.host()[matIdxY]);
                    dr.z(common_->drZ_.host()[matIdxZ]);
                    double dexj = e->x_.host()[idx] - e->x_.host()[index(i, j - 1, k)];
                    double dexk = e->x_.host()[idx] - e->x_.host()[index(i, j, k - 1)];
                    double deyi = e->y_.host()[idx] - e->y_.host()[index(i - 1, j, k)];
                    double deyk = e->y_.host()[idx] - e->y_.host()[index(i, j, k - 1)];
                    double dezj = e->z_.host()[idx] - e->z_.host()[index(i, j - 1, k)];
                    double dezi = e->z_.host()[idx] - e->z_.host()[index(i - 1, j, k)];
                    double dezjin = e->thinExtraZ_.host()[idx] - e->thinExtraZ_.host()[index(i, j - 1, k)];
                    double deziin = e->thinExtraZ_.host()[idx] - e->thinExtraZ_.host()[index(i - 1, j, k)];
                    x_.host()[idx] =
                            (-(dezj * thinOutRatio_.host()[matIdxX] + dezjin * thinInRatio_.host()[matIdxX]) /
                            dr.y() + deyk / dr.z()) * caX_.host()[matIdxX] + x_.host()[idx] * cbX_.host()[matIdxX];
                    y_.host()[idx] =
                            (-dexk / dr.z() + (dezi * thinOutRatio_.host()[matIdxY] + deziin * thinInRatio_.host()[matIdxY]) /
                            dr.x()) * caY_.host()[matIdxY] + y_.host()[idx] * cbY_.host()[matIdxY];
                    z_.host()[idx] =
                            (-deyi / dr.x() + dexj / dr.y()) * caZ_.host()[matIdxZ] +
                            z_.host()[idx] * cbZ_.host()[matIdxZ];
                }
            }
        }
        // Now the CPML boundary modifications
        for(k = start_.z(); k < end_.z(); k++) {
            for(j = start_.y(); j < end_.y(); j++) {
                for(i = start_.x(); i < end_.x(); i++) {
                    int idx = index(i, j, k);
                    if(common_->material_.host()[idx] == fdtd::Parameters::cpmlMaterial) {
                        int matIdxX = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthX_.host()[idx]);
                        int matIdxY = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthY_.host()[idx]);
                        int matIdxZ = common_->matDepthIndex(common_->material_.host()[idx], common_->boundaryDepthZ_.host()[idx]);
                        double dexj = e->x_.host()[idx] - e->x_.host()[index(i, j - 1, k)];
                        double dexk = e->x_.host()[idx] - e->x_.host()[index(i, j, k - 1)];
                        double deyi = e->y_.host()[idx] - e->y_.host()[index(i - 1, j, k)];
                        double deyk = e->y_.host()[idx] - e->y_.host()[index(i, j, k - 1)];
                        double dezj = e->z_.host()[idx] - e->z_.host()[index(i, j - 1, k)];
                        double dezi = e->z_.host()[idx] - e->z_.host()[index(i - 1, j, k)];
                        psiIndex = wxPsiIndex(i, j, k);
                        if(psiIndex >= 0) {
                            yxPsi_.host()[psiIndex] = cpmlBX_.host()[matIdxX] * yxPsi_.host()[psiIndex] + cpmlCX_.host()[matIdxX] * dezi / m_->p()->dr().x();
                            y_.host()[idx] += yxPsi_.host()[psiIndex] * caY_.host()[matIdxY];
                            zxPsi_.host()[psiIndex] = cpmlBX_.host()[matIdxX] * zxPsi_.host()[psiIndex] + cpmlCX_.host()[matIdxX] * deyi / m_->p()->dr().x();
                            z_.host()[idx] -= zxPsi_.host()[psiIndex] * caZ_.host()[matIdxZ];
                        }
                        psiIndex = wyPsiIndex(i, j, k);
                        if(psiIndex >= 0) {
                            xyPsi_.host()[psiIndex] = cpmlBY_.host()[matIdxY] * xyPsi_.host()[psiIndex] + cpmlCY_.host()[matIdxY] * dezj / m_->p()->dr().y();
                            x_.host()[idx] -= xyPsi_.host()[psiIndex] * caX_.host()[matIdxX];
                            zyPsi_.host()[psiIndex] = cpmlBY_.host()[matIdxY] * zyPsi_.host()[psiIndex] + cpmlCY_.host()[matIdxY] * dexj / m_->p()->dr().y();
                            z_.host()[idx] += zyPsi_.host()[psiIndex] * caZ_.host()[matIdxZ];
                        }
                        psiIndex = wzPsiIndex(i, j, k);
                        if(psiIndex >= 0) {
                            xzPsi_.host()[psiIndex] = cpmlBZ_.host()[matIdxZ] * xzPsi_.host()[psiIndex] + cpmlCZ_.host()[matIdxZ] * deyk / m_->p()->dr().z();
                            x_.host()[idx] += xzPsi_.host()[psiIndex] * caX_.host()[matIdxX];
                            yzPsi_.host()[psiIndex] = cpmlBZ_.host()[matIdxZ] * yzPsi_.host()[psiIndex] + cpmlCZ_.host()[matIdxZ] * dexk / m_->p()->dr().z();
                            y_.host()[idx] -= yzPsi_.host()[psiIndex] * caY_.host()[matIdxY];
                        }
                    }
                }
            }
        }
    }
}

// Perform a single step on part of the model
// For shared memory multiprocessing we partition the model
// along the Z axis.
void HCudaCpml::step(const box::ThreadInfo* info) {
    // Step the grid
    stepField(info);
    // Now correct for any incident waves at the points on the
    // boundary between the total field and scattered field zones
    m_->s()->stepH(info, x_.host(), y_.host(), z_.host());
    // Handle any periodic boundary conditions
    doPeriodicBoundaries(info, x_.host(), y_.host(), z_.host());
    print();
}

// Initialise the arrays of constants
void HCudaCpml::initialiseConstantArrays() {
    fdtd::Parameters* p = m_->p();
    fdtd::Domain* d = m_->d();
    // Calculate the constants for each material
    for(auto pos : d->materialLookup()) {
        fdtd::Material* mat = pos.second;
        for(int boundaryDepth = -1; boundaryDepth <= p->boundarySize(); boundaryDepth++) {
            int idx = common_->matDepthIndex(mat->index(), boundaryDepth);
            caX_.host()[idx] = mat->cah(fdtd::Material::directionX, boundaryDepth);
            caY_.host()[idx] = mat->cah(fdtd::Material::directionY, boundaryDepth);
            caZ_.host()[idx] = mat->cah(fdtd::Material::directionZ, boundaryDepth);
            cbX_.host()[idx] = mat->cbh(fdtd::Material::directionX, boundaryDepth);
            cbY_.host()[idx] = mat->cbh(fdtd::Material::directionY, boundaryDepth);
            cbZ_.host()[idx] = mat->cbh(fdtd::Material::directionZ, boundaryDepth);
            auto thinMat = dynamic_cast<fdtd::ThinSheetMaterial*>(mat);
            if(m_->p()->useThinSheetSubcells() && thinMat != nullptr) {
                thinInRatio_.host()[idx] = thinMat->inRatio();
                thinOutRatio_.host()[idx] = thinMat->outRatio();
            } else {
                thinInRatio_.host()[idx] = 0.0;
                thinOutRatio_.host()[idx] = 1.0;
            }
            auto boundaryMat = dynamic_cast<fdtd::CpmlMaterial*>(mat);
            if(boundaryMat != nullptr) {
                cpmlBX_.host()[idx] = boundaryMat->b(fdtd::Material::directionX, boundaryDepth);
                cpmlCX_.host()[idx] = boundaryMat->c(fdtd::Material::directionX, boundaryDepth);
                cpmlBY_.host()[idx] = boundaryMat->b(fdtd::Material::directionY, boundaryDepth);
                cpmlCY_.host()[idx] = boundaryMat->c(fdtd::Material::directionY, boundaryDepth);
                cpmlBZ_.host()[idx] = boundaryMat->b(fdtd::Material::directionZ, boundaryDepth);
                cpmlCZ_.host()[idx] = boundaryMat->c(fdtd::Material::directionZ, boundaryDepth);
            } else {
                cpmlBX_.host()[idx] = 0.0;
                cpmlCX_.host()[idx] = 0.0;
                cpmlBY_.host()[idx] = 0.0;
                cpmlCY_.host()[idx] = 0.0;
                cpmlBZ_.host()[idx] = 0.0;
                cpmlCZ_.host()[idx] = 0.0;
            }
        }
    }
}

// Initialise the start and end cell coordinates
void HCudaCpml::initialiseStartEnd() {
    end_.x(m_->p()->n().x());
    end_.y(m_->p()->n().y());
    end_.z(m_->p()->n().z());
    // Only calculate the 0th entries on periodic axes
    start_.x(1);
    start_.y(1);
    start_.z(1);
    if(m_->p()->boundaryX() == fdtd::Parameters::Boundary::periodic) {
        start_.x(0);
    }
    if(m_->p()->boundaryY() == fdtd::Parameters::Boundary::periodic) {
        start_.y(0);
    }
    if(m_->p()->boundaryZ() == fdtd::Parameters::Boundary::periodic) {
        start_.z(0);
    }
}

