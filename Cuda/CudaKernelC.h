/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright notice,
  * this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  * this list of conditions and the following disclaimer in the documentation
  * and/or other materials provided with the distribution.
  *
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "Kernel.cuh"
#include "CudaIndexing.h"
#include <device_launch_parameters.h>

// Globals
static const int threadsPerBlock = 256;

// The GPU kernel function that will be executed in parallel
__global__ void kernelDoFdtdStep(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int nX, int nY, int nZ,
                                 double* ex, double* ey, double* ez,
                                 double* hx, double* hy, double* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 double* drX, double* drY, double* drZ,
                                 double* caX, double* cbX,
                                 double* caY, double* cbY,
                                 double* caZ, double* cbZ,
                                 double* thinExtraZ, double* caThin, double* cbThin) {
    int yDim = endY - startY;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / yDim + startZ;
    int j = tidx % yDim + startY;
    // If this is in range
    if(j < endY && k < endZ) {
        // Do the slice
        for(int i = startX; i < endX; i++) {
            int idx = CUDAINDEX(i, j, k, nX, nY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx], boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx], boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx], boundaryDepthZ[idx], boundarySize);
            double drx = drX[matIdxX];
            double dry = drY[matIdxY];
            double drz = drZ[matIdxZ];
            double dhxj = hx[CUDAINDEX(i, j + 1, k, nX, nY)] - hx[idx];
            double dhxk = hx[CUDAINDEX(i, j, k + 1, nX, nY)] - hx[idx];
            double dhyi = hy[CUDAINDEX(i + 1, j, k, nX, nY)] - hy[idx];
            double dhyk = hy[CUDAINDEX(i, j, k + 1, nX, nY)] - hy[idx];
            double dhzj = hz[CUDAINDEX(i, j + 1, k, nX, nY)] - hz[idx];
            double dhzi = hz[CUDAINDEX(i + 1, j, k, nX, nY)] - hz[idx];
            ex[idx] =
                    (dhzj / dry - dhyk / drz) * caX[matIdxX] +
                    ex[idx] * cbX[matIdxX];
            ey[idx] =
                    (dhxk / drz - dhzi / drx) * caY[matIdxY] +
                    ey[idx] * cbY[matIdxY];
            ez[idx] =
                    (dhyi / drx - dhxj / dry) * caZ[matIdxZ] +
                    ez[idx] * cbZ[matIdxZ];
            thinExtraZ[idx] =
                    (dhyi / drx - dhxj / dry) * caThin[matIdxZ] +
                    thinExtraZ[idx] * cbThin[matIdxZ];
        }
    }
}

// A function that invokes the kernel function
cudaError_t kernelFdtdStep(int startX, int startY, int startZ,
                           int endX, int endY, int endZ,
                           int nX, int nY, int nZ,
                           double* ex, double* ey, double* ez,
                           double* hx, double* hy, double* hz,
                           char* material, int boundarySize,
                           char* boundaryDepthX, char* boundaryDepthY,
                           char* boundaryDepthZ,
                           double* drX, double* drY, double* drZ,
                           double* caX, double* cbX,
                           double* caY, double* cbY,
                           double* caZ, double* cbZ,
                           double* thinExtraZ, double* caThin, double* cbThin) {
    int yDim = endY - startY;
    int zDim = endZ - startZ;
    int totalThreads = yDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoFdtdStep << < requiredBlocks, threadsPerBlock >> > (startX, startY, startZ,
            endX, endY, endZ,
            nX, nY, nZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            drX, drY, drZ,
            caX, cbX,
            caY, cbY,
            caZ, cbZ,
            thinExtraZ, caThin, cbThin);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the CPML x left correction
__global__ void kernelDoCpmlEFieldXLeft(int startX, int startY, int startZ,
                             int endX, int endY, int endZ,
                             int arraySizeX, int arraySizeY, int arraySizeZ,
                             double* ex, double* ey, double* ez,
                             double* hx, double* hy, double* hz,
                             char* material, int boundarySize,
                             char* boundaryDepthX, char* boundaryDepthY,
                             char* boundaryDepthZ,
                             double* yxPsiL, double* zxPsiL,
                             double* cpmlBX, double* cpmlCX,
                             double* caY, double* caZ,
                             int psiWidth, double dx) {
    int xDim = boundarySize - startX;
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / xDim + startZ;
    int i = tidx % xDim + startX;
    // If this is in range
    if(i < boundarySize && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            double dhyi = hy[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy[idx];
            double dhzi = hz[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI(i, j, k, psiWidth);
            yxPsiL[psiIndex] = cpmlBX[matIdxX] * yxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * dhzi / dx;
            ey[idx] -= yxPsiL[psiIndex] * caY[matIdxY];
            zxPsiL[psiIndex] = cpmlBX[matIdxX] * zxPsiL[psiIndex] +
                                      cpmlCX[matIdxX] * dhyi / dx;
            ez[idx] += zxPsiL[psiIndex] * caZ[matIdxZ];
        }
    }
}

// A function that invokes the kernel function
cudaError_t kernelCpmlEFieldXLeft(int startX, int startY, int startZ,
                                int endX, int endY, int endZ,
                                int arraySizeX, int arraySizeY, int arraySizeZ,
                                double* ex, double* ey, double* ez,
                                double* hx, double* hy, double* hz,
                                char* material, int boundarySize,
                                char* boundaryDepthX, char* boundaryDepthY,
                                char* boundaryDepthZ,
                                double* yxPsiL, double* zxPsiL,
                                double* cpmlBX, double* cpmlCX,
                                double* caY, double* caZ,
                                int psiWidth, double dx) {
    int xDim = boundarySize - startX;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldXLeft<<<requiredBlocks, threadsPerBlock>>>(
            startX, startY, startZ,
            endX, endY, endZ,
            arraySizeX, arraySizeY, arraySizeZ,
            ex, ey, ez,
            hx, hy, hz,
            material, boundarySize,
            boundaryDepthX, boundaryDepthY,
            boundaryDepthZ,
            yxPsiL, zxPsiL,
            cpmlBX, cpmlCX,
            caY, caZ,
            psiWidth, dx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

// A kernel function that calculates one slice of the X right CPML correction
__global__ void kernelDoCpmlEFieldXRight(int startX, int startY, int startZ,
                                 int endX, int endY, int endZ,
                                 int arraySizeX, int arraySizeY, int arraySizeZ,
                                 double* ex, double* ey, double* ez,
                                 double* hx, double* hy, double* hz,
                                 char* material, int boundarySize,
                                 char* boundaryDepthX, char* boundaryDepthY,
                                 char* boundaryDepthZ,
                                 double* yxPsiR, double* zxPsiR,
                                 double* cpmlBX, double* cpmlCX,
                                 double* caY, double* caZ,
                                 int psiWidth, double dx, int nx) {
    int xDim = endX - (nx - boundarySize);
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int k = tidx / xDim + startZ;
    int i = tidx % xDim + (nx - boundarySize);
    // If this is in range
    if(i < boundarySize && k < endZ) {
        // Calculate the slice of corrections
        for(int j = startY; j < endY; j++) {
            int idx = CUDAINDEX(i, j, k, arraySizeX, arraySizeY);
            int matIdxX = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthX[idx], boundarySize);
            int matIdxY = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthY[idx], boundarySize);
            int matIdxZ = CUDAINDEXMATDEPTH(material[idx],
                                            boundaryDepthZ[idx], boundarySize);
            double dhyi = hy[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hy[idx];
            double dhzi = hz[CUDAINDEX(i + 1, j, k, arraySizeX, arraySizeY)] - hz[idx];
            int psiIndex = CUDAINDEXPSI((nx - i - 1 + boundarySize), j, k, psiWidth);
            yxPsiR[psiIndex] = cpmlBX[matIdxX] * yxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * dhzi / dx;
            ey[idx] -= yxPsiR[psiIndex] * caY[matIdxY];
            zxPsiR[psiIndex] = cpmlBX[matIdxX] * zxPsiR[psiIndex] +
                                      cpmlCX[matIdxX] * dhyi / dx;
            ez[idx] += zxPsiR[psiIndex] * caZ[matIdxZ];
        }
    }
}

// Call the E Field CPML correction for the X right boundary
cudaError_t kernelCpmlEFieldXRight(int startX, int startY, int startZ,
                     int endX, int endY, int endZ,
                     int arraySizeX, int arraySizeY, int arraySizeZ,
                     double* ex, double* ey, double* ez,
                     double* hx, double* hy, double* hz,
                     char* material, int boundarySize,
                     char* boundaryDepthX, char* boundaryDepthY,
                     char* boundaryDepthZ,
                     double* yxPsiR, double* zxPsiR,
                     double* cpmlBX, double* cpmlCX,
                     double* caY, double* caZ,
                     int psiWidth, double dx, int nx) {
    int xDim = boundarySize - startX;
    int zDim = endZ - startZ;
    int totalThreads = xDim * zDim;
    int requiredBlocks = (totalThreads + threadsPerBlock - 1) / threadsPerBlock;
    kernelDoCpmlEFieldXRight<<<requiredBlocks, threadsPerBlock>>>(
        startX, startY, startZ,
        endX, endY, endZ,
        arraySizeX, arraySizeY, arraySizeZ,
        ex, ey, ez,
        hx, hy, hz,
        material, boundarySize,
        boundaryDepthX, boundaryDepthY,
        boundaryDepthZ,
        yxPsiR, zxPsiR,
        cpmlBX, cpmlCX,
        caY, caZ,
        psiWidth, dx, nx);
    cudaError_t error = cudaGetLastError();
    if(error == cudaSuccess) {
        error = cudaDeviceSynchronize();
    }
    return error;
}

