/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CUDACPMLFIELD_H
#define FDTDLIFE_CUDACPMLFIELD_H

#include <CpmlField.h>
#include <Box/Vector.h>
#include "CudaMemory.h"
class CudaCommon;
namespace fdtd {class Parameters;}

class CudaCpmlField {
public:
    // Construction
    CudaCpmlField(fdtd::Parameters* p, CudaCommon* common);

    // API
    void initialise();
    size_t memoryUse();
    box::Vector<double> value(const box::Vector<int>& cell);
    bool writeData(std::ofstream& file);
    bool readData(std::ifstream& file);
    int wxPsiIndex(int x, int y, int z) const;
    int wyPsiIndex(int x, int y, int z) const;
    int wzPsiIndex(int x, int y, int z) const;
    int index(int i, int j, int k) const;
    void print(const char* title);

public:
    // The FDTD cell memory, indexed by x,y,z
    CudaMemory<double> x_;
    CudaMemory<double> y_;
    CudaMemory<double> z_;

    // Memory indexed by the special CPML indices
    CudaMemory<double> xyPsi_;
    CudaMemory<double> xzPsi_;
    CudaMemory<double> yzPsi_;
    CudaMemory<double> yxPsi_;
    CudaMemory<double> zxPsi_;
    CudaMemory<double> zyPsi_;
    CudaMemory<double> yxPsiL_;
    CudaMemory<double> yxPsiR_;
    CudaMemory<double> zxPsiL_;
    CudaMemory<double> zxPsiR_;
    CudaMemory<double> xyPsiL_;
    CudaMemory<double> xyPsiR_;
    CudaMemory<double> zyPsiL_;
    CudaMemory<double> zyPsiR_;
    CudaMemory<double> xzPsiL_;
    CudaMemory<double> xzPsiR_;
    CudaMemory<double> yzPsiL_;
    CudaMemory<double> yzPsiR_;

    // Memory containing material constants, indexed by material id and boundary depth
    CudaMemory<double> caX_;   // The A constant for x (The average when containing a thin material)
    CudaMemory<double> caY_;   // The A constant for y (The average when containing a thin material)
    CudaMemory<double> caZ_;   // The A constant for z
    CudaMemory<double> cbX_;   // The B constant for x (The average when containing a thin material)
    CudaMemory<double> cbY_;   // The B constant for y (The average when containing a thin material)
    CudaMemory<double> cbZ_;   // The B constant for z
    CudaMemory<double> cpmlBX_;  // The CPML boundary B constant
    CudaMemory<double> cpmlBY_;  // The CPML boundary B constant
    CudaMemory<double> cpmlBZ_;  // The CPML boundary B constant
    CudaMemory<double> cpmlCX_;  // The CPML boundary C constant
    CudaMemory<double> cpmlCY_;  // The CPML boundary C constant
    CudaMemory<double> cpmlCZ_;  // The CPML boundary C constant

    // Other members
    fdtd::Parameters* p_;
    size_t arraySize_;
    size_t arraySizePsi_;
    size_t arraySizePsiLR_;
    int psiWidth_;
    box::Vector<int> start_;
    box::Vector<int> end_;
    CudaCommon* common_;   // Things common to both fields
};


#endif //FDTDLIFE_CUDACPMLFIELD_H
