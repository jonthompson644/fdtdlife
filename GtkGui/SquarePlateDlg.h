/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SQUAREPLATEDLG_H
#define FDTDLIFE_SQUAREPLATEDLG_H

#include "ShapesDlg.h"
#include <Domain/SquarePlate.h>
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"
#include "MaterialSelection.h"
#include "NamedTripleEntry.h"

// A dialog panel that shows square plate shape parameters
class SquarePlateDlg : public ShapesDlg::SubDlg {
public:
    // Constructor
    SquarePlateDlg() = default;
    void construct(ShapesDlg* owner, domain::Shape* shape) override;

    // API
    void populate() override;

protected:
    // Members
    domain::SquarePlate* shape_{};

    // The widgets
    MaterialSelection material_{"Material"};
    NamedToggleButton ignore_{"Ignore"};
    NamedTripleEntry<box::VectorExpression<double>> layerPosition_{
            "Layer Position x,y,z (m)"};
    NamedToggleButton square_{"Square Cell"};
    NamedEntry<box::Expression> cellSizeX_{"Cell Size X (m)"};
    NamedEntry<box::Expression> cellSizeY_{"Cell Size Y (m)"};
    NamedEntry<box::Expression> plateThickness_{"Plate Thickness (m)"};
    NamedTripleEntry<box::VectorExpression<double>> increment_{"Increment x,y,z (m)"};
    NamedTripleEntry<box::VectorExpression<size_t>> duplicate_{"Duplicate x,y,z"};
    NamedEntry<box::Expression> plateSize_{"Plate Size (%)"};
    MaterialSelection fillMaterial_{"Fill Material"};
    NamedEntry<box::Expression> repeatX_{"Repeat X"};
    NamedEntry<box::Expression> repeatY_{"Repeat Y"};
    NamedEntry<double> offsetX_{"Offset X (m)"};
    NamedEntry<double> offsetY_{"Offset Y (m)"};

    // Handlers
    virtual void onChange();
};

#endif //FDTDLIFE_SQUAREPLATEDLG_H
