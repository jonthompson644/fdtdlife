/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "FileDialog.h"

#include <utility>
#include <Box/Utility.h>

// Constructor
FileDialog::FileDialog(Gtk::Window* parentWindow, const std::string& name,
                       const std::string& suggested, const std::string& acceptButtonName,
                       Gtk::FileChooserAction action, TestSuite* testSuite,
                       std::string extension, const std::string& filterName) :
        FileChooserDialog(*parentWindow, name, action),
        testSuite_(testSuite),
        name_(name),
        extension_(std::move(extension)) {
    set_filename(suggested);
    add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    add_button(acceptButtonName, Gtk::RESPONSE_ACCEPT);
    auto filter1 = Gtk::FileFilter::create();
    filter1->set_name(filterName);
    filter1->add_pattern("*." + extension_);
    add_filter(filter1);
    auto filter2 = Gtk::FileFilter::create();
    filter2->set_name("All files");
    filter2->add_pattern("*.*");
    add_filter(filter2);
}

// Ask the question by running the modal dialog
bool FileDialog::ask(std::string& selected) {
    int result;
    std::string filename;
    if(testSuite_ != nullptr) {
        // Running under the test suite, ask it instead
        result = testSuite_->runFileModalDialog(name_, this, filename);
    } else {
        // Running normally, do the modal dialog
        result = run();
        filename = get_filename();
    }
    if(result == Gtk::RESPONSE_ACCEPT) {
        selected = filename;
        // If no extension has been supplied, tack on the default
        std::string extension = box::Utility::extension(selected);
        if(extension.empty()) {
            selected += "." + extension_;
        }
    }
    return result == Gtk::RESPONSE_ACCEPT;
}
