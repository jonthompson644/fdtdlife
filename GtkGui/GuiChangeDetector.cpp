/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include "GuiChangeDetector.h"

// Test template for named entries whose type matches the original member
template<class T>
T GuiChangeDetector::test(NamedEntry<T>& widget, const T& original, int why) {
    T value = widget.value();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}
// Instantiate particular types to make linking work
template
double GuiChangeDetector::test<double>(NamedEntry<double>&, const double&, int why);
template
size_t GuiChangeDetector::test<size_t>(NamedEntry<size_t>&, const size_t&, int why);
template
int GuiChangeDetector::test<int>(NamedEntry<int>&, const int&, int why);
template
std::string GuiChangeDetector::test<std::string>(NamedEntry<std::string>&, const std::string&, int why);

// Test for checkboxes and booleans
bool GuiChangeDetector::test(NamedCheckbox& widget, bool original, int why) {
    bool value = widget.value();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test for toggle buttons and booleans
bool GuiChangeDetector::test(NamedToggleButton& widget, bool original, int why) {
    bool value = widget.value();
    if(value != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return value;
}

// Test for vector expressions
box::VectorExpression<double> GuiChangeDetector::test(
        NamedTripleEntry<std::string>& widget,
        const box::VectorExpression<double>& original,
        int why) {
    box::VectorExpression<double> v(widget.value());
    if(v.x().text() != original.x().text() || v.y().text() != original.y().text()
       || v.z().text() != original.z().text()) {
        changeDetected_ = true;
        why_ = why;
    }
    return v;
}

// Test for vector expressions
box::VectorExpression<size_t> GuiChangeDetector::test(
        NamedTripleEntry<std::string>& widget,
        const box::VectorExpression<size_t>& original,
        int why) {
    box::VectorExpression<size_t> v(widget.value());
    if(v.x().text() != original.x().text() || v.y().text() != original.y().text()
       || v.z().text() != original.z().text()) {
        changeDetected_ = true;
        why_ = why;
    }
    return v;
}

// Test for vector doubles
box::Vector<double> GuiChangeDetector::test(
        NamedTripleEntry<double>& widget,
        const box::Vector<double>& original,
        int why) {
    box::Vector<double> v = widget.value();
    if(!box::Utility::equals(v.x(), original.x()) ||
       !box::Utility::equals(v.y(), original.y()) ||
       !box::Utility::equals(v.z(), original.z())) {
        changeDetected_ = true;
        why_ = why;
    }
    return v;
}

// Test for expressions
box::Expression GuiChangeDetector::test(
        NamedEntry<std::string>& widget,
        const box::Expression& original,
        int why) {
    std::string v = widget.value();
    if(v != original.text()) {
        changeDetected_ = true;
        why_ = why;
    }
    return box::Expression(v);
}

// Test for expressions
box::Expression GuiChangeDetector::test(
        NamedEntry<box::Expression>& widget,
        const box::Expression& original,
        int why) {
    box::Expression v = widget.value();
    if(v.text() != original.text()) {
        changeDetected_ = true;
        why_ = why;
    }
    return v;
}

// Test for a binary pattern
box::BinaryPattern GuiChangeDetector::test(NamedBinaryPattern& widget,
                                           const box::BinaryPattern& original, int why) {
    const box::BinaryPattern& v = widget.value();
    if(v != original) {
        changeDetected_ = true;
        why_ = why;
    }
    return v;
}
