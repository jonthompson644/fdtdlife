/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSANALYSERDLG_H
#define FDTDLIFE_LENSANALYSERDLG_H

#include "AnalysersDlg.h"
#include <Sensor/LensPhaseAnalyser.h>
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"
#include "NamedMenuButton.h"
#include "SensorSelection.h"
#include "NamedDataSeries.h"
#include "VerticalLayout.h"
#include "HorizontalLayout.h"

// Base class for all lens anaylser dialogs
class LensAnalyserDlg : public AnalysersDlg::SubDlg, public fdtd::Notifiable {
public:
    // Constructor
    explicit LensAnalyserDlg(std::string  dataUnits);
    void construct(AnalysersDlg* owner, sensor::Analyser* analyser) override;

    // Overrides of SubDlg
    void populate() override;

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected:
    // Members
    sensor::Analyser* analyser_{nullptr};
    std::string dataUnits_;

    // The widgets
    NamedEntry<box::Expression> frequency_{"Frequency (Hz)"};
    SensorSelection sensor_{"Sensor"};
    HorizontalLayout line1_{{frequency_, sensor_}};
    NamedSelection<sensor::Analyser::SensorLocation> location_{
            "Location", {"Smallest Z", "Largest Z"}};
    NamedSelection<sensor::Analyser::SensorComponent> component_{
            "Component", {"X", "Y"}};
    HorizontalLayout line2_{{location_, component_}};
    VerticalLayout configLayout_ {{line1_, line2_}};
    NamedEntry<double> error_{"Error", true};
    NamedDataSeries sensorData_;

    // Handlers
    virtual void onChange();

    // Helpers
    virtual void showData();
};

#endif //FDTDLIFE_LENSANALYSERDLG_H
