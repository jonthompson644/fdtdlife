/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SensorSelection.h"
#include <Sensor/Sensor.h>
#include "FdtdLife.h"

// Constructor
SensorSelection::SensorSelection(const std::string& name) :
        NamedSelection(name, {}) {
}

// Second stage constructor.  Connects the database to the widget.
void SensorSelection::construct(sensor::Sensors* sensors) {
    sensors_ = sensors;
    Notifiable::construct(sensors->m());
    signal_changed().connect(sigc::mem_fun(
            *this, &SensorSelection::onSelectionChange));
    updateSensors();
}

// Handle model change notifications
void SensorSelection::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) {
    // Base class first
    Notifiable::notify(source, why, data);
    // Update the sensors list
    if(why == FdtdLife::notifyDomainContentsChange) {
        updateSensors();
    }
}

// Update the sensors list
void SensorSelection::updateSensors() {
    InitialisingGui::Set s(initialising_);
    // Which sensorId is currently selected?
    int current = value();
    // Now re-fill the list
    remove_all();
    rowToId_.clear();
    int row = 0;
    // The no sensorId entry
    append("<< None >>");
    rowToId_[row++] = -1;
    for(auto& sensor : *sensors_) {
        append(sensor->name());
        rowToId_[row++] = sensor->identifier();
    }
    // Reselect the material
    value(current);
}

// Return the identifier of the selected sensorId
int SensorSelection::value() const {
    int result = -1;
    int row = get_active_row_number();
    auto pos = rowToId_.find(row);
    if(pos != rowToId_.end()) {
        result = pos->second;
    }
    return result;
}

// Select the specified sensorId
void SensorSelection::value(int id) {
    // Find the row with this id
    int row = 0;
    for(auto& item : rowToId_) {
        if(item.second == id) {
            row = item.first;
            break;
        }
    }
    // Select the row
    set_active(row);
}

// Connect the change handler
void SensorSelection::connectChangeHandler(const sigc::slot<void>& slot) {
    if(curConnection_.connected()) {
        curConnection_.disconnect();
    }
    curConnection_ = changedSignal_.connect(slot);
}

// Handler for a change notification from the combo box
void SensorSelection::onSelectionChange() {
    if(!initialising_) {
        changedSignal_.emit();
    }
}
