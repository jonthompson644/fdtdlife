/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FACTORYBUTTON_H
#define FDTDLIFE_FACTORYBUTTON_H

#include <gtkmm/menubutton.h>
#include <gtkmm/menu.h>
#include <Box/Factory.h>
#include <giomm.h>
#include "GuiElement.h"

// A wrapper for a menu button that selects and creates an object from a factory
template<class T, class M=int>
class FactoryButton : public Gtk::MenuButton, public GuiElement {
public:
    // Types
    enum class These {
        except, only
    };
    // Construction
    FactoryButton(const std::string& name, box::Factory<T,M>& factory,
                  const std::set<M>& modes = {}, These these = These::except) :
            factory_(factory) {
        // Initialise the button
        set_label(name);
        get_style_context()->add_class("namedbutton");
        auto menuModel = Gio::Menu::create();
        size_t test = (these == These::except ? 0 : 1);
        for(auto& item : factory_.modeNames()) {
            if(modes.count(item.first) == test) {
                menuModel->append(item.second);
            }
        }
        menu_.bind_model(menuModel, false);
        set_popup(menu_);
        // Connect the handler to the menu items
        for(auto& c : menu_.get_children()) {
            auto* menuItem = dynamic_cast<Gtk::MenuItem*>(c);
            if(menuItem != nullptr) {
                menuItem->signal_activate().connect(
                        sigc::bind<std::string>(
                                sigc::mem_fun(this, &FactoryButton::onMenuSelect),
                                menuItem->get_label()));
            }
        }
        // Register
        registerItem(name, *this);
    }
    FactoryButton() = delete;

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // Signals
    sigc::signal<void, M>& createSignal() { return createSignal_; };

    // Handlers
    void onMenuSelect(std::string buttonName) {
        M mode = factory_.nameToMode(buttonName);
        createSignal_.emit(mode);
    }

protected:
    // Members
    box::Factory<T,M>& factory_;
    Gtk::Menu menu_;
    sigc::signal<void, M> createSignal_;
};

#endif //FDTDLIFE_FACTORYBUTTON_H
