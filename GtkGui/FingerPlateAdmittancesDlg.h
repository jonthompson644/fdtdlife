/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FINGERPLATEADMITTANCESDLG_H
#define FDTDLIFE_FINGERPLATEADMITTANCESDLG_H

#include <gtkmm/dialog.h>
#include <Domain/FingerPlate.h>
#include "GuiElement.h"
#include "NamedListView.h"
#include "NamedButton.h"
#include "VerticalLayout.h"
#include "HorizontalLayout.h"
#include "InitialisingGui.h"


// A dialog that edits the admittance tables that can be associated with a super-capacitance shape
class FingerPlateAdmittancesDlg : public Gtk::Dialog, public GuiElement {
public:
    // Construction
    FingerPlateAdmittancesDlg(Gtk::Window& parentWindow, domain::FingerPlate* shape);
    FingerPlateAdmittancesDlg() = delete;

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; };

    // API
    void doModal();

protected:
    // Members
    domain::FingerPlate* shape_;
    InitialisingGui initialising_;

    // Table list view columns
    class TablesColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        TablesColumns() {
            add(colName_);
            add(colUnitCell_);
            add(colPatchRatio_);
            add(colMinFeatureSize_);
        }
        // Columns
        Gtk::TreeModelColumn<std::string> colName_;
        Gtk::TreeModelColumn<double> colUnitCell_;
        Gtk::TreeModelColumn<double> colPatchRatio_;
        Gtk::TreeModelColumn<double> colMinFeatureSize_;
    } tablesColumns_;

    // Widgets
    NamedListView tables_{"Admittance Tables"};
    NamedButton addButton_{"Add"};
    NamedButton deleteButton_{"Delete"};
    NamedButton editButton_{"Edit"};
    NamedButton pasteButton_{"Paste"};
    NamedButton copyButton_{"Copy"};
    NamedButton deleteAllButton_{"Delete All"};
    NamedButton fillDownButton_{"Fill Down"};
    VerticalLayout buttonsLayout_{{addButton_, deleteButton_, editButton_, pasteButton_, copyButton_,
                                   deleteAllButton_, fillDownButton_}};
    HorizontalLayout layout_{{tables_, buttonsLayout_}};

    // Handlers
    void onAdd();
    void onDelete();
    void onEdit();
    void onPaste();
    void onCopy();
    void onDeleteAll();
    void onFillDown();

    // Helpers
    void populate(domain::FingerPlate::ShapeAdmittance* sel = nullptr);
};


#endif //FDTDLIFE_FINGERPLATEADMITTANCESDLG_H
