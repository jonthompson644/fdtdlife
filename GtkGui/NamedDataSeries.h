/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDDATASERIES_H
#define FDTDLIFE_NAMEDDATASERIES_H

#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include "GuiElement.h"
#include "DataSeriesWidget.h"
#include "ContextMenu.h"
#include "Clipboard.h"

// A class that lays the data series widget with a name widget
class NamedDataSeries : public Gtk::VBox, public GuiElement {
public:
    // Construction
    explicit NamedDataSeries(const std::string& name);
    explicit NamedDataSeries(bool hasTitle = false);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // API
    void clipboard(Clipboard* c) { clipboard_ = c; }
    void clear() { dataSeries_.clear(); }
    void data(const std::string& name, const std::vector<double>& d, bool show=true) {
        dataSeries_.data(name, d, show);
    }
    void data(const std::string& name, const std::vector<std::complex<double>>& d,
            bool show=true) {
        dataSeries_.data(name, d, show);
    }
    void axisX(double min, double step, size_t n, const std::string& units) {
        dataSeries_.axisX(min, step, n, units);
    }
    void axisY(double min, double max, const std::string& units) {
        dataSeries_.axisY(min, max, units);
    }
    void axisY(const std::string& units) { dataSeries_.axisY(units); }
    void markers(size_t pos) { dataSeries_.markers(pos); };
    void markersOff() { dataSeries_.markersOff(); };
    double markerX() const { return dataSeries_.markerX(); }
    double markerY(const std::string& name) const { return dataSeries_.markerY(name); }
    ContextMenu& contextMenu() { return contextMenu_; }
    void title(const std::string& v);
    DataSeriesWidget& displayWidget() { return dataSeries_; }

protected:
    // Component widgets
    std::unique_ptr<Gtk::Label> nameLabel_;  // The name attached to the widget
    std::unique_ptr<Gtk::Label> titleLabel_;  // The title attached to the widget
    DataSeriesWidget dataSeries_;  // The data series
    ContextMenu contextMenu_;  // The right button click menu
    Clipboard* clipboard_{nullptr};  // The application clipboard object

    // Helpers
    void construct(const std::string& name);

    // Handlers
    void onContextMenu(GdkEventButton* event);
    void onCopyImageToClipboard();
    void onCopyCsvToClipboard();
};


#endif //FDTDLIFE_NAMEDDATASERIES_H
