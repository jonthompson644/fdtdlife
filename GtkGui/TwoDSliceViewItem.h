/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_TWODSLICEVIEWITEM_H
#define FDTDLIFE_TWODSLICEVIEWITEM_H

#include "ViewItem.h"
#include "TwoDSliceWidget.h"
#include "NamedTwoDSlice.h"

// A view of a 2D slice sensorId
class TwoDSliceViewItem : public ViewItem {
public:
    // Construction
    TwoDSliceViewItem() = default;
    void construct(int mode, int id, ViewFrame* frame) override;

    // Overrides of ViewItem
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void initialise() override;
    void update() override;

    // Getters
    TwoDSliceWidget::ColourMap colourMap() const { return colourMap_; }
    double displayMax() const { return displayMax_; }
    NamedTwoDSlice& dataDisplay() { return dataDisplay_; }

    // Setters
    void colourMap(TwoDSliceWidget::ColourMap v);
    void displayMax(double v);

protected:
    // Configuration
    TwoDSliceWidget::ColourMap colourMap_{TwoDSliceWidget::ColourMap::extKindlemann};
    double displayMax_{1.0};

    // Members
    NamedTwoDSlice dataDisplay_{true};
    size_t width_{};
    size_t height_{};
};


#endif //FDTDLIFE_TWODSLICEVIEWITEM_H
