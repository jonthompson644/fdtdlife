/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_RAYTRACERWIDGET_H
#define FDTDLIFE_RAYTRACERWIDGET_H

#include "CustomWidget.h"
#include "GuiElement.h"
#include "ContextMenu.h"
#include "Clipboard.h"
#include <Designer/LensRayTracer.h>

// A widget that displays a picture of the ray tracer information
class RayTracerWidget : public CustomWidget, public GuiElement {
public:
    // Construction
    explicit RayTracerWidget(const std::string& name = "RayTracer");

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // Overrides of CustomWidget
    void draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) override;
    bool on_button_press_event(GdkEventButton* button_event) override;

    // API
    void clipboard(Clipboard* c) { clipboard_ = c; }
    void setRayTracer(designer::LensRayTracer* rayTracer);

protected:
    // Members
    designer::LensRayTracer* rayTracer_{};
    double scaling_{};
    double xOffset_{};
    double zOffset_{};
    ContextMenu contextMenu_;  // The right button click menu
    Clipboard* clipboard_{nullptr};  // The application clipboard

    // Helpers
    static void drawBackground(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void scaling(int width, int height);
    void drawLens(const Cairo::RefPtr<Cairo::Context>& cr);
    void drawRays(const Cairo::RefPtr<Cairo::Context>& cr);

    // Handlers
    void onContextMenu(GdkEventButton* event);
    void onCopyImageToClipboard();
};


#endif //FDTDLIFE_RAYTRACERWIDGET_H
