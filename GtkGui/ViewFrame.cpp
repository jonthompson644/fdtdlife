/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ViewFrame.h"
#include "FdtdLife.h"
#include "TwoDSliceViewItem.h"
#include "MaterialViewItem.h"
#include "DataSeriesViewItem.h"
#include "PhaseShiftViewItem.h"
#include "AttenuationViewItem.h"
#include <Xml/Exception.h>
#include <Sensor/Sensor.h>

// Constructor
ViewFrame::ViewFrame() :
        viewUpdateWait_(sigc::mem_fun(this, &ViewFrame::updateWaitExpiry)) {
    // Initialise the view item factory
    itemFactory_.define(new box::Factory<ViewItem>::Creator<TwoDSliceViewItem>(
            sensor::DataSpec::modeTwoD, "2D Slice View"));
    itemFactory_.define(new box::Factory<ViewItem>::Creator<MaterialViewItem>(
            sensor::DataSpec::modeMaterial, "Material View"));
    itemFactory_.define(new box::Factory<ViewItem>::Creator<DataSeriesViewItem>(
            sensor::DataSpec::modeTimeSeries, "Data Series View"));
    itemFactory_.define(new box::Factory<ViewItem>::Creator<PhaseShiftViewItem>(
            sensor::DataSpec::modePhaseShift, "Phase Shift View"));
    itemFactory_.define(new box::Factory<ViewItem>::Creator<AttenuationViewItem>(
            sensor::DataSpec::modeAttenuation, "Attenuation View"));
    // Connect the widgets
    ViewFrame::initialiseGuiElements(true);
}

// Second stage construction used by factories
void ViewFrame::construct(int mode, int id, FdtdLife* m) {
    Notifiable::construct(m);
    mode_ = mode;
    identifier_ = id;
    m_ = m;
    set_title(name_);
    show_all();
}

// Add the GUI elements to the page
void ViewFrame::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    add(layout_);
    layout_.set_column_spacing(1);
    layout_.set_row_spacing(1);
    get_style_context()->add_class("viewframe");
}

// Write the configuration to the DOM object
void ViewFrame::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("name") << name_;
    root << xml::Obj("displayallframes") << displayAllFrames_;
    root << xml::Obj("framedisplayperiod") << frameDisplayPeriod_;
    root << xml::Obj("width") << frameWidth_;
    root << xml::Obj("height") << frameHeight_;
    for(auto& item : items_) {
        root << xml::Obj("item") << *item;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
// Note: Don't read mode_ and id_ as they may be different and
// have already been set by the factory
void ViewFrame::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("name") >> name_;
    root >> xml::Obj("displayallframes") >> xml::Default(true) >> displayAllFrames_;
    root >> xml::Obj("framedisplayperiod") >> xml::Default(0) >> frameDisplayPeriod_;
    root >> xml::Obj("width") >> xml::Default(frameWidth_) >> frameWidth_;
    root >> xml::Obj("height") >> xml::Default(frameHeight_) >> frameHeight_;
    for(auto& o : root.obj("item")) {
        ViewItem* viewItem = nullptr;
        try {
            items_.emplace_back(itemFactory_.restore(*o, this));
            viewItem = items_.back().get();
        } catch(xml::Exception&) {
            // Legacy formats
            int sensorId;
            std::string dataSpec;
            *o >> xml::Obj("sensorId") >> xml::Default(sensorId) >> sensorId;
            *o >> xml::Obj("data") >> xml::Default(dataSpec) >> dataSpec;
            *o >> xml::Attr("sensor") >> xml::Default(sensorId) >> sensorId;
            *o >> xml::Attr("sensorId") >> xml::Default(sensorId) >> sensorId;
            *o >> xml::Attr("data") >> xml::Default(dataSpec) >> dataSpec;
            viewItem = addViewItem(sensorId, dataSpec);
        }
        if(viewItem != nullptr)
        {
            *o >> *viewItem;
        }
    }
    root >> xml::Close();
    set_title(name_);
    resize(frameWidth_, frameHeight_);
    initialise();
}

// Set the name of the view
void ViewFrame::name(const std::string& name) {
    name_ = name;
    set_title(name_);
}

// Return the view item for the given sensorId data specification
ViewItem* ViewFrame::viewForSpec(int sensorId, const std::string& dataSpec) {
    ViewItem* result = nullptr;
    auto pos = std::find_if(items_.begin(), items_.end(),
                            [sensorId, dataSpec](std::unique_ptr<ViewItem>& item) {
                                return item->sensorId() == sensorId &&
                                       item->dataSpecName() == dataSpec;
                            });
    if(pos != items_.end()) {
        result = pos->get();
    }
    return result;
}

// Add a view item if one does not already exist
ViewItem* ViewFrame::addViewItem(int sensorId, const std::string& dataSpec) {
    ViewItem* item = viewForSpec(sensorId, dataSpec);
    if(item == nullptr) {
        sensor::DataSpec* spec = m_->sensors().getDataSpec(sensorId, dataSpec);
        if(spec != nullptr) {
            try {
                items_.emplace_back(itemFactory_.make(spec->mode(), this));
                items_.back()->sensorId(sensorId);
                items_.back()->dataSpecName(dataSpec);
                registerChild(*items_.back());
                initialise();
            } catch(box::Factory<ViewItem>::Exception& e) {
                std::cout << e.what() << std::endl;
            }
        }
    }
    return item;
}

// Remove a view item if one does exist
void ViewFrame::removeViewItem(int sensorId, const std::string& dataSpec) {
    items_.remove_if([sensorId, dataSpec](std::unique_ptr<ViewItem>& item) {
        return item->sensorId() == sensorId &&
               item->dataSpecName() == dataSpec;
    });
    initialise();
}

// Initialise the view items ready for a model run
void ViewFrame::initialise() {
    // Initialise the components
    for(auto& item : items_) {
        item->initialise();
    }
    // Remove items from container
    for(auto& item : items_) {
        if(item->widget().get_parent() != nullptr) {
            item->widget().get_parent()->remove(item->widget());
        }
    }
    // Count the items
    int numVertItems = 0;
    int numHorzItems = 0;
    for(auto& item : items_) {
        if(item->vertical()) {
            numVertItems++;
        } else {
            numHorzItems++;
        }
    }
    // Layout the vertical items
    int col = 0;
    for(auto& item : items_) {
        if(item->vertical()) {
            item->widget().set_hexpand(true);
            item->widget().set_vexpand(true);
            layout_.attach(item->widget(), col++, 0,
                           1, std::max(numHorzItems, 1));
        }
    }
    // Layout the horizontal items
    int row = 0;
    for(auto& item : items_) {
        if(!item->vertical()) {
            item->widget().set_hexpand(true);
            item->widget().set_vexpand(true);
            layout_.attach(item->widget(), numVertItems, row++,
                           std::max(numVertItems, 1), 1);
        }
    }
    // Initialise GUI elements
    show_all();
}

// The model data has changed and the may views need updating
void ViewFrame::updateViews() {
    if(displayAllFrames_) {
        updateViewsNow();
    } else if(viewUpdateWait_.running()) {
        // Not time for an update yet
        viewUpdateWaiting_ = true;
    } else {
        updateViewsNow();
        viewUpdateWait_.start(frameDisplayPeriod_);
    }
}

// The view update dead time has expired
void ViewFrame::updateWaitExpiry() {
    if(viewUpdateWaiting_) {
        updateViewsNow();
        viewUpdateWait_.start(frameDisplayPeriod_);
    }
}

// Update the views now
void ViewFrame::updateViewsNow() {
    // We need to make the sensors calculate
    m_->sensors().calculate();
    m_->analysers().calculate();
    // Update the view item data
    for(auto& item : items_) {
        item->update();
    }
    // State
    viewUpdateWaiting_ = false;
}

// Handle a notification
void ViewFrame::notify(fdtd::Notifier* /*source*/, int why, fdtd::NotificationData* /*data*/) {
    switch(why) {
        case fdtd::Model::notifyStepComplete:
            updateViews();
            break;
        case FdtdLife::notifyMinorNoViews:
        case FdtdLife::notifyAnalysersCalculate:
        case fdtd::Model::notifySteppedE:
        case fdtd::Model::notifySteppedH:
            break;
        default:
            updateViewsNow();
            break;
    }
}

// Handle size allocation indications
void ViewFrame::on_size_allocate(Gtk::Allocation& allocate) {
    // Base class first
    Window::on_size_allocate(allocate);
    // Record information
    get_size(frameWidth_, frameHeight_);
    m_->doNotification(FdtdLife::notifyMinorChange);
}
