/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SourcesDlg.h"
#include "PlaneWaveDlg.h"
#include "ZoneSourceDlg.h"
#include "GravyWaveDlg.h"
#include "OkCancelDialog.h"
#include <Source/SourceFactory.h>

// Constructor
SourcesDlg::SourcesDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Sources"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("SourcesDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<PlaneWaveDlg>(
            source::SourceFactory::modePlaneWave));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<ZoneSourceDlg>(
            source::SourceFactory::modeZone));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<GravyWaveDlg>(
            source::SourceFactory::modeGravyWave));
    // Initialise
    setButtons({newButton_, deleteButton_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(*this, &SourcesDlg::onNewSource));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &SourcesDlg::onDeleteSource));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &SourcesDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &SourcesDlg::onItemSelected));
}

// Fill the controls with their values
void SourcesDlg::populate() {
    fillSourceList(model_.sources().getSource(currentItem()));
}

// Fill the source list
void SourcesDlg::fillSourceList(source::Source* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    auto& sources = model_.sources();
    for(auto& source : sources.sources()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = source->identifier();
        row[columns_.name_] = source->name();
        row[columns_.mode_] = sources.factory()->modeName(source->mode());
        if(source == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new source
void SourcesDlg::onNewSource(int mode) {
    source::Source* source = model_.sources().factory()->make(mode, &model_);
    std::stringstream name;
    name << "Source " << source->identifier();
    source->name(name.str());
    fillSourceList(source);
    model_.doNotification(FdtdLife::notifyMinorChange);
}

// Delete the currently selected source
void SourcesDlg::onDeleteSource() {
    int id = currentItem();
    if(id >= 0) {
        auto& sources = model_.sources();
        auto* source = sources.getSource(id);
        auto* nextSource = sources.getSource(nextItem());
        if(nextSource == nullptr) {
            nextSource = sources.getSource(prevItem());
        }
        if(source != nullptr) {
            std::stringstream s;
            s << "Ok to delete source \"" << source->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Source", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                delete source;
                fillSourceList(nextSource);
            }
        }
    }
}

// The indicated item has been changed
void SourcesDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        source::Source* source = model_.sources().getSource(row[columns_.id_]);
        if(source != nullptr) {
            source->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void SourcesDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        source::Source* source = model_.sources().getSource((*row)[columns_.id_]);
        if(source != nullptr) {
            newDialog = dlgFactory_.make(this, source);
        }
    }
    setSubDialog(newDialog);
}