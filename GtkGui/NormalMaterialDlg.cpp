/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NormalMaterialDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void NormalMaterialDlg::construct(MaterialsDlg* owner, domain::Material* material) {
    SubDialog::construct(owner, material);
    comment("NormalMaterial");
    material_ = material;
    initLayout();
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &NormalMaterialDlg::onChange));
    relativeEpsilon_.connectChangeHandler(sigc::mem_fun(
            *this, &NormalMaterialDlg::onRelativeEpsilonChange));
    relativeMu_.connectChangeHandler(sigc::mem_fun(
            *this, &NormalMaterialDlg::onRelativeMuChange));
}

// Initialise the layout of the dialog.  Override
// this to amend the contents of the dialog
void NormalMaterialDlg::initLayout() {
    addLines({{colour_,  priority_},
              {epsilon_, relativeEpsilon_},
              {mu_,      relativeMu_},
              {sigma_,   sigmaStar_}});
}

// Populate all the widgets with values
void NormalMaterialDlg::populate() {
    MaterialsDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    material_->evaluate();
    colour_.value(material_->color());
    priority_.value(material_->priority());
    epsilon_.value(material_->epsilon());
    box::Expression::Context c;
    relativeEpsilon_.value(material_->epsilon().value() / box::Constants::epsilon0_);
    mu_.value(material_->mu());
    relativeMu_.value(material_->mu().value() / box::Constants::mu0_);
    sigma_.value(material_->sigma());
    sigmaStar_.value(material_->sigmastar());
}

// Default change handler
void NormalMaterialDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newColour = c.test(colour_, material_->color(),
                                FdtdLife::notifyMinorChange);
        auto newPriority = c.test(priority_, material_->priority(),
                                  FdtdLife::notifyMinorChange);
        auto newEpsilon = c.test(epsilon_, material_->epsilon(),
                                 FdtdLife::notifyMinorChange);
        auto newMu = c.test(mu_, material_->mu(),
                            FdtdLife::notifyMinorChange);
        auto newSigma = c.test(sigma_, material_->sigma(),
                               FdtdLife::notifyMinorChange);
        auto newSigmaStar = c.test(sigmaStar_, material_->sigmastar(),
                                   FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            material_->color(newColour);
            material_->priority(newPriority);
            material_->epsilon(newEpsilon);
            material_->mu(newMu);
            material_->sigma(newSigma);
            material_->sigmastar(newSigmaStar);
            // Update the relatives
            material_->evaluate();
            relativeEpsilon_.value(material_->epsilon().value() /
                                   box::Constants::epsilon0_);
            relativeMu_.value(material_->mu().value() / box::Constants::mu0_);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Handle a relative epsilon change
void NormalMaterialDlg::onRelativeEpsilonChange() {
    material_->epsilon(box::Expression(relativeEpsilon_.value() * box::Constants::epsilon0_));
    epsilon_.value(material_->epsilon().text());
    dlg_->model().doNotification(FdtdLife::notifyMinorChange);
}

// Handle a relative mu change
void NormalMaterialDlg::onRelativeMuChange() {
    material_->mu(box::Expression(relativeMu_.value() * box::Constants::mu0_));
    mu_.value(material_->mu().text());
    dlg_->model().doNotification(FdtdLife::notifyMinorChange);
}
