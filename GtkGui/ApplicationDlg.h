/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_APPLICATIONDLG_H
#define FDTDLIFE_APPLICATIONDLG_H


#include <gtkmm/applicationwindow.h>
#include <gtkmm/builder.h>
#include <gtkmm/notebook.h>
#include <gtkmm/hvbox.h>
#include "ConfigurationDlg.h"
#include "FdtdLife.h"
#include "NotebookLayout.h"
#include "ToolbarLayout.h"
#include "SourcesDlg.h"
#include "MaterialsDlg.h"
#include "ShapesDlg.h"
#include "SensorsDlg.h"
#include "AnalysersDlg.h"
#include "ViewsDlg.h"
#include "SequencersDlg.h"
#include "VariablesDlg.h"
#include "DesignersDlg.h"
#include "NamedToolButton.h"
#include "VerticalLayout.h"
#include "NamedToolMenuButton.h"
#include "ParametersDlg.h"
#include <Fdtd/Notifiable.h>
#include <gtkmm/recentmanager.h>

class ApplicationDlg
        : public Gtk::ApplicationWindow, public GuiElement, public fdtd::Notifiable {
public:
    // Types
    enum class RunState {stopped, paused, running, animating};

    // Construction
    explicit ApplicationDlg(FdtdLife& model);

    // Accessors
    ConfigurationDlg& configurationPage() { return configurationPage_; }
    SourcesDlg& sourcesPage() { return sourcesPage_; }
    MaterialsDlg& materialsPage() { return materialsPage_; }
    ShapesDlg& shapesPage() { return shapesPage_; }
    SensorsDlg& sensorsPage() { return sensorsPage_; }
    AnalysersDlg& analysersPage() { return analysersPage_; }
    ViewsDlg& viewsPage() { return viewsPage_; }
    SequencersDlg& sequencersPage() { return sequencersPage_; }
    VariablesDlg& variablesPage() { return variablesPage_; }
    ParametersDlg& parametersPage() { return parametersPage_; }
    DesignersDlg& designersPage() { return designersPage_; }
    NotebookLayout& sections() { return sections_; }

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // Overrides of ApplicationWindow
    bool on_delete_event(GdkEventAny* event) override;

    // Getters
    bool modified() const { return modified_; }
    bool validFile() const { return validFile_; }

    // API
    bool isStopped() const { return runState_ == RunState::paused || runState_ == RunState::stopped; }
    void setStopped();

protected:
    // Handlers
    void onToolInitialise();
    void onToolFileNew();
    void onToolFileOpen();
    void onToolFileOpenRecent(std::string fileName);
    void onToolFileSave();
    void onToolFileSaveAs();
    void onToolRun();
    void onToolStop();
    void onTestContinue();
    bool onIdle();

    // Helpers
    void populate();
    bool maybeSave(const std::string& title);
    bool fileSave(const std::string& title);
    void doSave();
    void doOpen();
    void setTitle();
    void setRecentFiles();

protected:
    // Members
    FdtdLife& model_;
    bool modified_{false};
    std::string currentFileName_{"Untitled"};
    bool validFile_{false}; // Indicates that the file is valid and we can save without asking
    RunState runState_{RunState::stopped};  // Whether the model is running or not
    sigc::connection runConnection_;  // Connection object for the run handler
    Glib::RefPtr<Gtk::RecentManager> recentFiles_;  // The recent files list
    // Widgets
    NamedToolButton fileNew_{"New"};  // New file
    NamedToolMenuButton<std::string> fileOpen_{"Open", {}}; // Open existing file
    NamedToolButton fileSave_{"Save"};  // Save the file
    NamedToolButton fileSaveAs_{"Save As"};  // Save the file
    NamedToolButton initialise_{"Initialise"};  // Initialise the model
    NamedToolButton run_{"Run"}; // Run the model
    NamedToolButton stop_{"Stop"};  // Stop the model
    ToolbarLayout tools_{{fileNew_, fileOpen_, fileSave_, fileSaveAs_,
                                 initialise_, run_, stop_}};
    ConfigurationDlg configurationPage_;  // The configuration page
    SourcesDlg sourcesPage_; // The sources page
    MaterialsDlg materialsPage_; // The sources page
    ShapesDlg shapesPage_; // The shapes page
    SensorsDlg sensorsPage_; // The sensors page
    AnalysersDlg analysersPage_; // The analysers page
    ViewsDlg viewsPage_; // The views page
    SequencersDlg sequencersPage_; // The sequencers page
    VariablesDlg variablesPage_; // The variables page
    ParametersDlg parametersPage_; // The dielectric parameters page
    DesignersDlg designersPage_; // The designers page
    NotebookLayout sections_{{{"Configuration", configurationPage_},
                                     {"Sources", sourcesPage_},
                                     {"Materials", materialsPage_},
                                     {"Shapes", shapesPage_},
                                     {"Sensors", sensorsPage_},
                                     {"Analysers", analysersPage_},
                                     {"Views", viewsPage_},
                                     {"Sequencers", sequencersPage_},
                                     {"Variables", variablesPage_},
                                     {"Parameters", parametersPage_},
                                     {"Designers", designersPage_}}};
    VerticalLayout layout_{{tools_, sections_}};
    // Extra widgets in test mode
    std::unique_ptr<Gtk::ToolButton> testContinue_;  // Continue test
};


#endif //FDTDLIFE_APPLICATIONDLG_H
