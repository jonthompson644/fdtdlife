/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FdtdLife.h"

// Constructor
FdtdLife::FdtdLife(TestSuite* testSuite) :
        testSuite_(testSuite) {
    viewFactory_.define(new box::Factory<ViewFrame>::Creator<ViewFrame>(
            viewModeNormal, "View"));
}

// Write GUI configuration to the DOM
void FdtdLife::writeConfig(xml::DomObject& root) const {
    // Base class first
    Model::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("parameterscfg") << xml::Open()
         << xml::Obj("whitebackground") << whiteBackground_
         << xml::Close();
    for(auto& view : views_) {
        root << xml::Obj("view") << *view;
    }
    root << xml::Close();
}

// Read GUI configuration from the DOM
void FdtdLife::readConfig(xml::DomObject& root) {
    // Base class first
    Model::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    if(!root.obj("parameterscfg").empty()) {
        root >> xml::Obj("parameterscfg") >> xml::Open()
             >> xml::Obj("whitebackground") >> xml::Default(whiteBackground_)
             >> whiteBackground_ >> xml::Close();
    }
    for(auto& o : root.obj("view")) {
        auto view = restoreView(*o);
        *o >> *view;
    }
    root >> xml::Close();
}

// Make a view frame from a DOM object
ViewFrame* FdtdLife::restoreView(xml::DomObject& o) {
    views_.emplace_back(viewFactory_.restore(
            FdtdLife::viewModeNormal, o, this));
    return views_.back().get();
}

// Make an analyser given a mode
ViewFrame* FdtdLife::makeView(int mode) {
    views_.emplace_back(viewFactory_.make(mode, this));
    return views_.back().get();
}

// Return the designer with the given identifier
ViewFrame* FdtdLife::findView(int identifier) {
    ViewFrame* result = nullptr;
    for(auto& view : views_) {
        if(view->identifier() == identifier) {
            result = view.get();
            break;
        }
    }
    return result;
}

// Remove an analyser from the list.
void FdtdLife::removeView(ViewFrame* view) {
    views_.remove_if([view](std::unique_ptr<ViewFrame>& v) {
        return v.get() == view;
    });
}

// Initialise the system ready for a run
void FdtdLife::initialise() {
    // Base class first
    Model::initialise();
    // Now the views
    for(auto& view : views_) {
        view->initialise();
    }
}

// Clear the contents of the model
void FdtdLife::clear() {
    // Base class first
    Model::clear();
    // Now my stuff
    views_.clear();
}
