/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERPHASEDLG_H
#define FDTDLIFE_LENSDESIGNERPHASEDLG_H

#include "GridLayout.h"
#include "InitialisingGui.h"
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedButton.h"
#include "NamedToggleButton.h"
#include "HorizontalLayout.h"
#include <Designer/LensDesigner.h>

class FdtdLife;
class LensDesignerDlg;

// The phase sub-panel of the lens designer dialog.
class LensDesignerPhaseDlg : public GridLayout {
public:
    // Construction
    LensDesignerPhaseDlg() = default;
    void construct(FdtdLife* model, LensDesignerDlg* dlg);

    // API
    void populate();

protected:
    // Handlers
    void onChange();
    void onGenerate();

    // Members
    FdtdLife* model_{};
    LensDesignerDlg* dlg_{};
    InitialisingGui initialising_;

    // Widgets
    NamedEntry<double> permittivityAbove_{"Rel Permittivity Above"};
    NamedEntry<double> permittivityInside_{"Rel Permittivity Inside"};
    NamedEntry<double> permittivityBelow_{"Rel Permittivity Below"};
    NamedEntry<double> focalLength_{"Focal Length (m)"};
    NamedEntry<double> layerSpacing_{"Layer Spacing (m)"};
    NamedEntry<double> centreFrequency_{"Centre Frequency (Hz)"};
    NamedEntry<double> unitCell_{"Unit Cell (m)"};
    NamedEntry<double> lensDiameter_{"Lens Diameter (m)"};
    NamedEntry<size_t> totalNumberLayers_{"Total Number Of Layers"};
    NamedEntry<size_t> numberArStages_{"Number Of AR Stages"};
    NamedEntry<size_t> layersPerArStage_{"Layers Per AR Stage"};
    NamedEntry<double> edgeDistance_{"Edge Distance (m)"};
    NamedSelection<designer::LensDesigner::Modelling> modelling_{
        "Modelling", {"Metamaterial Cells", "Dielectric Blocks",
                      "", "Finger Patterns"}};
    NamedEntry<size_t> fdtdCellsPerUnitCell_{"FDTD Cells Per Unit Cell"};
    NamedEntry<double> focalLengthFudgeFactor_{"Focal Length Fudge Factor"};
    NamedEntry<double> plateThickness_{"Plate Thickness (m)"};
    NamedEntry<size_t> sliceThickness_{"Slice Thickness (unit cells)"};
    NamedEntry<size_t> bitsPerHalfSide_{"Bits Per Half Side"};
    NamedEntry<double> minFeatureSize_{"Min Feature Size (m)"};
    NamedEntry<double> fingerOffset_{"Finger Offset (m)"};
    NamedSelection<designer::LensDesigner::ColumnType> columnType_{
        "Curved Columns", {"Straight", "Curved", "Curved Const G"}};
    NamedEntry<std::string> errors_{"Errors", true, 3};
    NamedButton generate_{"Generate"};
    HorizontalLayout buttons_{{generate_}, 4};
};


#endif //FDTDLIFE_LENSDESIGNERPHASEDLG_H
