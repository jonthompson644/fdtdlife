/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FingerPlateAdmittancesDlg.h"
#include "FingerPlateDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void FingerPlateDlg::construct(ShapesDlg* owner, domain::Shape* shape) {
    SubDialog::construct(owner, shape);
    material_.construct(shape->m()->d());
    fillMaterial_.construct(shape->m()->d());
    comment("FingerPlate");
    shape_ = dynamic_cast<domain::FingerPlate*>(shape);
    addLines({{material_,       ignore_},
              {layerPosition_},
              {square_,plateThickness_},
              {cellSizeX_, cellSizeY_},
              {increment_},
              {duplicate_},
              {plateSize_,      fillMaterial_},
              {left_,           right_},
              {top_,            bottom_},
              {minFeatureSize_, fingerOffset_},
              {admittanceTables_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &FingerPlateDlg::onChange));
    admittanceTables_.signal_clicked().connect(
            sigc::mem_fun(this, &FingerPlateDlg::onAdmittanceTables));
}

// Populate all the widgets with values
void FingerPlateDlg::populate() {
    ShapesDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    material_.value(shape_->material());
    ignore_.value(shape_->ignore());
    layerPosition_.value(shape_->center());
    square_.value(shape_->square());
    cellSizeX_.value(shape_->cellSizeX());
    cellSizeY_.value(shape_->cellSizeY());
    plateThickness_.value(shape_->layerThickness());
    increment_.value(shape_->increment());
    duplicate_.value(shape_->duplicate());
    plateSize_.value(shape_->plateSize());
    fillMaterial_.value(shape_->fillMaterial());
    left_.value(shape_->fingersLeft());
    right_.value(shape_->fingersRight());
    top_.value(shape_->fingersTop());
    bottom_.value(shape_->fingersBottom());
    minFeatureSize_.value(shape_->minFeatureSize());
    fingerOffset_.value(shape_->fingerOffset());
}

// Default change handler
void FingerPlateDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newMaterial = c.test(material_, shape_->material(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newIgnore = c.test(ignore_, shape_->ignore(),
                                FdtdLife::notifyDomainContentsChange);
        auto newLayerPosition = c.test(layerPosition_, shape_->center(),
                                       FdtdLife::notifyDomainContentsChange);
        auto newCellSizeX = c.test(cellSizeX_, shape_->cellSizeX(),
                                   FdtdLife::notifyMinorChange);
        auto newCellSizeY = c.test(cellSizeY_, shape_->cellSizeY(),
                                   FdtdLife::notifyMinorChange);
        auto newSquare = c.test(square_, shape_->square(),
                                FdtdLife::notifyMinorChange);
        auto newLayerThickness = c.test(plateThickness_, shape_->layerThickness(),
                                        FdtdLife::notifyDomainContentsChange);
        auto newIncrement = c.test(increment_, shape_->increment(),
                                   FdtdLife::notifyDomainContentsChange);
        auto newDuplicate = c.test(duplicate_, shape_->duplicate(),
                                   FdtdLife::notifyDomainContentsChange);
        auto newPlateSize = c.test(plateSize_, shape_->plateSize(),
                                   FdtdLife::notifyDomainContentsChange);
        auto newFillMaterial = c.test(fillMaterial_, shape_->fillMaterial(),
                                      FdtdLife::notifyDomainContentsChange);
        auto newLeft = c.test(left_, shape_->fingersLeft(),
                              FdtdLife::notifyDomainContentsChange);
        auto newRight = c.test(right_, shape_->fingersRight(),
                               FdtdLife::notifyDomainContentsChange);
        auto newTop = c.test(top_, shape_->fingersTop(),
                             FdtdLife::notifyDomainContentsChange);
        auto newBottom = c.test(bottom_, shape_->fingersBottom(),
                                FdtdLife::notifyDomainContentsChange);
        auto newMinFeatureSize = c.test(minFeatureSize_, shape_->minFeatureSize(),
                                        FdtdLife::notifyDomainContentsChange);
        auto newFingerOffset = c.test(fingerOffset_, shape_->fingerOffset(),
                                        FdtdLife::notifyDomainContentsChange);
        if(c.changeDetected()) {
            // Write any changes
            shape_->material(newMaterial);
            shape_->ignore(newIgnore);
            shape_->center(newLayerPosition);
            shape_->cellSizeX(newCellSizeX);
            shape_->cellSizeY(newCellSizeY);
            shape_->square(newSquare);
            shape_->layerThickness(newLayerThickness);
            shape_->increment(newIncrement);
            shape_->duplicate(newDuplicate);
            shape_->plateSize(newPlateSize);
            shape_->fillMaterial(newFillMaterial);
            shape_->fingersLeft(newLeft);
            shape_->fingersRight(newRight);
            shape_->fingersTop(newTop);
            shape_->fingersBottom(newBottom);
            shape_->minFeatureSize(newMinFeatureSize);
            shape_->fingerOffset(newFingerOffset);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Handler for the admittance tables button
void FingerPlateDlg::onAdmittanceTables() {
    FingerPlateAdmittancesDlg box(*dlg_->parentWindow(), shape_);
    box.doModal();
}
