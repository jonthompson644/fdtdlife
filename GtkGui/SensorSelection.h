/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SENSORSELECTION_H
#define FDTDLIFE_SENSORSELECTION_H

#include <Sensor/Sensors.h>
#include "NamedSelection.h"
#include <Fdtd/Notifiable.h>
#include "InitialisingGui.h"

// A class that uses a named selection as a materials selector
class SensorSelection : public NamedSelection<int>, public fdtd::Notifiable {
public:
    // Construction
    explicit SensorSelection(const std::string& name);
    void construct(sensor::Sensors* sensors);

    // API
    void updateSensors();
    int value() const override;
    void value(int id) override;

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // Overrides of NamedSelection
    void connectChangeHandler(const sigc::slot<void>& slot) override;

protected:
    // Members
    sensor::Sensors* sensors_;
    std::map<int, int> rowToId_;  // Maps a row number to a material identifier
    sigc::signal<void> changedSignal_;
    InitialisingGui initialising_;

    // Selection change handler
    void onSelectionChange();
};

#endif //FDTDLIFE_SENSORSELECTION_H
