/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VIEWFRAME_H
#define FDTDLIFE_VIEWFRAME_H

#include <Box/Expression.h>
#include <gtkmm/window.h>
#include "ViewItem.h"
#include <Box/Factory.h>
#include <gtkmm/grid.h>
#include "GuiElement.h"
#include "Timer.h"
#include <Fdtd/Notifiable.h>

class FdtdLife;
namespace xml { class DomObject; }

// A data view
class ViewFrame : public Gtk::Window, public GuiElement, public fdtd::Notifiable {
public:
    // Construction
    ViewFrame();
    virtual void construct(int mode, int id, FdtdLife* m);

    // API
    virtual void initialise();
    virtual void writeConfig(xml::DomObject& root) const;
    virtual void readConfig(xml::DomObject& root);
    virtual void evaluate(box::Expression::Context& /*c*/) {}
    friend xml::DomObject& operator<<(xml::DomObject& o, const ViewFrame& s) {
        s.writeConfig(o);
        return o;
    }
    friend xml::DomObject& operator>>(xml::DomObject& o, ViewFrame& s) {
        s.readConfig(o);
        return o;
    }
    FdtdLife* m() { return m_; }
    ViewItem* viewForSpec(int sensorId, const std::string& dataSpec);
    ViewItem* addViewItem(int sensorId, const std::string& dataSpec);
    void removeViewItem(int sensorId, const std::string& dataSpec);

    // Overrides of GuiElement
    void initialiseGuiElements(bool recursive) override;
    Gtk::Widget& widget() override { return *this; }

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // Overrides of Gtk:Window
    void on_size_allocate(Gtk::Allocation& allocate) override;

    // Getters
    [[nodiscard]] int mode() const { return mode_; }
    [[nodiscard]] int identifier() const { return identifier_; }
    [[nodiscard]] const std::string& name() const { return name_; }
    [[nodiscard]] int frameDisplayPeriod() const { return frameDisplayPeriod_; }
    [[nodiscard]] bool displayAllFrames() const { return displayAllFrames_; }
    Gtk::Grid& layout() { return layout_; }

    // Setters
    void name(const std::string& v);
    void frameDisplayPeriod(int v) { frameDisplayPeriod_ = v; }
    void displayAllFrames(bool v) { displayAllFrames_ = v; }

protected:
    // Configuration
    int mode_{-1};  // The type of the analyser
    int identifier_{-1};  // The identifier of the analyser
    std::string name_;  // The name of the analyser
    int frameDisplayPeriod_{2000};  // Display frames no faster than this (ms)
    bool displayAllFrames_{false};  // Display all frames
    int frameWidth_{200};  // Width of the view frame
    int frameHeight_{200};  // Height of the view frame
    std::list<std::unique_ptr<ViewItem>> items_;  // The items that make the view

    // Other members
    FdtdLife* m_{};  // The model
    box::Factory<ViewItem> itemFactory_;
    Gtk::Grid layout_;  // The laid out items
    Timer viewUpdateWait_;  // The time to wait before updating the view again
    bool viewUpdateWaiting_{false};

    // Helpers
    void updateViews();
    void updateViewsNow();

    // Handlers
    void updateWaitExpiry();
};


#endif //FDTDLIFE_VIEWFRAME_H
