/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SensorsDlg.h"
#include "OkCancelDialog.h"
#include "ArraySensorDlg.h"
#include "TwoDSliceDlg.h"
#include "MaterialSensorDlg.h"
#include <Sensor/Sensors.h>

// Constructor
SensorsDlg::SensorsDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Sensors"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("SensorsDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<ArraySensorDlg>(
            sensor::Sensors::modeArraySensor));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<TwoDSliceDlg>(
            sensor::Sensors::modeTwoD));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<MaterialSensorDlg>(
            sensor::Sensors::modeMaterial));
    // Initialise
    setButtons({newButton_, deleteButton_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(*this, &SensorsDlg::onNewSensor));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &SensorsDlg::onDeleteSensor));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &SensorsDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &SensorsDlg::onItemSelected));
}

// Fill the controls with their values
void SensorsDlg::populate() {
    fillSensorList(model_.sensors().find(currentItem()));
}

// Fill the source list
void SensorsDlg::fillSensorList(sensor::Sensor* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    for(auto& sensor : model_.sensors()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = sensor->identifier();
        row[columns_.name_] = sensor->name();
        row[columns_.mode_] = model_.sensors().factory().modeName(sensor->mode());
        if(sensor.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new shape
void SensorsDlg::onNewSensor(int mode) {
    sensor::Sensor* sensor = model_.sensors().makeSensor(mode);
    std::stringstream name;
    name << "Sensor " << sensor->identifier();
    sensor->name(name.str());
    fillSensorList(sensor);
    model_.doNotification(FdtdLife::notifyDomainContentsChange);
}

// Delete the currently selected sensorId
void SensorsDlg::onDeleteSensor() {
    int id = currentItem();
    if(id >= 0) {
        auto* sensor = model_.sensors().find(id);
        auto* nextSensor = model_.sensors().find(nextItem());
        if(nextSensor == nullptr) {
            nextSensor = model_.sensors().find(prevItem());
        }
        if(sensor != nullptr) {
            std::stringstream s;
            s << "Ok to delete sensorId \"" << sensor->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Sensor", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.sensors().remove(sensor);
                fillSensorList(nextSensor);
            }
        }
    }
}

// The indicated item has been changed
void SensorsDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        sensor::Sensor* sensor = model_.sensors().find(row[columns_.id_]);
        if(sensor != nullptr) {
            sensor->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void SensorsDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        sensor::Sensor* sensor = model_.sensors().find((*row)[columns_.id_]);
        if(sensor != nullptr) {
            newDialog = dlgFactory_.make(this, sensor);
        }
    }
    setSubDialog(newDialog);
}
