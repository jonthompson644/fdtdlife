/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FDTDLIFE_H
#define FDTDLIFE_FDTDLIFE_H

#include <Fdtd/Model.h>
#include "TestSuite.h"
#include "Clipboard.h"
#include "ViewFrame.h"

// The extended model class for the GTK application
class FdtdLife : public fdtd::Model {
public:
    // Types
    enum {
        notifyInitialisation=notifyNextAvailableCode, notifyDataRead,
        notifyMinorChange, notifyDomainSizeChange, notifyDomainContentsChange,
        notifyMinorNoViews, notifyIndividualLoaded, notifyModelStop
    };
    enum {viewModeNormal};

    // Construction
    explicit FdtdLife(TestSuite* testSuite = nullptr);

    // Overrides of Model
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void initialise() override;
    void clear() override;

    // Getters
    [[nodiscard]] bool whiteBackground() const { return whiteBackground_; }
    [[nodiscard]] TestSuite* testSuite() const { return testSuite_; }

    // Setters
    void whiteBackground(bool v) { whiteBackground_ = v; }

    // API
    Clipboard& clipboard() { return clipboard_; }
    box::Factory<ViewFrame>& viewFactory() { return viewFactory_; }
    std::list<std::unique_ptr<ViewFrame>>& views() { return views_; }
    ViewFrame* restoreView(xml::DomObject& o);
    ViewFrame* makeView(int mode);
    ViewFrame* findView(int identifier);
    void removeView(ViewFrame* analyser);

protected:
    // Configuration
    bool whiteBackground_{false};  // Use a white background for plots
    TestSuite* testSuite_;
    Clipboard clipboard_;
    box::Factory<ViewFrame> viewFactory_;
    std::list<std::unique_ptr<ViewFrame>> views_;  // The list of views
};


#endif //FDTDLIFE_FDTDLIFE_H
