/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FdtdApp.h"
#include <gtkmm/cssprovider.h>

// Constructor
FdtdApp::FdtdApp(int argc, char** argv, TestSuite* testSuite) :
        Application(argc, argv),
        model_(testSuite),
        mainWindow_(model_),
        testSuite_(testSuite) {
    applyStyles();
}

// Run the application
void FdtdApp::runApplication() {
    run(mainWindow_);
}

// Apply the global style sheet
void FdtdApp::applyStyles() {
    std::string css = R"---(
        entry {
            min-height: 0px;
            padding-top: 2mm; padding-bottom: 2mm;
            margin-top: 2mm; margin-bottom: 2mm;
            margin-left: 1mm; margin-right: 1mm;
        }
        .namedbutton {
            min-height: 0px;
            padding-top: 2mm; padding-bottom: 2mm;
            margin-top: 2mm; margin-bottom: 2mm;
            margin-left: 1mm; margin-right: 1mm;
        }
        .combo {
            min-height: 10mm;
            padding-top: 0px; padding-bottom: 0px;
            margin-top: 1mm; margin-bottom: 1mm;
            margin-left: 1mm; margin-right: 1mm;
        }
        notebook {
            margin-left: 2mm; margin-right: 2mm;
        }
        .namedentry {
            margin-left: 4mm;
            margin-right: 1mm;
        }
        .namedentryaligned {
            margin-left: 4mm;
            margin-right: 1mm;
        }
        .namedcheckbox {
            margin-left: 4mm;
            margin-right: 2mm;
        }
        .framedlayout {
            margin-left: 2mm; margin-right: 2mm;
            margin-top: 2mm; margin-bottom: 2mm;
        }
        .framedlayout>border {
            border-radius: 5px;
        }
        .notebooklayout {
            border-radius: 5px;
            margin-bottom: 2mm;
        }
        .namedlistview {
            padding: 1mm;
        }
        .namedlistviewname {
            padding: 2mm;
            font-weight: bold;
        }
        .itemsandinfoscroll {
            margin-right: 1mm;
            margin-left: 2mm;
            margin-top: 2mm;
            margin-bottom: 2mm;
            border: 1px solid rgb(213,208,204);
        }
        .itemsandinfodlg {
            margin-right: 2mm;
            margin-left: 1mm;
            margin-top: 2mm;
            margin-bottom: 2mm;
        }
        .itemsandinfotree {
            padding: 1mm;
        }
        tooltip {
            background-color: darkblue;
        }
        tooltip * {
            padding: 0px;
            margin: 0px;
            font-size: 8pt;
        }
    )---";
    cssProvider_ = Gtk::CssProvider::create();
    auto screen = Gdk::Display::get_default()->get_default_screen();
    Gtk::StyleContext::add_provider_for_screen(screen, cssProvider_,
                                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    cssProvider_->load_from_data(css);
}
