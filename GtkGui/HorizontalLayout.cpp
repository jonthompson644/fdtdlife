/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "HorizontalLayout.h"

// Constructor
HorizontalLayout::HorizontalLayout(
        const std::vector<std::reference_wrapper<GuiElement>>& elements,
        size_t numCols) {
    addElements(elements, numCols);
}

// Add elements to the horizontal layout
void HorizontalLayout::addElements(
        const std::vector<std::reference_wrapper<GuiElement>>& elements,
        size_t numCols) {
    // Record the elements for later placement
    numCols_ = numCols;
    elements_ = elements;
    // Register the elements
    for(auto& item : elements_) {
        registerChild(item);
    }
}

// Add the GUI elements to the page
void HorizontalLayout::initialiseGuiElements(bool recursive) {
    // Base class
    GuiElement::initialiseGuiElements(recursive);
    // Me
    int col = 0;
    for(auto& item : elements_) {
        Gtk::Widget* leftWidget = item.get().leftWidget();
        size_t width = 1;
        if(leftWidget != nullptr) {
            width = item.get().leftColumns();
            attach(*leftWidget, col, 0, width, 1);
            col += width;
        }
        Gtk::Widget* rightWidget = item.get().rightWidget();
        if(rightWidget != nullptr) {
            width = item.get().rightColumns();
            attach(*rightWidget, col, 0, width, 1);
            col += width;
        }
    }
}
