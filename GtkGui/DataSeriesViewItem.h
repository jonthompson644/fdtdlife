/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DATASERIESVIEWITEM_H
#define FDTDLIFE_DATASERIESVIEWITEM_H

#include "ViewItem.h"
#include "NamedDataSeries.h"

// A view of a data series sensorId
class DataSeriesViewItem : public ViewItem {
public:
    // Construction
    DataSeriesViewItem() = default;
    void construct(int mode, int id, ViewFrame* frame) override;

    // Overrides of ViewItem
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void initialise() override;
    void update() override;

    // API
    virtual void convertData(int /*x*/, std::vector<double>& /*dy*/, double& /*dx*/) {}
    virtual void fixInfo(sensor::DataSpec::Info& /*info*/) {}

    // Getters
    double xMin() const { return xMin_; }
    double xMax() const { return xMax_; }
    bool xAutomatic() const { return xAutomatic_; }
    double yMin() const { return yMin_; }
    double yMax() const { return yMax_; }
    bool yAutomatic() const { return yAutomatic_; }
    NamedDataSeries& dataDisplay() { return dataDisplay_; }
    const std::string& title() const { return title_; }

    // Setters
    void xMin(double v) { xMin_ = v; }
    void xMax(double v) { xMax_ = v; }
    void xAutomatic(bool v) { xAutomatic_ = v; }
    void yMin(double v) { yMin_ = v; }
    void yMax(double v) { yMax_ = v; }
    void yAutomatic(bool v) { yAutomatic_ = v; }
    void title(const std::string& v) { title_ = v; }

protected:
    // Configuration
    double xMin_{0.0};
    double xMax_{0.0};
    bool xAutomatic_{true};
    double yMin_{0.0};
    double yMax_{0.0};
    bool yAutomatic_{true};
    std::string title_;

    // Members
    NamedDataSeries dataDisplay_{true};
};


#endif //FDTDLIFE_DATASERIESVIEWITEM_H
