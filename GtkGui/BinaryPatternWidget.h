/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_BINARYPATTERNWIDGET_H
#define FDTDLIFE_BINARYPATTERNWIDGET_H

#include <gtkmm/drawingarea.h>
#include <Box/BinaryPattern.h>

// A widget that allows the editing of a binary pattern
class BinaryPatternWidget : public Gtk::DrawingArea {
public:
    // Construction
    explicit BinaryPatternWidget(bool fourFold = true, bool readOnly = false);

    // API
    void clear();

    // Getters
    bool fourFold() const { return fourFold_; }
    size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
    sigc::signal<void>& changedSignal() { return changedSignal_; }
    const box::BinaryPattern& pattern() const { return pattern2Fold_; }
    size_t pixelsPerBit() const { return pixelsPerBit_; }

    // Setters
    void fourFold(bool v) { fourFold_ = v; }
    void bitsPerHalfSide(size_t v);
    void pattern(const box::BinaryPattern& v);

    // Overrides of DrawingArea
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
    bool on_button_press_event(GdkEventButton* button_event) override;

protected:
    // Members
    box::BinaryPattern pattern2Fold_;
    size_t bitsPerHalfSide_ {8};
    bool fourFold_{true};
    size_t pixelsPerBit_{};
    size_t rectPixels_{};
    bool readOnly_{};
    static const size_t bitsPerCodingWord_ = 64;  // Number of bits in each word of the coding
    sigc::signal<void> changedSignal_;
};


#endif //FDTDLIFE_BINARYPATTERNWIDGET_H
