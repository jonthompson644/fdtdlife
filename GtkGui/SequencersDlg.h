/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SEQUENCERSDLG_H
#define FDTDLIFE_SEQUENCERSDLG_H

#include "InitialisingGui.h"
#include "ItemsAndInfoLayout.h"
#include "FdtdLife.h"
#include "NamedButton.h"
#include "FactoryButton.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include <Seq/Sequencer.h>

class ApplicationDlg;

// The dialog panel showing sequencers on the top level page's notebook
class SequencersDlg : public ItemsAndInfoLayout {
public:
    // Types
    typedef SubDialog<SequencersDlg, seq::Sequencer> SubDlg;

    // Construction
    SequencersDlg(FdtdLife& model, ApplicationDlg* parentWindow);

    // API
    void populate();

    // Accessors
    FdtdLife& model() { return model_; }
    ApplicationDlg* dlg() { return dlg_; }

protected:
    // Members
    FdtdLife& model_;
    InitialisingGui initialising_;
    ItemsAndInfoLayout::Columns columns_;
    FactoryButton<seq::Sequencer> newButton_{
            "New Sequencer", model_.sequencers().sequencerFactory(),
            {seq::Sequencers::modeSingle, seq::Sequencers::modeCharacterizer},
            FactoryButton<seq::Sequencer>::These::only};
    NamedButton deleteButton_{"Delete Sequencer"};
    NamedButton copyToClipboard_{"Copy to Clipboard"};
    NamedButton pasteFromClipboard_{"Paste from Clipboard"};
    NamedButton clearSequencer_{"Clear Sequencer and Discard Data"};
    NamedButton makeCsv_{"Make CSV on Clipboard"};
    SubDialogFactory<SubDlg> dlgFactory_;
    ApplicationDlg* dlg_;

    // Helpers
    void fillSequencerList(seq::Sequencer* select = nullptr);
    seq::Sequencer* currentSequencer();

    // Handlers
    void onNewSequencer(int mode);
    void onDeleteSequencer();
    void onCopyToClipboard();
    void onPasteFromClipboard();
    void onClearSequencer();
    void onMakeCsv();
    void onItemChanged(const Gtk::ListStore::Row& row);
    void onItemSelected(const Gtk::ListStore::Row* row);
};

#endif //FDTDLIFE_SEQUENCERSDLG_H
