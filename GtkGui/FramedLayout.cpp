/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FramedLayout.h"

#include <utility>

// Constructor
FramedLayout::FramedLayout(std::string name, GuiElement& child, Gtk::PackOptions packOptions) :
        name_(std::move(name)) {
    packOptions_ = packOptions;
    registerChild(child);
}

// Constructor
FramedLayout::FramedLayout(std::string name, Gtk::PackOptions packOptions) :
        name_(std::move(name)) {
    packOptions_ = packOptions;
}

// Add the GUI elements to the page
void FramedLayout::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    set_label(name_);
    if(!children_.empty()) {
        Frame::add(children_.front()->widget());
    }
    get_style_context()->add_class("framedlayout");
    set_label_align(0.01, 0.5);
    registerItem(name_, *this);
}

// Add a child to the frame, replaces any existing child
void FramedLayout::addChild(GuiElement& child) {
    registerChild(child);
    Frame::add(child.widget());
}

// Remove a child from the frame
void FramedLayout::removeChild() {
    Frame::remove();
}