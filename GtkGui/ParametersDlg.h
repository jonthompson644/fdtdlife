/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETERSDLG_H
#define FDTDLIFE_PARAMETERSDLG_H

#include <gtkmm/cellrenderercombo.h>
#include "FdtdLife.h"
#include "InitialisingGui.h"
#include "NamedListView.h"
#include "NamedButton.h"
#include "NamedMenuButton.h"
#include "HorizontalLayout.h"
#include "VerticalLayout.h"

// The dielectric parameters top level page
class ParametersDlg : public VerticalLayout {
public:
    // Construction
    ParametersDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // API
    void populate();

    // Tables list view columns
    class TablesColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        TablesColumns() {
            add(colIdentifier_);
            add(colName_);
            add(colUnitCell_);
            add(colLayerSpacing_);
            add(colIncidenceAngle_);
            add(colPatchRatio_);
        }
        // Columns
        Gtk::TreeModelColumn<int> colIdentifier_;
        Gtk::TreeModelColumn<std::string> colName_;
        Gtk::TreeModelColumn<double> colUnitCell_;
        Gtk::TreeModelColumn<double> colLayerSpacing_;
        Gtk::TreeModelColumn<double> colIncidenceAngle_;
        Gtk::TreeModelColumn<double> colPatchRatio_;
    } tablesColumns_;

    // Curves list view columns
    class CurvesColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        CurvesColumns() {
            add(colIdentifier_);
            add(colWhich_);
        }
        // Columns
        Gtk::TreeModelColumn<int> colIdentifier_;
        Gtk::TreeModelColumn<std::string> colWhich_;
    } curvesColumns_;

    // Columns for the curves which combo box
    class CurveWhichColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        CurveWhichColumns() {
            add(colName_);
        }
        // Columns
        Gtk::TreeModelColumn<std::string> colName_;
    } curveWhichColumns_;

    // Points list view columns
    class PointsColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        PointsColumns() {
            add(colIdentifier_);
            add(colFrequency_);
            add(colRealValue_);
            add(colImagValue_);
        }
        // Columns
        Gtk::TreeModelColumn<int> colIdentifier_;
        Gtk::TreeModelColumn<double> colFrequency_;
        Gtk::TreeModelColumn<double> colRealValue_;
        Gtk::TreeModelColumn<double> colImagValue_;
    } pointsColumns_;

    // Handlers
    void onNewTable();
    void onDeleteTable();
    void onCopyTable();
    void onCopyAllTables();
    void onPasteTable();
    void onPasteExcel(domain::ParameterCurve::Which which);
    void onClearTables();
    void onFillDown();
    void onTableChanged(const Gtk::TreeModel::Path& path,
                        const Gtk::TreeModel::iterator& pos);
    void onTableSelected();
    void onNewCurve();
    void onDeleteCurve();
    void onCopyCurve();
    void onPasteCurve();
    void onClearCurves();
    void onCurveChanged(const Gtk::TreeModel::Path& path,
                        const Gtk::TreeModel::iterator& pos);
    void onCurveSelected();
    void onNewPoint();
    void onDeletePoint();
    void onClearPoints();
    void onPointChanged(const Gtk::TreeModel::Path& path,
                        const Gtk::TreeModel::iterator& pos);
    void onWhichEdited(const std::string& path, const std::string& newText);

    // Helpers
    void populateTables(domain::ParameterTable* sel = nullptr);
    void populateCurves(domain::ParameterCurve* sel = nullptr);
    void populatePoints(domain::ParameterPoint* sel = nullptr);
    domain::ParameterTable* currentTable();
    domain::ParameterTable* nextTable();
    domain::ParameterCurve* currentCurve();
    domain::ParameterCurve* nextCurve();
    domain::ParameterPoint* currentPoint();
    domain::ParameterPoint* nextPoint();

    // Members
    FdtdLife& model_;
    Gtk::Window* parentWindow_;
    InitialisingGui initialising_;
    Gtk::CellRendererCombo curveWhichRenderer_;
    Glib::RefPtr<Gtk::ListStore> curveWhichModel_;

    // Widgets
    NamedListView tables_{"Parameter Tables"};  // The list of tables
    NamedButton newTable_{"New Table"};
    NamedButton deleteTable_{"Delete Table"};
    NamedButton copyTable_{"Copy Table"};
    NamedButton copyAllTables_{"Copy All Tables"};
    NamedButton pasteTable_{"Paste Tables"};
    NamedMenuButton<domain::ParameterCurve::Which> pasteExcel_{
            "Paste From Excel",
            {{"Permittivity", domain::ParameterCurve::Which::permittivity},
             {"Permeability", domain::ParameterCurve::Which::permeability},
             {"Admittance", domain::ParameterCurve::Which::admittance},
             {"Refractive Index", domain::ParameterCurve::Which::refractiveIndex}}};
    NamedButton clearTables_{"Clear Tables"};
    NamedButton fillDown_{"Fill Down"};
    HorizontalLayout tableButtons_{{newTable_, deleteTable_, copyTable_,
                                           copyAllTables_, pasteTable_, pasteExcel_,
                                           clearTables_, fillDown_}};
    NamedListView curves_{"Table Curves"};  // The current curve
    NamedButton newCurve_{"New Curve"};
    NamedButton deleteCurve_{"Delete Curve"};
    NamedButton copyCurve_{"Copy Curve"};
    NamedButton pasteCurve_{"Paste Curves"};
    NamedButton clearCurves_{"Clear Curves"};
    HorizontalLayout curveButtons_{{newCurve_, deleteCurve_, copyCurve_,
                                           pasteCurve_, clearCurves_}};
    VerticalLayout curvesLayout_{{curves_, curveButtons_}};
    NamedListView points_{"Curve Points"};  // The current item
    NamedButton newPoint_{"New Point"};
    NamedButton deletePoint_{"Delete Point"};
    NamedButton clearPoints_{"Clear Points"};
    HorizontalLayout pointButtons_{{newPoint_, deletePoint_, clearPoints_}};
    VerticalLayout pointsLayout_{{points_, pointButtons_}};
    HorizontalLayout bottomHalf_{{curvesLayout_, pointsLayout_}};
};


#endif //FDTDLIFE_PARAMETERSDLG_H
