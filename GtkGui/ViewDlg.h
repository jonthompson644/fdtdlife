/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VIEWDLG_H
#define FDTDLIFE_VIEWDLG_H

#include <Fdtd/Notifiable.h>
#include "ViewsDlg.h"
#include "NamedEntry.h"
#include "NamedToggleButton.h"
#include "NamedCheckboxList.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include "HorizontalLayout.h"

class ViewDlg : public ViewsDlg::SubDlg, public fdtd::Notifiable {
public:
    // Types
    typedef SubDialog<ViewDlg, ViewItem> SubDlg;

    // Constructor
    ViewDlg() = default;
    void construct(ViewsDlg* owner, ViewFrame* view) override;

    // Overrides of SubDlg
    void populate() override;

    // Overrides of fdtd::Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected:
    // Members
    ViewFrame* view_{nullptr};
    SubDialogFactory<SubDlg> itemDlgFactory_;

    // The widgets
    NamedEntry<int> minDisplayRate_{"Min Display Rate (ms)"};
    NamedToggleButton displayAllFrames_{"Display All Frames"};
    NamedCheckboxList sensorDataSources_{"Sensor Data Sources"};
    FramedLayout subDialogFrame_{"Item Properties"};
    std::unique_ptr<GridLayout> subDialog_;
    HorizontalLayout line2_{{sensorDataSources_, subDialogFrame_},
                        4};

    // Handlers
    virtual void onChange();
    void onDataSourceSelection(bool checked, int identifier, const std::string& name);
    void onDataSourceChange(bool checked, int identifier, const std::string& name);

    // Helpers
    void fillDataSources();

};


#endif //FDTDLIFE_VIEWDLG_H
