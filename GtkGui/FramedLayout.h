/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_FRAMEDLAYOUT_H
#define FDTDLIFE_FRAMEDLAYOUT_H

#include <gtkmm/frame.h>
#include "GuiElement.h"

// A class that lays a widget out in a frame
class FramedLayout : public Gtk::Frame, public GuiElement {
public:
    // Construction
    FramedLayout(std::string  name, GuiElement& child,
            Gtk::PackOptions packOptions = Gtk::PACK_EXPAND_WIDGET);
    FramedLayout(std::string  name, Gtk::PackOptions packOptions = Gtk::PACK_EXPAND_WIDGET);

    // Overrides of GuiElement
    void initialiseGuiElements(bool recursive) override;
    Gtk::Widget& widget() override { return *this; }

    // API
    void addChild(GuiElement& child);
    void removeChild();

protected:
    std::string name_;
};


#endif //FDTDLIFE_FRAMEDLAYOUT_H
