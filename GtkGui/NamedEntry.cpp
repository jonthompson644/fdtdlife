/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include "NamedEntry.h"

// Constructor
template<class T>
NamedEntry<T>::NamedEntry(const std::string& name, bool readOnly,
                                        size_t numColsRight) :
        readOnly_(readOnly),
        numColsRight_(numColsRight) {
    // Initialise the components
    nameLabel_.set_label(name);
    nameLabel_.get_style_context()->add_class("namedentryaligned");
    nameLabel_.set_halign(Gtk::ALIGN_END);
    set_editable(!readOnly_);
    set_can_focus(!readOnly_);
    // Register
    registerItem(name, *this);
    // Connect change detector handlers
    signal_activate().connect(
            sigc::mem_fun(this, &NamedEntry::onActivate));
    signal_state_flags_changed().connect(
            sigc::mem_fun(this, &NamedEntry::onStateFlags));
}

// Set the value
template<class T>
void NamedEntry<T>::value(T value) {
    std::stringstream s;
    s << value;
    set_text(s.str());
}

// Get the value
template<class T>
T NamedEntry<T>::value() {
    std::stringstream s(get_text());
    T result;
    s >> result;
    value(result);
    return result;
}

// Attach a change handler
template<class T>
void NamedEntry<T>::connectChangeHandler(const sigc::slot<void>& slot) {
    if(curConnection_.connected()) {
        curConnection_.disconnect();
    }
    curConnection_ = changedSignal_.connect(slot);
}

// Handles the activation signal
template<class T>
void NamedEntry<T>::onActivate() {
    changedSignal_.emit();
}

// Handles state flag notifications
template<class T>
void NamedEntry<T>::onStateFlags(Gtk::StateFlags previous) {
    if((get_state_flags() & GTK_STATE_FLAG_FOCUSED) == 0 &&
       (previous & GTK_STATE_FLAG_FOCUSED) != 0) {
        changedSignal_.emit();
    }
}

// Set the readonly flag
template<class T>
void NamedEntry<T>::readOnly(bool value) {
    readOnly_ = value;
    set_editable(!readOnly_);
    set_can_focus(!readOnly_);
}

// Instantiate particular types to make linking work
template
class NamedEntry<double>;

template
class NamedEntry<int>;

template
class NamedEntry<size_t>;

template
class NamedEntry<long>;

template
class NamedEntry<std::string>;

// Constructor for bool specialisation
NamedEntry<bool>::NamedEntry(const std::string& name) :
        NamedEntry<std::string>(name, true) {
}

// Set the value for bool specialisation
void NamedEntry<bool>::value(bool value) {
    set_text(value ? "yes" : "no");
}

// Constructor for box::Expression specialisation
NamedEntry<box::Expression>::NamedEntry(const std::string& name) :
        NamedEntry<std::string>(name, false) {
}

// Set the value for box::expression specialisation
void NamedEntry<box::Expression>::value(const box::Expression& value) {
    set_text(value.text());
    set_tooltip_text(box::Utility::fromDouble(value.value()));
}

// Get the value for box::expression specialisation
box::Expression NamedEntry<box::Expression>::value() {
    return box::Expression(get_text());
}
