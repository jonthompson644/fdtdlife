/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SequencersDlg.h"
#include "OkCancelDialog.h"
#include <Seq/Sequencers.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include "SingleJobDlg.h"
#include "CharacterizerDlg.h"
#include "ApplicationDlg.h"

// Constructor
SequencersDlg::SequencersDlg(FdtdLife& model, ApplicationDlg* parentWindow) :
        ItemsAndInfoLayout("Sequencers"),
        model_(model),
        dlg_(parentWindow) {
    comment("SequencersDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<SingleJobDlg>(
            seq::Sequencers::modeSingle));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<CharacterizerDlg>(
            seq::Sequencers::modeCharacterizer));
    // Initialise
    setButtons({newButton_, deleteButton_, copyToClipboard_, pasteFromClipboard_,
                clearSequencer_, makeCsv_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(*this, &SequencersDlg::onNewSequencer));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &SequencersDlg::onDeleteSequencer));
    copyToClipboard_.signal_clicked().connect(
            sigc::mem_fun(*this, &SequencersDlg::onCopyToClipboard));
    pasteFromClipboard_.signal_clicked().connect(
            sigc::mem_fun(*this, &SequencersDlg::onPasteFromClipboard));
    clearSequencer_.signal_clicked().connect(
            sigc::mem_fun(*this, &SequencersDlg::onClearSequencer));
    makeCsv_.signal_clicked().connect(
            sigc::mem_fun(*this, &SequencersDlg::onMakeCsv));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &SequencersDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &SequencersDlg::onItemSelected));
}

// Fill the controls with their values
void SequencersDlg::populate() {
    fillSequencerList(model_.sequencers().find(currentItem()));
}

// Fill the analyser list
void SequencersDlg::fillSequencerList(seq::Sequencer* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    for (auto& sequencer: model_.sequencers().sequencers()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = sequencer->identifier();
        row[columns_.name_] = sequencer->name();
        row[columns_.mode_] = model_.sequencers().sequencerFactory().modeName(
                sequencer->mode());
        if (sequencer.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new sequencer
void SequencersDlg::onNewSequencer(int mode) {
    auto sequencer = model_.sequencers().makeSequencer(mode);
    std::stringstream name;
    name << "Sequencer " << sequencer->identifier();
    sequencer->name(name.str());
    fillSequencerList(sequencer.get());
    model_.doNotification(FdtdLife::notifyDomainContentsChange);
}

// Delete the currently selected sequencer
void SequencersDlg::onDeleteSequencer() {
    int id = currentItem();
    if (id >= 0) {
        auto* sequencer = model_.sequencers().find(id);
        auto* nextSequencer = model_.sequencers().find(nextItem());
        if (nextSequencer == nullptr) {
            nextSequencer = model_.sequencers().find(prevItem());
        }
        if (sequencer != nullptr) {
            std::stringstream s;
            s << "Ok to delete sequencer \"" << sequencer->name() << "\"?";
            OkCancelDialog box(dlg_, "Delete Sequencer", s.str(),
                               model_.testSuite());
            if (box.ask()) {
                model_.sequencers().remove(sequencer);
                fillSequencerList(nextSequencer);
            }
        }
    }
}

// The indicated item has been changed
void SequencersDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if (!initialising_) {
        seq::Sequencer* sequencer = model_.sequencers().find(row[columns_.id_]);
        if (sequencer != nullptr) {
            sequencer->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void SequencersDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if (row != nullptr) {
        seq::Sequencer* sequencer = model_.sequencers().find((*row)[columns_.id_]);
        if (sequencer != nullptr) {
            newDialog = dlgFactory_.make(this, sequencer);
        }
    }
    setSubDialog(newDialog);
}

// Return the current selected sequencer
seq::Sequencer* SequencersDlg::currentSequencer() {
    return model_.sequencers().find(currentItem());
}

// Copy the current sequencer to the clipboard
void SequencersDlg::onCopyToClipboard() {
    seq::Sequencer* seq = currentSequencer();
    if(seq != nullptr) {
        // Encode the shape into XML
        auto* o = new xml::DomObject("sequencer");
        *o << *seq;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Copy the data to the clipboard
        model_.clipboard().put(text, "fdtd/sequencer");
    }
}

// Paste a sequencer from the clipboard
void SequencersDlg::onPasteFromClipboard() {
    // Do we have a recognised format?
    if(model_.clipboard().hasTarget("fdtd/sequencer")) {
        std::string text = model_.clipboard().getText("fdtd/sequencer");
        try {
            // Read the XML into a DOM
            xml::DomDocument dom("sequencer");
            xml::Reader reader;
            reader.readString(&dom, text);
            // Create a sequencer from the DOM
            auto seq = model_.sequencers().makeSequencer(*dom.getObject());
            *dom.getObject() >> *seq;
            fillSequencerList(seq.get());
        } catch(xml::Exception& e) {
            std::cout << "Sequencer paste failed to read XML: " << e.what() << std::endl;
        }
    }
}

// Clear the current sequencer and discard all its data
void SequencersDlg::onClearSequencer() {
    seq::Sequencer* sequencer = currentSequencer();
    if(sequencer != nullptr) {
        std::stringstream msg;
        msg << "Ok to clear the sequencer \"" << sequencer->name() << "\" and discard all its data?";
        OkCancelDialog box(dlg_, "Clear Sequencer", msg.str(),
                           model_.testSuite());
        if (box.ask()) {
            sequencer->clearAndDiscard();
            InitialisingGui::Set set(initialising_);
            model_.doNotification(FdtdLife::notifySequencerChange);
        }
    }
}

// Put CSV data on the clipboard from the sequencer data
void SequencersDlg::onMakeCsv() {
    seq::Sequencer* seq = currentSequencer();
    if(seq != nullptr) {
        // Get the CSV data
        std::string csv = seq->makeCsv();
        // Copy the data to the clipboard
        model_.clipboard().put(csv);
    }
}
