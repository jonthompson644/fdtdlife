/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_GUIELEMENT_H
#define FDTDLIFE_GUIELEMENT_H

#include <string>
#include <gtkmm/widget.h>
#include <gtkmm/box.h>

// A base class for all GUI elements.
class GuiElement {
public:
    // Construction
    GuiElement() = default;
    virtual ~GuiElement();

    // API
    void connectDefaultChangeHandler(const sigc::slot<void>& slot);
    virtual void connectChangeHandler(const sigc::slot<void>& /*slot*/) {}
    virtual void initialiseGuiElements(bool recursive);
    virtual Gtk::Widget& widget() = 0;
    void registerItem(const std::string& name, Gtk::Widget& widget);
    void registerChild(GuiElement& child);
    void deregisterChild(GuiElement& child);
    void clearChildren() { items_.clear(); }
    Gtk::Widget* findWidget(const std::string& name);
    void dump(int level=0);
    void comment(const std::string& v) { comment_ = v; }
    virtual Gtk::Widget* leftWidget() { return nullptr; }
    virtual Gtk::Widget* rightWidget() { return &widget(); }
    [[nodiscard]] virtual size_t rightColumns() const { return 1; }
    [[nodiscard]] virtual size_t leftColumns() const { return 1; }

    // Getters
    [[nodiscard]] Gtk::PackOptions packOptions() const { return packOptions_; }

protected:
    // Members
    std::string comment_;
    std::map<std::string, std::reference_wrapper<Gtk::Widget>> items_;
    std::list<GuiElement*> children_;
    Gtk::PackOptions packOptions_{Gtk::PACK_SHRINK};
    GuiElement* parent_{};
};


#endif //FDTDLIFE_GUIELEMENT_H
