/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ItemsAndInfoLayout.h"
#include <utility>

// Constructor
ItemsAndInfoLayout::ItemsAndInfoLayout(const std::string& name,
                                       const std::vector<std::reference_wrapper<
                                               GuiElement>>& buttons) {
    registerItem(name, treeView_);
    setButtons(buttons);
}

// Constructor
ItemsAndInfoLayout::ItemsAndInfoLayout(const std::string& name) {
    registerItem(name, treeView_);
}

// Set the columns
void ItemsAndInfoLayout::setColumns(Columns& columns) {
    columns_ = &columns;
    // Create the columns
    treeView_.setColumns(&columns);
    treeView_.append_column("ID", columns.id_);
    treeView_.append_column_editable("Name", columns.name_);
    treeView_.append_column("Type", columns.mode_);
    treeView_.model()->signal_row_changed().connect(
            sigc::mem_fun(*this, &ItemsAndInfoLayout::onRowChanged));
    treeView_.get_selection()->signal_changed().connect(
            sigc::mem_fun(*this, &ItemsAndInfoLayout::onRowSelected));
}

// Add the GUI elements to the page
void ItemsAndInfoLayout::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    scrolledWindow_.add(treeView_);
    scrolledWindow_.set_policy(Gtk::POLICY_AUTOMATIC,
                               Gtk::POLICY_AUTOMATIC);
    scrolledWindow_.set_min_content_width(300);
    leftColumn_.pack_start(scrolledWindow_, Gtk::PACK_EXPAND_WIDGET);
    leftColumn_.pack_end(buttonBox_, Gtk::PACK_SHRINK);
    for(auto& child : children_) {
        buttonBox_.pack_start(child->widget(), Gtk::PACK_SHRINK);
    }
    treeView_.get_selection()->set_mode(Gtk::SELECTION_SINGLE);
    pack_start(leftColumn_, Gtk::PACK_SHRINK);
    pack_end(subDialogFrame_, Gtk::PACK_EXPAND_WIDGET);
    scrolledWindow_.get_style_context()->add_class("itemsandinfoscroll");
    treeView_.get_style_context()->add_class("itemsandinfotree");
    subDialogFrame_.get_style_context()->add_class("itemsandinfodlg");
}

// Register the buttons
void ItemsAndInfoLayout::setButtons(
        const std::vector<std::reference_wrapper<GuiElement>>& buttons) {
    for(auto button : buttons) {
        registerChild(button);
    }
}

// Return the identifier of the currently selected item
int ItemsAndInfoLayout::currentItem() {
    int result = -1;
    if(treeView_.get_selection()->count_selected_rows() > 0) {
        Gtk::ListStore::Row selection = *treeView_.get_selection()->get_selected();
        result = selection[columns_->id_];
    }
    return result;
}

// Return the identifier of the item after the currently selected item
// or -1 if the current item is the last
int ItemsAndInfoLayout::nextItem() {
    int result = -1;
    if(treeView_.get_selection()->count_selected_rows() > 0) {
        auto rowPos = treeView_.get_selection()->get_selected();
        rowPos++;
        if(rowPos != treeView_.model()->children().end()) {
            result = (*rowPos)[columns_->id_];
        }
    }
    return result;
}

// Return the identifier of the item before the currently selected item
// or -1 if the current item is the first
int ItemsAndInfoLayout::prevItem() {
    int result = -1;
    if(treeView_.get_selection()->count_selected_rows() > 0) {
        auto rowPos = treeView_.get_selection()->get_selected();
        if(rowPos != treeView_.model()->children().begin()) {
            rowPos--;
            result = (*rowPos)[columns_->id_];
        }
    }
    return result;
}

// Handle the row changed signal, emit an item changed signal
void ItemsAndInfoLayout::onRowChanged(const Gtk::TreeModel::Path& /*path*/,
                                      const Gtk::TreeModel::iterator& pos) {
    Gtk::ListStore::Row row = *pos;
    itemChangedSignal_.emit(row);
}

// Handle a row selection change signal, emit the item selected signal
void ItemsAndInfoLayout::onRowSelected() {
    const Gtk::ListStore::Row* row = nullptr;
    if(treeView_.get_selection()->count_selected_rows() > 0) {
        auto rowPos = treeView_.get_selection()->get_selected();
        row = &(*rowPos);
    }
    itemSelectedSignal_.emit(row);
}

// Set the current sub-dialog
void ItemsAndInfoLayout::setSubDialog(GridLayout *subDialog) {
    subDialogFrame_.remove();
    if(subDialog != nullptr) {
        subDialogFrame_.add(*subDialog);
        subDialogFrame_.show_all_children();
        registerChild(*subDialog);
    }
    subDialog_.reset(subDialog);
}
