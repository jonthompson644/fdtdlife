/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Sensor/TwoDSlice.h>
#include "TwoDSliceViewItem.h"
#include "ViewFrame.h"
#include "FdtdLife.h"

// Second stage constructor used by the factory
void TwoDSliceViewItem::construct(int mode, int id, ViewFrame* frame) {
    registerChild(dataDisplay_);
    ViewItem::construct(mode, id, frame);
    pack_start(dataDisplay_.widget(), Gtk::PACK_EXPAND_WIDGET);
    dataDisplay_.clipboard(&frame->m()->clipboard());
    initialise();
}

// Write the configuration to an XML DOM
void TwoDSliceViewItem::writeConfig(xml::DomObject& root) const {
    // Base class first
    ViewItem::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("colormap") << colourMap_;
    root << xml::Obj("displaymax") << displayMax_;
    root << xml::Close();
}

// Read the configuration from an XML DOM
void TwoDSliceViewItem::readConfig(xml::DomObject& root) {
    // Base class first
    ViewItem::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("colormap") >> xml::Default(colourMap_) >> colourMap_;
    root >> xml::Obj("displaymax") >> xml::Default(displayMax_) >> displayMax_;
    root >> xml::Close();
}

// Initialise the item ready for the run
void TwoDSliceViewItem::initialise() {
    // Get information from the sensor
    auto* twoDSliceSensor = dynamic_cast<sensor::TwoDSlice*>(sensor());
    if(twoDSliceSensor != nullptr) {
        width_ = twoDSliceSensor->nSliceX();
        height_ = twoDSliceSensor->nSliceY();
        vertical_ = height_ >= width_;
        dataDisplay_.title(twoDSliceSensor->name());
        dataDisplay_.config(displayMax_, colourMap_);
        update();
    }
}

// Update the display with data from the sensor
void TwoDSliceViewItem::update() {
    // Get data from the sensor
    auto* twoDSliceSensor = dynamic_cast<sensor::TwoDSlice*>(sensor());
    if(twoDSliceSensor != nullptr) {
        auto* spec = twoDSliceSensor->getDataSpec(dataSpecName_);
        double d, dx, dy;
        if(spec != nullptr) {
            std::vector<double> data;
            data.resize(width_ * height_);
            for(size_t y = 0; y < height_; y++) {
                for(size_t x = 0; x < width_; x++) {
                    twoDSliceSensor->getData(spec, x, y, &d, &dx, &dy);
                    data[y * width_ + x] = d;
                }
            }
            dataDisplay_.data(width_, height_, data);
        }
    }
}

// Set the colour map configuration
void TwoDSliceViewItem::colourMap(TwoDSliceWidget::ColourMap v) {
    colourMap_ = v;
    dataDisplay_.config(displayMax_, colourMap_);
}

// Set the display maximum configuration
void TwoDSliceViewItem::displayMax(double v) {
    displayMax_ = v;
    dataDisplay_.config(displayMax_, colourMap_);
}
