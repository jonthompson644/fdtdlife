/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "MaterialsDlg.h"
#include "OkCancelDialog.h"
#include "CloseDialog.h"
#include "NormalMaterialDlg.h"
#include "CpmlMaterialDlg.h"

// Constructor
MaterialsDlg::MaterialsDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Materials"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("MaterialsDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<NormalMaterialDlg>(
            domain::Domain::materialModeNormal));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<NormalMaterialDlg>(
            domain::Domain::materialModeBackground));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<CpmlMaterialDlg>(
            domain::Domain::materialModeCpmlBoundary));
    // Initialise
    setButtons({newButton_, deleteButton_});
    setColumns(columns_);
    treeView_.append_column("Permittivity", columns_.permittivity_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(*this, &MaterialsDlg::onNewMaterial));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &MaterialsDlg::onDeleteMaterial));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &MaterialsDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &MaterialsDlg::onItemSelected));
}

// Fill the controls with their values
void MaterialsDlg::populate() {
    fillMaterialList(model_.d()->getMaterial(currentItem()));
}

// Fill the materials list
void MaterialsDlg::fillMaterialList(domain::Material* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    auto& domain = *model_.d();
    for(auto& material : domain.materials()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = material->index();
        row[columns_.name_] = material->name();
        row[columns_.mode_] = domain.materialFactory().modeName(material->mode());
        row[columns_.permittivity_] = material->epsilon().value() / box::Constants::epsilon0_;
        if(material.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new material
void MaterialsDlg::onNewMaterial(int mode) {
    auto material = model_.d()->makeMaterial(mode);
    std::stringstream name;
    name << "Material " << material->index();
    material->name(name.str());
    fillMaterialList(material.get());
    model_.doNotification(FdtdLife::notifyMaterialChange);
}

// Delete the currently selected material
void MaterialsDlg::onDeleteMaterial() {
    int id = currentItem();
    auto& domain = *model_.d();
    auto* material = domain.getMaterial(id);
    auto* nextMaterial = domain.getMaterial(nextItem());
    if(nextMaterial == nullptr) {
        nextMaterial = domain.getMaterial(prevItem());
    }
    if(material != nullptr) {
        if(id >= domain::Domain::firstUserMaterial && id <= domain::Domain::lastUserMaterial) {
            std::stringstream s;
            s << "Ok to delete material \"" << material->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Material", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                domain.remove(material);
                fillMaterialList(nextMaterial);
                model_.doNotification(FdtdLife::notifyMaterialChange);
            }
        } else {
            std::stringstream s;
            s << "Deleting built in material \"" << material->name() << "\" is not possible";
            CloseDialog box(parentWindow_, "Cannot Delete Material", s.str(),
                            model_.testSuite());
            box.ask();
        }
    }
}

// The indicated item has been changed
void MaterialsDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        domain::Material* material = model_.d()->getMaterial(row[columns_.id_]);
        if(material != nullptr) {
            material->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMaterialChange);
        }
    }
}

// The indicated item has been selected
void MaterialsDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        domain::Material* material = model_.d()->getMaterial((*row)[columns_.id_]);
        if(material != nullptr) {
            newDialog = dlgFactory_.make(this, material);
        }
    }
    setSubDialog(newDialog);
}