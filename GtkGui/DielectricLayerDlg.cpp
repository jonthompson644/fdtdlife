/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "DielectricLayerDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factory
void DielectricLayerDlg::construct(ShapesDlg* owner, domain::Shape* shape) {
    SubDialog::construct(owner, shape);
    material_.construct(shape->m()->d());
    comment("DielectricLayer");
    shape_ = dynamic_cast<domain::DielectricLayer*>(shape);
    addLines({{material_, ignore_},
              {layerPosition_},
              {cellSize_},
              {thickness_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &DielectricLayerDlg::onChange));
}

// Populate all the widgets with values
void DielectricLayerDlg::populate() {
    ShapesDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    material_.value(shape_->material());
    ignore_.value(shape_->ignore());
    layerPosition_.value(shape_->center());
    cellSize_.value(shape_->cellSizeX());
    thickness_.value(shape_->thickness());
}

// Default change handler
void DielectricLayerDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newMaterial = c.test(material_, shape_->material(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newIgnore = c.test(ignore_, shape_->ignore(),
                                FdtdLife::notifyDomainContentsChange);
        auto newLayerPosition = c.test(layerPosition_, shape_->center(),
                                       FdtdLife::notifyMinorChange);
        auto newCellSize = c.test(cellSize_, shape_->cellSizeX(),
                                  FdtdLife::notifyMinorChange);
        auto newThickness = c.test(thickness_, shape_->thickness(),
                                   FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            shape_->material(newMaterial);
            shape_->ignore(newIgnore);
            shape_->center(newLayerPosition);
            shape_->cellSizeX(newCellSize);
            shape_->thickness(newThickness);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
