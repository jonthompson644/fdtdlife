/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_GUICHANGEDETECTOR_H
#define FDTDLIFE_GUICHANGEDETECTOR_H

#include "NamedEntry.h"
#include "NamedTripleEntry.h"
#include "NamedTripleSelection.h"
#include "NamedCheckbox.h"
#include "NamedSelection.h"
#include "NamedBinaryPattern.h"
#include "NamedToggleButton.h"
#include <Box/VectorExpression.h>

// A class used to detect changes a user may have made in a dialog panel
class GuiChangeDetector {
public:
    // Construction
    GuiChangeDetector() = default;

    // Accessors
    [[nodiscard]] bool changeDetected() const { return changeDetected_; }
    [[nodiscard]] int why() const { return why_; }

    // API
    template<class T>
    T test(NamedEntry<T>& widget, const T& original, int why);
    bool test(NamedCheckbox& widget, bool original, int why);
    bool test(NamedToggleButton& widget, bool original, int why);
    box::VectorExpression<double> test(NamedTripleEntry<std::string>& widget,
                                       const box::VectorExpression<double>& original, int why);
    box::VectorExpression<size_t> test(NamedTripleEntry<std::string>& widget,
                                       const box::VectorExpression<size_t>& original, int why);
    box::Vector<double> test(NamedTripleEntry<double>& widget,
                             const box::Vector<double>& original, int why);
    box::Expression test(NamedEntry<std::string>& widget,
                         const box::Expression& original, int why);
    box::Expression test(NamedEntry<box::Expression>& widget,
                         const box::Expression& original, int why);
    template<class T>
    T test(NamedSelection<T>& widget, T original, int why) {
        T value = widget.value();
        if(value != original) {
            changeDetected_ = true;
            why_ = why;
        }
        return value;
    }
    box::BinaryPattern test(NamedBinaryPattern& widget, const box::BinaryPattern& original,
                            int why);
    template<class T>
    box::Vector<T> test(NamedTripleSelection<T>& widget,
                             const box::Vector<T>& original, int why) {
        box::Vector<T> value(widget.value());
        if(value != original) {
            changeDetected_ = true;
            why_ = why;
        }
        return value;
    }

protected:
    bool changeDetected_{false};
    int why_{0};
};


#endif //FDTDLIFE_GUICHANGEDETECTOR_H
