/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CUSTOMWIDGET_H
#define FDTDLIFE_CUSTOMWIDGET_H

#include <gtkmm/drawingarea.h>

// Base class for custom widgets based on Gtk::DrawingArea
class CustomWidget : public Gtk::DrawingArea {
public:
    // Construction
    CustomWidget();

    // Overrides of DrawingArea
    bool on_button_press_event(GdkEventButton* button_event) override;
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

    // API
    Glib::RefPtr<Gdk::Pixbuf> capture();

    // Getters
    sigc::signal<void, GdkEventButton*>& contextMenuSignal() { return contextMenuSignal_; }

protected:
    // Override in derived classes
    virtual void draw(const Cairo::RefPtr<Cairo::Context>& /*cr*/,
                      int /*width*/, int /*height*/) = 0;

    // Members
    sigc::signal<void, GdkEventButton*> contextMenuSignal_;
};


#endif //FDTDLIFE_CUSTOMWIDGET_H
