/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDBINARYPATTERN_H
#define FDTDLIFE_NAMEDBINARYPATTERN_H

#include <gtkmm/grid.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include "GuiElement.h"
#include "BinaryPatternWidget.h"
#include "NamedCheckbox.h"

// A class the lays out the binary pattern widget with other widgets.
class NamedBinaryPattern : public Gtk::Grid, public GuiElement {
public:
    // Construction
    explicit NamedBinaryPattern(const std::string& name, size_t numColsRight=1);
    explicit NamedBinaryPattern(size_t numColsRight=1);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void connectChangeHandler(const sigc::slot<void>& slot) override;
    size_t rightColumns() const override { return numColsRight_; }
    size_t leftColumns() const override { return 0; }

    // API
    void value(const box::BinaryPattern& v);
    const box::BinaryPattern& value() const { return binaryPattern_.pattern(); }
    bool fourFold() const { return fourFold_.get_active(); }
    void bitsPerHalfSide(size_t v) { binaryPattern_.bitsPerHalfSide(v); }

protected:
    // Component widgets
    std::unique_ptr<Gtk::Label> nameLabel_;  // The name attached to the widget
    Gtk::CheckButton fourFold_{"Four Fold"};  // The four fold check box
    Gtk::Entry hexadecimalCodingEntry_;  // The hexadecimal coding
    BinaryPatternWidget binaryPattern_;  // The binary pattern editor

    // Members
    sigc::connection curConnection_;  // The current connection
    sigc::signal<void> changedSignal_;
    size_t numColsRight_;

    // Handlers
    void onPatternChanged();
    void onFourFoldChanged();

    // Helpers
    void construct(const std::string& name);
};


#endif //FDTDLIFE_NAMEDBINARYPATTERN_H
