/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDTRIPLESELECTION_H
#define FDTDLIFE_NAMEDTRIPLESELECTION_H

#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/comboboxtext.h>
#include "GuiElement.h"

// A class that binds a label with three drop list selections
template<class T>
class NamedTripleSelection : public Gtk::Grid, public GuiElement {
public:
    // Construction
    NamedTripleSelection() = delete;
    NamedTripleSelection(const std::string& name, const std::vector<std::string>& values,
                         size_t numColsRight = 3) :
            numColsRight_(numColsRight) {
        // Initialise them
        nameLabel_.set_label(name);
        nameLabel_.get_style_context()->add_class("namedentryaligned");
        nameLabel_.set_halign(Gtk::ALIGN_END);
        for(auto& v : values) {
            x_.append(v);
            y_.append(v);
            z_.append(v);
        }
        x_.set_hexpand();
        y_.set_hexpand();
        z_.set_hexpand();
        // Register
        registerItem(name + ".x", x_);
        registerItem(name + ".y", y_);
        registerItem(name + ".z", z_);
        // Layout the selections
        attach(x_, 0, 0);
        attach(y_, 1, 0);
        attach(z_, 2, 0);
    }
    NamedTripleSelection(const std::string& name,
                         const std::initializer_list<const char*>& values,
                         size_t numColsRight = 3) :
            numColsRight_(numColsRight) {
        // Initialise them
        nameLabel_.set_label(name);
        nameLabel_.get_style_context()->add_class("namedentryaligned");
        nameLabel_.set_halign(Gtk::ALIGN_END);
        for(auto& v : values) {
            x_.append(v);
            y_.append(v);
            z_.append(v);
        }
        x_.set_hexpand();
        y_.set_hexpand();
        z_.set_hexpand();
        // Register
        registerItem(name + ".x", x_);
        registerItem(name + ".y", y_);
        registerItem(name + ".z", z_);
        // Layout the selections
        attach(x_, 0, 0);
        attach(y_, 1, 0);
        attach(z_, 2, 0);
    }

    // Accessors
    virtual void value(const box::Vector<T>& value) {
        x_.set_active(static_cast<int>(value.x()));
        y_.set_active(static_cast<int>(value.y()));
        z_.set_active(static_cast<int>(value.z()));
    }
    virtual box::Vector<T> value() const {
        return {static_cast<T>(x_.get_active_row_number()),
                static_cast<T>(y_.get_active_row_number()),
                static_cast<T>(z_.get_active_row_number())};
    }
    Gtk::ComboBoxText& x() { return x_; }
    Gtk::ComboBoxText& y() { return y_; }
    Gtk::ComboBoxText& z() { return z_; }

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void connectChangeHandler(const sigc::slot<void>& slot) override {
        if(curConnectionX_.connected()) {
            curConnectionX_.disconnect();
        }
        if(curConnectionY_.connected()) {
            curConnectionY_.disconnect();
        }
        if(curConnectionZ_.connected()) {
            curConnectionZ_.disconnect();
        }
        curConnectionX_ = x_.signal_changed().connect(slot);
        curConnectionY_ = y_.signal_changed().connect(slot);
        curConnectionZ_ = z_.signal_changed().connect(slot);
    }
    Gtk::Widget* leftWidget() override { return &nameLabel_; }
    size_t rightColumns() const override { return numColsRight_; }

protected:
    // Members
    Gtk::Label nameLabel_;  // The name attached to the value
    Gtk::ComboBoxText x_;  // The x selection
    Gtk::ComboBoxText y_;  // The y selection
    Gtk::ComboBoxText z_;  // The z selection
    sigc::connection curConnectionX_;  // The current connection
    sigc::connection curConnectionY_;  // The current connection
    sigc::connection curConnectionZ_;  // The current connection
    size_t numColsRight_;  // Number of columns occupied by the selection widgets
};

#endif //FDTDLIFE_NAMEDSELECTION_H
