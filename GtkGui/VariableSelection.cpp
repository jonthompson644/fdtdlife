/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableSelection.h"
#include <Fdtd/Variables.h>
#include "FdtdLife.h"

// Constructor
VariableSelection::VariableSelection(const std::string &name) :
    NamedSelection(name, {}){
}

// Second stage constructor.  Connects the database to the widget
void VariableSelection::construct(fdtd::Variables *variables) {
    variables_ = variables;
    Notifiable::construct(variables_->m());
    signal_changed().connect(sigc::mem_fun(
            *this, &VariableSelection::onSelectionChange));
    updateVariables();
}

// Update the variables list
void VariableSelection::updateVariables() {
    InitialisingGui::Set s(initialising_);
    // Which variable is currently selected?
    int current = value();
    // Now re-fill the list
    remove_all();
    rowToId_.clear();
    int row = 0;
    // The no variable entry
    append("<< None >>");
    rowToId_[row++] = -1;
    for(auto& variable : *variables_) {
        append(variable.name());
        rowToId_[row++] = variable.identifier();
    }
    // Reselect the variable
    value(current);
}

// Return the identifier of the selected variable
int VariableSelection::value() const {
    int result = -1;
    int row = get_active_row_number();
    auto pos = rowToId_.find(row);
    if(pos != rowToId_.end()) {
        result = pos->second;
    }
    return result;
}

// Select the specified variable
void VariableSelection::value(int id) {
    // Find the row with this id
    int row = 0;
    for(auto& item : rowToId_) {
        if(item.second == id) {
            row = item.first;
            break;
        }
    }
    // Select the row
    set_active(row);
}

// Connect the change handler
void VariableSelection::connectChangeHandler(const sigc::slot<void>& slot) {
    if(curConnection_.connected()) {
        curConnection_.disconnect();
    }
    curConnection_ = changedSignal_.connect(slot);
}

// Handler for a change notification from the combo box
void VariableSelection::onSelectionChange() {
    if(!initialising_) {
        changedSignal_.emit();
    }
}

// Handle model change notifications
void VariableSelection::notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) {
    // Base class first
    Notifiable::notify(source, why, data);
    // Update the variables list
    if(why == FdtdLife::notifyVariableChange) {
        updateVariables();
    }
}
