/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETEREXTRACTORDLG_H
#define FDTDLIFE_PARAMETEREXTRACTORDLG_H

#include "DesignersDlg.h"
#include <Designer/ParameterExtractor.h>
#include <Designer/HfssSParameter.h>
#include "NamedSelection.h"
#include "HorizontalLayout.h"
#include "NamedListView.h"
#include "NamedEntry.h"
#include "NamedDataSeries.h"

// A dialog panel for the parameter extractor
class ParameterExtractorDlg : public DesignersDlg::SubDlg {
public:
    // Construction
    ParameterExtractorDlg() = default;
    void construct(DesignersDlg* owner, designer::Designer* designer) override;

    // Overrides of SubDlg
    void populate() override;

protected:
    // Members
    designer::ParameterExtractor* parameterExtractor_{nullptr};

    // Parameters list view columns
    class ParametersColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        ParametersColumns() {
            add(colIdentifier_);
            add(colHeader_);
            add(colUnitCell_);
            add(colPatchRatio_);
            add(colIncidenceAngle_);
        }
        // Columns
        Gtk::TreeModelColumn<size_t> colIdentifier_;
        Gtk::TreeModelColumn<std::string> colHeader_;
        Gtk::TreeModelColumn<double> colUnitCell_;
        Gtk::TreeModelColumn<double> colPatchRatio_;
        Gtk::TreeModelColumn<double> colIncidenceAngle_;
    } parametersColumns_;

    // Parameters list view columns
    class DetailsColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        DetailsColumns() {
            add(colFrequency_);
            add(colS11_);
            add(colS12_);
            add(colPermittivity_);
            add(colPermeability_);
            add(colRefIndex_);
        }
        // Columns
        Gtk::TreeModelColumn<double> colFrequency_;
        Gtk::TreeModelColumn<std::string> colS11_;
        Gtk::TreeModelColumn<std::string> colS12_;
        Gtk::TreeModelColumn<std::string> colPermittivity_;
        Gtk::TreeModelColumn<std::string> colPermeability_;
        Gtk::TreeModelColumn<std::string> colRefIndex_;
    } detailsColumns_;

    // The widgets
    NamedEntry<double> layerSpacing_{"Layer Spacing (m)"};
    NamedEntry<size_t> numLayers_{"Number of Layers"};
    NamedEntry<double> vacuumGap_{"Vacuum Gap (m)"};
    NamedEntry<double> basePermittivity_{"Dielectric Permittivty"};
    NamedButton pasteHfss_{"Paste HFSS Data"};
    NamedButton calculate_{"Calculate"};
    NamedButton makeParams_{"Make Parameters"};
    NamedButton deleteTable_{"Delete Table"};
    HorizontalLayout line0_{{pasteHfss_, calculate_, makeParams_, deleteTable_,
                             basePermittivity_}};
    HorizontalLayout line1_{{layerSpacing_, numLayers_, vacuumGap_}};
    NamedListView parameters_{"S Parameters"};
    NamedDataSeries plot_{std::string("Plot")};
    HorizontalLayout line2_{{parameters_, plot_}};
    NamedListView details_{"Details"};

    // Handlers
    void onPasteHfss();
    void onSelectParameter();
    void onCalculate();
    void onParameterChanged(const Gtk::TreeModel::Path& path,
                            const Gtk::TreeModel::iterator& pos);
    void onMakeParams();
    void onDeleteTable();

    // Helpers
    void fillParameters(designer::HfssSParameter* sel = nullptr);
    void fillDetails();
    designer::HfssSParameter* currentParameter();
    void fillPlot();
};


#endif //FDTDLIFE_PARAMETEREXTRACTORDLG_H
