/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PhaseShiftViewItemDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void PhaseShiftViewItemDlg::construct(ViewDlg* owner, ViewItem* item) {
    item_ = dynamic_cast<PhaseShiftViewItem*>(item);
    DataSeriesViewItemDlg::construct(owner, item);
    comment("PhaseShiftViewItemDlg");
}

// Add the lines to the dialog.  Overridden by the derived classes to taylor their displays.
void PhaseShiftViewItemDlg::addLinesToDialog() {
    addLine({xScaleType_});
    DataSeriesViewItemDlg::addLinesToDialog();
}

// Populate all the widgets with values
void PhaseShiftViewItemDlg::populate() {
    DataSeriesViewItemDlg::populate();
    InitialisingGui::Set s(initialising_);
    xScaleType_.value(item_->xScaleType());
}

// Default change handler
void PhaseShiftViewItemDlg::onChange() {
    DataSeriesViewItemDlg::onChange();
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newXScaleType = c.test(xScaleType_, item_->xScaleType(),
                                    FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            item_->xScaleType(newXScaleType);
            // Tell others
            dlg_->dlg()->model().doNotification(c.why());
        }
    }
}
