/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VIEWITEM_H
#define FDTDLIFE_VIEWITEM_H

#include <Sensor/SensorUser.h>
#include <Sensor/Sensor.h>
#include <gtkmm/hvbox.h>
#include <Xml/DomObject.h>
#include "GuiElement.h"

class ViewFrame;

// Base class for the items that make up the contents of a view
class ViewItem : public Gtk::VBox, public GuiElement, public sensor::SensorUser {
public:
    // Construction
    ViewItem() = default;
    virtual void construct(int mode, int id, ViewFrame* frame);

    // Serialisation
    virtual void writeConfig(xml::DomObject& root) const;
    virtual void readConfig(xml::DomObject& root);
    friend xml::DomObject& operator<<(xml::DomObject& o, const ViewItem& v) {
        v.writeConfig(o);
        return o;
    }
    friend xml::DomObject& operator>>(xml::DomObject& o, ViewItem& v) {
        v.readConfig(o);
        return o;
    }

    // API
    virtual void initialise() {}
    virtual void update() {}

    // Overrides of GuiElement
    void initialiseGuiElements(bool recursive) override;
    Gtk::Widget& widget() override { return *this; }

    // Getters
    int sensorId() const { return sensorId_; }
    const std::string& dataSpecName() const { return dataSpecName_; }
    int mode() const { return mode_; }
    int identifier() const { return identifier_; }
    bool vertical() const { return vertical_; }

    // Setters
    void sensorId(int v) { sensorId_ = v; }
    void dataSpecName(const std::string& v) { dataSpecName_ = v; }

protected:
    // Members
    ViewFrame* frame_{};  // The owner view
    int identifier_{};  // Unique identifier
    int mode_{};  // What kind of view item
    int sensorId_{};  // The connected sensorId
    std::string dataSpecName_;  // The name of the specification
    bool vertical_{false};  // Preferred direction for this item

    // Helpers
    sensor::DataSpec* dataSpec();
    sensor::Sensor* sensor();
};


#endif //FDTDLIFE_VIEWITEM_H
