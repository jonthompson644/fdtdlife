/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SHAPESDLG_H
#define FDTDLIFE_SHAPESDLG_H

#include "InitialisingGui.h"
#include "ItemsAndInfoLayout.h"
#include "FdtdLife.h"
#include "NamedButton.h"
#include "FactoryButton.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include "NamedMenuButton.h"
#include <Domain/Shape.h>

// The dialog panel showing shapes on the top level page's notebook
class ShapesDlg : public ItemsAndInfoLayout {
public:
    // Types
    typedef SubDialog<ShapesDlg, domain::Shape> SubDlg;

    // Construction
    ShapesDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // API
    void populate();

    // Accessors
    FdtdLife& model() { return model_; }
    Gtk::Window* parentWindow() { return parentWindow_; }

protected:
    // Export commands
    enum class ExportCmds {
        allHfss, allDxf
    };
    // Members
    FdtdLife& model_;
    InitialisingGui initialising_;
    ItemsAndInfoLayout::Columns columns_;
    FactoryButton<domain::Shape> newButton_{
            "New Shape", model_.d()->shapeFactory(),
            {domain::Domain::shapeModeDielectricLayer,
             domain::Domain::shapeModeSquarePlate,
             domain::Domain::shapeModeSquareHole,
             domain::Domain::shapeModeCuboidArray,
             domain::Domain::shapeModeFingerPlate,
             domain::Domain::shapeModeBinaryPlateNxN},
            FactoryButton<domain::Shape>::These::only};
    NamedButton deleteButton_{"Delete Shape"};
    NamedMenuButton<ExportCmds> exportButton_{
            "Export",
            {{"Write All HFSS...", ExportCmds::allHfss},
             {"Write All, DXF...", ExportCmds::allDxf}}};
    SubDialogFactory<SubDlg> dlgFactory_;
    Gtk::Window* parentWindow_;

    // Helpers
    void fillShapeList(domain::Shape* select = nullptr);

    // Handlers
    void onNewShape(int mode);
    void onDeleteShape();
    void onExport(ExportCmds cmd);
    void onItemChanged(const Gtk::ListStore::Row& row);
    void onItemSelected(const Gtk::ListStore::Row* row);
};


#endif //FDTDLIFE_SHAPESDLG_H
