/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDENTRY_H
#define FDTDLIFE_NAMEDENTRY_H

#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include "GuiElement.h"
#include <Box/Expression.h>

// A class that binds a label with an entry widget
template<class T>
class NamedEntry : public Gtk::Entry, public GuiElement {
public:
    // Construction
    NamedEntry() = delete;
    explicit NamedEntry(const std::string& name, bool readOnly = false,
                               size_t numColsRight = 1);

    // Accessors
    void value(T value);
    T value();
    bool readOnly() const { return readOnly_; }
    void readOnly(bool value);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void connectChangeHandler(const sigc::slot<void>& slot) override;
    Gtk::Widget* leftWidget() override { return &nameLabel_; }
    size_t rightColumns() const override { return numColsRight_; }

protected:
    // Members
    Gtk::Label nameLabel_;  // The name attached to the value
    sigc::connection curConnection_;  // The current connection
    bool readOnly_;  // The element is read only
    sigc::signal<void> changedSignal_;
    size_t numColsRight_;  // Number of columns occupied by the entry widget

    // Handlers
    void onStateFlags(Gtk::StateFlags previous);
    void onActivate();
};

template<>
class NamedEntry<bool> : public NamedEntry<std::string> {
public:
    NamedEntry() = delete;
    explicit NamedEntry(const std::string& name);
    using NamedEntry<std::string>::value;
    void value(bool value);
};

template<>
class NamedEntry<box::Expression> : public NamedEntry<std::string> {
public:
    NamedEntry() = delete;
    explicit NamedEntry(const std::string& name);
    using NamedEntry<std::string>::value;
    void value(const box::Expression& value);
    box::Expression value();
};


#endif //FDTDLIFE_NAMEDENTRY_H
