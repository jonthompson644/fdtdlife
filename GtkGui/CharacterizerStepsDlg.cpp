/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CharacterizerStepsDlg.h"
#include "CharacterizerDlg.h"
#include "ApplicationDlg.h"
#include <Seq/CharacterizerStep.h>
#include "FdtdLife.h"

// Second stage constructor used by factory
void CharacterizerStepsDlg::construct(seq::Characterizer* sequencer, CharacterizerDlg* dlg) {
    sequencer_ = sequencer;
    dlg_ = dlg;
    fdtd::Notifiable::construct(sequencer_->m());
    // Initialise widgets
    addLines({{steps_},
              {plots_}});
    steps_.setColumns(&stepsColumns_);
    steps_.appendColumn("Step", stepsColumns_.colStep_);
    steps_.appendColumn("Description", stepsColumns_.colDescription_);
    steps_.appendColumn("Status", stepsColumns_.colStatus_);
    steps_.set_hexpand(true);
    steps_.set_vexpand(true);
    // Connect handlers
    steps_.selection()->signal_changed().connect(
            sigc::mem_fun(*this, &CharacterizerStepsDlg::onStepSelected));
}

// Initialise the widgets
void CharacterizerStepsDlg::populate(seq::CharacterizerStep* sel) {
    if (sel == nullptr) {
        sel = currentStep().get();
    }
    {
        auto* runningStep = sequencer_->currentStep().get();
        InitialisingGui::Set s(initialising_);
        steps_.model()->clear();
        for (auto& step: sequencer_->steps()) {
            std::string stepStr = step->step().text();
            std::string statusStr = step->status(runningStep);
            std::string descriptionStr = sequencer_->stepDescription(step);
            auto model = steps_.model();
            auto rowp = model->append();
            auto& row = *rowp;
            row[stepsColumns_.colStep_] = stepStr;
            row[stepsColumns_.colStatus_] = statusStr;
            row[stepsColumns_.colDescription_] = descriptionStr;
            if (step.get() == sel) {
                steps_.selection()->select(row);
            }
        }
    }
    // Call the selected step handler now as it would be automatically called
    // while we had initialising activated.
    onStepSelected();
}

// The table selection has changed
void CharacterizerStepsDlg::onStepSelected() {
    if (!initialising_) {
        // Get the item
        auto item = currentStep();
        if (item != nullptr) {
            // Fill the model from the individual if we are not running
            if (dlg_->dlg()->dlg()->isStopped()) {
                dlg_->dlg()->dlg()->setStopped();
                sequencer_->loadStepIntoModel(item);
                dlg_->dlg()->model().initialise();
                sequencer_->updateFrequencies();
                dlg_->dlg()->model().doNotification(FdtdLife::notifyIndividualLoaded);
            }
            // Fill the item part of the display
            plotItemData(item);
        } else {
            transmittance_.clear();
            phaseShift_.clear();
        }
    }
}

// Return the current scanner
std::shared_ptr<seq::CharacterizerStep> CharacterizerStepsDlg::currentStep() {
    std::shared_ptr<seq::CharacterizerStep> result = nullptr;
    auto rowPos = steps_.curSelection();
    if (rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        seq::ScannerPos step(selection[stepsColumns_.colStep_]);
        result = sequencer_->getStep(step);
    }
    return result;
}

// Place the items's data into the data displays
void CharacterizerStepsDlg::plotItemData(const std::shared_ptr<seq::CharacterizerStep>& item) {
    double frequencySpacing = sequencer_->frequencies().spacing();
    transmittance_.data("X", item->data().transmittance(), true);
    transmittance_.axisX(frequencySpacing / box::Constants::giga_,
                         frequencySpacing / box::Constants::giga_,
                         item->data().transmittance().size(), "GHz");
    transmittance_.axisY(0.0, 1.0, "");
    phaseShift_.data("X", item->data().phaseShift(), true);
    phaseShift_.axisX(frequencySpacing / box::Constants::giga_,
                      frequencySpacing / box::Constants::giga_,
                      item->data().phaseShift().size(), "GHz");
    phaseShift_.axisY(-180.0, 180.0, "deg");
    if(item->data().xyComponents()) {
        transmittance_.data("Y", item->data().transmittanceY(), true);
        phaseShift_.data("Y", item->data().phaseShiftY(), true);
    }
}

// Handle a configuration change notification
void CharacterizerStepsDlg::notify(fdtd::Notifier* source, int why,
                                   fdtd::NotificationData* /*data*/) {
    if (source == sequencer_->m()) {
        switch (why) {
            case FdtdLife::notifySequencerChange:
            case FdtdLife::notifyRunComplete:
                populate();
            default:
                break;
        }
    }
}
