/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CHARACTERIZERPARAMETERSDLG_H
#define FDTDLIFE_CHARACTERIZERPARAMETERSDLG_H

#include "GridLayout.h"
#include <Seq/Characterizer.h>
#include "InitialisingGui.h"
#include "NamedListView.h"
#include "NamedButton.h"
#include "NamedMenuButton.h"
#include "VerticalLayout.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include "FactoryButton.h"
#include "FramedLayout.h"

class CharacterizerDlg;

// The parameters sub-panel of the characterizer dialog
class CharacterizerParametersDlg : public GridLayout
{
public:
    // Types
    using SubDlg = SubDialog<CharacterizerParametersDlg, seq::Scanner>;

    // Construction
    CharacterizerParametersDlg() = default;
    void construct(seq::Characterizer* sequencer, CharacterizerDlg* dlg);

    // API
    void populate(seq::Scanner* sel = nullptr);
    CharacterizerDlg* dlg() const { return dlg_; }

protected:
    // Handlers
    void onScannerSelected();
    void onNewScanner(seq::Characterizer::ScannerMode mode);

    // Members
    seq::Characterizer* sequencer_{};
    CharacterizerDlg* dlg_{};
    InitialisingGui initialising_;
    SubDialogFactory<SubDlg, seq::Characterizer::ScannerMode> dlgFactory_;
    std::unique_ptr<SubDlg> subDialog_;

    // Scanners list view columns
    class ScannersColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        ScannersColumns() {
            add(colIdentifier_);
            add(colType_);
            add(colSteps_);
        }
        // Columns
        Gtk::TreeModelColumn<int> colIdentifier_;
        Gtk::TreeModelColumn<std::string> colType_;
        Gtk::TreeModelColumn<size_t> colSteps_;
    } scannersColumns_;

    // Widgets
    NamedListView scanners_{"Scanners"};
    std::unique_ptr<FactoryButton<seq::Scanner, seq::Characterizer::ScannerMode>> newScanner_;
    NamedButton deleteScanner_{"Delete Scanner"};
    FramedLayout subDialogFrame_{"Scanner"};
    VerticalLayout leftHandSide_;

    // Helpers
    seq::Scanner* currentScanner();
};


#endif //FDTDLIFE_CHARACTERIZERPARAMETERSDLG_H
