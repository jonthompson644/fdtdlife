/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "BinaryPatternWidget.h"
#include <iostream>

// Constructor
BinaryPatternWidget::BinaryPatternWidget(bool fourFold, bool readOnly) :
        fourFold_(fourFold),
        readOnly_(readOnly) {
    set_vexpand(true);
    set_hexpand(true);
    set_events(static_cast<Gdk::EventMask>(
                       static_cast<unsigned int>(get_events()) | GDK_BUTTON_PRESS_MASK));
    pattern2Fold_.n(bitsPerHalfSide_);
}

// Draw the binary pattern widget
bool BinaryPatternWidget::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
    // Set up the mapping
    auto allocation = get_allocation();
    int width = allocation.get_width();
    int height = allocation.get_height();
    pixelsPerBit_ = std::min(width, height) / 2 / bitsPerHalfSide_;
    rectPixels_ = bitsPerHalfSide_ * 2 * pixelsPerBit_;
    // Draw the pixels
    for(size_t i = 0; i < bitsPerHalfSide_; i++) {
        for(size_t j = 0; j < bitsPerHalfSide_; j++) {
            // Select the brush
            bool on = pattern2Fold_.get(i, j);
            if(on) {
                cr->set_source_rgb(0.0, 0.0, 0.0);
            } else {
                cr->set_source_rgb(1.0, 1.0, 1.0);
            }
            // Top left
            cr->rectangle(static_cast<int>(i * pixelsPerBit_),
                          static_cast<int>(j * pixelsPerBit_),
                          pixelsPerBit_, pixelsPerBit_);
            cr->fill();
            // Top right
            cr->rectangle(static_cast<int>((2 * bitsPerHalfSide_ - 1 - i) * pixelsPerBit_),
                          static_cast<int>(j * pixelsPerBit_),
                          pixelsPerBit_, pixelsPerBit_);
            cr->fill();
            // Bottom left
            cr->rectangle(static_cast<int>(i * pixelsPerBit_),
                          static_cast<int>((2 * bitsPerHalfSide_ - 1 - j) * pixelsPerBit_),
                          pixelsPerBit_, pixelsPerBit_);
            cr->fill();
            // Bottom right
            cr->rectangle(static_cast<int>((2 * bitsPerHalfSide_ - 1 - i) * pixelsPerBit_),
                          static_cast<int>((2 * bitsPerHalfSide_ - 1 - j) * pixelsPerBit_),
                          pixelsPerBit_, pixelsPerBit_);
            cr->fill();
        }
    }
    // Draw the grid lines
    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->set_line_width(1);
    for(size_t i = 0; i <= bitsPerHalfSide_ * 2; i++) {
        cr->move_to(0, static_cast<int>(i * pixelsPerBit_) + 0.5);
        cr->line_to(rectPixels_ + 0.5, static_cast<int>(i * pixelsPerBit_) + 0.5);
    }
    cr->stroke();
    for(size_t j = 0; j <= bitsPerHalfSide_ * 2; j++) {
        cr->move_to(static_cast<int>(j * pixelsPerBit_) + 0.5, 0);
        cr->line_to(static_cast<int>(j * pixelsPerBit_) + 0.5, rectPixels_ + 0.5);
    }
    cr->stroke();
    return true;
}

// Override of the button press handler
bool BinaryPatternWidget::on_button_press_event(GdkEventButton* button_event) {
    if(button_event->x >= 0 && button_event->x < rectPixels_ &&
       button_event->y >= 0 && button_event->y < rectPixels_) {
        // Raw bit coordinates
        size_t bitX = button_event->x / pixelsPerBit_;
        size_t bitY = button_event->y / pixelsPerBit_;
        // Translate to top left corner
        if(bitX >= bitsPerHalfSide_) {
            bitX = bitsPerHalfSide_ * 2 - 1 - bitX;
        }
        if(bitY >= bitsPerHalfSide_) {
            bitY = bitsPerHalfSide_ * 2 - 1 - bitY;
        }
        // Flip the bit
        bool oldBit = pattern2Fold_.get(bitX, bitY);
        pattern2Fold_.set(bitX, bitY, !oldBit);
        // Flip the four-fold bit if necessary
        if(fourFold_ && bitX != bitY) {
            pattern2Fold_.set(bitY, bitX, !oldBit);
        }
        // Cause a redraw
        queue_draw();
        // Send the signal
        changedSignal_.emit();
    }
    return true;
}

// Clear the current pattern
void BinaryPatternWidget::clear() {
    pattern2Fold_.clear();
    queue_draw();
}

// Set the bits per half side.  This has the side effect of clearing the
// current pattern.
void BinaryPatternWidget::bitsPerHalfSide(size_t v) {
    bitsPerHalfSide_ = v;
    pattern2Fold_.n(v);
    clear();
}

// Set the pattern
void BinaryPatternWidget::pattern(const box::BinaryPattern& v) {
    pattern2Fold_ = v;
    if(v.n() != bitsPerHalfSide_) {
        bitsPerHalfSide(v.n());
    }
    pattern2Fold_ = v;
    queue_draw();
}

