/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "MaterialsSelectDlg.h"
#include <Domain/Material.h>

// Constructor
MaterialsSelectDlg::MaterialsSelectDlg(Gtk::Window* parentWindow, const std::string& name,
                                       domain::Domain* domain, TestSuite* testSuite) :
        Dialog(name, parentWindow),
        domain_(domain),
        testSuite_(testSuite) {
    registerChild(layout_);
    add_button("Cancel", Gtk::RESPONSE_CANCEL);
    add_button("OK", Gtk::RESPONSE_OK);
    set_default_response(Gtk::RESPONSE_OK);
    for(auto& mat : domain_->materials()) {
        materials_.emplace_back(std::make_pair(
                std::make_unique<NamedToggleButton>(mat->name()), mat->index()));
        layout_.addLine({*materials_.back().first});
    }
    MaterialsSelectDlg::initialiseGuiElements(true);
    show_all_children();
}

// Initialise the GUI elements
void MaterialsSelectDlg::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    get_content_area()->pack_start(layout_);
}

// Ask the user for the information
bool MaterialsSelectDlg::ask(std::set<int>& selected) {
    int result;
    if(testSuite_ != nullptr) {
        // Running under the test suite, ask it instead
        result = Gtk::RESPONSE_CANCEL;
    } else {
        // Running normally, do the modal dialog
        result = run();
        selected.clear();
        for(auto& material : materials_) {
            if(material.first->get_active()) {
                selected.insert(material.second);
            }
        }
    }
    return result == Gtk::RESPONSE_OK;
}
