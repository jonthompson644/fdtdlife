/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_TWODSLICEWIDGET_H
#define FDTDLIFE_TWODSLICEWIDGET_H

#include "CustomWidget.h"
#include "Rgb.h"

class TwoDSliceWidget : public CustomWidget {
public:
    // Types
    enum class ColourMap {
        extKindlemann, divergingBY, material
    };

    // Construction
    TwoDSliceWidget() = default;

    // API
    void data(size_t xSize, size_t ySize, const std::vector<double>& d);
    void config(double displayMax, ColourMap colourMap);

    // Overrides of CustomWidget
    void draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) override;

protected:
    // Members
    std::vector<double> data_;
    size_t xSize_{};
    size_t ySize_{};
    double displayMax_{1.0};
    ColourMap colourMap_{ColourMap::extKindlemann};

    // Drawing information
    double pixelsPerCell_{};
    bool drawCellBorders_{};
    double topLeftX_{};
    double topLeftY_{};
    size_t numColours_{};
    std::vector<Rgb> colours_;
    double scale_{};
    double offset_{};

    // Helpers
    bool dataValid();
    static void drawBackground(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void calcScaling(int width, int height);
    void drawData(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void loadColours();
};


#endif //FDTDLIFE_TWODSLICEWIDGET_H
