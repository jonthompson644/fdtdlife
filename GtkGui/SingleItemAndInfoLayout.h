/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SINGLEITEMANDINFOLAYOUT_H
#define FDTDLIFE_SINGLEITEMANDINFOLAYOUT_H

#include <gtkmm/grid.h>
#include <gtkmm/scrolledwindow.h>
#include "GuiElement.h"
#include "VertButtonLayout.h"
#include "FramedLayout.h"
#include "ListWidget.h"
#include "GridLayout.h"
#include "NamedRadioButton.h"
#include "HorizontalSeparator.h"

// A layout that has item selection radio buttons and possible some push buttons on the left
// and a panel showing the item details dialog on the right.  The right hand
// dialog is created dynamically based on the type of the item.
class SingleItemAndInfoLayout : public Gtk::Grid, public GuiElement {
public:
    // Construction
    explicit SingleItemAndInfoLayout(const std::string& name,
            const std::vector<std::reference_wrapper<GuiElement>>& buttons = {});

    // API
    void setButtons(const std::vector<std::reference_wrapper<GuiElement>>& buttons);
    void setModeNames(const std::list<std::pair<int,std::string>>& modeNames);
    const std::string& currentMode() { return currentMode_; }
    void currentMode(const std::string& modeName);
    void setSubDialog(GridLayout* subDialog);
    GridLayout* subDialog() { return subDialog_.get(); }

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void initialiseGuiElements(bool recursive) override;

    // Signals
    sigc::signal<void, const std::string&>& modeSelectedSignal() {
        return modeSelectedSignal_;
    };

protected:
    // Members
    Gtk::Grid buttonBox_;
    std::vector<std::string> modeNames_;
    sigc::signal<void, const std::string&> modeSelectedSignal_;
    Gtk::Frame subDialogFrame_;
    std::unique_ptr<GridLayout> subDialog_;
    NamedRadioButton noneButton_{"<<None>>"};
    std::vector<NamedRadioButton> modeButtons_;
    std::string currentMode_;
    std::vector<std::reference_wrapper<GuiElement>> extraButtons_;
    HorizontalSeparator separator_;

    // Handlers
    void onModeClicked(size_t index);
    void onNoneClicked();
};

#endif //FDTDLIFE_SINGLEITEMANDINFOLAYOUT_H
