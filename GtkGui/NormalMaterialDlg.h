/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NORMALMATERIALDLG_H
#define FDTDLIFE_NORMALMATERIALDLG_H

#include "MaterialsDlg.h"
#include <Domain/Material.h>
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"

// A dialog panel that shows the normal material parameters
class NormalMaterialDlg : public MaterialsDlg::SubDlg {
public:
    // Constructor
    NormalMaterialDlg() = default;
    void construct(MaterialsDlg* owner, domain::Material* material) override;

    // API
    void populate() override;
    virtual void initLayout();

protected:
    // Members
    domain::Material* material_{};

    // The widgets
    NamedSelection<box::Constants::Colour> colour_{
            "Display Colour", box::Constants::colourNames_};
    NamedEntry<int> priority_{"Priority"};
    NamedEntry<box::Expression> epsilon_{"Epsilon"};
    NamedEntry<double> relativeEpsilon_{"Relative Epsilon"};
    NamedEntry<box::Expression> mu_{"Mu"};
    NamedEntry<double> relativeMu_{"Relative Mu"};
    NamedEntry<box::Expression> sigma_{"Sigma"};
    NamedEntry<box::Expression> sigmaStar_{"Sigma*"};

    // Handlers
    virtual void onChange();
    void onRelativeEpsilonChange();
    void onRelativeMuChange();
};


#endif //FDTDLIFE_NORMALMATERIALDLG_H
