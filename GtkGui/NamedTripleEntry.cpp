/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NamedTripleEntry.h"
#include <Box/Utility.h>

// Constructor
template<class T>
NamedTripleEntry<T>::NamedTripleEntry(const std::string& name, bool readOnly,
                                      size_t numColsRight) :
        readOnly_(readOnly),
        numColsRight_(numColsRight) {
    // Layout the components
    attach(valueXEntry_, 0, 0);
    attach(valueYEntry_, 1, 0);
    attach(valueZEntry_, 2, 0);
    set_hexpand();
    // Initialise them
    nameLabel_.set_label(name);
    nameLabel_.get_style_context()->add_class("namedentry");
    nameLabel_.set_halign(Gtk::ALIGN_END);
    valueXEntry_.set_editable(!readOnly_);
    valueYEntry_.set_editable(!readOnly_);
    valueZEntry_.set_editable(!readOnly_);
    valueXEntry_.set_can_focus(!readOnly_);
    valueYEntry_.set_can_focus(!readOnly_);
    valueZEntry_.set_can_focus(!readOnly_);
    valueXEntry_.set_hexpand();
    valueYEntry_.set_hexpand();
    valueZEntry_.set_hexpand();
    // Register
    registerItem(name + ".x", valueXEntry_);
    registerItem(name + ".y", valueYEntry_);
    registerItem(name + ".z", valueZEntry_);
    // Connect change detector handlers
    valueXEntry_.signal_activate().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onActivate));
    valueYEntry_.signal_activate().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onActivate));
    valueZEntry_.signal_activate().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onActivate));
    valueXEntry_.signal_state_flags_changed().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onStateFlagsX));
    valueYEntry_.signal_state_flags_changed().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onStateFlagsY));
    valueZEntry_.signal_state_flags_changed().connect(
            sigc::mem_fun(this, &NamedTripleEntry::onStateFlagsZ));
}

// Set the value
template<class T>
void NamedTripleEntry<T>::value(const box::Vector<T>& v) {
    std::stringstream sx;
    std::stringstream sy;
    std::stringstream sz;
    sx << v.x();
    sy << v.y();
    sz << v.z();
    valueXEntry_.set_text(sx.str());
    valueYEntry_.set_text(sy.str());
    valueZEntry_.set_text(sz.str());
}

// Get the value
template<class T>
box::Vector<T> NamedTripleEntry<T>::value() {
    std::stringstream sx(valueXEntry_.get_text());
    std::stringstream sy(valueYEntry_.get_text());
    std::stringstream sz(valueZEntry_.get_text());
    T x;
    T y;
    T z;
    sx >> x;
    sy >> y;
    sz >> z;
    value({x, y, z});
    return {x, y, z};
}

// Attach a change handler
template<class T>
void NamedTripleEntry<T>::connectChangeHandler(const sigc::slot<void>& slot) {
    if(!readOnly_) {
        if(curConnection_.connected()) {
            curConnection_.disconnect();
        }
        curConnection_ = changedSignal_.connect(slot);
    }
}

// Handles the activation signal
template<class T>
void NamedTripleEntry<T>::onActivate() {
    changedSignal_.emit();
}

// Handles state flag notifications for X
template<class T>
void NamedTripleEntry<T>::onStateFlagsX(Gtk::StateFlags previous) {
    if((valueXEntry_.get_state_flags() & GTK_STATE_FLAG_FOCUSED) == 0 &&
       (previous & GTK_STATE_FLAG_FOCUSED) != 0) {
        changedSignal_.emit();
    }
}

// Handles state flag notifications for Y
template<class T>
void NamedTripleEntry<T>::onStateFlagsY(Gtk::StateFlags previous) {
    if((valueYEntry_.get_state_flags() & GTK_STATE_FLAG_FOCUSED) == 0 &&
       (previous & GTK_STATE_FLAG_FOCUSED) != 0) {
        changedSignal_.emit();
    }
}

// Handles state flag notifications for Z
template<class T>
void NamedTripleEntry<T>::onStateFlagsZ(Gtk::StateFlags previous) {
    if((valueZEntry_.get_state_flags() & GTK_STATE_FLAG_FOCUSED) == 0 &&
       (previous & GTK_STATE_FLAG_FOCUSED) != 0) {
        changedSignal_.emit();
    }
}

// Instantiate particular types to make linking work
template
class NamedTripleEntry<double>;

template
class NamedTripleEntry<long>;

template
class NamedTripleEntry<size_t>;

template
class NamedTripleEntry<std::string>;

// Constructor for box::VectorExpression<double> specialisation
NamedTripleEntry<box::VectorExpression<double>>::NamedTripleEntry(
        const std::string& name) :
        NamedTripleEntry<std::string>(name, false) {
}

// Set the value for box::expression specialisation
void NamedTripleEntry<box::VectorExpression<double>>::value(
        const box::VectorExpression<double>& value) {
    valueXEntry_.set_text(value.x().text());
    valueYEntry_.set_text(value.y().text());
    valueZEntry_.set_text(value.z().text());
    valueXEntry_.set_tooltip_text(box::Utility::fromDouble(value.x().value()));
    valueYEntry_.set_tooltip_text(box::Utility::fromDouble(value.y().value()));
    valueZEntry_.set_tooltip_text(box::Utility::fromDouble(value.z().value()));
}

// Get the value for box::expression specialisation
box::VectorExpression<double> NamedTripleEntry<box::VectorExpression<double>>::value() {
    return box::VectorExpression<double>(valueXEntry_.get_text(),
                                         valueYEntry_.get_text(),
                                         valueZEntry_.get_text());
}

// Constructor for box::VectorExpression<size_t> specialisation
NamedTripleEntry<box::VectorExpression<size_t>>::NamedTripleEntry(
        const std::string& name) :
        NamedTripleEntry<std::string>(name, false) {
}

// Set the value for box::expression specialisation
void NamedTripleEntry<box::VectorExpression<size_t>>::value(
        const box::VectorExpression<size_t>& value) {
    valueXEntry_.set_text(value.x().text());
    valueYEntry_.set_text(value.y().text());
    valueZEntry_.set_text(value.z().text());
    valueXEntry_.set_tooltip_text(box::Utility::fromSizeT(value.x().value()));
    valueYEntry_.set_tooltip_text(box::Utility::fromSizeT(value.y().value()));
    valueZEntry_.set_tooltip_text(box::Utility::fromSizeT(value.z().value()));
}

// Get the value for box::expression specialisation
box::VectorExpression<size_t> NamedTripleEntry<box::VectorExpression<size_t>>::value() {
    return box::VectorExpression<size_t>(valueXEntry_.get_text(),
                                         valueYEntry_.get_text(),
                                         valueZEntry_.get_text());
}
