/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ViewDlg.h"
#include "GuiChangeDetector.h"
#include "DataSeriesViewItemDlg.h"
#include "PhaseShiftViewItemDlg.h"
#include "AttenuationViewItemDlg.h"
#include "TwoDSliceViewItemDlg.h"
#include <Sensor/Sensor.h>

// Second stage constructor used by the factory
void ViewDlg::construct(ViewsDlg* owner, ViewFrame* view) {
    view_ = view;
    SubDialog::construct(owner, view);
    Notifiable::construct(&dlg_->model());
    // The sub-dialog factory
    itemDlgFactory_.define(new box::Factory<SubDlg>::Creator<TwoDSliceViewItemDlg>(
            sensor::DataSpec::modeTwoD));
    itemDlgFactory_.define(new box::Factory<SubDlg>::Creator<DataSeriesViewItemDlg>(
            sensor::DataSpec::modeTimeSeries));
    itemDlgFactory_.define(new box::Factory<SubDlg>::Creator<PhaseShiftViewItemDlg>(
            sensor::DataSpec::modePhaseShift));
    itemDlgFactory_.define(new box::Factory<SubDlg>::Creator<AttenuationViewItemDlg>(
            sensor::DataSpec::modeAttenuation));
    // Initialise
    addLines({{minDisplayRate_, displayAllFrames_},
              {line2_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &ViewDlg::onChange));
    sensorDataSources_.selectedSignal().connect(
            sigc::mem_fun(*this, &ViewDlg::onDataSourceSelection));
    sensorDataSources_.changedSignal().connect(
            sigc::mem_fun(*this, &ViewDlg::onDataSourceChange));
}

// Handle a notification
void ViewDlg::notify(fdtd::Notifier* /*source*/, int why,
                     fdtd::NotificationData* /*data*/) {
    if(why == FdtdLife::notifySensorChange || why == FdtdLife::notifyDomainContentsChange) {
        fillDataSources();
    }
}

// Populate all the widgets with values
void ViewDlg::populate() {
    ViewsDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    minDisplayRate_.value(view_->frameDisplayPeriod());
    displayAllFrames_.value(view_->displayAllFrames());
    fillDataSources();
}

// Default change handler
void ViewDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newMinDisplayRate = c.test(minDisplayRate_,
                                        view_->frameDisplayPeriod(),
                                        FdtdLife::notifyMinorChange);
        auto newDisplayAllFrames = c.test(displayAllFrames_,
                                          view_->displayAllFrames(),
                                          FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            view_->frameDisplayPeriod(newMinDisplayRate);
            view_->displayAllFrames(newDisplayAllFrames);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Fill the list of data specifications
void ViewDlg::fillDataSources() {
    InitialisingGui::Set s(initialising_);
    sensorDataSources_.clear();
    for(auto& sensor : dlg_->model().sensors()) {
        for(auto& spec : sensor->dataSpecs()) {
            bool checked = view_->viewForSpec(sensor->identifier(), spec->name());
            sensorDataSources_.addCheckbox(sensor->name() + ":" + spec->name(),
                                           checked, sensor->identifier());
        }
    }
}

// The data source selection has changed
void ViewDlg::onDataSourceSelection(bool checked, int identifier, const std::string& name) {
    if(!initialising_) {
        GridLayout* newDialog = nullptr;
        // Get the data spec name
        if(checked) {
            auto pos = name.find(':');
            if(pos >= 0) {
                std::string dataSpecName = name.substr(pos + 1);
                auto viewItem = view_->viewForSpec(identifier, dataSpecName);
                if(viewItem != nullptr) {
                    newDialog = itemDlgFactory_.make(this, viewItem);
                }
            }
        }
        subDialogFrame_.removeChild();
        if(newDialog != nullptr) {
            subDialogFrame_.addChild(*newDialog);
            subDialogFrame_.show_all_children();
        }
        subDialog_.reset(newDialog);
    }
}

// The data source checkbox has changed
void ViewDlg::onDataSourceChange(bool checked, int identifier, const std::string& name) {
    if(!initialising_) {
        // Get the data spec name
        auto pos = name.find(':');
        if(pos >= 0) {
            std::string dataSpec = name.substr(pos + 1);
            if(checked) {
                view_->addViewItem(identifier, dataSpec);
            } else {
                view_->removeViewItem(identifier, dataSpec);
            }
            onDataSourceSelection(checked, identifier, name);
        }
    }
}
