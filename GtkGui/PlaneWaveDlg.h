/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PLANEWAVEDLG_H
#define FDTDLIFE_PLANEWAVEDLG_H

#include <Source/PlaneWaveSource.h>
#include "SourcesDlg.h"
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"

// A dialog panel that shows plane wave source parameters
class PlaneWaveDlg : public SourcesDlg::SubDlg {
public:
    // Constructor
    PlaneWaveDlg() = default;
    void construct(SourcesDlg* owner, source::Source* source) override;

    // API
    void populate() override;

protected:
    // Members
    source::PlaneWaveSource* source_{};

    // The widgets
    NamedEntry<box::Expression> firstFrequency_{"First Frequency (Hz)"};
    NamedEntry<box::Expression> frequencyStep_{"Frequency Step (Hz)"};
    NamedEntry<box::Expression> numFrequencies_{"Number Of Frequencies"};
    NamedEntry<box::Expression> polarisation_{"Polarisation (-90..90 deg)"};
    NamedSelection<source::PlaneWaveSource::Distribution> distribution_{
            "Distribution",
            {"Flat", "Gaussian"}};
    NamedEntry<box::Expression> amplitude_{"E Field Amplitude (V/m)"};
    NamedEntry<box::Expression> azimuth_{"Azimuth (0..90 deg)"};
    NamedEntry<double> pmlConductivity_{"PML Conductivity"};
    NamedEntry<double> pmlConductivityFactor_{"PML Conductivity Factor"};
    NamedEntry<box::Expression> initialTime_{"Initial Time (s)"};
    NamedEntry<box::Expression> runFor_{"Run For (s)"};
    NamedToggleButton continuous_{"Continuous"};
    NamedEntry<double> gaussianB_{"Gaussian B"};
    NamedEntry<double> gaussianC_{"Gaussian C"};

    // Handlers
    void onChange();
};


#endif //FDTDLIFE_PLANEWAVEDLG_H
