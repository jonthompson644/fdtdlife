/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CHARACTERIZERDLG_H
#define FDTDLIFE_CHARACTERIZERDLG_H

#include "SequencersDlg.h"
#include "Seq/Characterizer.h"
#include "NotebookLayout.h"
#include "CharacterizerParametersDlg.h"
#include "CharacterizerStepsDlg.h"

// A dialog panel that shows the parameters for a single job
class CharacterizerDlg : public SequencersDlg::SubDlg
{
public:
    // Constructor
    CharacterizerDlg() = default;
    void construct(SequencersDlg* owner, seq::Sequencer* sequencer) override;

    // Overrides of SubDlg
    void populate() override;

    // API
    NotebookLayout& sections() { return notebook_; }

protected:
    // Members
    seq::Characterizer* sequencer_{};

    // The widgets
    CharacterizerParametersDlg parametersPage_;
    CharacterizerStepsDlg stepsPage_;
    NotebookLayout notebook_{{
                                     {"Parameters", parametersPage_},
                                     {"Steps", stepsPage_}}};
};


#endif //FDTDLIFE_CHARACTERIZERDLG_H
