/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "PhaseShiftViewItem.h"

// Write the configuration to an XML file
void PhaseShiftViewItem::writeConfig(xml::DomObject& root) const {
    // Base class first
    DataSeriesViewItem::writeConfig(root);
    // Now my things
    root << xml::Reopen();
    root << xml::Obj("xscaletype") << xScaleType_;
    root << xml::Close();
}

// Read the configuration from an XML file
void PhaseShiftViewItem::readConfig(xml::DomObject& root) {
    // Base class first
    DataSeriesViewItem::readConfig(root);
    // Now my things
    root >> xml::Reopen();
    root >> xml::Obj("xscaletype") >> xScaleType_;
    root >> xml::Close();
}

// Convert a data value according to the scale type
double PhaseShiftViewItem::convertXData(double value) {
    double result = value;
    switch(xScaleType_) {
        case XScaleType::frequency:
            break;
        case XScaleType::waveNumber:
            result = value * box::Constants::giga_ / box::Constants::c_ / 100.0;
            break;
    }
    return result;
}

// Convert the data according to current configuration
void PhaseShiftViewItem::convertData(int /*x*/, std::vector<double>& /*dy*/, double& dx) {
    dx = convertXData(dx);
}

// Fix the sensor spec information according to the current configuration
void PhaseShiftViewItem::fixInfo(sensor::DataSpec::Info& info) {
    info.xLeft_ = convertXData(info.xLeft_);
    info.xRight_ = convertXData(info.xRight_);
}
