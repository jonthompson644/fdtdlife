/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensAnalyserDlg.h"
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <utility>
#include "GuiChangeDetector.h"

// Constructor
LensAnalyserDlg::LensAnalyserDlg(std::string dataUnits) :
        dataUnits_(std::move(dataUnits)) {
}

// Second stage constructor used by the factory
void LensAnalyserDlg::construct(AnalysersDlg* owner, sensor::Analyser* analyser) {
    analyser_ = analyser;
    SubDialog::construct(owner, analyser);
    Notifiable::construct(&dlg_->model());
    sensor_.construct(&analyser->m()->sensors());
    addLines({{configLayout_}, {error_}, {sensorData_}});
    initialiseGuiElements(true);
    populate();
    sensorData_.clipboard(&owner->model().clipboard());
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &LensAnalyserDlg::onChange));
}

// Handle a notification
void LensAnalyserDlg::notify(fdtd::Notifier* source, int why,
                             fdtd::NotificationData* /*data*/) {
    if(source == &dlg_->model() && why == fdtd::Model::notifyAnalysersCalculate) {
        showData();
    }
}

// Populate all the widgets with values
void LensAnalyserDlg::populate() {
    AnalysersDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    sensor_.value(analyser_->sensorId());
    location_.value(analyser_->location());
    component_.value(analyser_->component());
    showData();
}

// Default change handler
void LensAnalyserDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newSensor = c.test(sensor_, analyser_->sensorId(),
                                FdtdLife::notifyMinorChange);
        auto newLocation = c.test(location_, analyser_->location(),
                                  FdtdLife::notifyMinorChange);
        auto newComponent = c.test(component_, analyser_->component(),
                                   FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            analyser_->sensorId(newSensor);
            analyser_->location(newLocation);
            analyser_->component(newComponent);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Show the data
void LensAnalyserDlg::showData() {
    error_.value(analyser_->error());
    sensorData_.axisY(analyser_->minY(), analyser_->maxY(), dataUnits_);
    sensorData_.axisX(analyser_->minX() / box::Constants::milli_,
                      analyser_->stepX() / box::Constants::milli_,
                      analyser_->data().size(), "mm");
    sensorData_.data("data", analyser_->data());
    sensorData_.data("reference", analyser_->reference());
}
