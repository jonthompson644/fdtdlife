/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Source/PlaneWaveSource.h>
#include <Fdtd/RecordedData.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include "ArraySensorDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factory
void ArraySensorDlg::construct(SensorsDlg* owner, sensor::Sensor* sensor) {
    SubDialog::construct(owner, sensor);
    comment("ArraySensor");
    sensor_ = dynamic_cast<sensor::ArraySensor*>(sensor);
    addLines({{colour_, currentFrequency_},
              {numPointsX_, numPointsY_},
              {averageOverCell_, includeReflectionData_},
              {animateFrequency_, frequencyStep_},
              {useWindow_, manualZ_},
              {transmittedZ_, reflectedZ_},
              {componentsLine_},
              {manualSamples_, numberOfSamples_},
              {copyToClipboard_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &ArraySensorDlg::onChange));
    copyToClipboard_.doSignal().connect(
            sigc::mem_fun(*this, &ArraySensorDlg::onCommand));
}

// Populate all the widgets with values
void ArraySensorDlg::populate() {
    SensorsDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    colour_.value(sensor_->colour());
    numPointsX_.value(sensor_->numPointsX());
    numPointsY_.value(sensor_->numPointsY());
    currentFrequency_.value(sensor_->frequency());
    averageOverCell_.value(sensor_->averageOverCell());
    includeReflectionData_.value(sensor_->includeReflected());
    useWindow_.value(sensor_->useWindow());
    animateFrequency_.value(sensor_->animateFrequency());
    frequencyStep_.value(sensor_->frequencyStep());
    manualZ_.value(sensor_->manualZPos());
    transmittedZ_.value(sensor_->transmittedZPos());
    transmittedZ_.readOnly(!sensor_->manualZPos());
    reflectedZ_.value(sensor_->reflectedZPos());
    reflectedZ_.readOnly(!sensor_->manualZPos());
    componentX_.value(sensor_->componentSel().x());
    componentY_.value(sensor_->componentSel().y());
    componentZ_.value(sensor_->componentSel().z());
    manualSamples_.value(sensor_->manualSamples());
    numberOfSamples_.value(sensor_->nSamples());
    numberOfSamples_.readOnly(!sensor_->manualSamples());
}

// Default change handler
void ArraySensorDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newColour = c.test(colour_, sensor_->colour(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newNumPointsX = c.test(numPointsX_, sensor_->numPointsX(),
                                FdtdLife::notifyDomainContentsChange);
        auto newNumPointsY = c.test(numPointsY_, sensor_->numPointsY(),
                                FdtdLife::notifyDomainContentsChange);
        auto newCurrentFrequency = c.test(currentFrequency_, sensor_->frequency(),
                                FdtdLife::notifyMinorChange);
        auto newAverageOverCell = c.test(averageOverCell_,
                sensor_->averageOverCell(),
                                FdtdLife::notifyDomainContentsChange);
        auto newIncludeReflectionData = c.test(includeReflectionData_,
                sensor_->includeReflected(),
                                FdtdLife::notifyMinorChange);
        auto newUseWindow = c.test(useWindow_, sensor_->useWindow(),
                                FdtdLife::notifyMinorChange);
        auto newAnimateFrequency = c.test(animateFrequency_,
                sensor_->animateFrequency(),
                                FdtdLife::notifyMinorChange);
        auto newFrequencyStep = c.test(frequencyStep_, sensor_->frequencyStep(),
                                FdtdLife::notifyMinorChange);
        auto newManualZ = c.test(manualZ_, sensor_->manualZPos(),
                                FdtdLife::notifyDomainContentsChange);
        auto newTransmittedZ = c.test(transmittedZ_, sensor_->transmittedZPos(),
                                FdtdLife::notifyDomainContentsChange);
        auto newReflectedZ = c.test(reflectedZ_, sensor_->reflectedZPos(),
                                FdtdLife::notifyDomainContentsChange);
        auto newComponentX = c.test(componentX_, sensor_->componentSel().x(),
                                FdtdLife::notifyMinorChange);
        auto newComponentY = c.test(componentY_, sensor_->componentSel().y(),
                                FdtdLife::notifyMinorChange);
        auto newComponentZ = c.test(componentZ_, sensor_->componentSel().z(),
                                FdtdLife::notifyMinorChange);
        auto newManualSamples = c.test(manualSamples_, sensor_->manualSamples(),
                                FdtdLife::notifyDomainContentsChange);
        auto newNumberOfSamples = c.test(numberOfSamples_, sensor_->nSamples(),
                                FdtdLife::notifyDomainContentsChange);
        if(c.changeDetected()) {
            // Write any changes
            sensor_->colour(newColour);
            sensor_->numPointsX(newNumPointsX);
            sensor_->numPointsY(newNumPointsY);
            sensor_->frequency(newCurrentFrequency);
            sensor_->averageOverCell(newAverageOverCell);
            sensor_->includeReflected(newIncludeReflectionData);
            sensor_->useWindow(newUseWindow);
            sensor_->animateFrequency(newAnimateFrequency);
            sensor_->frequencyStep(newFrequencyStep);
            sensor_->manualZPos(newManualZ);
            sensor_->transmittedZPos(newTransmittedZ);
            sensor_->reflectedZPos(newReflectedZ);
            sensor_->componentSel({newComponentX, newComponentY, newComponentZ});
            sensor_->manualSamples(newManualSamples);
            sensor_->nSamples(newNumberOfSamples);
            transmittedZ_.readOnly(!sensor_->manualZPos());
            reflectedZ_.readOnly(!sensor_->manualZPos());
            numberOfSamples_.readOnly(!sensor_->manualSamples());
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// A command has been selected
void ArraySensorDlg::onCommand(ArraySensorDlg::Command cmd) {
    // Place text on the clipboard
    switch(cmd) {
        case Command::csvTransmittedTimeSeries:
            dlg_->model().clipboard().put(sensor_->transmittedTimeSeriesCsv());
            break;
        case Command::csvReflectedTimeSeries:
            dlg_->model().clipboard().put(sensor_->reflectedTimeSeriesCsv());
            break;
        case Command::csvTransmittedSpectra:
            dlg_->model().clipboard().put(sensor_->transmittedSpectraCsv());
            break;
        case Command::csvReflectedSpectra:
            dlg_->model().clipboard().put(sensor_->reflectedSpectraCsv());
            break;
        case Command::xmlTransmittedAdmittance: {
            std::string text;
            auto* o = new xml::DomObject("admittance");
            double unitCell = dlg_->model().p()->p2().x().value() -
                              dlg_->model().p()->p1().x().value();
            auto source = dynamic_cast<source::PlaneWaveSource*>(
                    dlg_->model().sources().sources().front());
            if(unitCell > 0.0 && source != nullptr) {
                fdtd::RecordedData recordedData;
                sensor_->copyOutputData(0, 0, recordedData);
                *o << recordedData.admittance(unitCell, source->frequencies().first().value(),
                                              source->frequencies().spacing().value());
                xml::DomDocument dom(o);
                xml::Writer writer;
                writer.writeString(&dom, text);
            }
            dlg_->model().clipboard().put(text, "fdtdlife/admittance");
            break;
        }
    }
}


