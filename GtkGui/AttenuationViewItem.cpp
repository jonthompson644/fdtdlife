/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "AttenuationViewItem.h"

// Write the configuration to an XML file
void AttenuationViewItem::writeConfig(xml::DomObject& root) const {
    // Base class first
    PhaseShiftViewItem::writeConfig(root);
    // Now my things
    root << xml::Reopen();
    root << xml::Obj("yscaletype") << yScaleType_;
    root << xml::Close();
}

// Read the configuration from an XML file
void AttenuationViewItem::readConfig(xml::DomObject& root) {
    // Base class first
    PhaseShiftViewItem::readConfig(root);
    // Now my things
    root >> xml::Reopen();
    root >> xml::Obj("yscaletype") >> yScaleType_;
    root >> xml::Close();
}

// Convert a data value according to the scale type
double AttenuationViewItem::convertYData(double value) {
    double result = value;
    switch(yScaleType_) {
        case YScaleType::linear:
            // The data supplied is power
            result = std::sqrt(result);
            break;
        case YScaleType::power:
            // The data supplied is power
            break;
        case YScaleType::decibels:
            // The data supplied is power
            if(value > 0.0) {
                result = 10.0 * log10(value);
            } else {
                result = 0.0;
            }
            break;
    }
    return result;
}

// Convert the data according to current configuration
void AttenuationViewItem::convertData(int x, std::vector<double>& dy, double& dx) {
    PhaseShiftViewItem::convertData(x, dy, dx);
    for(auto& item : dy) {
        item = convertYData(item);
    }
}

// Fix the sensor spec information according to the current configuration
void AttenuationViewItem::fixInfo(sensor::DataSpec::Info& info) {
    PhaseShiftViewItem::fixInfo(info);
    if(!yAutomatic_ && (yMin_!=yMax_)) {
        info.max_ = yMax_;
        info.min_ = yMin_;
    }
    info.max_ = convertYData(info.max_);
    info.min_ = convertYData(info.min_);
    switch(yScaleType_) {
        case YScaleType::linear:
        case YScaleType::power:
            info.yUnits_ = "";
            break;
        case YScaleType::decibels:
            info.yUnits_ = "dB";
            break;
    }
}
