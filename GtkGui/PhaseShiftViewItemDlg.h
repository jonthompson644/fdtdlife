/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PHASESHIFTVIEWITEMDLG_H
#define FDTDLIFE_PHASESHIFTVIEWITEMDLG_H

#include "ViewDlg.h"
#include "PhaseShiftViewItem.h"
#include "NamedSelection.h"
#include "DataSeriesViewItemDlg.h"

// A dialog panel for data series view item configuration
class PhaseShiftViewItemDlg : public DataSeriesViewItemDlg {
public:
    // Constructor
    PhaseShiftViewItemDlg() = default;
    void construct(ViewDlg* owner, ViewItem* item) override;

    // API
    void populate() override;

protected:
    // Members
    PhaseShiftViewItem* item_{};

    // The widgets
    NamedSelection<PhaseShiftViewItem::XScaleType> xScaleType_{
            "X Scale Type",
            {"Frequency", "Wave Number"}};

    // Handlers
    void onChange() override;

    // Helpers
    void addLinesToDialog() override;  // Override to change the layout
};

#endif //FDTDLIFE_PHASESHIFTVIEWITEMDLG_H
