/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ZoneSourceDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void ZoneSourceDlg::construct(SourcesDlg* owner, source::Source* source) {
    SubDialog::construct(owner, source);
    comment("ZoneSourceDlg");
    source_ = dynamic_cast<source::ZoneSource*>(source);
    addLines({{firstFrequency_, frequencyStep_},
              {numFrequencies_},
              {amplitude_, polarisation_},
              {azimuth_, elevation_},
              {initialTime_, runFor_},
              {continuous_},
              {zoneCenter_},
              {zoneSize_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &ZoneSourceDlg::onChange));
}

// Populate all the widgets with values
void ZoneSourceDlg::populate() {
    SourcesDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    firstFrequency_.value(source_->frequencies().first());
    frequencyStep_.value(source_->frequencies().spacing());
    numFrequencies_.value(source_->frequencies().n());
    amplitude_.value(source_->amplitude());
    polarisation_.value(source_->polarisation());
    azimuth_.value(source_->azimuth());
    elevation_.value(source_->elevation());
    initialTime_.value(source_->initialTime());
    runFor_.value(source_->time());
    continuous_.value(source_->continuous());
    zoneCenter_.value(source_->zoneCenter());
    zoneSize_.value(source_->zoneSize());
}

// Default change handler
void ZoneSourceDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newFirstFrequency = c.test(firstFrequency_,
                                        source_->frequencies().first(),
                                        FdtdLife::notifyMinorChange);
        auto newFrequencyStep = c.test(frequencyStep_,
                                       source_->frequencies().spacing(),
                                       FdtdLife::notifyMinorChange);
        auto newNumFrequencies = c.test(numFrequencies_,
                                        source_->frequencies().n(),
                                        FdtdLife::notifyMinorChange);
        auto newAmplitude = c.test(amplitude_, source_->amplitude(),
                                   FdtdLife::notifyMinorChange);
        auto newPolarisation = c.test(polarisation_, source_->polarisation(),
                                      FdtdLife::notifyMinorChange);
        auto newAzimuth = c.test(azimuth_, source_->azimuth(),
                                 FdtdLife::notifyMinorChange);
        auto newElevation = c.test(elevation_, source_->elevation(),
                                 FdtdLife::notifyMinorChange);
        auto newInitialTime = c.test(initialTime_, source_->initialTime(),
                                     FdtdLife::notifyMinorChange);
        auto newRunFor = c.test(runFor_, source_->time(),
                                FdtdLife::notifyMinorChange);
        auto newContinuous = c.test(continuous_, source_->continuous(),
                                    FdtdLife::notifyMinorChange);
        auto newZoneCenter = c.test(zoneCenter_, source_->zoneCenter(),
                                    FdtdLife::notifyDomainContentsChange);
        auto newZoneSize = c.test(zoneSize_, source_->zoneSize(),
                                    FdtdLife::notifyDomainContentsChange);
        if(c.changeDetected()) {
            // Write any changes
            source_->frequencies().first(newFirstFrequency);
            source_->frequencies().spacing(newFrequencyStep);
            source_->frequencies().n(newNumFrequencies);
            source_->amplitude(newAmplitude);
            source_->polarisation(newPolarisation);
            source_->azimuth(newAzimuth);
            source_->elevation(newElevation);
            source_->initialTime(newInitialTime);
            source_->time(newRunFor);
            source_->continuous(newContinuous);
            source_->zoneCenter(newZoneCenter);
            source_->zoneSize(newZoneSize);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
