/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_GRIDLAYOUT_H
#define FDTDLIFE_GRIDLAYOUT_H

#include <gtkmm/grid.h>
#include "GuiElement.h"

// Arranges elements in a grid.
// Each element is allocated two columns, the left for the name, the right for the entry.
// Text in the name columns will be right justified.
// Checkboxes will only occupy the right column.
class GridLayout : public Gtk::Grid, public GuiElement {
public:
    // Construction
    explicit GridLayout(const std::vector<std::vector<
            std::reference_wrapper<GuiElement>>>& elements);
    GridLayout() = default;

    // API
    void addLines(const std::vector<std::vector<
            std::reference_wrapper<GuiElement>>>& elements);
    void addLine(const std::vector<std::reference_wrapper<GuiElement>>& elements);

    // Overrides of GuiElement
    void initialiseGuiElements(bool recursive) override;
    Gtk::Widget& widget() override { return *this; }

protected:
    std::vector<std::vector<GuiElement*>> elements_;
};


#endif //FDTDLIFE_GRIDLAYOUT_H
