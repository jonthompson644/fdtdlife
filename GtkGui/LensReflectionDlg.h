/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSREFLECTIONDLG_H
#define FDTDLIFE_LENSREFLECTIONDLG_H

#include "AnalysersDlg.h"
#include <Sensor/LensReflectionAnalyser.h>
#include "LensAnalyserDlg.h"

// A dialog panel that shows array sensorId parameters
class LensReflectionDlg : public LensAnalyserDlg {
public:
    // Constructor
    LensReflectionDlg();
    void construct(AnalysersDlg* owner, sensor::Analyser* analyser) override;

    // Overrides of LensAnalyserDlg
    void populate() override;
    void onChange() override;

    // Handlers
    void onCaptureReferenceNow();

protected:
    // Members
    sensor::LensReflectionAnalyser* analyser_{};

    // Extra widgets
    NamedEntry<box::Expression> referenceTime_{"Reference Time (s)"};
    NamedButton captureReferenceNow_{"Capture Reference Now"};
    HorizontalLayout extraLine1_{{referenceTime_, captureReferenceNow_}};
};

#endif //FDTDLIFE_LENSREFLECTIONDLG_H
