/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_TWODSLICEDLG_H
#define FDTDLIFE_TWODSLICEDLG_H

#include "SensorsDlg.h"
#include <Sensor/TwoDSlice.h>
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "HorizontalLayout.h"

// A dialog panel that shows 2D sensorId parameters
class TwoDSliceDlg : public SensorsDlg::SubDlg {
public:
    // Constructor
    TwoDSliceDlg() = default;
    explicit TwoDSliceDlg(bool noDataSource);
    void construct(SensorsDlg* owner, sensor::Sensor* sensor) override;

    // API
    void populate() override;

protected:
    // Members
    sensor::TwoDSlice* sensor_{};
    bool noDataSource_{};

    // The widgets
    NamedSelection<box::Constants::Colour> colour_{
            "Display Colour", box::Constants::colourNames_};
    NamedSelection<box::Constants::Orientation> orientation_{
            "Orientation", box::Constants::orientationNames_};
    NamedSelection<sensor::TwoDSlice::DataSource> dataSource_{
            "Data Source", {"E Mag", "H Mag", "Ex", "Ey", "Ez",
                            "Hx", "Hy", "Hz", "Material"}};
    NamedEntry<double> offset_{"Offset"};
    NamedButton layerPlus_{"Layer+"};
    NamedButton layerMinus_{"Layer-"};
    HorizontalLayout line4_{{offset_, layerPlus_, layerMinus_}, 2};

    // Handlers
    virtual void onChange();
    void onLayerPlus();
    void onLayerMinus();
};

#endif //FDTDLIFE_TWODSLICEDLG_H
