/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariablesDlg.h"
#include "OkCancelDialog.h"
#include <Box/Utility.h>

// Constructor
VariablesDlg::VariablesDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        Notifiable(&model),
        model_(model),
        parentWindow_(parentWindow) {
    registerChild(variables_);
    registerChild(buttons_);
}

// Add the GUI elements to the page
void VariablesDlg::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    // Initialise
    variables_.setColumns(&columns_);
    variables_.appendColumn("ID", columns_.colIdentifier_);
    variables_.appendColumnEditable("Name", columns_.colName_);
    variables_.appendColumnEditable("Expression", columns_.colExpression_);
    variables_.appendColumn("Value", columns_.colValue_);
    variables_.appendColumnEditable("Comment", columns_.colComment_);
    variables_.listWidget().set_reorderable(true);
    variables_.set_hexpand(true);
    variables_.set_vexpand(true);
    attach_next_to(variables_, Gtk::POS_BOTTOM);
    buttons_.set_hexpand(true);
    buttons_.set_vexpand(false);
    attach_next_to(buttons_, Gtk::POS_BOTTOM);
    populate();
    // Connect handlers
    newVariable_.signal_clicked().connect(sigc::mem_fun(
            this, &VariablesDlg::onNewVariable));
    deleteVariable_.signal_clicked().connect(sigc::mem_fun(
            this, &VariablesDlg::onDeleteVariable));
    variables_.model()->signal_row_changed().connect(
            sigc::mem_fun(this, &VariablesDlg::onVariableChanged));
    variables_.listWidget().itemMovedSignal().connect(
            sigc::mem_fun(this, &VariablesDlg::onItemMoved));
}

// Populate all the widgets with values
void VariablesDlg::populate() {
    fillVariableList(currentVariable());
}

// Fill the list with variables
void VariablesDlg::fillVariableList(fdtd::Variable* select) {
    InitialisingGui::Set s(initialising_);
    box::Expression::Context context;
    variables_.model()->clear();
    for(auto& v : model_.variables()) {
        auto row = *(variables_.model()->append());
        row[columns_.colName_] = v.name();
        row[columns_.colExpression_] = v.text();
        model_.variables().fillContext(context, &v);
        row[columns_.colValue_] = v.displayValue(context);
        row[columns_.colComment_] = v.comment();
        row[columns_.colIdentifier_] = v.identifier();
        if(&v == select) {
            variables_.selection()->select(row);
        }
    }
}

// Get the currently selected variable
fdtd::Variable* VariablesDlg::currentVariable() {
    fdtd::Variable* result = nullptr;
    if(variables_.selection()->count_selected_rows() > 0) {
        Gtk::ListStore::Row selection = *variables_.selection()->get_selected();
        int id = selection[columns_.colIdentifier_];
        result = model_.variables().find(id);
    }
    return result;
}

// Create a new variable
void VariablesDlg::onNewVariable() {
    fillVariableList(&model_.variables().add("Unnamed", "1.0"));
    model_.doNotification(fdtd::Model::notifyVariableChange);
}

// Delete a variable
void VariablesDlg::onDeleteVariable() {
    auto* var = currentVariable();
    if(var != nullptr) {
        auto* nextVar = nextVariable();
        if(nextVar == nullptr) {
            nextVar = prevVariable();
        }
        if(var != nullptr) {
            std::stringstream s;
            s << "Ok to delete variable \"" << var->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Variable", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.variables().remove(var);
                fillVariableList(nextVar);
                model_.doNotification(fdtd::Model::notifyVariableChange);
            }
        }
    }
}

// Return the variable after the currently selected
fdtd::Variable* VariablesDlg::nextVariable() {
    fdtd::Variable* result = nullptr;
    if(variables_.selection()->count_selected_rows() > 0) {
        Gtk::TreeIter rowPos;
        rowPos = variables_.selection()->get_selected();
        rowPos++;
        if(rowPos != variables_.model()->children().end()) {
            int id = (*rowPos)[columns_.colIdentifier_];
            result = model_.variables().find(id);
        }
    }
    return result;
}

// Return the variable before the currently selected
fdtd::Variable* VariablesDlg::prevVariable() {
    fdtd::Variable* result = nullptr;
    if(variables_.selection()->count_selected_rows() > 0) {
        auto rowPos = variables_.selection()->get_selected();
        if(rowPos != variables_.model()->children().begin()) {
            rowPos--;
            int id = (*rowPos)[columns_.colIdentifier_];
            result = model_.variables().find(id);
        }
    }
    return result;
}

// A variable has changed in the GUI
void VariablesDlg::onVariableChanged(const Gtk::TreeModel::Path& /*path*/,
                                     const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto row = *pos;
        int id = row[columns_.colIdentifier_];
        auto* var = model_.variables().find(id);
        if(var != nullptr) {
            InitialisingGui::Set s(initialising_);
            var->name(row[columns_.colName_]);
            var->text(row[columns_.colExpression_]);
            var->comment(row[columns_.colComment_]);
            updateValues();
            model_.doNotification(fdtd::Model::notifyVariableChange);
        }
    }
}

// Update the value fields of the variables
void VariablesDlg::updateValues() {
    box::Expression::Context context;
    for(auto& row : variables_.model()->children()) {
        int id = row[columns_.colIdentifier_];
        auto* var = model_.variables().find(id);
        if(var != nullptr) {
            model_.variables().fillContext(context, var);
            row[columns_.colValue_] = var->displayValue(context);
        }
    }
}

// Handle a change notification
void VariablesDlg::notify(fdtd::Notifier* /*source*/, int why,
                          fdtd::NotificationData* /*data*/) {
    if(!initialising_) {
        if(why == fdtd::Model::notifyVariableChange) {
            fillVariableList(currentVariable());
        }
    }
}

// Variables have been reordered
void VariablesDlg::onItemMoved(const Gtk::TreeModel::iterator& pos, size_t location) {
    InitialisingGui::Set s(initialising_);
    int variableId = (*pos)[columns_.colIdentifier_];
    model_.variables().move(variableId, location);
    fillVariableList(currentVariable());
}
