/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDCHECKBOX_H
#define FDTDLIFE_NAMEDCHECKBOX_H

#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include <gtkmm/checkbutton.h>
#include "GuiElement.h"

class NamedCheckbox : public Gtk::HBox, public GuiElement {
public:
    // Construction
    NamedCheckbox() = delete;
    explicit NamedCheckbox(const std::string& name, bool readOnly=false);

    // Accessors
    void value(bool value);
    bool value();

    // Overrides of GuiElement
    void connectChangeHandler(const sigc::slot<void>& slot) override;
    Gtk::Widget& widget() override { return *this; }

protected:
    // Members
    Gtk::Label nameLabel_;  // The name attached to the value
    Gtk::CheckButton valueCheck_;  // The check box for the boolean
    sigc::connection curConnection_;  // The current connection
};

#endif //FDTDLIFE_NAMEDCHECKBOX_H
