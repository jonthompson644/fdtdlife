/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CuboidArrayDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void CuboidArrayDlg::construct(ShapesDlg* owner, domain::Shape* shape) {
    SubDialog::construct(owner, shape);
    material_.construct(shape->m()->d());
    comment("CuboidArray");
    shape_ = dynamic_cast<domain::CuboidArray*>(shape);
    addLines({{material_, ignore_},
              {center_},
              {size_},
              {increment_},
              {duplicate_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &CuboidArrayDlg::onChange));
}

// Populate all the widgets with values
void CuboidArrayDlg::populate() {
    ShapesDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    material_.value(shape_->material());
    ignore_.value(shape_->ignore());
    center_.value(shape_->center());
    size_.value(shape_->size());
    increment_.value(shape_->increment());
    duplicate_.value(shape_->duplicate());
}

// Default change handler
void CuboidArrayDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newMaterial = c.test(material_, shape_->material(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newIgnore = c.test(ignore_, shape_->ignore(),
                                FdtdLife::notifyDomainContentsChange);
        auto newCenter = c.test(center_, shape_->center(),
                                       FdtdLife::notifyDomainContentsChange);
        auto newSize = c.test(size_, shape_->size(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newIncrement = c.test(increment_, shape_->increment(),
                                   FdtdLife::notifyDomainContentsChange);
        auto newDuplicate = c.test(duplicate_, shape_->duplicate(),
                                   FdtdLife::notifyDomainContentsChange);
        if(c.changeDetected()) {
            // Write any changes
            shape_->material(newMaterial);
            shape_->ignore(newIgnore);
            shape_->center(newCenter);
            shape_->size(newSize);
            shape_->increment(newIncrement);
            shape_->duplicate(newDuplicate);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
