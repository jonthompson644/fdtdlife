/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_GRAVYWAVEDLG_H
#define FDTDLIFE_GRAVYWAVEDLG_H

#include <Source/GravyWaveSource.h>
#include "SourcesDlg.h"
#include "NamedEntry.h"
#include "NamedTripleEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"

// A dialog panel that shows the gravitational wave source parameters
class GravyWaveDlg : public SourcesDlg::SubDlg {
public:
    // Constructor
    GravyWaveDlg() = default;
    void construct(SourcesDlg* owner, source::Source* source) override;

    // API
    void populate() override;

protected:
    // Members
    source::GravyWaveSource* source_{};

    // The widgets
    NamedEntry<box::Expression> firstFrequency_{"First Frequency (Hz)"};
    NamedEntry<box::Expression> frequencyStep_{"Frequency Step (Hz)"};
    NamedEntry<box::Expression> numFrequencies_{"Number Of Frequencies"};
    NamedEntry<box::Expression> azimuth_{"Azimuth (0..90 deg)"};
    NamedEntry<box::Expression> elevation_{"Elevation (-90..90 deg)"};
    NamedEntry<box::Expression> polarisation_{"Polarisation (0..360 deg)"};
    NamedEntry<box::Expression> magneticField_{"Magnetic Field (T)"};
    NamedEntry<box::Expression> peakAmplitude_{"Amplitude"};
    NamedEntry<box::Expression> timeOfPeak_{"Time of Peak (s)"};
    NamedEntry<box::Expression> width_{"Width (s)"};
    NamedTripleEntry<box::VectorExpression<double>> zoneCenter_{"Zone Center (m)"};
    NamedTripleEntry<box::VectorExpression<double>> zoneSize_{"Zone Size (m)"};

    // Handlers
    void onChange();
};


#endif //FDTDLIFE_GRAVYWAVEDLG_H
