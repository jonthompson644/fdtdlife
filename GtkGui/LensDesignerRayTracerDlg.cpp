/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensDesignerRayTracerDlg.h"
#include "FdtdLife.h"
#include "GuiChangeDetector.h"
#include <Designer/LensDesigner.h>

// Constructor
void LensDesignerRayTracerDlg::construct(FdtdLife* model, LensDesignerDlg* dlg) {
    model_ = model;
    dlg_ = dlg;
    // Initialise widgets
    addElements({configLayout_, rayTracer_});
    populate();
    rayTracer_.clipboard(&model_->clipboard());
    // Connect handlers
    calculate_.signal_clicked().connect(
            sigc::mem_fun(this, &LensDesignerRayTracerDlg::onCalculate));
    pasteAndcalculate_.signal_clicked().connect(
            sigc::mem_fun(this, &LensDesignerRayTracerDlg::onPasteAndCalculate));
    connectDefaultChangeHandler(
            sigc::mem_fun(this, &LensDesignerRayTracerDlg::onChange));
}

// Populate the controls
void LensDesignerRayTracerDlg::populateControls() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        auto& r = d->rayTracer();
        InitialisingGui::Set s(initialising_);
        deltaAngle_.value(r.deltaAngle());
        maxAngle_.value(r.maxAngle());
        deltaR_.value(r.deltaR());
        deltaS_.value(r.deltaS());
    }
}

// Populate the panel
void LensDesignerRayTracerDlg::populate() {
    populateControls();
    populateRays();
}

// Handle changes to the configuration controls
void LensDesignerRayTracerDlg::onChange() {
    if(!initialising_) {
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        if(d != nullptr) {
            auto& r = d->rayTracer();
            GuiChangeDetector c;
            auto newDeltaAngle = c.test(deltaAngle_, r.deltaAngle(),
                                        FdtdLife::notifyMinorChange);
            auto newMaxAngle = c.test(maxAngle_, r.maxAngle(),
                                        FdtdLife::notifyMinorChange);
            auto newDeltaR = c.test(deltaR_, r.deltaR(),
                                        FdtdLife::notifyMinorChange);
            auto newDeltaS = c.test(deltaS_, r.deltaS(),
                                        FdtdLife::notifyMinorChange);
            if(c.changeDetected()) {
                r.deltaAngle(newDeltaAngle);
                r.maxAngle(newMaxAngle);
                r.deltaR(newDeltaR);
                r.deltaS(newDeltaS);
                // Tell others
                model_->doNotification(c.why());
            }
        }
    }
}

// The calculate button has been pressed
void LensDesignerRayTracerDlg::onCalculate() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        d->rayTracer().calculateRays();
        populateRays();
    }
}

// The paste and calculate button has been pressed
void LensDesignerRayTracerDlg::onPasteAndCalculate() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        if(model_->clipboard().hasTarget("UTF8_STRING")) {
            std::string text = model_->clipboard().getText();
            d->putColumnsCsv(text);
            model_->doNotification(FdtdLife::notifyMaterialChange);
            d->rayTracer().calculateRays();
            populateRays();
        }
    }
}

// Update the rays display
void LensDesignerRayTracerDlg::populateRays() {
    if(!initialising_) {
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        designer::LensRayTracer* r = nullptr;
        if(d != nullptr) {
            r = &d->rayTracer();
        }
        rayTracer_.setRayTracer(r);
    }
}

