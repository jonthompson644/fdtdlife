/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LISTWIDGET_H
#define FDTDLIFE_LISTWIDGET_H

#include <gtkmm/liststore.h>
#include <gtkmm/treeview.h>

// This is a wrapper for the official GTK tree view that
// includes the list store and access to the column spec
// so that the test system can get at the information it needs
class ListWidget : public Gtk::TreeView {
public:
    // Construction
    ListWidget() = default;

    // API
    void setColumns(Gtk::TreeModel::ColumnRecord* columns);
    Glib::RefPtr<Gtk::ListStore>& model() {return treeModel_;}
    Gtk::TreeModel::ColumnRecord* columns() {return columns_;}
    sigc::signal<void, const Gtk::TreeModel::iterator&, size_t>& itemMovedSignal() {
        return itemMovedSignal_;
    }

    // Overrides of TreeView
    void on_drag_end(const Glib::RefPtr<Gdk::DragContext>& context) override;

    // Signal handlers
    void onItemInserted(const Gtk::TreeModel::Path& path,
                            const Gtk::TreeModel::iterator& pos);

protected:
    // Members
    Glib::RefPtr<Gtk::ListStore> treeModel_;
    Gtk::TreeModel::ColumnRecord* columns_{};
    sigc::signal<void, const Gtk::TreeModel::iterator&, size_t> itemMovedSignal_;
    size_t dropLocation_{};  // The line number a drop was made at
    Gtk::TreeModel::iterator droppedRow_;  // The row being dropped
};


#endif //FDTDLIFE_LISTWIDGET_H
