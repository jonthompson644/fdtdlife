/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ListWidget.h"
#include <Box/Utility.h>

// Create the list store with the given set of columns
void ListWidget::setColumns(Gtk::TreeModel::ColumnRecord* columns) {
    // Set up the model
    columns_ = columns;
    treeModel_ = Gtk::ListStore::create(*columns);
    set_model(treeModel_);
    // Connect handlers
    treeModel_->signal_row_inserted().connect(
            sigc::mem_fun(this, &ListWidget::onItemInserted));
}

// The inserted signal can indicate the start of a drag/drop reordering operation.
void ListWidget::onItemInserted(const Gtk::TreeModel::Path& path,
                    const Gtk::TreeModel::iterator& /*pos*/) {
    // Record drop information
    std::string pathName = path.to_string();
    dropLocation_ = box::Utility::toSizeT(pathName);
    droppedRow_ = get_selection()->get_selected();
}

// A drag has completed
void ListWidget::on_drag_end(const Glib::RefPtr<Gdk::DragContext>& /*context*/) {
    // Issue the moved signal
    if(droppedRow_ && dropLocation_ < treeModel_->children().size()) {
        itemMovedSignal_.emit(droppedRow_, dropLocation_);
    }
    // The widget seems to be in a funny state that requires an extra mouse click
    // at this point.  Haven't yet found anything to clear it.
}
