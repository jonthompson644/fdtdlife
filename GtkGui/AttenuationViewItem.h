/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ATTENUATIONVIEWITEM_H
#define FDTDLIFE_ATTENUATIONVIEWITEM_H

#include "PhaseShiftViewItem.h"

// A view of a data series for transmission or reflection data
class AttenuationViewItem : public PhaseShiftViewItem {
public:
    // Types
    enum class YScaleType {linear, decibels, power};

    // Construction
    AttenuationViewItem() = default;

    // Overrides of PhaseShiftViewItem
    void writeConfig(xml::DomObject& root) const override;
    void readConfig(xml::DomObject& root) override;
    void convertData(int x, std::vector<double>& dy, double& dx) override;
    void fixInfo(sensor::DataSpec::Info& info) override;

    // Getters
    YScaleType yScaleType() const { return yScaleType_; }

    // Setters
    void yScaleType(YScaleType v) { yScaleType_ = v; }

protected:
    // Configuration
    YScaleType yScaleType_{YScaleType::power};

    // Helpers
    double convertYData(double value);
};


#endif //FDTDLIFE_ATTENUATIONVIEWITEM_H
