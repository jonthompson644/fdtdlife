/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SingleItemAndInfoLayout.h"

// Constructor
SingleItemAndInfoLayout::SingleItemAndInfoLayout(
        const std::string& name,
        const std::vector<std::reference_wrapper<GuiElement>>& buttons) {
    registerChild(noneButton_);
    registerItem(name, noneButton_);
    setButtons(buttons);
}

// Set the columns
void SingleItemAndInfoLayout::setModeNames(
        const std::list<std::pair<int, std::string>>& modeNames) {
    modeNames_.clear();
    for(auto& name : modeNames) {
        modeNames_.emplace_back(name.second);
    }
}

// Add the GUI elements to the page
void SingleItemAndInfoLayout::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    buttonBox_.attach_next_to(noneButton_, Gtk::POS_BOTTOM);
    noneButton_.signal_clicked().connect(sigc::mem_fun(
            this, &SingleItemAndInfoLayout::onNoneClicked));
    // Create the radio buttons
    size_t index = 0;
    for(auto& modeName : modeNames_) {
        modeButtons_.emplace_back(modeName, noneButton_);
        modeButtons_.back().signal_clicked().connect(
                sigc::bind<size_t>(sigc::mem_fun(this,
                                                 &SingleItemAndInfoLayout::onModeClicked),
                                   index));
        modeButtons_.back().join_group(noneButton_);
        buttonBox_.attach_next_to(modeButtons_.back(), Gtk::POS_BOTTOM);
        index++;
    }
    noneButton_.set_active();
    // Create the additional buttons
    if(!extraButtons_.empty()) {
        buttonBox_.attach_next_to(separator_, Gtk::POS_BOTTOM);
        for(auto& button : extraButtons_) {
            buttonBox_.attach_next_to(button.get().widget(), Gtk::POS_BOTTOM);
        }
    }
    attach_next_to(buttonBox_, Gtk::POS_RIGHT);
    attach_next_to(subDialogFrame_, Gtk::POS_RIGHT);
    subDialogFrame_.get_style_context()->add_class("itemsandinfodlg");
}

// Register the buttons
void SingleItemAndInfoLayout::setButtons(
        const std::vector<std::reference_wrapper<GuiElement>>& buttons) {
    for(auto button : buttons) {
        extraButtons_.emplace_back(button);
        registerChild(button);
    }
}

// Sets the current mode, if the string is blank, the none button is selected
void SingleItemAndInfoLayout::currentMode(const std::string& modeName) {
    if(modeName.empty()) {
        noneButton_.set_active();
        currentMode_.clear();
    } else {
        auto buttonPos = modeButtons_.begin();
        auto namePos = modeNames_.begin();
        bool found = false;
        while(buttonPos != modeButtons_.end() && namePos != modeNames_.end() && !found) {
            if(*namePos == modeName) {
                found = true;
                buttonPos->set_active();
                currentMode_ = modeName;
            }
            ++buttonPos;
            ++namePos;
        }
    }
}

// A mode radio button has been clicked
void SingleItemAndInfoLayout::onModeClicked(size_t index) {
    if(modeButtons_[index].get_active()) {
        modeSelectedSignal_.emit(modeNames_[index]);
    }
}

// The none radio button has been clicked
void SingleItemAndInfoLayout::onNoneClicked() {
    if(noneButton_.get_active()) {
        modeSelectedSignal_.emit("");
    }
}

// Set the current sub-dialog
void SingleItemAndInfoLayout::setSubDialog(GridLayout* subDialog) {
    subDialogFrame_.remove();
    if(subDialog != nullptr) {
        registerChild(*subDialog);
        subDialog->initialiseGuiElements(true);
        subDialogFrame_.add(*subDialog);
        subDialogFrame_.show_all_children();
    }
    subDialog_.reset(subDialog);
}

