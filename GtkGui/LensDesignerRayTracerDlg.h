/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERRAYTRACERDLG_H
#define FDTDLIFE_LENSDESIGNERRAYTRACERDLG_H

#include "VerticalLayout.h"
#include "InitialisingGui.h"
#include "NamedEntry.h"
#include "NamedButton.h"
#include "GridLayout.h"
#include "RayTracerWidget.h"

class FdtdLife;

class LensDesignerDlg;

// A panel that displays the ray tracer output
class LensDesignerRayTracerDlg : public VerticalLayout {
public:
    // Construction
    LensDesignerRayTracerDlg() = default;
    void construct(FdtdLife* model, LensDesignerDlg* dlg);

    // API
    void populate();

protected:
    // Widgets
    NamedEntry<double> deltaAngle_{"Delta Angle (deg)"};
    NamedEntry<double> maxAngle_{"Max Angle (deg)"};
    NamedEntry<double> deltaR_{"Delta R (m)"};
    NamedEntry<double> deltaS_{"Delta S (m)"};
    NamedButton calculate_{"Calculate", 1};
    NamedButton pasteAndcalculate_{"Paste Columns & Calc", 1};
    GridLayout configLayout_{{{deltaAngle_, maxAngle_, calculate_},
                              {deltaR_, deltaS_, pasteAndcalculate_}}};
    RayTracerWidget rayTracer_;

    // Members
    FdtdLife* model_{};
    LensDesignerDlg* dlg_{};
    InitialisingGui initialising_;

    // Handlers
    void onCalculate();
    void onPasteAndCalculate();
    void onChange();

    // Helpers
    void populateControls();
    void populateRays();
};


#endif //FDTDLIFE_LENSDESIGNERRAYTRACERDLG_H
