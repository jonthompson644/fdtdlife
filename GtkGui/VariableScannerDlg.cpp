/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "VariableScannerDlg.h"
#include "FdtdLife.h"
#include "GuiChangeDetector.h"
#include "CharacterizerDlg.h"

// Second stage constructor
void VariableScannerDlg::construct(CharacterizerParametersDlg* dlg, seq::Scanner* scanner) {
    CharacterizerParametersDlg::SubDlg::construct(dlg, scanner);
    scanner_ = dynamic_cast<seq::VariableScanner*>(scanner);
    variable_.construct(&scanner_->s()->m()->variables());
    // Initialise widgets
    addLines({{startStep_},{numSteps_},{from_}, {to_}, {variable_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &VariableScannerDlg::onChange));
}

// Populate the widgets
void VariableScannerDlg::populate() {
    InitialisingGui::Set s(initialising_);
    startStep_.value(scanner_->startStep());
    numSteps_.value(scanner_->numSteps());
    from_.value(scanner_->from());
    to_.value(scanner_->to());
    variable_.value(scanner_->variableId());
}

// Default change handler
void VariableScannerDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newStartStep = c.test(startStep_, scanner_->startStep(),
                                  FdtdLife::notifyMinorChange);
        auto newNumSteps = c.test(numSteps_, scanner_->numSteps(),
                                  FdtdLife::notifyMinorChange);
        auto newFrom = c.test(from_, scanner_->from(),
                                  FdtdLife::notifyMinorChange);
        auto newTo = c.test(to_, scanner_->to(),
                                  FdtdLife::notifyMinorChange);
        auto newVariable = c.test(variable_, scanner_->variableId(),
                                  FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            scanner_->startStep(newStartStep);
            scanner_->numSteps(newNumSteps);
            scanner_->from(newFrom);
            scanner_->to(newTo);
            scanner_->variableId(newVariable);
            // Tell others
            dlg_->dlg()->dlg()->model().doNotification(c.why());
            dlg_->populate(scanner_);
        }
    }
}
