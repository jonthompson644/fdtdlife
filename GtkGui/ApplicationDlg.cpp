/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ApplicationDlg.h"
#include "YesNoCancelDialog.h"
#include "FileSaveDialog.h"
#include "FileOpenDialog.h"
#include <memory>
#include <gtkmm/filechooserdialog.h>
#include <Box/Utility.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Exception.h>
#include <Xml/Reader.h>
#include <Fdtd/NodeManager.h>

// Constructor
ApplicationDlg::ApplicationDlg(FdtdLife& model) :
        Notifiable(&model),
        model_(model),
        recentFiles_(Gtk::RecentManager::get_default()),
        configurationPage_(model, this),
        sourcesPage_(model, this),
        materialsPage_(model, this),
        shapesPage_(model, this),
        sensorsPage_(model, this),
        analysersPage_(model, this),
        viewsPage_(model, this),
        sequencersPage_(model, this),
        variablesPage_(model, this),
        parametersPage_(model, this),
        designersPage_(model, this) {
    // Test mode stuff
    if(model.testSuite() != nullptr) {
        testContinue_ = std::make_unique<Gtk::ToolButton>("Continue Test");
        testContinue_->signal_clicked().connect(
                sigc::mem_fun(this, &ApplicationDlg::onTestContinue));
        tools_.append(*testContinue_);
    }
    // Finish the GUI
    registerChild(layout_);
    GuiElement::initialiseGuiElements(true);
    ApplicationWindow::add(layout_);
    // Connect the handlers
    initialise_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolInitialise));
    fileNew_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolFileNew));
    fileOpen_.mainDoSignal().connect(
            sigc::mem_fun(this, &ApplicationDlg::onToolFileOpen));
    fileOpen_.menuDoSignal().connect(
            sigc::mem_fun(this, &ApplicationDlg::onToolFileOpenRecent));
    fileSave_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolFileSave));
    fileSaveAs_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolFileSaveAs));
    run_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolRun));
    stop_.connectHandler(
            sigc::mem_fun(this, &ApplicationDlg::onToolStop));
    // Finish
    setTitle();
    show_all_children();
    setRecentFiles();
}

// Handle a model change notification
void ApplicationDlg::notify(fdtd::Notifier* /*source*/, int why,
                            fdtd::NotificationData* /*data*/) {
    // Record whether the model is modified and will need saving.
    modified_ = modified_ ||
                (why == FdtdLife::notifyMinorChange) ||
                (why == FdtdLife::notifyDomainSizeChange) ||
                (why == FdtdLife::notifyMinorNoViews) ||
                (why == FdtdLife::notifySensorChange) ||
                (why == FdtdLife::notifyMaterialChange) ||
                (why == FdtdLife::notifyAnalyserChange) ||
                (why == FdtdLife::notifySequencerChange) ||
                (why == FdtdLife::notifyShapeChange) ||
                (why == FdtdLife::notifyVariableChange) ||
                (why == FdtdLife::notifyDomainContentsChange);
}

// Check to see if the current model should be saved
// Returns true if the caller can proceed.
bool ApplicationDlg::maybeSave(const std::string& title) {
    bool result = true;
    if(modified_) {
        std::stringstream s;
        s << "Do you wish to keep changes to " << box::Utility::stem(currentFileName_) << "?";
        YesNoCancelDialog box(this, title, s.str(),
                              model_.testSuite());
        switch(box.ask()) {
            case YesNoCancelDialog::Answer::yes:
                result = fileSave(title);
                break;
            case YesNoCancelDialog::Answer::no:
                break;
            case YesNoCancelDialog::Answer::cancel:
                result = false;
                break;
        }
    }
    return result;
}

// Continue the test sequence by exiting the current loop
void ApplicationDlg::onTestContinue() {
    if(model_.testSuite() != nullptr) {
        gtk_main_quit();
    }
}

// Handle the initialisation toolbar button
void ApplicationDlg::onToolInitialise() {
    switch(runState_) {
        case RunState::stopped:
        case RunState::paused:
            runState_ = RunState::stopped;
            model_.initialise();
            populate();
            break;
        case RunState::running:
        case RunState::animating:
            break;
    }
}


// Handle the file new toolbar button
void ApplicationDlg::onToolFileNew() {
    if(maybeSave("File New")) {
        model_.clear();
        currentFileName_ = "Untitled";
        modified_ = false;
        validFile_ = false;
        setTitle();
        populate();
    }
}

// Set the application title
void ApplicationDlg::setTitle() {
    std::stringstream s;
    s << "FDTD Life - " << box::Utility::stem(currentFileName_);
    set_title(s.str());
}

// Handle the file open toolbar button
void ApplicationDlg::onToolFileOpen() {
    // Need to save the current model?
    if(maybeSave("Open File")) {
        FileOpenDialog box(this, "Open File", currentFileName_,
                           model_.testSuite());
        if(box.ask(currentFileName_)) {
            doOpen();
        }
    }
}

// Handle the file save toolbar button
void ApplicationDlg::onToolFileSave() {
    if(validFile_) {
        doSave();
    } else {
        onToolFileSaveAs();
    }
}

// Save the file to the current filename if valid, otherwise ask for a filename
// Returns true if the file was saved.
bool ApplicationDlg::fileSave(const std::string& title) {
    bool result = true;
    if(validFile_) {
        doSave();
    } else {
        FileSaveDialog box(this, title, currentFileName_,
                           model_.testSuite());
        if(box.ask(currentFileName_)) {
            doSave();
            validFile_ = true;
            setTitle();
        } else {
            result = false;
        }
    }
    return result;
}

// Save the model to the current file name
void ApplicationDlg::doSave() {
    std::string pathName = box::Utility::path(currentFileName_) + "/";
    std::string fileName = box::Utility::stem(currentFileName_);
    model_.writeConfigFile(pathName, fileName);
    modified_ = false;
    recentFiles_->add_item(currentFileName_);
    setRecentFiles();
}

// Read a model from the current file
void ApplicationDlg::doOpen() {
    model_.clear();
    std::string pathName = box::Utility::path(currentFileName_) + "/";
    std::string fileName = box::Utility::stem(currentFileName_);
    validFile_ = model_.readConfigFile(pathName, fileName);
    modified_ = false;
    if(validFile_) {
        recentFiles_->add_item(currentFileName_);
        setRecentFiles();
    }
    setTitle();
    populate();
}

// Handle the file save as toolbar button
void ApplicationDlg::onToolFileSaveAs() {
    FileSaveDialog box(this, "Save File As", currentFileName_,
                       model_.testSuite());
    if(box.ask(currentFileName_)) {
        doSave();
        validFile_ = true;
        setTitle();
    }
}

// Handle the run model toolbar button
void ApplicationDlg::onToolRun() {
    switch(runState_) {
        case RunState::stopped:
            runState_ = RunState::running;
            if(model_.start()) {
                runConnection_ = Glib::signal_idle().connect(
                        sigc::mem_fun(this, &ApplicationDlg::onIdle));
            } else {
                runState_ = RunState::stopped;
            }
            break;
        case RunState::paused:
            runState_ = RunState::running;
            runConnection_ = Glib::signal_idle().connect(
                    sigc::mem_fun(this, &ApplicationDlg::onIdle));
            break;
        case RunState::running:
        case RunState::animating:
            break;
    }
}

// Perform a processing step
bool ApplicationDlg::onIdle() {
    switch(runState_) {
        case RunState::running:
            switch(model_.p()->phase()) {
                case fdtd::Configuration::phaseElectric:
                case fdtd::Configuration::phaseMagnetic:
                    model_.step();
                    break;
                case fdtd::Configuration::phaseDone:
                    runState_ = RunState::paused;
                    runConnection_.disconnect();
                    break;
            }
            break;
        case RunState::paused:
        case RunState::stopped:
        case RunState::animating:
            break;
    }
    return true;  // We always need more idle time
}

// Handle the stop model toolbar button
void ApplicationDlg::onToolStop() {
    switch(runState_) {
        case RunState::running:
            runState_ = RunState::paused;
            runConnection_.disconnect();
            model_.doNotification(FdtdLife::notifyModelStop);
            break;
        case RunState::paused:
        case RunState::stopped:
        case RunState::animating:
            break;
    }
}

// Populate all the panels
void ApplicationDlg::populate() {
    configurationPage_.populate();
    sourcesPage_.populate();
    materialsPage_.populate();
    shapesPage_.populate();
    sensorsPage_.populate();
    analysersPage_.populate();
    viewsPage_.populate();
    sequencersPage_.populate();
    variablesPage_.populate();
    parametersPage_.populate();
    designersPage_.populate();
}

// Handle the application about to close
bool ApplicationDlg::on_delete_event(GdkEventAny* event) {
    bool result = true;
    if(maybeSave("Quit")) {
        result = Widget::on_delete_event(event);
    }
    return result;
}

// Set the recent files list in the open recent button
void ApplicationDlg::setRecentFiles() {
    std::map<std::string, std::string> values;
    for(auto& item : recentFiles_->get_items()) {
        std::string fileName = item->get_uri_display();
        std::string displayName = box::Utility::stem(fileName);
        std::string extension = box::Utility::extension(fileName);
        if(extension == "fdtd") {
            if(values.count(displayName) == 0) {
                values[displayName] = fileName;
            }
        }
    }
    fileOpen_.setMenuItems(values);
}

// A recent file has been selected
void ApplicationDlg::onToolFileOpenRecent(std::string fileName) {
    currentFileName_ = fileName;
    doOpen();
}

// Set the run state to stopped and clear out any waiting jobs
void ApplicationDlg::setStopped() {
    model_.stop();
    runState_ = RunState::stopped;
    model_.processingMgr()->clear();
    model_.processingMgr()->doNotification(fdtd::NodeManager::notifyJobListChanged);
}
