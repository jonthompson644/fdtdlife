/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Clipboard.h"

// Destructor
Clipboard::~Clipboard() {
    auto clipboard = Gtk::Clipboard::get();
    clipboard->clear();
}

// Place text onto the clipboard.  Text is always
// placed using the standard text target and in addition an
// optional specified target
void Clipboard::put(const std::string& value, const std::string& target) {
    auto clipboard = Gtk::Clipboard::get();
    std::vector<Gtk::TargetEntry> targets;
    targets.emplace_back("UTF8_STRING");
    if(!target.empty()) {
        targets.emplace_back(target);
    }
    clipboard->set(targets,
                   sigc::mem_fun(*this, &Clipboard::onClipboardGet),
                   sigc::mem_fun(*this, &Clipboard::onClipboardClear));
    clipboardText_ = value;
}

// Place an image onto the clipboard.
void Clipboard::put(const Glib::RefPtr<Gdk::Pixbuf>& value) {
    returnedImage_.clear();
    auto clipboard = Gtk::Clipboard::get();
    clipboard->set_image(value);
}

// Handle requests for clipboard data
void Clipboard::onClipboardGet(Gtk::SelectionData& selectionData, guint /*info*/) {
    const std::string target = selectionData.get_target();
    if(!clipboardText_.empty()) {
        selectionData.set_text(clipboardText_);
    }
}

// Clear the clipboard
void Clipboard::onClipboardClear() {
    clipboardText_.clear();
}

// Return the clipboard contents for the specified target
const std::string& Clipboard::getText(const std::string& target) {
    returnedText_.clear();
    auto clipboard = Gtk::Clipboard::get();
    if(clipboard->wait_is_target_available(target)) {
        returnedText_ = clipboard->wait_for_text();
    }
    return returnedText_;
}

// Return the clipboard contents for the specified target
const Glib::RefPtr<Gdk::Pixbuf>& Clipboard::getImage() {
    auto clipboard = Gtk::Clipboard::get();
    returnedImage_ = clipboard->wait_for_image();
    return returnedImage_;
}

// Return the list of targets available
std::vector<Glib::ustring> Clipboard::targets() {
    auto clipboard = Gtk::Clipboard::get();
    return clipboard->wait_for_targets();
}

// Return true if the specified target is available
bool Clipboard::hasTarget(const std::string& target) {
    auto clipboard = Gtk::Clipboard::get();
    return clipboard->wait_is_target_available(target);
}
