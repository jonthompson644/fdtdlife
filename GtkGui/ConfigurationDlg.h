/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CONFIGURATIONDLG_H
#define FDTDLIFE_CONFIGURATIONDLG_H


#include <gtkmm/hvbox.h>
#include <gtkmm/frame.h>
#include <gtkmm/checkbutton.h>
#include "NamedEntry.h"
#include "NamedTripleEntry.h"
#include "NamedTripleSelection.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"
#include "GridLayout.h"
#include "FdtdLife.h"
#include "NamedButton.h"
#include "InitialisingGui.h"
#include "Timer.h"
#include <Fdtd/Configuration.h>
#include <Fdtd/Notifiable.h>

// The configuration and status panel for the top level dialog
class ConfigurationDlg : public GridLayout, public fdtd::Notifiable {
public:
    // Construction
    ConfigurationDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // API
    void populate();

protected:
    // Handlers
    void onChange();
    void onDynamicInfoExpiry();

    // Helpers
    void populateConfiguration();
    void populateStaticInformation();
    void populateDynamicInformation();

protected:
    // Members
    FdtdLife& model_;
    Gtk::Window* parentWindow_;
    InitialisingGui initialising_;
    Timer dynamicInfoWait_;  // The wait before the dynamic information is updated again
    bool dynamicInfoUpdateWaiting_{false};
    static const unsigned int dynamicInfoDeadTime_ = 2000;

    // Configuration widgets
    NamedToggleButton fdtd_{"FDTD"};
    NamedToggleButton propMatrices_{"Propagation Matrices"};
    NamedEntry<double> stabilityFactor_{"Stability Factor"};
    NamedEntry<size_t> scatteredZone_{"Scattered Zone Size"};
    NamedTripleEntry<box::VectorExpression<double>> lowCorner_{
            "Bottom Corner x,y,z (m)"};
    NamedTripleEntry<box::VectorExpression<double>> highCorner_{"Top Corner x,y,z (m)"};
    NamedTripleEntry<box::VectorExpression<double>> cellSize_{"Cell Size x,y,z (m)"};
    NamedEntry<size_t> boundarySize_{"Boundary Size"};
    NamedTripleSelection<fdtd::Configuration::Boundary> boundaries_{
            "Boundaries x,y,z", {"Reflecting", "Periodic", "Absorbing"}};
    NamedEntry<box::Expression> timeSpan_{"Time Span (s)"};
    NamedEntry<size_t> numThreads_{"Number Of Threads"};
    NamedSelection<fdtd::PlateAdmittance::Formula> propMatrixFormula_{
            "Propagation Matrix Formula", {
                    "Lee et al", "Ulrich", "Arnaud et al", "Chen", "Admittance Table"}};
    NamedButton admittanceTableButton_{"Admittance Table"};
    NamedEntry<double> animationPeriod_{"Sensor Animation Period (s)"};
    NamedToggleButton whiteBackground_{"White Background"};
    NamedToggleButton useThinSheetSubcells_{"Use Thin Sheet Subcells"};
    // Information widgets
    NamedTripleEntry<size_t> nCells_{"Number Of Cells x,y,z", true};
    NamedEntry<size_t> nTimeSteps_{"Number Of Time Steps", true};
    NamedEntry<double> timeStep_{"Time Step (s)", true};
    NamedEntry<size_t> memoryUse_{"Memory Use (MBytes)", true};
    NamedEntry<bool> usingOpenMP_{"Using openMP"};
    NamedEntry<size_t> threadsUsed_{"Num Threads Used", true};
    NamedEntry<size_t> threadsAvail_{"Available Threads", true};
    NamedEntry<size_t> curTimeStep_{"Cur Time Step", true};
    NamedEntry<double> stepTime_{"Step Time (s)", true};
    NamedEntry<double> cellRate_{"Cell Rate (kcells/s)", true};
    NamedEntry<double> maxRate_{"Max Rate (kcells/s)", true};
};


#endif //FDTDLIFE_CONFIGURATIONDLG_H
