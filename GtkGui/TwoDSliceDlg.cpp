/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "TwoDSliceDlg.h"
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include "GuiChangeDetector.h"
#include <Domain/UnitCellShape.h>

// Constructor used by derived classes to suppress
// the data source control
TwoDSliceDlg::TwoDSliceDlg(bool noDataSource) :
        noDataSource_(noDataSource) {
}

// Second stage constructor used by the factory
void TwoDSliceDlg::construct(SensorsDlg* owner, sensor::Sensor* sensor) {
    SubDialog::construct(owner, sensor);
    comment("TwoDSlice");
    sensor_ = dynamic_cast<sensor::TwoDSlice*>(sensor);
    if(noDataSource_) {
        addLines({{colour_}, {orientation_}, {line4_}});
    } else {
        addLines({{colour_}, {orientation_}, {dataSource_}, {line4_}});
    }
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &TwoDSliceDlg::onChange));
    layerPlus_.connectHandler(sigc::mem_fun(*this, &TwoDSliceDlg::onLayerPlus));
    layerMinus_.connectHandler(sigc::mem_fun(*this, &TwoDSliceDlg::onLayerMinus));
}

// Populate all the widgets with values
void TwoDSliceDlg::populate() {
    SensorsDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    colour_.value(sensor_->colour());
    orientation_.value(sensor_->orientation());
    if(!noDataSource_) {
        dataSource_.value(sensor_->dataSource());
    }
    offset_.value(sensor_->offset());
}

// Default change handler
void TwoDSliceDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newColour = c.test(colour_, sensor_->colour(),
                                FdtdLife::notifyDomainContentsChange);
        auto newOrientation = c.test(orientation_, sensor_->orientation(),
                                     FdtdLife::notifyDomainContentsChange);
        sensor::TwoDSlice::DataSource newDataSource {};
        if(!noDataSource_) {
            newDataSource = c.test(dataSource_, sensor_->dataSource(),
                                        FdtdLife::notifyDomainContentsChange);
        }
        auto newOffset = c.test(offset_, sensor_->offset(),
                                FdtdLife::notifyDomainContentsChange);
        if(c.changeDetected()) {
            // Write any changes
            sensor_->colour(newColour);
            sensor_->orientation(newOrientation);
            if(!noDataSource_) {
                sensor_->dataSource(newDataSource);
            }
            sensor_->offset(newOffset);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Advance the offset to the next layer
void TwoDSliceDlg::onLayerPlus() {
    // We can only do this for Z orientation
    if(sensor_->orientation() == box::Constants::orientationZPlane) {
        dlg_->model().d()->evaluate();
        double newOffset = sensor_->offset();
        bool found = false;
        double delta = dlg_->model().p()->dr().z().value() / 2.0;
        // Find the layer shape with the lowest z that is bigger than the current offset
        for(auto& s : dlg_->model().d()->shapes()) {
            auto* layer = dynamic_cast<domain::UnitCellShape*>(s.get());
            if(layer != nullptr) {
                if(layer->center().value().z() > sensor_->offset() + delta) {
                    if(layer->center().value().z() < newOffset || !found) {
                        newOffset = layer->center().value().z();
                        found = true;
                    }
                }
            }
        }
        if(found) {
            offset_.value(newOffset);
            onChange();
        }
    }
}

// Move the offset to the position of the previous layer
void TwoDSliceDlg::onLayerMinus() {
    // We can only do this for Z orientation
    if(sensor_->orientation() == box::Constants::orientationZPlane) {
        dlg_->model().d()->evaluate();
        double newOffset = sensor_->offset();
        bool found = false;
        double delta = dlg_->model().p()->dr().z().value() / 2.0;
        // Find the layer shape with the biggest z that is smaller than the current offset
        for(auto& s : dlg_->model().d()->shapes()) {
            auto* layer = dynamic_cast<domain::UnitCellShape*>(s.get());
            if(layer != nullptr) {
                if(layer->center().value().z() < sensor_->offset() - delta) {
                    if(layer->center().value().z() > newOffset || !found) {
                        newOffset = layer->center().value().z();
                        found = true;
                    }
                }
            }
        }
        if(found) {
            offset_.value(newOffset);
            onChange();
        }
    }
}
