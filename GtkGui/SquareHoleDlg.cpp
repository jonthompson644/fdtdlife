/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "SquareHoleDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void SquareHoleDlg::construct(ShapesDlg* owner, domain::Shape* shape) {
    SubDialog::construct(owner, shape);
    material_.construct(shape->m()->d());
    fillMaterial_.construct(shape->m()->d());
    comment("SquareHole");
    shape_ = dynamic_cast<domain::SquareHole*>(shape);
    addLines({{material_, ignore_},
              {layerPosition_},
              {cellSize_, plateThickness_},
              {increment_},
              {duplicate_},
              {holeSize_, fillMaterial_},
              {repeatX_, repeatY_},
              {offsetX_, offsetY_}});
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &SquareHoleDlg::onChange));
}

// Populate all the widgets with values
void SquareHoleDlg::populate() {
    ShapesDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    material_.value(shape_->material());
    ignore_.value(shape_->ignore());
    layerPosition_.value(shape_->center());
    cellSize_.value(shape_->cellSizeX());
    plateThickness_.value(shape_->layerThickness());
    increment_.value(shape_->increment());
    duplicate_.value(shape_->duplicate());
    holeSize_.value(shape_->holeSize());
    fillMaterial_.value(shape_->fillMaterial());
    repeatX_.value(shape_->repeatX());
    repeatY_.value(shape_->repeatY());
    offsetX_.value(shape_->holeOffsetX());
    offsetY_.value(shape_->holeOffsetY());
}

// Default change handler
void SquareHoleDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newMaterial = c.test(material_, shape_->material(),
                                  FdtdLife::notifyDomainContentsChange);
        auto newIgnore = c.test(ignore_, shape_->ignore(),
                                FdtdLife::notifyDomainContentsChange);
        auto newLayerPosition = c.test(layerPosition_, shape_->center(),
                                       FdtdLife::notifyMinorChange);
        auto newCellSize = c.test(cellSize_, shape_->cellSizeX(),
                                  FdtdLife::notifyMinorChange);
        auto newLayerThickness = c.test(plateThickness_, shape_->layerThickness(),
                                        FdtdLife::notifyMinorChange);
        auto newIncrement = c.test(increment_, shape_->increment(),
                                   FdtdLife::notifyMinorChange);
        auto newDuplicate = c.test(duplicate_, shape_->duplicate(),
                                   FdtdLife::notifyMinorChange);
        auto newHoleSize = c.test(holeSize_, shape_->holeSize(),
                                   FdtdLife::notifyMinorChange);
        auto newFillMaterial = c.test(fillMaterial_, shape_->fillMaterial(),
                                      FdtdLife::notifyMinorChange);
        auto newRepeatX = c.test(repeatX_, shape_->repeatX(),
                                 FdtdLife::notifyMinorChange);
        auto newRepeatY = c.test(repeatY_, shape_->repeatY(),
                                 FdtdLife::notifyMinorChange);
        auto newOffsetX = c.test(offsetX_, shape_->holeOffsetX(),
                                 FdtdLife::notifyMinorChange);
        auto newOffsetY = c.test(offsetY_, shape_->holeOffsetY(),
                                 FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            shape_->material(newMaterial);
            shape_->ignore(newIgnore);
            shape_->center(newLayerPosition);
            shape_->cellSizeX(newCellSize);
            shape_->layerThickness(newLayerThickness);
            shape_->increment(newIncrement);
            shape_->duplicate(newDuplicate);
            shape_->holeSize(newHoleSize);
            shape_->fillMaterial(newFillMaterial);
            shape_->repeatX(newRepeatX);
            shape_->repeatY(newRepeatY);
            shape_->holeOffsetX(newOffsetX);
            shape_->holeOffsetY(newOffsetY);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
