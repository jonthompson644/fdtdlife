/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ViewItem.h"
#include "FdtdLife.h"

// Second phase construction used by factory
void ViewItem::construct(int mode, int id, ViewFrame* frame) {
    frame_ = frame;
    identifier_ = id;
    mode_ = mode;
}

// Write configuration to the DOM object
void ViewItem::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("identifier") << identifier_;
    root << xml::Obj("mode") << mode_;
    root << xml::Obj("sensorId") << sensorId_;
    root << xml::Obj("data") << dataSpecName_;
    root << xml::Close();
}

// Read configuration from the DOM object
void ViewItem::readConfig(xml::DomObject& root) {
    // Nothing to read but we must open and close so that reopen works
    root >> xml::Open();
    root >> xml::Obj("sensorId") >> xml::Default(sensorId_) >> sensorId_;
    root >> xml::Obj("data") >> xml::Default(dataSpecName_) >> dataSpecName_;
    // Backwards compatability
    root >> xml::Attr("data") >> xml::Default(dataSpecName_) >> dataSpecName_;
    root >> xml::Attr("sensorId") >> xml::Default(sensorId_) >> sensorId_;
    root >> xml::Close();
}

// Return the data specification for this item
sensor::DataSpec* ViewItem::dataSpec() {
    return frame_->m()->sensors().getDataSpec(sensorId_, dataSpecName_);
}

// Return the sensor for this item
sensor::Sensor* ViewItem::sensor() {
    return frame_->m()->sensors().find(sensorId_);
}

// Add the GUI elements to the page
void ViewItem::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    for(auto& child : children_) {
        pack_start(child->widget(), child->packOptions());
    }
}
