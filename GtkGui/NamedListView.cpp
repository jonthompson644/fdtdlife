/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NamedListView.h"

// Constructor
NamedListView::NamedListView(const std::string& name) {
    name_ = std::make_unique<Gtk::Label>(name);
    construct(name);
    packOptions_ = Gtk::PACK_EXPAND_WIDGET;
}

// Constructor
NamedListView::NamedListView() {
    construct("listview");
}

// The common part of the construction
void NamedListView::construct(const std::string& name) {
    registerItem(name, treeView_);
}

// Add the GUI elements to the page
void NamedListView::initialiseGuiElements(bool recursive) {
    GuiElement::initialiseGuiElements(recursive);
    get_style_context()->add_class("namedlistview");
    if(name_) {
        name_->set_vexpand(false);
        name_->set_hexpand(true);
        name_->set_halign(Gtk::ALIGN_START);
        name_->get_style_context()->add_class("namedlistviewname");
        attach_next_to(*name_, Gtk::POS_BOTTOM);
    }
    scrolledWindow_.add(treeView_);
    scrolledWindow_.set_policy(Gtk::POLICY_AUTOMATIC,
                               Gtk::POLICY_AUTOMATIC);
    scrolledWindow_.set_min_content_width(300);
    scrolledWindow_.set_vexpand(true);
    scrolledWindow_.set_hexpand(true);
    attach_next_to(scrolledWindow_, Gtk::POS_BOTTOM);
    treeView_.get_selection()->set_mode(Gtk::SELECTION_SINGLE);
}

// Return the row position of the current selection
Gtk::TreeIter NamedListView::curSelection() {
    return selection()->get_selected();
}

// Return the row after the currently selected
Gtk::TreeIter NamedListView::nextSelection() {
    Gtk::TreeIter result = curSelection();
    if(result) {
        result++;
    }
    return result;
}

// Return the row before the currently selected
Gtk::TreeIter NamedListView::prevSelection() {
    Gtk::TreeIter result = curSelection();
    if(result) {
        result--;
    }
    return result;
}

