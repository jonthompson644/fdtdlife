/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include "ParameterExtractorDlg.h"

// Second stage constructor used by factories
void ParameterExtractorDlg::construct(DesignersDlg* owner, designer::Designer* designer) {
    addLines({
                     {line0_},
                     {line1_},
                     {line2_},
                     {details_}});
    parameterExtractor_ = dynamic_cast<designer::ParameterExtractor*>(designer);
    SubDialog::construct(owner, designer);
    plot_.clipboard(&dlg_->model().clipboard());
    parameters_.setColumns(&parametersColumns_);
    parameters_.appendColumn("Id", parametersColumns_.colIdentifier_);
    parameters_.appendColumnEditable("Header", parametersColumns_.colHeader_);
    parameters_.appendColumnEditable("Unit Cell (m)", parametersColumns_.colUnitCell_);
    parameters_.appendColumnEditable("Patch Ratio (%)",
                                     parametersColumns_.colPatchRatio_);
    parameters_.appendColumnEditable("Angle (deg)",
                                     parametersColumns_.colIncidenceAngle_);
    parameters_.set_hexpand(true);
    parameters_.set_vexpand(true);
    details_.setColumns(&detailsColumns_);
    details_.appendColumn("Frequency", detailsColumns_.colFrequency_);
    details_.appendColumn("S11", detailsColumns_.colS11_);
    details_.appendColumn("S12", detailsColumns_.colS12_);
    details_.appendColumn("Permittivity", detailsColumns_.colPermittivity_);
    details_.appendColumn("Permeability", detailsColumns_.colPermeability_);
    details_.appendColumn("Refractive Index", detailsColumns_.colRefIndex_);
    details_.set_hexpand(true);
    details_.set_vexpand(true);
    parameters_.selection()->signal_changed().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onSelectParameter));
    parameters_.model()->signal_row_changed().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onParameterChanged));
    pasteHfss_.signal_clicked().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onPasteHfss));
    calculate_.signal_clicked().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onCalculate));
    makeParams_.signal_clicked().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onMakeParams));
    deleteTable_.signal_clicked().connect(sigc::mem_fun(
            this, &ParameterExtractorDlg::onDeleteTable));
    populate();
}

// Fill the dialog panel
void ParameterExtractorDlg::populate() {
    layerSpacing_.value(parameterExtractor_->layerSpacing());
    numLayers_.value(parameterExtractor_->numLayers());
    vacuumGap_.value(parameterExtractor_->vacuumGap());
    basePermittivity_.value(parameterExtractor_->basePermittivity());
    fillParameters(currentParameter());
    fillDetails();
    fillPlot();
}

// Fill the S parameters list
void ParameterExtractorDlg::fillParameters(designer::HfssSParameter* sel) {
    InitialisingGui::Set s(initialising_);
    parameters_.model()->clear();
    bool selectFirst = sel == nullptr;
    size_t n = 0;
    for(auto& p : *parameterExtractor_) {
        auto row = *(parameters_.model()->append());
        row[parametersColumns_.colIdentifier_] = n;
        row[parametersColumns_.colHeader_] = p.header();
        row[parametersColumns_.colUnitCell_] = p.unitCell();
        row[parametersColumns_.colPatchRatio_] = p.patchRatio();
        row[parametersColumns_.colIncidenceAngle_] = p.incidenceAngle();
        if(&p == sel || selectFirst) {
            parameters_.selection()->select(row);
            selectFirst = false;
        }
        n++;
    }
}

// Fill the details list view
void ParameterExtractorDlg::fillDetails() {
    InitialisingGui::Set s(initialising_);
    details_.model()->clear();
    auto* curParam = currentParameter();
    if(curParam != nullptr) {
        for(auto& p : *curParam) {
            auto row = *(details_.model()->append());
            row[detailsColumns_.colFrequency_] = p.frequency_;
            row[detailsColumns_.colS11_] = box::Utility::fromComplex(p.s11_);
            row[detailsColumns_.colS12_] = box::Utility::fromComplex(p.s12_);
            row[detailsColumns_.colPermittivity_] = box::Utility::fromComplex(p.permittivity_);
            row[detailsColumns_.colPermeability_] = box::Utility::fromComplex(p.permeability_);
            row[detailsColumns_.colRefIndex_] = box::Utility::fromComplex(p.refractiveIndex_);
        }
    }
}

// Return the currently selected parameter
designer::HfssSParameter* ParameterExtractorDlg::currentParameter() {
    designer::HfssSParameter* result = nullptr;
    auto rowPos = parameters_.curSelection();
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        std::string header = selection[parametersColumns_.colHeader_];
        result = parameterExtractor_->find(header);
    }
    return result;
}

// Paste HFSS text from the clipboard
void ParameterExtractorDlg::onPasteHfss() {
    if(dlg_->model().clipboard().hasTarget("UTF8_STRING")) {
        std::string text = dlg_->model().clipboard().getText("UTF8_STRING");
        parameterExtractor_->pasteHfssData(text);
        populate();
    }
}

// A parameter has been selected
void ParameterExtractorDlg::onSelectParameter() {
    if(!initialising_) {
        fillDetails();
        fillPlot();
    }
}

// Perform the parameter extraction calculations
void ParameterExtractorDlg::onCalculate() {
    // Get the global parameters
    parameterExtractor_->layerSpacing(layerSpacing_.value());
    parameterExtractor_->numLayers(numLayers_.value());
    parameterExtractor_->vacuumGap(vacuumGap_.value());
    parameterExtractor_->basePermittivity(basePermittivity_.value());
    // Perform the calculations
    parameterExtractor_->calculate();
    // User interface
    fillDetails();
}

// Handle edits made in the parameters_ list view
void ParameterExtractorDlg::onParameterChanged(const Gtk::TreeModel::Path& /*path*/,
                                               const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto row = *pos;
        size_t n = row[parametersColumns_.colIdentifier_];
        auto param = parameterExtractor_->find(n);
        if(param != parameterExtractor_->end()) {
            InitialisingGui::Set s(initialising_);
            param->header(row[parametersColumns_.colHeader_]);
            param->unitCell(row[parametersColumns_.colUnitCell_]);
            param->patchRatio(row[parametersColumns_.colPatchRatio_]);
            param->incidenceAngle(row[parametersColumns_.colIncidenceAngle_]);
            dlg_->model().doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// Plot the refractive index graph
void ParameterExtractorDlg::fillPlot() {
    auto* curParam = currentParameter();
    if(curParam != nullptr && !curParam->empty()) {
        // Set up the axes
        double maxN = parameterExtractor_->maxRefractiveIndex();
        plot_.axisY(0.0, maxN, "");
        double minFreq = curParam->front().frequency_;
        double maxFreq = curParam->back().frequency_;
        double size = curParam->size();
        double stepSize = (maxFreq - minFreq) / (size - 1);
        plot_.axisX(minFreq, stepSize, size, "GHz");
        // Fill the data
        std::vector<double> data;
        for(auto& p : *curParam) {
            data.push_back(p.refractiveIndex_.real());
        }
        plot_.data("Refractive index", data);
    }
}

// Convert the extracted data into entries in the parameters table
void ParameterExtractorDlg::onMakeParams() {
    parameterExtractor_->makeParameters();
    dlg_->model().initialise();
}

// Delete the selected parameter table
void ParameterExtractorDlg::onDeleteTable() {
    auto rowPos = parameters_.curSelection();
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        size_t n = selection[parametersColumns_.colIdentifier_];
        parameterExtractor_->deleteParameter(n);
        fillParameters();
    }
}
