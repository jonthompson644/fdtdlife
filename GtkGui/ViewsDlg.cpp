/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ViewsDlg.h"
#include "OkCancelDialog.h"
#include "ViewDlg.h"
#include "FdtdLife.h"

// Constructor
ViewsDlg::ViewsDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Views"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("ViewsDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<ViewDlg>(
            FdtdLife::viewModeNormal));
    // Initialise
    setButtons({newButton_, deleteButton_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &ViewsDlg::onNewView));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &ViewsDlg::onDeleteView));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &ViewsDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &ViewsDlg::onItemSelected));
}

// Fill the controls with their values
void ViewsDlg::populate() {
    fillViewList(model_.findView(currentItem()));
}

// Fill the view list
void ViewsDlg::fillViewList(ViewFrame* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    for(auto& view : model_.views()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = view->identifier();
        row[columns_.name_] = view->name();
        row[columns_.mode_] = model_.viewFactory().modeName(view->mode());
        if(view.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new view
void ViewsDlg::onNewView() {
    ViewFrame* view = model_.makeView(FdtdLife::viewModeNormal);
    std::stringstream name;
    name << "View " << view->identifier();
    view->name(name.str());
    fillViewList(view);
    model_.doNotification(FdtdLife::notifyMinorChange);
}

// Delete the currently selected view
void ViewsDlg::onDeleteView() {
    int id = currentItem();
    if(id >= 0) {
        auto* view = model_.findView(id);
        auto* nextView = model_.findView(nextItem());
        if(nextView == nullptr) {
            nextView = model_.findView(prevItem());
        }
        if(view != nullptr) {
            std::stringstream s;
            s << "Ok to delete view \"" << view->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete View", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.removeView(view);
                fillViewList(nextView);
            }
        }
    }
}

// The indicated item has been changed
void ViewsDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        ViewFrame* view = model_.findView(row[columns_.id_]);
        if(view != nullptr) {
            view->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void ViewsDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        ViewFrame* view = model_.findView((*row)[columns_.id_]);
        if(view != nullptr) {
            newDialog = dlgFactory_.make(this, view);
        }
    }
    setSubDialog(newDialog);
}
