/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Box/Utility.h>
#include <Xml/DomDocument.h>
#include <Xml/Writer.h>
#include <Xml/Reader.h>
#include <Xml/Exception.h>
#include <gtkmm/cellrenderercombo.h>
#include "ParametersDlg.h"
#include "OkCancelDialog.h"

// Constructor
ParametersDlg::ParametersDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        model_(model),
        parentWindow_(parentWindow) {
    // Initialise widgets
    addElements({tables_, tableButtons_, bottomHalf_});
    tables_.setColumns(&tablesColumns_);
    tables_.appendColumn("Id", tablesColumns_.colIdentifier_);
    tables_.appendColumnEditable("Name", tablesColumns_.colName_);
    tables_.appendColumnEditable("Unit Cell (m)", tablesColumns_.colUnitCell_);
    tables_.appendColumnEditable("Layer Spacing (m)", tablesColumns_.colLayerSpacing_);
    tables_.appendColumnEditable("Incidence Angle (deg)",
                                 tablesColumns_.colIncidenceAngle_);
    tables_.appendColumnEditable("Patch Ratio",
                                 tablesColumns_.colPatchRatio_);
    tables_.set_hexpand(true);
    tables_.set_vexpand(true);
    curveWhichModel_ = Gtk::ListStore::create(curveWhichColumns_);
    for(auto name : domain::ParameterCurve::whichNames_) {
        auto row = *curveWhichModel_->append();
        row[curveWhichColumns_.colName_] = name;
    }
    curves_.setColumns(&curvesColumns_);
    curves_.appendColumn("Id", curvesColumns_.colIdentifier_);
    int colCount = curves_.listWidget().append_column("Which", curveWhichRenderer_);
    auto whichCol = curves_.listWidget().get_column(colCount - 1);
    whichCol->add_attribute(curveWhichRenderer_.property_text(),
                            curvesColumns_.colWhich_);
    curveWhichRenderer_.set_property("editable", true);
    curveWhichRenderer_.set_property("model", curveWhichModel_);
    curveWhichRenderer_.set_property("text-column", 0);
    curveWhichRenderer_.set_property("has-entry", false);
    curves_.set_hexpand(true);
    curves_.set_vexpand(true);
    points_.setColumns(&pointsColumns_);
    points_.appendColumn("Id", pointsColumns_.colIdentifier_);
    points_.appendColumnEditable("Frequency (Hz)", pointsColumns_.colFrequency_);
    points_.appendColumnEditable("Real Value", pointsColumns_.colRealValue_);
    points_.appendColumnEditable("Imag Value", pointsColumns_.colImagValue_);
    points_.set_hexpand(true);
    points_.set_vexpand(true);
    populate();
    // Connect handlers
    curveWhichRenderer_.signal_edited().connect(
            sigc::mem_fun(this, &ParametersDlg::onWhichEdited));
    newTable_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onNewTable));
    deleteTable_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onDeleteTable));
    copyTable_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onCopyTable));
    copyAllTables_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onCopyAllTables));
    pasteTable_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onPasteTable));
    pasteExcel_.doSignal().connect(
            sigc::mem_fun(this, &ParametersDlg::onPasteExcel));
    clearTables_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onClearTables));
    fillDown_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onFillDown));
    tables_.model()->signal_row_changed().connect(
            sigc::mem_fun(this, &ParametersDlg::onTableChanged));
    tables_.selection()->signal_changed().connect(
            sigc::mem_fun(*this, &ParametersDlg::onTableSelected));
    newCurve_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onNewCurve));
    deleteCurve_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onDeleteCurve));
    copyCurve_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onCopyCurve));
    pasteCurve_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onPasteCurve));
    clearCurves_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onClearCurves));
    curves_.model()->signal_row_changed().connect(
            sigc::mem_fun(this, &ParametersDlg::onCurveChanged));
    curves_.selection()->signal_changed().connect(
            sigc::mem_fun(this, &ParametersDlg::onCurveSelected));
    newPoint_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onNewPoint));
    deletePoint_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onDeletePoint));
    clearPoints_.signal_clicked().connect(
            sigc::mem_fun(this, &ParametersDlg::onClearPoints));
    points_.model()->signal_row_changed().connect(
            sigc::mem_fun(this, &ParametersDlg::onPointChanged));
}

// Populate the widgets with values
void ParametersDlg::populate() {
    populateTables(currentTable());
}

// Add a new table
void ParametersDlg::onNewTable() {
    auto* t = model_.d()->parameterTables().make();
    if(t != nullptr) {
        InitialisingGui::Set s(initialising_);
        std::stringstream name;
        name << "Table " << t->identifier();
        t->name(name.str());
        populateTables(t);
        model_.doNotification(FdtdLife::notifyMinorNoViews);
    }
}

// Delete a table
void ParametersDlg::onDeleteTable() {
    auto* table = currentTable();
    if(table != nullptr) {
        auto* nextSel = nextTable();
        if(table != nullptr) {
            std::stringstream s;
            s << "Ok to delete dielectric parameter table \"" << table->name() << "\"?";
            OkCancelDialog box(parentWindow_,
                               "Delete Parameter Table", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.d()->parameterTables().remove(table);
                populateTables(nextSel);
                model_.doNotification(FdtdLife::notifyMinorNoViews);
            }
        }
    }
}

// Add a new curve to a parameter table
void ParametersDlg::onNewCurve() {
    auto* table = currentTable();
    if(table != nullptr) {
        auto* c = table->make();
        if(c != nullptr) {
            populateCurves(c);
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Delete a curve
void ParametersDlg::onDeleteCurve() {
    auto* curve = currentCurve();
    auto* table = currentTable();
    if(curve != nullptr && table != nullptr) {
        auto* nextSel = nextCurve();
        std::stringstream s;
        s << "Ok to delete parameter curve \""
          << curve->identifier() << "\"?";
        OkCancelDialog box(parentWindow_,
                           "Delete Curve", s.str(),
                           model_.testSuite());
        if(box.ask()) {
            table->remove(curve);
            populateCurves(nextSel);
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Add a point to a curve
void ParametersDlg::onNewPoint() {
    auto* curve = currentCurve();
    if(curve != nullptr) {
        auto* point = curve->make();
        if(point != nullptr) {
            populatePoints(point);
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Delete a point from a curve
void ParametersDlg::onDeletePoint() {
    auto* curve = currentCurve();
    auto* point = currentPoint();
    if(curve != nullptr && point != nullptr) {
        auto* nextSel = nextPoint();
        std::stringstream s;
        s << "Ok to delete parameter point for \""
          << point->frequency() << "Hz\"?";
        OkCancelDialog box(parentWindow_,
                           "Delete Point", s.str(),
                           model_.testSuite());
        if(box.ask()) {
            curve->remove(point);
            populatePoints(nextSel);
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Paste excel text from the clipboard
// The excel tabbed text format is:
//     <ignored> <patchRatio%> ... <patchRatio%>
//     <frequencyGHz> <refractiveIndex> ... <refractiveIndex>
//     ...
//     <frequencyGHz> <refractiveIndex> ... <refractiveIndex>
void ParametersDlg::onPasteExcel(domain::ParameterCurve::Which which) {
    for(auto& target : model_.clipboard().targets()) {
        std::cout << "\"" << target << "\"" << std::endl;
    }
    if(model_.clipboard().hasTarget("UTF8_STRING")) {
        domain::ParameterTable* table = nullptr;
        std::string text = model_.clipboard().getText("UTF8_STRING");
        std::list<domain::ParameterCurve*> curves;
        // Split into lines
        std::vector<std::string> lines = box::Utility::split(text, "\n");
        bool firstLine = true;
        for(const auto& line : lines) {
            auto curvePos = curves.begin();
            // Split the line into columns
            std::vector<std::string> cols = box::Utility::split(line, "\t");
            bool firstCol = true;
            double frequency = 0.0;
            for(auto& col : cols) {
                if(firstLine) {
                    if(firstCol) {
                        // Skip the frequency column header
                    } else {
                        // Make a table for this patch ratio
                        table = model_.d()->parameterTables().make();
                        std::stringstream name;
                        name << "Table " << table->identifier();
                        table->name(name.str());
                        box::Utility::stripBack(col, "%");
                        table->patchRatio(box::Utility::toDouble(col));
                        // And make a curve and save it
                        curves.push_back(table->make());
                        curves.back()->which(which);
                    }
                } else {
                    if(firstCol) {
                        // Remember the frequency of this row
                        frequency = box::Utility::toDouble(col) * box::Constants::giga_;
                    } else {
                        // Make an entry for this point
                        auto* point = (*curvePos)->make();
                        point->frequency(frequency);
                        point->value(box::Utility::toComplex(col));
                        curvePos++;
                    }
                }
                firstCol = false;
            }
            firstLine = false;
        }
        populateTables(table);
    }
}

// Fill the table list
void ParametersDlg::populateTables(domain::ParameterTable* sel) {
    InitialisingGui::Set s(initialising_);
    tables_.model()->clear();
    bool selectFirst = sel == nullptr;
    for(auto& table : model_.d()->parameterTables()) {
        auto row = *(tables_.model()->append());
        row[tablesColumns_.colName_] = table->name();
        row[tablesColumns_.colUnitCell_] = table->unitCell();
        row[tablesColumns_.colLayerSpacing_] = table->layerSpacing();
        row[tablesColumns_.colIdentifier_] = table->identifier();
        row[tablesColumns_.colIncidenceAngle_] = table->incidenceAngle();
        row[tablesColumns_.colPatchRatio_] = table->patchRatio();
        if(table.get() == sel || selectFirst) {
            tables_.selection()->select(row);
            selectFirst = false;
        }
    }
}

// Fill the curves list view from the current table
void ParametersDlg::populateCurves(domain::ParameterCurve* sel) {
    InitialisingGui::Set s(initialising_);
    curves_.model()->clear();
    auto* table = currentTable();
    bool selectFirst = sel == nullptr;
    if(table != nullptr) {
        for(auto& curve : *table) {
            auto row = *(curves_.model()->append());
            row[curvesColumns_.colIdentifier_] = curve->identifier();
            row[curvesColumns_.colWhich_] = domain::ParameterCurve::whichName(curve->which());
            if(curve.get() == sel || selectFirst) {
                curves_.selection()->select(row);
                selectFirst = false;
            }
        }
    }
}

// Fill the points list view from the current curve
void ParametersDlg::populatePoints(domain::ParameterPoint* sel) {
    InitialisingGui::Set s(initialising_);
    points_.model()->clear();
    auto* curve = currentCurve();
    bool selectFirst = sel == nullptr;
    if(curve != nullptr) {
        for(auto& point : *curve) {
            auto row = *(points_.model()->append());
            row[pointsColumns_.colIdentifier_] = point->identifier();
            row[pointsColumns_.colFrequency_] = point->frequency();
            row[pointsColumns_.colRealValue_] = point->value().real();
            row[pointsColumns_.colImagValue_] = point->value().imag();
            if(point.get() == sel || selectFirst) {
                points_.selection()->select(row);
                selectFirst = false;
            }
        }
    }
}

// Return the currently selected table
domain::ParameterTable* ParametersDlg::currentTable() {
    domain::ParameterTable* result = nullptr;
    auto rowPos = tables_.curSelection();
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[tablesColumns_.colIdentifier_];
        result = model_.d()->parameterTables().find(id);
    }
    return result;
}

// Return the table after (or before) the current selection
domain::ParameterTable* ParametersDlg::nextTable() {
    domain::ParameterTable* result = nullptr;
    auto rowPos = tables_.nextSelection();
    if(!rowPos) {
        rowPos = tables_.prevSelection();
    }
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[tablesColumns_.colIdentifier_];
        result = model_.d()->parameterTables().find(id);
    }
    return result;
}

// Return the current selected curve
domain::ParameterCurve* ParametersDlg::currentCurve() {
    domain::ParameterCurve* result = nullptr;
    auto table = currentTable();
    auto rowPos = curves_.curSelection();
    if(table && rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[curvesColumns_.colIdentifier_];
        result = table->find(id);
    }
    return result;
}

// Return the curve after (or before) the currently selected
domain::ParameterCurve* ParametersDlg::nextCurve() {
    domain::ParameterCurve* result = nullptr;
    auto table = currentTable();
    auto rowPos = curves_.nextSelection();
    if(!rowPos) {
        rowPos = curves_.prevSelection();
    }
    if(table && rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[curvesColumns_.colIdentifier_];
        result = table->find(id);
    }
    return result;
}

// Return the current selected point
domain::ParameterPoint* ParametersDlg::currentPoint() {
    domain::ParameterPoint* result = nullptr;
    auto* curve = currentCurve();
    auto rowPos = points_.curSelection();
    if(curve && rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[pointsColumns_.colIdentifier_];
        result = curve->find(id);
    }
    return result;
}

// Return the point after (or before) the currently selected
domain::ParameterPoint* ParametersDlg::nextPoint() {
    domain::ParameterPoint* result = nullptr;
    auto* curve = currentCurve();
    auto rowPos = points_.nextSelection();
    if(!rowPos) {
        rowPos = points_.prevSelection();
    }
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[pointsColumns_.colIdentifier_];
        result = curve->find(id);
    }
    return result;
}

// Handle edits made in the tables_ list view
void ParametersDlg::onTableChanged(const Gtk::TreeModel::Path& /*path*/,
                                   const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto row = *pos;
        int id = row[tablesColumns_.colIdentifier_];
        auto* table = model_.d()->parameterTables().find(id);
        if(table != nullptr) {
            InitialisingGui::Set s(initialising_);
            table->name(row[tablesColumns_.colName_]);
            table->unitCell(row[tablesColumns_.colUnitCell_]);
            table->layerSpacing(row[tablesColumns_.colLayerSpacing_]);
            table->incidenceAngle(row[tablesColumns_.colIncidenceAngle_]);
            table->patchRatio(row[tablesColumns_.colPatchRatio_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// Handle edits made in the curves_ list view
void ParametersDlg::onCurveChanged(const Gtk::TreeModel::Path& /*path*/,
                                   const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto* table = currentTable();
        if(table != nullptr) {
            auto row = *pos;
            int id = row[curvesColumns_.colIdentifier_];
            auto* curve = table->find(id);
            if(curve != nullptr) {
                InitialisingGui::Set s(initialising_);
                auto which = domain::ParameterCurve::whichValue(
                        row[curvesColumns_.colWhich_]);
                curve->which(which);
                model_.doNotification(FdtdLife::notifyMinorChange);
            }
        }
    }
}

// Handle edits made in the points_ list view
void ParametersDlg::onPointChanged(const Gtk::TreeModel::Path& /*path*/,
                                   const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto* curve = currentCurve();
        if(curve != nullptr) {
            auto row = *pos;
            int id = row[pointsColumns_.colIdentifier_];
            auto* point = curve->find(id);
            if(point != nullptr) {
                InitialisingGui::Set s(initialising_);
                point->frequency(row[pointsColumns_.colFrequency_]);
                point->value({row[pointsColumns_.colRealValue_],
                              row[pointsColumns_.colImagValue_]});
                model_.doNotification(FdtdLife::notifyMinorChange);
            }
        }
    }
}

// The table selection has changed
void ParametersDlg::onTableSelected() {
    auto* sel = currentCurve();
    populateCurves(sel);
}

// The curve selection has changed
void ParametersDlg::onCurveSelected() {
    auto* sel = currentPoint();
    populatePoints(sel);
}

// Copy the current table to the clipboard
void ParametersDlg::onCopyTable() {
    auto* table = currentTable();
    if(table) {
        // Convert to XML
        auto* o = new xml::DomObject("parametertable");
        *o << *table;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Place on the clipboard
        model_.clipboard().put(text, "fdtd/parametertable");
    }
}

// Copy all the tables to the clipboard
void ParametersDlg::onCopyAllTables() {
    // Convert to XML
    auto* o = new xml::DomObject("parametertables");
    *o << model_.d()->parameterTables();
    xml::DomDocument dom(o);
    xml::Writer writer;
    std::string text;
    writer.writeString(&dom, text);
    // Place on the clipboard
    model_.clipboard().put(text, "fdtd/parametertables");
}

// Paste a table from the clipboard
void ParametersDlg::onPasteTable() {
    std::string objName;
    std::string text;
    if(model_.clipboard().hasTarget("fdtd/parametertable")) {
        text = model_.clipboard().getText("fdtd/parametertable");
        objName = "parametertable";
    } else if(model_.clipboard().hasTarget("fdtd/parametertables")) {
        text = model_.clipboard().getText("fdtd/parametertables");
        objName = "parametertables";
    }
    if(!objName.empty()) {
        try {
            // Read the XML
            xml::DomDocument dom(objName);
            xml::Reader reader;
            reader.readString(&dom, text);
            // Deserialize
            if(objName == "parametertable") {
                auto table = model_.d()->parameterTables().make();
                *dom.getObject() >> *table;
                populateTables(table);
            } else if(objName == "parametertables") {
                for(auto& o : dom.getObject()->obj("parametertable")) {
                    auto table = model_.d()->parameterTables().make();
                    *o >> *table;
                }
                populateTables();
            }
            model_.doNotification(FdtdLife::notifyMinorChange);
        } catch(xml::Exception& e) {
            std::cout << "Parameter table paste failed to read XML: " << e.what() << std::endl;
        }
    }
}

// Copy a curve to the clipboard
void ParametersDlg::onCopyCurve() {
    auto* curve = currentCurve();
    if(curve) {
        // Convert to XML
        auto* o = new xml::DomObject("parametercurve");
        *o << *curve;
        xml::DomDocument dom(o);
        xml::Writer writer;
        std::string text;
        writer.writeString(&dom, text);
        // Place on the clipboard
        model_.clipboard().put(text, "fdtd/parametercurve");
    }
}

// Paste a curve from the clipboard
void ParametersDlg::onPasteCurve() {
    std::string objName;
    std::string text;
    if(model_.clipboard().hasTarget("fdtd/parametercurve")) {
        text = model_.clipboard().getText("fdtd/parametercurve");
        objName = "parametercurve";
    }
    auto* table = currentTable();
    if(!objName.empty() && table) {
        try {
            // Read the XML
            xml::DomDocument dom(objName);
            xml::Reader reader;
            reader.readString(&dom, text);
            // Deserialize
            if(objName == "parametercurve") {
                auto curve = table->make();
                *dom.getObject() >> *curve;
                populateCurves(curve);
            }
            model_.doNotification(FdtdLife::notifyMinorChange);
        } catch(xml::Exception& e) {
            std::cout << "Parameter table paste failed to read XML: " << e.what()
                      << std::endl;
        }
    }
}

// A curve which combo box has been edited
void ParametersDlg::onWhichEdited(const std::string& path,
                                  const std::string& newText) {
    std::cout << "which edited " << path << " " << newText << std::endl;
    auto row = *curves_.model()->get_iter(path);
    row[curvesColumns_.colWhich_] = newText;
}

// Clear all the tables
void ParametersDlg::onClearTables() {
    OkCancelDialog box(parentWindow_,
                       "Clear Parameter Tables",
                       "Ok to delete all dielectric parameter tables?",
                       model_.testSuite());
    if(box.ask()) {
        model_.d()->parameterTables().clear();
        populateTables();
        model_.doNotification(FdtdLife::notifyMinorNoViews);
    }
}

// Clear all the curves for the current table
void ParametersDlg::onClearCurves() {
    auto* table = currentTable();
    if(table != nullptr) {
        OkCancelDialog box(parentWindow_,
                           "Clear Curves",
                           "Ok to delete all dielectric parameter curves?",
                           model_.testSuite());
        if(box.ask()) {
            table->clear();
            populateCurves();
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Clear all the points for the current curve
void ParametersDlg::onClearPoints() {
    auto* curve = currentCurve();
    if(curve != nullptr) {
        OkCancelDialog box(parentWindow_,
                           "Clear Points",
                           "Ok to delete all dielectric curve points?",
                           model_.testSuite());
        if(box.ask()) {
            curve->clear();
            populatePoints();
            model_.doNotification(FdtdLife::notifyMinorNoViews);
        }
    }
}

// Copy the unit cell, layer spacing and incidence angle values
// from the current table to all following tables.
void ParametersDlg::onFillDown() {
    auto rowPos = tables_.curSelection();
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[tablesColumns_.colIdentifier_];
        auto from = model_.d()->parameterTables().find(id);
        rowPos++;
        while(rowPos) {
            id = (*rowPos)[tablesColumns_.colIdentifier_];
            auto to = model_.d()->parameterTables().find(id);
            to->unitCell(from->unitCell());
            to->layerSpacing(from->layerSpacing());
            to->incidenceAngle(from->incidenceAngle());
            rowPos++;
        }
        populateTables(from);
        model_.doNotification(FdtdLife::notifyMinorNoViews);
    }
}
