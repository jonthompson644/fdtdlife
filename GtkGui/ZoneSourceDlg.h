/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ZONESOURCEDLG_H
#define FDTDLIFE_ZONESOURCEDLG_H

#include <Source/ZoneSource.h>
#include "SourcesDlg.h"
#include "NamedEntry.h"
#include "NamedTripleEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"

// A dialog panel that shows the zone source parameters
class ZoneSourceDlg : public SourcesDlg::SubDlg {
public:
    // Constructor
    ZoneSourceDlg() = default;
    void construct(SourcesDlg* owner, source::Source* source) override;

    // API
    void populate() override;

protected:
    // Members
    source::ZoneSource* source_{};

    // The widgets
    NamedEntry<box::Expression> firstFrequency_{"First Frequency (Hz)"};
    NamedEntry<box::Expression> frequencyStep_{"Frequency Step (Hz)"};
    NamedEntry<box::Expression> numFrequencies_{"Number Of Frequencies"};
    NamedEntry<double> amplitude_{"Electric Current Density (A/m)"};
    NamedEntry<double> polarisation_{"Polarisation (0..360 deg)"};
    NamedEntry<double> azimuth_{"Azimuth (0..90 deg)"};
    NamedEntry<double> elevation_{"Elevation (-90..90 deg)"};
    NamedEntry<double> initialTime_{"Initial Time (s)"};
    NamedEntry<double> runFor_{"Run For (s)"};
    NamedToggleButton continuous_{"Continuous"};
    NamedTripleEntry<double> zoneCenter_{"Zone Center (m)"};
    NamedTripleEntry<double> zoneSize_{"Zone Size (m)"};

    // Handlers
    void onChange();
};


#endif //FDTDLIFE_ZONESOURCEDLG_H
