/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "TwoDSliceWidget.h"
#include "extKindlmannColors.h"
#include "materialColors.h"
#include "divergingBYColors.h"

// Set the data
void TwoDSliceWidget::data(size_t xSize, size_t ySize, const std::vector<double>& d) {
    data_ = d;
    xSize_ = xSize;
    ySize_ = ySize;
    data_.resize(xSize_ * ySize_);
    queue_draw();
}

// Set the configuration
void TwoDSliceWidget::config(double displayMax, ColourMap colourMap) {
    displayMax_ = displayMax;
    colourMap_ = colourMap;
    loadColours();
    queue_draw();
}

// Draw the data into the widget
void TwoDSliceWidget::draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) {
    drawBackground(cr, width, height);
    if(dataValid()) {
        calcScaling(width, height);
        drawData(cr, width, height);
    }
}

// Fill the background of the panel
void TwoDSliceWidget::drawBackground(const Cairo::RefPtr<Cairo::Context>& cr,
                                     int width, int height) {
    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->rectangle(0.0, 0.0, width, height);
    cr->fill();
}

// Checks the data supplied for validity
bool TwoDSliceWidget::dataValid() {
    if(numColours_ == 0) {
        loadColours();
    }
    return xSize_ > 0 && ySize_ > 0 && data_.size() == xSize_ * ySize_
           && numColours_ > 0 && !colours_.empty();
}

// Calculate the scaling factors
void TwoDSliceWidget::calcScaling(int width, int height) {
    // Work out the area scaling
    double pixelsPerCellX = static_cast<double>(width) / static_cast<double>(xSize_);
    double pixelsPerCellY = static_cast<double>(height) / static_cast<double>(ySize_);
    pixelsPerCell_ = std::min(pixelsPerCellX, pixelsPerCellY);
    topLeftX_ = (width - pixelsPerCell_ * xSize_) / 2.0;
    topLeftY_ = (height - pixelsPerCell_ * ySize_) / 2.0;
    drawCellBorders_ = pixelsPerCell_ > 8;
}

// Load the colours
void TwoDSliceWidget::loadColours() {
    // Get the colour scale
    const Rgb* cols;
    switch(colourMap_) {
        case ColourMap::extKindlemann:
            numColours_ = NUMEXTKINDLEMANNCOLORS;
            cols = extKindlemannColors;
            scale_ = displayMax_ / numColours_;
            offset_ = 0.0;
            break;
        case ColourMap::divergingBY:
            numColours_ = NUMDIVERGINGBYCOLORS;
            cols = divergingBYColors;
            scale_ = displayMax_ * 2.0 / numColours_;
            offset_ = -displayMax_;
            break;
        case ColourMap::material:
            numColours_ = NUMMATERIALCOLORS;
            cols = materialColors;
            scale_ = 1.0;
            offset_ = 0.0;
            break;
    }
    colours_.resize(numColours_);
    for(size_t i = 0; i < numColours_; i++) {
        colours_[i].r_ = cols[i].r_ / 256.0;
        colours_[i].g_ = cols[i].g_ / 256.0;
        colours_[i].b_ = cols[i].b_ / 256.0;
    }
}

// Draw the data
void TwoDSliceWidget::drawData(const Cairo::RefPtr<Cairo::Context>& cr,
                               int /*width*/, int /*height*/) {
    // For each cell...
    double x = topLeftX_;
    double y = topLeftY_;
    for(size_t j = 0; j < ySize_; j++) {
        for(size_t i = 0; i < xSize_; i++) {
            double value = data_[j * xSize_ + i];
            size_t col = static_cast<size_t>(
                    std::max(0.0, std::round((value - offset_) / scale_)));
            col = std::min(col, numColours_ - 1);
            cr->rectangle(x, y, pixelsPerCell_, pixelsPerCell_);
            if(drawCellBorders_) {
                cr->set_source_rgb(0.0, 0.0, 1.0);
            } else {
                cr->set_source_rgb(colours_[col].r_, colours_[col].g_, colours_[col].b_);
            }
            cr->stroke_preserve();
            cr->set_source_rgb(colours_[col].r_, colours_[col].g_, colours_[col].b_);
            cr->fill();
            x += pixelsPerCell_;
        }
        x = topLeftX_;
        y += pixelsPerCell_;
    }
}
