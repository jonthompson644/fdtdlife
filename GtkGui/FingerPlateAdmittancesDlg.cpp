/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "FingerPlateAdmittancesDlg.h"

// Constructor
FingerPlateAdmittancesDlg::FingerPlateAdmittancesDlg(Gtk::Window& parentWindow, domain::FingerPlate* shape) :
        Dialog("Admittance Tables", parentWindow, true),
        shape_(shape) {
    GuiElement::registerChild(layout_);
    GuiElement::initialiseGuiElements(true);
    add_button("Close", Gtk::RESPONSE_CLOSE);
    get_content_area()->add(layout_);
    show_all_children();
    tables_.setColumns(&tablesColumns_);
    tables_.appendColumn("Name", tablesColumns_.colName_);
    tables_.appendColumn("Unit Cell (m)", tablesColumns_.colUnitCell_);
    tables_.appendColumn("Patch Ratio", tablesColumns_.colPatchRatio_);
    tables_.appendColumn("Min Feature (%)", tablesColumns_.colMinFeatureSize_);
    tables_.set_hexpand(true);
    tables_.set_vexpand(true);
}

// Run the modal dialog
void FingerPlateAdmittancesDlg::doModal() {
    Gtk::Dialog::run();
}

void FingerPlateAdmittancesDlg::populate(domain::FingerPlate::ShapeAdmittance* /*sel*/) {
#if 0
    if(sel == nullptr) {
        //sel = currentScanner();
    }
    {
        InitialisingGui::Set s(initialising_);
        tables_.model()->clear();
        for (auto &table: shape_->scanners()) {
            auto model = tables_.model();
            auto rowp = model->append();
            auto &row = *rowp;
            row[scannersColumns_.colIdentifier_] = scanner->id();
            row[scannersColumns_.colType_] = seq::toString(scanner->mode());
            row[scannersColumns_.colSteps_] = scanner->numSteps();
            if (scanner.get() == sel) {
                scanners_.selection()->select(row);
            }
        }
    }
    // Call the selected scanner handler now as it would be automatically called
    // while we had initialising activated.
    onScannerSelected();
#endif
}

// New table button handler
void FingerPlateAdmittancesDlg::onAdd() {
    shape_->admittances().emplace_back();
    populate(&shape_->admittances().back());
}

void FingerPlateAdmittancesDlg::onDelete() {

}

void FingerPlateAdmittancesDlg::onEdit() {

}

void FingerPlateAdmittancesDlg::onPaste() {

}

void FingerPlateAdmittancesDlg::onCopy() {

}

void FingerPlateAdmittancesDlg::onDeleteAll() {

}

void FingerPlateAdmittancesDlg::onFillDown() {

}

