/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensTransmissionDlg.h"
#include "GuiChangeDetector.h"

// Constructor
LensTransmissionDlg::LensTransmissionDlg() :
        LensAnalyserDlg("V/m") {
}

// Second stage constructor used by the factory
void LensTransmissionDlg::construct(AnalysersDlg* owner, sensor::Analyser* analyser) {
    comment("LensTransmission");
    analyser_ = dynamic_cast<sensor::LensTransmissionAnalyser*>(analyser);
    configLayout_.addElements({extraLine1_});
    LensAnalyserDlg::construct(owner, analyser);
}

// Populate all the widgets with values
void LensTransmissionDlg::populate() {
    LensAnalyserDlg::populate();
    InitialisingGui::Set s(initialising_);
    frequency_.value(analyser_->frequency());
    gaussianA_.value(analyser_->gaussianA());
    gaussianC_.value(analyser_->gaussianC());
}

// Default change handler
void LensTransmissionDlg::onChange() {
    LensAnalyserDlg::onChange();
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newFrequency = c.test(frequency_, analyser_->frequency(),
                                   FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            analyser_->frequency(newFrequency);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Handle a notification
void LensTransmissionDlg::notify(fdtd::Notifier* source, int why,
                             fdtd::NotificationData* data) {
    // Base class first
    LensAnalyserDlg::notify(source, why, data);
    // Now my stuff
    if(source == &dlg_->model() && why == fdtd::Model::notifyAnalysersCalculate) {
        gaussianA_.value(analyser_->gaussianA());
        gaussianC_.value(analyser_->gaussianC());
    }
}

