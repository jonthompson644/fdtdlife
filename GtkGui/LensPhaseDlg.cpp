/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensPhaseDlg.h"
#include "GuiChangeDetector.h"

// Constructor
LensPhaseDlg::LensPhaseDlg() :
        LensAnalyserDlg("rad") {
}

// Second stage constructor used by the factory
void LensPhaseDlg::construct(AnalysersDlg* owner, sensor::Analyser* analyser) {
    comment("LensPhase");
    analyser_ = dynamic_cast<sensor::LensPhaseAnalyser*>(analyser);
    LensAnalyserDlg::construct(owner, analyser);
}

// Populate all the widgets with values
void LensPhaseDlg::populate() {
    LensAnalyserDlg::populate();
    InitialisingGui::Set s(initialising_);
    frequency_.value(analyser_->frequency());
}

// Default change handler
void LensPhaseDlg::onChange() {
    LensAnalyserDlg::onChange();
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newFrequency = c.test(frequency_, analyser_->frequency(),
                                   FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            analyser_->frequency(newFrequency);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
