/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDSELECTION_H
#define FDTDLIFE_NAMEDSELECTION_H

#include <gtkmm/label.h>
#include <gtkmm/comboboxtext.h>
#include "GuiElement.h"

// A class that binds a label with a drop list selection
template<class T>
class NamedSelection : public Gtk::ComboBoxText, public GuiElement {
public:
    // Construction
    NamedSelection() = delete;
    NamedSelection(const std::string& name,
                   const std::initializer_list<const char*>& values) {
        // Initialise them
        nameLabel_.set_label(name);
        nameLabel_.get_style_context()->add_class("namedentryaligned");
        nameLabel_.set_halign(Gtk::ALIGN_END);
        int n = 0;
        int m = 0;
        for(auto& v : values) {
            if(strlen(v) != 0) {
                convert_[n] = static_cast<T>(m);
                append(v);
                n++;
            }
            m++;
        }
        // Register
        registerItem(name, *this);
    }

    // Accessors
    virtual void value(T value) {
        for(auto& c : convert_) {
            if(c.second == value) {
                set_active(c.first);
            }
        }
    }
    virtual T value() const {
        return convert_.at(get_active_row_number());
    }

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void connectChangeHandler(const sigc::slot<void>& slot) override {
        if(curConnection_.connected()) {
            curConnection_.disconnect();
        }
        curConnection_ = signal_changed().connect(slot);
    }
    Gtk::Widget* leftWidget() override { return &nameLabel_; }

protected:
    // Members
    Gtk::Label nameLabel_;  // The name attached to the value
    sigc::connection curConnection_;  // The current connection
    std::map<int, T> convert_;  // Conversion from widget value to enumeration
};

#endif //FDTDLIFE_NAMEDSELECTION_H
