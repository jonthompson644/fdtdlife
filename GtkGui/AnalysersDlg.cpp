/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "AnalysersDlg.h"
#include "OkCancelDialog.h"
#include "LensPhaseDlg.h"
#include "LensReflectionDlg.h"
#include "LensTransmissionDlg.h"
#include <Sensor/Analysers.h>

// Constructor
AnalysersDlg::AnalysersDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Analysers"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("AnalysersDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensPhaseDlg>(
            sensor::Analysers::modeLensPhase));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensReflectionDlg>(
            sensor::Analysers::modeLensReflection));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<LensTransmissionDlg>(
            sensor::Analysers::modeLensTransmission));
    // Initialise
    setButtons({newButton_, deleteButton_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(*this, &AnalysersDlg::onNewAnalyser));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(*this, &AnalysersDlg::onDeleteAnalyser));
    itemChangedSignal().connect(
            sigc::mem_fun(*this, &AnalysersDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(*this, &AnalysersDlg::onItemSelected));
}

// Fill the controls with their values
void AnalysersDlg::populate() {
    fillAnalyserList(model_.analysers().find(currentItem()));
}

// Fill the analyser list
void AnalysersDlg::fillAnalyserList(sensor::Analyser* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    for(auto& analyser : model_.analysers()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = analyser->identifier();
        row[columns_.name_] = analyser->name();
        row[columns_.mode_] = model_.analysers().factory().modeName(analyser->mode());
        if(analyser.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new analyser
void AnalysersDlg::onNewAnalyser(int mode) {
    sensor::Analyser* analyser = model_.analysers().make(mode);
    std::stringstream name;
    name << "Analyser " << analyser->identifier();
    analyser->name(name.str());
    fillAnalyserList(analyser);
    model_.doNotification(FdtdLife::notifyDomainContentsChange);
}

// Delete the currently selected analyser
void AnalysersDlg::onDeleteAnalyser() {
    int id = currentItem();
    if(id >= 0) {
        auto* analyser = model_.analysers().find(id);
        auto* nextAnalyser = model_.analysers().find(nextItem());
        if(nextAnalyser == nullptr) {
            nextAnalyser = model_.analysers().find(prevItem());
        }
        if(analyser != nullptr) {
            std::stringstream s;
            s << "Ok to delete analyser \"" << analyser->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Analyser", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.analysers().remove(analyser);
                fillAnalyserList(nextAnalyser);
            }
        }
    }
}

// The indicated item has been changed
void AnalysersDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        sensor::Analyser* analyser = model_.analysers().find(row[columns_.id_]);
        if(analyser != nullptr) {
            analyser->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void AnalysersDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        sensor::Analyser* analyser = model_.analysers().find((*row)[columns_.id_]);
        if(analyser != nullptr) {
            newDialog = dlgFactory_.make(this, analyser);
        }
    }
    setSubDialog(newDialog);
}
