/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ShapesDlg.h"
#include "OkCancelDialog.h"
#include "DielectricLayerDlg.h"
#include "SquarePlateDlg.h"
#include "SquareHoleDlg.h"
#include "CuboidArrayDlg.h"
#include "FingerPlateDlg.h"
#include "BinaryPlateNxNDlg.h"
#include "FileSaveDialog.h"
#include "MaterialsSelectDlg.h"
#include <Source/SourceFactory.h>
#include <Box/DxfWriter.h>
#include <Box/HfssWriter.h>
#include <Source/Source.h>

// Constructor
ShapesDlg::ShapesDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        ItemsAndInfoLayout("Shapes"),
        model_(model),
        parentWindow_(parentWindow) {
    comment("ShapesDlg");
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<DielectricLayerDlg>(
            domain::Domain::shapeModeDielectricLayer));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<SquarePlateDlg>(
            domain::Domain::shapeModeSquarePlate));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<SquareHoleDlg>(
            domain::Domain::shapeModeSquareHole));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<CuboidArrayDlg>(
            domain::Domain::shapeModeCuboidArray));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<FingerPlateDlg>(
            domain::Domain::shapeModeFingerPlate));
    dlgFactory_.define(new box::Factory<SubDlg>::Creator<BinaryPlateNxNDlg>(
            domain::Domain::shapeModeBinaryPlateNxN));
    // Initialise
    setButtons({newButton_, deleteButton_, exportButton_});
    setColumns(columns_);
    populate();
    // Add handlers
    newButton_.createSignal().connect(
            sigc::mem_fun(this, &ShapesDlg::onNewShape));
    deleteButton_.signal_clicked().connect(
            sigc::mem_fun(this, &ShapesDlg::onDeleteShape));
    itemChangedSignal().connect(
            sigc::mem_fun(this, &ShapesDlg::onItemChanged));
    itemSelectedSignal().connect(
            sigc::mem_fun(this, &ShapesDlg::onItemSelected));
    exportButton_.doSignal().connect(
            sigc::mem_fun(this, &ShapesDlg::onExport));
}

// Fill the controls with their values
void ShapesDlg::populate() {
    fillShapeList(model_.d()->getShape(currentItem()));
}

// Fill the source list
void ShapesDlg::fillShapeList(domain::Shape* select) {
    InitialisingGui::Set s(initialising_);
    treeView_.model()->clear();
    for(auto& shape : model_.d()->shapes()) {
        auto row = *(treeView_.model()->append());
        row[columns_.id_] = shape->identifier();
        row[columns_.name_] = shape->name();
        row[columns_.mode_] = model_.d()->shapeFactory().modeName(shape->mode());
        if(shape.get() == select) {
            treeView_.get_selection()->select(row);
        }
    }
}

// Create a new shape
void ShapesDlg::onNewShape(int mode) {
    domain::Shape* shape = model_.d()->makeShape(mode);
    std::stringstream name;
    name << "Shape " << shape->identifier();
    shape->name(name.str());
    fillShapeList(shape);
    model_.doNotification(FdtdLife::notifyDomainContentsChange);
}

// Delete the currently selected shape
void ShapesDlg::onDeleteShape() {
    int id = currentItem();
    if(id >= 0) {
        auto* shape = model_.d()->getShape(id);
        auto* nextShape = model_.d()->getShape(nextItem());
        if(nextShape == nullptr) {
            nextShape = model_.d()->getShape(prevItem());
        }
        if(shape != nullptr) {
            std::stringstream s;
            s << "Ok to delete shape \"" << shape->name() << "\"?";
            OkCancelDialog box(parentWindow_, "Delete Shape", s.str(),
                               model_.testSuite());
            if(box.ask()) {
                model_.d()->remove(shape);
                fillShapeList(nextShape);
            }
        }
    }
}

// The indicated item has been changed
void ShapesDlg::onItemChanged(const Gtk::ListStore::Row& row) {
    if(!initialising_) {
        domain::Shape* shape = model_.d()->getShape(row[columns_.id_]);
        if(shape != nullptr) {
            shape->name(row[columns_.name_]);
            model_.doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// The indicated item has been selected
void ShapesDlg::onItemSelected(const Gtk::ListStore::Row* row) {
    GridLayout* newDialog = nullptr;
    if(row != nullptr) {
        domain::Shape* shape = model_.d()->getShape((*row)[columns_.id_]);
        if(shape != nullptr) {
            newDialog = dlgFactory_.make(this, shape);
        }
    }
    setSubDialog(newDialog);
}

// Handle an export command
void ShapesDlg::onExport(ShapesDlg::ExportCmds cmd) {
    // Get a filename
    std::string extension;
    std::string filterName;
    std::string title;
    switch(cmd) {
        case ExportCmds::allHfss:
            title = "Export shapes to HFSS python file";
            extension = "py";
            filterName = "HFSS Python Files";
            break;
        case ExportCmds::allDxf:
            title = "Export shapes to a DXF file";
            extension = "dxf";
            filterName = "DXF Files";
            break;
    }
    std::string fileName = box::Utility::makeFilename(
            model_.pathName(),
            model_.fileName(),
            extension);
    FileSaveDialog box(parentWindow_, title, fileName,
                       model_.testSuite(), extension, filterName);
    std::string selectedFileName;
    if(box.ask(selectedFileName)) {
        MaterialsSelectDlg matBox(parentWindow_, title, model_.d(),
                                  model_.testSuite());
        std::set<int> selectedMats;
        if(matBox.ask(selectedMats)) {
            switch(cmd) {
                case ExportCmds::allHfss: {
                    box::HfssWriter hfss(
                            selectedFileName,
                            box::HfssWriter::Clipping::none,
                            model_.p()->boundary().x() ==
                            fdtd::Configuration::Boundary::periodic,
                            model_.p()->boundary().y() ==
                            fdtd::Configuration::Boundary::periodic,
                            model_.sources().sources().size() == 1
                            && model_.sources().sources().front()->mode() ==
                               source::SourceFactory::modePlaneWave);
                    model_.writeExport(hfss, selectedMats);
                    break;
                }
                case ExportCmds::allDxf: {
                    box::DxfWriter dxf(selectedFileName);
                    model_.writeExport(dxf, selectedMats);
                    break;
                }
            }
        }
    }
}
