/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDLISTVIEW_H
#define FDTDLIFE_NAMEDLISTVIEW_H

#include <gtkmm/grid.h>
#include "GuiElement.h"
#include "ListWidget.h"
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/label.h>
#include <utility>
#include <Fdtd/Variable.h>

// A named list view with scrolling capability
class NamedListView : public Gtk::Grid, public GuiElement {
public:
    // Construction
    explicit NamedListView(const std::string& name);
    NamedListView();

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void initialiseGuiElements(bool recursive) override;

    // API
    template<class T>
    int appendColumn(const std::string& name, const Gtk::TreeModelColumn<T>& col) {
        return treeView_.append_column(name, col);
    }
    template<class T>
    int appendColumnEditable(const std::string& name, const Gtk::TreeModelColumn<T>& col) {
        return treeView_.append_column_editable(name, col);
    }
    Glib::RefPtr<Gtk::ListStore>& model() { return treeView_.model(); }
    Glib::RefPtr<Gtk::TreeSelection> selection() { return treeView_.get_selection(); }
    void setColumns(Gtk::TreeModel::ColumnRecord* cols) { treeView_.setColumns(cols); }
    ListWidget& listWidget() { return treeView_; }
    Gtk::TreeIter curSelection();
    Gtk::TreeIter nextSelection();
    Gtk::TreeIter prevSelection();

protected:
    // Members
    ListWidget treeView_;
    Gtk::ScrolledWindow scrolledWindow_;
    std::unique_ptr<Gtk::Label> name_;

    // Helpers
    void construct(const std::string& name);
};


#endif //FDTDLIFE_NAMEDLISTVIEW_H
