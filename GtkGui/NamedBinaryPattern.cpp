/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NamedBinaryPattern.h"

// Constructor with a name
NamedBinaryPattern::NamedBinaryPattern(const std::string& name, size_t numColsRight) :
        numColsRight_(numColsRight) {
    nameLabel_ = std::make_unique<Gtk::Label>(name);
    construct(name);
}

// Constructor without name
NamedBinaryPattern::NamedBinaryPattern(size_t numColsRight) :
        numColsRight_(numColsRight) {
    construct("");
}

// Common constructor code
void NamedBinaryPattern::construct(const std::string& name) {
    packOptions_ = Gtk::PACK_EXPAND_WIDGET;
    fourFold_.set_active(true);
    size_t row = 0;
    if(nameLabel_) {
        attach(*nameLabel_, 0, row++, 2, 1);
    }
    attach(fourFold_, 0, row);
    attach(hexadecimalCodingEntry_, 1, row++);
    attach(binaryPattern_, 0, row, 2, 1);
    hexadecimalCodingEntry_.set_editable(false);
    hexadecimalCodingEntry_.set_can_focus(false);
    binaryPattern_.show();
    // Register
    registerItem(name + ".binarypattern", binaryPattern_);
    registerItem(name + ".fourfold", fourFold_);
    registerItem(name + ".hexcoding", hexadecimalCodingEntry_);
    // Connect
    binaryPattern_.changedSignal().connect(
            sigc::mem_fun(this, &NamedBinaryPattern::onPatternChanged));
    fourFold_.signal_clicked().connect(
            sigc::mem_fun(this, &NamedBinaryPattern::onFourFoldChanged));
}

// Connect the change handler
void NamedBinaryPattern::connectChangeHandler(const sigc::slot<void>& slot) {
    if(curConnection_.connected()) {
        curConnection_.disconnect();
    }
    curConnection_ = changedSignal_.connect(slot);
}

// The pattern widget has changed
void NamedBinaryPattern::onPatternChanged() {
    hexadecimalCodingEntry_.set_text(binaryPattern_.pattern().hexadecimal());
    changedSignal_.emit();
}

// The four fold pattern check box has changed
void NamedBinaryPattern::onFourFoldChanged() {
    binaryPattern_.fourFold(fourFold_.get_active());
}

// Set the binary pattern
void NamedBinaryPattern::value(const box::BinaryPattern& v) {
    binaryPattern_.pattern(v);
    hexadecimalCodingEntry_.set_text(binaryPattern_.pattern().hexadecimal());
}
