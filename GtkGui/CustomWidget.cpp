/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CustomWidget.h"

// Constructor
CustomWidget::CustomWidget() {
    set_vexpand(true);
    set_hexpand(true);
    set_events(static_cast<Gdk::EventMask>(
                       static_cast<unsigned int>(get_events()) | GDK_BUTTON_PRESS_MASK));
}

// Draw the widget into the given surface
Glib::RefPtr<Gdk::Pixbuf> CustomWidget::capture() {
    auto surface = Cairo::ImageSurface::create(
            Cairo::Format::FORMAT_RGB24,
            get_allocation().get_width() * 2,
            get_allocation().get_height() * 2);
    auto cr = Cairo::Context::create(surface);
    // Draw into the bitmap at double density
    cr->scale(2,2);
    draw(cr, surface->get_width()/2, surface->get_height()/2);
    // Return an image
    auto image = Gdk::Pixbuf::create(
            surface, 0, 0,
            get_allocation().get_width() * 2,
            get_allocation().get_height() * 2);
    // Need to redraw to reset any scaling information etc.
    queue_draw();
    return image;
}

// Null override to un-protect the function
bool CustomWidget::on_button_press_event(GdkEventButton* button_event) {
    bool result = DrawingArea::on_button_press_event(button_event);
    if(button_event->type == GDK_BUTTON_PRESS && button_event->button == 3) {
        // Right button press
        contextMenuSignal_.emit(button_event);
    }
    return result;
}

// Handle drawing requests from GTK
bool CustomWidget::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
    auto allocation = get_allocation();
    draw(cr, allocation.get_width(), allocation.get_height());
    return true;
}
