/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensReflectionDlg.h"
#include "GuiChangeDetector.h"

// Constructor
LensReflectionDlg::LensReflectionDlg() :
        LensAnalyserDlg("V/m") {
}

// Second stage constructor used by the factory
void LensReflectionDlg::construct(AnalysersDlg* owner, sensor::Analyser* analyser) {
    comment("LensReflection");
    analyser_ = dynamic_cast<sensor::LensReflectionAnalyser*>(analyser);
    configLayout_.addElements({extraLine1_});
    LensAnalyserDlg::construct(owner, analyser);
    captureReferenceNow_.signal_clicked().connect(
            sigc::mem_fun(*this, &LensReflectionDlg::onCaptureReferenceNow));
}

// Populate all the widgets with values
void LensReflectionDlg::populate() {
    LensAnalyserDlg::populate();
    InitialisingGui::Set s(initialising_);
    frequency_.value(analyser_->frequency());
    referenceTime_.value(analyser_->referenceTime());
}

// Default change handler
void LensReflectionDlg::onChange() {
    LensAnalyserDlg::onChange();
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newFrequency = c.test(frequency_, analyser_->frequency(),
                                   FdtdLife::notifyMinorChange);
        auto newReferenceTime = c.test(referenceTime_, analyser_->referenceTime(),
                                       FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            analyser_->frequency(newFrequency);
            analyser_->referenceTime(newReferenceTime);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}

// Handle the capture reference now button
void LensReflectionDlg::onCaptureReferenceNow() {
    analyser_->captureReferenceNow();
    // The reference time will have changed
    referenceTime_.value(analyser_->referenceTime());
}
