/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ITEMSANDINFOLAYOUT_H
#define FDTDLIFE_ITEMSANDINFOLAYOUT_H

#include <gtkmm/hvbox.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/scrolledwindow.h>
#include "GuiElement.h"
#include "VertButtonLayout.h"
#include "FramedLayout.h"
#include "ListWidget.h"
#include "GridLayout.h"

// A layout that has an item selection list and maybe some buttons on the left
// and a panel showing an item details dialog on the right.  The right hand
// dialog is created dynamically based on the type of the item.
class ItemsAndInfoLayout : public Gtk::HBox, public GuiElement {
public:
    // The column specification, derive from to add extra columns
    class Columns : public Gtk::TreeModel::ColumnRecord {
    public:
        Columns() {
            add(id_);
            add(name_);
            add(mode_);
        }
        Gtk::TreeModelColumn<int> id_;
        Gtk::TreeModelColumn<std::string> name_;
        Gtk::TreeModelColumn<std::string> mode_;
    };

    // Construction
    ItemsAndInfoLayout(const std::string& name,
                       const std::vector<std::reference_wrapper<GuiElement>>& buttons);
    explicit ItemsAndInfoLayout(const std::string& name);

    // API
    void setColumns(Columns& columns);
    Columns* columns() { return columns_; }
    void setButtons(const std::vector<std::reference_wrapper<GuiElement>>& buttons);
    int currentItem();
    int nextItem();
    int prevItem();
    void setSubDialog(GridLayout* subDialog);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void initialiseGuiElements(bool recursive) override;

    // Signals
    sigc::signal<void, const Gtk::ListStore::Row&>& itemChangedSignal() {
        return itemChangedSignal_;
    };
    sigc::signal<void, const Gtk::ListStore::Row*>& itemSelectedSignal() {
        return itemSelectedSignal_;
    };

protected:
    // Members
    ListWidget treeView_;
    Gtk::ButtonBox buttonBox_{Gtk::ORIENTATION_VERTICAL};
    Gtk::ScrolledWindow scrolledWindow_;
    Columns* columns_{};
    sigc::signal<void, const Gtk::ListStore::Row&> itemChangedSignal_;
    sigc::signal<void, const Gtk::ListStore::Row*> itemSelectedSignal_;
    Gtk::Frame subDialogFrame_;
    Gtk::VBox leftColumn_;
    std::unique_ptr<GridLayout> subDialog_;

    // Handlers
    void onRowChanged(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& pos);
    void onRowSelected();
};


#endif //FDTDLIFE_ITEMSANDINFOLAYOUT_H
