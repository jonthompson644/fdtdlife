/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "GridLayout.h"

// Constructor
GridLayout::GridLayout(const std::vector<std::vector<
        std::reference_wrapper<GuiElement>>>& elements) {
    addLines(elements);
}

// Add widgets to the grid layout
void GridLayout::addLines(const std::vector<std::vector<
        std::reference_wrapper<GuiElement>>>& elements) {
    // Register the elements
    for(auto& line : elements) {
        addLine(line);
    }
}

// Add a line to the grid layout
void GridLayout::addLine(const std::vector<std::reference_wrapper<GuiElement>>& elements) {
    // Record the elements for later placement
    elements_.emplace_back();
    // Register the elements
    for(auto& item : elements) {
        elements_.back().push_back(&item.get());
        registerChild(item.get());
    }
}

// Add the GUI elements to the page
void GridLayout::initialiseGuiElements(bool recursive) {
    // Base class
    GuiElement::initialiseGuiElements(recursive);
    // Me
    int row = 0;
    for(auto& line : elements_) {
        int col = 0;
        for(auto& item : line) {
            Gtk::Widget* leftWidget = item->leftWidget();
            size_t width = 0;
            if(leftWidget != nullptr) {
                width = item->leftColumns();
                attach(*leftWidget, col, row, width, 1);
            }
            col += width;
            Gtk::Widget* rightWidget = item->rightWidget();
            width = 1;
            if(rightWidget != nullptr) {
                width = item->rightColumns();
                attach(*rightWidget, col, row, width, 1);
            }
            col += width;
        }
        row++;
    }
}
