/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDTOOLMENUBUTTON_H
#define FDTDLIFE_NAMEDTOOLMENUBUTTON_H

#include <gtkmm/menutoolbutton.h>
#include <gtkmm/menu.h>
#include <giomm.h>
#include "GuiElement.h"

// A wrapper class for a tool bar menu button that selects values from the type T
template<class T>
class NamedToolMenuButton : public Gtk::MenuToolButton, public GuiElement {
public:
    // Construction
    NamedToolMenuButton() = delete;
    NamedToolMenuButton(const std::string& name, const std::map<std::string, T>& values) {
        // Initialise the button
        set_label(name);
        get_style_context()->add_class("namedbutton");
        menuModel_ = Gio::Menu::create();
        menu_.bind_model(menuModel_, false);
        set_menu(menu_);
        setMenuItems(values);
        // Register
        registerItem(name, *this);
    }

    // Set the menu items
    void setMenuItems(const std::map<std::string, T>& values) {
        // Fill the menu with items
        values_ = values;
        menuModel_->remove_all();
        for(auto& item : values_) {
            menuModel_->append(item.first);
        }
        // Connect the handler to the menu items
        for(auto& c : menu_.get_children()) {
            auto* menuItem = dynamic_cast<Gtk::MenuItem*>(c);
            if(menuItem != nullptr) {
                menuItem->signal_activate().connect(
                        sigc::bind<std::string>(
                                sigc::mem_fun(this, &NamedToolMenuButton::onMenuSelect),
                                menuItem->get_label()));
            }
        }
        // Enable/disable the button
        set_sensitive(!values_.empty());
    }

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // Overrides of MenuToolButton
    void on_clicked() override {
        MenuToolButton::on_clicked();
        mainDoSignal_.emit();
    }

    // Signals
    sigc::signal<void, T>& menuDoSignal() { return menuDoSignal_; };
    sigc::signal<void>& mainDoSignal() { return mainDoSignal_; };

    // Handlers
    void onMenuSelect(const std::string& buttonName) {
        T value = values_[buttonName];
        menuDoSignal_.emit(value);
    }

protected:
    // Members
    std::map<std::string, T> values_;
    Glib::RefPtr<Gio::Menu> menuModel_;
    Gtk::Menu menu_;
    sigc::signal<void, T> menuDoSignal_;
    sigc::signal<void> mainDoSignal_;
};

#endif //FDTDLIFE_NAMEDTOOLMENUBUTTON_H
