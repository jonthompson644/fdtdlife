/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CharacterizerParametersDlg.h"
#include <Seq/Characterizer.h>
#include <Seq/Scanner.h>
#include "VariableScannerDlg.h"

// Second stage constructor used by factory
void CharacterizerParametersDlg::construct(seq::Characterizer* sequencer, CharacterizerDlg* dlg) {
    sequencer_ = sequencer;
    dlg_ = dlg;
    // The sub-dialog factory
    dlgFactory_.define(new box::Factory<SubDlg, seq::Characterizer::ScannerMode>::Creator<VariableScannerDlg>(
            seq::Characterizer::ScannerMode::variable));
    // Initialise widgets
    newScanner_ = std::make_unique<FactoryButton<seq::Scanner, seq::Characterizer::ScannerMode>>(
            "New Scanner", sequencer_->scannerFactory(),
            std::set<seq::Characterizer::ScannerMode>{seq::Characterizer::ScannerMode::variable},
            FactoryButton<seq::Scanner, seq::Characterizer::ScannerMode>::These::only);
    leftHandSide_.addElements({scanners_, *newScanner_, deleteScanner_});
    addLine({leftHandSide_, subDialogFrame_});
    scanners_.setColumns(&scannersColumns_);
    scanners_.appendColumn("Id", scannersColumns_.colIdentifier_);
    scanners_.appendColumn("Type", scannersColumns_.colType_);
    scanners_.appendColumn("Steps", scannersColumns_.colSteps_);
    scanners_.set_hexpand(true);
    scanners_.set_vexpand(true);
    // Connect handlers
    scanners_.selection()->signal_changed().connect(
            sigc::mem_fun(*this, &CharacterizerParametersDlg::onScannerSelected));
    newScanner_->createSignal().connect(
            sigc::mem_fun(*this, &CharacterizerParametersDlg::onNewScanner));
}

// Initialise the widgets
void CharacterizerParametersDlg::populate(seq::Scanner* sel) {
    if(sel == nullptr) {
        sel = currentScanner();
    }
    {
        InitialisingGui::Set s(initialising_);
        scanners_.model()->clear();
        for (auto &scanner: sequencer_->scanners()) {
            auto model = scanners_.model();
            auto rowp = model->append();
            auto &row = *rowp;
            row[scannersColumns_.colIdentifier_] = scanner->id();
            row[scannersColumns_.colType_] = seq::toString(scanner->mode());
            row[scannersColumns_.colSteps_] = scanner->numSteps();
            if (scanner.get() == sel) {
                scanners_.selection()->select(row);
            }
        }
    }
    // Call the selected scanner handler now as it would be automatically called
    // while we had initialising activated.
    onScannerSelected();
}

// The table selection has changed
void CharacterizerParametersDlg::onScannerSelected() {
    if(!initialising_) {
        subDialogFrame_.removeChild();
        subDialog_.reset();
        auto* scanner = currentScanner();
        if(scanner != nullptr) {
            subDialog_.reset(dlgFactory_.make(this, scanner));
            subDialogFrame_.addChild(*subDialog_);
            subDialogFrame_.show_all_children();
            registerChild(*subDialog_);
        }
    }
}

// Return the current scanner
seq::Scanner* CharacterizerParametersDlg::currentScanner() {
    seq::Scanner* result = nullptr;
    auto rowPos = scanners_.curSelection();
    if(rowPos) {
        Gtk::ListStore::Row selection = *rowPos;
        int id = selection[scannersColumns_.colIdentifier_];
        result = sequencer_->getScanner(id);
    }
    return result;
}

// Create a new scanner
void CharacterizerParametersDlg::onNewScanner(seq::Characterizer::ScannerMode mode) {
    auto* scanner = sequencer_->makeScanner(mode).get();
    populate(scanner);
}
