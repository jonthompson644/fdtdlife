/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NamedTwoDSlice.h"
#include "Clipboard.h"

// Constructor with a name
NamedTwoDSlice::NamedTwoDSlice(const std::string& name) {
    nameLabel_ = std::make_unique<Gtk::Label>(name);
    pack_start(*nameLabel_, Gtk::PACK_SHRINK);
    construct(name);
}

// Constructor without name
NamedTwoDSlice::NamedTwoDSlice(bool hasTitle) {
    if(hasTitle) {
        titleLabel_ = std::make_unique<Gtk::Label>("");
        registerItem("twodslice.title", *titleLabel_);
        pack_start(*titleLabel_, Gtk::PACK_SHRINK);
    }
    construct("twodslice");
}

// Common constructor code
void NamedTwoDSlice::construct(const std::string& name) {
    packOptions_ = Gtk::PACK_EXPAND_WIDGET;
    pack_start(twoDSlice_, Gtk::PACK_EXPAND_WIDGET);
    // Register
    registerItem(name, twoDSlice_);
    registerItem(name + ".contextmenu", contextMenu_);
    // The context menu
    contextMenu_.item("_Copy image to clipboard", true,
                      sigc::mem_fun(*this, &NamedTwoDSlice::onCopyImageToClipboard));
    twoDSlice_.contextMenuSignal().connect(
            sigc::mem_fun(*this, &NamedTwoDSlice::onContextMenu));
}

// Set the name of the item
void NamedTwoDSlice::title(const std::string& v) {
    if(titleLabel_) {
        titleLabel_->set_label(v);
    }
}

// Handle the context menu
void NamedTwoDSlice::onContextMenu(GdkEventButton* event) {
    contextMenu_.run(*this, event);
}

// Handle copying the image to the clipboard
void NamedTwoDSlice::onCopyImageToClipboard() {
    if(clipboard_) {
        auto image = twoDSlice_.capture();
        clipboard_->put(image);
    }
}

