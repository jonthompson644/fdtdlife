/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CheckboxList.h"

// Constructor
CheckboxList::CheckboxList(const std::string& name) {
    get_selection()->set_mode(Gtk::SELECTION_SINGLE);
    // Register the columns with the model
    setColumns(&columns_);
    // Display the columns
    append_column_editable("", columns_.colCheck_);
    append_column(name, columns_.colName_);
    show_all_children();
    // Connect handlers
    get_selection()->signal_changed().connect(
            sigc::mem_fun(*this, &CheckboxList::onRowSelect));
    model()->signal_row_changed().connect(
            sigc::mem_fun(*this, &CheckboxList::onRowChanged));
}

// Clear all the checkboxes from the list
void CheckboxList::clear() {
    model()->clear();
}

// Add a checkbox to the list
void CheckboxList::addCheckbox(const std::string& name, bool initialState, int identifier) {
    auto row = *(model()->append());
    row[columns_.colName_] = name;
    row[columns_.colCheck_] = initialState;
    row[columns_.colIdentifier_] = identifier;
}

// Handler for the row selection change signal
void CheckboxList::onRowSelect() {
    if(get_selection()->get_selected()) {
        Gtk::ListStore::Row row = *get_selection()->get_selected();
        selectedSignal_.emit(row[columns_.colCheck_], row[columns_.colIdentifier_],
                             row[columns_.colName_]);
    }
}

// Handler for the row selection change signal
void CheckboxList::onRowChanged(const Gtk::TreeModel::Path& /*path*/,
                                const Gtk::TreeModel::iterator& pos) {
    Gtk::ListStore::Row row = *pos;
    changedSignal_.emit(row[columns_.colCheck_], row[columns_.colIdentifier_],
                        row[columns_.colName_]);
}
