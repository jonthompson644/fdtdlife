/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_MATERIALSDLG_H
#define FDTDLIFE_MATERIALSDLG_H

#include "InitialisingGui.h"
#include "ItemsAndInfoLayout.h"
#include "FdtdLife.h"
#include "NamedButton.h"
#include "FactoryButton.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include <Domain/Material.h>

// The dialog panel showing materials on the top level page's notebook
class MaterialsDlg : public ItemsAndInfoLayout {
public:
    // Types
    typedef SubDialog<MaterialsDlg, domain::Material> SubDlg;

    class MatColumns : public ItemsAndInfoLayout::Columns {
    public:
        MatColumns() {
            add(permittivity_);
        }
        Gtk::TreeModelColumn<double> permittivity_;
    };

    // Construction
    MaterialsDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // API
    void populate();

    // Accessors
    FdtdLife& model() { return model_; }

protected:
    // Members
    FdtdLife& model_;
    InitialisingGui initialising_;
    MatColumns columns_;
    FactoryButton<domain::Material> newButton_{
            "New Material", model_.d()->materialFactory(),
            {domain::Domain::materialModeNormal},
            FactoryButton<domain::Material>::These::only};
    NamedButton deleteButton_{"Delete Material"};
    SubDialogFactory<SubDlg> dlgFactory_;
    Gtk::Window* parentWindow_;

    // Helpers
    void fillMaterialList(domain::Material* select = nullptr);

    // Handlers
    void onNewMaterial(int mode);
    void onDeleteMaterial();
    void onItemChanged(const Gtk::ListStore::Row& row);
    void onItemSelected(const Gtk::ListStore::Row* row);
};

#endif //FDTDLIFE_MATERIALSDLG_H
