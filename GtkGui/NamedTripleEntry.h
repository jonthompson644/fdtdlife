/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDTRIPLEENTRY_H
#define FDTDLIFE_NAMEDTRIPLEENTRY_H

#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <Box/Vector.h>
#include <Box/VectorExpression.h>
#include "GuiElement.h"

// A class that binds a label with three entry widgets
template<class T>
class NamedTripleEntry : public Gtk::Grid, public GuiElement {
public:
    // Construction
    NamedTripleEntry() = delete;
    explicit NamedTripleEntry(const std::string& name, bool readOnly=false,
                              size_t numColsRight = 3);

    // Accessors
    void value(const box::Vector<T>& v);
    box::Vector<T> value();

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void connectChangeHandler(const sigc::slot<void>& slot) override;
    Gtk::Widget* leftWidget() override { return &nameLabel_; }
    size_t rightColumns() const override { return numColsRight_; }

protected:
    // Members
    Gtk::Label nameLabel_;  // The name attached to the value
    Gtk::Entry valueXEntry_;  // The current X value
    Gtk::Entry valueYEntry_;  // The current Y value
    Gtk::Entry valueZEntry_;  // The current Z value
    sigc::connection curConnection_;  // The current changed connection
    bool readOnly_;  // The element is read only
    sigc::signal<void> changedSignal_;
    size_t numColsRight_;  // Number of columns occupied by the entry widgets

    // Handlers
    void onStateFlagsX(Gtk::StateFlags previous);
    void onStateFlagsY(Gtk::StateFlags previous);
    void onStateFlagsZ(Gtk::StateFlags previous);
    void onActivate();
};

template<>
class NamedTripleEntry<box::VectorExpression<double>> :
        public NamedTripleEntry<std::string> {
public:
    NamedTripleEntry() = delete;
    explicit NamedTripleEntry(const std::string& name);
    using NamedTripleEntry<std::string>::value;
    void value(const box::VectorExpression<double>& value);
    box::VectorExpression<double> value();
};

template<>
class NamedTripleEntry<box::VectorExpression<size_t>> :
        public NamedTripleEntry<std::string> {
public:
    NamedTripleEntry() = delete;
    explicit NamedTripleEntry(const std::string& name);
    using NamedTripleEntry<std::string>::value;
    void value(const box::VectorExpression<size_t>& value);
    box::VectorExpression<size_t> value();
};

#endif //FDTDLIFE_NAMEDTRIPLEENTRY_H
