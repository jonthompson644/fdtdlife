/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_VARIABLESDLG_H
#define FDTDLIFE_VARIABLESDLG_H

#include "FdtdLife.h"
#include "InitialisingGui.h"
#include "NamedListView.h"
#include "NamedButton.h"
#include <gtkmm/grid.h>
#include "GuiElement.h"
#include "HorizontalLayout.h"
#include <Fdtd/Notifiable.h>

// The variables top level page
class VariablesDlg : public Gtk::Grid, public GuiElement, public fdtd::Notifiable {
public:
    // Construction
    VariablesDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }
    void initialiseGuiElements(bool recursive) override;

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // API
    void populate();

protected:
    // Handlers
    void onNewVariable();
    void onDeleteVariable();
    void onVariableChanged(const Gtk::TreeModel::Path& path,
            const Gtk::TreeModel::iterator& pos);
    void onItemMoved(const Gtk::TreeModel::iterator& pos, size_t location);

    // Helpers
    void fillVariableList(fdtd::Variable* select = nullptr);
    void updateValues();
    fdtd::Variable* currentVariable();
    fdtd::Variable* prevVariable();
    fdtd::Variable* nextVariable();

protected:
    // The model
    FdtdLife& model_;
    Gtk::Window* parentWindow_;
    InitialisingGui initialising_;

    // The variable list columns
    class Columns : public Gtk::TreeModel::ColumnRecord {
    public:
        Columns() {
            add(colIdentifier_);
            add(colName_);
            add(colExpression_);
            add(colValue_);
            add(colComment_);
        }
        // Columns
        Gtk::TreeModelColumn<std::string> colName_;
        Gtk::TreeModelColumn<std::string> colExpression_;
        Gtk::TreeModelColumn<std::string> colValue_;
        Gtk::TreeModelColumn<std::string> colComment_;
        Gtk::TreeModelColumn<int> colIdentifier_;
    } columns_;

    // Configuration widgets
    NamedListView variables_;   // The list of variables
    NamedButton newVariable_{"New Variable"};
    NamedButton deleteVariable_{"Delete Variable"};
    HorizontalLayout buttons_{{newVariable_, deleteVariable_}};
};


#endif //FDTDLIFE_VARIABLESDLG_H
