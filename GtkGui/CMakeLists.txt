cmake_minimum_required(VERSION 3.8)
project(GtkGuiLibrary)

# GTK
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})
message("Dirs=${GTKMM_INCLUDE_DIRS}")

set(SOURCE_FILES ApplicationDlg.cpp ApplicationDlg.h
        ConfigurationDlg.cpp ConfigurationDlg.h
        FdtdLife.cpp FdtdLife.h
        NamedCheckbox.cpp NamedCheckbox.h
        FramedLayout.cpp FramedLayout.h
        NotebookLayout.cpp NotebookLayout.h
        ToolbarLayout.cpp ToolbarLayout.h
        FdtdApp.cpp FdtdApp.h
        GuiElement.cpp GuiElement.h
        NamedButton.cpp NamedButton.h
        GuiChangeDetector.cpp GuiChangeDetector.h
        SourcesDlg.cpp SourcesDlg.h
        ItemsAndInfoLayout.cpp ItemsAndInfoLayout.h
        VertButtonLayout.cpp VertButtonLayout.h
        FactoryButton.h
        InitialisingGui.cpp InitialisingGui.h
        PlaneWaveDlg.cpp PlaneWaveDlg.h
        SubDialog.h SubDialogFactory.h
        ListWidget.cpp ListWidget.h
        OkCancelDialog.cpp OkCancelDialog.h
        TestSuite.cpp TestSuite.h
        ModalDialog.h
        ZoneSourceDlg.cpp ZoneSourceDlg.h
        GravyWaveDlg.cpp GravyWaveDlg.h
        MaterialsDlg.cpp MaterialsDlg.h
        CloseDialog.cpp CloseDialog.h
        NormalMaterialDlg.cpp NormalMaterialDlg.h
        CpmlMaterialDlg.cpp CpmlMaterialDlg.h
        ShapesDlg.cpp ShapesDlg.h
        DielectricLayerDlg.cpp DielectricLayerDlg.h
        MaterialSelection.cpp MaterialSelection.h
        SquarePlateDlg.cpp SquarePlateDlg.h
        SquareHoleDlg.cpp SquareHoleDlg.h
        CuboidArrayDlg.cpp CuboidArrayDlg.h
        FingerPlateDlg.cpp FingerPlateDlg.h
        BinaryPlateNxNDlg.cpp BinaryPlateNxNDlg.h
        NamedBinaryPattern.cpp NamedBinaryPattern.h
        BinaryPatternWidget.cpp BinaryPatternWidget.h
        SensorsDlg.cpp SensorsDlg.h
        ArraySensorDlg.cpp ArraySensorDlg.h
        NamedMenuButton.h
        TwoDSliceDlg.cpp TwoDSliceDlg.h
        Clipboard.cpp Clipboard.h
        MaterialSensorDlg.cpp MaterialSensorDlg.h
        AnalysersDlg.cpp AnalysersDlg.h
        LensPhaseDlg.cpp LensPhaseDlg.h
        SensorSelection.cpp SensorSelection.h
        DataSeriesWidget.cpp DataSeriesWidget.h
        CustomWidget.cpp CustomWidget.h
        ContextMenu.cpp ContextMenu.h
        NamedDataSeries.cpp NamedDataSeries.h
        LensAnalyserDlg.cpp LensAnalyserDlg.h
        LensReflectionDlg.cpp LensReflectionDlg.h
        LensTransmissionDlg.cpp LensTransmissionDlg.h
        ViewsDlg.cpp ViewsDlg.h
        ViewFrame.cpp ViewFrame.h
        ViewDlg.cpp ViewDlg.h
        NamedCheckboxList.cpp NamedCheckboxList.h
        CheckboxList.cpp CheckboxList.h
        ViewItem.cpp ViewItem.h
        TwoDSliceViewItem.cpp TwoDSliceViewItem.h
        DataSeriesViewItem.cpp DataSeriesViewItem.h
        DataSeriesViewItemDlg.cpp DataSeriesViewItemDlg.h
        TwoDSliceViewItemDlg.cpp TwoDSliceViewItemDlg.h
        AttenuationViewItem.cpp AttenuationViewItem.h
        AttenuationViewItemDlg.cpp AttenuationViewItemDlg.h
        PhaseShiftViewItem.cpp PhaseShiftViewItem.h
        PhaseShiftViewItemDlg.cpp PhaseShiftViewItemDlg.h
        MaterialViewItem.cpp MaterialViewItem.h
        TwoDSliceWidget.cpp TwoDSliceWidget.h
        Rgb.h
        NamedTwoDSlice.cpp NamedTwoDSlice.h
        SequencersDlg.cpp SequencersDlg.h
        NamedListView.cpp NamedListView.h
        VariablesDlg.cpp VariablesDlg.h
        NamedToolButton.cpp NamedToolButton.h
        YesNoCancelDialog.cpp YesNoCancelDialog.h
        FileSaveDialog.cpp FileSaveDialog.h
        FileOpenDialog.cpp FileOpenDialog.h
        FileDialog.cpp FileDialog.h
        Timer.cpp Timer.h
        SingleItemAndInfoLayout.cpp SingleItemAndInfoLayout.h
        DesignersDlg.cpp DesignersDlg.h
        LensDesignerDlg.cpp LensDesignerDlg.h
        LensDesignerPhaseDlg.cpp LensDesignerPhaseDlg.h
        GridLayout.cpp GridLayout.h
        NamedEntry.cpp NamedEntry.h
        NamedSelection.h
        NamedRadioButton.cpp NamedRadioButton.h
        HorizontalLayout.cpp HorizontalLayout.h
        VerticalLayout.cpp VerticalLayout.h
        LensDesignerColumnsDlg.cpp LensDesignerColumnsDlg.h
        NamedToggleButton.cpp NamedToggleButton.h
        MaterialsSelectDlg.cpp MaterialsSelectDlg.h
        NamedTripleEntry.cpp NamedTripleEntry.h
        LensDesignerRayTracerDlg.cpp LensDesignerRayTracerDlg.h
        RayTracerWidget.cpp RayTracerWidget.h
        NamedTripleSelection.h
        HorizontalSeparator.h
        NamedToolMenuButton.h ParametersDlg.cpp ParametersDlg.h ParameterExtractorDlg.cpp ParameterExtractorDlg.h SingleJobDlg.cpp SingleJobDlg.h CharacterizerDlg.cpp CharacterizerDlg.h CharacterizerParametersDlg.cpp CharacterizerParametersDlg.h VariableScannerDlg.cpp VariableScannerDlg.h VariableSelection.cpp VariableSelection.h CharacterizerStepsDlg.cpp CharacterizerStepsDlg.h FingerPlateAdmittancesDlg.cpp FingerPlateAdmittancesDlg.h)

add_library(GtkGuiLibrary STATIC ${SOURCE_FILES})
