/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CHECKBOXLIST_H
#define FDTDLIFE_CHECKBOXLIST_H

#include "ListWidget.h"

// Provides a list of checkboxes
class CheckboxList : public ListWidget {
public:
    // Construction
    explicit CheckboxList(const std::string& name);
    CheckboxList() = delete;

    // API
    void clear();
    void addCheckbox(const std::string& name, bool initialState, int identifier);

    // Signals
    sigc::signal<void, bool, int, const std::string&>& changedSignal() { return changedSignal_; }
    sigc::signal<void, bool, int, const std::string&>& selectedSignal() { return selectedSignal_; }

protected:
    // Signals
    sigc::signal<void, bool, int, const std::string&> changedSignal_;
    sigc::signal<void, bool, int, const std::string&> selectedSignal_;

    class Columns : public Gtk::TreeModel::ColumnRecord {
    public:
        Columns() {
            add(colCheck_);
            add(colName_);
            add(colIdentifier_);
        }
        // Columns
        Gtk::TreeModelColumn<std::string> colName_;
        Gtk::TreeModelColumn<bool> colCheck_;
        Gtk::TreeModelColumn<int> colIdentifier_;
    } columns_;

    // Signal handlers
    void onRowSelect();
    void onRowChanged(const Gtk::TreeModel::Path& path, const Gtk::TreeModel::iterator& pos);
};


#endif //FDTDLIFE_CHECKBOXLIST_H
