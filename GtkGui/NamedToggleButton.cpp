/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "NamedToggleButton.h"

#include <utility>


// Constructor
NamedToggleButton::NamedToggleButton(const std::string& name, size_t numColsRight) :
        NamedToggleButton(name, {"yes", "no"}, numColsRight) {
}

// Constructor
NamedToggleButton::NamedToggleButton(const std::string& name,
                                     std::pair<std::string, std::string> onOff,
                                     size_t numColsRight) :
        numColsRight_(numColsRight),
        onOff_(std::move(onOff)) {
    // Initialise the components
    nameLabel_.set_label(name);
    nameLabel_.get_style_context()->add_class("namedentryaligned");
    nameLabel_.set_halign(Gtk::ALIGN_END);
    set_label(onOff_.second);
    get_style_context()->add_class("namedbutton");
    // Register
    registerItem(name, *this);
    // Connect detector handlers
    signal_toggled().connect(
            sigc::mem_fun(this, &NamedToggleButton::onToggle));
}

// Set the current state of the toggle button
void NamedToggleButton::value(bool v) {
    set_active(v);
}

// Get the current state of the toggle button
bool NamedToggleButton::value() {
    return get_active();
}

// Connect the supplied change handler
void NamedToggleButton::connectChangeHandler(const sigc::slot<void>& slot) {
    if(curConnection_.connected()) {
        curConnection_.disconnect();
    }
    curConnection_ = signal_toggled().connect(slot);
}

// Handle changes of the button state
void NamedToggleButton::onToggle() {
    if(get_active()) {
        set_label(onOff_.first);
    } else {
        set_label(onOff_.second);
    }
}


