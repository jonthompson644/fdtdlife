/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "YesNoCancelDialog.h"

// Constructor
YesNoCancelDialog::YesNoCancelDialog(Gtk::Window* parentWindow, const std::string& name,
                               const std::string& description, TestSuite* testSuite) :
        MessageDialog(*parentWindow, "", false,
                      Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_NONE),
        testSuite_(testSuite),
        name_(name) {
    add_button("Cancel", Gtk::RESPONSE_CANCEL);
    add_button("No", Gtk::RESPONSE_NO);
    add_button("Yes", Gtk::RESPONSE_YES);
    set_title(name);
    set_secondary_text(description);
    get_action_area()->set_layout(Gtk::BUTTONBOX_CENTER);
}

// Ask the question by running the modal dialog
YesNoCancelDialog::Answer YesNoCancelDialog::ask() {
    // Ask the question
    int result;
    if(testSuite_ != nullptr) {
        // Running under the test suite, ask it instead
        result = testSuite_->runModalDialog(name_, this);
    } else {
        // Running normally, do the modal dialog
        result = run();
    }
    // Decode the result
    Answer answer;
    switch(result) {
        case Gtk::RESPONSE_YES:
            answer = Answer::yes;
            break;
        case Gtk::RESPONSE_NO:
            answer = Answer::no;
            break;
        default:
            answer = Answer::cancel;
            break;
    }
    return answer;
}
