/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "CpmlMaterialDlg.h"
#include "GuiChangeDetector.h"
#include <Domain/CpmlMaterial.h>

// Second stage constructor used by the factor
void CpmlMaterialDlg::construct(MaterialsDlg* owner, domain::Material* material) {
    material_ = dynamic_cast<domain::CpmlMaterial*>(material);
    epsilon_.readOnly(true);
    relativeEpsilon_.readOnly(true);
    mu_.readOnly(true);
    relativeMu_.readOnly(true);
    sigma_.readOnly(true);
    sigmaStar_.readOnly(true);
    NormalMaterialDlg::construct(owner, material);
    comment("CpmlMaterial");
}

// Initialise the layout of the dialog
void CpmlMaterialDlg::initLayout() {
    NormalMaterialDlg::initLayout();
    addLines({{sigmaCoeff_, kappaMax_},
              {aCoeffMax_},
              {sigmaByKappaM_, aCoeffM_}});
}

// Populate all the widgets with values
void CpmlMaterialDlg::populate() {
    NormalMaterialDlg::populate();
    InitialisingGui::Set s(initialising_);
    sigmaCoeff_.value(material_->sigmaCoeff());
    kappaMax_.value(material_->kappaMax());
    aCoeffMax_.value(material_->aMax());
    sigmaByKappaM_.value(material_->msk());
    aCoeffM_.value(material_->ma());
}

// Default change handler
void CpmlMaterialDlg::onChange() {
    NormalMaterialDlg::onChange();
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newSigmaCoeff = c.test(sigmaCoeff_, material_->sigmaCoeff(),
                                    FdtdLife::notifyMinorChange);
        auto newKappaMax = c.test(kappaMax_, material_->kappaMax(),
                                    FdtdLife::notifyMinorChange);
        auto newACoeffMax = c.test(aCoeffMax_, material_->aMax(),
                                    FdtdLife::notifyMinorChange);
        auto newSigmaByKappaM = c.test(sigmaByKappaM_, material_->msk(),
                                    FdtdLife::notifyMinorChange);
        auto newACoeffM = c.test(aCoeffM_, material_->ma(),
                                    FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            material_->sigmaCoeff(newSigmaCoeff);
            material_->kappaMax(newKappaMax);
            material_->aMax(newACoeffMax);
            material_->msk(newSigmaByKappaM);
            material_->ma(newACoeffM);
            // Tell others
            dlg_->model().doNotification(c.why());
        }
    }
}
