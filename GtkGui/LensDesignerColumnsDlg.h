/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H
#define FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H

#include <Fdtd/Notifiable.h>
#include "VerticalLayout.h"
#include "NamedListView.h"
#include "NamedCheckbox.h"
#include "NamedEntry.h"
#include "NamedToggleButton.h"
#include "NamedSelection.h"
#include "HorizontalLayout.h"
#include "NamedButton.h"
#include "NamedMenuButton.h"
#include "InitialisingGui.h"

class FdtdLife;

class LensDesignerDlg;

class LensDesignerColumnsDlg : public VerticalLayout, public fdtd::Notifiable {
public:
    // Construction
    LensDesignerColumnsDlg() = default;
    void construct(FdtdLife* model, LensDesignerDlg* dlg);

    // API
    void populate();

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

protected:
    // Tables list view columns
    class ColumnsColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        explicit ColumnsColumns(size_t numColumns) {
            colColumns_.resize(numColumns);
            add(colLayer_);
            for(auto& col : colColumns_) {
                add(col);
            }
        }
        // Columns
        Gtk::TreeModelColumn<size_t> colLayer_;
        std::vector<Gtk::TreeModelColumn<double>> colColumns_;
    };

    std::unique_ptr<ColumnsColumns> columnsColumns_;

    // Command enumerations
    enum class GaCommand {
        column, adjustFull
    };
    enum class ExportCommand {
        diameterSliceHfss, quadrantHfss, quadrantHfssLayer
    };
    enum class FillModel {
        squarePlates, dielectricBlocks, fingerPatterns
    };

    // Handlers
    void onChange();
    void onCopyCsv();
    void onPasteCsv();
    void onGenerateGa(GaCommand v);
    void onExport(ExportCommand v);
    void onMakeModel(FillModel v);
    void onColumnsChanged(const Gtk::TreeModel::Path& path,
                          const Gtk::TreeModel::iterator& pos);
    void onWhich();

    // Helpers
    void populateColumns();
    void initialiseColumns();
    void populateControls();

    // Members
    FdtdLife* model_{};
    LensDesignerDlg* dlg_{};
    InitialisingGui initialising_;
    sigc::connection colChangedConn_;

    // Widgets
    NamedSelection<designer::LensDesigner::WhichParameter> which_{
            "Which Parameter",
            {"Refractive Indices", "Unit Cells",
             "Incidence Angles", "Patch Ratio", "Error"}};
    HorizontalLayout line0_{{which_}};
    NamedListView columns_{""};
    NamedToggleButton exportSymmetrical_{"Export Symmetrical"};
    NamedEntry<size_t> columnNum_{"Column Num"};
    NamedEntry<double> gaRange_{"GA Range"};
    NamedEntry<size_t> gaBits_{"GA Bits"};
    NamedEntry<size_t> exportLayer_{"Export Layer"};
    HorizontalLayout line1_{{exportSymmetrical_, columnNum_, gaRange_, gaBits_}};
    NamedButton copyCsv_{"Copy CSV"};
    NamedButton pasteCsv_{"Paste CSV"};
    NamedMenuButton<GaCommand> generateGa_{
            "Generate GA",
            {{"Single Column", GaCommand::column},
             {"Adjust Full", GaCommand::adjustFull}}};
    NamedMenuButton<ExportCommand> export_{
            "Export",
            {{"Diameter Slice HFSS", ExportCommand::diameterSliceHfss},
             {"Quadrant HFSS", ExportCommand::quadrantHfss},
             {"Quadrant HFSS Layer", ExportCommand::quadrantHfssLayer}}};
    NamedMenuButton<FillModel> makeModel_{
            "Make Model",
            {{"Square Plates", FillModel::squarePlates},
             {"Dielectric Blocks", FillModel::dielectricBlocks},
             {"Finger Patterns", FillModel::fingerPatterns}}};
    HorizontalLayout line2_{{copyCsv_, pasteCsv_,
                                    generateGa_, export_, makeModel_, exportLayer_}};
};


#endif //FDTDLIFE_LENSDESIGNERCOLUMNSDLG_H
