/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "RayTracerWidget.h"
#include "Clipboard.h"

// Constructor
RayTracerWidget::RayTracerWidget(const std::string& name) {
    registerItem(name, *this);
    registerItem(name + ".contextmenu", contextMenu_);
    set_hexpand();
    set_vexpand();
    // The context menu
    contextMenu_.item("_Copy image to clipboard", true,
                      sigc::mem_fun(*this, &RayTracerWidget::onCopyImageToClipboard));
    contextMenuSignal().connect(
            sigc::mem_fun(*this, &RayTracerWidget::onContextMenu));
}

// Handle the context menu
void RayTracerWidget::onContextMenu(GdkEventButton* event) {
    contextMenu_.run(*this, event);
}

// Handle copying the image to the clipboard
void RayTracerWidget::onCopyImageToClipboard() {
    if(clipboard_) {
        auto image = capture();
        clipboard_->put(image);
    }
}

// Set the ray tracer object that is to be drawn
void RayTracerWidget::setRayTracer(designer::LensRayTracer* rayTracer) {
    rayTracer_ = rayTracer;
    queue_draw();
}

// Button presses come here
bool RayTracerWidget::on_button_press_event(GdkEventButton* button_event) {
    // TODO: Any actions on button presses, note that the base class handles the context menu.
    return CustomWidget::on_button_press_event(button_event);
}

// Draw the ray tracer information
void RayTracerWidget::draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) {
    drawBackground(cr, width, height);
    if(rayTracer_ != nullptr) {
        scaling(width, height);
        drawLens(cr);
        drawRays(cr);
    }
}

// Fill the background of the panel
void RayTracerWidget::drawBackground(const Cairo::RefPtr<Cairo::Context>& cr,
                                     int width, int height) {
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->rectangle(0.0, 0.0, width, height);
    cr->fill();
}

// Calculate the scaling
void RayTracerWidget::scaling(int width, int height) {
    double rayTracerHeight = rayTracer_->z2() - rayTracer_->z1();
    double rayTracerWidth = rayTracer_->x2() - rayTracer_->x1() + rayTracer_->unitCell();
    double xScale = (rayTracerWidth) / width;
    double zScale = (rayTracerHeight) / height;
    scaling_ = std::max(xScale, zScale);
    xOffset_ = -(rayTracer_->x1() - rayTracer_->unitCell()/2) / scaling_;
    zOffset_ = -rayTracer_->z1() / scaling_;
}

// Draw a representation of the lens structure
void RayTracerWidget::drawLens(const Cairo::RefPtr<Cairo::Context>& cr) {
    double z1 = rayTracer_->zLens1() / scaling_ + zOffset_;
    double z2 = rayTracer_->zLens2() / scaling_ + zOffset_;
    double dx = rayTracer_->unitCell() / scaling_;
    double xp = (rayTracer_->x1() + rayTracer_->x2()) / 2.0 / scaling_ + xOffset_;
    double xn = xp;
    size_t numCols = (rayTracer_->columnsInDiameter() + 1)/ 2;
    if((rayTracer_->columnsInDiameter() % 2) == 1) {
        // Odd number of columns
        // Draw the central column
        cr->set_source_rgb(0.0, 0.5, 0.5);
        cr->rectangle(xp - dx / 2.0, z1, dx, z2 - z1);
        cr->fill_preserve();
        cr->set_source_rgb(0.8, 0.8, 0.8);
        cr->stroke();
        // Initialise for the loop
        xp += dx / 2.0;
        xn -= 3.0 * dx / 2.0;
        numCols--;
    } else {
        // Even number of columns
        xn -= dx;
    }
    for(size_t i=0; i<numCols; i++) {
        // Draw the left column
        cr->set_source_rgb(0.0, 0.5, 0.5);
        cr->rectangle(xn, z1, dx, z2 - z1);
        cr->fill_preserve();
        cr->set_source_rgb(0.8, 0.8, 0.8);
        cr->stroke();
        // Draw the right column
        cr->set_source_rgb(0.0, 0.5, 0.5);
        cr->rectangle(xp, z1, dx, z2 - z1);
        cr->fill_preserve();
        cr->set_source_rgb(0.8, 0.8, 0.8);
        cr->stroke();
        // Next in loop
        xp += dx;
        xn -= dx;
    }
}

// Draw the rays
void RayTracerWidget::drawRays(const Cairo::RefPtr<Cairo::Context>& cr) {
    for(auto& ray : rayTracer_->rays()) {
        bool first = true;
        designer::LensRayTracer::Coord lastPos;
        for(auto& pos : ray.path_) {
            if(!first) {
                cr->set_source_rgb(0.0, 0.0, 0.0);
                cr->move_to(lastPos.x_ / scaling_ + xOffset_,
                        lastPos.z_ / scaling_ + zOffset_);
                cr->line_to(pos.x_ / scaling_ + xOffset_,
                        pos.z_ / scaling_ + zOffset_);
                cr->stroke();
            }
            lastPos = pos;
            first = false;
        }
    }
}

