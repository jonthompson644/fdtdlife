/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "GuiElement.h"
#include <iostream>

// Destructor
GuiElement::~GuiElement() {
    // Deregister from the parent
    if(parent_ != nullptr) {
        parent_->deregisterChild(*this);
    }
}

// Called by the derived element to register its named widgets
void GuiElement::registerItem(const std::string& name, Gtk::Widget& widget) {
    if(items_.count(name) > 0) {
        std::cerr << "Duplicate item name: " << name << std::endl;
    }
    items_.emplace(name, std::ref(widget));
}

// Called by the derived class to register its children
void GuiElement::registerChild(GuiElement& child) {
    children_.emplace_back(&child);
    child.parent_ = this;
}

// Called to deregister a child
void GuiElement::deregisterChild(GuiElement& child) {
    children_.remove(&child);
    child.parent_ = nullptr;
}

// Connects the default change handler to all the children
void GuiElement::connectDefaultChangeHandler(const sigc::slot<void>& slot) {
    for(auto& child : children_) {
        child->connectChangeHandler(slot);
        child->connectDefaultChangeHandler(slot);
    }
}

// Call all the children to initialise.  Must be called by any overrides
void GuiElement::initialiseGuiElements(bool recursive) {
    if(recursive) {
        for(auto& child : children_) {
            child->initialiseGuiElements(true);
        }
    }
}

// Find a widget with the given name
Gtk::Widget* GuiElement::findWidget(const std::string& name) {
    Gtk::Widget* result = nullptr;
    // Is it one of mine?
    auto pos = items_.find(name);
    if(pos != items_.end()) {
        result = &pos->second.get();
    } else {
        // Try the children
        for(auto& child : children_) {
            result = child->findWidget(name);
            if(result != nullptr) {
                break;
            }
        }
    }
    return result;
}

// Dump the widget to stdout and all its children
void GuiElement::dump(int level) {
    // The indent
    std::string indent;
    for(int i = 0; i < level; i++) {
        indent += "    ";
    }
    // The element header
    std::cout << indent << "Element "
              << "{" << comment_ << "} "
              << static_cast<void*>(this) << " [" << static_cast<void*>(parent_) << "]: ";
    for(auto& item : items_) {
        std::cout << "\"" << item.first << "\" ";
    }
    std::cout << std::endl;
    // The children
    for(auto& child : children_) {
        child->dump(level + 1);
    }
}
