/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNERDLG_H
#define FDTDLIFE_LENSDESIGNERDLG_H

#include "DesignersDlg.h"
#include "NotebookLayout.h"
#include "LensDesignerPhaseDlg.h"
#include "LensDesignerColumnsDlg.h"
#include "LensDesignerRayTracerDlg.h"

class LensDesignerDlg : public DesignersDlg::SubDlg {
public:
    // Constructor
    LensDesignerDlg() = default;
    void construct(DesignersDlg* owner, designer::Designer* designer) override;

    // Overrides of SubDlg
    void populate() override;

    // API
    NotebookLayout& sections() { return notebook_; }

protected:
    // Members
    designer::Designer* designer_{nullptr};

    // The widgets
    LensDesignerPhaseDlg phasePage_;
    LensDesignerColumnsDlg columnsPage_;
    LensDesignerRayTracerDlg rayTracerPage_;
    NotebookLayout notebook_{{
                                     {"Phase", phasePage_},
                                     {"Columns", columnsPage_},
                                     {"Ray Tracer", rayTracerPage_}}};
};


#endif //FDTDLIFE_LENSDESIGNERDLG_H
