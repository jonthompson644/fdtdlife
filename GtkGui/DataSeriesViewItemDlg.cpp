/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "DataSeriesViewItemDlg.h"
#include "GuiChangeDetector.h"

// Second stage constructor used by the factor
void DataSeriesViewItemDlg::construct(ViewDlg* owner, ViewItem* item) {
    SubDialog::construct(owner, item);
    comment("DataSeriesViewItemDlg");
    item_ = dynamic_cast<DataSeriesViewItem*>(item);
    addLinesToDialog();
    initialiseGuiElements(true);
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(*this, &DataSeriesViewItemDlg::onChange));
}

// Add the lines to the dialog.  Overridden by the derived classes to taylor their displays.
void DataSeriesViewItemDlg::addLinesToDialog() {
    addLines({{xMax_, xMin_, xAutomatic_},
              {yMax_, yMin_, yAutomatic_},
              {title_}});
}

// Populate all the widgets with values
void DataSeriesViewItemDlg::populate() {
    ViewDlg::SubDlg::populate();
    InitialisingGui::Set s(initialising_);
    xMax_.value(item_->xMax());
    xMin_.value(item_->xMin());
    xAutomatic_.value(item_->xAutomatic());
    yMax_.value(item_->yMax());
    yMin_.value(item_->yMin());
    yAutomatic_.value(item_->yAutomatic());
    title_.value(item_->title());
}

// Default change handler
void DataSeriesViewItemDlg::onChange() {
    if(!initialising_) {
        // Test for changes
        GuiChangeDetector c;
        auto newXMin = c.test(xMin_, item_->xMin(),
                              FdtdLife::notifyMinorChange);
        auto newXMax = c.test(xMax_, item_->xMax(),
                              FdtdLife::notifyMinorChange);
        auto newXAutomatic = c.test(xAutomatic_, item_->xAutomatic(),
                              FdtdLife::notifyMinorChange);
        auto newYMin = c.test(yMin_, item_->yMin(),
                              FdtdLife::notifyMinorChange);
        auto newYMax = c.test(yMax_, item_->yMax(),
                              FdtdLife::notifyMinorChange);
        auto newYAutomatic = c.test(yAutomatic_, item_->yAutomatic(),
                                    FdtdLife::notifyMinorChange);
        auto newTitle = c.test(title_, item_->title(),
                               FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            item_->xMin(newXMin);
            item_->xMax(newXMax);
            item_->xAutomatic(newXAutomatic);
            item_->yMin(newYMin);
            item_->yMax(newYMax);
            item_->yAutomatic(newYAutomatic);
            item_->title(newTitle);
            // Tell others
            dlg_->dlg()->model().doNotification(c.why());
        }
    }
}
