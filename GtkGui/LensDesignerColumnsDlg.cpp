/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Designer/LensDesigner.h>
#include "LensDesignerColumnsDlg.h"
#include "LensDesignerDlg.h"
#include "FdtdLife.h"
#include "FileSaveDialog.h"
#include "GuiChangeDetector.h"
#include <Box/Utility.h>

// Constructor
void LensDesignerColumnsDlg::construct(FdtdLife* model, LensDesignerDlg* dlg) {
    Notifiable::construct(model);
    model_ = model;
    dlg_ = dlg;
    // Initialise widgets
    addElements({line0_, columns_, line1_, line2_});
    which_.value(designer::LensDesigner::WhichParameter::refractiveIndices);
    populate();
    // Connect handlers
    copyCsv_.signal_clicked().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onCopyCsv));
    pasteCsv_.signal_clicked().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onPasteCsv));
    generateGa_.doSignal().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onGenerateGa));
    export_.doSignal().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onExport));
    makeModel_.doSignal().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onMakeModel));
    colChangedConn_ = columns_.model()->signal_row_changed().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onColumnsChanged));
    connectDefaultChangeHandler(sigc::mem_fun(
            this, &LensDesignerColumnsDlg::onChange));
    which_.signal_changed().connect(
            sigc::mem_fun(this, &LensDesignerColumnsDlg::onWhich));
}

// Initialise the columns of the columns display
void LensDesignerColumnsDlg::initialiseColumns() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        colChangedConn_.disconnect();
        columns_.listWidget().remove_all_columns();
        columnsColumns_ = std::make_unique<ColumnsColumns>(d->columns().size());
        columns_.setColumns(columnsColumns_.get());
        columns_.appendColumn("Layer", columnsColumns_->colLayer_);
        for(size_t i = 0; i < columnsColumns_->colColumns_.size(); i++) {
            columns_.appendColumnEditable("Col " + box::Utility::fromSizeT(i),
                                          columnsColumns_->colColumns_[i]);
        }
        columns_.set_hexpand(true);
        columns_.set_vexpand(true);
        // It looks like clearing the columns removes the connection
        sigc::connection connection;
        colChangedConn_ = columns_.model()->signal_row_changed().connect(
                sigc::mem_fun(this, &LensDesignerColumnsDlg::onColumnsChanged));
    }
}

// Populate the dialog with values from the model
void LensDesignerColumnsDlg::populate() {
    initialiseColumns();
    populateColumns();
    populateControls();
}

// Fill the odd controls from the model
void LensDesignerColumnsDlg::populateControls() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        InitialisingGui::Set s(initialising_);
        exportSymmetrical_.value(d->exportSymmetricalHalf());
        columnNum_.value(d->columnNumber());
        gaRange_.value(d->geneticRange());
        gaBits_.value(d->geneticBits());
        exportLayer_.value(d->exportLayer());
    }
}

// Handle changes to the odd controls
void LensDesignerColumnsDlg::onChange() {
    if(!initialising_) {
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        if(d != nullptr) {
            GuiChangeDetector c;
            auto newExportSymmetrical = c.test(exportSymmetrical_,
                                               d->exportSymmetricalHalf(),
                                               FdtdLife::notifyMinorChange);
            auto newColumnNum = c.test(columnNum_, d->columnNumber(),
                                       FdtdLife::notifyMinorChange);
            auto newGaRange = c.test(gaRange_, d->geneticRange(),
                                     FdtdLife::notifyMinorChange);
            auto newGaBits = c.test(gaBits_, d->geneticBits(),
                                    FdtdLife::notifyMinorChange);
            auto newExportLayer = c.test(exportLayer_, d->exportLayer(),
                                    FdtdLife::notifyMinorChange);
            if(c.changeDetected()) {
                d->exportSymmetricalHalf(newExportSymmetrical);
                d->columnNumber(newColumnNum);
                d->geneticRange(newGaRange);
                d->geneticBits(newGaBits);
                d->exportLayer(newExportLayer);
                // Tell others
                model_->doNotification(c.why());
            }
        }
    }
}

// Populate the column list view
void LensDesignerColumnsDlg::populateColumns() {
    if(!initialising_) {
        InitialisingGui::Set s(initialising_);
        columns_.model()->clear();
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        if(d != nullptr) {
            for(size_t layer = 0; layer < d->numLayers(); layer++) {
                auto row = *(columns_.model()->append());
                row[columnsColumns_->colLayer_] = layer;
                size_t i = 0;
                for(auto& col : d->columns()) {
                    double value = 0.0;
                    if(layer < col.numLayers()) {
                        switch(which_.value()) {
                            case designer::LensDesigner::WhichParameter::refractiveIndices:
                                value = col.cell(layer).refractiveIndex();
                                break;
                            case designer::LensDesigner::WhichParameter::unitCells:
                                value = col.cell(layer).unitCell();
                                break;
                            case designer::LensDesigner::WhichParameter::incidenceAngle:
                                value = col.cell(layer).incidenceAngle() /
                                        box::Constants::deg2rad_;
                                break;
                            case designer::LensDesigner::WhichParameter::patchRatio:
                                value = col.cell(layer).plateSize();
                                break;
                            case designer::LensDesigner::WhichParameter::error:
                                value = col.cell(layer).error();
                                break;
                        }
                    }
                    row[columnsColumns_->colColumns_[i]] = value;
                    i++;
                }
            }
        }
    }
}

// Handle the copy CSV to clipboard command
void LensDesignerColumnsDlg::onCopyCsv() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        model_->clipboard().put(d->getColumnsCsv(which_.value()));
    }
}

// Paste CSV data from the clipboard
void LensDesignerColumnsDlg::onPasteCsv() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        if(model_->clipboard().hasTarget("UTF8_STRING")) {
            std::string text = model_->clipboard().getText();
            d->putColumnsCsv(text);
            model_->doNotification(FdtdLife::notifyMaterialChange);
        }
    }
}

// Handle the generate genetic algorithm commands
void LensDesignerColumnsDlg::onGenerateGa(LensDesignerColumnsDlg::GaCommand v) {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        switch(v) {
            case GaCommand::column:
                d->generateColumnGa();
                break;
            case GaCommand::adjustFull:
                d->generateFullAdjustGa();
                break;
            default:
                break;
        }
    }
}

// Handle the export commands
void LensDesignerColumnsDlg::onExport(LensDesignerColumnsDlg::ExportCommand v) {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        // Get a filename
        std::string extension;
        std::string filterName;
        std::string title;
        switch(v) {
            case ExportCommand::diameterSliceHfss:
                title = "Export diameter slice to HFSS python file";
                extension = "py";
                filterName = "HFSS Python Files";
                break;
            case ExportCommand::quadrantHfss:
                title = "Export quadrant to HFSS python file";
                extension = "py";
                filterName = "HFSS Python Files";
                break;
            case ExportCommand::quadrantHfssLayer:
                title = "Export quadrant layer to HFSS python file";
                extension = "py";
                filterName = "HFSS Python Files";
                break;
        }
        std::string fileName = box::Utility::makeFilename(
                model_->pathName(),
                model_->fileName(),
                extension);
        FileSaveDialog box(dlg_->dlg()->parentWindow(), title, fileName,
                           model_->testSuite(), extension, filterName);
        std::string selected;
        if(box.ask(selected)) {
            // Perform the export
            switch(v) {
                case ExportCommand::diameterSliceHfss:
                    d->exportDiameterSliceAsHfss(selected);
                    break;
                case ExportCommand::quadrantHfss:
                    d->exportQuadrantAsHfss(selected);
                    break;
                case ExportCommand::quadrantHfssLayer:
                    d->exportQuadrantLayerAsHfss(selected);
                    break;
            }
        }
    }
}

// Fill the model according to the current state
void LensDesignerColumnsDlg::onMakeModel(LensDesignerColumnsDlg::FillModel v) {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    if(d != nullptr) {
        switch(v) {
            case FillModel::squarePlates:
                d->modelling(designer::LensDesigner::Modelling::metamaterialCells);
                break;
            case FillModel::dielectricBlocks:
                d->modelling(designer::LensDesigner::Modelling::dielectricBlocks);
                break;
            case FillModel::fingerPatterns:
                d->modelling(designer::LensDesigner::Modelling::fingerPatterns);
                break;
        }
        d->loadModel();
    }
}

// A column entry has been changed
void LensDesignerColumnsDlg::onColumnsChanged(const Gtk::TreeModel::Path& /*path*/,
                                              const Gtk::TreeModel::iterator& pos) {
    if(!initialising_) {
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        if(d != nullptr) {
            auto row = *pos;
            size_t layer = row[columnsColumns_->colLayer_];
            size_t i = 0;
            for(auto& col : d->columns()) {
                double value = row[columnsColumns_->colColumns_[i]];
                auto& cell = col.cell(layer);
                switch(which_.value()) {
                    case designer::LensDesigner::WhichParameter::refractiveIndices:
                        cell.refractiveIndex(value);
                        break;
                    case designer::LensDesigner::WhichParameter::unitCells:
                        cell.unitCell(value);
                        break;
                    case designer::LensDesigner::WhichParameter::incidenceAngle:
                        cell.incidenceAngle(value * box::Constants::deg2rad_);
                        break;
                    case designer::LensDesigner::WhichParameter::patchRatio:
                        cell.plateSize(value);
                        break;
                    case designer::LensDesigner::WhichParameter::error:
                        cell.error(value);
                        break;
                }
                i++;
            }
            d->loadModel();
            model_->doNotification(FdtdLife::notifyMinorChange);
        }
    }
}

// Handle change notifications
void LensDesignerColumnsDlg::notify(fdtd::Notifier* /*source*/, int why,
                                    fdtd::NotificationData* /*data*/) {
    switch(why) {
        case FdtdLife::notifySequencerChange:
        case FdtdLife::notifyMaterialChange:
            populate();
            break;
        default:
            break;
    }
}

// Which parameter to display in the columns has changed
void LensDesignerColumnsDlg::onWhich() {
    populateColumns();
}
