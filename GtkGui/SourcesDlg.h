/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_SOURCESDLG_H
#define FDTDLIFE_SOURCESDLG_H

#include "InitialisingGui.h"
#include "ItemsAndInfoLayout.h"
#include "FdtdLife.h"
#include "NamedButton.h"
#include "FactoryButton.h"
#include "SubDialog.h"
#include "SubDialogFactory.h"
#include <Source/Source.h>

// The dialog panel showing sources on the top level page's notebook
class SourcesDlg : public ItemsAndInfoLayout {
public:
    // Types
    typedef SubDialog<SourcesDlg, source::Source> SubDlg;

    // Construction
    explicit SourcesDlg(FdtdLife& model, Gtk::Window* parentWindow);

    // API
    void populate();

    // Accessors
    FdtdLife& model() { return model_; }

protected:
    // Members
    FdtdLife& model_;
    InitialisingGui initialising_;
    ItemsAndInfoLayout::Columns columns_;
    FactoryButton<source::Source> newButton_{"New Source",
                                             *model_.sources().factory()};
    NamedButton deleteButton_{"Delete Source"};
    SubDialogFactory<SubDlg> dlgFactory_;
    Gtk::Window* parentWindow_;

    // Helpers
    void fillSourceList(source::Source* select = nullptr);

    // Handlers
    void onNewSource(int mode);
    void onDeleteSource();
    void onItemChanged(const Gtk::ListStore::Row& row);
    void onItemSelected(const Gtk::ListStore::Row* row);
};


#endif //FDTDLIFE_SOURCESDLG_H
