/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_NAMEDTWODSLICE_H
#define FDTDLIFE_NAMEDTWODSLICE_H

#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include "GuiElement.h"
#include "TwoDSliceWidget.h"
#include "ContextMenu.h"
#include "Clipboard.h"

// A class that lays out a 2D slice widget with a name widget
class NamedTwoDSlice : public Gtk::VBox, public GuiElement {
public:
    // Construction
    explicit NamedTwoDSlice(const std::string& name);
    explicit NamedTwoDSlice(bool hasTitle = false);

    // Overrides of GuiElement
    Gtk::Widget& widget() override { return *this; }

    // API
    void clipboard(Clipboard* c) { clipboard_ = c; }
    void data(size_t xSize, size_t ySize, const std::vector<double>& d) {
        twoDSlice_.data(xSize, ySize, d);
    }
    void config(double displayMax, TwoDSliceWidget::ColourMap colourMap) {
        twoDSlice_.config(displayMax, colourMap);
    }
    ContextMenu& contextMenu() { return contextMenu_; }
    void title(const std::string& v);
    TwoDSliceWidget& displayWidget() { return twoDSlice_; }


protected:
    // Component widgets
    std::unique_ptr<Gtk::Label> nameLabel_;  // The name attached to the widget
    std::unique_ptr<Gtk::Label> titleLabel_;  // The title attached to the widget
    TwoDSliceWidget twoDSlice_;  // The 2D slice
    ContextMenu contextMenu_;  // The right button click menu
    Clipboard* clipboard_{nullptr};  // The application's clipboard

    // Helpers
    void construct(const std::string& name);

    // Handlers
    void onContextMenu(GdkEventButton* event);
    void onCopyImageToClipboard();
};


#endif //FDTDLIFE_NAMEDTWODSLICE_H
