/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "DataSeriesWidget.h"
#include <Box/Utility.h>
#include <iomanip>

// Colours
const std::vector<std::tuple<double, double, double>> DataSeriesWidget::colours_ = {
        {0.0, 1.0, 0.0},
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 1.0},
        {1.0, 0.0, 1.0},
        {1.0, 1.0, 1.0}
};

// Define or update a data series in the widget
void DataSeriesWidget::data(const std::string& name,
                            const std::vector<double>& data, bool show) {
    // Get or create the data series
    auto pos = std::find_if(series_.begin(), series_.end(),
                            [name](const Data& d) { return d.name_ == name; });
    if(pos == series_.end()) {
        series_.emplace_front(name);
        pos = series_.begin();
    }
    // Copy the data in
    pos->set(data, show);
    // Handle auto scaling
    doAutoYScale();
    // Update the display
    queue_draw();
}

// Define or update a pair of data series in the widget
void DataSeriesWidget::data(const std::string& name,
                            const std::vector<std::complex<double>>& d, bool show) {
    std::vector<double> real;
    std::vector<double> imag;
    for(auto& c : d) {
        real.push_back(c.real());
        imag.push_back(c.imag());
    }
    data("Re(" + name + ")", real, show);
    data("Im(" + name + ")", imag, show);
}

// Set the X axis information for the widget
void DataSeriesWidget::axisX(double min, double step, size_t n, const std::string& units) {
    n = std::max(n, 2UL);
    xAxis_.set(false, min, min + (static_cast<double>(n) - 1) * step,
               step, units);
}

// Set the Y axis information for the widget
void DataSeriesWidget::axisY(double min, double max, const std::string& units) {
    yAxis_.set(false, min, max, 1.0, units);
}

// Set the Y axis information for the widget
void DataSeriesWidget::axisY(const std::string& units) {
    yAxis_.set(true, 0.0, 0.0, 1.0, units);
    doAutoYScale();
}

// Checks the data supplied for validity
bool DataSeriesWidget::dataValid() {
    bool result = true;
    // Do we have any data sets?
    result = !series_.empty();
    // Make sure all data sets have the same length that is greater than zero
    if(result) {
        size_t dataLength = series_.begin()->data_.size();
        result = dataLength > 0;
        for(auto& item : series_) {
            result = result && item.data_.size() == dataLength;
        }
    }
    return result;
}

// Calculate the scaling factors and tick mark intervals.
void DataSeriesWidget::calcScaling(int width, int height) {
    // Calculate scaling etc.
    xAxis_.calcScaling(width);
    yAxis_.calcScaling(height);
}

// Draw the binary pattern widget
void DataSeriesWidget::draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) {
    drawBackground(cr, width, height);
    if(dataValid()) {
        calcScaling(width, height);
        drawVerticalAxis(cr, width, height);
        drawHorizontalAxis(cr, width, height);
        drawData(cr, width, height);
        drawMarkers(cr, width, height);
        drawLegend(cr, width, height);
    }
}

// Fill the background of the panel
void DataSeriesWidget::drawBackground(const Cairo::RefPtr<Cairo::Context>& cr,
                                      int width, int height) {
    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->rectangle(0.0, 0.0, width, height);
    cr->fill();
}

// Draw the vertical axis
void DataSeriesWidget::drawVerticalAxis(const Cairo::RefPtr<Cairo::Context>& cr,
                                        int width, int height) {
    Pango::FontDescription font("sans normal 9");
    double tick = (floor(yAxis_.min_ / yAxis_.tickStep_) + 1.0) * yAxis_.tickStep_;
    int digits = 1 - static_cast<int>(round(log10(yAxis_.tickStep_)));
    if(digits <= 0) {
        digits = 1;
    }
    while(tick < yAxis_.max_) {
        std::stringstream s;
        if(digits <= 4) {
            s << std::fixed << std::setprecision(digits) << tick << yAxis_.units_;
        } else {
            s << std::scientific << std::setprecision(2) << tick << yAxis_.units_;
        }
        int tickY = height - static_cast<int>(round((tick - yAxis_.min_) * yAxis_.scale_));
        cr->set_source_rgb(0.0, 0.0, 0.5);
        cr->set_line_width(1.0);
        cr->move_to(0, tickY + 0.5);
        cr->line_to(width, tickY + 0.5);
        cr->stroke();
        auto layout = create_pango_layout(s.str());
        layout->set_font_description(font);
        int textWidth, textHeight;
        layout->get_pixel_size(textWidth, textHeight);
        cr->set_source_rgb(0.50, 0.50, 0.50);
        cr->move_to(5, tickY - textHeight / 2.0);
        layout->show_in_cairo_context(cr);
        tick += yAxis_.tickStep_;
    }
}

// Draw the horizontal axis
void DataSeriesWidget::drawHorizontalAxis(const Cairo::RefPtr<Cairo::Context>& cr,
                                          int /*width*/, int height) {
    Pango::FontDescription font("sans normal 9");
    double tick = (floor(xAxis_.min_ / xAxis_.tickStep_) + 1.0) * xAxis_.tickStep_;
    int digits = 1 - static_cast<int>(round(log10(xAxis_.tickStep_)));
    if(digits <= 0) {
        digits = 1;
    }
    while(tick < xAxis_.max_) {
        std::stringstream s;
        if(digits <= 4) {
            s << std::fixed << std::setprecision(digits) << tick << xAxis_.units_;
        } else {
            s << std::scientific << std::setprecision(2) << tick << xAxis_.units_;
        }
        int tickX = static_cast<int>(round((tick - xAxis_.min_) * xAxis_.scale_));
        cr->set_source_rgb(0.0, 0.0, 0.5);
        cr->set_line_width(1.0);
        cr->move_to(tickX + 0.5, height);
        cr->line_to(tickX + 0.5, 0.0);
        cr->stroke();
        auto layout = create_pango_layout(s.str());
        layout->set_font_description(font);
        int textWidth, textHeight;
        layout->get_pixel_size(textWidth, textHeight);
        cr->set_source_rgb(0.50, 0.50, 0.50);
        cr->move_to(tickX - textWidth / 2.0, height - textHeight - 5);
        layout->show_in_cairo_context(cr);
        tick += xAxis_.tickStep_;
    }
}

// Draw the data streams themselves
void DataSeriesWidget::drawData(const Cairo::RefPtr<Cairo::Context>& cr,
                                int /*width*/, int height) {
    size_t colourIndex = numVisibleSeries() % colours_.size();
    for(auto& d : series_) {
        if(d.show_) {
            if(colourIndex == 0) {
                colourIndex = colours_.size() - 1;
            } else {
                colourIndex--;
            }
            if(!d.data_.empty()) {
                auto& col = colours_[colourIndex];
                cr->set_source_rgb(std::get<0>(col), std::get<1>(col),
                                   std::get<2>(col));
                double x = 0.0;
                double y = height - round((d.data_[0] - yAxis_.min_) * yAxis_.scale_);
                cr->move_to(x + 0.5, y + 0.5);
                for(size_t i = 1; i < d.data_.size(); i++) {
                    x += xAxis_.scale_ * xAxis_.step_;
                    y = height - round((d.data_[i] - yAxis_.min_) * yAxis_.scale_);
                    cr->line_to(x + 0.5, y + 0.5);
                }
                cr->stroke();
            }
        }
    }
}

// Draw the markers
void DataSeriesWidget::drawMarkers(const Cairo::RefPtr<Cairo::Context>& cr,
                                   int /*width*/, int height) {
    if(hasMarkers_) {
        size_t colourIndex = numVisibleSeries() % colours_.size();
        for(auto& d : series_) {
            if(d.show_) {
                if(colourIndex == 0) {
                    colourIndex = colours_.size() - 1;
                } else {
                    colourIndex--;
                }
                if(markerPos_ < d.data_.size()) {
                    double x = markerPos_ * xAxis_.scale_ * xAxis_.step_;
                    double y = height -
                               round((d.data_[markerPos_] - yAxis_.min_) * yAxis_.scale_);
                    auto& col = colours_[colourIndex];
                    cr->set_source_rgb(std::get<0>(col), std::get<1>(col),
                                       std::get<2>(col));
                    cr->rectangle(x - markerSize_ / 2.0, y - markerSize_ / 2.0,
                                  markerSize_, markerSize_);
                    cr->stroke();
                }
            }
        }
    }
}

// Draw the legend text
void DataSeriesWidget::drawLegend(const Cairo::RefPtr<Cairo::Context>& cr,
                                  int width, int /*height*/) {
    Pango::FontDescription font("sans normal 9");
    size_t colourIndex = 0;
    double y = 5.0;
    double x = width / 2.0;
    if(hasMarkers_) {
        std::stringstream s;
        s << markerX() << " " << xAxis_.units_;
        auto xLayout = create_pango_layout(s.str());
        xLayout->set_font_description(font);
        int textWidth, textHeight;
        xLayout->get_pixel_size(textWidth, textHeight);
        cr->set_source_rgb(0.50, 0.50, 0.50);
        cr->move_to(x, y);
        xLayout->show_in_cairo_context(cr);
        y += textHeight;
    }
    for(auto pos = series_.rbegin(); pos != series_.rend(); ++pos) {
        if(pos->show_) {
            auto& col = colours_[colourIndex];
            std::stringstream s;
            s << pos->name_;
            if(hasMarkers_ && markerPos_ < pos->data_.size()) {
                s << " " << pos->data_[markerPos_] << " " << yAxis_.units_;
            }
            auto yLayout = create_pango_layout(s.str());
            yLayout->set_font_description(font);
            int textWidth, textHeight;
            yLayout->get_pixel_size(textWidth, textHeight);
            cr->set_source_rgb(0.50, 0.50, 0.50);
            cr->move_to(x, y);
            yLayout->show_in_cairo_context(cr);
            cr->set_source_rgb(std::get<0>(col), std::get<1>(col),
                               std::get<2>(col));
            cr->move_to(x - 3.0, y + textHeight / 2.0);
            cr->line_to(x - 13.0, y + textHeight / 2.0);
            cr->stroke();
            y += textHeight;
            colourIndex++;
            if(colourIndex == colours_.size()) {
                colourIndex = 0;
            }
        }
    }
}

// Switch markers on and position them at the specified location
void DataSeriesWidget::markers(size_t pos) {
    hasMarkers_ = true;
    markerPos_ = pos;
    queue_draw();
}

// Switch markers on and position them at the location specified
// by the x value
void DataSeriesWidget::markers(double x) {
    hasMarkers_ = true;
    if(xAxis_.step_ > 0.0 && !series_.empty()) {
        // Convert the x coordinate into an index into the data
        int pos = static_cast<int>(round((x - xAxis_.min_) / xAxis_.step_));
        if(pos < 0.0) {
            markerPos_ = 0;
        } else if(pos >= static_cast<int>(series_.front().data_.size())) {
            markerPos_ = series_.front().data_.size() - 1;
        } else {
            markerPos_ = static_cast<size_t>(pos);
        }
        queue_draw();
    }
}

// Switch markers off
void DataSeriesWidget::markersOff() {
    hasMarkers_ = false;
    queue_draw();
}

// Override of the button press handler
bool DataSeriesWidget::on_button_press_event(GdkEventButton* button_event) {
    bool result = CustomWidget::on_button_press_event(button_event);
    auto allocation = get_allocation();
    if(button_event->x >= 0 && button_event->x < allocation.get_width() &&
       button_event->y >= 0 && button_event->y < allocation.get_height()) {
        // Do the scaling if necessary
        calcScaling(allocation.get_width(), allocation.get_height());
        // Set the marker to this x position
        markerPos_ = static_cast<size_t>(std::round(button_event->x /
                                                    xAxis_.scale_ / xAxis_.step_));
        // Cause a redraw
        queue_draw();
    }
    return result;
}

// Return the X value of the marker position in units
double DataSeriesWidget::markerX() const {
    return markerPos_ * xAxis_.step_ + xAxis_.min_;
}

// Return a Y value associated with the marker position
double DataSeriesWidget::markerY(const std::string& name) const {
    double result = 0.0;
    auto pos = std::find_if(series_.begin(), series_.end(),
                            [name](const Data& d) { return d.name_ == name; });
    if(pos != series_.end()) {
        if(markerPos_ < pos->data_.size()) {
            result = pos->data_[markerPos_];
        }
    }
    return result;
}

// Calculate the range for the Y axis in auto scaling mode
void DataSeriesWidget::doAutoYScale() {
    if(yAxis_.automatic_) {
        double min = 0.0;
        double max = 0.0;
        if(!series_.empty() && !series_.front().data_.empty()) {
            bool first = true;
            for(auto& series : series_) {
                if(series.show_) {
                    for(auto& point : series.data_) {
                        if(first) {
                            min = point;
                            max = point;
                            first = false;
                        } else {
                            min = std::min(min, point);
                            max = std::max(max, point);
                        }
                    }
                }
            }
        }
        if(box::Utility::equals(min, max)) {
            max = min + 1.0;
        }
        yAxis_.set(min, max);
    }
}

// Returns the number of visible series
size_t DataSeriesWidget::numVisibleSeries() {
    size_t result = 0;
    for(auto& s : series_) {
        if(s.show_) {
            result++;
        }
    }
    return result;
}

// Set axis configuration
void DataSeriesWidget::Axis::set(bool automatic, double min, double max, double step,
                                 const std::string& units) {
    automatic_ = automatic;
    min_ = min;
    max_ = max;
    step_ = step;
    units_ = units;
}

// Set axis configuration in automatic mode.
// Returns true if they have changed
void DataSeriesWidget::Axis::set(double min, double max) {
    min_ = min;
    max_ = max;
}

// Calculate the scaling factor and tick mark spacing for the axis
void DataSeriesWidget::Axis::calcScaling(int pixels) {
    scale_ = 1.0;
    tickStep_ = pixels;
    double diff = max_ - min_;
    if(diff > 0.0) {
        // Scaling
        scale_ = static_cast<double>(pixels) / diff;
        // Tick marks
        tickStep_ = pow(10.0, floor(log10(diff)));
        if(diff / tickStep_ < 2.0) {
            tickStep_ /= 4.0;
        } else if(diff / tickStep_ < 3.0) {
            tickStep_ /= 2.0;
        }
        if(diff / tickStep_ > 8.0) {
            tickStep_ *= 2.0;
        }
    }
}

// Clear the widget
void DataSeriesWidget::clear() {
    series_.clear();
}

// Convert the widget data into CSV
std::string DataSeriesWidget::toCsv() const {
    std::stringstream t;
    if(!series_.empty()) {
        // The header line
        t << "x";
        for(auto& d :  series_) {
            t << "\t\"" << d.name_ << "\"";
        }
        t << std::endl;
        // Data lines
        for(size_t x = 0U; x < series_.front().data_.size(); x++) {
            double xValue = x * xAxis_.step_ + xAxis_.min_;
            t << xValue;
            for(auto& d :  series_) {
                t << "\t\"" << d.data_[x] << "\"";
            }
            t << std::endl;
        }
    }
    return t.str();
}
