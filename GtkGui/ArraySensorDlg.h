/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_ARRAYSENSORDLG_H
#define FDTDLIFE_ARRAYSENSORDLG_H

#include "SensorsDlg.h"
#include <Sensor/ArraySensor.h>
#include "NamedEntry.h"
#include "NamedSelection.h"
#include "NamedToggleButton.h"
#include "NamedMenuButton.h"
#include "HorizontalLayout.h"

// A dialog panel that shows array sensorId parameters
class ArraySensorDlg : public SensorsDlg::SubDlg {
public:
    // Types
    enum class Command {
        csvTransmittedTimeSeries = 0, csvReflectedTimeSeries, csvTransmittedSpectra,
        csvReflectedSpectra, xmlTransmittedAdmittance
    };

    // Constructor
    ArraySensorDlg() = default;
    void construct(SensorsDlg* owner, sensor::Sensor* sensor) override;

    // API
    void populate() override;

protected:
    // Members
    sensor::ArraySensor* sensor_{};

    // The widgets
    NamedSelection<box::Constants::Colour> colour_{
            "Display Colour", box::Constants::colourNames_};
    NamedEntry<double> currentFrequency_{"Current Frequency"};
    NamedEntry<size_t> numPointsX_{"Num Points X"};
    NamedEntry<size_t> numPointsY_{"Num Points Y"};
    NamedToggleButton averageOverCell_{"Average Over Cell"};
    NamedToggleButton includeReflectionData_{"Include Reflection Data"};
    NamedToggleButton useWindow_{"Use Window"};
    NamedToggleButton animateFrequency_{"Animate Frequency"};
    NamedEntry<double> frequencyStep_{"Frequency Step"};
    NamedToggleButton manualZ_{"Manual Z"};
    NamedEntry<double> transmittedZ_{"Transmitted Z (m)", true};
    NamedEntry<double> reflectedZ_{"Reflected Z (m)", true};
    NamedToggleButton componentX_{"Components: X"};
    NamedToggleButton componentY_{"Y"};
    NamedToggleButton componentZ_{"Z"};
    HorizontalLayout componentsLine_{{componentX_, componentY_, componentZ_}, 3};
    NamedToggleButton manualSamples_{"Manual Samples"};
    NamedEntry<size_t> numberOfSamples_{"Number Of Samples", true};
    NamedMenuButton<Command> copyToClipboard_{
            "Copy To Clipboard",
            {{"Transmitted Time Series CSV", Command::csvTransmittedTimeSeries},
             {"Reflected Time Series CSV", Command::csvReflectedTimeSeries},
             {"Transmitted Spectra CSV", Command::csvTransmittedSpectra},
             {"Reflected Spectra CSV", Command::csvReflectedSpectra},
             {"Transmitted AdmittanceXML", Command::xmlTransmittedAdmittance}}};

    // Handlers
    virtual void onChange();
    void onCommand(Command cmd);
};

#endif //FDTDLIFE_ARRAYSENSORDLG_H
