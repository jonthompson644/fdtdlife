/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "DataSeriesViewItem.h"
#include "ViewFrame.h"
#include "FdtdLife.h"

// Second stage constructor used by the factory
void DataSeriesViewItem::construct(int mode, int id, ViewFrame* frame) {
    registerChild(dataDisplay_);
    ViewItem::construct(mode, id, frame);
    dataDisplay_.clipboard(&frame->m()->clipboard());
    pack_start(dataDisplay_.widget(), Gtk::PACK_EXPAND_WIDGET);
    initialise();
}

// Write the configuration to an XML file
void DataSeriesViewItem::writeConfig(xml::DomObject& root) const {
    // Base class first
    ViewItem::writeConfig(root);
    // Now my things
    root << xml::Reopen();
    root << xml::Obj("xmin") << xMin_;
    root << xml::Obj("xmax") << xMax_;
    root << xml::Obj("xautomatic") << xAutomatic_;
    root << xml::Obj("ymin") << yMin_;
    root << xml::Obj("ymax") << yMax_;
    root << xml::Obj("yautomatic") << yAutomatic_;
    root << xml::Obj("title") << title_;
    root << xml::Close();
}

// Read the configuration from an XML file
void DataSeriesViewItem::readConfig(xml::DomObject& root) {
    // Base class first
    ViewItem::readConfig(root);
    // Now my things
    root >> xml::Reopen();
    root >> xml::Obj("xmin") >> xMin_;
    root >> xml::Obj("xmax") >> xMax_;
    root >> xml::Obj("xautomatic") >> xAutomatic_;
    root >> xml::Obj("ymin") >> yMin_;
    root >> xml::Obj("ymax") >> yMax_;
    root >> xml::Obj("yautomatic") >> yAutomatic_;
    root >> xml::Obj("title") >> xml::Default("") >> title_;
    root >> xml::Close();
}

// Initialise the item ready for the run
void DataSeriesViewItem::initialise() {
    std::string title = title_;
    if(title_.empty())
    {
        auto* s = sensor();
        if(s != nullptr)
        {
            title = s->name() + ":" + dataSpecName_;
        }
    }
    dataDisplay_.title(title);
}

// Update the data the view is displaying
void DataSeriesViewItem::update() {
    // Get data from the sensor
    auto* spec = dataSpec();
    if(spec != nullptr) {
        sensor::DataSpec::Info info;
        spec->getInfo(&info);
        fixInfo(info);
        if(info.xSize_ > 0) {
            std::vector<std::vector<double>> data;
            data.resize(spec->numChannels());
            for(size_t c = 0; c < spec->numChannels(); c++) {
                data[c].resize(info.xSize_);
            }
            std::vector<double> dy;
            dy.resize(spec->numChannels());
            double dx = 0.0;
            for(size_t x = 0; x < info.xSize_; x++) {
                spec->getData(x, dy, dx);
                convertData(static_cast<int>(x), dy, dx);
                for(size_t c = 0; c < dy.size() && c < data.size(); c++) {
                    data[c][x] = dy[c];
                }
            }
            double step = (info.xRight_ - info.xLeft_) / (static_cast<double>(info.xSize_) - 1);
            dataDisplay_.axisX(info.xLeft_, step, info.xSize_, info.xUnits_);
            if(yAutomatic_) {
                dataDisplay_.axisY(info.yUnits_);
            } else {
                dataDisplay_.axisY(yMin_, yMax_, info.yUnits_);
            }
            for(size_t c = 0; c < spec->numChannels(); c++) {
                dataDisplay_.data(spec->channelName(c), data[c], info.channelUsed(c));
            }
        }
    }
}
