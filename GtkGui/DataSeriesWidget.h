/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DATASERIESWIDGET_H
#define FDTDLIFE_DATASERIESWIDGET_H

#include <utility>
#include <complex>
#include <vector>
#include "CustomWidget.h"

class FdtdLife;

// A widget that plots multiple data series on an xy graph
class DataSeriesWidget : public CustomWidget {
public:
    // Types
    class Data {
    public:
        explicit Data(std::string name) :
                name_(std::move(name)) {}
        void set(const std::vector<double>& data, bool show) {
            data_ = data;
            show_ = show;
        }
        std::vector<double> data_;
        std::string name_;
        bool show_;
    };

    class Axis {
    public:
        Axis() = default;
        void set(bool automatic, double min, double max, double step,
                 const std::string& units);
        void set(double min, double max);
        void calcScaling(int pixels);
        double min_{0.0};  // Minimum value
        double max_{1.0};  // Maximum value
        bool automatic_{false};  // Scale to match data
        std::string units_;  // Units text
        double scale_{};  // Scale factor, pixels per unit
        double tickStep_{};  // Scale tick step
        double step_{};  // Step size for the x axis, units per step
    };

    // Construction
    DataSeriesWidget() = default;

    // API
    void clear();
    void data(const std::string& name, const std::vector<double>& d, bool show = true);
    void data(const std::string& name, const std::vector<std::complex<double>>& d,
              bool show = true);
    void axisX(double min, double step, size_t n, const std::string& units);
    void axisY(double min, double max, const std::string& units);
    void axisY(const std::string& units);
    void markers(size_t pos);  // Set the marker position directly as the index
    void markers(double pos);  // Set the marker to the given x position
    void markersOff();
    double markerX() const;
    double markerY(const std::string& name) const;
    std::string toCsv() const;

    // Overrides of CustomWidget
    void draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height) override;
    bool on_button_press_event(GdkEventButton* button_event) override;

protected:
    // Members
    std::list<Data> series_;
    Axis xAxis_;
    Axis yAxis_;
    bool hasMarkers_{true};
    size_t markerPos_{0};  // Position of the marker as an index into the data
    static const std::vector<std::tuple<double, double, double>> colours_;
    static constexpr double markerSize_ = 5.0;

    // Helper functions
    bool dataValid();
    void calcScaling(int width, int height);
    static void drawBackground(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void drawVerticalAxis(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void drawHorizontalAxis(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void drawData(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void drawMarkers(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void drawLegend(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
    void doAutoYScale();
    size_t numVisibleSeries();
};


#endif //FDTDLIFE_DATASERIESWIDGET_H
