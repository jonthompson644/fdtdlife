/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CHARACTERIZERSTEPSDLG_H
#define FDTDLIFE_CHARACTERIZERSTEPSDLG_H

#include "GridLayout.h"
#include <Seq/Characterizer.h>
#include <Fdtd/Notifiable.h>
#include "InitialisingGui.h"
#include "NamedListView.h"
#include "VerticalLayout.h"
#include "NamedDataSeries.h"
#include "HorizontalLayout.h"

class CharacterizerDlg;

class CharacterizerStepsDlg : public GridLayout, public fdtd::Notifiable {
public:
    // Construction
    CharacterizerStepsDlg() = default;
    void construct(seq::Characterizer* sequencer, CharacterizerDlg* dlg);

    // Overrides of Notifiable
    void notify(fdtd::Notifier* source, int why, fdtd::NotificationData* data) override;

    // API
    void populate(seq::CharacterizerStep* sel=nullptr);

protected:
    // Handlers
    void onStepSelected();

    // Members
    seq::Characterizer* sequencer_{};
    CharacterizerDlg* dlg_{};
    InitialisingGui initialising_;

    // Steps list view columns
    class StepsColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        StepsColumns() {
            add(colStep_);
            add(colDescription_);
            add(colStatus_);
        }
        // Columns
        Gtk::TreeModelColumn<std::string> colStep_;
        Gtk::TreeModelColumn<std::string> colDescription_;
        Gtk::TreeModelColumn<std::string> colStatus_;
    } stepsColumns_;

    // Widgets
    NamedListView steps_{"Steps"};
    NamedDataSeries transmittance_{std::string("Transmittance")};
    NamedDataSeries phaseShift_{std::string("Phase Shift")};
    HorizontalLayout plots_{{transmittance_, phaseShift_}};

    // Helpers
    std::shared_ptr<seq::CharacterizerStep> currentStep();
    void plotItemData(const std::shared_ptr<seq::CharacterizerStep>& item);
};


#endif //FDTDLIFE_CHARACTERIZERSTEPSDLG_H
