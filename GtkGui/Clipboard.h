/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CLIPBOARD_H
#define FDTDLIFE_CLIPBOARD_H

#include <string>
#include <gtkmm/clipboard.h>

// A wrapper for the GTK clipboard to make it easy to use in this application
class Clipboard {
public:
    // Construction
    Clipboard() = default;
    ~Clipboard();

    // API
    void put(const std::string& value, const std::string& target="");
    void put(const Glib::RefPtr<Gdk::Pixbuf>& value);
    const std::string& getText(const std::string& target="UTF8_STRING");
    const Glib::RefPtr<Gdk::Pixbuf>& getImage();
    std::vector<Glib::ustring> targets();
    bool hasTarget(const std::string& target);

protected:
    // Members
    std::string clipboardText_;
    std::string returnedText_;
    Glib::RefPtr<Gdk::Pixbuf> returnedImage_;

    // Handlers
    void onClipboardGet(Gtk::SelectionData& selectionData, guint info);
    void onClipboardClear();
};


#endif //FDTDLIFE_CLIPBOARD_H
