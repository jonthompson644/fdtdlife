/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "ConfigurationDlg.h"
#include "GuiChangeDetector.h"

// Constructor
ConfigurationDlg::ConfigurationDlg(FdtdLife& model, Gtk::Window* parentWindow) :
        Notifiable(&model),
        model_(model),
        parentWindow_(parentWindow),
        dynamicInfoWait_(
                sigc::mem_fun(this, &ConfigurationDlg::onDynamicInfoExpiry)) {
    // Initialise
    addLines({
                     {fdtd_,              propMatrices_},
                     {lowCorner_},
                     {highCorner_},
                     {cellSize_},
                     {nCells_},
                     {boundaries_},
                     {scatteredZone_,     stabilityFactor_},
                     {boundarySize_,      useThinSheetSubcells_},
                     {numThreads_,        threadsAvail_},
                     {usingOpenMP_,       threadsUsed_},
                     {animationPeriod_,   whiteBackground_},
                     {timeSpan_,          timeStep_},
                     {curTimeStep_,       nTimeSteps_},
                     {memoryUse_,         stepTime_},
                     {cellRate_,          maxRate_},
                     {propMatrixFormula_, admittanceTableButton_},
             });
    populate();
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(
            this, &ConfigurationDlg::onChange));
}

// Populate all the widgets with values
void ConfigurationDlg::populate() {
    populateConfiguration();
    populateStaticInformation();
    populateDynamicInformation();
}

// Populate all the widgets with values
void ConfigurationDlg::populateConfiguration() {
    auto* p = model_.p();
    InitialisingGui::Set s(initialising_);
    // Configuration
    fdtd_.value(p->doFdtd());
    propMatrices_.value(p->doPropagationMatrices());
    stabilityFactor_.value(p->s());
    scatteredZone_.value(p->scatteredFieldZoneSize());
    lowCorner_.value(p->p1());
    highCorner_.value(p->p2());
    cellSize_.value(p->dr());
    boundarySize_.value(p->boundarySize());
    boundaries_.value(p->boundary());
    timeSpan_.value(p->tSize());
    numThreads_.value(p->numThreadsCfg());
    propMatrixFormula_.value(p->plateAdmittance().formula());
    animationPeriod_.value(p->animationPeriod());
    whiteBackground_.value(model_.whiteBackground());
    useThinSheetSubcells_.value(p->useThinSheetSubcells());
}

// Populate all the widgets with values
void ConfigurationDlg::populateStaticInformation() {
    auto* p = model_.p();
    InitialisingGui::Set s(initialising_);
    // Information
    nCells_.value(p->geometry()->n());
    timeStep_.value(p->dt());
    nTimeSteps_.value(p->nt());
    memoryUse_.value(model_.memoryUse() / box::Constants::mega_);
    usingOpenMP_.value(p->usingOpenMp());
    threadsUsed_.value(p->numThreadsUsed());
    threadsAvail_.value(p->maxNumThreads());
}

// Populate all the widgets with values
void ConfigurationDlg::populateDynamicInformation() {
    auto* p = model_.p();
    InitialisingGui::Set s(initialising_);
    // Information
    curTimeStep_.value(p->timeStep());
    stepTime_.value(p->stepProcessingTime());
    cellRate_.value(p->cellProcessingRate());
    maxRate_.value(p->maxCellProcessingRate());
    dynamicInfoUpdateWaiting_ = false;
}

// The dynamic information update dead time is over
void ConfigurationDlg::onDynamicInfoExpiry() {
    if(dynamicInfoUpdateWaiting_) {
        populateDynamicInformation();
        dynamicInfoWait_.start(dynamicInfoDeadTime_);
    }
}

// Default change handler
void ConfigurationDlg::onChange() {
    if(!initialising_) {
        auto* p = model_.p();
        // Test for changes
        GuiChangeDetector c;
        auto newDoFdtd = c.test(fdtd_, p->doFdtd(), FdtdLife::notifyMinorChange);
        auto newDoPropMat = c.test(propMatrices_, p->doPropagationMatrices(),
                                   FdtdLife::notifyMinorChange);
        auto newS = c.test(stabilityFactor_, p->s(), FdtdLife::notifyMinorChange);
        auto newScatteredZone = c.test(scatteredZone_, p->scatteredFieldZoneSize(),
                                       FdtdLife::notifyDomainSizeChange);
        auto newP1 = c.test(lowCorner_, p->p1(), FdtdLife::notifyDomainSizeChange);
        auto newP2 = c.test(highCorner_, p->p2(), FdtdLife::notifyDomainSizeChange);
        auto newDr = c.test(cellSize_, p->dr(), FdtdLife::notifyDomainSizeChange);
        auto newBoundarySize = c.test(boundarySize_, p->boundarySize(),
                                      FdtdLife::notifyDomainSizeChange);
        auto newBoundary = c.test(boundaries_, p->boundary(),
                                  FdtdLife::notifyDomainSizeChange);
        auto newTSize = c.test(timeSpan_, p->tSize(), FdtdLife::notifyMinorChange);
        auto newNumThreads = c.test(numThreads_, p->numThreadsCfg(),
                                    FdtdLife::notifyMinorChange);
        auto newPropMatFormula = c.test(propMatrixFormula_, p->plateAdmittance().formula(),
                                        FdtdLife::notifyMinorChange);
        auto newAnimationPeriod = c.test(animationPeriod_, p->animationPeriod(),
                                         FdtdLife::notifyMinorChange);
        auto newWhiteBackground = c.test(whiteBackground_,
                                         model_.whiteBackground(),
                                         FdtdLife::notifyMinorChange);
        auto newUseThinSheets = c.test(useThinSheetSubcells_, p->useThinSheetSubcells(),
                                       FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            // Write any changes
            p->doFdtd(newDoFdtd);
            p->doPropagationMatrices(newDoPropMat);
            p->s(newS);
            p->scatteredFieldZoneSize(newScatteredZone);
            p->p1(newP1);
            p->p2(newP2);
            p->dr(newDr);
            p->boundarySize(newBoundarySize);
            p->boundary(newBoundary);
            p->tSize(newTSize);
            p->numThreadsCfg(newNumThreads);
            p->plateAdmittance().formula(newPropMatFormula);
            p->animationPeriod(newAnimationPeriod);
            model_.whiteBackground(newWhiteBackground);
            p->useThinSheetSubcells(newUseThinSheets);
            // Tell others
            model_.doNotification(c.why());
        }
    }
}

// Handle a notification
void ConfigurationDlg::notify(fdtd::Notifier* /*source*/, int why,
                              fdtd::NotificationData* /*data*/) {
    switch(why) {
        case FdtdLife::notifySteppedE:
        case FdtdLife::notifySteppedH:
            break;
        case FdtdLife::notifyStepComplete:
            if(dynamicInfoWait_.running()) {
                dynamicInfoUpdateWaiting_ = true;
            } else {
                populateDynamicInformation();
                dynamicInfoWait_.start(dynamicInfoDeadTime_);
            }
            break;
        case FdtdLife::notifyModelStop:
            populateDynamicInformation();
            dynamicInfoWait_.stop();
            break;
        default:
            populateStaticInformation();
            populateDynamicInformation();
            break;
    }
}
