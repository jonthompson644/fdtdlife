/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensDesignerPhaseDlg.h"
#include "FdtdLife.h"
#include "GuiChangeDetector.h"
#include <Designer/LensDesigner.h>

// Second stage constructor
void LensDesignerPhaseDlg::construct(FdtdLife* model, LensDesignerDlg* dlg) {
    model_ = model;
    dlg_ = dlg;
    addLines({{permittivityAbove_,      focalLength_},
              {permittivityInside_,     layerSpacing_},
              {permittivityBelow_,      centreFrequency_},
              {unitCell_,               lensDiameter_},
              {totalNumberLayers_,      numberArStages_},
              {layersPerArStage_,       edgeDistance_},
              {modelling_,              fdtdCellsPerUnitCell_},
              {focalLengthFudgeFactor_, plateThickness_},
              {sliceThickness_,         bitsPerHalfSide_},
              {minFeatureSize_,         fingerOffset_},
              {columnType_},
              {errors_},
              {buttons_}});
    // Connect handlers
    connectDefaultChangeHandler(sigc::mem_fun(this,
                                              &LensDesignerPhaseDlg::onChange));
    generate_.signal_clicked().connect(
            sigc::mem_fun(this, &LensDesignerPhaseDlg::onGenerate));
}

// Populate the widgets with values
void LensDesignerPhaseDlg::populate() {
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    InitialisingGui::Set s(initialising_);
    permittivityAbove_.value(d->permittivityAbove());
    permittivityInside_.value(d->permittivityIn());
    permittivityBelow_.value(d->permittivityBelow());
    focalLength_.value(d->focusDistance());
    layerSpacing_.value(d->layerSpacing());
    centreFrequency_.value(d->centerFrequency());
    unitCell_.value(d->unitCell());
    lensDiameter_.value(d->lensDiameter());
    totalNumberLayers_.value(d->numLayers());
    numberArStages_.value(d->numArcStages());
    layersPerArStage_.value(d->layersPerArcStage());
    edgeDistance_.value(d->edgeDistance());
    modelling_.value(d->modelling());
    fdtdCellsPerUnitCell_.value(d->fdtdCellsPerUnitCell());
    focalLengthFudgeFactor_.value(d->focalLengthFudgeFactor());
    plateThickness_.value(d->plateThickness());
    sliceThickness_.value(d->sliceThickness());
    bitsPerHalfSide_.value(d->bitsPerHalfSide());
    minFeatureSize_.value(d->minFeatureSize());
    fingerOffset_.value(d->fingerOffset());
    columnType_.value(d->columnType());
    errors_.value(d->error());
}

// Default change handler
void LensDesignerPhaseDlg::onChange() {
    if(!initialising_) {
        auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
        GuiChangeDetector c;
        auto newPermittivityAbove = c.test(permittivityAbove_, d->permittivityAbove(),
                                           FdtdLife::notifyMinorChange);
        auto newPermittivityInside = c.test(permittivityInside_, d->permittivityIn(),
                                            FdtdLife::notifyMinorChange);
        auto newPermittivityBelow = c.test(permittivityBelow_, d->permittivityBelow(),
                                           FdtdLife::notifyMinorChange);
        auto newFocalLength = c.test(focalLength_, d->focusDistance(),
                                     FdtdLife::notifyMinorChange);
        auto newLayerSpacing = c.test(layerSpacing_, d->layerSpacing(),
                                      FdtdLife::notifyMinorChange);
        auto newCentreFrequency = c.test(centreFrequency_, d->centerFrequency(),
                                         FdtdLife::notifyMinorChange);
        auto newUnitCell = c.test(unitCell_, d->unitCell(),
                                  FdtdLife::notifyMinorChange);
        auto newLensDiameter = c.test(lensDiameter_, d->lensDiameter(),
                                      FdtdLife::notifyMinorChange);
        auto newTotalNumberLayers = c.test(totalNumberLayers_, d->numLayers(),
                                           FdtdLife::notifyMinorChange);
        auto newNumberArStages = c.test(numberArStages_, d->numArcStages(),
                                        FdtdLife::notifyMinorChange);
        auto newLayersPerArStage = c.test(layersPerArStage_, d->layersPerArcStage(),
                                          FdtdLife::notifyMinorChange);
        auto newEdgeDistance = c.test(edgeDistance_, d->edgeDistance(),
                                      FdtdLife::notifyMinorChange);
        auto newModelling = c.test(modelling_, d->modelling(),
                                   FdtdLife::notifyMinorChange);
        auto newFdtdCellsPerUnitCell = c.test(fdtdCellsPerUnitCell_,
                                              d->fdtdCellsPerUnitCell(),
                                              FdtdLife::notifyMinorChange);
        auto newFocalLengthFudgeFactor = c.test(focalLengthFudgeFactor_,
                                                d->focalLengthFudgeFactor(),
                                                FdtdLife::notifyMinorChange);
        auto newPlateThickness = c.test(plateThickness_, d->plateThickness(),
                                        FdtdLife::notifyMinorChange);
        auto newSliceThickness = c.test(sliceThickness_, d->sliceThickness(),
                                        FdtdLife::notifyMinorChange);
        auto newBitsPerHalfSide = c.test(bitsPerHalfSide_, d->bitsPerHalfSide(),
                                         FdtdLife::notifyMinorChange);
        auto newMinFeatureSize = c.test(minFeatureSize_, d->minFeatureSize(),
                                        FdtdLife::notifyMinorChange);
        auto newFingerOffset = c.test(fingerOffset_, d->fingerOffset(),
                                      FdtdLife::notifyMinorChange);
        auto newColumType = c.test(columnType_, d->columnType(),
                                      FdtdLife::notifyMinorChange);
        if(c.changeDetected()) {
            d->permittivityAbove(newPermittivityAbove);
            d->permittivityIn(newPermittivityInside);
            d->permittivityBelow(newPermittivityBelow);
            d->focusDistance(newFocalLength);
            d->layerSpacing(newLayerSpacing);
            d->centerFrequency(newCentreFrequency);
            d->unitCell(newUnitCell);
            d->lensDiameter(newLensDiameter);
            d->numLayers(newTotalNumberLayers);
            d->numArcStages(newNumberArStages);
            d->layersPerArcStage(newLayersPerArStage);
            d->edgeDistance(newEdgeDistance);
            d->modelling(newModelling);
            d->fdtdCellsPerUnitCell(newFdtdCellsPerUnitCell);
            d->focalLengthFudgeFactor(newFocalLengthFudgeFactor);
            d->plateThickness(newPlateThickness);
            d->sliceThickness(newSliceThickness);
            d->bitsPerHalfSide(newBitsPerHalfSide);
            d->minFeatureSize(newMinFeatureSize);
            d->fingerOffset(newFingerOffset);
            d->columnType(newColumType);
            // Tell others
            model_->doNotification(c.why());
        }
    }
}

// Generate a model from the designer
void LensDesignerPhaseDlg::onGenerate() {
    model_->designers().clearModel();
    auto* d = dynamic_cast<designer::LensDesigner*>(model_->designers().designer());
    d->generatePhaseShift();
    InitialisingGui::Set s(initialising_);
    errors_.value(d->error());
    model_->doNotification(FdtdLife::notifySequencerChange);
    model_->doNotification(FdtdLife::notifyDomainContentsChange);
    model_->doNotification(FdtdLife::notifyDomainSizeChange);
}
