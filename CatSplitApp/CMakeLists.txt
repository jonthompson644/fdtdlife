cmake_minimum_required(VERSION 3.8)
project(CatSplitApp)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmakemodules")
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(SOURCE_FILES main.cpp
        CatSplit.cpp CatSplit.h CatItem.cpp CatItem.h)

add_executable(CatSplitApp ${SOURCE_FILES})
target_link_libraries(CatSplitApp XmlLibrary)
target_link_libraries(CatSplitApp BoxLibrary)
