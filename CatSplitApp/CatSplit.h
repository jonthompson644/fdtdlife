/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#ifndef FDTDLIFE_CATSPLIT_H
#define FDTDLIFE_CATSPLIT_H

#include <string>
#include <list>
#include "CatItem.h"

// The main class for the splitter
class CatSplit {
public:
    // Construction
    CatSplit(const std::string& inputFileName);

protected:
    // Variables
    std::string inputFileName_;   // The name of the input file
    // Catalogue
    size_t bitsPerHalfSide_{};   // Number of pixels on half a side
    bool fourFoldSymmetry_{};  // Indicates if these are four fold symmetrical patterns
    double unitCell_{};  // The unit cell the data was recorded at
    double startFrequency_{};  // The first frequency point
    double frequencyStep_{};  // The sterps between frequencies
    bool inclCornerToCorner_{};  // Indicates if patterns with touching corners are included
    std::list<CatItem> items_;  // The catalogue items

protected:
    // Helpers
    bool readCatalogueFile(const std::string& fileName);
    void write(xml::DomObject& root) const;
    void read(xml::DomObject& root);
};


#endif //FDTDLIFE_FDTDMPI_H
