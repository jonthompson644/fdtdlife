/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/
#include "CatSplit.h"
#include <Xml/DomObject.h>
#include <Xml/Reader.h>
#include <Xml/Writer.h>
#include <Xml/DomDocument.h>
#include <Xml/Exception.h>
#include <iostream>

// Constructor
CatSplit::CatSplit(const std::string& fileName) {
    // Load the file
    if(readCatalogueFile(fileName)) {

    }
}

// Read the catalogue file
bool CatSplit::readCatalogueFile(const std::string& fileName) {
    bool result = false;
    inputFileName_ = fileName;
    // Parse the file into a DOM
    xml::Reader reader;
    xml::DomDocument dom("binarycatalogue");
    try {
        std::cout << "Parsing file into DOM" << std::endl;
        reader.readFile(&dom, inputFileName_);
        std::cout << "File parsed into DOM" << std::endl;
        //read(*dom.getObject());
        result = true;
    } catch(xml::Exception& e) {
        std::cout << "Failed to read config XML file: " << e.what() << std::endl;
    }
    return result;
}

// Read the catalogue from the DOM
void CatSplit::read(xml::DomObject& root) {
    items_.clear();
    root >> xml::Open();
    root >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
    root >> xml::Obj("fourfoldsymmetry") >> fourFoldSymmetry_;
    root >> xml::Obj("unitcell") >> unitCell_;
    root >> xml::Obj("startfrequency") >> startFrequency_;
    root >> xml::Obj("frequencystep") >> frequencyStep_;
    root >> xml::Obj("inclcornertocorner") >> inclCornerToCorner_;
    for(auto& o : root.obj("catalogueitem")) {
        items_.emplace_back();
        *o >> items_.back();
    }
    root >> xml::Close();
}

// Write the catalogue to the DOM
void CatSplit::write(xml::DomObject& root) const {

}

