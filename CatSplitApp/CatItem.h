/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CATITEM_H
#define FDTDLIFE_CATITEM_H

#include <Xml/DomObject.h>

// An item in the catalogue
class CatItem {
public:
    // Construction
    CatItem() = default;

    // API
    void write(xml::DomObject& root) const;
    void read(xml::DomObject& root);
    friend xml::DomObject& operator<<(xml::DomObject& o, const CatItem& v) {
        v.write(o);
        return o;
    }
    friend xml::DomObject& operator>>(xml::DomObject& o, CatItem& v) {
        v.read(o);
        return o;
    }

protected:
    // Members
    uint64_t code_{};  // The pattern's code
    uint64_t pattern_{};  // The pattern
    std::vector<double> transmittance_;
    std::vector<double> phaseShift_;
    std::vector<double> transmittanceY_;
    std::vector<double> phaseShiftY_;
};


#endif //FDTDLIFE_CATITEM_H
