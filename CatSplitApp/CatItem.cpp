/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <Xml/Exception.h>
#include "CatItem.h"

// Write the object to the DOM
void CatItem::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("code") << code_;
    root << xml::Obj("pattern") << pattern_;
    root << xml::Obj("transmittance") << transmittance_;
    root << xml::Obj("phaseshift") << phaseShift_;
    if(!transmittanceY_.empty()) {
        root << xml::Obj("transmittancey") << transmittanceY_;
        root << xml::Obj("phaseshifty") << phaseShiftY_;
    }
    root << xml::Close();
}

// Read the object from the DOM
void CatItem::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("code") >> xml::Default(0) >> code_;
    root >> xml::Obj("pattern") >> xml::Default(0) >> pattern_;
    root >> xml::Obj("transmittance") >> transmittance_;
    root >> xml::Obj("phaseshift") >> phaseShift_;
    try {
        root >> xml::Obj("transmittancey") >> transmittanceY_;
        root >> xml::Obj("phaseshifty") >> phaseShiftY_;
    } catch(xml::Exception&) {
    }
    root >> xml::Close();
}
