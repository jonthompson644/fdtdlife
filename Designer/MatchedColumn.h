/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_MATCHEDCOLUMN_H
#define FDTDLIFE_MATCHEDCOLUMN_H

#include <cstdlib>
#include <vector>
#include "CellConfig.h"
#include <Xml/DomObject.h>
#include <Box/Point2D.h>

namespace designer {

    class LensDesigner;

    // Information regarding a single column in a lens design
    class MatchedColumn {
    public:
        // Construction
        explicit MatchedColumn(size_t index);

        // Getters
        [[nodiscard]] size_t index() const { return index_; }
        [[nodiscard]] double wm() const { return wm_; }
        [[nodiscard]] double lm() const { return lm_; }
        [[nodiscard]] double phaseShift() const { return phaseShift_; }
        [[nodiscard]] double currentPhaseShift() const { return currentPhaseShift_; }
        [[nodiscard]] double mainNe() const { return mainNe_; }
        [[nodiscard]] double pathLength() const { return pathLength_; }
        [[nodiscard]] double errorN() const { return errorN_; }

        // API
        void calculateVerticalCol(LensDesigner* designer);
        void initialiseCurvedCol(double mainNe, size_t numLayers, double lm, double d);
        void calcCurvedColDimensions(double nBelow, double g, double d, double s,
                                     double b);
        void adjustCurvedColPhaseShifts(double nInside, double nBelow, double f,
                                        MatchedColumn* outerCol);
        void assignRefractiveIndices(double nAbove, double nBelow, double nInside,
                                     size_t numLayers, size_t layersPerArStage,
                                     size_t numArStages, MatchedColumn* outerCol);
        void assignPatches(LensDesigner* designer, MatchedColumn* lastCol);
        void calcCurrentPhaseShift(LensDesigner* designer);
        [[nodiscard]] size_t numLayers() const { return cells_.size(); }
        CellConfig& cell(size_t layer) { return cells_[layer]; }
        std::vector<CellConfig>& cells() { return cells_; }
        bool isResultValid();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const MatchedColumn& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, MatchedColumn& v) {
            v.readConfig(o);
            return o;
        }

    protected:
        // Members
        size_t index_;   // The column number (0 is the center)

        // Calculations
        double wm_{};  // The distance from the focus to the colum
        double lm_{};  // The distance of the column from the center
        double phaseShift_{};   // The total phase shift required in the column
        double mainNe_{};  // The effective refractive index of the main layers
        double currentPhaseShift_{};  // The current phase shift of the column
        double pathLength_{};  // The total path length of the column
        double errorN_{};  // The last error in the phase shift

        // The cell configuration
        std::vector<CellConfig> cells_;  // The configuration of the cells

        // Helpers
        void calcColumnInfo(LensDesigner* designer);
    };

}


#endif //FDTDLIFE_MATCHEDCOLUMN_H
