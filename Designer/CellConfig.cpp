/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include <cmath>
#include "CellConfig.h"

// Return true if the results are valid and not NAN
bool designer::CellConfig::isResultValid() const {
    return !std::isnan(refractiveIndex_) && !std::isnan(plateSize_)
           && !std::isnan(fdtdPlateSize_);
}

// Write the configuration to the XML object
void designer::CellConfig::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("refractiveindex") << refractiveIndex_;
    root << xml::Obj("platesize") << plateSize_;
    root << xml::Obj("fdtdplatesize") << fdtdPlateSize_;
    root << xml::Obj("dielectricmaterial") << dielectricMaterial_;
    root << xml::Obj("leftcuboid") << leftShape_;
    root << xml::Obj("rightcuboid") << rightShape_;
    root << xml::Obj("variable") << variable_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("incidenceangle") << incidenceAngle_;
    root << xml::Obj("pathlength") << pathLength_;
    root << xml::Close();
}

// Read the configuration from the XML object
void designer::CellConfig::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("refractiveindex") >> refractiveIndex_;
    root >> xml::Obj("platesize") >> plateSize_;
    root >> xml::Obj("fdtdplatesize") >> fdtdPlateSize_;
    root >> xml::Obj("dielectricmaterial") >> dielectricMaterial_;
    root >> xml::Obj("leftcuboid") >> leftShape_;
    root >> xml::Obj("rightcuboid") >> rightShape_;
    root >> xml::Obj("variable") >> variable_;
    root >> xml::Obj("unitcell") >> xml::Default(0.0) >> unitCell_;
    root >> xml::Obj("incidenceangle") >> xml::Default(0.0) >> incidenceAngle_;
    root >> xml::Obj("pathlength") >> xml::Default(0.0) >> pathLength_;
    root >> xml::Close();
}

// Set the left hand shape identifier at a particular y
void designer::CellConfig::leftShape(size_t y, int v) {
    leftShape_.resize(std::max(leftShape_.size(), y + 1U));
    leftShape_[y] = v;
}

// Set the right hand shape identifier at a particular y
void designer::CellConfig::rightShape(size_t y, int v) {
    rightShape_.resize(std::max(rightShape_.size(), y + 1U));
    rightShape_[y] = v;
}
