/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_PARAMETEREXTRACTOR_H
#define FDTDLIFE_PARAMETEREXTRACTOR_H

#include "Designer.h"
#include "HfssSParameter.h"

namespace fdtd { class Model; }

namespace designer {

    // A class that extracts complex permittivity and permeability
    // from HFSS S-parameter data.
    class ParameterExtractor : public Designer {
    public:
        // Construction
        ParameterExtractor() = default;
        void construct(int mode, int identifier, fdtd::Model* m) override;

        // Overrides of Designer
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;

        // API
        void pasteHfssData(const std::string& text);
        HfssSParameter* find(const std::string& header);
        std::list<designer::HfssSParameter>::iterator find(size_t n);
        HfssSParameter& restore();
        void calculate();
        double maxRefractiveIndex();
        void makeParameters();
        void deleteParameter(size_t n);

        // Iterators
        typedef std::list<HfssSParameter>::iterator iterator;
        iterator begin() { return parameters_.begin(); }
        iterator end() { return parameters_.end(); }

        // Getters
        [[nodiscard]] double layerSpacing() const { return layerSpacing_; }
        [[nodiscard]] size_t numLayers() const { return numLayers_; }
        [[nodiscard]] double vacuumGap() const { return vacuumGap_; }
        [[nodiscard]] double basePermittivity() const { return basePermittivity_; }

        // Setters
        void layerSpacing(double v) { layerSpacing_ = v; }
        void numLayers(size_t v) { numLayers_ = v; }
        void vacuumGap(double v) { vacuumGap_ = v; }
        void basePermittivity(double v) { basePermittivity_ = v; }

    protected:
        // Configuration
        double layerSpacing_{0.0001};  // Layer spacing
        size_t numLayers_{10};  // Number of layers
        double vacuumGap_{0.0005};  // The gap above and below the dielectric
        double basePermittivity_{1.0};  // The permittivity of the base dielectric

        // Calculated values
        std::list<HfssSParameter> parameters_;

        // Helpers
        static std::map<std::string, double> parseHeader(const std::string& text);

        // Types
        enum class PasteKind {
            s11Real, s12Real, s11Imag, s12Imag
        };


    };
}


#endif //FDTDLIFE_PARAMETEREXTRACTOR_H
