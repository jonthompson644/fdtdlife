/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensRayTracer.h"
#include "LensDesigner.h"
#include <Fdtd/Model.h>

// Second phase constructor
void designer::LensRayTracer::construct(designer::LensDesigner* designer) {
    designer_ = designer;
}

// Fill the model with initial rays at the focal point
void designer::LensRayTracer::calculateRays() {
    // Get the coordinates of the focal point
    Coord focalPoint;
    focalPoint.x_ = 0.0;
    focalPoint.z_ = designer_->numLayers() * designer_->layerSpacing() -
                    designer_->layerSpacing() / 2.0 + designer_->focusDistance();
    // Create the rays
    rays_.clear();
    double angle = 0.0;
    rays_.emplace_back(angle, focalPoint);
    angle += deltaAngle_;
    while(angle <= maxAngle_) {
        rays_.emplace_back(angle, focalPoint);
        rays_.emplace_back(-angle, focalPoint);
        angle += deltaAngle_;
    }
    // Set up global values
    columnsInDiameter_ = designer_->columnsInDiameter();
    unitCell_ = designer_->unitCell();
    x1_ = -designer_->lensDiameter() / 2.0;
    x2_ = +designer_->lensDiameter() / 2.0;
    zLens1_ = -designer_->layerSpacing() / 2.0;
    zLens2_ = zLens1_ + designer_->numLayers() * designer_->layerSpacing();
    z1_ = -designer_->edgeDistance();
    z2_ = zLens2_ + designer_->focusDistance();
    // Propagate the rays
    for(auto& ray : rays_) {
        propagateRay(ray);
    }
}

// Propagate the ray until it leaves the domain
void designer::LensRayTracer::propagateRay(designer::LensRayTracer::Ray& ray) {
    // Does the ray start in the domain?
    Coord pos = ray.path_.back();
    bool going = pos.x_ >= x1_ && pos.x_ <= x2_ && pos.z_ >= z1_ && pos.z_ <= z2_;
    while(going) {
        // Calculate the position of the next step
        double deltaRX = deltaR_ * std::sin(ray.angle_ * box::Constants::deg2rad_);
        double deltaRZ = deltaR_ * std::cos(ray.angle_ * box::Constants::deg2rad_);
        double deltaSX = deltaS_ * std::cos(ray.angle_ * box::Constants::deg2rad_);
        double deltaSZ = deltaS_ * std::sin(ray.angle_ * box::Constants::deg2rad_);
        Coord newPos(pos.x_ + deltaRX, pos.z_ - deltaRZ);
        Coord halfwayPos(pos.x_ + deltaRX / 2.0, pos.z_ - deltaRZ / 2.0);
        Coord n1Pos(halfwayPos.x_ - deltaSX, halfwayPos.z_ - deltaSZ);
        Coord n2Pos(halfwayPos.x_ + deltaSX, halfwayPos.z_ + deltaSZ);
        // Get the refractive indices at the two n positions
        double n1 = designer_->getRefractiveIndexAt(n1Pos.x_, n1Pos.z_);
        double n2 = designer_->getRefractiveIndexAt(n2Pos.x_, n2Pos.z_);
        // Calculate the rotation produced
        double deltaTheta = std::atan2(deltaR_ * (n2 - n1), 2.0 * deltaS_ * n2);
        // Update the ray position
        ray.path_.emplace_back(newPos);
        ray.angle_ += (deltaTheta / box::Constants::deg2rad_);
        // Is the ray still in the domain?
        pos = newPos;
        going = pos.x_ >= x1_ && pos.x_ <= x2_ && pos.z_ >= z1_ && pos.z_ <= z2_;
    }
}

// Write the configuration to the DOM object
void designer::LensRayTracer::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("deltaangle") << deltaAngle_;
    root << xml::Obj("maxangle") << maxAngle_;
    root << xml::Obj("deltar") << deltaR_;
    root << xml::Obj("deltas") << deltaS_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void designer::LensRayTracer::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("deltaangle") >> deltaAngle_;
    root >> xml::Obj("maxangle") >> xml::Default(30.0) >> maxAngle_;
    root >> xml::Obj("deltar") >> deltaR_;
    root >> xml::Obj("deltas") >> deltaS_;
    root >> xml::Close();
}
