/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "EquivalentPattern.h"

// Write to the XML DOM object
void designer::EquivalentPattern::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("platesize") << plateSize_;
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    root << xml::Obj("coding") << evenCoding_;
    root << xml::Obj("oddcoding") << oddCoding_;
    root << xml::Close();
}

// Read from the XML DOM object
void designer::EquivalentPattern::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("platesize") >> plateSize_;
    root >> xml::Obj("bitsperhalfside") >> bitsPerHalfSide_;
    root >> xml::Obj("coding") >> evenCoding_;
    root >> xml::Obj("oddcoding") >> xml::Default(std::vector<uint64_t>())
         >> oddCoding_;
    root >> xml::Close();
}

// Swap this and the other.  Required for sort
void designer::EquivalentPattern::swap(designer::EquivalentPattern& other) {
    std::swap(plateSize_, other.plateSize_);
    std::swap(bitsPerHalfSide_, other.bitsPerHalfSide_);
    std::swap(evenCoding_, other.evenCoding_);
    std::swap(oddCoding_, other.oddCoding_);
}

// Return the odd coding.  If it's empty, we return the even coding for both
const std::vector<uint64_t>& designer::EquivalentPattern::oddCoding() const {
    if(oddCoding_.empty()) {
        return evenCoding_;
    } else {
        return oddCoding_;
    }
}
