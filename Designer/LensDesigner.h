/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSDESIGNER_H
#define FDTDLIFE_LENSDESIGNER_H

#include "Designer.h"
#include <list>
#include <vector>
#include <Box/Vector.h>
#include <Box/Expression.h>
#include <Domain/CatalogueLayer.h>
#include "MatchedColumn.h"
#include "EquivalentPattern.h"
#include "LensRayTracer.h"

namespace box { class StlWriter; }
namespace gs { class GeneticSearch; }
namespace domain { class BinaryPlateNxN; }
namespace fdtd { class Variable; }

namespace designer {

    // A class that aids lens design
    class LensDesigner : public Designer {
    public:
        // Types
        enum class Modelling {
            metamaterialCells = 0, dielectricBlocks = 1, fingerPatterns = 3
        };
        enum class ColumnType {
            straight, curved, curvedConstG
        };
        enum class WhichParameter {
            refractiveIndices, unitCells, incidenceAngle, patchRatio, error
        };

        // Construction
        LensDesigner() = default;
        void construct(int mode, int identifier, fdtd::Model* m) override;

        // Overrides of Designer
        void writeConfig(xml::DomObject& root) const override;
        void readConfig(xml::DomObject& root) override;

        // API
        void updateRefractiveIndices();
        double findPatchRatio(double n, double incidenceAngle, double unitCell, double& error);
        [[nodiscard]] double fdtdSize(double s) const;
        void exportDiameterSliceAsStl(const std::string& fileName);
        void exportQuadrantLayerAsDxf(const std::string& fileName, size_t layer);
        void exportDiameterSliceAsHfss(const std::string& fileName);
        void exportQuadrantAsHfss(const std::string& fileName);
        void exportQuadrantLayerAsHfss(const std::string& fileName);
        void exportColumnAsHfss(const std::string& fileName);
        bool isResultValid();
        void generatePhaseShift();
        void generateColumnGa();
        void generateFullAdjustGa();
        void loadModel();
        double getRefractiveIndexAt(double x, double z);
        std::string getColumnsCsv(WhichParameter which);
        void putColumnsCsv(const std::string& csv);
        [[nodiscard]] bool hasEvenColumns() const { return (columnsInDiameter_ % 2) == 0; }

        // Getters
        [[nodiscard]] double permittivityAbove() const { return permittivityAbove_; }
        [[nodiscard]] double permittivityBelow() const { return permittivityBelow_; }
        [[nodiscard]] double permittivityIn() const { return permittivityIn_; }
        [[nodiscard]] double focusDistance() const { return focusDistance_; }
        [[nodiscard]] double centerFrequency() const { return centerFrequency_; }
        [[nodiscard]] double unitCell() const { return unitCell_; }
        [[nodiscard]] double lensDiameter() const { return lensDiameter_; }
        [[nodiscard]] double layerSpacing() const { return layerSpacing_; }
        [[nodiscard]] size_t numLayers() const { return numLayers_; }
        [[nodiscard]] size_t layersPerArcStage() const { return layersPerArcStage_; }
        [[nodiscard]] size_t fdtdCellsPerUnitCell() const { return fdtdCellsPerUnitCell_; }
        std::list<MatchedColumn>& columns() { return columns_; }
        [[nodiscard]] size_t numColumns() const { return columns_.size(); }
        [[nodiscard]] size_t columnsInDiameter() const { return columnsInDiameter_; }
        [[nodiscard]] double wavelengthBelow() const { return wavelengthBelow_; }
        [[nodiscard]] double tau() const { return tau_; }
        [[nodiscard]] double edgeDistance() const { return edgeDistance_; }
        [[nodiscard]] Modelling modelling() const { return modelling_; }
        [[nodiscard]] double focalLengthFudgeFactor() const { return focalLengthFudgeFactor_; }
        [[nodiscard]] double plateThickness() const { return plateThickness_; }
        [[nodiscard]] bool exportSymmetricalHalf() const { return exportSymmetricalHalf_; }
        [[nodiscard]] const std::string& error() const { return error_; }
        [[nodiscard]] size_t columnNumber() const { return columnNumber_; }
        [[nodiscard]] size_t numArcStages() const { return numArcStages_; }
        [[nodiscard]] double geneticRange() const { return geneticRange_; }
        [[nodiscard]] size_t geneticBits() const { return geneticBits_; }
        [[nodiscard]] size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
        [[nodiscard]] size_t sliceThickness() const { return sliceThickness_; }
        [[nodiscard]] size_t exportLayer() const { return exportLayer_; }
        [[nodiscard]] double minFeatureSize() const { return minFeatureSize_; }
        [[nodiscard]] double fingerOffset() const { return fingerOffset_; }
        [[nodiscard]] ColumnType columnType() const { return columnType_; }
        [[nodiscard]] double lensStructureSize() const { return lensStructureSize_; }
        LensRayTracer& rayTracer() { return rayTracer_; }

        // Setters
        void permittivityAbove(double v) { permittivityAbove_ = v; }
        void permittivityBelow(double v) { permittivityBelow_ = v; }
        void permittivityIn(double v) { permittivityIn_ = v; }
        void focusDistance(double v) { focusDistance_ = v; }
        void centerFrequency(double v) { centerFrequency_ = v; }
        void unitCell(double v) { unitCell_ = v; }
        void lensDiameter(double v) { lensDiameter_ = v; }
        void layerSpacing(double v) { layerSpacing_ = v; }
        void numLayers(size_t v) { numLayers_ = v; }
        void layersPerArcStage(size_t v) { layersPerArcStage_ = v; }
        void fdtdCellsPerUnitCell(size_t v) { fdtdCellsPerUnitCell_ = v; }
        void edgeDistance(double v) { edgeDistance_ = v; }
        void modelling(Modelling v) { modelling_ = v; }
        void focalLengthFudgeFactor(double v) { focalLengthFudgeFactor_ = v; }
        void plateThickness(double v) { plateThickness_ = v; }
        void exportSymmetricalHalf(bool v) { exportSymmetricalHalf_ = v; }
        void error(const std::string& v) { error_ = v; }
        void columnNumber(size_t v) { columnNumber_ = v; }
        void numArcStages(size_t v) { numArcStages_ = v; }
        void geneticRange(double v) { geneticRange_ = v; }
        void geneticBits(size_t v) { geneticBits_ = v; }
        void bitsPerHalfSide(size_t v) { bitsPerHalfSide_ = v; }
        void sliceThickness(size_t v) { sliceThickness_ = v; }
        void exportLayer(size_t v) { exportLayer_ = v; }
        void minFeatureSize(double v) { minFeatureSize_ = v; }
        void fingerOffset(double v) { fingerOffset_ = v; }
        void columnType(ColumnType v) { columnType_ = v; }

    protected:
        // Configuration
        double permittivityBelow_{2.2};  // Relative permittivity below the lens (to the focus)
        double permittivityAbove_{1.0};  // Relative permittivity above the lens
        double permittivityIn_{
                2.2};  // Relative permittivity inside (when using metamaterials)
        double focusDistance_{0.002};  // Distance from the back of the lens to the focus point
        double centerFrequency_{100e9};  // Center operating frequency
        double unitCell_{50e-6};  // Unit cell of the columns
        double lensDiameter_{6.3e-3};  // The diameter of the lens
        double layerSpacing_{100e-6};  // The spacing between layers
        double edgeDistance_{0.0002}; // Distance from the lens to the edge of the model
        Modelling modelling_{Modelling::metamaterialCells};  // Modelling mode
        size_t numLayers_{14};  // Total number of layers
        size_t numArcStages_{1};  // Number of ARC stages
        size_t layersPerArcStage_{2};  // The number of layers in each ARC stage
        size_t fdtdCellsPerUnitCell_{40};  // The number of FDTD cells for each unit cell
        double focalLengthFudgeFactor_{1.0};  // A fudge factor for the focal length
        double plateThickness_{0.000001};  // The thickness of the metal plates
        bool exportSymmetricalHalf_{true};  // Only export columns for a symmetrical half
        size_t columnNumber_{0};  // Which column to generate the GA for
        double geneticRange_{10.0};  // The range over which to search (in %)
        size_t geneticBits_{4};  // The number of bits to represent the search range
        size_t bitsPerHalfSide_{10};  // The number of pixels in a half side of an NxN plate
        size_t sliceThickness_{1};  // The number of unit cells in the modelled slice
        double minFeatureSize_{10e-6}; // The minimum feature size
        double fingerOffset_{0.0};  // Finger offset
        size_t exportLayer_{0};  // Which layer to export
        ColumnType columnType_{ColumnType::straight};  // Used curved columns
        LensRayTracer rayTracer_;

        // Calculated values
        size_t columnsInDiameter_{};  // Number of columns required for the diameter
        std::list<MatchedColumn> columns_;  // The lens design columns
        double wavelengthBelow_{};  // The wavelength of light below the lens
        double tau_{};  // Ratio of main layers thickness to anti-reflection layers thickness
        double lensStructureSize_{};  // The thickness of the lens structure
        int metalMaterialIndex_{};  // The index of the metal material
        int belowMaterialIndex_{};  // The index of the material below the lens
        int inMaterialIndex_{};  // The index of the material inside (for metamaterials)
        double dr_{};  // The FDTD cell size
        std::string error_; // The last error reported

        // Helpers
        domain::Shape* makeSquare(const box::Vector<double>& pos, MatchedColumn& column,
                        size_t layer, bool left, size_t y, size_t numDuplicates);
        bool doGlobalCalculations();
        gs::GeneticSearch* loadGeneticSearch();
        void loadLensReflectionTarget(gs::GeneticSearch* s) const;
        void loadLensTransmissionTarget(gs::GeneticSearch* s) const;
        void loadLensPhaseTarget(gs::GeneticSearch* s) const;
        fdtd::Variable* makeGaVariable(const std::string& name,
                                       double value,
                                       gs::GeneticSearch* s);
        void makeVariable(size_t numLayers, size_t& topLayer, size_t& bottomLayer,
                          std::vector<fdtd::Variable*>& vars, MatchedColumn& col,
                          const std::string& name, gs::GeneticSearch* s);
        void connectVariables(std::vector<fdtd::Variable*>& vars, MatchedColumn& col);
        void exportDiameterSlice(box::ExportWriter& writer);
        void exportColumn(box::ExportWriter& writer);
        void exportQuadrantLayer(box::ExportWriter& writer, size_t layer);
        MatchedColumn& column(size_t colNum);
        void loadModelConfig();
        void loadStraightColumns(std::list<domain::Shape*>& shapes);
        void loadCurvedColumns(std::list<domain::Shape*>& shapes, bool slice);
        domain::Shape* makeCurvedSquare(MatchedColumn& column, size_t layer, double x, bool slice);
        void generateCurvedCols();
    };
}


#endif //FDTDLIFE_LENSDESIGNER_H
