/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "HfssSParameter.h"
#include "ParameterExtractor.h"
#include <Box/Utility.h>
#include <Box/Constants.h>

// Set the S11 value of the point with the given frequency
void designer::HfssSParameter::s11(double frequency, double value, bool imaginary) {
    Item* point = find(frequency);
    if(point) {
        if(imaginary) {
            point->s11_.imag(value);
        } else {
            point->s11_.real(value);
        }
    }
}

// Set the S12 value of the point with the given frequency
void designer::HfssSParameter::s12(double frequency, double value, bool imaginary) {
    Item* point = find(frequency);
    if(point) {
        if(imaginary) {
            point->s12_.imag(value);
        } else {
            point->s12_.real(value);
        }
    }
}

// Find or create a point with the given frequency
designer::HfssSParameter::Item* designer::HfssSParameter::find(double frequency) {
    Item* result = nullptr;
    for(auto& point : points_) {
        if(box::Utility::equals(point.frequency_, frequency)) {
            result = &point;
            break;
        }
    }
    if(result == nullptr) {
        points_.emplace_back(frequency);
        result = &points_.back();
    }
    return result;
}

// Perform the parameter extraction calculations
void designer::HfssSParameter::calculate(designer::ParameterExtractor* designer) {
    Item* lastP = nullptr;
    double offset = 0.0;
    double thickness = designer->layerSpacing() * designer->numLayers() /
                       std::cos(incidenceAngle_ * box::Constants::deg2rad_);
    for(auto& p : points_) {
        // Initial calculations up to the getting the natural log
        double omega = 2.0 * box::Constants::pi_ * p.frequency_ * 1e9;
        std::complex<double> z0Squared = std::exp(std::complex<double>(
                0,
                -2.0 * omega * designer->vacuumGap()
                / box::Constants::c_));
        std::complex<double> v1 = (p.s12_ + p.s11_) / z0Squared;
        std::complex<double> v2 = (p.s12_ - p.s11_) / z0Squared;
        std::complex<double> x = (1.0 - v1 * v2) / (v1 - v2);
        std::complex<double> sqrtXSquaredMinus1 = std::sqrt(x * x - 1.0);
        std::complex<double> gammaA = x + sqrtXSquaredMinus1;
        std::complex<double> gammaB = x - sqrtXSquaredMinus1;
        double modGammaA = std::abs(gammaA);
        std::complex<double> gamma = modGammaA <= 1.0 ? gammaA : gammaB;
        std::complex<double> z = (v1 - gamma) / (1.0 - v1 * gamma);
        p.lnZ_ = std::log(z);
        // Fix up the imaginary part of the natural log to fix the branching problem
        if(lastP != nullptr) {
            double im = p.lnZ_.imag() - offset;
            double diff = lastP->lnZ_.imag() - im;
            if(diff < 0) {
                offset += 2.0 * box::Constants::pi_;
                im -= 2.0 * box::Constants::pi_;
            }
            p.lnZ_.imag(im);
        }
        // Final calculations to get the parameters
        std::complex<double> sqrtC1 = (1.0 + gamma) / (gamma - 1.0);
        std::complex<double> sqrtC2 = std::complex<double>(
                0,box::Constants::c_ / omega / thickness) * p.lnZ_;
        std::complex<double> c1 = sqrtC1 * sqrtC1;
        std::complex<double> c2 = sqrtC2 * sqrtC2;
        p.permittivity_ = std::sqrt(c2 / c1);
        p.permeability_ = std::sqrt(c1 * c2);
        p.refractiveIndex_ = std::sqrt(c2);
        // Remember the last point
        lastP = &p;
    }
}

// Read the S parameter from the DOM
void designer::HfssSParameter::read(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("header") >> header_;
    root >> xml::Obj("unitcell") >> unitCell_;
    root >> xml::Obj("patchratio") >> patchRatio_;
    root >> xml::Obj("incidenceangle") >> incidenceAngle_;
    for(auto& o : root.obj("point")) {
        *o >> restore();
    }
    root >> xml::Close();
}

// Write the S parameter to the DOM
void designer::HfssSParameter::write(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("header") << header_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("patchratio") << patchRatio_;
    root << xml::Obj("incidenceangle") << incidenceAngle_;
    for(auto& p : points_) {
        root << xml::Obj("point") << p;
    }
    root << xml::Close();
}

// Restore a point from the DOM
designer::HfssSParameter::Item& designer::HfssSParameter::restore() {
    points_.emplace_back(0.0);
    return points_.back();
}

// Return the maximum refractive index
double designer::HfssSParameter::maxRefractiveIndex() {
    double result = 0.0;
    for(auto& p : points_) {
        result = std::max(result, p.refractiveIndex_.real());
    }
    return result;
}
