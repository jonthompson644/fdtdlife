/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_EQUIVALENTPATTERN_H
#define FDTDLIFE_EQUIVALENTPATTERN_H

#include <vector>
#include <Xml/DomObject.h>

namespace designer {

    // Contains equivalent plate size information for
    // binary coded patterns
    class EquivalentPattern {
    public:
        // Construction
        EquivalentPattern() = default;
        EquivalentPattern(const EquivalentPattern& other) = default;

        // Getters
        double plateSize() const { return plateSize_; }
        size_t bitsPerHalfSide() const { return bitsPerHalfSide_; }
        const std::vector<uint64_t>& evenCoding() const { return evenCoding_; }
        const std::vector<uint64_t>& oddCoding() const;

        // Setters
        void plateSize(double v) { plateSize_ = v; }
        void bitsPerHalfSide(size_t v) { bitsPerHalfSide_ = v; }
        void evenCoding(const std::vector<uint64_t>& v) { evenCoding_ = v; }
        void oddCoding(const std::vector<uint64_t>& v) { oddCoding_ = v; }

        // API
        EquivalentPattern& operator=(const EquivalentPattern& other) = default;
        void swap(EquivalentPattern& other);
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const EquivalentPattern& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, EquivalentPattern& v) {
            v.readConfig(o);
            return o;
        }

    protected:
        // Members
        double plateSize_{};  // The equivalent plate size in %
        size_t bitsPerHalfSide_{};  // The number of bits in half a side
        std::vector<uint64_t> evenCoding_;  // The binary encoding for even numbered cells
        std::vector<uint64_t> oddCoding_;  // The binary encoding for odd numbered cells
    };
}


#endif //FDTDLIFE_EQUIVALENTPATTERN_H
