/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DESIGNERS_H
#define FDTDLIFE_DESIGNERS_H

#include <list>
#include <Box/Factory.h>
#include "Designer.h"

namespace fdtd { class Model; }
namespace fdtd { class Variable; }
namespace domain { class Material; }
namespace domain { class Cuboid; }
namespace domain { class CuboidArray; }
namespace domain { class BinaryPlateNxN; }
namespace domain { class FingerPlate; }
namespace domain { class CatalogueLayer; }
namespace domain { class Shape; }
namespace gs { class GeneticSearch; }

namespace designer {

    // The container for all the designers
    class Designers {
    public:
        // Construction
        explicit Designers(fdtd::Model* m);

        // API
        enum {modeLensDesigner, modeParameterExtractor};
        Designer* restore(xml::DomObject& o);
        Designer* make(int mode);
        void remove();
        Designer* designer() {return designer_.get();}
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Designers& s) {
            s.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Designers& s) {
            s.readConfig(o);
            return o;
        }
        void clearModel();
        domain::Material* makeNormalMaterial();
        domain::Cuboid* makeCuboid();
        domain::CuboidArray* makeCuboidArray();
        domain::FingerPlate* makeFingerPlate();
        domain::BinaryPlateNxN* makeBinaryPlate();
        domain::CatalogueLayer* makeCatalogueLayer();
        fdtd::Variable* makeVariable(const std::string& name, const std::string& value);
        gs::GeneticSearch* makeGeneticSearch();
        box::Factory<Designer>& factory() {return factory_;}
        void clear();
        void removeShapes(const std::list<domain::Shape*>& shapes);

    protected:
        // Members
        std::unique_ptr<Designer> designer_;  // The current designer
        box::Factory<Designer> factory_;  // The factory that creates designers
        fdtd::Model* m_;  // The model
        std::list<int> myMaterials_;  // Materials created by a designer
        std::list<int> myShapes_;  // Shapes created by a designer
        std::list<int> myVariables_;  // Variables created by the designer
        std::list<int> mySequencers_;  // Sequencers created by the designer
    };
}


#endif //FDTDLIFE_DESIGNERS_H
