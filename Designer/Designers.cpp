/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "Designers.h"
#include "Designer.h"
#include <Fdtd/Model.h>
#include <Domain/Domain.h>
#include <Domain/Material.h>
#include <Domain/Cuboid.h>
#include <Domain/CuboidArray.h>
#include <Domain/BinaryPlateNxN.h>
#include <Domain/FingerPlate.h>
#include "LensDesigner.h"
#include "ParameterExtractor.h"
#include <Gs/GeneticSearch.h>
#include <Domain/CatalogueLayer.h>

// Constructor
designer::Designers::Designers(fdtd::Model* m) :
        m_(m) {
    factory_.define(new box::Factory<Designer>::Creator<LensDesigner>(
            modeLensDesigner, "Lens Designer"));
    factory_.define(new box::Factory<Designer>::Creator<ParameterExtractor>(
            modeParameterExtractor, "Parameter Extractor"));
}

// Make a designer given a mode
designer::Designer* designer::Designers::make(int mode) {
    designer_.reset(factory_.make(mode, m_));
    return designer_.get();
}

// Remove the current designer
void designer::Designers::remove() {
    designer_.reset();
}

// Make a designer from a DOM object
designer::Designer* designer::Designers::restore(xml::DomObject& o) {
    designer_.reset(factory_.restore(o, m_));
    return designer_.get();
}

// Write the configuration to the DOM
void designer::Designers::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    if(designer_) {
        root << xml::Obj("designer") << *designer_;
    }
    root << xml::Obj("mymaterials") << myMaterials_;
    root << xml::Obj("myshapes") << myShapes_;
    root << xml::Obj("myvariables") << myVariables_;
    root << xml::Obj("mysequencers") << mySequencers_;
    root << xml::Close();
}

// Read the configuration from the DOM
void designer::Designers::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    designer_.reset(nullptr);
    for(auto& o : root.obj("designer")) {
        auto* d = restore(*o);
        *o >> *d;
    }
    root >> xml::Obj("mymaterials") >> xml::Default(std::list<int>())
         >> myMaterials_;
    root >> xml::Obj("myshapes") >> xml::Default(std::list<int>())
         >> myShapes_;
    root >> xml::Obj("myvariables") >> xml::Default(std::list<int>())
         >> myVariables_;
    root >> xml::Obj("mysequencers") >> xml::Default(std::list<int>())
         >> mySequencers_;
    root >> xml::Close();
}

// Remove all the model objects created by the designer
void designer::Designers::clearModel() {
    // Remove any shapes
    for(auto id : myShapes_) {
        auto* shape = m_->d()->getShape(id);
        m_->d()->remove(shape);
    }
    // Remove any materials
    for(auto id : myMaterials_) {
        auto* mat = m_->d()->getMaterial(id);
        if(mat != nullptr) {
            m_->d()->remove(mat);
        }
    }
    // Remove any variables
    for(auto id : myVariables_) {
        auto* var = m_->variables().find(id);
        if(var != nullptr) {
            m_->variables().remove(var);
        }
    }
    // Remove any sequencers
    for(auto id : mySequencers_) {
        auto* seq = m_->sequencers().find(id);
        if(seq != nullptr) {
            m_->sequencers().remove(seq);
        }
    }
    // Remove sequencer items too
    m_->clearSeqGenerated();
}

// Remove the list of shapes from the designer and the model
void designer::Designers::removeShapes(const std::list<domain::Shape*>& shapes) {
    for(auto& shape : shapes) {
        if(shape != nullptr) {
            myShapes_.remove(shape->identifier());
            m_->d()->remove(shape);
        }
    }
}

// Make a material in the model for a designer
domain::Material* designer::Designers::makeNormalMaterial() {
    auto* result = m_->d()->makeMaterial(domain::Domain::materialModeNormal).get();
    myMaterials_.push_back(result->index());
    return result;
}

// Make a cuboid shape in the model for a designer
domain::Cuboid* designer::Designers::makeCuboid() {
    auto* result = m_->d()->makeShape(domain::Domain::shapeModeCuboid);
    myShapes_.push_back(result->identifier());
    return dynamic_cast<domain::Cuboid*>(result);
}

// Make a cuboid array shape in the model for a designer
domain::CuboidArray* designer::Designers::makeCuboidArray() {
    auto* result = m_->d()->makeShape(domain::Domain::shapeModeCuboidArray);
    myShapes_.push_back(result->identifier());
    return dynamic_cast<domain::CuboidArray*>(result);
}

// Make a finger plate in the model for a designer
domain::FingerPlate* designer::Designers::makeFingerPlate() {
    auto* result = m_->d()->makeShape(domain::Domain::shapeModeFingerPlate);
    myShapes_.push_back(result->identifier());
    return dynamic_cast<domain::FingerPlate*>(result);
}

// Make a binary NxN plate shape in the model for a designer
domain::BinaryPlateNxN* designer::Designers::makeBinaryPlate() {
    auto* result = m_->d()->makeShape(domain::Domain::shapeModeBinaryPlateNxN);
    myShapes_.push_back(result->identifier());
    return dynamic_cast<domain::BinaryPlateNxN*>(result);
}

// Make a catalogue layer shape in the model for a designer
domain::CatalogueLayer* designer::Designers::makeCatalogueLayer() {
    auto* result = m_->d()->makeShape(domain::Domain::shapeModeCatalogueLayer);
    myShapes_.push_back(result->identifier());
    return dynamic_cast<domain::CatalogueLayer*>(result);
}

// Make a variable in the model for a designer
fdtd::Variable* designer::Designers::makeVariable(const std::string& name,
                                                  const std::string& value) {
    auto* result = &m_->variables().add(name, value);
    myVariables_.push_back(result->identifier());
    return result;
}

// Create a genetic search sequencer
gs::GeneticSearch* designer::Designers::makeGeneticSearch() {
    gs::GeneticSearch* result = dynamic_cast<gs::GeneticSearch*>(
            m_->sequencers().makeSequencer(seq::Sequencers::modeGeneticSearch).get());
    mySequencers_.push_back(result->identifier());
    return result;
}

// Clear the designers
void designer::Designers::clear() {
    designer_.reset(nullptr);
    myMaterials_.clear();
    myShapes_.clear();
    myVariables_.clear();
    mySequencers_.clear();
    factory_.clear();
}



