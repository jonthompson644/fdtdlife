/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_DESIGNER_H
#define FDTDLIFE_DESIGNER_H

#include <Xml/DomObject.h>

namespace fdtd {class Model;}

namespace designer {

    // Base class for all designers
    class Designer {
    public:
        // Construction
        Designer() = default;
        virtual void construct(int mode, int identifier, fdtd::Model* m);
        virtual ~Designer() = default;

        // API
        virtual void writeConfig(xml::DomObject& root) const;
        virtual void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const Designer& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, Designer& v) {
            v.readConfig(o);
            return o;
        }

        // Getters
        [[nodiscard]] int identifier() const {return identifier_;}
        [[nodiscard]] int mode() const {return mode_;}
        [[nodiscard]] const std::string& name() const {return name_;}
        fdtd::Model* m() {return m_;}

        // Setters
        void name(const std::string& v) {name_ = v;}

    protected:
        // Members
        fdtd::Model* m_ {};  // The designers class
        int identifier_ {-1};  // The unique identifier
        int mode_ {};  // The type of designer
        std::string name_;  // The name of the designer
    };
}


#endif //FDTDLIFE_DESIGNER_H
