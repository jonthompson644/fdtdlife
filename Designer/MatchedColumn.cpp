/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "MatchedColumn.h"
#include "LensDesigner.h"
#include <Fdtd/Model.h>
#include <Box/Constants.h>
#include <Domain/Material.h>
#include <cmath>
#include <Box/Utility.h>

// Constructor
designer::MatchedColumn::MatchedColumn(size_t index) :
        index_(index) {
}

// Initialise the column for the curved column algorithm
void designer::MatchedColumn::initialiseCurvedCol(double mainNe, size_t numLayers,
                                                  double lm, double d) {
    mainNe_ = mainNe;
    lm_ = lm;
    wm_ = std::sqrt(lm_ * lm_ + d * d);
    cells_.resize(numLayers);
    for(auto& cell : cells_) {
        cell.refractiveIndex(mainNe);
    }
}

// Perform the column calculations using the desired phase shifts
// and the original vertical columns method
void designer::MatchedColumn::calculateVerticalCol(LensDesigner* designer) {
    cells_.resize(designer->numLayers());
    // The focal length we are to use
    double f = designer->focusDistance() * designer->focalLengthFudgeFactor();
    // The distance over which the wave travels to meet the start of the column
    lm_ = index_ * designer->unitCell();
    if(designer->hasEvenColumns()) {
        // For an even number of columns we offset by half a column
        lm_ += designer->unitCell() / 2.0;
    }
    wm_ = std::sqrt(lm_ * lm_ + f * f);
    // The distance to the last column
    double cMax = static_cast<double>((designer->numColumns() - 1)) * designer->unitCell();
    if((designer->columnsInDiameter() % 2) == 0) {
        // For an even number of columns we offset by half a column
        cMax += designer->unitCell() / 2.0;
    }
    double aMax = std::sqrt(cMax * cMax + f * f);
    // The phase over these distances and thus the phase shift required by this column
    double phase = (wm_ / designer->wavelengthBelow()) * 2.0 * box::Constants::pi_;
    double phaseMax = (aMax / designer->wavelengthBelow()) * 2.0 * box::Constants::pi_;
    phaseShift_ = phaseMax - phase;
    // The initial main refractive index
    double neInside = std::sqrt(designer->permittivityIn());
    mainNe_ = neInside;
    double error = 0.0;
    for(size_t i = 0; i < 10; i++) {
        mainNe_ -= error;
        // The column information
        calcColumnInfo(designer);
        // The total path length
        double pathLength = 0.0;
        for(auto& c : cells_) {
            pathLength += c.pathLength();
        }
        // Fill the ARC layers
        assignRefractiveIndices(std::sqrt(designer->permittivityAbove()),
                                std::sqrt(designer->permittivityBelow()),
                                std::sqrt(designer->permittivityIn()),
                                designer->numLayers(), designer->layersPerArcStage(),
                                designer->numArcStages(), nullptr);
        // Calculate the main refractive index error
        calcCurrentPhaseShift(designer);
        error = (currentPhaseShift_ - phaseShift_) * box::Constants::c_
                / 2.0 / box::Constants::pi_ / designer->centerFrequency()
                / pathLength;
        assignPatches(designer, nullptr);
    }
}

// Calculate various column parameters we need before the refractive index determination
void designer::MatchedColumn::calcColumnInfo(LensDesigner* designer) {
    double insideN = std::sqrt(designer->permittivityIn());
    // The distance of the center of the column from the lens center
    double l = index_ * designer->unitCell();
    if(designer->hasEvenColumns()) {
        l += designer->unitCell() / 2.0;
    }
    // The focal length
    double f = designer->focusDistance() * designer->focalLengthFudgeFactor();
    // The incidence angle (from the normal)
    double centerTheta = std::atan2(l, f);
    centerTheta = std::asin(insideN / mainNe_ * std::sin(centerTheta));
    // The information for non-curved columns
    for(auto& cell : cells_) {
        cell.unitCell(designer->unitCell());
        cell.incidenceAngle(0.0);
        cell.pathLength(designer->layerSpacing());
    }
    // Now modify for curved columns
    if(designer->columnType() == LensDesigner::ColumnType::curved ||
       designer->columnType() == LensDesigner::ColumnType::curvedConstG) {
        // The center circle
        double centerRadius = 0.0;
        double centerE = 0.0;
        if(!box::Utility::equals(centerTheta, 0.0)) {
            centerRadius = designer->lensStructureSize() / std::sin(centerTheta);
            centerE = centerRadius * std::cos(centerTheta);
        }
        // The outer circle
        double outerTheta = std::atan2(l + designer->unitCell() / 2.0, f);
        outerTheta = std::asin(insideN / mainNe_ * std::sin(outerTheta));
        double outerRadius = designer->lensStructureSize() / std::sin(outerTheta);
        double outerE = outerRadius * std::cos(outerTheta);
        // The inner circle
        double innerTheta = std::atan2(l - designer->unitCell() / 2.0, f);
        innerTheta = std::asin(insideN / mainNe_ * std::sin(innerTheta));
        double innerRadius = 0.0;
        double innerE = 0.0;
        if(!box::Utility::equals(innerTheta, 0.0)) {
            innerRadius = designer->lensStructureSize() / std::sin(innerTheta);
            innerE = innerRadius * std::cos(innerTheta);
        }
        // Now for each layer
        for(size_t layer = 0; layer < cells_.size(); ++layer) {
            auto* cell = &cells_[layer];
            double yn = f + layer * designer->layerSpacing() + designer->layerSpacing();
            double part = yn - f - designer->lensStructureSize();
            // The outer circle
            double outerX = outerE - designer->unitCell() / 2.0 - l -
                            std::sqrt(outerRadius * outerRadius - part * part);
            // The inner circle
            double innerX = 0.0;
            if(!box::Utility::equals(innerTheta, 0.0)) {
                if(innerRadius < 0) {
                    innerX = innerE + designer->unitCell() / 2.0 - l +
                             std::sqrt(innerRadius * innerRadius - part * part);
                } else {
                    innerX = innerE + designer->unitCell() / 2.0 - l -
                             std::sqrt(innerRadius * innerRadius - part * part);
                }
            }
            // The unit cell
            double unitCell = innerX - outerX;
            cell->unitCell(unitCell);
            // The incidence angle
            double part1 = layer * designer->layerSpacing() -
                           designer->lensStructureSize();
            double part2 = layer * designer->layerSpacing() + designer->layerSpacing() -
                           designer->lensStructureSize();
            double centerX1 = 0.0;
            double centerX2 = 0.0;
            if(!box::Utility::equals(centerTheta, 0.0)) {
                centerX1 = centerE - l -
                           std::sqrt(centerRadius * centerRadius - part1 * part1);
                centerX2 = centerE - l -
                           std::sqrt(centerRadius * centerRadius - part2 * part2);
            }
            double deltaCenterX = centerX1 - centerX2;
            double phi = std::atan2(deltaCenterX, designer->layerSpacing());
            cell->incidenceAngle(phi);
            // The path length
            double pathLength = designer->layerSpacing();
            if(!box::Utility::equals(centerTheta, 0.0)) {
                double hyp = std::sqrt(designer->layerSpacing() * designer->layerSpacing() +
                                       deltaCenterX * deltaCenterX);
                double psi = std::asin(hyp / 2.0 / centerRadius);
                pathLength = 2.0 * centerRadius * psi;
            }
            cell->pathLength(pathLength);
        }
    }
}

// Returns true if the results are valid and none of the refractive
// indices are NAN
bool designer::MatchedColumn::isResultValid() {
    bool result = true;
    for(auto& cell : cells_) {
        result = result && cell.isResultValid();
    }
    return result;
}

// Write the configuration to the XML object
void designer::MatchedColumn::writeConfig(xml::DomObject& root) const {
    root << xml::Open();
    root << xml::Obj("a") << wm_;
    root << xml::Obj("c") << lm_;
    root << xml::Obj("phaseshift") << phaseShift_;
    root << xml::Obj("currentphaseshift") << currentPhaseShift_;
    root << xml::Obj("mainne") << mainNe_;
    for(auto& cell : cells_) {
        root << xml::Obj("cell") << cell;
    }
    root << xml::Close();
}

// Read the configuration from the XML object
void designer::MatchedColumn::readConfig(xml::DomObject& root) {
    root >> xml::Open();
    root >> xml::Obj("a") >> wm_;
    root >> xml::Obj("c") >> lm_;
    root >> xml::Obj("phaseshift") >> phaseShift_;
    root >> xml::Obj("currentphaseshift") >> xml::Default(0.0) >> currentPhaseShift_;
    root >> xml::Obj("mainne") >> xml::Default(mainNe_) >> mainNe_;
    cells_.clear();
    for(auto& o : root.obj("cell")) {
        cells_.emplace_back();
        *o >> cells_.back();
    }
    root >> xml::Close();
}

// Assign the main and anti-reflection refractive indices to the layers
void designer::MatchedColumn::assignRefractiveIndices(
        double nAbove, double nBelow, double nInside, size_t numLayers,
        size_t layersPerArStage, size_t numArStages, MatchedColumn* outerCol) {
    // Fill the ARC layers
    size_t firstLayer = 0;
    size_t lastLayer = numLayers - 1;
    double neFirst;
    double neLast;
    if(layersPerArStage * numArStages < numLayers &&
       (outerCol == nullptr || index_ <= outerCol->index())) {
        switch(numArStages) {
            case 1:
                neFirst = std::max(nInside, std::sqrt(nAbove * mainNe_));
                neLast = std::max(nInside, std::sqrt(nBelow * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                break;
            case 2:
                neFirst = std::max(nInside, std::cbrt(nAbove * nAbove * mainNe_));
                neLast = std::max(nInside, std::cbrt(nBelow * nBelow * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside, std::cbrt(nAbove * mainNe_ * mainNe_));
                neLast = std::max(nInside, std::cbrt(nBelow * mainNe_ * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                break;
            case 3:
                neFirst = std::max(nInside,
                                   std::pow(nAbove * nAbove * nAbove * mainNe_, 0.25));
                neLast = std::max(nInside,
                                  std::pow(nBelow * nBelow * nBelow * mainNe_, 0.25));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside, std::sqrt(nAbove * mainNe_));
                neLast = std::max(nInside, std::sqrt(nBelow * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside,
                                   std::pow(nAbove * mainNe_ * mainNe_ * mainNe_, 0.25));
                neLast = std::max(nInside,
                                  std::pow(nBelow * mainNe_ * mainNe_ * mainNe_, 0.25));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                break;
            case 4:
                neFirst = std::max(nInside,
                                   std::pow(nAbove * nAbove * nAbove * nAbove * mainNe_,
                                            0.2));
                neLast = std::max(nInside,
                                  std::pow(nBelow * nBelow * nBelow * nBelow * mainNe_,
                                           0.2));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside, std::cbrt(nAbove * nAbove * mainNe_));
                neLast = std::max(nInside, std::cbrt(nBelow * nBelow * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside, std::cbrt(nAbove * mainNe_ * mainNe_));
                neLast = std::max(nInside, std::cbrt(nBelow * mainNe_ * mainNe_));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                neFirst = std::max(nInside,
                                   std::pow(nAbove * mainNe_ * mainNe_ * mainNe_ * mainNe_,
                                            0.2));
                neLast = std::max(nInside,
                                  std::pow(nBelow * mainNe_ * mainNe_ * mainNe_ * mainNe_,
                                           0.2));
                for(size_t i = 0; i < layersPerArStage; i++) {
                    cells_[firstLayer++].refractiveIndex(neFirst);
                    cells_[lastLayer--].refractiveIndex(neLast);
                }
                break;
            default:
                break;
        }
    }
    // Fill the remaining main layers
    for(size_t i = firstLayer; i <= lastLayer; i++) {
        cells_[i].refractiveIndex(mainNe_);
    }
}

// Set the metamaterial patch sizes for the cells
void designer::MatchedColumn::assignPatches(LensDesigner* designer, MatchedColumn* lastCol) {
    // Set the plate sizes
    for(auto& c : cells_) {
        if(lastCol == nullptr || index_ < lastCol->index()) {
            double error = 0.0;
            double mainSize = designer->findPatchRatio(c.refractiveIndex(),
                                                       c.incidenceAngle(), c.unitCell(),
                                                       error);
            double fdtdSize = designer->fdtdSize(mainSize);
            c.error(error);
            c.plateSize(mainSize);
            c.fdtdPlateSize(fdtdSize);
        } else {
            c.error(0.0);
            c.plateSize(0.0);
            c.fdtdPlateSize(0.0);
        }
    }
}

// Calculate the current total phase shift in the column
// over that occurring in an empty column
void designer::MatchedColumn::calcCurrentPhaseShift(designer::LensDesigner* designer) {
    currentPhaseShift_ = 0.0;
    for(auto& cell : cells_) {
        double excessN = cell.refractiveIndex() - std::sqrt(designer->permittivityIn());
        double layerPhaseShift = 2.0 * box::Constants::pi_ * designer->centerFrequency()
                                 * cell.pathLength() * excessN / box::Constants::c_;
        currentPhaseShift_ += layerPhaseShift;
    }
}

// Calculate the dimensions of the curved columns
void designer::MatchedColumn::calcCurvedColDimensions(double nBelow, double g,
                                                      double d, double s, double b) {
    // The incidence angle (from the normal)
    double centerTheta = std::atan2(lm_, d);
    centerTheta = std::asin(nBelow / mainNe_ * std::sin(centerTheta));
    // The center circle
    double centerRadius = 0.0;
    double centerE = 0.0;
    if(!box::Utility::equals(centerTheta, 0.0)) {
        centerRadius = b / std::sin(centerTheta);
        centerE = centerRadius * std::cos(centerTheta);
    }
    // The outer circle
    double outerTheta = std::atan2(lm_ + g / 2.0, d);
    outerTheta = std::asin(nBelow / mainNe_ * std::sin(outerTheta));
    double outerRadius = b / std::sin(outerTheta);
    double outerE = outerRadius * std::cos(outerTheta);
    // The inner circle
    double innerTheta = std::atan2(lm_ - g / 2.0, d);
    innerTheta = std::asin(nBelow / mainNe_ * std::sin(innerTheta));
    double innerRadius = 0.0;
    double innerE = 0.0;
    if(!box::Utility::equals(innerTheta, 0.0)) {
        innerRadius = b / std::sin(innerTheta);
        innerE = innerRadius * std::cos(innerTheta);
    }
    // Initialise the total path length
    pathLength_ = 0.0;
    // Now for each layer
    for(size_t layer = 0; layer < cells_.size(); ++layer) {
        auto* cell = &cells_[layer];
        double yn = d + layer * s + s;
        double part = yn - d - b;
        // The outer circle
        double outerX = outerE - g / 2.0 - lm_ -
                        std::sqrt(outerRadius * outerRadius - part * part);
        // The inner circle
        double innerX = 0.0;
        if(!box::Utility::equals(innerTheta, 0.0)) {
            if(innerRadius < 0) {
                innerX = innerE + g / 2.0 - lm_ +
                         std::sqrt(innerRadius * innerRadius - part * part);
            } else {
                innerX = innerE + g / 2.0 - lm_ -
                         std::sqrt(innerRadius * innerRadius - part * part);
            }
        }
        // The unit cell
        double unitCell = innerX - outerX;
        cell->unitCell(unitCell);
        // The incidence angle
        double part1 = layer * s - b;
        double part2 = layer * s + s - b;
        double centerX1 = 0.0;
        double centerX2 = 0.0;
        if(!box::Utility::equals(centerTheta, 0.0)) {
            centerX1 = centerE - lm_ -
                       std::sqrt(centerRadius * centerRadius - part1 * part1);
            centerX2 = centerE - lm_ -
                       std::sqrt(centerRadius * centerRadius - part2 * part2);
        }
        double deltaCenterX = centerX1 - centerX2;
        double phi = std::atan2(deltaCenterX, s);
        cell->incidenceAngle(phi);
        // The path length
        double p = s;
        if(!box::Utility::equals(centerTheta, 0.0)) {
            double hyp = std::sqrt(s * s + deltaCenterX * deltaCenterX);
            double psi = std::asin(hyp / 2.0 / centerRadius);
            p = 2.0 * centerRadius * psi;
        }
        cell->pathLength(p);
        pathLength_ += p;
    }
}

// Adjust the cell refractive indices to account for the current error
void designer::MatchedColumn::adjustCurvedColPhaseShifts(
        double nInside, double nBelow, double f,
        MatchedColumn* outerCol) {
    if(outerCol == nullptr || index_ <= outerCol->index()) {
        // Calculate the current phase shift of this column
        currentPhaseShift_ = 0.0;
        for(auto& cell : cells_) {
            double layerPhaseShift = 2.0 * box::Constants::pi_ * f * cell.pathLength()
                                     * cell.refractiveIndex() / box::Constants::c_;
            currentPhaseShift_ += layerPhaseShift;
        }
        // Calculate the desired phase shift of this column
        double outerColPhaseShift = 2.0 * box::Constants::pi_ * f * outerCol->pathLength()
                                    * nInside / box::Constants::c_;
        double outerFocusPhaseShift = 2.0 * box::Constants::pi_ * f * nBelow /
                                      box::Constants::c_ * outerCol->wm();
        double focusPhaseShift = 2.0 * box::Constants::pi_ * f * nBelow /
                                 box::Constants::c_ * wm_;
        phaseShift_ = outerColPhaseShift + outerFocusPhaseShift - focusPhaseShift;
        // Calculate the refractive index error
        errorN_ = (phaseShift_ - currentPhaseShift_) * box::Constants::c_
                  / 2.0 / box::Constants::pi_ / f / pathLength_;
        // Adjust the main refractive index to compensate
        mainNe_ += errorN_;
    } else {
        mainNe_ = nInside;
        errorN_ = 0.0;
    }
}

