/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_HFSSSPARAMETER_H
#define FDTDLIFE_HFSSSPARAMETER_H

#include <string>
#include <complex>
#include <list>
#include <utility>
#include <Xml/DomObject.h>

namespace designer {
    class ParameterExtractor;

    // A class that holds as HFSS s-parameter for one configuration over a frequency range
    class HfssSParameter {
    public:
        // Construction
        explicit HfssSParameter(std::string header) :
                header_(std::move(header)) {}

        // Types
        class Item {
        public:
            explicit Item(double frequency) :
                    frequency_(frequency) {}
            double frequency_{0.0};
            std::complex<double> s11_;
            std::complex<double> s12_;
            std::complex<double> permittivity_;
            std::complex<double> permeability_;
            std::complex<double> lnZ_;
            std::complex<double> refractiveIndex_;
            void write(xml::DomObject& root) const {
                root << xml::Open();
                root << xml::Obj("frequency") << frequency_;
                root << xml::Obj("s11") << s11_;
                root << xml::Obj("s12") << s12_;
                root << xml::Obj("permittivity") << permittivity_;
                root << xml::Obj("permeability") << permeability_;
                root << xml::Obj("lnz") << lnZ_;
                root << xml::Obj("refractiveindex") << refractiveIndex_;
                root << xml::Close();
            }
            void read(xml::DomObject& root) {
                root >> xml::Open();
                root >> xml::Obj("frequency") >> frequency_;
                root >> xml::Obj("s11") >> s11_;
                root >> xml::Obj("s12") >> s12_;
                root >> xml::Obj("permittivity") >> permittivity_;
                root >> xml::Obj("permeability") >> permeability_;
                root >> xml::Obj("lnz") >> lnZ_;
                root >> xml::Obj("refractiveindex") >> xml::Default(0)
                     >> refractiveIndex_;
                root >> xml::Close();
            }
            friend xml::DomObject& operator<<(xml::DomObject& o, const Item& v) {
                v.write(o);
                return o;
            }
            friend xml::DomObject& operator>>(xml::DomObject& o, Item& v) {
                v.read(o);
                return o;
            }
        };

        // Getters
        [[nodiscard]] const std::string& header() const { return header_; }
        [[nodiscard]] double unitCell() const { return unitCell_; }
        [[nodiscard]] double patchRatio() const { return patchRatio_; }
        [[nodiscard]] double incidenceAngle() const { return incidenceAngle_; }

        // Setters
        void header(const std::string& v) { header_ = v; }
        void unitCell(double v) { unitCell_ = v; };
        void patchRatio(double v) { patchRatio_ = v; };
        void incidenceAngle(double v) { incidenceAngle_ = v; };

        // API
        void s11(double frequency, double value, bool imaginary);
        void s12(double frequency, double value, bool imaginary);
        Item* find(double frequency);
        void calculate(ParameterExtractor* designer);
        double maxRefractiveIndex();
        void write(xml::DomObject& root) const;
        void read(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const HfssSParameter& v) {
            v.write(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, HfssSParameter& v) {
            v.read(o);
            return o;
        }

        // Iterators
        typedef std::list<Item>::iterator iterator;
        iterator begin() { return points_.begin(); }
        iterator end() { return points_.end(); }
        Item& front() { return points_.front(); }
        Item& back() { return points_.back(); }
        bool empty() { return points_.empty(); }
        size_t size() { return points_.size(); }

    protected:
        // Members
        std::string header_;
        double unitCell_{0.0};
        double patchRatio_{0.0};
        double incidenceAngle_{0.0};
        std::list<Item> points_;

        // Helpers
        Item& restore();
    };

}


#endif //FDTDLIFE_HFSSSPARAMETER_H
