/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "ParameterExtractor.h"
#include <Box/Utility.h>
#include <Box/Constants.h>
#include <Fdtd/Model.h>

// Phase 2 constructor called by the factory
void designer::ParameterExtractor::construct(int mode, int identifier, fdtd::Model* m) {
    // Base class first
    Designer::construct(mode, identifier, m);
}

// Write the configuration to the DOM object
void designer::ParameterExtractor::writeConfig(xml::DomObject& root) const {
    // Base class first
    Designer::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("layerspacing") << layerSpacing_;
    root << xml::Obj("numlayers") << numLayers_;
    root << xml::Obj("vacuumgap") << vacuumGap_;
    root << xml::Obj("basepermittivity") << basePermittivity_;
    for(auto& p : parameters_) {
        root << xml::Obj("sparameter") << p;
    }
    root << xml::Close();
}

// Read the configuration from the DOM object
void designer::ParameterExtractor::readConfig(xml::DomObject& root) {
    // Base class first
    Designer::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("layerspacing") >> xml::Default(0.0001) >> layerSpacing_;
    root >> xml::Obj("numlayers") >> xml::Default(10) >> numLayers_;
    root >> xml::Obj("basepermittivity") >> xml::Default(1.0) >> basePermittivity_;
    root >> xml::Obj("vacuumgap") >> vacuumGap_;
    for(auto& o : root.obj("sparameter")) {
        *o >> restore();
    }
    root >> xml::Close();
}

// Paste the HFSS text into the parameter extractor
void designer::ParameterExtractor::pasteHfssData(const std::string& text) {
    const std::string headerSep = " []";
    const std::string headerLeader = " - ";
    auto lines = box::Utility::split(text, "\n");
    bool firstLine = true;
    std::list<std::pair<PasteKind, HfssSParameter*>> results;
    double frequency = 0.0;
    std::list<std::pair<PasteKind, HfssSParameter*>>::iterator resultPos;
    for(auto& line : lines) {
        if(!line.empty()) {
            resultPos = results.begin();
            auto items = box::Utility::split(line, "\t");
            bool firstItem = true;
            for(auto& item : items) {
                item = box::Utility::strip(item, "\"");
                if(firstLine) {
                    // Header line
                    if(firstItem) {
                        // Skip the frequency column header
                    } else {
                        // Parse the column header text
                        auto pos = item.find(headerSep);
                        std::string header;
                        std::string prefix;
                        if(pos == std::string::npos) {
                            prefix = item;
                        } else {
                            if((pos + headerSep.size() + headerLeader.size()) < item.size()) {
                                header = item.substr(pos + headerSep.size() +
                                                     headerLeader.size());
                            }
                            prefix = item.substr(0, pos);
                        }
                        PasteKind kind;
                        if(prefix == "re(S(Floquet1:1,Floquet1:1))" ||
                           prefix == "re(S(Floquet1:2,Floquet1:2))") {
                            kind = PasteKind::s11Real;
                        } else if(prefix == "im(S(Floquet1:1,Floquet1:1))" ||
                                  prefix == "im(S(Floquet1:2,Floquet1:2))") {
                            kind = PasteKind::s11Imag;
                        } else if(prefix == "re(S(Floquet2:1,Floquet1:1))" ||
                                  prefix == "re(S(Floquet2:2,Floquet1:2))") {
                            kind = PasteKind::s12Real;
                        } else if(prefix == "im(S(Floquet2:1,Floquet1:1))" ||
                                  prefix == "im(S(Floquet2:2,Floquet1:2))") {
                            kind = PasteKind::s12Imag;
                        }
                        // Find or create the result for this column
                        auto* parameter = find(header);
                        results.emplace_back(kind, parameter);
                        auto info = parseHeader(header);
                        if(info.count("g")) {
                            parameter->unitCell(info["g"]);
                        }
                        if(info.count("p")) {
                            parameter->patchRatio(info["p"] /
                                                  box::Constants::percent_);
                        }
                        if(info.count("incAngle")) {
                            parameter->incidenceAngle(info["incAngle"]);
                        }
                    }
                } else {
                    if(firstItem) {
                        // The frequency column
                        frequency = box::Utility::toDouble(item);
                    } else if(!box::Utility::equals(frequency, 0.0)) {
                        // Store the value
                        double value = box::Utility::toDouble(item);
                        switch(resultPos->first) {
                            case PasteKind::s11Real:
                                resultPos->second->s11(frequency, value, false);
                                break;
                            case PasteKind::s12Real:
                                resultPos->second->s12(frequency, value, false);
                                break;
                            case PasteKind::s11Imag:
                                resultPos->second->s11(frequency, value, true);
                                break;
                            case PasteKind::s12Imag:
                                resultPos->second->s12(frequency, value, true);
                                break;
                        }
                        ++resultPos;
                    }
                }
                firstItem = false;
            }
        }
        firstLine = false;
    }
}

// Find or create an S-parameter with the given header
designer::HfssSParameter* designer::ParameterExtractor::find(const std::string& header) {
    HfssSParameter* result = nullptr;
    for(auto& param : parameters_) {
        if(param.header() == header) {
            result = &param;
            break;
        }
    }
    if(result == nullptr) {
        parameters_.emplace_back(header);
        result = &parameters_.back();
    }
    return result;
}

// Perform the parameter extraction calculations
void designer::ParameterExtractor::calculate() {
    for(auto& p : parameters_) {
        p.calculate(this);
    }
}

// Restore a parameter from the DOM object
designer::HfssSParameter& designer::ParameterExtractor::restore() {
    parameters_.emplace_back("");
    return parameters_.back();
}

// Parse the HFSS header text.
std::map<std::string, double> designer::ParameterExtractor::parseHeader(
        const std::string& text) {
    std::map<std::string, double> result;
    auto items = box::Utility::split(text, " ");
    for(auto& item : items) {
        auto parts = box::Utility::split(item, "=");
        if(parts.size() == 2) {
            std::string valStr = box::Utility::strip(parts[1], "\'");
            double value = box::Utility::toDouble(valStr);
            if(box::Utility::endsWith(valStr, "mm")) {
                value *= box::Constants::milli_;
            } else if(box::Utility::endsWith(valStr, "GHz")) {
                value *= box::Constants::giga_;
            }
            result[parts[0]] = value;
        }
    }
    return result;
}

// Return the maximum refractive index from the data set
double designer::ParameterExtractor::maxRefractiveIndex() {
    double result = 0.0;
    for(auto& p : parameters_) {
        result = std::max(result, p.maxRefractiveIndex());
    }
    return result;
}

// Convert the extracted data into entries in the parameters table
// Also creates entries for the raw dielectric
void designer::ParameterExtractor::makeParameters() {
    auto& params = m_->d()->parameterTables();
    std::set<int> unitCellsUm;   // The set of unit cells in microns
    // Create the tables from the parameters
    for(auto& t : parameters_) {
        auto* table = params.make();
        table->name(t.header());
        table->layerSpacing(layerSpacing_);
        table->incidenceAngle(t.incidenceAngle());
        table->unitCell(t.unitCell());
        table->patchRatio(t.patchRatio());
        auto* curve = table->make();
        curve->which(domain::ParameterCurve::Which::refractiveIndex);
        for(auto& p : t) {
            auto* point = curve->make();
            point->frequency(p.frequency_ * box::Constants::giga_);
            point->value(p.refractiveIndex_);
        }
        unitCellsUm.insert(static_cast<int>(table->unitCell() / box::Constants::micro_));
    }
    // Generate tables for the base dielectric with 0 patch ratio
    auto& t = parameters_.front();
    for(auto g : unitCellsUm) {
        std::stringstream s;
        s << "g=" << g << "um, p=0";
        auto* table = params.make();
        table->name(s.str());
        table->layerSpacing(layerSpacing_);
        table->incidenceAngle(0.0);
        table->unitCell(g * box::Constants::micro_);
        table->patchRatio(0.0);
        auto* curve = table->make();
        curve->which(domain::ParameterCurve::Which::refractiveIndex);
        for(auto& p : t) {
            auto* point = curve->make();
            point->frequency(p.frequency_ * box::Constants::giga_);
            point->value(std::sqrt(basePermittivity_));
        }
    }
}

// Delete the n'th parameter table
void designer::ParameterExtractor::deleteParameter(size_t n) {
    auto pos = find(n);
    if(pos != parameters_.end()) {
        parameters_.erase(pos);
    }
}

// Return an iterator to the nth parameter in the list
std::list<designer::HfssSParameter>::iterator designer::ParameterExtractor::find(size_t n) {
    auto result = parameters_.end();
    if(n < parameters_.size()) {
        result = std::next(parameters_.begin(), n);
    }
    return result;
}

