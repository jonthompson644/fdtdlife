/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_LENSRAYTRACER_H
#define FDTDLIFE_LENSRAYTRACER_H

#include <list>
#include <Xml/DomObject.h>

namespace designer { class LensDesigner; }

// This class implements the ray tracing algorithm used for the lens modelling.
// This implementation is inherently 2D lying in the x/z plane of the 3D universe.
namespace designer {
    class LensRayTracer {
    public:
        // Types
        class Coord {
        public:
            Coord(double x, double z) :
                    x_(x),
                    z_(z) {}
            Coord() = default;
            double x_{0.0};
            double z_{0.0};
        };

        class Ray {
        public:
            Ray(double angle, const Coord& pos) :
                    angle_(angle) { path_.emplace_back(pos); }
            std::list<Coord> path_;  // The path the ray has taken
            double angle_;  // The current angle of the ray from the z axis
        };

        // Construction
        LensRayTracer() = default;
        void construct(LensDesigner* designer);

        // API
        void calculateRays();
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const LensRayTracer& p) {
            p.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, LensRayTracer& p) {
            p.readConfig(o);
            return o;
        }

        // Getters
        [[nodiscard]] double deltaAngle() const { return deltaAngle_; }
        [[nodiscard]] double deltaR() const { return deltaR_; }
        [[nodiscard]] double deltaS() const { return deltaS_; }
        [[nodiscard]] double maxAngle() const { return maxAngle_; }
        [[nodiscard]] const std::list<Ray>& rays() const { return rays_; }
        [[nodiscard]] double x1() const { return x1_; }
        [[nodiscard]] double x2() const { return x2_; }
        [[nodiscard]] double z1() const { return z1_; }
        [[nodiscard]] double z2() const { return z2_; }
        [[nodiscard]] double zLens1() const { return zLens1_; }
        [[nodiscard]] double zLens2() const { return zLens2_; }
        [[nodiscard]] size_t columnsInDiameter() const { return columnsInDiameter_; }
        [[nodiscard]] double unitCell() const { return unitCell_; }

        // Setters
        void deltaAngle(double v) { deltaAngle_ = v; }
        void deltaR(double v) { deltaR_ = v; }
        void deltaS(double v) { deltaS_ = v; }
        void maxAngle(double v) { maxAngle_ = v; }

    protected:
        // Members
        LensDesigner* designer_{};
        std::list<Ray> rays_;
        double x1_{};  // X domain boundaries
        double x2_{};
        double z1_{};  // Z domain boundaries
        double z2_{};
        double zLens1_{};  // Z lens boundaries
        double zLens2_{};
        size_t columnsInDiameter_{};  // The number of columns in the lens
        double unitCell_{};  // The unit cell size

        // Configuration
        double deltaAngle_{1.0};   // The angle increment for the starting rays
        double deltaR_{0.0001};  // The calculation increment along the path
        double deltaS_{0.0001};  // The distance each side of the path to find the ref index
        double maxAngle_{30.0};  // The maximum angle for rays

        // Helper functions
        void propagateRay(Ray& ray);
    };
}


#endif //FDTDLIFE_LENSRAYTRACER_H
