/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#ifndef FDTDLIFE_CELLCONFIG_H
#define FDTDLIFE_CELLCONFIG_H

#include <Xml/DomObject.h>

namespace designer {

    // A class containing the configuration information for a column cell
    class CellConfig {
    public:
        // Construction
        CellConfig() = default;

        // Types
        enum {errorOk=0, errorRangeLimited=1};

        // Getters
        [[nodiscard]] double refractiveIndex() const { return refractiveIndex_; }
        [[nodiscard]] double plateSize() const { return plateSize_; }
        [[nodiscard]] double fdtdPlateSize() const { return fdtdPlateSize_; }
        [[nodiscard]] int dielectricMaterial() const { return dielectricMaterial_; }
        [[nodiscard]] int leftShape(size_t y) const { return leftShape_[y]; }
        [[nodiscard]] int rightShape(size_t y) const { return rightShape_[y]; }
        [[nodiscard]] int variable() const { return variable_; }
        [[nodiscard]] double unitCell() const { return unitCell_; }
        [[nodiscard]] double incidenceAngle() const { return incidenceAngle_; }
        [[nodiscard]] double pathLength() const { return pathLength_; }
        [[nodiscard]] double error() const { return error_; }

        // Setters
        void refractiveIndex(double v) { refractiveIndex_ = v; }
        void plateSize(double v) { plateSize_ = v; }
        void fdtdPlateSize(double v) { fdtdPlateSize_ = v; }
        void dielectricMaterial(int v) { dielectricMaterial_ = v; }
        void leftShape(size_t y, int v);
        void rightShape(size_t y, int v);
        void variable(int v) { variable_ = v; }
        void unitCell(double v) { unitCell_ = v; }
        void incidenceAngle(double v) { incidenceAngle_ = v; }
        void pathLength(double v) { pathLength_ = v; }
        void error(double v) { error_ = v; }

        // API
        [[nodiscard]] bool isResultValid() const;
        void writeConfig(xml::DomObject& root) const;
        void readConfig(xml::DomObject& root);
        friend xml::DomObject& operator<<(xml::DomObject& o, const CellConfig& v) {
            v.writeConfig(o);
            return o;
        }
        friend xml::DomObject& operator>>(xml::DomObject& o, CellConfig& v) {
            v.readConfig(o);
            return o;
        }
        [[nodiscard]] size_t numLeftShapes() const { return leftShape_.size(); }
        [[nodiscard]] size_t numRightShapes() const { return rightShape_.size(); }

    protected:
        // Members
        double refractiveIndex_{};  // The refractive index of the cell
        double plateSize_{};  // The size of the plate that creates the refractive index
        double fdtdPlateSize_{};  // The plate size rounded to the FDTD resolution
        int dielectricMaterial_{};  // The material used by this cell in dielectric mode
        std::vector<int> leftShape_;  // The identifier of the left hand shapes
        std::vector<int> rightShape_;  // The identifier of the right hand shapes
        int variable_{};  // The identifier of a variable used with the column
        double unitCell_{};  // The unit cell of this layer in the column
        double incidenceAngle_{};  // The incidence angle of this layer in the column
        double pathLength_{};  // The length of the path inside the cell
        double error_{};  // Difference between the desired and achieved refractive index
    };
}


#endif //FDTDLIFE_CELLCONFIG_H
