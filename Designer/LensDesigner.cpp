/*
  * ===========================================================================
  * Copyright 2017 Jonathan Thompson
  * 
  * Redistribution and use in source and binary forms, with or without modification, 
  * are permitted provided that the following conditions are met:
  * 
  * 1. Redistributions of source code must retain the above copyright notice, 
  * this list of conditions and the following disclaimer.
  * 
  * 2. Redistributions in binary form must reproduce the above copyright notice, 
  * this list of conditions and the following disclaimer in the documentation 
  * and/or other materials provided with the distribution.
  * 
  * 
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
  * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  * ===========================================================================
*/

#include "LensDesigner.h"
#include <Fdtd/Model.h>
#include <Domain/Material.h>
#include <Domain/Cuboid.h>
#include <Domain/CuboidArray.h>
#include <Domain/BinaryPlateNxN.h>
#include <Domain/FingerPlate.h>
#include <Box/Utility.h>
#include <Box/StlWriter.h>
#include <Gs/GeneticSearch.h>
#include <Gs/FitnessFunction.h>
#include <Gs/FitnessAnalyser.h>
#include <Gs/Population.h>
#include <Gs/Individual.h>
#include <Gs/Chromosome.h>
#include <Gs/VariableGene.h>
#include <Sensor/LensReflectionAnalyser.h>
#include <Sensor/LensTransmissionAnalyser.h>
#include <Sensor/LensPhaseAnalyser.h>
#include <Domain/CatalogueLayer.h>
#include <Box/DxfWriter.h>
#include <Box/HfssWriter.h>

// Phase 2 constructor called by the factory
void designer::LensDesigner::construct(int mode, int identifier, fdtd::Model* m) {
    // Base class first
    Designer::construct(mode, identifier, m);
    // Other stuff
    rayTracer_.construct(this);
}

// Generate the design using the phase shift method and load the model
void designer::LensDesigner::generatePhaseShift() {
    if(doGlobalCalculations()) {
        // Perform the column calculations
        switch(columnType_) {
            case ColumnType::straight:
                // Do original vertical columns method
                for(auto& col : columns_) {
                    col.calculateVerticalCol(this);
                }
                break;
            case ColumnType::curved:
            case ColumnType::curvedConstG:
                // Do new curved columns method
                generateCurvedCols();
                break;
        }
        if(!isResultValid()) {
            error_ = "Results contain a NAN";
        } else {
            // Load the model
            loadModel();
        }
    }
}

// Generate the design using the new curved columns method
void designer::LensDesigner::generateCurvedCols() {
    // Do we have valid data?
    if(numLayers_ > 0) {
        // Refractive indices
        double nBelow = std::sqrt(permittivityBelow_);
        double nInside = std::sqrt(permittivityIn_);
        double nAbove = std::sqrt(permittivityAbove_);
        // Initialise all the column refractive indices to the base dielectric
        double d = focusDistance_ * focalLengthFudgeFactor_;
        double lm = 0.0;
        if(hasEvenColumns()) {
            lm += unitCell_ / 2.0;
        }
        for(auto& col : columns_) {
            col.initialiseCurvedCol(nInside, numLayers_, lm, d);
            lm += unitCell_;
        }
        MatchedColumn* lastCol = nullptr;
        for(size_t i = 0; i < 20; i++) {
            // Calculate the cell size, arc length and incidence angle for each column
            for(auto& col : columns_) {
                col.calcCurvedColDimensions(nBelow, unitCell_, d,
                                            layerSpacing_, lensStructureSize_);
            }
            // Find the outermost column, ie. the highest numbered column that
            // is still within the lens radius
            double x = 0.0;
            for(auto& col : columns_) {
                auto& cell = col.cell(numLayers_ - 1);
                if(col.index() == 0 && !hasEvenColumns()) {
                    x += cell.unitCell() / 2.0;
                } else {
                    x += cell.unitCell();
                }
                if(x < lensDiameter_ / 2.0) {
                    lastCol = &col;
                }
            }
            // Adjust the refractive indices in each column to account
            // for the phase shift error
            for(auto& col : columns_) {
                col.adjustCurvedColPhaseShifts(nInside, nBelow, centerFrequency_, lastCol);
            }
            // Assign the refractive index to layers taking into account
            // any anti-reflection layers
            for(auto& col : columns_) {
                col.assignRefractiveIndices(nAbove, nBelow, nInside, numLayers_,
                                            layersPerArcStage_, numArcStages_, lastCol);
            }
            std::cout << "Iteration " << i << " outer column " << lastCol->index()
                      << " center main n " << columns_.front().mainNe()
                      << " error n " << columns_.front().errorN() << std::endl;
        }
        // If we are in constant unit cell on a layer mode...
        if(columnType_ == ColumnType::curvedConstG) {
            // ...Fix up the layer unit cells as the average on each layer
            for(size_t layer = 0; layer < numLayers_; layer++) {
                //  The total width of the active columns in the layer
                double totalWidth = 0.0;
                double nCols = 0.0;
                bool first = hasEvenColumns();
                for(auto& col : columns_) {
                    if(col.index() <= lastCol->index()) {
                        auto& cell = col.cell(layer);
                        if(first) {
                            totalWidth += cell.unitCell() / 2.0;
                            nCols += 0.5;
                            first = false;
                        } else {
                            totalWidth += cell.unitCell();
                            nCols += 1.0;
                        }
                    }
                }
                // Set the columns to the average width
                double avWidth = totalWidth / nCols;
                for(auto& col : columns_) {
                    if(col.index() <= lastCol->index()) {
                        auto& cell = col.cell(layer);
                        cell.unitCell(avWidth);
                    }
                }
            }
        }
        // Assign meta-material patches
        for(auto& col : columns_) {
            col.assignPatches(this, lastCol);
        }
    }
}

// Perform the common calculations for all modes
bool designer::LensDesigner::doGlobalCalculations() {
    bool result = false;
    error_ = "";
    // Check data validity
    bool valid = true;
    valid = valid && unitCell_ > 0.0;
    valid = valid && lensDiameter_ > unitCell_;
    if(valid) {
        // Work out the number of columns required
        columnsInDiameter_ = static_cast<size_t>(std::ceil(lensDiameter_ / unitCell_));
        size_t numColumns = (columnsInDiameter_ + 1U) / 2U;
        columns_.clear();
        for(size_t i = 0; i < numColumns; i++) {
            columns_.emplace_back(i);
        }
        // Perform the global calculations
        double velocity = box::Constants::c_ / std::sqrt(permittivityBelow_);
        wavelengthBelow_ = velocity / centerFrequency_;
        tau_ = static_cast<double>(numLayers_ - 2 * layersPerArcStage_)
               / (2.0 * static_cast<double>(layersPerArcStage_));
        lensStructureSize_ = numLayers_ * layerSpacing_;
        result = true;
    } else {
        error_ = "Invalid unit cell or lens diameter";
    }
    return result;
}

// Load the columns into the model
void designer::LensDesigner::loadModel() {
    loadModelConfig();
    // The columns
    std::list<domain::Shape*> shapes;
    switch(columnType_) {
        case ColumnType::straight:
            loadStraightColumns(shapes);
            break;
        case ColumnType::curved:
        case ColumnType::curvedConstG:
            loadCurvedColumns(shapes, false);
            break;
    }
}

// Load the non-shape parts of the model
void designer::LensDesigner::loadModelConfig() {
    // Remove anything previously loaded
    m_->designers().clearModel();
    // The parameters
    m_->p()->doFdtd(true);
    m_->p()->doPropagationMatrices(false);
    m_->p()->s(0.5);
    m_->p()->scatteredFieldZoneSize(2);
    double diameter = columnsInDiameter_ * unitCell_;
    m_->p()->p1({-diameter / 2.0, -unitCell_ * sliceThickness_ / 2.0,
                 -edgeDistance_});
    m_->p()->p2({diameter / 2.0, unitCell_ * sliceThickness_ / 2.0,
                 lensStructureSize_ + focusDistance_});
    dr_ = unitCell_ / fdtdCellsPerUnitCell_;
    m_->p()->dr({dr_, dr_, dr_});
    m_->p()->boundarySize(40);
    m_->p()->boundary({fdtd::Configuration::Boundary::absorbing,
                       fdtd::Configuration::Boundary::periodic,
                       fdtd::Configuration::Boundary::absorbing});
    m_->p()->useThinSheetSubcells(false);
    // The metal material and lens body material
    switch(modelling_) {
        case Modelling::metamaterialCells:
        case Modelling::fingerPatterns: {
            auto metal = m_->designers().makeNormalMaterial();
            metal->epsilon(box::Expression(box::Constants::epsilonMetal_));
            metal->mu(box::Expression(box::Constants::mu0_));
            metal->sigma(box::Expression(box::Constants::sigmaMetal_));
            metal->sigmastar(box::Expression(box::Constants::sigmastar0_));
            metal->color(box::Constants::Colour::colourMagenta);
            metal->name("Metal");
            metal->priority(2);
            metalMaterialIndex_ = metal->index();
            if(!box::Utility::equals(permittivityAbove_, permittivityIn_)) {
                auto body = m_->designers().makeNormalMaterial();
                body->epsilon(box::Expression(box::Constants::epsilon0_ *
                                              permittivityIn_));
                body->mu(box::Expression(box::Constants::mu0_));
                body->sigma(box::Expression(box::Constants::sigma0_));
                body->sigmastar(box::Expression(box::Constants::sigmastar0_));
                body->color(box::Constants::Colour::colourCyan);
                body->name("LensBody");
                body->priority(1);
                inMaterialIndex_ = body->index();
            }
            break;
        }
        case Modelling::dielectricBlocks:
            break;
    }
    // The background material
    auto background = m_->d()->getMaterial(domain::Domain::defaultMaterial);
    background->epsilon(box::Expression(box::Constants::epsilon0_ *
                                        permittivityAbove_));
    // The material below the lens
    if(!box::Utility::equals(permittivityAbove_, permittivityBelow_)) {
        auto below = m_->designers().makeNormalMaterial();
        below->epsilon(box::Expression(box::Constants::epsilon0_ * permittivityBelow_));
        below->mu(box::Expression(box::Constants::mu0_));
        below->sigma(box::Expression(box::Constants::sigma0_));
        below->sigmastar(box::Expression(box::Constants::sigmastar0_));
        below->color(box::Constants::Colour::colourYellow);
        below->name("BelowLens");
        below->priority(1);
        belowMaterialIndex_ = below->index();
    }
    // Make the inside and below blocks
    switch(modelling_) {
        case Modelling::metamaterialCells:
        case Modelling::fingerPatterns:
            if(!box::Utility::equals(permittivityAbove_, permittivityIn_)) {
                auto insideCube = m_->designers().makeCuboid();
                insideCube->material(inMaterialIndex_);
                insideCube->p1({-lensDiameter_ / 2.0 - dr_ / 2.0,
                                -unitCell_ * sliceThickness_ / 2.0,
                                -layerSpacing_ / 2.0});
                insideCube->p2({lensDiameter_ / 2.0,
                                unitCell_ * sliceThickness_ / 2.0,
                                numLayers_ * layerSpacing_ - layerSpacing_ / 2.0});
                insideCube->name("InsideCube");
            }
            break;
        case Modelling::dielectricBlocks:
            break;
    }
    if(!box::Utility::equals(permittivityAbove_, permittivityBelow_)) {
        auto belowCube = m_->designers().makeCuboid();
        belowCube->material(belowMaterialIndex_);
        belowCube->p1({-lensDiameter_ / 2.0 - dr_ / 2.0,
                       -unitCell_ * sliceThickness_ / 2.0,
                       numLayers_ * layerSpacing_ - layerSpacing_ / 2.0});
        belowCube->p2({lensDiameter_ / 2.0,
                       unitCell_ * sliceThickness_ / 2.0,
                       numLayers_ * layerSpacing_ - layerSpacing_ / 2.0 + focusDistance_});
        belowCube->name("BelowCube");
    }
}

// Load the straight columns into the model
void designer::LensDesigner::loadStraightColumns(std::list<domain::Shape*>& shapes) {
    // The refractive index cells
    double offset = 0.0;
    if(hasEvenColumns()) {
        offset = unitCell_ / 2.0;
    }
    box::Vector<double> pos;
    for(auto& col : columns_) {
        pos.x(col.index() * unitCell_ + offset);
        pos.y(0.0);
        pos.z(0.0);
        // The layers
        size_t layer = 0;
        while(layer < numLayers_) {
            // How many identical layers?
            size_t numDuplicates = 1;
            while((layer + numDuplicates) < numLayers_
                  && box::Utility::equals(
                    col.cell(layer + numDuplicates).refractiveIndex(),
                    col.cell(layer).refractiveIndex())) {
                numDuplicates++;
            }
            // Plant the item
            pos.y(-static_cast<double>(sliceThickness_ - 1U) * unitCell_ / 2.0);
            for(size_t j = 0; j < sliceThickness_; j++) {
                shapes.push_back(
                        makeSquare(pos, col, layer, false, j, numDuplicates));
                if(hasEvenColumns() || col.index() != 0) {
                    shapes.push_back(
                            makeSquare(pos, col, layer, true, j, numDuplicates));
                }
                pos.y(pos.y() + unitCell_);
            }
            pos.z(pos.z() + layerSpacing_ * numDuplicates);
            layer += numDuplicates;
        }
    }
}

// Load the curved columns into the model
void designer::LensDesigner::loadCurvedColumns(std::list<domain::Shape*>& shapes, bool slice) {
    // Find the outermost column that still appears at the top of the lens
    double x = 0.0;
    size_t outermostCol = 0;
    if(numLayers_ > 0) {
        for(auto& col : columns_) {
            auto& cell = col.cell(numLayers_ - 1);
            if((x + cell.unitCell()) < (lensDiameter_ / 2.0)) {
                outermostCol = col.index();
                if(col.index() == 0 && !hasEvenColumns()) {
                    x = cell.unitCell() / 2.0;
                } else {
                    x += cell.unitCell();
                }
            } else {
                break;
            }
        }
    }
    // Now plant the cells for these columns
    for(size_t layer = 0; layer < numLayers_; layer++) {
        x = 0.0;
        for(auto& col : columns_) {
            auto& cell = col.cell(layer);
            if(col.index() <= outermostCol) {
                if(col.index() == 0 && !hasEvenColumns()) {
                    // Plant the central column in odd mode
                    shapes.push_back(makeCurvedSquare(col, layer, 0.0, slice));
                    x = cell.unitCell() / 2.0;
                } else {
                    // Plant the left and right columns
                    x += cell.unitCell() / 2.0;
                    shapes.push_back(makeCurvedSquare(col, layer, -x, slice));
                    shapes.push_back(makeCurvedSquare(col, layer, x, slice));
                    x += cell.unitCell() / 2.0;
                }
            }
        }
    }
}

// Write the configuration to the DOM object
void designer::LensDesigner::writeConfig(xml::DomObject& root) const {
    // Base class first
    Designer::writeConfig(root);
    // Now my stuff
    root << xml::Reopen();
    root << xml::Obj("permittivityabove") << permittivityAbove_;
    root << xml::Obj("permittivitybelow") << permittivityBelow_;
    root << xml::Obj("permittivityin") << permittivityIn_;
    root << xml::Obj("focusdistance") << focusDistance_;
    root << xml::Obj("centerfrequency") << centerFrequency_;
    root << xml::Obj("unitcell") << unitCell_;
    root << xml::Obj("lensdiameter") << lensDiameter_;
    root << xml::Obj("layerspacing") << layerSpacing_;
    root << xml::Obj("numlayers") << numLayers_;
    root << xml::Obj("nummatchinglayers") << layersPerArcStage_;
    root << xml::Obj("numarcstages") << numArcStages_;
    root << xml::Obj("fdtdcellsperunitcell") << fdtdCellsPerUnitCell_;
    root << xml::Obj("edgedistance") << edgeDistance_;
    root << xml::Obj("modelling") << modelling_;
    root << xml::Obj("focallengthfudgefactor") << focalLengthFudgeFactor_;
    root << xml::Obj("platethickness") << plateThickness_;
    root << xml::Obj("exportsymmetricalhalf") << exportSymmetricalHalf_;
    root << xml::Obj("gacolumnnumber") << columnNumber_;
    root << xml::Obj("geneticrange") << geneticRange_;
    root << xml::Obj("geneticbits") << geneticBits_;
    root << xml::Obj("bitsperhalfside") << bitsPerHalfSide_;
    root << xml::Obj("slicethickness") << sliceThickness_;
    root << xml::Obj("minfeaturesize") << minFeatureSize_;
    root << xml::Obj("fingeroffset") << fingerOffset_;
    for(auto& c : columns_) {
        root << xml::Obj("column") << c;
    }
    root << xml::Obj("columnsindiameter") << columnsInDiameter_;
    root << xml::Obj("wavelengthbelow") << wavelengthBelow_;
    root << xml::Obj("tau") << tau_;
    root << xml::Obj("lensstructuresize") << lensStructureSize_;
    root << xml::Obj("metalmaterialindex") << metalMaterialIndex_;
    root << xml::Obj("belowmaterialindex") << belowMaterialIndex_;
    root << xml::Obj("inmaterialindex") << inMaterialIndex_;
    root << xml::Obj("dr") << dr_;
    root << xml::Obj("error") << error_;
    root << xml::Obj("columntype") << columnType_;
    root << xml::Obj("raytracer") << rayTracer_;
    root << xml::Obj("exportlayer") << exportLayer_;
    root << xml::Close();
}

// Read the configuration from the DOM object
void designer::LensDesigner::readConfig(xml::DomObject& root) {
    // Base class first
    Designer::readConfig(root);
    // Now my stuff
    root >> xml::Reopen();
    root >> xml::Obj("permittivityabove") >> xml::Default(1.0)
         >> permittivityAbove_;
    root >> xml::Obj("permittivitybelow") >> xml::Default(2.2)
         >> permittivityBelow_;
    root >> xml::Obj("permittivityin") >> xml::Default(2.2) >> permittivityIn_;
    if(!root.obj("bgndrelpermittivity").empty()) {
        root >> xml::Obj("bgndrelpermittivity") >> permittivityAbove_;
        permittivityBelow_ = permittivityAbove_;
        permittivityIn_ = permittivityAbove_;
    }
    root >> xml::Obj("focusdistance") >> focusDistance_;
    root >> xml::Obj("centerfrequency") >> centerFrequency_;
    root >> xml::Obj("unitcell") >> unitCell_;
    root >> xml::Obj("lensdiameter") >> lensDiameter_;
    root >> xml::Obj("layerspacing") >> layerSpacing_;
    root >> xml::Obj("nummatchinglayers") >> xml::Default(0) >> layersPerArcStage_;
    // Start backwards compatible
    root >> xml::Obj("nummainlayers") >> xml::Default(0) >> numLayers_;
    numLayers_ += layersPerArcStage_ * 2;
    // End backwards compatible
    root >> xml::Obj("numarcstages") >> xml::Default(1U) >> numArcStages_;
    root >> xml::Obj("numlayers") >> xml::Default(numLayers_) >> numLayers_;
    root >> xml::Obj("fdtdcellsperunitcell") >> fdtdCellsPerUnitCell_;
    root >> xml::Obj("edgedistance") >> xml::Default(0.002) >> edgeDistance_;
    root >> xml::Obj("modelling")
         >> xml::Default(static_cast<int>(Modelling::metamaterialCells))
         >> modelling_;
    root >> xml::Obj("focallengthfudgefactor") >> xml::Default(1.0)
         >> focalLengthFudgeFactor_;
    root >> xml::Obj("platethickness") >> xml::Default(0.000001)
         >> plateThickness_;
    root >> xml::Obj("exportsymmetricalhalf") >> xml::Default(true)
         >> exportSymmetricalHalf_;
    root >> xml::Obj("gacolumnnumber") >> xml::Default(0) >> columnNumber_;
    root >> xml::Obj("geneticrange") >> xml::Default(10.0) >> geneticRange_;
    root >> xml::Obj("geneticbits") >> xml::Default(4U) >> geneticBits_;
    root >> xml::Obj("bitsperhalfside") >> xml::Default(10) >> bitsPerHalfSide_;
    root >> xml::Obj("slicethickness") >> xml::Default(1) >> sliceThickness_;
    root >> xml::Obj("minfeaturesize") >> xml::Default(10e-6) >> minFeatureSize_;
    root >> xml::Obj("fingeroffset") >> xml::Default(0.0) >> fingerOffset_;
    doGlobalCalculations(); // In case no calculated parameters stored
    columns_.clear();
    size_t layer = 0;
    for(auto& o : root.obj("column")) {
        columns_.emplace_back(layer);
        *o >> columns_.back();
        layer++;
    }
    root >> xml::Obj("columnsindiameter") >> xml::Default(columnsInDiameter_)
         >> columnsInDiameter_;
    root >> xml::Obj("wavelengthbelow") >> xml::Default(wavelengthBelow_)
         >> wavelengthBelow_;
    root >> xml::Obj("tau") >> xml::Default(tau_) >> tau_;
    root >> xml::Obj("lensstructuresize") >> xml::Default(lensStructureSize_)
         >> lensStructureSize_;
    root >> xml::Obj("metalmaterialindex") >> xml::Default(metalMaterialIndex_)
         >> metalMaterialIndex_;
    root >> xml::Obj("belowmaterialindex") >> xml::Default(belowMaterialIndex_)
         >> belowMaterialIndex_;
    root >> xml::Obj("inmaterialindex") >> xml::Default(inMaterialIndex_)
         >> inMaterialIndex_;
    root >> xml::Obj("dr") >> xml::Default(dr_) >> dr_;
    root >> xml::Obj("error") >> xml::Default(error_) >> error_;
    root >> xml::Obj("columntype") >> xml::Default(0) >> columnType_;
    root >> xml::Obj("exportlayer") >> xml::Default(0) >> exportLayer_;
    if(!root.obj("raytracer").empty()) {
        root >> xml::Obj("raytracer") >> rayTracer_;
    }
    root >> xml::Close();
}

// Return the patch ratio (rounded to 2 decimal places)
// that will give the specified effective refractive index at the incidence angle
// and unit cell
double designer::LensDesigner::findPatchRatio(double n, double incidenceAngle,
                                              double unitCell, double& error) {
    double result = m_->d()->parameterTables().findPatchRatio(
            unitCell, layerSpacing_, incidenceAngle / box::Constants::deg2rad_,
            centerFrequency_, n, error);
    // Round to two decimal places
    result = std::round(result * 100.0) / 100.0;
    return result;
}

// Round the given square size to one that fits the FDTD grid
double designer::LensDesigner::fdtdSize(double s) const {
    double percentPer2Cells = 100.0 * 2.0 / fdtdCellsPerUnitCell_;
    return std::round(s / percentPer2Cells) * percentPer2Cells;
}

// Make a square plate
domain::Shape* designer::LensDesigner::makeSquare(const box::Vector<double>& pos,
                                                  MatchedColumn& column, size_t layer,
                                                  bool left, size_t y,
                                                  size_t numDuplicates) {
    CellConfig& cell = column.cell(layer);
    domain::Shape* shape = nullptr;
    box::Vector<double> direction;
    if(left) {
        direction = box::Vector<double>(-1, 1, 1);
    } else {
        direction = box::Vector<double>(1, 1, 1);
    }
    switch(modelling_) {
        case Modelling::metamaterialCells: {
            for(size_t i = 0; i < numDuplicates; i++) {
                auto* s = m_->designers().makeCuboid();
                shape = s;
                double size = cell.fdtdPlateSize();
                box::Vector<double> offset(unitCell_ / 2.0 * size / 100.0,
                                           unitCell_ / 2.0 * size / 100.0,
                                           dr_ / 2.0);
                box::Vector<double> layerOffset(0.0, 0.0, i * layerSpacing_);
                s->p1(pos * direction - offset + layerOffset);
                s->p2(pos * direction + offset + layerOffset);
                s->material(metalMaterialIndex_);
                std::stringstream text;
                text << "Col" << column.index() << "Layer" << layer + i << (left ? "L" : "R")
                     << y;
                s->name(text.str());
            }
            break;
        }
        case Modelling::fingerPatterns: {
            auto* s = m_->designers().makeFingerPlate();
            shape = s;
            s->center().value(pos * direction);
            s->cellSizeX(box::Expression(unitCell_));
            s->increment().x().value(unitCell_);
            s->increment().y().value(unitCell_);
            s->increment().z().value(layerSpacing_);
            s->duplicate().z().value(numDuplicates);
            s->layerThickness(box::Expression(plateThickness_));
            s->fillMaterial(domain::Domain::defaultMaterial);
            s->material(metalMaterialIndex_);
            s->plateSize().value(cell.plateSize());
            s->minFeatureSize(box::Expression(minFeatureSize_ / unitCell_
                                              / box::Constants::percent_));
            s->fingerOffset(box::Expression(fingerOffset_ / unitCell_
                                            / box::Constants::percent_));
            s->fingersTop(true);
            s->fingersBottom(false);
            s->fingersLeft(true);
            s->fingersRight(false);
            std::stringstream text;
            text << "Col" << column.index() << "Layer" << layer << (left ? "L" : "R") << y;
            s->name(text.str());
            break;
        }
        case Modelling::dielectricBlocks: {
            auto* s = m_->designers().makeCuboidArray();
            shape = s;
            s->center().value(pos * direction);
            s->size().value({unitCell_, unitCell_, layerSpacing_});
            s->increment().value({0, 0, layerSpacing_});
            s->duplicate().value({1, 1, numDuplicates});
            auto* mat = m_->d()->getMaterial(cell.dielectricMaterial());
            if(mat == nullptr || mat->index() == domain::Domain::defaultMaterial) {
                // Create a material for this cell
                mat = m_->designers().makeNormalMaterial();
            }
            // Configure the material
            mat->epsilon(box::Expression(cell.refractiveIndex()
                                         * cell.refractiveIndex()
                                         * box::Constants::epsilon0_));
            mat->mu(box::Expression(box::Constants::mu0_));
            mat->sigma(box::Expression(box::Constants::sigma0_));
            mat->sigmastar(box::Expression(box::Constants::sigmastar0_));
            mat->color(box::Constants::Colour::colourCyan);
            std::stringstream name;
            name << "Dielectric" << layer << "_" << column.index();
            mat->name(name.str());
            cell.dielectricMaterial(mat->index());
            s->material(mat->index());
            std::stringstream text;
            text << "Col" << column.index() << "Layer" << layer << (left ? "L" : "R") << y;
            s->name(text.str());
            break;
        }
    }
    if(shape != nullptr) {
        if(left) {
            cell.leftShape(y, shape->identifier());
        } else {
            cell.rightShape(y, shape->identifier());
        }
    }
    return shape;
}

// Make a square plate for a curved column
domain::Shape* designer::LensDesigner::makeCurvedSquare(MatchedColumn& column, size_t layer,
                                                        double x, bool slice) {
    CellConfig& cell = column.cell(layer);
    domain::Shape* shape = nullptr;
    double z = static_cast<double>(numLayers_ - layer) * layerSpacing_ - layerSpacing_ / 2.0;
    switch(modelling_) {
        case Modelling::metamaterialCells: {
            break;
        }
        case Modelling::fingerPatterns: {
            auto* s = m_->designers().makeFingerPlate();
            shape = s;
            s->center().value({x, 0, z});
            s->cellSizeX(box::Expression(cell.unitCell()));
            s->cellSizeY(box::Expression(unitCell_));
            s->increment().value({cell.unitCell(), cell.unitCell(), layerSpacing_});
            s->layerThickness(box::Expression(plateThickness_));
            s->fillMaterial(domain::Domain::defaultMaterial);
            s->material(metalMaterialIndex_);
            s->plateSize().value(cell.plateSize());
            s->minFeatureSize(box::Expression(minFeatureSize_ / unitCell_
                                              / box::Constants::percent_));
            s->fingerOffset(box::Expression(fingerOffset_ / unitCell_
                                            / box::Constants::percent_));
            s->fingersTop(false);
            s->fingersBottom(false);
            s->fingersLeft(true);
            s->fingersRight(false);
            std::stringstream text;
            text << "Col" << column.index() << "Layer" << layer << (x <= 0.0 ? "L" : "R");
            s->name(text.str());
            break;
        }
        case Modelling::dielectricBlocks: {
            auto* s = m_->designers().makeCuboidArray();
            shape = s;
            s->center().value({x, 0, z});
            if(slice) {
                s->size().value({cell.unitCell(), unitCell(), layerSpacing_});
                s->increment().value({cell.unitCell(), unitCell(), layerSpacing_});
            } else {
                s->size().value({cell.unitCell(), cell.unitCell(), layerSpacing_});
                s->increment().value({cell.unitCell(), cell.unitCell(), layerSpacing_});
            }
            auto* mat = m_->d()->getMaterial(cell.dielectricMaterial());
            if(mat == nullptr || mat->index() == domain::Domain::defaultMaterial) {
                // Create a material for this cell
                mat = m_->designers().makeNormalMaterial();
            }
            // Configure the material
            mat->epsilon(box::Expression(cell.refractiveIndex()
                                         * cell.refractiveIndex()
                                         * box::Constants::epsilon0_));
            mat->mu(box::Expression(box::Constants::mu0_));
            mat->sigma(box::Expression(box::Constants::sigma0_));
            mat->sigmastar(box::Expression(box::Constants::sigmastar0_));
            if((column.index() % 2) == 0) {
                mat->color(box::Constants::Colour::colourMagenta);
            } else {
                mat->color(box::Constants::Colour::colourCyan);
            }
            std::stringstream name;
            name << "Dielectric" << layer << "_" << column.index();
            mat->name(name.str());
            cell.dielectricMaterial(mat->index());
            s->material(mat->index());
            std::stringstream text;
            text << "Col" << column.index() << "Layer" << layer << (x <= 0.0 ? "L" : "R");
            s->name(text.str());
            break;
        }
    }
    if(shape != nullptr) {
        if(x <= 0.0) {
            cell.leftShape(0, shape->identifier());
        } else {
            cell.rightShape(0, shape->identifier());
        }
    }
    return shape;
}

// Write the results out as a STL file
void designer::LensDesigner::exportDiameterSliceAsStl(const std::string& fileName) {
    // Set up the export writer
    box::ExportWriter::Clipping clipping = box::ExportWriter::Clipping::none;
    if(exportSymmetricalHalf_) {
        clipping = box::ExportWriter::Clipping::xAxis;
    }
    box::StlWriter stl(fileName, clipping);
    // Now export
    exportDiameterSlice(stl);
}

// Write the results out as an HFSS python file
void designer::LensDesigner::exportDiameterSliceAsHfss(const std::string& fileName) {
    // Set up the export writer
    box::ExportWriter::Clipping clipping = box::ExportWriter::Clipping::none;
    if(exportSymmetricalHalf_) {
        clipping = box::ExportWriter::Clipping::xAxis;
    }
    box::HfssWriter hfss(fileName, clipping, false, true,
                         false);
    // Now export
    exportDiameterSlice(hfss);
}

// Output a quadrant layer in DXF format
void designer::LensDesigner::exportQuadrantLayerAsDxf(const std::string& fileName,
                                                      size_t layer) {
    // Set up the export writer
    box::DxfWriter dxf(fileName, box::ExportWriter::Clipping::xyAxis);
    exportQuadrantLayer(dxf, layer);
}

// Output a quadrant in HFSS python script format
void designer::LensDesigner::exportQuadrantAsHfss(const std::string& fileName) {
    box::HfssWriter hfss(fileName, box::ExportWriter::Clipping::xyAxis,
                         false, false, false);
    for(size_t layer = 0; layer < numLayers_; layer++) {
        exportQuadrantLayer(hfss, layer);
    }
}

// Output a quadrant layer in HFSS python script format
void designer::LensDesigner::exportQuadrantLayerAsHfss(const std::string& fileName) {
    box::HfssWriter hfss(fileName, box::ExportWriter::Clipping::xyAxis,
                         false, false, false);
    exportQuadrantLayer(hfss, exportLayer_);
}

// We shall write out a whole circle's worth of lens by overlaying the circle
// with a square grid with unit cell spacing.  Then at the center of each grid
// cell we will calculate the radius.  Then we will find the two cells along
// the diameter that correspond to this location and interpolate the plate
// size between them.  Then we shall generate a plate for that plate size.
void designer::LensDesigner::exportQuadrantLayer(box::ExportWriter& writer,
                                                 size_t layer) {
    std::list<domain::Shape*> shapes;
    double z = static_cast<double>(numLayers_ - layer) * layerSpacing_ - layerSpacing_ / 2.0;
    // Iterate over the cells of a quadrant
    for(size_t cx = 0; cx < columns_.size(); cx++) {
        for(size_t cy = 0; cy < columns_.size(); cy++) {
            // The unit cell of this layer (we must assume it is constant on the layer)
            double unitCell = columns_.front().cell(layer).unitCell();
            // Find the distance of this cell to the center
            double x = cx * unitCell;
            double y = cy * unitCell;
            if(hasEvenColumns()) {
                x += unitCell * box::Constants::half_;
                y += unitCell * box::Constants::half_;
            }
            double r = std::sqrt(x * x + y * y);
            // Find the cells below and above this distance
            size_t belowPos;
            size_t abovePos;
            double rBelow;
            double rAbove;
            if(hasEvenColumns()) {
                if(r <= unitCell * box::Constants::half_) {
                    belowPos = 0;
                    abovePos = 0;
                } else {
                    belowPos = std::floor((r - unitCell * box::Constants::half_) / unitCell);
                    abovePos = std::ceil((r - unitCell * box::Constants::half_) / unitCell);
                }
                rBelow = belowPos * unitCell + unitCell * box::Constants::half_;
                rAbove = abovePos * unitCell + unitCell * box::Constants::half_;
            } else {
                belowPos = std::floor(r / unitCell);
                abovePos = std::ceil(r / unitCell);
                rBelow = belowPos * unitCell;
                rAbove = abovePos * unitCell;
            }
            if(belowPos < columns_.size()) {
                MatchedColumn* belowCol = nullptr;
                MatchedColumn* aboveCol = nullptr;
                belowCol = &(*std::next(columns_.begin(), belowPos));
                if(abovePos < columns_.size()) {
                    aboveCol = &(*std::next(columns_.begin(), abovePos));
                } else {
                    aboveCol = belowCol;
                }
                auto& belowCell = belowCol->cell(layer);
                auto& aboveCell = aboveCol->cell(layer);
                // Don't do anything if the below cell plate size is zero
                if(belowCell.plateSize() > 0) {
                    // Interpolate the refractive index and incidence angle between these two
                    double refIndex = box::Utility::interpolate(
                            rBelow, rAbove,
                            belowCell.refractiveIndex(), aboveCell.refractiveIndex(), r);
                    double incAngle = box::Utility::interpolate(
                            rBelow, rAbove,
                            belowCell.incidenceAngle(), aboveCell.incidenceAngle(), r);
                    // Now get the plate size that provides the refractive index
                    double error = 0.0;
                    double plateSize = findPatchRatio(refIndex, incAngle, unitCell, error);
                    if(error > 0.0) {
                        std::cout << "Error in plate size, x=" << cx << ", y=" << cy
                                  << ", refIndex=" << refIndex << ", incAngle=" << incAngle
                                  << ", unitCell=" << unitCell << ", error=" << error
                                  << std::endl;
                    }
                    if(plateSize > 0.5) {  // Don't use unfeasibly small plates.
                        // Make a shape to implement the plate
                        switch(modelling_) {
                            case Modelling::metamaterialCells:
                            case Modelling::fingerPatterns: {
                                auto* s = m_->designers().makeFingerPlate();
                                s->center({x, y, z});
                                s->cellSizeX(box::Expression(unitCell));
                                s->cellSizeY(box::Expression(unitCell));
                                s->increment({unitCell, unitCell, layerSpacing_});
                                s->layerThickness(box::Expression(plateThickness_));
                                s->fillMaterial(domain::Domain::defaultMaterial);
                                s->material(metalMaterialIndex_);
                                s->plateSize(box::Expression(plateSize));
                                s->minFeatureSize(box::Expression(minFeatureSize_ / unitCell
                                                                  / box::Constants::percent_));
                                s->fingerOffset(box::Expression(fingerOffset_ / unitCell
                                                                / box::Constants::percent_));
                                s->fingersTop(true);
                                s->fingersBottom(false);
                                s->fingersLeft(true);
                                s->fingersRight(false);
                                std::stringstream text;
                                text << "ColX" << cx << "Y" << cy << "Layer" << layer;
                                s->name(text.str());
                                shapes.push_back(s);
                                break;
                            }
                            case Modelling::dielectricBlocks: {
#if 0
                                auto* s = m_->designers().makeCuboidArray();
                                s->center().value(pos * direction);
                                s->size().value({unitCell_, unitCell_, layerSpacing_});
                                s->increment().value({0, 0, layerSpacing_});
                                s->duplicate().value({1, 1, numDuplicates});
                                auto* mat = m_->d()->getMaterial(cell.dielectricMaterial());
                                if(mat == nullptr || mat->index() == domain::Domain::defaultMaterial) {
                                    // Create a material for this cell
                                    mat = m_->designers().makeNormalMaterial();
                                }
                                // Configure the material
                                mat->epsilon(box::Expression(cell.refractiveIndex()
                                                             * cell.refractiveIndex()
                                                             * box::Constants::epsilon0_));
                                mat->mu(box::Expression(box::Constants::mu0_));
                                mat->sigma(box::Expression(box::Constants::sigma0_));
                                mat->sigmastar(box::Expression(box::Constants::sigmastar0_));
                                mat->color(box::Constants::Colour::colourCyan);
                                std::stringstream name;
                                name << "Dielectric" << layer << "_" << column.index();
                                mat->name(name.str());
                                cell.dielectricMaterial(mat->index());
                                s->material(mat->index());
                                std::stringstream text;
                                text << "Col" << column.index() << "Layer" << layer << (left ? "L" : "R") << y;
                                s->name(text.str());
#endif
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    // Export them
    box::Expression::Context c;
    m_->variables().fillContext(c);
    for(auto& shape : shapes) {
        if(shape != nullptr) {
            shape->evaluate(c);
            shape->write(writer, {shape->material()});
        }
    }
    // Clean up
    m_->designers().removeShapes(shapes);
}

// Write the results out as a slice across the diameter
void designer::LensDesigner::exportDiameterSlice(box::ExportWriter& writer) {
    // Get the shapes
    loadModelConfig();
    std::list<domain::Shape*> shapes;
    switch(columnType_) {
        case ColumnType::straight:
            loadStraightColumns(shapes);
            break;
        case ColumnType::curved:
        case ColumnType::curvedConstG:
            loadCurvedColumns(shapes, true);
            break;
    }
    // Export them
    box::Expression::Context c;
    m_->variables().fillContext(c);
    for(auto& shape : shapes) {
        if(shape != nullptr) {
            shape->evaluate(c);
            shape->write(writer, {shape->material()});
        }
    }
    // Clean up
    m_->designers().removeShapes(shapes);
}

// Write a column of results out
void designer::LensDesigner::exportColumn(box::ExportWriter& writer) {
    if(columnNumber_ < columns_.size()) {
        // The domain
        double g = unitCell_;
        box::Vector<double> p1(-g / 2, -g / 2, m_->p()->p1().z().value());
        box::Vector<double> p2(g / 2, g / 2, m_->p()->p2().z().value());
        writer.writeCuboid(p1 / box::Constants::milli_,
                           p2 / box::Constants::milli_,
                           "Domain", "Background");
        // The column
        auto& col = *std::next(columns_.begin(), columnNumber_);
        // The position
        box::Vector<double> pos;
        // The layers
        for(size_t layer = 0; layer < numLayers_; layer++) {
            CellConfig& cell = col.cell(layer);
            double size = cell.plateSize();
            if(size > 0.5) {  // Don't export if the square is unrealistically small
                if(cell.numLeftShapes() > 0) {
                    auto* plate = dynamic_cast<domain::UnitCellShape*>(
                            m_->d()->getShape(cell.leftShape(0)));
                    if(plate != nullptr) {
                        auto t = plate->center().value();
                        plate->center().value(pos);
                        plate->write(writer, {plate->material()});
                        plate->center().value(t);
                    }
                }
            }
            pos.z(pos.z() + layerSpacing_);
        }
    }
}

// Update the refractive indices of the existing model
void designer::LensDesigner::updateRefractiveIndices() {
    if(isResultValid()) {
        for(auto& column : columns_) {
            for(size_t layer = 0; layer < numLayers_; layer++) {
                if(column.cells().size() > layer) {
                    CellConfig& cell = column.cell(layer);
                    for(size_t j = 0; j < sliceThickness_; j++) {
                        switch(modelling_) {
                            case Modelling::fingerPatterns: {
                                double error;
                                cell.plateSize(findPatchRatio(
                                        cell.refractiveIndex(),
                                        0.0, cell.unitCell(), error));
                                cell.error(error);
                                if(cell.numLeftShapes() > j) {
                                    auto* left = dynamic_cast<domain::FingerPlate*>(
                                            m_->d()->getShape(cell.leftShape(j)));
                                    if(left != nullptr) {
                                        left->plateSize().value(cell.plateSize());
                                    }
                                }
                                if(cell.numRightShapes() > j) {
                                    auto* right = dynamic_cast<domain::FingerPlate*>(
                                            m_->d()->getShape(cell.rightShape(j)));
                                    if(right != nullptr) {
                                        right->plateSize().value(cell.plateSize());
                                    }
                                }
                                break;
                            }
                            case Modelling::metamaterialCells: {
                                double error;
                                cell.plateSize(findPatchRatio(
                                        cell.refractiveIndex(),
                                        0.0, cell.unitCell(), error));
                                cell.error(error);
                                box::Vector<double> center;
                                box::Vector<double> offset(
                                        unitCell_ / 2.0 * cell.plateSize() / 100.0,
                                        unitCell_ / 2.0 * cell.plateSize() / 100.0,
                                        dr_ / 2.0);
                                auto* left = dynamic_cast<domain::Cuboid*>(
                                        m_->d()->getShape(cell.leftShape(j)));
                                if(left != nullptr) {
                                    center = (left->p1() + left->p2()) / 2.0;
                                    left->p1(center - offset);
                                    left->p2(center + offset);
                                }
                                auto* right = dynamic_cast<domain::Cuboid*>(
                                        m_->d()->getShape(cell.rightShape(j)));
                                if(right != nullptr) {
                                    center = (right->p1() + right->p2()) / 2.0;
                                    right->p1(center - offset);
                                    right->p2(center + offset);
                                }
                                break;
                            }
                            case Modelling::dielectricBlocks: {
                                if(cell.dielectricMaterial() >=
                                   domain::Domain::firstUserMaterial) {
                                    auto* mat = m_->d()->getMaterial(
                                            cell.dielectricMaterial());
                                    if(mat != nullptr) {
                                        mat->epsilon(
                                                box::Expression(
                                                        cell.refractiveIndex() *
                                                        cell.refractiveIndex() *
                                                        box::Constants::epsilon0_));
                                        mat->initialise();
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        switch(modelling_) {
            case Modelling::metamaterialCells:
            case Modelling::fingerPatterns:
                m_->d()->generate();
                break;
            case Modelling::dielectricBlocks:
                break;
        }
    }
}

// Returns true if the result is valid and none of the
// calculations return NAN
bool designer::LensDesigner::isResultValid() {
    bool result = true;
    for(auto& col : columns_) {
        if(!col.isResultValid()) {
            result = false;
        }
    }
    return result;
}

// Create a genetic search to improve a column
void designer::LensDesigner::generateColumnGa() {
    // Load the model with the columns as currently designed
    loadModel();
    // Create the genetic search
    auto* s = loadGeneticSearch();
    loadLensTransmissionTarget(s);
    loadLensReflectionTarget(s);
    loadLensPhaseTarget(s);
    // Find the column we are to work with
    auto col = std::next(columns_.begin(), columnNumber_);
    if(col != columns_.end()) {
        // Now create variables for the cells in the search column
        // and connect them to the materials
        size_t layer = 0;
        for(auto& cell : col->cells()) {
            // Make the variable
            std::stringstream name;
            name << "Col" << columnNumber_ << "Layer" << layer;
            double epsilon = cell.refractiveIndex() * cell.refractiveIndex();
            auto* var = makeGaVariable(name.str(), epsilon, s);
            cell.variable(var->identifier());
            layer++;
            // Connect to the material
            auto* mat = m_->d()->getMaterial(cell.dielectricMaterial());
            if(mat != nullptr) {
                std::stringstream v;
                v << "epsilon0*" << name.str();
                mat->epsilon(box::Expression(v.str()));
                mat->evaluate();
                mat->color(box::Constants::Colour::colourYellow);
            }
        }
    }
    // Create the initial population
    s->clearAndDiscard();
    // Fix up the first individual to be the designed version
    box::Expression::Context c;
    m_->variables().fillContext(c);
    if(!s->population()->individuals().empty()) {
        auto ind = s->population()->individuals().front();
        for(auto& g : ind->chromosome()->genes()) {
            auto* varGene = dynamic_cast<gs::VariableGene*>(g);
            if(varGene != nullptr) {
                auto* v = m_->variables().find(varGene->variableId());
                if(v != nullptr) {
                    varGene->value(v->value());
                }
            }
        }
        ind->encodeGenes(s);
    }
    // Notify changes
    m_->doNotification(fdtd::Model::notifyVariableChange);
    m_->doNotification(fdtd::Model::notifyMaterialChange);
    m_->doNotification(fdtd::Model::notifySequencerChange);
}

// Load and configure a genetic search sequencer into the model
gs::GeneticSearch* designer::LensDesigner::loadGeneticSearch() {
    gs::GeneticSearch* result = m_->designers().makeGeneticSearch();
    result->microGa(true);
    result->algorithm(gs::GeneticSearch::algorithmFdtd);
    result->averageSensor(false);
    result->xyComponents(false);
    result->useExistingDomain(true);
    result->populationSize(5);
    result->unfitnessThreshold(0.00001);
    result->selectionMode(gs::GeneticSearch::selectionTournament);
    result->tournamentSize(2);
    result->breedingPoolSize(5);
    result->crossoverMode(gs::GeneticSearch::crossoverUniform);
    result->keepParents(false);
    result->collectFieldData(false);
    return result;
}

// Load a lens reflection target into the genetic search
void designer::LensDesigner::loadLensReflectionTarget(gs::GeneticSearch* s) const {
    auto* a = dynamic_cast<sensor::LensReflectionAnalyser*>(
            s->m()->analysers().make(sensor::Analysers::modeLensReflection));
    auto* f = dynamic_cast<gs::FitnessAnalyser*>(
            s->fitness()->make(gs::FitnessFunction::modeAnalyser));
    a->frequency(box::Expression(centerFrequency_));
    a->referenceTime(box::Expression(std::sqrt(permittivityBelow_) * focusDistance_ /
                                     box::Constants::c_));
    a->name("GA Reflection");
    f->analyserId(a->identifier());
}

// Load a lens transmission target into the genetic search
void designer::LensDesigner::loadLensTransmissionTarget(gs::GeneticSearch* s) const {
    auto* a = dynamic_cast<sensor::LensTransmissionAnalyser*>(
            s->m()->analysers().make(sensor::Analysers::modeLensTransmission));
    auto* f = dynamic_cast<gs::FitnessAnalyser*>(
            s->fitness()->make(gs::FitnessFunction::modeAnalyser));
    a->frequency(box::Expression(centerFrequency_));
    a->name("GA Transmission");
    f->analyserId(a->identifier());
}

// Load a lens phase target into the genetic search
void designer::LensDesigner::loadLensPhaseTarget(gs::GeneticSearch* s) const {
    auto* a = dynamic_cast<sensor::LensPhaseAnalyser*>(
            s->m()->analysers().make(sensor::Analysers::modeLensPhase));
    auto* f = dynamic_cast<gs::FitnessAnalyser*>(
            s->fitness()->make(gs::FitnessFunction::modeAnalyser));
    a->frequency(box::Expression(centerFrequency_));
    a->name("GA Phase");
    f->analyserId(a->identifier());
}

// Generate a genetic algorithm configuration that allows for the
// optimisation of the design.  This currently operates in two modes,
// for dielectric blocks the refractive index is varied around the desired
// value, for binary plates the plate pattern is varied.
void designer::LensDesigner::generateFullAdjustGa() {
    // Load the model with the columns as currently designed
    loadModel();
    // Create the genetic search
    auto* s = loadGeneticSearch();
    loadLensTransmissionTarget(s);
    loadLensReflectionTarget(s);
    loadLensPhaseTarget(s);
    // For each column...
    for(auto& col : columns_) {
        // Now create the permittivity variables for this column
        size_t topLayer = 0;
        size_t bottomLayer = numLayers_ - 1;
        std::vector<fdtd::Variable*> vars;
        vars.resize(numLayers_);
        // The variables for the ARC stages
        for(size_t stage = 0; stage < numArcStages_ && topLayer <= bottomLayer; stage++) {
            // Create a variable for this ARC stage
            std::stringstream name;
            name << "Col" << col.index() << "Stage" << stage;
            makeVariable(layersPerArcStage_, topLayer, bottomLayer,
                         vars, col, name.str(), s);
        }
        // The variable for the central layers
        if(topLayer <= bottomLayer) {
            std::stringstream name;
            name << "Col" << col.index() << "Central";
            makeVariable(numLayers_, topLayer, bottomLayer,
                         vars, col, name.str(), s); // Note: the rest of the layers
        }
        // Connect the variables to the appropriate objects
        connectVariables(vars, col);
    }
    // Create the initial population
    s->clearAndDiscard();
    // Fix up the first individual to be the designed version
    box::Expression::Context c;
    m_->variables().fillContext(c);
    if(!s->population()->individuals().empty()) {
        auto ind = s->population()->individuals().front();
        for(auto& g : ind->chromosome()->genes()) {
            auto* varGene = dynamic_cast<gs::VariableGene*>(g);
            if(varGene != nullptr) {
                auto* v = m_->variables().find(varGene->variableId());
                if(v != nullptr) {
                    varGene->value(v->value());
                }
            }
        }
        ind->encodeGenes(s);
    }
    // Notify changes
    m_->doNotification(fdtd::Model::notifyVariableChange);
    m_->doNotification(fdtd::Model::notifyAnalyserChange);
    m_->doNotification(fdtd::Model::notifyShapeChange);
    m_->doNotification(fdtd::Model::notifyMaterialChange);
    m_->doNotification(fdtd::Model::notifySequencerChange);
}

// Make a variable and corresponding gene for a genetic algorithm
fdtd::Variable* designer::LensDesigner::makeGaVariable(const std::string& name,
                                                       double value, gs::GeneticSearch* s) {
    // Make the variable
    std::stringstream text;
    text << value;
    auto* var = m_->designers().makeVariable(name, text.str());
    // Load a gene template
    int identifier = s->geneFactory()->allocIdentifier();
    auto t = new gs::GeneTemplate(identifier, s->geneFactory(),
                                  gs::GeneTemplate::geneTypeVariable);
    s->geneTemplateAdded(t);
    double from{};
    double to{};
    size_t bits{};
    switch(modelling_) {
        case Modelling::dielectricBlocks: {
            double delta = value * geneticRange_ * box::Constants::percent_ *
                           box::Constants::half_;
            from = value - delta;
            to = value + delta;
            bits = geneticBits_;
            break;
        }
        case Modelling::metamaterialCells:
        case Modelling::fingerPatterns:
            break;
    }
    t->range({bits, from, to});
    t->variableId(var->identifier());
    return var;
}

// Make the variable for a set of layers in the design
void designer::LensDesigner::makeVariable(size_t numLayers, size_t& topLayer,
                                          size_t& bottomLayer,
                                          std::vector<fdtd::Variable*>& vars,
                                          MatchedColumn& col, const std::string& name,
                                          gs::GeneticSearch* s) {

    auto& cell = col.cell(topLayer);
    double value = cell.refractiveIndex() * cell.refractiveIndex();
    auto* var = makeGaVariable(name, value, s);
    // Store it against the layers in this stage
    for(size_t i = 0; i < numLayers && topLayer <= bottomLayer; i++) {
        vars[topLayer] = var;
        vars[bottomLayer] = var;
        topLayer++;
        bottomLayer--;
    }
}

// Connect the genetic variables to the appropriate object
void designer::LensDesigner::connectVariables(std::vector<fdtd::Variable*>& vars,
                                              designer::MatchedColumn& col) {
    for(size_t layer = 0; layer < numLayers_; layer++) {
        for(size_t j = 0; j < sliceThickness_; j++) {
            switch(modelling_) {
                case Modelling::dielectricBlocks: {
                    // Connect the variables to the materials
                    auto* mat = m_->d()->getMaterial(col.cell(layer).dielectricMaterial());
                    if(mat != nullptr) {
                        std::stringstream v;
                        v << "epsilon0*" << vars[layer]->name();
                        mat->epsilon(box::Expression(v.str()));
                        mat->evaluate();
                        mat->color(box::Constants::Colour::colourYellow);
                    }
                    break;
                }
                case Modelling::metamaterialCells:
                case Modelling::fingerPatterns: {
                    // Connect the variables to the shapes
                    auto* shape = dynamic_cast<domain::CatalogueLayer*>(
                            m_->d()->getShape(col.cell(layer).leftShape(j)));
                    if(shape != nullptr) {
                        shape->select().text(vars[layer]->name());
                    }
                    shape = dynamic_cast<domain::CatalogueLayer*>(
                            m_->d()->getShape(col.cell(layer).rightShape(j)));
                    if(shape != nullptr) {
                        shape->select().text(vars[layer]->name());
                    }
                    break;
                }
            }
        }
    }
}

// Write the results out as an HFSS python file
void designer::LensDesigner::exportColumnAsHfss(const std::string& fileName) {
    // Set up the export writer
    box::HfssWriter hfss(fileName, box::ExportWriter::Clipping::none,
                         true, true, true);
    // Now export
    exportColumn(hfss);
}

// Return the refractive index at the specified location in the domain
double designer::LensDesigner::getRefractiveIndexAt(double x, double z) {
    double result = 0.0;
    if(!columns_.empty()) {
        // Are we in the lens structure, above it or below it?
        if(z < -layerSpacing_ / 2.0) {
            // We are above the lens
            result = std::sqrt(permittivityAbove_);
        } else if(z > (numLayers_ * layerSpacing_ - layerSpacing_ / 2.0)) {
            // We are below the lens
            result = std::sqrt(permittivityBelow_);
        } else {
            // We are in the lens, which column and layer?
            auto layerNum = static_cast<size_t>(
                    std::floor((z + layerSpacing_ / 2.0) / layerSpacing_));
            x = abs(x);
            if(x > columns_.size() * unitCell_) {
                // We are out of the edge of the lens
                result = std::sqrt(permittivityAbove_);
            } else if(x < unitCell_ / 2.0) {
                if((columnsInDiameter_ % 2) == 1) {
                    // Odd number of columns
                    result = box::Utility::interpolate(
                            0.0, unitCell_,
                            column(0).cell(layerNum).refractiveIndex(),
                            column(1).cell(layerNum).refractiveIndex(),
                            x);
                } else {
                    // Even number of columns
                    result = column(0).cell(layerNum).refractiveIndex();
                }
            } else {
                auto colNumLow = static_cast<size_t>(
                        std::floor((x - unitCell_ / 2.0) / unitCell_));
                size_t colNumHigh = colNumLow + 1;
                colNumLow = std::min(colNumLow, columns_.size() - 1);
                colNumHigh = std::min(colNumHigh, columns_.size() - 1);
                if((columnsInDiameter_ % 2) == 1) {
                    // Odd number of columns
                    result = box::Utility::interpolate(
                            colNumLow * unitCell_, colNumHigh * unitCell_,
                            column(colNumLow).cell(layerNum).refractiveIndex(),
                            column(colNumHigh).cell(layerNum).refractiveIndex(),
                            x);
                } else {
                    // Odd number of columns
                    result = box::Utility::interpolate(
                            colNumLow * unitCell_ + unitCell_ / 2.0,
                            colNumHigh * unitCell_ + unitCell_ / 2.0,
                            column(colNumLow).cell(layerNum).refractiveIndex(),
                            column(colNumHigh).cell(layerNum).refractiveIndex(),
                            x);
                }
            }
        }
    }
    return result;
}

// Return the specified column
designer::MatchedColumn& designer::LensDesigner::column(size_t colNum) {
    auto pos = std::find_if(columns_.begin(), columns_.end(),
                            [colNum](const MatchedColumn& col) {
                                return col.index() == colNum;
                            });
    return *pos;
}

// Get the column data in a CSV format
std::string designer::LensDesigner::getColumnsCsv(WhichParameter which) {
    std::stringstream t;
    t << "Layer";
    for(size_t col = 0; col < columns_.size(); col++) {
        t << "\t" << col;
    }
    t << std::endl;
    for(size_t layer = 0; layer < numLayers_; layer++) {
        t << layer;
        for(auto& c : columns_) {
            double value;
            switch(which) {
                case WhichParameter::refractiveIndices:
                    value = c.cell(layer).refractiveIndex();
                    break;
                case WhichParameter::unitCells:
                    value = c.cell(layer).unitCell();
                    break;
                case WhichParameter::incidenceAngle:
                    value = c.cell(layer).incidenceAngle();
                    break;
                case WhichParameter::patchRatio:
                    value = c.cell(layer).plateSize();
                    break;
                case WhichParameter::error:
                    value = c.cell(layer).error();
                    break;
            }
            t << "\t" << value;
        }
        t << std::endl;
    }
    return t.str();
}

// Set the column data from a CSV format
void designer::LensDesigner::putColumnsCsv(const std::string& text) {
    std::vector<std::string> lines = box::Utility::split(text, "\n");
    size_t layer = 0;
    bool firstLine = true;
    for(auto& line : lines) {
        if(firstLine) {
            // Skip the header line
            firstLine = false;
        } else {
            std::vector<std::string> values = box::Utility::split(line, "\t");
            auto col = columns_.begin();
            bool firstCol = true;
            for(auto& value : values) {
                if(firstCol) {
                    // Skip the layer number column
                    firstCol = false;
                } else {
                    double n = box::Utility::toDouble(value);
                    if(layer < numLayers_ && col != columns_.end()) {
                        col->cell(layer).refractiveIndex(n);
                    }
                    ++col;
                }
            }
            layer++;
        }
    }
    updateRefractiveIndices();
}
